import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { KeycloakService } from 'keycloak-angular';
import {
  CitizenServiceAdapter,
  FeatureCitizenServicesModule,
} from '@sw-code/urbo-cms-feature-citizen-services';
import { FeatureNewsModule, NewsAdapter } from '@sw-code/urbo-cms-feature-news';
import {
  EventAdapter,
  FeatureEventModule,
} from '@sw-code/urbo-cms-feature-events';
import {
  FeatureWasteModule,
  WasteAdapter,
} from '@sw-code/urbo-cms-feature-waste';
import {
  AppFeedbackAdapter,
  FeatureAppFeedbackModule,
} from '@sw-code/urbo-cms-feature-app-feedback';
import { FeaturePoisModule, PoiAdapter } from '@sw-code/urbo-cms-feature-pois';
import {
  AdminAdapter,
  FeatureAdminModule,
} from '@sw-code/urbo-cms-feature-admin';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  CoreAdapter,
  CoreModule,
  MAP_STYLE_URL,
  MAP_ZOOM,
} from '@sw-code/urbo-cms-core';
import { RouterModule } from '@angular/router';
import { AppDashboardComponent } from './dashboard/app-dashboard.component';
import { environment } from '../environments/environment';
import {
  Configuration as SearchApiConfiguration,
  SearchApiModule,
} from '@sw-code/urbo-cms-search-api';
import {
  Configuration as CoreApiConfiguration,
  CoreApiModule,
} from '@sw-code/urbo-cms-backend-api';
import {
  AuthApiModule,
  Configuration as AuthApiConfiguration,
} from '@sw-code/urbo-cms-auth-api';
import {
  Configuration as QueueApiConfiguration,
  QueueApiModule,
} from '@sw-code/urbo-cms-queue-api';
import { DefaultCoreAdapter } from '@sw-code/urbo-cms-core-adapter';
import { DefaultAdminAdapter } from '@sw-code/urbo-cms-feature-admin-adapter';
import { DefaultAppFeedbackAdapter } from '@sw-code/urbo-cms-feature-appfeedback-adapter';
import { DefaultEventAdapter } from '@sw-code/urbo-cms-feature-events-adapter';
import { DefaultNewsAdapter } from '@sw-code/urbo-cms-feature-news-adapter';
import { DefaultWasteAdapter } from '@sw-code/urbo-cms-feature-waste-adapter';
import { DefaultPoiAdapter } from '@sw-code/urbo-cms-feature-pois-adapter';
import { DefaultCitizenServiceAdapter } from '@sw-code/urbo-cms-feature-citizen-services-adapter';
import { darkModeMediaQuery } from '../../dark-mode-config';
import {
  FeatureParkingModule,
  ParkingAdapter,
} from '@sw-code/urbo-cms-feature-parking';
import { DefaultParkingAdapter } from '@sw-code/urbo-cms-feature-parking-adapter';

const FEATURE_MODULES = [
  FeatureCitizenServicesModule.forRoot(),
  FeatureNewsModule.forRoot(environment),
  FeatureEventModule.forRoot(),
  FeatureWasteModule.forRoot(),
  FeatureParkingModule.forRoot(),
  FeatureAppFeedbackModule.forRoot(),
  FeaturePoisModule.forRoot(environment),
  FeatureAdminModule.forRoot(),
];

const CLIENT_MODULES = [
  CoreApiModule,
  SearchApiModule,
  AuthApiModule,
  QueueApiModule,
];

const MODULES = [
  AppRoutingModule,
  BrowserModule,
  BrowserAnimationsModule,
  CoreModule.forRoot(environment),
  RouterModule,
];

const ADAPTER_PROVIDERS = [
  { provide: CoreAdapter, useClass: DefaultCoreAdapter },
  { provide: AdminAdapter, useClass: DefaultAdminAdapter },
  { provide: AppFeedbackAdapter, useClass: DefaultAppFeedbackAdapter },
  { provide: CitizenServiceAdapter, useClass: DefaultCitizenServiceAdapter },
  { provide: EventAdapter, useClass: DefaultEventAdapter },
  { provide: NewsAdapter, useClass: DefaultNewsAdapter },
  { provide: WasteAdapter, useClass: DefaultWasteAdapter },
  { provide: PoiAdapter, useClass: DefaultPoiAdapter },
  { provide: ParkingAdapter, useClass: DefaultParkingAdapter },
];

const CLIENT_CONFIGURATION = [
  {
    provide: AuthApiConfiguration,
    useFactory: () =>
      new AuthApiConfiguration({ basePath: environment.apiBaseUrl }),
  },
  {
    provide: CoreApiConfiguration,
    useFactory: () =>
      new CoreApiConfiguration({ basePath: environment.apiBaseUrl }),
  },
  {
    provide: QueueApiConfiguration,
    useFactory: () =>
      new CoreApiConfiguration({ basePath: environment.apiBaseUrl }),
  },
  {
    provide: SearchApiConfiguration,
    useFactory: () =>
      new SearchApiConfiguration({
        basePath: environment.searchApiBaseUrl,
        credentials: {
          BearerAuth: environment.searchApiKey,
        },
      }),
  },
];

@NgModule({
  declarations: [AppComponent, AppDashboardComponent],
  imports: [...MODULES, ...FEATURE_MODULES, ...CLIENT_MODULES],
  bootstrap: [AppComponent],
  exports: [BrowserModule],
  providers: [
    KeycloakService,
    ...CLIENT_CONFIGURATION,
    ...ADAPTER_PROVIDERS,
    {
      provide: MAP_STYLE_URL,
      useFactory: () =>
        darkModeMediaQuery.matches
          ? environment.map.styleUrlDark
          : environment.map.styleUrlLight,
    },
    {
      provide: MAP_ZOOM,
      useValue: environment.map.zoom,
    },
  ],
})
export class AppModule {}
