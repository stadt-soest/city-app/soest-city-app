import { Injectable } from '@angular/core';
import {
  DynamicAuthorizationGuard,
  ModuleRegistryService,
} from '@sw-code/urbo-cms-core';
import { LayoutComponent, NotFoundComponent } from '@sw-code/urbo-cms-ui';
import { AppDashboardComponent } from './dashboard/app-dashboard.component';

@Injectable({
  providedIn: 'root',
})
export class RouteBuilderService {
  constructor(private readonly moduleRegistry: ModuleRegistryService) {}

  getRoutes() {
    const featureRoutes = this.moduleRegistry
      .getFeatureRoutes()
      .map((route) => {
        return {
          ...route,
          canActivate: [
            ...(route.canActivate || []),
            DynamicAuthorizationGuard,
          ],
        };
      });

    return [
      {
        path: '',
        component: LayoutComponent,
        children: [
          { path: '', component: AppDashboardComponent },
          ...featureRoutes,
        ],
      },
      {
        path: 'not-found',
        component: NotFoundComponent,
      },
      {
        path: '**',
        redirectTo: 'not-found',
      },
    ];
  }
}
