import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModuleRegistryService } from '@sw-code/urbo-cms-core';

@Component({
  selector: 'urbo-cms-app-dashboard',
  templateUrl: './app-dashboard.component.html',
  styleUrls: ['./app-dashboard.component.scss'],
  standalone: false,
})
export class AppDashboardComponent implements OnInit {
  dashboardData: { moduleName: string; entityCount: number }[] = [];
  latestBugReports: { route: string; answer: string; createdAt: string }[] = [];

  constructor(
    private readonly moduleRegistryService: ModuleRegistryService,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.loadDashboardData();
    this.loadLatestFeedback();
  }

  loadDashboardData() {
    this.dashboardData = this.moduleRegistryService.getEntityCounts();
  }

  loadLatestFeedback() {
    this.moduleRegistryService.getFeedbackData().subscribe({
      next: (data) => {
        this.latestBugReports = data;
      },
      error: (error) => {
        console.error('Failed to load latest feedback', error);
        this.latestBugReports = [];
      },
    });
  }

  navigateToModule(moduleName: string) {
    const routePath = this.moduleRegistryService.getModuleRoute(moduleName);
    if (routePath) {
      this.router.navigate([routePath]);
    }
  }
}
