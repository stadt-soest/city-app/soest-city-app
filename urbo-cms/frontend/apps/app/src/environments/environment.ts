import { baseEnvironment } from './environment.base';

export const environment = {
  production: false,
  keycloakUrl: 'http://localhost:8090',
  keycloakRealm: 'swcode',
  keycloakClientId: 'urbo-frontend',
  apiBaseUrl: 'http://localhost:8080',
  searchApiBaseUrl: 'http://localhost:7700',
  searchApiKey: 'masterKey',
  ...baseEnvironment,
};
