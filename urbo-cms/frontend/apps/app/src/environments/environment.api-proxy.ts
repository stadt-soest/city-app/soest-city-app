import { baseEnvironment } from './environment.base';

export const environment = {
  production: false,
  apiBaseUrl: '/api',
  searchApiBaseUrl: '/search-api',
  searchApiKey:
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOm51bGwsImFwaUtleVVpZCI6ImI0ZTAxMWQ0LTcxNzItNGZjOC1hMTBlLWU1NDVjZDUzNjI2NCIsInNlYXJjaFJ1bGVzIjp7IioiOnsiZmlsdGVyIjoidGVuYW50SWRlbnRpZmllciA9IHNvZXN0In19fQ.ALK6HUmfse4mg8Y38bgsB56q7uuD4_0BXXZ7W0EWonE',
  keycloakUrl: 'https://auth.swcode.io/auth',
  keycloakRealm: 'urbo',
  keycloakClientId: 'urbo-cms-app',
  ...baseEnvironment,
};
