export const baseEnvironment = {
  map: {
    styleUrlDark: `https://tiles.openfreemap.org/styles/positron`,
    styleUrlLight: `https://tiles.openfreemap.org/styles/bright`,
    zoom: 14,
  },
  colorPairs: [
    {
      label: 'Grün',
      value: { lightThemeHexColor: '#75A816', darkThemeHexColor: '#94C436' },
    },
    {
      label: 'Mint',
      value: { lightThemeHexColor: '#1ACA7B', darkThemeHexColor: '#31C683' },
    },
    {
      label: 'Blaugrün',
      value: { lightThemeHexColor: '#154F5A', darkThemeHexColor: '#136C79' },
    },
    {
      label: 'Blau',
      value: { lightThemeHexColor: '#0092EB', darkThemeHexColor: '#008ADA' },
    },
    {
      label: 'Violett',
      value: { lightThemeHexColor: '#56479D', darkThemeHexColor: '#6E60B7' },
    },
    {
      label: 'Rot',
      value: { lightThemeHexColor: '#A50000', darkThemeHexColor: '#D34E56' },
    },
    {
      label: 'Orange',
      value: { lightThemeHexColor: '#F3791C', darkThemeHexColor: '#E18B4E' },
    },
    {
      label: 'Grau',
      value: { lightThemeHexColor: '#808080', darkThemeHexColor: '#808080' },
    },
  ],
};
