import { baseEnvironment } from './environment.base';

export const environment = {
  production: true,
  keycloakUrl: 'https://auth.swcode.io/auth',
  keycloakRealm: 'urbo',
  keycloakClientId: 'urbo-cms-app',
  apiBaseUrl: 'https://api.urbo.digital',
  searchApiBaseUrl: 'https://search-urbo.swcode.io',
  searchApiKey:
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOm51bGwsImFwaUtleVVpZCI6ImI0ZTAxMWQ0LTcxNzItNGZjOC1hMTBlLWU1NDVjZDUzNjI2NCIsInNlYXJjaFJ1bGVzIjp7IioiOnsiZmlsdGVyIjoidGVuYW50SWRlbnRpZmllciA9IHNvZXN0In19fQ.ALK6HUmfse4mg8Y38bgsB56q7uuD4_0BXXZ7W0EWonE',
  ...baseEnvironment,
};
