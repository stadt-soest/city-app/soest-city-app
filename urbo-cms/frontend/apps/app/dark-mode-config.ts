export const darkModeMediaQuery = window.matchMedia(
  '(prefers-color-scheme: dark)'
);
