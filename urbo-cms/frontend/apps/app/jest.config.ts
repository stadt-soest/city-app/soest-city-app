export default {
  displayName: 'app',
  preset: '../../jest.preset.js',
  coverageDirectory: '../../coverage/apps/app',
};
