export interface WasteLocation {
  id: string;
  city: string;
  street: string;
  wasteDates: WasteDate[];
  formattedWasteDates?: string;
}

export interface WasteDate {
  date: string;
  wasteType: WasteType;
}

export enum WasteType {
  WASTEPAPER = 'WASTEPAPER',
  YELLOW_BAG = 'YELLOW_BAG',
  BIOWASTE = 'BIOWASTE',
  RESIDUAL_WASTE_14DAILY = 'RESIDUAL_WASTE_14DAILY',
  RESIDUAL_WASTE_4WEEKLY = 'RESIDUAL_WASTE_4WEEKLY',
  DIAPER_WASTE = 'DIAPER_WASTE',
}

export const WasteTypeDescriptions: { [key in WasteType]: string } = {
  [WasteType.WASTEPAPER]: 'Altpapier',
  [WasteType.YELLOW_BAG]: 'Gelber Sack',
  [WasteType.BIOWASTE]: 'Bioabfall',
  [WasteType.RESIDUAL_WASTE_14DAILY]: 'Restmüll (14-täglich)',
  [WasteType.RESIDUAL_WASTE_4WEEKLY]: 'Restmüll (4-wöchentlich)',
  [WasteType.DIAPER_WASTE]: 'Windelabfall',
};
