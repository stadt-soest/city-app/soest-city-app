import { Observable } from 'rxjs';
import { WasteDate, WasteLocation, WasteType } from './models/waste.model';

export abstract class WasteAdapter {
  abstract getWaste(
    page: number,
    rows: number,
    searchTerm?: string
  ): Observable<{ content: WasteLocation[]; totalRecords: number }>;

  abstract getWasteDetailByIdAndStreet(
    street: string
  ): Observable<WasteLocation[]>;

  abstract formatWasteDates(wasteDates: WasteDate[]): string;

  abstract formatDate(dateString: string): string;

  abstract getWasteTypeDescription(wasteType: WasteType): string;
}
