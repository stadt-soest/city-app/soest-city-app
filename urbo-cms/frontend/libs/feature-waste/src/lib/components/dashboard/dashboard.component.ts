import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WasteLocation } from '../../models/waste.model';
import { WasteAdapter } from '../../waste.adapter';

@Component({
  selector: 'urbo-cms-waste-dashboard',
  templateUrl: './dashboard.component.html',
  standalone: false,
})
export class DashboardComponent implements OnInit {
  waste!: WasteLocation[];
  totalRecords = 0;
  rows = 10;
  currentSearchTerm = '';

  cols: any[] = [
    { header: 'Stadt', field: 'city' },
    { header: 'Straße', field: 'street' },
    { header: 'Termine', field: 'formattedWasteDates', type: 'truncated-text' },
  ];

  constructor(
    private readonly wasteService: WasteAdapter,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.loadData(1, this.rows);
  }

  loadData(page: number, rows: number, searchTerm?: string) {
    this.wasteService.getWaste(page, rows, searchTerm).subscribe((data) => {
      this.waste = data.content.map((location) => ({
        ...location,
        formattedWasteDates: this.wasteService.formatWasteDates(
          location.wasteDates
        ),
      }));
      this.totalRecords = data.totalRecords;
    });
  }

  onPage(event: any) {
    const page = event.first / event.rows + 1;
    this.loadData(page, event.rows, this.currentSearchTerm);
  }

  onSearch(event: any) {
    const searchQuery: string = event;
    this.currentSearchTerm = searchQuery;

    if (searchQuery) {
      this.loadData(1, this.rows, searchQuery);
    } else {
      this.loadData(1, this.rows);
    }
  }

  showWasteDetails(clickedRowData: WasteLocation) {
    this.router.navigate([
      `/waste/${clickedRowData.id}/${clickedRowData.street}`,
    ]);
  }
}
