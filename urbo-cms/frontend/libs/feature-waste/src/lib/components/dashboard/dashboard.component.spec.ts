import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DashboardComponent } from './dashboard.component';
import { of } from 'rxjs';
import { WasteType } from '../../models/waste.model';
import { WasteAdapter } from '../../waste.adapter';
import { UITestModule } from '@sw-code/urbo-cms-ui';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let mockWasteService: any;

  beforeEach(async () => {
    mockWasteService = {
      getWaste: jest.fn().mockReturnValue(
        of({
          content: [],
          totalRecords: 0,
        })
      ),
      formatWasteDates: jest.fn((wasteDates) => {
        return wasteDates
          .map(
            (wasteDate: { wasteType: WasteType; date: string }) =>
              `${wasteDate.wasteType} (${wasteDate.date})`
          )
          .join(', ');
      }),
    };

    await TestBed.configureTestingModule({
      imports: [UITestModule.forRoot()],
      declarations: [DashboardComponent],
      providers: [{ provide: WasteAdapter, useValue: mockWasteService }],
    }).compileComponents();

    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should initialize cols and invoke loadData', () => {
      jest.spyOn(component, 'loadData');

      component.ngOnInit();

      const expectedCols = [
        { header: 'Stadt', field: 'city' },
        { header: 'Straße', field: 'street' },
        {
          header: 'Termine',
          field: 'formattedWasteDates',
          type: 'truncated-text',
        },
      ];

      expect(component.cols).toEqual(expectedCols);
      expect(component.loadData).toHaveBeenCalledWith(1, component.rows);
    });
  });

  describe('loadData', () => {
    it('should fetch waste data and format waste dates', () => {
      const mockWaste = [
        {
          city: 'City1',
          street: 'Street1',
          wasteDates: [
            { wasteType: WasteType.WASTEPAPER, date: '2023-11-01' },
            { wasteType: WasteType.YELLOW_BAG, date: '2023-11-02' },
          ],
        },
      ];

      mockWasteService.getWaste.mockReturnValue(
        of({
          content: mockWaste,
          totalRecords: 1,
        })
      );

      component.loadData(0, 1);

      expect(component.waste[0].formattedWasteDates).toBe(
        'WASTEPAPER (2023-11-01), YELLOW_BAG (2023-11-02)'
      );
      expect(component.totalRecords).toBe(1);
    });
  });

  describe('onPage', () => {
    it('should correctly calculate page number and call loadData with appropriate arguments', () => {
      jest.spyOn(component, 'loadData');

      const mockEvent = {
        first: 10,
        rows: 5,
      };

      component.onPage(mockEvent);

      const expectedPage = mockEvent.first / mockEvent.rows + 1;
      expect(component.loadData).toHaveBeenCalledWith(
        expectedPage,
        mockEvent.rows,
        ''
      );
    });
  });
});
