import { Component, OnInit } from '@angular/core';
import { WasteLocation } from '../../models/waste.model';
import { ActivatedRoute } from '@angular/router';
import { WasteAdapter } from '../../waste.adapter';

@Component({
  selector: 'urbo-cms-waste-details',
  styleUrls: ['./waste-details.component.scss'],
  templateUrl: './waste-details.component.html',
  standalone: false,
})
export class WasteDetailsComponent implements OnInit {
  wasteLocation?: WasteLocation;
  totalRecords!: number;

  constructor(
    private readonly route: ActivatedRoute,
    protected wasteService: WasteAdapter
  ) {}

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    const street = this.route.snapshot.paramMap.get('street');
    if (id && street) {
      this.fetchWasteLocationByIdAndStreet(id, street);
    }
  }

  fetchWasteLocationByIdAndStreet(id: string, street: string) {
    this.wasteService
      .getWasteDetailByIdAndStreet(street)
      .subscribe((locations) => {
        this.wasteLocation = locations
          .map((location) => ({
            ...location,
            formattedWasteDates: this.wasteService.formatWasteDates(
              location.wasteDates
            ),
          }))
          .find((location) => location.id === id);
      });
  }
}
