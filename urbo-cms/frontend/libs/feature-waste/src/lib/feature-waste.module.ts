import {
  ModuleWithProviders,
  NgModule,
  inject,
  provideAppInitializer,
} from '@angular/core';
import { take } from 'rxjs';
import { CoreModule } from 'keycloak-angular';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { WasteDetailsComponent } from './components/details/waste-details.component';
import {
  FeatureModule,
  ModuleCategory,
  ModuleInfo,
  ModuleRegistryService,
} from '@sw-code/urbo-cms-core';
import { WasteAdapter } from './waste.adapter';
import { UIModule } from '@sw-code/urbo-cms-ui';

const MODULES = [CoreModule, UIModule];

@NgModule({
  declarations: [DashboardComponent, WasteDetailsComponent],
  imports: [...MODULES],
  exports: [...MODULES],
})
export class FeatureWasteModule {
  readonly featureModule: FeatureModule;
  moduleName = 'Entsorgung';

  constructor(private readonly wasteService: WasteAdapter) {
    this.featureModule = {
      info: new ModuleInfo(
        this.moduleName,
        ModuleCategory.MODULES,
        'pi pi-trash',
        'waste'
      ),
      routes: [
        {
          path: 'waste',
          loadChildren: () =>
            import('./components/routing.module').then((m) => m.RoutingModule),
        },
      ],
      dashboardData: {
        moduleName: this.moduleName,
        entityCount: 0,
      },
    };
    this.wasteService
      .getWaste(0, 10)
      .pipe(take(1))
      .subscribe((data) => {
        this.featureModule.dashboardData!.entityCount = data.totalRecords;
      });
  }

  static forRoot(): ModuleWithProviders<FeatureWasteModule> {
    return {
      ngModule: FeatureWasteModule,
      providers: [
        provideAppInitializer(() => {
          const initializerFn = (
            (registry: ModuleRegistryService, module: FeatureWasteModule) =>
            () => {
              registry.registerFeature(module.featureModule);
            }
          )(inject(ModuleRegistryService), inject(FeatureWasteModule));
          return initializerFn();
        }),
      ],
    };
  }
}
