import { ModuleWithProviders, NgModule } from '@angular/core';
import { UIModule } from '../ui.module';

const MODULES = [UIModule];

@NgModule({
  imports: [...MODULES],
  exports: [...MODULES],
})
export class UITestModule {
  static forRoot(): ModuleWithProviders<UITestModule> {
    return {
      ngModule: UITestModule,
      providers: [],
    };
  }
}
