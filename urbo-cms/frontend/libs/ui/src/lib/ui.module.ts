import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './components/menu/menu.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MenuItemComponent } from './components/menu-item/menu-item.component';
import { InputTextModule } from 'primeng/inputtext';
import { SidebarModule } from 'primeng/sidebar';
import { BadgeModule } from 'primeng/badge';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputSwitchModule } from 'primeng/inputswitch';
import { ButtonModule } from 'primeng/button';
import { TooltipModule } from 'primeng/tooltip';
import { RippleModule } from 'primeng/ripple';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { LayoutComponent } from './components/layout.component';
import { TableModule } from 'primeng/table';
import { CardModule } from 'primeng/card';
import { PaginatorModule } from 'primeng/paginator';
import { ChipModule } from 'primeng/chip';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { MultiSelectModule } from 'primeng/multiselect';
import { AccordionModule } from 'primeng/accordion';
import { GalleriaModule } from 'primeng/galleria';
import { OrderListModule } from 'primeng/orderlist';
import { DialogModule } from 'primeng/dialog';
import { TabViewModule } from 'primeng/tabview';
import { AvatarModule } from 'primeng/avatar';
import { DeleteButtonComponent } from './components/table/subcomponents/delete-button.component';
import { LoadingIndicatorComponent } from './components/loading-indicator.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ProfileMenuComponent } from './components/profile-menu/profile-menu.component';
import { CreateButtonComponent } from './components/table/subcomponents/create-button.component';
import { EditButtonComponent } from './components/table/subcomponents/edit-button.component';
import { ViewButtonComponent } from './components/table/subcomponents/view-button.component';
import { TableComponent } from './components/table/table.component';
import { HighlightButtonComponent } from './components/table/subcomponents/highlight-button.component';
import { TableCellRendererDirective } from './directives/table-cell-renderer.directive';
import { HasPermissionDirective } from './directives/has-permission.directive';
import { CarouselModule } from 'primeng/carousel';
import { TagModule } from 'primeng/tag';
import { FileUploadModule } from 'primeng/fileupload';
import { CheckboxModule } from 'primeng/checkbox';
import { ChipsModule } from 'primeng/chips';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { SkeletonModule } from 'primeng/skeleton';
import { CalendarModule } from 'primeng/calendar';

const MODULES = [
  CommonModule,
  FormsModule,
  RouterModule,
  InputTextModule,
  SidebarModule,
  BadgeModule,
  RadioButtonModule,
  InputSwitchModule,
  ButtonModule,
  TooltipModule,
  RippleModule,
  TableModule,
  CardModule,
  PaginatorModule,
  ChipModule,
  ProgressSpinnerModule,
  ConfirmDialogModule,
  MultiSelectModule,
  AccordionModule,
  GalleriaModule,
  OrderListModule,
  DialogModule,
  TabViewModule,
  InputTextareaModule,
  ReactiveFormsModule,
  CheckboxModule,
  FileUploadModule,
  ChipsModule,
  AvatarModule,
  CardModule,
  ButtonModule,
  TagModule,
  CarouselModule,
  SkeletonModule,
  CalendarModule,
];

const COMPONENTS = [
  ProfileMenuComponent,
  LayoutComponent,
  MenuComponent,
  SidebarComponent,
  MenuItemComponent,
  BreadcrumbComponent,
  TopbarComponent,
  LoadingIndicatorComponent,
  FileUploadComponent,
  NotFoundComponent,
  ProfileMenuComponent,
  CreateButtonComponent,
  DeleteButtonComponent,
  EditButtonComponent,
  ViewButtonComponent,
  TableComponent,
  HighlightButtonComponent,
];

const DIRECTIVES = [TableCellRendererDirective, HasPermissionDirective];

@NgModule({
  declarations: [...COMPONENTS, ...DIRECTIVES],
  imports: [...MODULES],
  exports: [...MODULES, ...COMPONENTS, ...DIRECTIVES],
  providers: [...DIRECTIVES],
})
export class UIModule {}
