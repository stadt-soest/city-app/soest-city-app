import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Permission } from '@sw-code/urbo-cms-core';

@Component({
  selector: 'urbo-cms-ui-highlight-button',
  template: `
    <button
      pButton
      [icon]="icon"
      class="p-button-rounded p-button-info m-1"
      pTooltip="Markieren"
      *urboCmsUiHasPermission="permission"
      (click)="emitHighlight()"
    ></button>
  `,
  standalone: false,
})
export class HighlightButtonComponent implements OnInit, OnChanges {
  @Input() data: any;
  @Input() permission?: Permission;
  @Output() highlight = new EventEmitter<any>();
  icon = '';

  ngOnInit() {
    this.setHighlightIcon();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['data']) {
      this.setHighlightIcon();
    }
  }

  setHighlightIcon() {
    if (this.data.highlight) {
      this.icon = 'pi pi-star-fill';
    } else {
      this.icon = 'pi pi-star';
    }
  }

  emitHighlight() {
    this.data.highlighted = !this.data.highlight;

    this.highlight.emit(this.data);
    this.setHighlightIcon();
  }
}
