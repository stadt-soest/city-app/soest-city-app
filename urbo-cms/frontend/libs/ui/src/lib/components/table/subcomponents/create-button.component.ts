import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Permission } from '@sw-code/urbo-cms-core';

@Component({
  selector: 'urbo-cms-ui-create-button',
  template: `
    <button
      pButton
      icon="pi pi-plus m-1"
      label="Erstellen"
      class="p-button-rounded"
      *urboCmsUiHasPermission="permission"
      (click)="this.create.emit()"
    ></button>
  `,
  standalone: false,
})
export class CreateButtonComponent {
  @Input() permission?: Permission;
  @Output() create = new EventEmitter<void>();
}
