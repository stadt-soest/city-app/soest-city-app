import {
  Component,
  EventEmitter,
  Input,
  Output,
  TemplateRef,
  OnInit,
} from '@angular/core';
import { TableColumn } from './table-column.interface';
import { Permission } from '@sw-code/urbo-cms-core';
import { Table } from 'primeng/table';

@Component({
  selector: 'urbo-cms-ui-table',
  templateUrl: './table.component.html',
  styleUrl: 'table.component.scss',
  standalone: false,
})
export class TableComponent implements OnInit {
  @Input() data: any[] = [];
  @Input() columns: TableColumn[] = [];
  @Input() paginator = true;
  @Input() rows = 10;
  @Input() lazy = true;
  @Input() visibleSearch = true;
  @Input() visibleDateRangeFilter = false;
  @Input() rowsPerPageOptions: number[] = [10, 25, 50];
  @Input() filterOptions: { label: string; value: any }[] = [];
  @Input() totalRecords = 0;
  @Input() selectedFilter: string | null = null;
  @Input() highlightPermission?: Permission;
  @Input() viewPermission?: Permission;
  @Input() editPermission?: Permission;
  @Input() deletePermission?: Permission;
  @Input() createPermission?: Permission;
  @Input() customActionTemplate?: TemplateRef<any>;

  @Output() globalFilter: EventEmitter<string> = new EventEmitter<string>();
  @Output() pageChange: EventEmitter<{ first: number; rows: number }> =
    new EventEmitter<{
      first: number;
      rows: number;
    }>();
  @Output() filterChange = new EventEmitter<string | null>();
  @Output() dateRangeFilterSelected: EventEmitter<Date[]> = new EventEmitter<
    Date[]
  >();
  @Output() createButtonClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() sortChange = new EventEmitter<{
    field: string;
    order: 'asc' | 'desc';
  }>();

  @Output() view: EventEmitter<any> = new EventEmitter<any>();
  @Output() highlight: EventEmitter<any> = new EventEmitter<any>();
  @Output() edit: EventEmitter<any> = new EventEmitter<any>();
  @Output() delete: EventEmitter<any> = new EventEmitter<any>();

  private currentSortField: string | null = null;
  private currentSortOrder: 'asc' | 'desc' = 'asc';
  globalFilterFields: string[] = [];
  rangeDates: Date[] | undefined;
  today = new Date();

  ngOnInit() {
    if (!this.lazy) {
      this.globalFilterFields = this.columns.map((col) => col.field);
    }
  }

  onGlobalFilter(event: Event, dt: Table) {
    const value = (event.target as HTMLInputElement).value.toLowerCase();
    if (this.lazy) {
      this.globalFilter.emit(value);
    } else {
      dt.filterGlobal(value, 'contains');
    }
  }

  onFilterChange() {
    this.filterChange.emit(this.selectedFilter);
  }

  onPageChange(event: { first: number; rows: number }) {
    this.pageChange.emit({ first: event.first, rows: event.rows });
  }

  onSort(sortField: { field: string; order: number }) {
    if (this.currentSortField === sortField.field) {
      this.currentSortOrder = this.currentSortOrder === 'asc' ? 'desc' : 'asc';
    } else {
      this.currentSortOrder = 'asc';
    }

    this.currentSortField = sortField.field;

    this.sortChange.emit({
      field: sortField.field,
      order: this.currentSortOrder,
    });
  }

  onDateRangeChange() {
    if (this.rangeDates?.length === 2) {
      this.dateRangeFilterSelected.emit(this.rangeDates);
    } else {
      this.dateRangeFilterSelected.emit([]);
    }
  }
}
