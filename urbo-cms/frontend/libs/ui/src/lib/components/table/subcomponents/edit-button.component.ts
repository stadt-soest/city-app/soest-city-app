import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Permission } from '@sw-code/urbo-cms-core';

@Component({
  selector: 'urbo-cms-ui-edit-button',
  template: `
    <button
      pButton
      icon="pi pi-pencil"
      class="p-button-rounded p-button-warning m-1"
      pTooltip="Bearbeiten"
      *urboCmsUiHasPermission="permission"
      (click)="this.edit.emit(this.data)"
    ></button>
  `,
  standalone: false,
})
export class EditButtonComponent {
  @Input() data: any;
  @Input() permission?: Permission;
  @Output() edit = new EventEmitter<any>();
}
