import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Permission } from '@sw-code/urbo-cms-core';

@Component({
  selector: 'urbo-cms-ui-view-button',
  template: `
    <button
      pButton
      icon="pi pi-eye"
      class="p-button-rounded p-button-info m-1"
      pTooltip="Ansehen"
      *urboCmsUiHasPermission="permission"
      (click)="this.view.emit(this.data)"
    ></button>
  `,
  standalone: false,
})
export class ViewButtonComponent {
  @Input() data: any;
  @Input() permission?: Permission;
  @Output() view = new EventEmitter<any>();
}
