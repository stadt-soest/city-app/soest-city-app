import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Permission } from '@sw-code/urbo-cms-core';

@Component({
  selector: 'urbo-cms-ui-delete-button',
  template: `
    <button
      pButton
      icon="pi pi-trash"
      class="p-button-rounded p-button-danger m-1"
      pTooltip="Löschen"
      *urboCmsUiHasPermission="permission"
      (click)="this.delete.emit(this.data)"
    ></button>
  `,
  standalone: false,
})
export class DeleteButtonComponent {
  @Input() data: any;
  @Input() permission?: Permission;
  @Output() delete = new EventEmitter<any>();
}
