import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { LayoutService } from '@sw-code/urbo-cms-core';

@Component({
  selector: 'urbo-cms-ui-topbar',
  templateUrl: './topbar.component.html',
  standalone: false,
})
export class TopbarComponent implements OnInit {
  public isLoggedIn = false;
  @ViewChild('menubutton') menuButton!: ElementRef;

  constructor(
    public layoutService: LayoutService,
    private readonly keycloakService: KeycloakService
  ) {}

  async ngOnInit(): Promise<void> {
    this.isLoggedIn = this.keycloakService.isLoggedIn();
  }

  onMenuButtonClick() {
    this.layoutService.onMenuToggle();
  }

  onProfileButtonClick() {
    this.layoutService.showProfileSidebar();
  }
}
