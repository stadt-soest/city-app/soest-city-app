import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'urbo-cms-ui-file-upload',
  templateUrl: './file-upload.component.html',
  standalone: false,
})
export class FileUploadComponent {
  selectedFilePreview: string | null = null;
  selectedFile?: File;
  altDescription = 'Image Preview';
  @Output() fileSelected = new EventEmitter<File>();

  onFileSelected(event: { files: File[] }, fileUpload: { clear: () => void }) {
    const files: File[] = event.files;
    if (files.length > 0) {
      this.selectedFile = files[0];

      const reader = new FileReader();
      reader.onload = () => {
        this.selectedFilePreview = reader.result as string;
      };
      reader.readAsDataURL(this.selectedFile);

      this.fileSelected.emit(this.selectedFile);
      fileUpload.clear();
    }
  }
}
