import { Component } from '@angular/core';

@Component({
  selector: 'urbo-cms-ui-not-found',
  templateUrl: './not-found.component.html',
  standalone: false,
})
export class NotFoundComponent {}
