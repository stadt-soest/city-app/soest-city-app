import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { LayoutService } from '@sw-code/urbo-cms-core';

@Component({
  selector: 'urbo-cms-ui-profile-menu',
  templateUrl: './profile-menu.component.html',
  styleUrls: ['./profile-menu.component.scss'],
  standalone: false,
})
export class ProfileMenuComponent implements OnInit {
  username?: string;

  constructor(
    public layoutService: LayoutService,
    private readonly keycloak: KeycloakService
  ) {}

  ngOnInit() {
    this.keycloak.loadUserProfile().then((profile) => {
      this.username = profile.username;
    });
  }

  get visible(): boolean {
    return this.layoutService.state.profileSidebarVisible;
  }

  set visible(_val: boolean) {
    this.layoutService.state.profileSidebarVisible = _val;
  }

  async logout() {
    await this.keycloak.logout();
  }
}
