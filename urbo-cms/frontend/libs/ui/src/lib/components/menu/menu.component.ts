import { Component, OnInit } from '@angular/core';
import { MenuItem, MenuItemGroup, MenuService } from '@sw-code/urbo-cms-core';

@Component({
  selector: 'urbo-cms-ui-menu',
  templateUrl: './menu.component.html',
  standalone: false,
})
export class MenuComponent implements OnInit {
  model: any[] = [];
  menuItemGroups: MenuItemGroup[];

  constructor(private readonly menuService: MenuService) {
    this.menuItemGroups = this.menuService.getMenuItems();
  }

  ngOnInit() {
    for (const group of this.menuItemGroups) {
      const topLevelMenu: any = {
        label: group.category,
        items: [],
      };

      for (const item of group.menuItems) {
        topLevelMenu.items.push(this.createMenuItem(item));
      }
      this.model.push(topLevelMenu);
    }
  }

  createMenuItem(item: MenuItem): any {
    const menuItem: any = {
      label: item.title,
      icon: item.icon,
      routerLink: [item.baseRoutingPath],
    };

    if (item.subMenuItems && item.subMenuItems.length > 0) {
      menuItem.items = item.subMenuItems.map((subItem) =>
        this.createMenuItem(subItem)
      );
    }

    return menuItem;
  }
}
