import { Component, inject } from '@angular/core';
import { LoadingService } from '@sw-code/urbo-cms-core';

@Component({
  selector: 'urbo-cms-ui-loading-spinner',
  template: `
    <div *ngIf="loading$ | async" class="loading-spinner">
      <p-progressSpinner></p-progressSpinner>
    </div>
  `,
  styles: [
    `
      .loading-spinner {
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        z-index: 9999;
      }
    `,
  ],
  standalone: false,
})
export class LoadingIndicatorComponent {
  private readonly loadingService = inject(LoadingService);
  loading$ = this.loadingService.isLoading();
}
