import {
  DestroyRef,
  Directive,
  Input,
  OnInit,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { Permission, UserAccessService } from '@sw-code/urbo-cms-core';

@Directive({
  selector: '[urboCmsUiHasPermission]',
  standalone: false,
})
export class HasPermissionDirective implements OnInit {
  @Input('urboCmsUiHasPermission') permission?: Permission;

  private hasView = false;

  constructor(
    private readonly templateRef: TemplateRef<unknown>,
    private readonly viewContainer: ViewContainerRef,
    private readonly userAccessService: UserAccessService,
    private readonly destroyRef: DestroyRef
  ) {}

  ngOnInit(): void {
    this.userAccessService.permissions$
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(() => {
        this.updateView();
      });
  }

  private updateView(): void {
    const isAdmin = this.userAccessService.hasAccess([
      Permission.Administration,
    ]);

    if (
      isAdmin ||
      !this.permission ||
      this.userAccessService.hasPermission(this.permission)
    ) {
      if (!this.hasView) {
        this.viewContainer.createEmbeddedView(this.templateRef);
        this.hasView = true;
      }
    } else if (this.hasView) {
      this.viewContainer.clear();
      this.hasView = false;
    }
  }
}
