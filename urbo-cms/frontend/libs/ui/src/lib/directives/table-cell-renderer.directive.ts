import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';
import { ImagePathPipe, ImagePipe } from '@sw-code/urbo-cms-core';

@Directive({
  selector: '[urboCmsUiTableCellRenderer]',
  providers: [ImagePipe, ImagePathPipe],
  standalone: false,
})
export class TableCellRendererDirective implements OnInit {
  @Input() urboCmsUiTableCellRenderer?: string;
  @Input() rowData: any;
  @Input({ required: true }) field = '';
  @Input() truncateLength = 50;

  constructor(
    private readonly el: ElementRef,
    private readonly renderer: Renderer2,
    private readonly imagePipe: ImagePipe
  ) {}

  ngOnInit() {
    this.renderCell();
  }

  private renderCell() {
    const value = this.getNestedValue(this.rowData, this.field);

    switch (this.urboCmsUiTableCellRenderer) {
      case 'datetime':
        this.renderer.setProperty(
          this.el.nativeElement,
          'innerHTML',
          value ? new Date(value).toLocaleString('de-DE') : ''
        );
        break;
      case 'date':
        this.renderer.setProperty(
          this.el.nativeElement,
          'innerHTML',
          value ? new Date(value).toLocaleDateString('de-DE') : ''
        );
        break;
      case 'time':
        this.renderer.setProperty(
          this.el.nativeElement,
          'innerHTML',
          value ? new Date(value).toLocaleTimeString('de-DE') : ''
        );
        break;
      case 'image':
        if (value) {
          const img = this.renderer.createElement('img');
          this.renderer.setAttribute(
            img,
            'src',
            this.imagePipe.transform(value)!
          );
          this.renderer.setAttribute(img, 'alt', 'Image');
          this.renderer.setAttribute(img, 'width', '100');
          this.renderer.appendChild(this.el.nativeElement, img);
        } else {
          this.renderer.setProperty(
            this.el.nativeElement,
            'innerHTML',
            'Kein Bild'
          );
        }
        break;
      default: {
        const safeText = this.escapeHtml(value || '');
        const truncatedText = this.truncateText(safeText);
        this.renderer.setProperty(
          this.el.nativeElement,
          'textContent',
          truncatedText
        );
      }
    }
  }

  private getNestedValue(obj: any, path: string): any {
    return path.split('.').reduce((acc, part) => acc?.[part], obj);
  }

  private escapeHtml(value: string): string {
    const div = document.createElement('div');
    div.innerText = value;
    return div.innerHTML;
  }

  private truncateText(text: string): string {
    if (text.length > this.truncateLength) {
      return text.substring(0, this.truncateLength) + '...';
    }
    return text;
  }
}
