import { Inject, Injectable } from '@angular/core';
import {
  KeycloakEventLegacy,
  KeycloakEventTypeLegacy,
  KeycloakService,
} from 'keycloak-angular';
import { UserAccessService } from './user-access.service';
import { CORE_MODULE_CONFIG, CoreModuleConfig } from '../../core-module.config';

@Injectable()
export class AuthService {
  constructor(
    private readonly keycloak: KeycloakService,
    private readonly userAccessService: UserAccessService,
    @Inject(CORE_MODULE_CONFIG) private readonly config: CoreModuleConfig
  ) {}

  initializeKeycloak(): Promise<any> {
    return this.keycloak
      .init({
        loadUserProfileAtStartUp: true,
        config: {
          url: this.config.keycloakUrl,
          realm: this.config.keycloakRealm,
          clientId: this.config.keycloakClientId,
        },
        initOptions: {
          onLoad: 'login-required',
          silentCheckSsoRedirectUri:
            window.location.origin + 'core/assets/silent-check-sso.html',
          checkLoginIframe: false,
        },
        bearerExcludedUrls: [
          'https://search-urbo.swcode.io',
          'http://localhost:7700',
        ],
      })
      .then(async () => {
        this.initializeKeycloakEventListeners();
        await this.userAccessService.initialize();
      })
      .catch((error) => {
        console.error(
          'Error initializing Keycloak or UserAccessService:',
          error
        );
      });
  }

  private initializeKeycloakEventListeners(): void {
    this.keycloak.keycloakEvents$.subscribe(
      async (event: KeycloakEventLegacy) => {
        switch (event.type) {
          case KeycloakEventTypeLegacy.OnAuthLogout:
            this.userAccessService.clearCache();
            break;

          case KeycloakEventTypeLegacy.OnAuthRefreshSuccess:
          case KeycloakEventTypeLegacy.OnAuthSuccess:
            try {
              this.userAccessService.clearCache();
              await this.userAccessService.initialize();
            } catch (error) {
              console.error(
                'Error initializing permissions after authentication:',
                error
              );
            }
            break;
        }
      }
    );
  }
}
