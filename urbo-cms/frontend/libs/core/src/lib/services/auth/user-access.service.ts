import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { CoreAdapter } from '../../core.adapter';

@Injectable()
export class UserAccessService {
  private initialized = false;
  private readonly permissionsSubject = new BehaviorSubject<string[]>([]);
  public permissions$ = this.permissionsSubject.asObservable();

  constructor(private readonly coreAdapter: CoreAdapter) {}

  public isInitialized(): boolean {
    return this.initialized;
  }

  public async initialize(): Promise<void> {
    if (!this.initialized) {
      try {
        await this.fetchAndCacheUserRolesPermissions();
        this.initialized = true;
      } catch (error) {
        console.error('Failed to initialize UserAccessService:', error);
      }
    }
  }

  public hasPermission(permission: string): boolean {
    return this.permissionsSubject.value.includes(permission);
  }

  public hasAccess(requiredPermissions: string[] = []): boolean {
    return (
      requiredPermissions.length === 0 ||
      requiredPermissions.some((permission) => this.hasPermission(permission))
    );
  }

  private async fetchAndCacheUserRolesPermissions(): Promise<void> {
    try {
      this.permissionsSubject.next(
        await this.coreAdapter.fetchCurrentUserPermissions()
      );
    } catch (error) {
      console.error('Failed to fetch user roles and permissions:', error);
      throw error;
    }
  }

  public clearCache(): void {
    this.permissionsSubject.next([]);
    this.initialized = false;
  }
}
