import { Injectable } from '@angular/core';
import { catchError, finalize, Observable, tap, throwError } from 'rxjs';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import { MessageService } from 'primeng/api';
import { LoadingService } from './loading.service';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
  constructor(
    private readonly messageService: MessageService,
    private readonly loadingService: LoadingService
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const LOADING_DELAY_MS = 200;
    let loadingShown = false;
    const showLoading = setTimeout(() => {
      this.loadingService.startLoading();
      loadingShown = true;
    }, LOADING_DELAY_MS);

    return next.handle(req).pipe(
      tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse && this.shouldShowSuccess(req)) {
          this.handleSuccess(req.method);
        }
      }),
      catchError((error: HttpErrorResponse) => {
        clearTimeout(showLoading);
        if (loadingShown) {
          this.loadingService.stopLoading();
        }
        this.handleError(error);
        return throwError(() => error);
      }),
      finalize(() => {
        clearTimeout(showLoading);
        if (loadingShown) {
          this.loadingService.stopLoading();
        }
      })
    );
  }

  private shouldShowSuccess(req: HttpRequest<any>): boolean {
    const isNonGetRequest = req.method !== 'GET';
    const isNotSearchEndpoint = !req.url.includes('/search');

    return isNonGetRequest && isNotSearchEndpoint;
  }

  private handleSuccess(method: string) {
    let successMsg = 'Erfolgreich';
    switch (method) {
      case 'POST':
        successMsg = 'Erfolgreich erstellt';
        break;
      case 'PUT':
      case 'PATCH':
        successMsg = 'Erfolgreich aktualisiert';
        break;
      case 'DELETE':
        successMsg = 'Erfolgreich gelöscht';
        break;
    }
    this.messageService.add({
      severity: 'success',
      detail: successMsg,
    });
  }

  private handleError(error: HttpErrorResponse) {
    const errorMsg =
      error.error instanceof ErrorEvent
        ? `Fehler: ${error.error.message}`
        : `Fehlercode: ${error.status}, Nachricht: ${error.message}`;

    this.messageService.add({
      severity: 'error',
      summary: 'Error',
      detail: errorMsg,
    });
  }
}
