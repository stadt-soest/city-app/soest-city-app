import { Injectable } from '@angular/core';
import { FeatureModule } from '../../interfaces/feature-module.interface';
import { Routes } from '@angular/router';
import { catchError, map, Observable, of } from 'rxjs';
import { UserAccessService } from '../auth/user-access.service';
import { Permission } from '../../models/permission.model';
import { ModuleInfo } from '../../models/menu.model';

@Injectable()
export class ModuleRegistryService {
  constructor(private readonly userAccessService: UserAccessService) {}

  private readonly features: FeatureModule[] = [];

  registerFeature(featureModule: FeatureModule) {
    this.features.push(featureModule);
  }

  getModuleInfos(): ModuleInfo[] {
    return this.features.map((feature) => feature.info);
  }

  getFeatureRoutes(): Routes {
    return this.features.reduce((acc: Routes, feature: FeatureModule) => {
      return acc.concat(feature.routes);
    }, []);
  }

  getFeedbackData(): Observable<
    { route: string; answer: string; createdAt: string }[]
  > {
    const feedbackModule = this.features.find(
      (feature) => feature.info.name === 'Feedback'
    );
    if (
      feedbackModule?.dashboardData?.detailsData &&
      this.userAccessService.hasPermission(
        Permission.AccessibilitySubmissionRead
      )
    ) {
      return feedbackModule.dashboardData.detailsData(0).pipe(
        map((data) => {
          if (Array.isArray(data) && data.length > 0) {
            return data.map((item) => {
              return {
                route: item.route,
                answer: item.answer,
                createdAt: item.createdAt.toLocaleString(),
              };
            });
          }
          return [];
        }),
        catchError((error) => {
          console.error('Error fetching feedback data:', error);
          return of([]);
        })
      );
    } else {
      return of([]);
    }
  }

  getEntityCounts(): { moduleName: string; entityCount: number }[] {
    return this.features
      .map((feature) => {
        if (feature.dashboardData) {
          return {
            moduleName: feature.dashboardData.moduleName,
            entityCount: feature.dashboardData.entityCount ?? 0,
          };
        } else {
          return null;
        }
      })
      .filter((data) => data !== null) as {
      moduleName: string;
      entityCount: number;
    }[];
  }

  getModuleRoute(moduleName: string): string | undefined {
    const module = this.features.find(
      (feature) => feature.dashboardData?.moduleName === moduleName
    );
    if (module && module.routes.length > 0) {
      return module.routes[0].path;
    }
    return undefined;
  }
}
