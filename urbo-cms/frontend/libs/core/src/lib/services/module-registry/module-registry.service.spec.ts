import { ModuleRegistryService } from './module-registry.service';
import { FeatureModule } from '../../interfaces/feature-module.interface';
import { ModuleCategory, ModuleInfo } from '../../models/menu.model';
import { UserAccessService } from '../auth/user-access.service';

describe('ModuleRegistryService', () => {
  let service: ModuleRegistryService;
  const featureModule: FeatureModule = {
    info: new ModuleInfo('Test', ModuleCategory.MODULES, 'icon', 'path'),
    routes: [{ path: 'test', component: {} as any }],
  };
  let userAccessService: jest.Mocked<UserAccessService>;

  beforeEach(() => {
    userAccessService = {
      hasPermission: jest.fn().mockReturnValue(true),
    } as any;

    service = new ModuleRegistryService(userAccessService);
  });

  it('should add the feature to the features list', () => {
    service.registerFeature(featureModule);
    expect(service['features'].length).toBe(1);
  });

  it('should return the info of the registered modules', () => {
    service.registerFeature(featureModule);
    const infos = service.getModuleInfos();

    expect(infos.length).toBe(1);
    expect(infos[0].name).toBe('Test');
  });

  it('should return the routes of the registered modules', () => {
    service.registerFeature(featureModule);
    const routes = service.getFeatureRoutes();

    expect(routes.length).toBe(1);
    expect(routes[0].path).toBe('test');
  });
});
