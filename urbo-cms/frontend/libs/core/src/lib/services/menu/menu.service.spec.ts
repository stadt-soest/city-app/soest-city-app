import { MenuService } from './menu.service';
import { ModuleRegistryService } from '../module-registry/module-registry.service';
import { UserAccessService } from '../auth/user-access.service';
import { MenuItem, ModuleCategory, ModuleInfo } from '../../models/menu.model';
import { MenuChangeEvent } from '../../models/menu-change-event';
import { Permission } from '../../models/permission.model';

describe('MenuService', () => {
  let service: MenuService;
  let mockModuleRegistry: jest.Mocked<ModuleRegistryService>;
  let userAccessService: jest.Mocked<UserAccessService>;

  beforeEach(() => {
    mockModuleRegistry = {
      getModuleInfos: jest.fn(),
      registerFeature: jest.fn(),
      getFeatureRoutes: jest.fn(),
    } as any;

    userAccessService = {
      hasAccess: jest.fn().mockReturnValue(true),
      isInitialized: jest.fn().mockReturnValue(true),
    } as any;

    service = new MenuService(mockModuleRegistry, userAccessService);
  });

  it('should get grouped menu items based on categories', () => {
    mockModuleRegistry.getModuleInfos.mockReturnValue([
      new ModuleInfo(
        'Module1',
        ModuleCategory.MODULES,
        'icon1',
        '/path1',
        [],
        ['Test']
      ),
      new ModuleInfo(
        'Module2',
        ModuleCategory.MODULES,
        'icon2',
        '/path2',
        [],
        ['Test']
      ),
    ]);

    userAccessService.hasAccess.mockReturnValue(true);

    const result = service.getMenuItems();

    expect(result.length).toBe(1);
    expect(result[0].menuItems.length).toBe(2);
    expect(result[0].menuItems[0].title).toBe('Module1');
    expect(result[0].menuItems[1].title).toBe('Module2');
  });

  it('should return all menu items if the user has admin permissions', () => {
    userAccessService.hasAccess.mockImplementation((permissions = []) =>
      permissions.includes(Permission.Administration)
    );

    mockModuleRegistry.getModuleInfos.mockReturnValue([
      new ModuleInfo('Module1', ModuleCategory.MODULES, 'icon1', '/path1', [
        new MenuItem('SubMenu1', 'iconSub', '/subpath1', [], ['PERM1']),
      ]),
      new ModuleInfo('Module2', ModuleCategory.MODULES, 'icon2', '/path2', [
        new MenuItem('SubMenu2', 'iconSub', '/subpath2', [], ['PERM2']),
      ]),
    ]);

    const result = service.getMenuItems();

    expect(result.length).toBe(1);
    expect(result[0].menuItems.length).toBe(2);
    expect(result[0].menuItems[0].title).toBe('Module1');
    expect(result[0].menuItems[0].subMenuItems!.length).toBe(1);
    expect(result[0].menuItems[1].title).toBe('Module2');
    expect(result[0].menuItems[1].subMenuItems!.length).toBe(1);
  });

  it('should transform subMenuItems if present', () => {
    mockModuleRegistry.getModuleInfos.mockReturnValue([
      new ModuleInfo('Module1', ModuleCategory.MODULES, 'icon1', '/path1', [
        new MenuItem('SubMenu1', 'iconSub', '/subpath1'),
      ]),
    ]);

    userAccessService.hasAccess.mockReturnValue(true);

    const result = service.getMenuItems();

    expect(result[0].menuItems[0].subMenuItems!.length).toBe(1);
    expect(result[0].menuItems[0].subMenuItems![0].title).toBe('SubMenu1');
  });

  it('should emit menu state change events', (done) => {
    const sampleEvent: MenuChangeEvent = { key: 'testKey', routeEvent: true };

    service.menuSource$.subscribe((event) => {
      expect(event).toBe(sampleEvent);
      done();
    });

    service.onMenuStateChange(sampleEvent);
  });

  it('should emit reset events', (done) => {
    service.resetSource$.subscribe(() => {
      done();
    });

    service.reset();
  });
});
