import { ModuleRegistryService } from '../module-registry/module-registry.service';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { MenuChangeEvent } from '../../models/menu-change-event';
import { UserAccessService } from '../auth/user-access.service';
import { Permission } from '../../models/permission.model';
import { MenuItem, MenuItemGroup, ModuleInfo } from '../../models/menu.model';

@Injectable()
export class MenuService {
  private readonly menuSource = new Subject<MenuChangeEvent>();
  private readonly resetSource = new Subject<void>();

  public menuSource$ = this.menuSource.asObservable();
  public resetSource$ = this.resetSource.asObservable();

  constructor(
    private readonly moduleRegistry: ModuleRegistryService,
    private readonly userAccessService: UserAccessService
  ) {}

  public getMenuItems(): MenuItemGroup[] {
    if (!this.userAccessService.isInitialized()) {
      console.error(
        'UserAccessService is not initialized. Returning an empty menu.'
      );
      return [];
    }

    const isAdmin = this.userAccessService.hasAccess([
      Permission.Administration,
    ]);

    const moduleInfos = this.moduleRegistry.getModuleInfos();
    return moduleInfos.reduce((groups, moduleInfo) => {
      this.processModuleInfo(groups, moduleInfo, isAdmin);
      return groups;
    }, [] as MenuItemGroup[]);
  }

  private processModuleInfo(
    groups: MenuItemGroup[],
    moduleInfo: ModuleInfo,
    isAdmin: boolean
  ): void {
    const effectivePermissions = this.getEffectivePermissions(
      moduleInfo.permissions
    );
    const accessibleSubMenuItems = this.filterAccessibleSubMenuItems(
      moduleInfo.subMenuItems,
      effectivePermissions,
      isAdmin
    );

    if (
      this.shouldAddModuleInfoToMenu(
        accessibleSubMenuItems,
        effectivePermissions,
        isAdmin
      )
    ) {
      this.addModuleInfoToMenu(
        groups,
        moduleInfo,
        accessibleSubMenuItems,
        effectivePermissions,
        isAdmin
      );
    }
  }

  private getEffectivePermissions(permissions: string[]): string[] {
    return permissions.length > 0 ? permissions : [];
  }

  private shouldAddModuleInfoToMenu(
    accessibleSubMenuItems: MenuItem[],
    effectivePermissions: string[],
    isAdmin: boolean
  ): boolean {
    return (
      isAdmin ||
      accessibleSubMenuItems.length > 0 ||
      this.userAccessService.hasAccess(effectivePermissions)
    );
  }

  private addModuleInfoToMenu(
    groups: MenuItemGroup[],
    moduleInfo: ModuleInfo,
    accessibleSubMenuItems: MenuItem[],
    effectivePermissions: string[],
    isAdmin: boolean
  ): void {
    const menuItem = new MenuItem(
      moduleInfo.name,
      moduleInfo.icon,
      moduleInfo.baseRoutingPath,
      accessibleSubMenuItems,
      isAdmin ? [] : effectivePermissions
    );

    let menuItemGroup = groups.find(
      (group) => group.category === moduleInfo.category
    );
    if (!menuItemGroup) {
      menuItemGroup = new MenuItemGroup(moduleInfo.category, []);
      groups.push(menuItemGroup);
    }

    if (
      accessibleSubMenuItems.length > 0 ||
      effectivePermissions.length > 0 ||
      isAdmin
    ) {
      menuItemGroup.menuItems.push(menuItem);
    }
  }

  private filterAccessibleSubMenuItems(
    subMenuItems?: MenuItem[],
    parentPermissions: string[] = [],
    isAdmin = false
  ): MenuItem[] {
    if (!subMenuItems) {
      return [];
    }

    return subMenuItems.reduce((filtered, subMenuItem) => {
      this.processSubMenuItem(
        filtered,
        subMenuItem,
        parentPermissions,
        isAdmin
      );
      return filtered;
    }, [] as MenuItem[]);
  }

  private processSubMenuItem(
    filtered: MenuItem[],
    subMenuItem: MenuItem,
    parentPermissions: string[],
    isAdmin: boolean
  ): void {
    const effectivePermissions = this.getEffectivePermissionsForSubMenuItem(
      subMenuItem,
      parentPermissions
    );
    const accessibleNestedSubMenuItems = this.filterAccessibleSubMenuItems(
      subMenuItem.subMenuItems,
      effectivePermissions,
      isAdmin
    );

    if (
      this.shouldAddSubMenuItemToFilteredList(
        effectivePermissions,
        accessibleNestedSubMenuItems,
        isAdmin
      )
    ) {
      filtered.push(
        this.createFilteredMenuItem(
          subMenuItem,
          accessibleNestedSubMenuItems,
          effectivePermissions
        )
      );
    }
  }

  private getEffectivePermissionsForSubMenuItem(
    subMenuItem: MenuItem,
    parentPermissions: string[]
  ): string[] {
    return subMenuItem.permissions.length > 0
      ? subMenuItem.permissions
      : parentPermissions;
  }

  private shouldAddSubMenuItemToFilteredList(
    effectivePermissions: string[],
    accessibleNestedSubMenuItems: MenuItem[],
    isAdmin: boolean
  ): boolean {
    return (
      isAdmin ||
      accessibleNestedSubMenuItems.length > 0 ||
      this.userAccessService.hasAccess(effectivePermissions)
    );
  }

  private createFilteredMenuItem(
    subMenuItem: MenuItem,
    accessibleNestedSubMenuItems: MenuItem[],
    effectivePermissions: string[]
  ): MenuItem {
    return new MenuItem(
      subMenuItem.title,
      subMenuItem.icon,
      subMenuItem.baseRoutingPath,
      accessibleNestedSubMenuItems,
      effectivePermissions
    );
  }

  public onMenuStateChange(event: MenuChangeEvent): void {
    this.menuSource.next(event);
  }

  public reset(): void {
    this.resetSource.next();
  }
}
