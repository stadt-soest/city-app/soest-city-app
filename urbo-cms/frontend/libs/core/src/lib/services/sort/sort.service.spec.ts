import { TestBed } from '@angular/core/testing';
import { CurrentSort } from '../../interfaces/current-sort.interface';
import { SortService } from './sort.service';

describe('SortService', () => {
  let service: SortService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SortService],
    });
    service = TestBed.inject(SortService);
  });

  describe('getColumns', () => {
    it('should mark fields as sortable based on an array of sortable fields', () => {
      const baseCols = [
        { header: 'Column 1', field: 'field1' },
        { header: 'Column 2', field: 'field2' },
      ];
      const sortableFields = ['field1'];

      const result = service.getColumns(baseCols, sortableFields);

      expect(result).toEqual([
        { header: 'Column 1', field: 'field1', sortable: true },
        { header: 'Column 2', field: 'field2', sortable: false },
      ]);
    });

    it('should mark fields as sortable based on a function', () => {
      const baseCols = [
        { header: 'Column 1', field: 'field1' },
        { header: 'Column 2', field: 'field2' },
      ];
      const sortableFields = (field: string) => field === 'field2';

      const result = service.getColumns(baseCols, sortableFields);

      expect(result).toEqual([
        { header: 'Column 1', field: 'field1', sortable: false },
        { header: 'Column 2', field: 'field2', sortable: true },
      ]);
    });
  });

  describe('handleSort', () => {
    it('should return the sorting event object as is', () => {
      const event: CurrentSort = { field: 'field1', order: 'asc' };

      const result = service.handleSort(event);

      expect(result).toEqual(event);
    });

    it('should handle a different sorting event object', () => {
      const event: CurrentSort = { field: 'field2', order: 'desc' };

      const result = service.handleSort(event);

      expect(result).toEqual(event);
    });
  });
});
