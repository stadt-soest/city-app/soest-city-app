import { Injectable } from '@angular/core';
import { CurrentSort } from '../../interfaces/current-sort.interface';

@Injectable()
export class SortService {
  getColumns(
    baseCols: any[],
    sortableFields: readonly string[] | ((field: string) => boolean)
  ): any[] {
    return baseCols.map((col) => ({
      ...col,
      sortable:
        typeof sortableFields === 'function'
          ? sortableFields(col.field)
          : sortableFields.includes(col.field),
    }));
  }

  handleSort(sortData: CurrentSort): CurrentSort {
    if (!sortData?.field) {
      throw new Error('Invalid sort event: field is required');
    }
    if (!sortData?.order || !['asc', 'desc'].includes(sortData.order)) {
      throw new Error('Invalid sort event: order must be "asc" or "desc"');
    }
    return { field: sortData.field, order: sortData.order };
  }
}
