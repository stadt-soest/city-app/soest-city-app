import { ModuleWithProviders, NgModule } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from '../core.module';

const MODULES = [CoreModule, HttpClientTestingModule, RouterTestingModule];

@NgModule({
  imports: [...MODULES],
  exports: [...MODULES],
})
export class CoreTestModule {
  static forRoot(): ModuleWithProviders<CoreTestModule> {
    return {
      ngModule: CoreTestModule,
      providers: CoreModule.forRoot({
        apiBaseUrl: '',
        keycloakUrl: '',
        keycloakClientId: '',
        keycloakRealm: '',
      }).providers,
    };
  }
}
