export abstract class CoreAdapter {
  abstract fetchCurrentUserPermissions(): Promise<string[]>;
}
