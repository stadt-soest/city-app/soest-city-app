export class ModuleInfo {
  name: string;
  category: ModuleCategory;
  icon: string;
  baseRoutingPath: string;
  subMenuItems?: MenuItem[];
  permissions: string[] = [];

  constructor(
    name: string,
    category: ModuleCategory,
    icon: string,
    baseRoutingPath: string,
    subMenuItems?: MenuItem[],
    permissions?: string[]
  ) {
    this.name = name;
    this.category = category;
    this.icon = icon;
    this.baseRoutingPath = baseRoutingPath;
    this.subMenuItems = subMenuItems;
    this.permissions = permissions ?? [];
  }
}

export enum ModuleCategory {
  MODULES = 'MODULES',
}

export class MenuItemGroup {
  category: ModuleCategory;
  menuItems: MenuItem[];

  constructor(category: ModuleCategory, menuItems: MenuItem[]) {
    this.category = category;
    this.menuItems = menuItems;
  }
}

export class MenuItem {
  title: string;
  icon: string;
  baseRoutingPath: string;
  subMenuItems?: MenuItem[];
  permissions: string[] = [];

  constructor(
    title: string,
    icon: string,
    baseRoutingPath: string,
    subMenuItems?: MenuItem[],
    permissions?: string[]
  ) {
    this.title = title;
    this.icon = icon;
    this.baseRoutingPath = baseRoutingPath;
    this.subMenuItems = subMenuItems;
    this.permissions = permissions ?? [];
  }
}
