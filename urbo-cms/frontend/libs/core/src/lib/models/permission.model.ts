export type Permission =
  | 'CITIZEN_SERVICE_WRITE'
  | 'CITIZEN_SERVICE_DELETE'
  | 'CATEGORY_READ'
  | 'CATEGORY_WRITE'
  | 'CATEGORY_DELETE'
  | 'NEWS_WRITE'
  | 'NEWS_DELETE'
  | 'NEWS_HIGHLIGHT'
  | 'PARKING_AREA_WRITE'
  | 'FEEDBACK_SUBMISSION_READ'
  | 'FEEDBACK_SUBMISSION_WRITE'
  | 'ACCESSIBILITY_SUBMISSION_READ'
  | 'ADMINISTRATION';

export const Permission = {
  CitizenServiceWrite: 'CITIZEN_SERVICE_WRITE' as Permission,
  CitizenServiceDelete: 'CITIZEN_SERVICE_DELETE' as Permission,
  CategoryRead: 'CATEGORY_READ' as Permission,
  CategoryWrite: 'CATEGORY_WRITE' as Permission,
  CategoryDelete: 'CATEGORY_DELETE' as Permission,
  NewsWrite: 'NEWS_WRITE' as Permission,
  NewsDelete: 'NEWS_DELETE' as Permission,
  NewsHighlight: 'NEWS_HIGHLIGHT' as Permission,
  ParkingAreaWrite: 'PARKING_AREA_WRITE' as Permission,
  FeedbackSubmissionRead: 'FEEDBACK_SUBMISSION_READ' as Permission,
  FeedbackSubmissionWrite: 'FEEDBACK_SUBMISSION_WRITE' as Permission,
  AccessibilitySubmissionRead: 'ACCESSIBILITY_SUBMISSION_READ' as Permission,
  Administration: 'ADMINISTRATION' as Permission,
};
