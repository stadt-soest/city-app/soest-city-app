export class I18nString {
  de?: string;
  en?: string;

  constructor(de?: string, en?: string) {
    this.de = de;
    this.en = en;
  }
}
