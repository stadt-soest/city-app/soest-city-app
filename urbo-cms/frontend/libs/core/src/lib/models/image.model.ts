export interface Image {
  originalSource: string;
  copyright?: string;
  description?: string;
  thumbnail?: ImageFile;
  highRes?: ImageFile;
}

export interface ImageFile {
  filename?: string;
  width?: number | null;
  height?: number | null;
}
