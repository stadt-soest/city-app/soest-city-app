export abstract class AbstractMapper<TDto, TModel> {
  abstract mapToModel(dto: TDto): TModel;
  abstract mapToDto(model: TModel): TDto;

  mapToModelArray(dtos: TDto[]): TModel[] {
    return dtos.map((dto) => this.mapToModel(dto));
  }
}
