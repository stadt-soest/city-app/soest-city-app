export interface CurrentSort {
  field: string;
  order: 'asc' | 'desc';
}
