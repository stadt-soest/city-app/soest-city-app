import { Routes } from '@angular/router';
import { Observable } from 'rxjs';
import { ModuleInfo } from '../models/menu.model';

export interface FeatureModule {
  info: ModuleInfo;
  routes: Routes;
  dashboardData?: {
    moduleName: string;
    entityCount?: number;
    detailsData?: (page: number) => Observable<any>;
  };
}
