import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { DynamicAuthorizationGuard } from './dynamic-authorization.guard';
import { KeycloakService } from 'keycloak-angular';
import { ModuleRegistryService } from '../../services/module-registry/module-registry.service';
import { MenuItem, ModuleCategory, ModuleInfo } from '../../models/menu.model';
import { CoreAdapter } from '../../core.adapter';
import { Permission } from '../../models/permission.model';

describe('DynamicAuthorizationGuard', () => {
  let guard: DynamicAuthorizationGuard;
  let mockRouter: jest.Mocked<Router>;
  let mockKeycloakService: jest.Mocked<KeycloakService>;
  let mockModuleRegistryService: jest.Mocked<ModuleRegistryService>;
  let mockAdapterClient: jest.Mocked<CoreAdapter>;

  beforeEach(() => {
    setupTest();
  });

  it('should navigate to not-found if moduleName is not found in route', async () => {
    const canActivate = await guard.canActivate({} as any, { url: '/' } as any);
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/not-found']);
    expect(canActivate).toBe(false);
  });

  it('should navigate to not-found if moduleInfo is not found', async () => {
    mockModuleRegistryService.getModuleInfos.mockReturnValue([]);
    const canActivate = await guard.canActivate(
      {} as any,
      { url: '/non-existing' } as any
    );
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/not-found']);
    expect(canActivate).toBe(false);
  });

  it('should redirect to login if user is not authenticated', async () => {
    mockModuleRegistryService.getModuleInfos.mockReturnValue([
      createModuleInfo('news'),
    ]);
    mockKeycloakService.isLoggedIn.mockReturnValue(false);

    const canActivate = await guard.canActivate(
      {} as any,
      { url: '/news' } as any
    );

    expect(mockKeycloakService.login).toHaveBeenCalled();
    expect(canActivate).toBe(false);
  });

  it('should navigate to not-found if user lacks required permissions', async () => {
    mockModuleRegistryService.getModuleInfos.mockReturnValue([
      createModuleInfo('admin', ['ADMIN_READ']),
    ]);
    mockKeycloakService.isLoggedIn.mockReturnValue(true);
    mockAdapterClient.fetchCurrentUserPermissions.mockResolvedValue([
      'USER_READ' as Permission,
    ]);

    const canActivate = await guard.canActivate(
      {} as any,
      { url: '/admin' } as any
    );

    expect(mockRouter.navigate).toHaveBeenCalledWith(['/not-found']);
    expect(canActivate).toBe(false);
  });

  it('should allow access if user has required permissions', async () => {
    mockModuleRegistryService.getModuleInfos.mockReturnValue([
      createModuleInfo('admin', ['ADMIN_READ']),
    ]);
    mockKeycloakService.isLoggedIn.mockReturnValue(true);
    mockAdapterClient.fetchCurrentUserPermissions.mockResolvedValue([
      'ADMIN_READ',
    ] as any);

    const canActivate = await guard.canActivate(
      {} as any,
      { url: '/admin' } as any
    );

    expect(canActivate).toBe(true);
  });

  it('should allow access if there are accessible subMenuItems', async () => {
    mockModuleRegistryService.getModuleInfos.mockReturnValue([
      createModuleInfo(
        'admin',
        [],
        [
          new MenuItem(
            'Dashboard',
            'icon',
            'admin/dashboard',
            [],
            ['DASHBOARD_READ']
          ),
        ]
      ),
    ]);
    mockKeycloakService.isLoggedIn.mockReturnValue(true);
    mockAdapterClient.fetchCurrentUserPermissions.mockResolvedValue([
      'DASHBOARD_READ',
    ] as any);

    const canActivate = await guard.canActivate(
      {} as any,
      { url: '/admin' } as any
    );

    expect(canActivate).toBe(true);
  });

  it('should navigate to not-found if no permissions or accessible subMenuItems', async () => {
    mockModuleRegistryService.getModuleInfos.mockReturnValue([
      createModuleInfo(
        'admin',
        ['ADMIN_READ'],
        [
          new MenuItem(
            'Dashboard',
            'icon',
            'admin/dashboard',
            [],
            ['DASHBOARD_READ']
          ),
        ]
      ),
    ]);
    mockKeycloakService.isLoggedIn.mockReturnValue(true);
    mockAdapterClient.fetchCurrentUserPermissions.mockResolvedValue([
      'USER_READ',
    ] as any);

    const canActivate = await guard.canActivate(
      {} as any,
      { url: '/admin' } as any
    );

    expect(mockRouter.navigate).toHaveBeenCalledWith(['/not-found']);
    expect(canActivate).toBe(false);
  });

  it('should allow access if nested subMenuItems are accessible', async () => {
    mockModuleRegistryService.getModuleInfos.mockReturnValue([
      createModuleInfo(
        'admin',
        [],
        [
          new MenuItem('Settings', 'icon', 'admin/settings', [
            new MenuItem(
              'Advanced',
              'icon',
              'admin/settings/advanced',
              [],
              ['SETTINGS_ADVANCED']
            ),
          ]),
        ]
      ),
    ]);
    mockKeycloakService.isLoggedIn.mockReturnValue(true);
    mockAdapterClient.fetchCurrentUserPermissions.mockResolvedValue([
      'SETTINGS_ADVANCED',
    ] as any);

    const canActivate = await guard.canActivate(
      {} as any,
      { url: '/admin' } as any
    );

    expect(canActivate).toBe(true);
  });

  it('should allow access if deeply nested sub-menu items are accessible', async () => {
    mockModuleRegistryService.getModuleInfos.mockReturnValue([
      createModuleInfo(
        'admin',
        [],
        [
          new MenuItem('Settings', 'icon', 'admin/settings', [
            new MenuItem('Advanced', 'icon', 'admin/settings/advanced', [
              new MenuItem(
                'Deep Settings',
                'icon',
                'admin/settings/advanced/deep',
                [],
                ['DEEP_SETTINGS']
              ),
            ]),
          ]),
        ]
      ),
    ]);
    mockKeycloakService.isLoggedIn.mockReturnValue(true);
    mockAdapterClient.fetchCurrentUserPermissions.mockResolvedValue([
      'DEEP_SETTINGS',
    ] as any);

    const canActivate = await guard.canActivate(
      {} as any,
      { url: '/admin' } as any
    );

    expect(canActivate).toBe(true);
  });

  it('should allow access if any of multiple sub-menu items are accessible', async () => {
    mockModuleRegistryService.getModuleInfos.mockReturnValue([
      createModuleInfo(
        'admin',
        [],
        [
          new MenuItem(
            'Dashboard',
            'icon',
            'admin/dashboard',
            [],
            ['DASHBOARD_READ']
          ),
          new MenuItem(
            'Settings',
            'icon',
            'admin/settings',
            [],
            ['SETTINGS_READ']
          ),
        ]
      ),
    ]);
    mockKeycloakService.isLoggedIn.mockReturnValue(true);
    mockAdapterClient.fetchCurrentUserPermissions.mockResolvedValue([
      'SETTINGS_READ',
    ] as any);

    const canActivate = await guard.canActivate(
      {} as any,
      { url: '/admin' } as any
    );

    expect(canActivate).toBe(true);
  });

  it('should allow access to all routes if user has ADMINISTRATION permission', async () => {
    mockModuleRegistryService.getModuleInfos.mockReturnValue([
      createModuleInfo('admin', ['ADMIN_READ']),
      createModuleInfo('news', ['NEWS_READ']),
    ]);

    mockKeycloakService.isLoggedIn.mockReturnValue(true);
    mockAdapterClient.fetchCurrentUserPermissions.mockResolvedValue([
      Permission.Administration,
    ]);

    const canActivateAdmin = await guard.canActivate(
      {} as any,
      { url: '/admin' } as any
    );
    const canActivateNews = await guard.canActivate(
      {} as any,
      { url: '/news' } as any
    );

    expect(canActivateAdmin).toBe(true);
    expect(canActivateNews).toBe(true);
    expect(mockRouter.navigate).not.toHaveBeenCalled();
  });

  function setupTest() {
    mockRouter = {
      navigate: jest.fn(),
    } as any;

    mockKeycloakService = {
      isLoggedIn: jest.fn(),
      login: jest.fn(),
    } as any;

    mockModuleRegistryService = {
      getModuleInfos: jest.fn(),
    } as any;

    mockAdapterClient = {
      fetchCurrentUserPermissions: jest.fn(),
    } as any;

    TestBed.configureTestingModule({
      providers: [
        DynamicAuthorizationGuard,
        { provide: Router, useValue: mockRouter },
        { provide: KeycloakService, useValue: mockKeycloakService },
        { provide: ModuleRegistryService, useValue: mockModuleRegistryService },
        { provide: CoreAdapter, useValue: mockAdapterClient },
      ],
    });

    guard = TestBed.inject(DynamicAuthorizationGuard);
  }

  function createModuleInfo(
    baseRoutingPath: string,
    permissions: string[] = [],
    subMenuItems: MenuItem[] = []
  ): ModuleInfo {
    return {
      baseRoutingPath,
      name: `${baseRoutingPath.charAt(0).toUpperCase()}${baseRoutingPath.slice(
        1
      )}`,
      category: ModuleCategory.MODULES,
      icon: `pi pi-${baseRoutingPath}`,
      permissions,
      subMenuItems,
    };
  }
});
