import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import { ModuleRegistryService } from '../../services/module-registry/module-registry.service';
import { MenuItem } from 'primeng/api';
import { Permission } from '../../models/permission.model';
import { CoreAdapter } from '../../core.adapter';
import { ModuleInfo } from '../../models/menu.model';

@Injectable({
  providedIn: 'root',
})
export class DynamicAuthorizationGuard implements CanActivate {
  constructor(
    private readonly router: Router,
    private readonly keycloak: KeycloakService,
    private readonly moduleRegistry: ModuleRegistryService,
    private readonly coreAdapter: CoreAdapter
  ) {}

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    const baseRoutingPath = this.getBaseRoutingPathFromRoute(state.url);

    if (!baseRoutingPath) {
      return this.navigateToNotFound();
    }

    const moduleInfo = this.getModuleInfo(baseRoutingPath);

    if (!moduleInfo) {
      return this.navigateToNotFound();
    }

    if (!(await this.isAuthenticated())) {
      return false;
    }

    const userPermissions = await this.getUserPermissions();

    if (this.hasAdminAccess(userPermissions)) {
      return true;
    }

    if (this.isAuthorized(userPermissions, moduleInfo)) {
      return true;
    } else {
      return this.navigateToNotFound();
    }
  }

  private hasAdminAccess(userPermissions: string[]): boolean {
    return userPermissions.includes(Permission.Administration);
  }

  private getBaseRoutingPathFromRoute(url: string): string | null {
    const segments = url.split('/');
    return segments.length > 1 ? segments[1] : null;
  }

  private async isAuthenticated(): Promise<boolean> {
    if (!this.keycloak.isLoggedIn()) {
      await this.keycloak.login();
      return false;
    }
    return true;
  }

  private async getUserPermissions(): Promise<string[]> {
    return (await this.coreAdapter.fetchCurrentUserPermissions()) || [];
  }

  private getModuleInfo(baseRoutingPath: string): ModuleInfo | undefined {
    return this.moduleRegistry
      .getModuleInfos()
      .find((info) => info.baseRoutingPath === baseRoutingPath);
  }

  private isAuthorized(
    userPermissions: string[],
    moduleInfo: ModuleInfo
  ): boolean {
    const modulePermissions = this.getEffectivePermissions(
      moduleInfo.permissions
    );

    if (modulePermissions.length === 0) {
      return true;
    }

    const accessibleSubMenuItems = this.filterAccessibleSubMenuItems(
      moduleInfo.subMenuItems || [],
      modulePermissions,
      userPermissions
    );

    return (
      accessibleSubMenuItems.length > 0 ||
      this.hasAccess(userPermissions, modulePermissions)
    );
  }

  private getEffectivePermissions(permissions: string[]): string[] {
    return permissions.length > 0 ? permissions : [];
  }

  private hasAccess(
    userPermissions: string[],
    requiredPermissions: string[]
  ): boolean {
    return requiredPermissions.some((permission) =>
      userPermissions.includes(permission)
    );
  }

  private filterAccessibleSubMenuItems(
    subMenuItems: MenuItem[],
    parentPermissions: string[],
    userPermissions: string[]
  ): MenuItem[] {
    return subMenuItems.filter((subItem) => {
      const effectivePermissions = this.getEffectivePermissionsForSubMenuItem(
        subItem,
        parentPermissions
      );
      return (
        this.hasAccess(userPermissions, effectivePermissions) ||
        this.filterAccessibleSubMenuItems(
          subItem['subMenuItems'] || [],
          effectivePermissions,
          userPermissions
        ).length > 0
      );
    });
  }

  private getEffectivePermissionsForSubMenuItem(
    subItem: MenuItem,
    parentPermissions: string[]
  ): string[] {
    return subItem['permissions'].length > 0
      ? subItem['permissions']
      : parentPermissions;
  }

  private navigateToNotFound(): boolean {
    this.router.navigate(['/not-found']);
    return false;
  }
}
