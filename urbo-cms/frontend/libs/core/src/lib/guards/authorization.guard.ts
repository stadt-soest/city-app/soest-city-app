import { KeycloakAuthGuard, KeycloakService } from 'keycloak-angular';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { CoreAdapter } from '../core.adapter';

@Injectable({
  providedIn: 'root',
})
export abstract class AuthorizationGuard extends KeycloakAuthGuard {
  protected abstract requiredPermissions: string[];

  protected constructor(
    protected override readonly router: Router,
    protected readonly keycloak: KeycloakService,
    private readonly coreAdapter: CoreAdapter
  ) {
    super(router, keycloak);
  }

  public override async isAccessAllowed(): Promise<boolean> {
    if (!this.authenticated) {
      await this.handleUnauthenticated();
      return false;
    }

    try {
      const userRolesPermissions = await this.getUserRolesPermissions();

      if (this.hasAccess(userRolesPermissions)) {
        return true;
      } else {
        this.handleAccessDenied();
        return false;
      }
    } catch (error) {
      return this.handleError(error);
    }
  }

  private async handleUnauthenticated(): Promise<void> {
    await this.keycloak.login();
  }

  private async getUserRolesPermissions() {
    return this.coreAdapter.fetchCurrentUserPermissions();
  }

  private hasAccess(userRolesPermissions: any): boolean {
    return (
      this.requiredPermissions.length === 0 ||
      userRolesPermissions.permissions.some((permission: string) =>
        this.requiredPermissions.includes(permission)
      )
    );
  }

  private handleAccessDenied(): void {
    this.router.navigate(['/not-found']);
  }

  private handleError(error: any): boolean {
    console.error(error);
    return false;
  }
}
