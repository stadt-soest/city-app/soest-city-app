import { KeycloakAuthGuard, KeycloakService } from 'keycloak-angular';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export abstract class RoleGuard extends KeycloakAuthGuard {
  protected constructor(
    protected override readonly router: Router,
    protected readonly keycloak: KeycloakService
  ) {
    super(router, keycloak);
  }

  protected abstract requiredRoles: string[];

  public override isAccessAllowed(): Promise<boolean> {
    return new Promise((resolve) => {
      if (!this.authenticated) {
        this.keycloak.login();
        return resolve(false);
      }

      if (
        this.requiredRoles.length === 0 ||
        this.roles.some((role) => this.requiredRoles.includes(role))
      ) {
        return resolve(true);
      } else {
        this.router.navigate(['/not-found']);
        return resolve(false);
      }
    });
  }
}
