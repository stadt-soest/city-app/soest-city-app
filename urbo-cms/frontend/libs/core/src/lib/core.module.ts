import {
  ModuleWithProviders,
  NgModule,
  Optional,
  SkipSelf,
  inject,
  provideAppInitializer,
  APP_INITIALIZER,
} from '@angular/core';
import { AuthService } from './services/auth/auth.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { ToastModule } from 'primeng/toast';
import { DragDropModule } from 'primeng/dragdrop';
import { SkeletonModule } from 'primeng/skeleton';
import { LoadingService } from './services/loading.service';
import { ModuleRegistryService } from './services/module-registry/module-registry.service';
import { MenuService } from './services/menu/menu.service';
import { SortService } from './services/sort/sort.service';
import { MessageService } from 'primeng/api';
import { HttpInterceptorService } from './services/http-interceptor.service';
import { UserAccessService } from './services/auth/user-access.service';
import { CORE_MODULE_CONFIG, CoreModuleConfig } from './core-module.config';
import { ImagePipe } from './pipes/image.pipe';
import { ImagePathPipe } from './pipes/image-path.pipe';
import { PrettyJsonPipe } from './pipes/pretty-json.pipe';

const MODULES: any[] = [
  HttpClientModule,
  KeycloakAngularModule,
  ToastModule,
  DragDropModule,
  SkeletonModule,
];

const SERVICES = [
  KeycloakService,
  AuthService,
  LoadingService,
  ModuleRegistryService,
  MenuService,
  SortService,
  {
    provide: APP_INITIALIZER,
    useFactory: (authService: AuthService) => (): Promise<any> =>
      authService.initializeKeycloak(),
    deps: [AuthService, KeycloakService],
    multi: true,
  },
  MessageService,
  { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true },
  UserAccessService,
];

const PIPES = [ImagePipe, ImagePathPipe, PrettyJsonPipe];

@NgModule({
  imports: [...MODULES],
  declarations: [...PIPES],
  exports: [...MODULES, ...PIPES],
})
export class CoreModule {
  static forRoot(config: CoreModuleConfig): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule,
      providers: [
        { provide: CORE_MODULE_CONFIG, useValue: config },
        ...SERVICES,
      ],
    };
  }

  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule has already been loaded. You should only import Core modules in the AppModule only.'
      );
    }
  }
}
