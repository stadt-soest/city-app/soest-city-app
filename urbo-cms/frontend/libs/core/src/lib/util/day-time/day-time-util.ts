import { LocalizationDto } from '@sw-code/urbo-cms-backend-api';

export enum DayOfWeek {
  MONDAY = 'MONDAY',
  TUESDAY = 'TUESDAY',
  WEDNESDAY = 'WEDNESDAY',
  THURSDAY = 'THURSDAY',
  FRIDAY = 'FRIDAY',
  SATURDAY = 'SATURDAY',
  SUNDAY = 'SUNDAY',
}

const dayTranslations: { [key in DayOfWeek]: string } = {
  MONDAY: 'Montag',
  TUESDAY: 'Dienstag',
  WEDNESDAY: 'Mittwoch',
  THURSDAY: 'Donnerstag',
  FRIDAY: 'Freitag',
  SATURDAY: 'Samstag',
  SUNDAY: 'Sonntag',
};

export const DEFAULT_OPENING_HOURS: OpeningHoursMap = {
  MONDAY: { from: '', to: '' },
  TUESDAY: { from: '', to: '' },
  WEDNESDAY: { from: '', to: '' },
  THURSDAY: { from: '', to: '' },
  FRIDAY: { from: '', to: '' },
  SATURDAY: { from: '', to: '' },
  SUNDAY: { from: '', to: '' },
};

export function formatDay(day: DayOfWeek): string {
  return dayTranslations[day];
}

export type OpeningHoursMap = { [day in DayOfWeek]: DateRange };

export type DateRange = { from: string; to: string };

export interface SpecialOpeningHours {
  from: string;
  to: string;
  descriptions: LocalizationDto[];
}

export type SpecialOpeningHoursMap = {
  [date: string]: SpecialOpeningHours;
};
