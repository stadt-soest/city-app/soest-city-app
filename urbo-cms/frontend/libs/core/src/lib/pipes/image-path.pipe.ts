import { Inject, Injectable, Pipe, PipeTransform } from '@angular/core';
import { CORE_MODULE_CONFIG, CoreModuleConfig } from '../core-module.config';

@Pipe({
  name: 'imagePath',
  standalone: false,
})
@Injectable({ providedIn: 'root' })
export class ImagePathPipe implements PipeTransform {
  constructor(
    @Inject(CORE_MODULE_CONFIG) private readonly config: CoreModuleConfig
  ) {}

  transform(imagePath: string) {
    return `${this.config.apiBaseUrl}/files/${imagePath}`;
  }
}
