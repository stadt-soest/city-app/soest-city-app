import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'prettyJson',
  standalone: false,
})
export class PrettyJsonPipe implements PipeTransform {
  transform(val: any) {
    return JSON.stringify(val, undefined, 2)
      .replace(/ /g, ' ')
      .replace(/\n/g, '<br/>');
  }
}
