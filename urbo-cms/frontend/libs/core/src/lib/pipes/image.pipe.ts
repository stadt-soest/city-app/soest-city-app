import { Pipe, PipeTransform } from '@angular/core';
import { ImagePathPipe } from './image-path.pipe';
import { Image } from '../models/image.model';

@Pipe({
  name: 'image',
  standalone: false,
})
export class ImagePipe implements PipeTransform {
  constructor(private readonly imagePathPipe: ImagePathPipe) {}

  transform(image: Image, highRes = false) {
    if (highRes && image.highRes) {
      return this.imagePathPipe.transform(image.highRes.filename!);
    } else if (image.thumbnail) {
      return this.imagePathPipe.transform(image.thumbnail.filename!);
    }

    return null;
  }
}
