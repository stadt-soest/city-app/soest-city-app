import { InjectionToken } from '@angular/core';

export interface CoreModuleConfig {
  apiBaseUrl: string;
  keycloakUrl: string;
  keycloakRealm: string;
  keycloakClientId: string;
}

export const CORE_MODULE_CONFIG = new InjectionToken<CoreModuleConfig>(
  'CoreModuleConfig'
);
