import { InjectionToken } from '@angular/core';

export { DynamicAuthorizationGuard } from './lib/guards/dynamic-authorization/dynamic-authorization.guard';
export { AuthorizationGuard } from './lib/guards/authorization.guard';
export { RoleGuard } from './lib/guards/role.guard';

export { CurrentSort } from './lib/interfaces/current-sort.interface';
export { FeatureModule } from './lib/interfaces/feature-module.interface';

export { AbstractMapper } from './lib/mappers/base/abstract-mapper';

export { I18nString } from './lib/models/i18n-string.model';
export * from './lib/models/image.model';
export * from './lib/models/permission.model';
export { MenuChangeEvent } from './lib/models/menu-change-event';

export { ImagePipe } from './lib/pipes/image.pipe';
export { ImagePathPipe } from './lib/pipes/image-path.pipe';
export { PrettyJsonPipe } from './lib/pipes/pretty-json.pipe';
export * from './lib/util/day-time/day-time-util';

export { UserAccessService } from './lib/services/auth/user-access.service';
export { AuthService } from './lib/services/auth/auth.service';
export { MenuService } from './lib/services/menu/menu.service';
export { ModuleRegistryService } from './lib/services/module-registry/module-registry.service';
export { LayoutService } from './lib/services/app.layout.service';
export * from './lib/models/menu.model';

export { SortService } from './lib/services/sort/sort.service';
export { HttpInterceptorService } from './lib/services/http-interceptor.service';
export { LoadingService } from './lib/services/loading.service';

export { CoreModule } from './lib/core.module';
export { CoreTestModule } from './lib/test/core-test.module';
export { CoreAdapter } from './lib/core.adapter';
export * from './lib/core-module.config';

export const MAP_STYLE_URL = new InjectionToken<string>('MapStyleUrl');
export const MAP_ZOOM = new InjectionToken<number>('MapZoom');
