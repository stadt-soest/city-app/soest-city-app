import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FeedbackSubmission } from './models/feedback-submission.model';
import { AccessibilitySubmission } from './models/accessibility-submission.model';

@Injectable()
export abstract class AppFeedbackAdapter {
  abstract getFeedbackSubmissions(
    page: number,
    rows: number,
    sortField?: string,
    sortOrder?: string
  ): Observable<{ content: FeedbackSubmission[]; totalRecords: number }>;

  abstract getFeedbackSubmissionsCount(): Observable<number>;

  abstract getFeedbackSubmission(
    submissionId: string
  ): Observable<FeedbackSubmission>;

  abstract getAccessibilitySubmissions(
    page: number,
    rows: number,
    sortField?: string,
    sortOrder?: string
  ): Observable<{ content: AccessibilitySubmission[]; totalRecords: number }>;

  abstract getAccessibilitySubmission(
    submissionId: string
  ): Observable<AccessibilitySubmission>;

  abstract getAccessibilitySubmissionsCount(): Observable<number>;
}
