import { Component, DestroyRef, OnInit } from '@angular/core';
import { take } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ActivatedRoute } from '@angular/router';
import { AccessibilitySubmission } from '../../../models/accessibility-submission.model';
import { AppFeedbackAdapter } from '../../../app-feedback.adapter';

@Component({
  selector: 'urbo-cms-app-feedback-accessibility-submission-details',
  styleUrl: 'accessibility-submission-details.component.scss',
  templateUrl: './accessibility-submission-details.component.html',
  standalone: false,
})
export class AccessibilitySubmissionDetailsComponent implements OnInit {
  accessibilitySubmission?: AccessibilitySubmission;
  screenshotsOpen = false;
  responsiveOptions: any[] = [
    {
      breakpoint: '1024px',
      numVisible: 5,
    },
    {
      breakpoint: '768px',
      numVisible: 3,
    },
    {
      breakpoint: '560px',
      numVisible: 1,
    },
  ];

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly accessibilitySubmissionService: AppFeedbackAdapter,
    private readonly destroyRef: DestroyRef
  ) {}

  ngOnInit() {
    this.loadAccessibilitySubmission();
  }

  loadAccessibilitySubmission() {
    this.accessibilitySubmissionService
      .getAccessibilitySubmission(this.activatedRoute.snapshot.params['id'])
      .pipe(take(1), takeUntilDestroyed(this.destroyRef))
      .subscribe((accessibilitySubmission: AccessibilitySubmission) => {
        this.accessibilitySubmission = accessibilitySubmission;
      });
  }
}
