import { Component, inject, OnInit } from '@angular/core';
import { AccessibilitySubmission } from '../../models/accessibility-submission.model';
import { CurrentSort, Permission, SortService } from '@sw-code/urbo-cms-core';
import { Router } from '@angular/router';
import { AppFeedbackAdapter } from '../../app-feedback.adapter';

@Component({
  selector: 'urbo-cms-app-feedback-accessibility-submissions',
  templateUrl: './accessibility-submissions.component.html',
  standalone: false,
})
export class AccessibilitySubmissionsComponent implements OnInit {
  baseCols = [
    { header: 'Eingereicht am', field: 'createdAt', type: 'datetime' },
    { header: 'Beschreibung', field: 'answer' },
  ];
  accessibilitySubmissions!: AccessibilitySubmission[];
  totalRecords!: number;
  rows = 10;
  protected readonly Permission = Permission;
  private readonly SORTABLE_FIELDS = ['createdAt', 'answer'];
  currentSort: CurrentSort = { field: '', order: 'asc' };
  private readonly appFeedbackAdapter = inject(AppFeedbackAdapter);
  private readonly router = inject(Router);
  private readonly sortService = inject(SortService);

  ngOnInit() {
    this.loadData(0, this.rows);
  }

  get cols() {
    return this.sortService.getColumns(this.baseCols, this.SORTABLE_FIELDS);
  }

  loadData(page: number, rows: number, sortField?: string, sortOrder?: string) {
    this.appFeedbackAdapter
      .getAccessibilitySubmissions(page, rows, sortField, sortOrder)
      .subscribe((data: any) => {
        this.accessibilitySubmissions = data.content;
        this.totalRecords = data.totalRecords;
      });
  }

  onPage(event: any) {
    const page = event.first / event.rows;
    this.loadData(
      page,
      event.rows,
      this.currentSort?.field,
      this.currentSort?.order
    );
  }

  showSubmissionDetails(clickedRowData: any) {
    this.router.navigate([
      '/app-feedback',
      'accessibility-submissions',
      clickedRowData.id,
    ]);
  }

  onSort(sortEvent: CurrentSort) {
    this.currentSort = this.sortService.handleSort(sortEvent);
    this.loadData(0, this.rows, this.currentSort.field, this.currentSort.order);
  }
}
