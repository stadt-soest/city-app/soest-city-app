import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FeedbackSubmissionsComponent } from './feedback-submissions/feedback-submissions.component';
import { FeedbackSubmissionDetailsComponent } from './feedback-submissions/feedback-submission-details.component/feedback-submission-details.component';
import { AccessibilitySubmissionsComponent } from './accessibility-submissions/accessibility-submissions.component';
import { AccessibilitySubmissionDetailsComponent } from './accessibility-submissions/accessibility-submission-details/accessibility-submission-details.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
  },
  {
    path: 'feedback-submissions',
    component: FeedbackSubmissionsComponent,
  },
  {
    path: 'feedback-submissions/:id',
    component: FeedbackSubmissionDetailsComponent,
  },
  {
    path: 'accessibility-submissions',
    component: AccessibilitySubmissionsComponent,
  },
  {
    path: 'accessibility-submissions/:id',
    component: AccessibilitySubmissionDetailsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {}
