import { Component, DestroyRef, OnInit } from '@angular/core';
import { take } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { AppFeedbackAdapter } from '../../app-feedback.adapter';

@Component({
  selector: 'urbo-cms-app-feedback-dashboard',
  templateUrl: './dashboard.component.html',
  standalone: false,
})
export class DashboardComponent implements OnInit {
  feedbackSubmissionCount = 0;
  accessibilitySubmissionCount = 0;

  constructor(
    private readonly appFeedbackAdapter: AppFeedbackAdapter,
    private readonly destroyRef: DestroyRef
  ) {}

  ngOnInit() {
    this.loadAppFeedbackSubmissionCount();
  }

  loadAppFeedbackSubmissionCount() {
    this.appFeedbackAdapter
      .getFeedbackSubmissionsCount()
      .pipe(take(1), takeUntilDestroyed(this.destroyRef))
      .subscribe((count: number) => {
        this.feedbackSubmissionCount = count;
      });
    this.appFeedbackAdapter
      .getAccessibilitySubmissionsCount()
      .pipe(take(1), takeUntilDestroyed(this.destroyRef))
      .subscribe((count: number) => {
        this.accessibilitySubmissionCount = count;
      });
  }
}
