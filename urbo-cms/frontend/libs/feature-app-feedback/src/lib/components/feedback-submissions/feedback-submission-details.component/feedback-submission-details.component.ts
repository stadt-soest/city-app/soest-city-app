import { Component, DestroyRef, OnInit } from '@angular/core';
import { take } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ActivatedRoute } from '@angular/router';
import { FeedbackSubmission } from '../../../models/feedback-submission.model';
import { AppFeedbackAdapter } from '../../../app-feedback.adapter';

@Component({
  selector: 'urbo-cms-app-feedback-submission-details',
  styleUrl: './feedback-submission-details.component.scss',
  templateUrl: './feedback-submission-details.component.html',
  standalone: false,
})
export class FeedbackSubmissionDetailsComponent implements OnInit {
  feedbackSubmission?: FeedbackSubmission;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly feedbackSubmissionService: AppFeedbackAdapter,
    private readonly destroyRef: DestroyRef
  ) {}

  ngOnInit() {
    this.loadFeedbackSubmission();
  }

  loadFeedbackSubmission() {
    this.feedbackSubmissionService
      .getFeedbackSubmission(this.activatedRoute.snapshot.params['id'])
      .pipe(take(1), takeUntilDestroyed(this.destroyRef))
      .subscribe((feedbackSubmission: FeedbackSubmission) => {
        this.feedbackSubmission = feedbackSubmission;
      });
  }
}
