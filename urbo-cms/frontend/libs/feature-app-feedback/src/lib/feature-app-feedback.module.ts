import {
  ModuleWithProviders,
  NgModule,
  inject,
  provideAppInitializer,
} from '@angular/core';
import {
  CoreModule,
  FeatureModule,
  MenuItem,
  ModuleCategory,
  ModuleInfo,
  ModuleRegistryService,
  Permission,
} from '@sw-code/urbo-cms-core';
import { DashboardDataLoaderService } from './services/dashboard-data-loader/dashboard-data-loader.service';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { FeedbackSubmissionsComponent } from './components/feedback-submissions/feedback-submissions.component';
import { FeedbackSubmissionDetailsComponent } from './components/feedback-submissions/feedback-submission-details.component/feedback-submission-details.component';
import { AccessibilitySubmissionsComponent } from './components/accessibility-submissions/accessibility-submissions.component';
import { AccessibilitySubmissionDetailsComponent } from './components/accessibility-submissions/accessibility-submission-details/accessibility-submission-details.component';
import { UIModule } from '@sw-code/urbo-cms-ui';

const MODULES = [CoreModule, UIModule];
const PROVIDERS = [DashboardDataLoaderService];

@NgModule({
  declarations: [
    DashboardComponent,
    FeedbackSubmissionsComponent,
    FeedbackSubmissionDetailsComponent,
    AccessibilitySubmissionsComponent,
    AccessibilitySubmissionDetailsComponent,
  ],
  imports: [...MODULES],
  exports: [...MODULES],
  providers: [...PROVIDERS],
})
export class FeatureAppFeedbackModule {
  readonly featureModule: FeatureModule;
  moduleName = 'Feedback';

  constructor(
    private readonly dashboardDataLoaderService: DashboardDataLoaderService
  ) {
    this.featureModule = {
      info: new ModuleInfo(
        this.moduleName,
        ModuleCategory.MODULES,
        'pi pi-comment',
        'app-feedback',
        [
          new MenuItem(
            'Feedback Submissions',
            'pi pi-table',
            'app-feedback/feedback-submissions',
            [],
            [Permission.FeedbackSubmissionRead]
          ),
          new MenuItem(
            'Accessibility Submissions',
            'pi pi-times-circle',
            'app-feedback/accessibility-submissions',
            [],
            [Permission.AccessibilitySubmissionRead]
          ),
        ]
      ),
      routes: [
        {
          path: 'app-feedback',
          loadChildren: () =>
            import('./components/routing.module').then((m) => m.RoutingModule),
        },
      ],
      dashboardData: {
        moduleName: this.moduleName,
        entityCount: 0,
        detailsData: () =>
          this.dashboardDataLoaderService.getFeedbackSubmissions(),
      },
    };
  }

  static forRoot(): ModuleWithProviders<FeatureAppFeedbackModule> {
    return {
      ngModule: FeatureAppFeedbackModule,
      providers: [
        provideAppInitializer(() => {
          const initializerFn = (
            (
              registry: ModuleRegistryService,
              module: FeatureAppFeedbackModule
            ) =>
            () => {
              registry.registerFeature(module.featureModule);
            }
          )(inject(ModuleRegistryService), inject(FeatureAppFeedbackModule));
          return initializerFn();
        }),
      ],
    };
  }
}
