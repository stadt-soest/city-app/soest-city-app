import { DeviceEnvironment } from './device-environment.model';
import { ImageFile } from '@sw-code/urbo-cms-core';

export class AccessibilitySubmission {
  id: string;
  createdAt: Date;
  answer: string;
  environment?: DeviceEnvironment;
  images: ImageFile[] = [];

  constructor(
    id: string,
    createdAt: Date,
    answer: string,
    environment: DeviceEnvironment,
    images: ImageFile[] = []
  ) {
    this.id = id;
    this.createdAt = createdAt;
    this.answer = answer;
    this.environment = environment;
    this.images = images;
  }
}

export class AccessibilitySubmissionAnswer {
  questionId?: string;
  selectedOptionId?: string;
  freeText?: string;

  constructor(questionId: string, selectedOptionId: string, freeText: string) {
    this.questionId = questionId;
    this.selectedOptionId = selectedOptionId;
    this.freeText = freeText;
  }
}
