export class DeviceEnvironment {
  deviceModelName: string;
  operatingSystemName: string;
  operatingSystemVersion: string;
  appVersion: string;
  browserName: string;
  usedAs: string;
  appSettings?: string;

  constructor(
    deviceModelName: string,
    operatingSystemName: string,
    operatingSystemVersion: string,
    appVersion: string,
    browserName: string,
    usedAs: string,
    appSettings?: string
  ) {
    this.deviceModelName = deviceModelName;
    this.operatingSystemName = operatingSystemName;
    this.operatingSystemVersion = operatingSystemVersion;
    this.appVersion = appVersion;
    this.browserName = browserName;
    this.usedAs = usedAs;
    this.appSettings = appSettings;
  }
}
