import { DeviceEnvironment } from './device-environment.model';

export class FeedbackSubmission {
  id: string;
  submissionTime: Date;
  deduplicationKey: string;
  environment: DeviceEnvironment;
  answers: FeedbackSubmissionAnswer[];

  constructor(
    id: string,
    createdAt: Date,
    deduplicationKey: string,
    environment: DeviceEnvironment,
    answers: FeedbackSubmissionAnswer[]
  ) {
    this.id = id;
    this.submissionTime = createdAt;
    this.deduplicationKey = deduplicationKey;
    this.environment = environment;
    this.answers = answers;
  }
}

export class FeedbackSubmissionAnswer {
  questionId?: string;
  selectedOptionId?: string;
  freeText?: string;

  constructor(questionId: string, selectedOptionId: string, freeText: string) {
    this.questionId = questionId;
    this.selectedOptionId = selectedOptionId;
    this.freeText = freeText;
  }
}
