import { Injectable } from '@angular/core';
import { take } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppFeedbackAdapter } from '../../app-feedback.adapter';

@Injectable()
export class DashboardDataLoaderService {
  constructor(private readonly feedbackService: AppFeedbackAdapter) {}

  getFeedbackSubmissions() {
    return this.feedbackService.getAccessibilitySubmissions(0, 5).pipe(
      take(1),
      map((response) => {
        const submissions = response.content.map((submission) => ({
          id: submission.id,
          answer: submission.answer,
          createdAt: submission.createdAt,
        }));

        return submissions.map((submission) => ({
          route: `/app-feedback/accessibility-submissions/${submission.id}`,
          answer: submission.answer,
          createdAt: submission.createdAt,
        }));
      })
    );
  }
}
