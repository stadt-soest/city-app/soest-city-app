export { FeatureAppFeedbackModule } from './lib/feature-app-feedback.module';
export { AppFeedbackAdapter } from './lib/app-feedback.adapter';
export * from './lib/models/accessibility-submission.model';
export * from './lib/models/device-environment.model';
export * from './lib/models/feedback-submission.model';
