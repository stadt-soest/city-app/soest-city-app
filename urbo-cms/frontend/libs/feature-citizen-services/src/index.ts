export { FeatureCitizenServicesModule } from './lib/feature-citizen-services.module';
export { CitizenServiceAdapter } from './lib/citizen-service.adapter';
export * from './lib/models/tag.model';
export * from './lib/models/citizen-service.model';
