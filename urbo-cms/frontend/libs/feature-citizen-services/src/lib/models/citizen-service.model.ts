import { Tag } from './tag.model';

export interface CitizenService {
  id: string;
  favorite: boolean;
  tags: Array<Tag>;
  visibility: boolean;
  title: string;
  description: string;
  url: string;
  editable: boolean;
}

export interface UpdateCitizenServiceDto {
  title: string;
  description: string;
  url: string;
  visible: boolean;
  favorite: boolean;
}
