export interface Tag {
  id: string;
  localizedNames: Array<Localization>;
}

export interface Localization {
  value: string;
  language: Language;
}

export type Language = 'DE' | 'EN';

export const Language = {
  De: 'DE' as Language,
  En: 'EN' as Language,
};

export interface CreateTagDto {
  id?: string | null;
  localizedNames: Array<Localization>;
}
