import {
  ModuleWithProviders,
  NgModule,
  inject,
  provideAppInitializer,
} from '@angular/core';
import {
  FeatureModule,
  MenuItem,
  ModuleCategory,
  ModuleInfo,
  ModuleRegistryService,
  Permission,
} from '@sw-code/urbo-cms-core';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CreateTagComponent } from './components/create-tag/create-tag.component';
import { TagsComponent } from './components/tags/tags.component';
import { UngroupedCitizenServicesComponent } from './components/ungrouped-citizen-services/ungrouped-citizen-services.component';
import { EditCitizenServiceComponent } from './components/edit-citizen-service/edit-citizen-service.component';
import { CreateCitizenServiceComponent } from './components/create-citizen-service/create-citizen-service.component';
import { CitizenServiceDetailsComponent } from './components/citizen-service-details/citizen-service-details.component';
import { CitizenServiceFormComponent } from './components/citizen-service-details/citizen-service-form/citizen-service-form.component';
import { take } from 'rxjs/operators';
import { CitizenServiceAdapter } from './citizen-service.adapter';
import { UIModule } from '@sw-code/urbo-cms-ui';

const MODULES = [UIModule];

@NgModule({
  declarations: [
    DashboardComponent,
    CreateTagComponent,
    TagsComponent,
    UngroupedCitizenServicesComponent,
    EditCitizenServiceComponent,
    CreateCitizenServiceComponent,
    CitizenServiceDetailsComponent,
    CitizenServiceFormComponent,
  ],
  imports: [...MODULES],
  exports: [...MODULES],
})
export class FeatureCitizenServicesModule {
  readonly featureModule: FeatureModule;
  moduleName = 'Bürgerservices';

  constructor(private readonly citizenServiceAdapter: CitizenServiceAdapter) {
    this.featureModule = {
      info: new ModuleInfo(
        this.moduleName,
        ModuleCategory.MODULES,
        'pi pi-users',
        'citizen-services',
        [
          new MenuItem(this.moduleName, 'pi pi-users', 'citizen-services'),
          new MenuItem(
            'Kategorien',
            'pi pi-folder-open',
            'citizen-services/categories',
            [],
            [Permission.CategoryRead]
          ),
        ]
      ),
      routes: [
        {
          path: 'citizen-services',
          loadChildren: () =>
            import('./components/routing.module').then((m) => m.RoutingModule),
        },
      ],
      dashboardData: {
        moduleName: this.moduleName,
        entityCount: 0,
      },
    };

    this.citizenServiceAdapter
      .getCitizenServices(0, 10)
      .pipe(take(1))
      .subscribe((response) => {
        if (this.featureModule.dashboardData) {
          this.featureModule.dashboardData.entityCount = response.totalRecords;
        }
      });
  }

  static forRoot(): ModuleWithProviders<FeatureCitizenServicesModule> {
    return {
      ngModule: FeatureCitizenServicesModule,
      providers: [
        provideAppInitializer(() => {
          const initializerFn = (
            (
              registry: ModuleRegistryService,
              module: FeatureCitizenServicesModule
            ) =>
            () => {
              registry.registerFeature(module.featureModule);
            }
          )(
            inject(ModuleRegistryService),
            inject(FeatureCitizenServicesModule)
          );
          return initializerFn();
        }),
      ],
    };
  }
}
