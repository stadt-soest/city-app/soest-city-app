import { ModuleWithProviders, NgModule } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FeatureCitizenServicesModule } from '../feature-citizen-services.module';
import { CoreModule } from '@sw-code/urbo-cms-core';

const MODULES = [FeatureCitizenServicesModule, HttpClientTestingModule];

@NgModule({
  imports: [...MODULES],
  exports: [...MODULES],
})
export class FeatureCitizenServiceTestModule {
  static forRoot(): ModuleWithProviders<FeatureCitizenServiceTestModule> {
    return {
      ngModule: FeatureCitizenServiceTestModule,
      providers: CoreModule.forRoot({
        keycloakRealm: '',
        keycloakClientId: '',
        keycloakUrl: '',
        apiBaseUrl: '',
      }).providers,
    };
  }
}
