import {
  CitizenService,
  UpdateCitizenServiceDto,
} from './models/citizen-service.model';
import { Observable } from 'rxjs';
import { CreateTagDto, Tag } from './models/tag.model';
import { CreateCitizenServiceDto } from '@sw-code/urbo-cms-backend-api';

export abstract class CitizenServiceAdapter {
  abstract getCitizenService(id: string): Observable<CitizenService>;

  abstract getCitizenServices(
    page: number,
    rows: number,
    options?: {
      visible?: boolean | null;
      favorite?: boolean | null;
      withoutTag?: boolean;
      onlyExternal?: 'EXTERNAL' | 'INTERNAL' | null;
      sortField?: string;
      sortOrder?: string;
    }
  ): Observable<{ content: CitizenService[]; totalRecords: number }>;

  abstract createCitizenService(
    createCitizenServiceDto: CreateCitizenServiceDto
  ): Observable<CitizenService>;

  abstract updateVisibilityAndFavorite(
    citizenServiceId: string,
    visible: boolean,
    favorite: boolean
  ): Observable<{ visibility: CitizenService; favorite: CitizenService }>;

  abstract updateCitizenService(
    citizenServiceId: string,
    dto: UpdateCitizenServiceDto
  ): Observable<CitizenService>;

  abstract deleteCitizenService(citizenServiceId: string): Observable<void>;

  abstract updateVisibility(
    citizenServiceId: string,
    visible: boolean
  ): Observable<CitizenService>;

  abstract getTags(): Observable<Tag[]>;

  abstract createOrUpdateTag(createTagDto: CreateTagDto): Observable<Tag>;

  abstract deleteTag(tagId: string): Observable<void>;

  abstract updateFavorite(
    citizenServiceId: string,
    favorite: boolean
  ): Observable<CitizenService>;

  abstract searchCitizenServices(
    search: string,
    page: number,
    rows: number
  ): Observable<{ content: CitizenService[]; totalRecords: number }>;

  abstract getBySearchTerm(
    searchTerm: string,
    page?: number,
    size?: number,
    sortField?: string,
    sortOrder?: string
  ): Observable<{
    total: number;
    items: CitizenService[];
  }>;

  abstract updateTagAssignments(
    tagId: string,
    serviceIds: string[]
  ): Observable<void>;
}
