import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { take } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CitizenService } from '../../models/citizen-service.model';
import { CitizenServiceAdapter } from '../../citizen-service.adapter';

@Component({
  selector: 'urbo-cms-citizen-services-edit',
  templateUrl: './edit-citizen-service.component.html',
  standalone: false,
})
export class EditCitizenServiceComponent implements OnInit {
  @Input() citizenService!: CitizenService;
  @Output() close = new EventEmitter<void>();

  citizenServiceForm!: FormGroup;

  constructor(
    private readonly fb: FormBuilder,
    private readonly citizenServiceApiClient: CitizenServiceAdapter
  ) {}

  ngOnInit(): void {
    this.citizenServiceForm = this.fb.group({
      title: [this.citizenService.title, Validators.required],
      description: [this.citizenService.description, Validators.required],
      url: [
        this.citizenService.url,
        [
          Validators.required,
          Validators.pattern(/^(ftp|http|https):\/\/[^ "]+$/),
        ],
      ],
      visible: [this.citizenService.visibility],
      favorite: [this.citizenService.favorite],
    });
  }

  onSubmit() {
    if (this.citizenServiceForm.invalid) {
      this.citizenServiceForm.markAllAsTouched();
      return;
    }

    this.citizenServiceApiClient
      .updateCitizenService(
        this.citizenService.id,
        this.citizenServiceForm.value
      )
      .pipe(take(1))
      .subscribe({
        next: () => {
          this.close.emit();
        },
        error: (error) => {
          console.error('Failed to update citizen service', error);
        },
      });
  }

  onCancel() {
    this.close.emit();
  }
}
