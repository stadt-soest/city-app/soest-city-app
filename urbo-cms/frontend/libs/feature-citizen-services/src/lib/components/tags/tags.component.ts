import { Component, OnInit, ViewChild } from '@angular/core';
import { catchError, take } from 'rxjs/operators';
import { ConfirmationService, MessageService } from 'primeng/api';
import { CitizenService } from '../../models/citizen-service.model';
import { CreateTagComponent } from '../create-tag/create-tag.component';
import { UngroupedCitizenServicesComponent } from '../ungrouped-citizen-services/ungrouped-citizen-services.component';
import { CitizenServiceAdapter } from '../../citizen-service.adapter';
import { Permission } from '@sw-code/urbo-cms-core';
import { Tag } from '../../models/tag.model';

@Component({
  selector: 'urbo-cms-citizen-services-tags',
  templateUrl: './tags.component.html',
  providers: [MessageService, ConfirmationService],
  standalone: false,
})
export class TagsComponent implements OnInit {
  tags: Tag[] = [];
  protected readonly Permission = Permission;
  loading = false;
  tagToEdit: Tag | null = null;
  groupedCitizenServices: { [key: string]: CitizenService[] } = {};
  allCitizenServices: { label: string; value: CitizenService }[] = [];

  @ViewChild(CreateTagComponent) createTagComponent?: CreateTagComponent;
  @ViewChild(UngroupedCitizenServicesComponent)
  ungroupedCitizenServicesComponent?: UngroupedCitizenServicesComponent;

  constructor(
    private readonly citizenServiceService: CitizenServiceAdapter,
    private readonly confirmationService: ConfirmationService
  ) {}

  ngOnInit(): void {
    this.loadTags();
    this.loadAllCitizenServices();
  }

  showDialog() {
    this.tagToEdit = null;
    this.createTagComponent?.showDialog();
  }

  loadTags() {
    this.citizenServiceService
      .getTags()
      .pipe(take(1))
      .subscribe((tags: Tag[]) => {
        this.tags = tags;
        this.groupCitizenServicesByTag();
      });
  }

  loadCitizenServices() {
    this.citizenServiceService
      .getCitizenServices(0, 100, { visible: true, favorite: false })
      .pipe(take(1))
      .subscribe((response: { content: CitizenService[] }) => {
        this.groupedCitizenServices = {};
        response.content.forEach((service) => {
          service.tags.forEach((tag: { id: string | number }) => {
            if (!this.groupedCitizenServices[tag.id]) {
              this.groupedCitizenServices[tag.id] = [];
            }
            this.groupedCitizenServices[tag.id].push(service);
          });
        });
      });
  }

  loadAllCitizenServices() {
    this.citizenServiceService
      .getCitizenServices(0, 100, { visible: true, favorite: false })
      .pipe(take(1))
      .subscribe((response: { content: CitizenService[] }) => {
        this.allCitizenServices = response.content.map((service) => ({
          label: service.title,
          value: service,
        }));
      });
  }

  groupCitizenServicesByTag() {
    this.groupedCitizenServices = this.tags.reduce<{
      [key: string]: CitizenService[];
    }>((acc, tag) => {
      acc[tag.id] = [];
      return acc;
    }, {});
    this.loadCitizenServices();
  }

  updateTag(tag: Tag) {
    this.loading = true;
    const updateTagDto = { ...tag, id: tag.id || null };
    this.citizenServiceService
      .createOrUpdateTag(updateTagDto)
      .pipe(
        take(1),
        catchError((err) => {
          this.loading = false;
          throw err;
        })
      )
      .subscribe(() => {
        const selectedServices = this.groupedCitizenServices[tag.id].map(
          (service) => service.id
        );
        this.citizenServiceService
          .updateTagAssignments(tag.id, selectedServices)
          .pipe(
            take(1),
            catchError((err) => {
              this.loading = false;
              throw err;
            })
          )
          .subscribe(() => {
            this.loadAllCitizenServices();
            this.loadTags();
            this.ungroupedCitizenServicesComponent?.loadUngroupedCitizenServices();
            this.loading = false;
          });
      });
  }

  confirmDeleteTag(tag: Tag) {
    this.confirmationService.confirm({
      key: 'deleteTag',
      message: 'Bist du dir sicher, dass du diese Kategorie löschen möchtest?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Yes',
      rejectLabel: 'Cancel',
      acceptButtonStyleClass: 'p-button-danger',
      rejectButtonStyleClass: 'p-button-default',
      accept: () => this.deleteTag(tag),
    });
  }

  private deleteTag(tag: Tag) {
    this.loading = true;
    this.citizenServiceService
      .deleteTag(tag.id)
      .pipe(
        take(1),
        catchError((err) => {
          this.loading = false;
          throw err;
        })
      )
      .subscribe(() => {
        this.loadTags();
        this.loading = false;
      });
  }
}
