import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  Output,
} from '@angular/core';
import { catchError, take } from 'rxjs/operators';
import { CitizenServiceAdapter } from '../../citizen-service.adapter';
import { CreateTagDto, Language, Tag } from '../../models/tag.model';
import { Permission } from '@sw-code/urbo-cms-core';

@Component({
  selector: 'urbo-cms-citizen-services-create-tag',
  templateUrl: './create-tag.component.html',
  standalone: false,
})
export class CreateTagComponent implements OnChanges {
  @Output() created: EventEmitter<void> = new EventEmitter<void>();
  @Output() updated: EventEmitter<void> = new EventEmitter<void>();
  @Input() tagToEdit: Tag | null = null;

  currentTag: CreateTagDto = { localizedNames: [] };
  protected readonly Permission = Permission;
  localizedNames: { de: string; en: string } = { de: '', en: '' };
  displayDialog = false;

  constructor(private readonly citizenServiceService: CitizenServiceAdapter) {}

  @HostListener('document:keydown.escape', ['$event'])
  handleEscape() {
    this.resetDialog();
  }

  ngOnChanges() {
    if (this.tagToEdit) {
      this.currentTag = { ...this.tagToEdit };
      this.localizedNames.de = this.getLocalization('DE');
      this.localizedNames.en = this.getLocalization('EN');
      this.displayDialog = true;
    }
  }

  getLocalization(language: Language): string {
    const localization = this.currentTag.localizedNames.find(
      (loc) => loc.language === language
    );
    return localization ? localization.value : '';
  }

  updateLocalizedNames(language: Language, value: string) {
    const localization = this.currentTag.localizedNames.find(
      (loc) => loc.language === language
    );
    if (localization) {
      localization.value = value;
    } else {
      this.currentTag.localizedNames.push({ language, value });
    }
  }

  createOrUpdateTag() {
    this.currentTag.localizedNames = [
      { language: 'DE', value: this.localizedNames.de },
      { language: 'EN', value: this.localizedNames.en },
    ];

    this.citizenServiceService
      .createOrUpdateTag(this.currentTag)
      .pipe(
        take(1),
        catchError((err) => {
          console.error('Error creating or updating tag:', err);
          return err;
        })
      )
      .subscribe(() => {
        if (this.currentTag.id) {
          this.updated.emit();
        } else {
          this.created.emit();
        }
        this.resetDialog();
      });
  }

  showDialog() {
    this.displayDialog = true;
  }

  hideDialog() {
    this.resetDialog();
  }

  private resetDialog() {
    this.displayDialog = false;
    this.currentTag = { localizedNames: [] };
    this.localizedNames = { de: '', en: '' };
    this.tagToEdit = null;
  }
}
