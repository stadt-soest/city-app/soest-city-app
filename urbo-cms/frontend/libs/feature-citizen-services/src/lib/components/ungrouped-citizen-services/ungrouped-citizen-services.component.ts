import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CitizenService } from '../../models/citizen-service.model';
import { catchError, take } from 'rxjs/operators';
import { CitizenServiceAdapter } from '../../citizen-service.adapter';

@Component({
  selector: 'urbo-cms-citizen-services-ungrouped',
  templateUrl: './ungrouped-citizen-services.component.html',
  standalone: false,
})
export class UngroupedCitizenServicesComponent implements OnInit {
  @Input() groupedCitizenServices!: { [key: string]: CitizenService[] };
  @Output() citizenServiceUpdated = new EventEmitter<void>();
  ungroupedCitizenServices: CitizenService[] = [];

  constructor(private readonly citizenServiceService: CitizenServiceAdapter) {}

  ngOnInit(): void {
    this.loadUngroupedCitizenServices();
  }

  loadUngroupedCitizenServices() {
    this.citizenServiceService
      .getCitizenServices(0, 100, {
        visible: true,
        favorite: undefined,
        withoutTag: true,
      })
      .pipe(
        take(1),
        catchError((err) => {
          throw err;
        })
      )
      .subscribe((response) => {
        this.ungroupedCitizenServices = response.content;
      });
  }
}
