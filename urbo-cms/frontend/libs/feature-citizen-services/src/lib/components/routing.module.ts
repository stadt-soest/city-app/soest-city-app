import { DashboardComponent } from './dashboard/dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { TagsComponent } from './tags/tags.component';
import { CitizenServiceDetailsComponent } from './citizen-service-details/citizen-service-details.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
  },
  {
    path: 'categories',
    component: TagsComponent,
  },
  {
    path: 'details/:id',
    component: CitizenServiceDetailsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {}
