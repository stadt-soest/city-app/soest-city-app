import { Component, EventEmitter, Output } from '@angular/core';
import { take } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CitizenServiceAdapter } from '../../citizen-service.adapter';

@Component({
  selector: 'urbo-cms-citizen-services-create',
  templateUrl: './create-citizen-service.component.html',
  standalone: false,
})
export class CreateCitizenServiceComponent {
  citizenServiceForm: FormGroup;
  @Output() close = new EventEmitter<void>();

  constructor(
    private readonly fb: FormBuilder,
    private readonly citizenServiceAdapter: CitizenServiceAdapter
  ) {
    this.citizenServiceForm = this.fb.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      url: [
        '',
        [Validators.required, Validators.pattern(/^(http|https):\/\/[^ "]+$/)],
      ],
      visible: [true],
      favorite: [false],
    });
  }

  onSubmit() {
    if (this.citizenServiceForm.valid) {
      this.citizenServiceAdapter
        .createCitizenService(this.citizenServiceForm.value)
        .pipe(take(1))
        .subscribe({
          next: () => {
            this.citizenServiceForm.reset({ visible: true, favorite: false });
            this.close.emit();
          },
          error: (error) => {
            console.error('Failed to create citizen service', error);
          },
        });
    }
  }

  onCancel() {
    this.citizenServiceForm.reset({ visible: true, favorite: false });
    this.close.emit();
  }
}
