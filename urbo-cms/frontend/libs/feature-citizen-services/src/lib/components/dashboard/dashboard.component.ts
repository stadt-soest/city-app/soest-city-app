import { Component, inject, OnInit } from '@angular/core';
import { CitizenService } from '../../models/citizen-service.model';
import { CurrentSort, Permission, SortService } from '@sw-code/urbo-cms-core';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { CitizenServiceAdapter } from '../../citizen-service.adapter';

@Component({
  selector: 'urbo-cms-citizen-services',
  templateUrl: './dashboard.component.html',
  standalone: false,
})
export class DashboardComponent implements OnInit {
  baseCols = [
    { header: 'Titel', field: 'title', type: 'text' },
    { header: 'Beschreibung', field: 'description', type: 'text' },
    { header: 'Url', field: 'url', type: 'link' },
  ];
  visibleCitizenServices!: CitizenService[];
  totalRecords!: number;
  rows = 10;
  currentSearchTerm = '';
  filterOptions = [
    { label: 'Alle', value: null },
    { label: 'Sichtbar', value: 'visible' },
    { label: 'Versteckt', value: 'hidden' },
    { label: 'Favorit', value: 'favorite' },
    { label: 'Erstellt', value: 'created' },
  ];
  selectedFilter: string | null = null;
  displayDialog = false;
  protected readonly Permission = Permission;
  private readonly SORTABLE_FIELDS = ['title', 'description', 'url'];
  private currentSort: CurrentSort = { field: '', order: 'asc' };
  private readonly citizenServiceService = inject(CitizenServiceAdapter);
  private readonly router = inject(Router);
  private readonly sortService = inject(SortService);

  ngOnInit() {
    this.loadData(0, this.rows);
  }

  get cols() {
    return this.sortService.getColumns(this.baseCols, this.SORTABLE_FIELDS);
  }

  loadData(
    page: number,
    rows: number,
    searchTerm?: string,
    sortField?: string,
    sortOrder?: string
  ) {
    if (searchTerm) {
      this.citizenServiceService
        .getBySearchTerm(searchTerm, page + 1, rows, sortField, sortOrder)
        .pipe(take(1))
        .subscribe((data) => {
          this.visibleCitizenServices = data.items;
          this.totalRecords = data.total;
        });
    } else {
      this.citizenServiceService
        .getCitizenServices(page, rows, {
          visible: this.getVisibilityFilter(),
          favorite: this.getFavoriteFilter(),
          withoutTag: undefined,
          onlyExternal: this.getSourceTypeFilter(),
          sortField,
          sortOrder,
        })
        .pipe(take(1))
        .subscribe((data) => {
          this.visibleCitizenServices = data.content;
          this.totalRecords = data.totalRecords;
        });
    }
  }

  onPage(event: { first: number; rows: number }) {
    const page = event.first / event.rows;
    this.loadData(
      page,
      event.rows,
      this.currentSearchTerm,
      this.currentSort?.field,
      this.currentSort?.order
    );
  }

  onSearch(searchQuery: string) {
    this.currentSearchTerm = searchQuery;

    this.loadData(0, this.rows, searchQuery);
  }

  onRowClick(rowData: CitizenService) {
    this.router.navigate(['/citizen-services/details', rowData.id]);
  }

  handleFilterChange(filter: string | null) {
    this.selectedFilter = filter;
    this.loadData(0, this.rows, this.currentSearchTerm);
  }

  showDialog() {
    this.displayDialog = true;
  }

  hideDialog() {
    this.displayDialog = false;
    this.loadData(0, this.rows);
  }

  private getVisibilityFilter(): boolean | null {
    if (this.selectedFilter === 'visible') {
      return true;
    } else if (this.selectedFilter === 'hidden') {
      return false;
    }
    return null;
  }

  private getFavoriteFilter(): boolean | null {
    if (this.selectedFilter === 'favorite') {
      return true;
    }
    return null;
  }

  private getSourceTypeFilter(): 'EXTERNAL' | 'INTERNAL' | null {
    if (this.selectedFilter === 'created') {
      return 'INTERNAL';
    } else if (this.selectedFilter === 'external') {
      return 'EXTERNAL';
    }
    return null;
  }

  onSort(sortEvent: CurrentSort) {
    this.currentSort = this.sortService.handleSort(sortEvent);
    this.loadData(
      0,
      this.rows,
      this.currentSearchTerm,
      this.currentSort.field,
      this.currentSort.order
    );
  }
}
