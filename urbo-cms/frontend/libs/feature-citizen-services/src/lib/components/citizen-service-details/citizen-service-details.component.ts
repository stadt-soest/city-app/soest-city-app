import { Component, OnInit } from '@angular/core';
import { concatMap, take } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { ConfirmationService, MessageService } from 'primeng/api';
import { CitizenService } from '../../models/citizen-service.model';
import { CitizenServiceAdapter } from '../../citizen-service.adapter';

@Component({
  selector: 'urbo-cms-citizen-services-details',
  templateUrl: './citizen-service-details.component.html',
  standalone: false,
})
export class CitizenServiceDetailsComponent implements OnInit {
  citizenService: CitizenService | null = null;
  editableFields: string[] = [];

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly citizenServiceService: CitizenServiceAdapter,
    private readonly confirmationService: ConfirmationService,
    private readonly messageService: MessageService
  ) {}

  get citizenServiceId(): string {
    return this.route.snapshot.paramMap.get('id') as string;
  }

  ngOnInit(): void {
    this.loadCitizenService();
  }

  onSubmit(formGroup: FormGroup): void {
    if (this.citizenService) {
      if (this.citizenService.editable) {
        this.updateAllFields(formGroup);
      } else {
        const { visible, favorite } = formGroup.value;
        this.updateVisibilityAndFavorite(visible, favorite);
      }
    }
  }

  confirmDelete() {
    this.confirmationService.confirm({
      message: 'Sind Sie sicher, dass Sie diesen Eintrag löschen möchten?',
      acceptLabel: 'Ja',
      rejectLabel: 'Nein',
      accept: () => this.deleteCitizenService(),
    });
  }

  private loadCitizenService() {
    this.citizenServiceService
      .getCitizenService(this.citizenServiceId)
      .pipe(take(1))
      .subscribe({
        next: (citizenService) => {
          this.citizenService = citizenService;
          this.setEditableFields();
        },
        error: () => {
          this.showErrorAndNavigateBack(
            'Fehler beim Laden des Bürgerservices.'
          );
        },
      });
  }

  private updateAllFields(formGroup: FormGroup) {
    const dto = formGroup.value;
    if (this.citizenService?.id) {
      this.citizenServiceService
        .updateCitizenService(this.citizenService.id, dto)
        .pipe(take(1))
        .subscribe({
          next: () => this.navigateToCitizenServiceDashboard(),
          error: () =>
            this.showErrorAndNavigateBack(
              'Fehler beim Aktualisieren des Bürgerservices.'
            ),
        });
    }
  }

  private updateVisibilityAndFavorite(visible: boolean, favorite: boolean) {
    const citizenService = this.citizenService;

    if (citizenService?.id) {
      this.citizenServiceService
        .updateVisibility(citizenService.id, visible)
        .pipe(
          concatMap(() =>
            this.citizenServiceService.updateFavorite(
              citizenService.id,
              favorite
            )
          ),
          take(1)
        )
        .subscribe({
          next: () => this.navigateToCitizenServiceDashboard(),
          error: () =>
            this.showErrorAndNavigateBack(
              'Fehler beim Aktualisieren der Sichtbarkeit oder Favoritenstatus.'
            ),
        });
    }
  }

  private setEditableFields() {
    if (this.citizenService?.editable) {
      this.editableFields = [
        'title',
        'description',
        'url',
        'visible',
        'favorite',
      ];
    } else {
      this.editableFields = ['visible', 'favorite'];
    }
  }

  private deleteCitizenService() {
    if (this.citizenService) {
      this.citizenServiceService
        .deleteCitizenService(this.citizenServiceId)
        .pipe(take(1))
        .subscribe({
          next: () => this.navigateToCitizenServiceDashboard(),
          error: () =>
            this.showErrorAndNavigateBack(
              'Fehler beim Löschen des Bürgerservices.'
            ),
        });
    }
  }

  private showErrorAndNavigateBack(message: string) {
    this.messageService.add({
      severity: 'error',
      summary: 'Fehler',
      detail: message,
    });
    this.navigateToCitizenServiceDashboard();
  }

  private navigateToCitizenServiceDashboard() {
    this.router.navigate(['/citizen-services']);
  }
}
