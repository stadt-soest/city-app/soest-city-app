import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CitizenService } from '../../../models/citizen-service.model';

@Component({
  selector: 'urbo-cms-citizen-services-form',
  templateUrl: './citizen-service-form.component.html',
  standalone: false,
})
export class CitizenServiceFormComponent implements OnChanges {
  @Input({ required: true }) citizenService?: CitizenService | null;
  @Input() editableFields: string[] = [];
  @Output() submitForm = new EventEmitter<FormGroup>();
  @Output() deleteForm = new EventEmitter<void>();

  formGroup!: FormGroup;

  constructor(private readonly fb: FormBuilder) {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes['citizenService']?.currentValue) {
      if (!this.formGroup) {
        this.initializeForm();
      }
      this.populateFormWithServiceData();
    }
  }

  initializeForm() {
    this.formGroup = this.fb.group({
      title: [
        { value: '', disabled: !this.isFieldEditable('title') },
        Validators.required,
      ],
      description: [
        { value: '', disabled: !this.isFieldEditable('description') },
        Validators.required,
      ],
      url: [
        {
          value: '',
          disabled: !this.isFieldEditable('url'),
        },
        [Validators.required, Validators.pattern(/^(http|https):\/\/[^ "]+$/)],
      ],
      visible: [{ value: true, disabled: !this.isFieldEditable('visible') }],
      favorite: [{ value: false, disabled: !this.isFieldEditable('favorite') }],
    });
  }

  populateFormWithServiceData() {
    if (this.citizenService) {
      this.formGroup.patchValue({
        title: this.citizenService.title,
        description: this.citizenService.description,
        url: this.citizenService.url,
        visible: this.citizenService.visibility,
        favorite: this.citizenService.favorite,
      });
    }
  }

  onSubmit() {
    if (this.formGroup.valid) {
      this.submitForm.emit(this.formGroup);
    } else {
      this.formGroup.markAllAsTouched();
    }
  }

  onDelete() {
    this.deleteForm.emit();
  }

  private isFieldEditable(field: string): boolean {
    if (this.citizenService?.editable) {
      return true;
    }
    return this.editableFields.includes(field);
  }
}
