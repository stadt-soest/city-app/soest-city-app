export { FeaturePoisModule } from './lib/feature-pois.module';
export { PoiAdapter } from './lib/poi.adapter';
export * from './lib/models/poi.model';
export * from './lib/models/poi-categories.model';
