import {
  ModuleWithProviders,
  NgModule,
  inject,
  provideAppInitializer,
} from '@angular/core';
import {
  CoreModule,
  FeatureModule,
  MenuItem,
  ModuleCategory,
  ModuleInfo,
  ModuleRegistryService,
  Permission,
} from '@sw-code/urbo-cms-core';
import { PoisComponent } from './components/pois/pois.component';
import { PoiDetailsComponent } from './components/pois/poi-details/poi-details.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { UnmappedCategoriesComponent } from './components/categories/unmapped-categories/unmapped-categories.component';
import { NewCategoryComponent } from './components/categories/new-category/new-category.component';
import { CategoryGroupsComponent } from './components/categories/category-groups/category-groups.component';
import { UngroupedCategoriesComponent } from './components/categories/category-groups/sub-components/ungrouped-categories/ungrouped-categories.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { DatePipe } from '@angular/common';
import { ConfirmationService } from 'primeng/api';
import { Accordion } from 'primeng/accordion';
import { take } from 'rxjs/operators';
import { PoiAdapter } from './poi.adapter';
import { POI_MODULE_CONFIG, PoiModuleConfig } from './poi-module.config';
import { UIModule } from '@sw-code/urbo-cms-ui';
import { MapComponent, MarkerComponent } from '@maplibre/ngx-maplibre-gl';

const MODULES = [CoreModule, UIModule];
const COMPONENTS = [
  PoisComponent,
  PoiDetailsComponent,
  CategoriesComponent,
  UnmappedCategoriesComponent,
  NewCategoryComponent,
  CategoryGroupsComponent,
  UngroupedCategoriesComponent,
  FileUploadComponent,
];
const STANDALONE_COMPONENTS = [MapComponent, MarkerComponent];
const PROVIDERS = [DatePipe, ConfirmationService, Accordion];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES, STANDALONE_COMPONENTS],
  exports: [...MODULES, STANDALONE_COMPONENTS],
  providers: [...PROVIDERS],
})
export class FeaturePoisModule {
  readonly featureModule: FeatureModule;
  moduleName = 'POIs';

  constructor(private readonly poiService: PoiAdapter) {
    this.featureModule = {
      info: new ModuleInfo(
        this.moduleName,
        ModuleCategory.MODULES,
        'pi pi-map',
        'pois',
        [
          new MenuItem(this.moduleName, 'pi pi-table', 'pois'),
          new MenuItem(
            'Kategorien',
            'pi pi-folder-open',
            'pois/categories',
            [],
            [Permission.CategoryRead]
          ),
          new MenuItem(
            'Gruppen',
            'pi pi-folder-open',
            'pois/groups',
            [],
            [Permission.CategoryRead]
          ),
        ]
      ),
      routes: [
        {
          path: 'pois',
          loadChildren: () =>
            import('./components/routing.module').then((m) => m.RoutingModule),
        },
      ],
      dashboardData: {
        moduleName: this.moduleName,
        entityCount: 0,
      },
    };
    this.poiService
      .getPois(0, 10)
      .pipe(take(1))
      .subscribe((response) => {
        this.featureModule.dashboardData!.entityCount = response.totalRecords;
      });
  }

  static forRoot(
    config: PoiModuleConfig
  ): ModuleWithProviders<FeaturePoisModule> {
    return {
      ngModule: FeaturePoisModule,
      providers: [
        { provide: POI_MODULE_CONFIG, useValue: config },
        provideAppInitializer(() => {
          const initializerFn = (
            (registry: ModuleRegistryService, module: FeaturePoisModule) =>
            () => {
              registry.registerFeature(module.featureModule);
            }
          )(inject(ModuleRegistryService), inject(FeaturePoisModule));
          return initializerFn();
        }),
      ],
    };
  }
}
