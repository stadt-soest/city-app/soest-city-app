import { InjectionToken } from '@angular/core';

interface ThemeHexColors {
  lightThemeHexColor: string;
  darkThemeHexColor: string;
}

interface ColorPair {
  label: string;
  value: ThemeHexColors;
}

export interface PoiModuleConfig {
  apiBaseUrl: string;
  colorPairs: ColorPair[];
}

export const POI_MODULE_CONFIG = new InjectionToken<PoiModuleConfig>(
  'PoiModuleConfig'
);
