import { I18nString } from '@sw-code/urbo-cms-core';

export class PoiCategory {
  id?: string;
  title: I18nString;
  sourceCategories: string[];
  filename?: string;
  groupCategoryId?: string;
  sortOrder?: number;
  colors?: ThemeColors;

  constructor(
    title?: I18nString,
    sourceCategories?: string[],
    id?: string,
    groupCategoryId?: string,
    sortOrder?: number,
    hexColor?: ThemeColors,
    filename?: string
  ) {
    this.title = title ?? new I18nString('', '');
    this.sourceCategories = sourceCategories ?? [];
    this.id = id;
    this.groupCategoryId = groupCategoryId;
    this.sortOrder = sortOrder;
    this.colors = hexColor;
    this.filename = filename;
  }
}

export interface ThemeColors {
  lightThemeHexColor?: string;
  darkThemeHexColor?: string;
}

export class PoiGroupCategory {
  id?: string;
  title: I18nString;
  sortOrder?: number;

  constructor(id?: string, title?: I18nString, sortOrder?: number) {
    this.id = id;
    this.title = title ?? new I18nString('', '');
    this.sortOrder = sortOrder;
  }
}

export class MultiSelectPoiCategory {
  label: string;
  value: string;

  constructor(label: string, value: string) {
    this.label = label;
    this.value = value;
  }
}

export interface Icon {
  key: string;
}
