export interface Poi {
  id: string;
  name: string;
  location: Location;
  contactPoint: ContactPoint;
  dateModified: Date | null;
  url?: string;
  categories: string[];
}

export interface Location {
  street?: string;
  postalCode?: string;
  city?: string;
  coordinates: Coordinates;
}

export interface ContactPoint {
  telephone: string;
  email: string;
  contactPerson: string;
}

export interface Coordinates {
  lat: number | null;
  long: number | null;
}
