import { Observable } from 'rxjs';
import { Poi } from './models/poi.model';
import { PoiCategory, PoiGroupCategory } from './models/poi-categories.model';

export abstract class PoiAdapter {
  abstract getPois(
    page: number,
    rows: number
  ): Observable<{ content: Poi[]; totalRecords: number }>;

  abstract getPoi(id: string): Observable<Poi>;

  abstract searchPois(
    searchTerm: string,
    page?: number,
    size?: number
  ): Observable<{ items: Poi[]; total: number }>;

  abstract getCategories(): Observable<PoiCategory[]>;

  abstract getCategoryGroups(): Observable<PoiGroupCategory[]>;

  abstract createCategory(category: PoiCategory): Observable<PoiCategory>;

  abstract createCategoryGroup(
    categoryGroup: PoiGroupCategory
  ): Observable<PoiGroupCategory>;

  abstract updateCategoryGroup(
    categoryGroup: PoiGroupCategory
  ): Observable<PoiGroupCategory>;

  abstract deleteCategoryGroup(categoryGroupId: string): Observable<void>;

  abstract getSourceCategories(): Observable<string[]>;

  abstract getUnmappedSourceCategories(): Observable<string[]>;

  abstract updateCategory(category: PoiCategory): Observable<PoiCategory>;

  abstract deleteCategory(category: PoiCategory): Observable<void>;

  abstract updateCategoryOrder(
    categories: { id: string; order: number }[]
  ): Observable<void>;

  abstract updateCategoryGroupOrder(
    groupCategories: { id: string; order: number }[]
  ): Observable<void>;

  abstract uploadFile(file: Blob): Observable<string>;
}
