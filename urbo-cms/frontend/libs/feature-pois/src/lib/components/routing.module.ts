import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PoisComponent } from './pois/pois.component';
import { CategoriesComponent } from './categories/categories.component';
import { CategoryGroupsComponent } from './categories/category-groups/category-groups.component';
import { PoiDetailsComponent } from './pois/poi-details/poi-details.component';

const routes: Routes = [
  {
    path: '',
    component: PoisComponent,
  },
  {
    path: 'categories',
    component: CategoriesComponent,
  },
  {
    path: 'groups',
    component: CategoryGroupsComponent,
  },
  {
    path: ':id',
    component: PoiDetailsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {}
