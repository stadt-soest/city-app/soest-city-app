import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  Output,
} from '@angular/core';
import { catchError, take } from 'rxjs';
import {
  PoiCategory,
  PoiGroupCategory,
} from '../../../models/poi-categories.model';
import { PoiAdapter } from '../../../poi.adapter';

@Component({
  selector: 'urbo-cms-pois-poi-new-category',
  templateUrl: './new-category.component.html',
  standalone: false,
})
export class NewCategoryComponent implements OnChanges {
  @Output() created: EventEmitter<void> = new EventEmitter<void>();
  @Output() updated: EventEmitter<void> = new EventEmitter<void>();
  @Input() categoryToEdit: PoiCategory | null = null;
  @Input() groupCategoryToEdit: PoiGroupCategory | null = null;
  @Input() activeTabIndex = 0;

  currentCategory: PoiCategory = new PoiCategory();
  currentGroupCategory: PoiGroupCategory = new PoiGroupCategory();
  displayDialog = false;

  constructor(private readonly poiCategoryService: PoiAdapter) {}

  @HostListener('document:keydown.escape', ['$event'])
  handleEscape() {
    this.resetDialog();
  }

  ngOnChanges() {
    if (this.categoryToEdit) {
      this.currentCategory = { ...this.categoryToEdit };
      this.activeTabIndex = 0;
      this.displayDialog = true;
    } else if (this.groupCategoryToEdit) {
      this.currentGroupCategory = { ...this.groupCategoryToEdit };
      this.activeTabIndex = 1;
      this.displayDialog = true;
    }
  }

  onTabChange(event: any) {
    if (!this.isEditing()) {
      this.activeTabIndex = event.index;
    }
  }

  isEditing(): boolean {
    return !!(this.categoryToEdit || this.groupCategoryToEdit);
  }

  createOrUpdate() {
    if (this.activeTabIndex === 0) {
      if (this.currentCategory.id) {
        this.updateCategory();
      } else {
        this.createCategory();
      }
    } else if (this.currentGroupCategory.id) {
      this.updateGroupCategory();
    } else {
      this.createGroupCategory();
    }
  }

  private createCategory() {
    this.poiCategoryService
      .createCategory(this.currentCategory)
      .pipe(
        take(1),
        catchError((err) => {
          return err;
        })
      )
      .subscribe(() => {
        this.created.emit();
        this.resetDialog();
      });
  }

  private updateCategory() {
    this.poiCategoryService
      .updateCategory(this.currentCategory)
      .pipe(
        take(1),
        catchError((err) => {
          return err;
        })
      )
      .subscribe(() => {
        this.updated.emit();
        this.resetDialog();
      });
  }

  private createGroupCategory() {
    this.poiCategoryService
      .createCategoryGroup(this.currentGroupCategory)
      .pipe(
        take(1),
        catchError((err) => {
          return err;
        })
      )
      .subscribe(() => {
        this.created.emit();
        this.resetDialog();
      });
  }

  private updateGroupCategory() {
    this.poiCategoryService
      .updateCategoryGroup(this.currentGroupCategory)
      .pipe(
        take(1),
        catchError((err) => {
          return err;
        })
      )
      .subscribe(() => {
        this.updated.emit();
        this.resetDialog();
      });
  }

  showDialog() {
    this.displayDialog = true;
  }

  hideDialog() {
    this.resetDialog();
  }

  private resetDialog() {
    this.displayDialog = false;
    this.currentCategory = new PoiCategory();
    this.categoryToEdit = null;
    this.groupCategoryToEdit = null;
    this.currentGroupCategory = new PoiGroupCategory();
  }
}
