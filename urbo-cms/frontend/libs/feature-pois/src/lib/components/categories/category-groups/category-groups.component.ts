import { Component, OnInit, ViewChild } from '@angular/core';
import { catchError, take } from 'rxjs';
import {
  PoiCategory,
  PoiGroupCategory,
} from '../../../models/poi-categories.model';
import { NewCategoryComponent } from '../new-category/new-category.component';
import { UngroupedCategoriesComponent } from './sub-components/ungrouped-categories/ungrouped-categories.component';
import { ConfirmationService } from 'primeng/api';
import { PoiAdapter } from '../../../poi.adapter';
import { Permission } from '@sw-code/urbo-cms-core';

@Component({
  selector: 'urbo-cms-pois-category-groups',
  templateUrl: './category-groups.component.html',
  styleUrls: ['./category-groups.component.scss'],
  standalone: false,
})
export class CategoryGroupsComponent implements OnInit {
  groupCategories: PoiGroupCategory[] = [];
  protected readonly Permission = Permission;
  categories: PoiCategory[] = [];
  groupedCategories: { [key: string]: PoiCategory[] } = {};
  categoryToEdit: PoiCategory | null = null;
  groupCategoryToEdit: PoiGroupCategory | null = null;
  @ViewChild(NewCategoryComponent)
  newCategoryGroupComponent?: NewCategoryComponent;
  @ViewChild(UngroupedCategoriesComponent)
  ungroupedCategoriesComponent?: UngroupedCategoriesComponent;

  draggedCategory: PoiCategory | null = null;
  draggedGroupId: string | null = null;

  constructor(
    private readonly poiCategoryService: PoiAdapter,
    private readonly confirmationService: ConfirmationService
  ) {}

  ngOnInit(): void {
    this.loadContent();
  }

  onDrop(event: DragEvent, groupId: string) {
    event.preventDefault();
    const data = event.dataTransfer?.getData('application/json');
    if (data) {
      const category: PoiCategory = JSON.parse(data);
      this.updateCategoryGroup(category, groupId);
      if (this.draggedGroupId) {
        this.updateCategoryOrder(this.draggedGroupId);
      }
      if (groupId !== this.draggedGroupId) {
        this.updateCategoryOrder(groupId);
      }
    }
  }

  allowDrop(event: DragEvent) {
    event.preventDefault();
  }

  loadContent() {
    this.loadGroupCategories();
    this.loadCategories();
  }

  loadGroupCategories() {
    this.poiCategoryService
      .getCategoryGroups()
      .pipe(take(1))
      .subscribe((groupCategories: PoiGroupCategory[]) => {
        this.groupCategories = groupCategories;
        this.groupCategoriesByGroup();
      });
  }

  loadCategories() {
    this.poiCategoryService
      .getCategories()
      .pipe(take(1))
      .subscribe((categories: PoiCategory[]) => {
        this.categories = categories;
        this.groupCategoriesByGroup();
      });
  }

  groupCategoriesByGroup() {
    this.groupedCategories = this.groupCategories.reduce((acc, group) => {
      if (group.id) {
        acc[group.id] = this.categories.filter(
          (category) => category.groupCategoryId === group.id
        );
      }
      return acc;
    }, {} as { [key: string]: PoiCategory[] });

    this.groupedCategories['ungrouped'] = this.categories.filter(
      (category) => !category.groupCategoryId
    );
  }

  moveGroupCategoryUp(event: Event, groupCategory: PoiGroupCategory) {
    event.stopPropagation();
    const index = this.groupCategories.indexOf(groupCategory);
    if (index > 0) {
      this.updateGroupCategoryOrder(index, index - 1);
    }
  }

  moveGroupCategoryDown(event: Event, groupCategory: PoiGroupCategory) {
    event.stopPropagation();
    const index = this.groupCategories.indexOf(groupCategory);
    if (index < this.groupCategories.length - 1) {
      this.updateGroupCategoryOrder(index, index + 1);
    }
  }

  updateGroupCategoryOrder(oldIndex: number, newIndex: number) {
    const groupCategoryToMove = this.groupCategories[oldIndex];
    if (!groupCategoryToMove?.id) {
      console.error('Invalid group category at the specified index.');
      return;
    }

    const newGroupCategories = [...this.groupCategories];
    newGroupCategories.splice(oldIndex, 1);
    newGroupCategories.splice(newIndex, 0, groupCategoryToMove);

    const updatedGroupOrder = newGroupCategories
      .map((groupCategory, index) => {
        if (!groupCategory.id) {
          console.error(`Group category at index ${index} is missing an ID.`);
          return null;
        }
        return {
          id: groupCategory.id,
          order: index,
        };
      })
      .filter((item): item is { id: string; order: number } => item !== null);

    this.poiCategoryService
      .updateCategoryGroupOrder(updatedGroupOrder)
      .pipe(take(1))
      .subscribe(() => {
        this.loadGroupCategories();
      });
  }

  confirmDeleteGroupCategory(event: Event, groupCategoryId: string) {
    event.stopPropagation();
    this.confirmationService.confirm({
      key: 'deleteGroupCategory',
      message:
        'Bist du dir sicher, dass du diese Gruppen-Kategorie löschen möchtest?',
      header: 'Bestätigung',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Ja',
      rejectLabel: 'Abbrechen',
      acceptButtonStyleClass: 'p-button-danger',
      rejectButtonStyleClass: 'p-button-default',
      accept: () => this.deleteGroupCategory(groupCategoryId),
    });
  }

  private deleteGroupCategory(groupCategoryId: string) {
    this.poiCategoryService
      .deleteCategoryGroup(groupCategoryId)
      .pipe(
        take(1),
        catchError((err) => {
          throw err;
        })
      )
      .subscribe(() => {
        this.loadGroupCategories();
      });
  }

  isFirstGroupCategory(groupCategory: PoiGroupCategory): boolean {
    return this.groupCategories.indexOf(groupCategory) === 0;
  }

  isLastGroupCategory(groupCategory: PoiGroupCategory): boolean {
    return (
      this.groupCategories.indexOf(groupCategory) ===
      this.groupCategories.length - 1
    );
  }

  editGroupCategory(event: Event, groupCategory: PoiGroupCategory) {
    event.stopPropagation();
    this.categoryToEdit = null;
    this.groupCategoryToEdit = groupCategory;
    this.newCategoryGroupComponent?.showDialog();
  }

  private updateCategoryGroup(category: PoiCategory, groupId: string) {
    category.groupCategoryId = groupId;
    this.poiCategoryService
      .updateCategory(category)
      .pipe(
        take(1),
        catchError((err) => {
          throw err;
        })
      )
      .subscribe(() => {
        this.ungroupedCategoriesComponent?.loadUngroupedCategories();
        this.loadCategories();
      });
  }

  showDialogForNewGroupCategory() {
    this.groupCategoryToEdit = null;
    this.categoryToEdit = null;
    this.newCategoryGroupComponent?.showDialog();
  }

  onDragStart(event: DragEvent, category: PoiCategory, groupId: string) {
    if (!event.dataTransfer) {
      console.error('Drag event does not have a dataTransfer object.');
      return;
    }

    this.draggedCategory = category;
    this.draggedGroupId = groupId;
    event.dataTransfer.setData('application/json', JSON.stringify(category));
    event.dataTransfer.dropEffect = 'move';
  }

  onDragEnd() {
    this.draggedCategory = null;
    this.draggedGroupId = null;
  }

  onDragOver(event: DragEvent, groupId: string, index: number) {
    event.preventDefault();
    if (this.draggedCategory && this.draggedGroupId === groupId) {
      const currentIndex = this.groupedCategories[groupId].indexOf(
        this.draggedCategory
      );
      if (currentIndex !== index) {
        this.groupedCategories[groupId].splice(currentIndex, 1);
        this.groupedCategories[groupId].splice(index, 0, this.draggedCategory);
      }
    }
  }

  private updateCategoryOrder(groupId: string) {
    const categories = this.groupedCategories[groupId];

    const updatedCategories = categories
      .map((category, index) => {
        if (!category.id) {
          console.error(`Category at index ${index} is missing an ID.`);
          return null;
        }
        return {
          id: category.id,
          order: index,
        };
      })
      .filter((item): item is { id: string; order: number } => item !== null);

    if (updatedCategories.length !== categories.length) {
      console.warn('Some categories were excluded due to missing IDs.');
    }

    this.poiCategoryService
      .updateCategoryOrder(updatedCategories)
      .pipe(take(1))
      .subscribe(() => {
        this.loadCategories();
      });
  }

  onCategoryUpdated() {
    this.loadCategories();
  }
}
