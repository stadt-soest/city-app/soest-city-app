import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { catchError, take } from 'rxjs';
import { PoiCategory } from '../../../../../models/poi-categories.model';
import { PoiAdapter } from '../../../../../poi.adapter';

@Component({
  selector: 'urbo-cms-pois-ungrouped-categories',
  templateUrl: './ungrouped-categories.component.html',
  standalone: false,
})
export class UngroupedCategoriesComponent implements OnInit {
  @Input() groupedCategories!: { [key: string]: PoiCategory[] };
  @Output() categoryUpdated = new EventEmitter<void>();
  ungroupedCategories: PoiCategory[] = [];

  constructor(private readonly poiCategoryService: PoiAdapter) {}

  ngOnInit(): void {
    this.loadUngroupedCategories();
  }

  onDragStart(event: DragEvent, category: PoiCategory) {
    event.dataTransfer?.setData('application/json', JSON.stringify(category));
  }

  onDrop(event: DragEvent) {
    event.preventDefault();
    const data = event.dataTransfer?.getData('application/json');
    if (data) {
      const category: PoiCategory = JSON.parse(data);
      this.updateCategoryGroup(category, null);
    }
  }

  allowDrop(event: DragEvent) {
    event.preventDefault();
  }

  loadUngroupedCategories() {
    this.poiCategoryService
      .getCategories()
      .pipe(
        take(1),
        catchError((err) => {
          throw err;
        })
      )
      .subscribe((categories: PoiCategory[]) => {
        this.ungroupedCategories = categories.filter(
          (category) => !category.groupCategoryId
        );
      });
  }

  private updateCategoryGroup(category: PoiCategory, groupId: string | null) {
    category.groupCategoryId = groupId!;

    this.poiCategoryService
      .updateCategory(category)
      .pipe(
        take(1),
        catchError((err) => {
          throw err;
        })
      )
      .subscribe(() => {
        this.loadUngroupedCategories();
        this.categoryUpdated.emit();
      });
  }
}
