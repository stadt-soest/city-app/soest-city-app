import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { catchError, finalize, take, tap, throwError } from 'rxjs';
import { ConfirmationService, MessageService } from 'primeng/api';
import {
  MultiSelectPoiCategory,
  PoiCategory,
  PoiGroupCategory,
} from '../../models/poi-categories.model';
import { NewCategoryComponent } from './new-category/new-category.component';
import { PoiAdapter } from '../../poi.adapter';
import { Permission } from '@sw-code/urbo-cms-core';
import { POI_MODULE_CONFIG, PoiModuleConfig } from '../../poi-module.config';

@Component({
  selector: 'urbo-cms-pois-poi-categories',
  templateUrl: './categories.component.html',
  providers: [MessageService, ConfirmationService],
  standalone: false,
})
export class CategoriesComponent implements OnInit {
  categories: PoiCategory[] = [];
  protected readonly Permission = Permission;
  unmappedSourceCategories: string[] = [];
  allSourceCategories: MultiSelectPoiCategory[] = [];
  groupCategoryOptions: any[] = [];
  groupCategories: PoiGroupCategory[] = [];
  groupedCategories: { [key: string]: PoiCategory[] } = {};
  loading = false;
  categoryToEdit: PoiCategory | null = null;
  groupCategoryToEdit: PoiGroupCategory | null = null;
  selectedFile: File | null = null;
  @ViewChild(NewCategoryComponent)
  newCategoryGroupComponent?: NewCategoryComponent;

  constructor(
    private readonly poiCategoryService: PoiAdapter,
    private readonly confirmationService: ConfirmationService,
    private readonly messageService: MessageService,
    @Inject(POI_MODULE_CONFIG) protected config: PoiModuleConfig
  ) {}

  ngOnInit(): void {
    this.loadContent();
  }

  showDialog(type: 'category' | 'groupCategory') {
    if (type === 'category') {
      this.categoryToEdit = null;
      this.groupCategoryToEdit = null;
    } else {
      this.groupCategoryToEdit = null;
      this.categoryToEdit = null;
    }
    this.newCategoryGroupComponent?.showDialog();
  }

  loadContent() {
    this.loadCategories();
    this.loadUnmappedCategories();
    this.loadAllSourceCategories();
    this.loadGroupCategories();
  }

  updateCategory(category: PoiCategory) {
    if (this.selectedFile) {
      this.loading = true;

      this.poiCategoryService
        .uploadFile(this.selectedFile)
        .pipe(
          tap((filename) => {
            category.filename = filename;
            this.updateCategoryData(category);
          }),
          catchError((error) => {
            this.loading = false;
            this.messageService.add({
              severity: 'error',
              summary: 'Error',
              detail: 'File upload failed',
            });
            return throwError(() => error);
          }),
          finalize(() => {
            this.loading = false;
          })
        )
        .subscribe();
    } else {
      this.updateCategoryData(category);
    }
  }

  updateCategoryData(category: PoiCategory) {
    this.poiCategoryService
      .updateCategory(category)
      .pipe(
        take(1),
        catchError((err) => {
          this.loading = false;
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Failed to update category',
          });
          throw err;
        })
      )
      .subscribe(() => {
        this.loadContent();
        this.loading = false;
      });
  }

  handleFileSelected(file: File) {
    this.selectedFile = file;
  }

  confirmDeleteCategory(category: PoiCategory) {
    this.confirmationService.confirm({
      message: 'Bist du dir sicher, dass du diese Kategorie löschen möchtest?',
      header: 'Bestätigung',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Ja',
      rejectLabel: 'Abbrechen',
      acceptButtonStyleClass: 'p-button-danger',
      rejectButtonStyleClass: 'p-button-default',
      accept: () => this.deleteCategory(category),
    });
  }

  private loadCategories() {
    this.poiCategoryService
      .getCategories()
      .pipe(take(1))
      .subscribe((categories) => {
        this.categories = categories;
        this.groupCategoriesByGroup();
      });
  }

  private loadUnmappedCategories() {
    this.poiCategoryService
      .getUnmappedSourceCategories()
      .pipe(take(1))
      .subscribe((unmappedSourceCategories) => {
        this.unmappedSourceCategories = unmappedSourceCategories;
      });
  }

  private loadGroupCategories() {
    this.poiCategoryService
      .getCategoryGroups()
      .pipe(take(1))
      .subscribe((groupCategories) => {
        this.groupCategories = groupCategories;
        this.groupCategoryOptions = groupCategories.map((groupCategory) => ({
          label: groupCategory.title.de,
          value: groupCategory.id,
        }));
        this.groupCategoriesByGroup();
      });
  }

  private deleteCategory(category: PoiCategory) {
    this.loading = true;
    this.poiCategoryService
      .deleteCategory(category)
      .pipe(
        take(1),
        catchError((err) => {
          this.loading = false;
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Failed to delete category',
          });
          throw err;
        })
      )
      .subscribe(() => {
        this.loadContent();
        this.loading = false;
        this.messageService.add({
          severity: 'success',
          summary: 'Success',
          detail: 'Category deleted successfully',
        });
      });
  }

  private loadAllSourceCategories() {
    this.poiCategoryService
      .getSourceCategories()
      .pipe(take(1))
      .subscribe((allSourceCategories) => {
        this.allSourceCategories = allSourceCategories.map(
          (category) => new MultiSelectPoiCategory(category, category)
        );
      });
  }

  private groupCategoriesByGroup() {
    this.groupedCategories = this.groupCategories.reduce((acc, group) => {
      if (group.id) {
        acc[group.id] = this.categories.filter(
          (category) => category.groupCategoryId === group.id
        );
      }
      return acc;
    }, {} as { [key: string]: PoiCategory[] });

    this.groupedCategories['ungrouped'] = this.categories.filter(
      (category) => !category.groupCategoryId
    );
  }
}
