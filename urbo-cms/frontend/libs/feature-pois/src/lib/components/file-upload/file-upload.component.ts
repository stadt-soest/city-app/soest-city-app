import { Component, EventEmitter, inject, Input, Output } from '@angular/core';
import { POI_MODULE_CONFIG } from '../../poi-module.config';
import { FileSelectEvent } from 'primeng/fileupload';

@Component({
  selector: 'urbo-cms-pois-file-upload',
  templateUrl: './file-upload.component.html',
  standalone: false,
})
export class FileUploadComponent {
  @Input() currentFilename?: string | null = null;
  @Output() fileSelected = new EventEmitter<File>();
  previewUrl: string | null = null;

  protected readonly config = inject(POI_MODULE_CONFIG);

  onFileSelect(event: FileSelectEvent, fileUpload: { clear: () => void }) {
    const file: File = event.files[0];
    if (file && file.type === 'image/svg+xml') {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent<FileReader>) => {
        this.previewUrl = e.target?.result as string | null;
      };
      reader.readAsDataURL(file);
      this.fileSelected.emit(file);
      this.currentFilename = null;
      fileUpload.clear();
    }
  }
}
