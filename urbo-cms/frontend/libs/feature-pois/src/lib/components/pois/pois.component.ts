import { Component, DestroyRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { Poi } from '../../models/poi.model';
import { PoiAdapter } from '../../poi.adapter';

@Component({
  selector: 'urbo-cms-pois',
  templateUrl: './pois.component.html',
  standalone: false,
})
export class PoisComponent implements OnInit {
  readonly rows: number = 10;
  pois: Poi[] = [];
  cols: any[] = [
    { field: 'name', header: 'Name', type: 'text' },
    { field: 'location.street', header: 'Street', type: 'text' },
    { field: 'location.postalCode', header: 'PLZ', type: 'text' },
    { field: 'location.city', header: 'Stadt', type: 'text' },
    { field: 'location.coordinates.lat', header: 'Breitengrad', type: 'text' },
    { field: 'location.coordinates.long', header: 'Längengrad', type: 'text' },
  ];
  totalRecords = 0;
  currentSearchTerm = '';

  constructor(
    private readonly poiService: PoiAdapter,
    private readonly router: Router,
    private readonly destroyRef: DestroyRef
  ) {}

  ngOnInit() {
    this.loadData(0, this.rows);
  }

  loadData(page: number, rows: number, searchTerm?: string) {
    if (searchTerm) {
      this.poiService
        .searchPois(searchTerm, page + 1, rows)
        .subscribe((data) => {
          this.pois = data.items;
          this.totalRecords = data.total;
        });
    } else {
      this.poiService
        .getPois(page, rows)
        .pipe(take(1), takeUntilDestroyed(this.destroyRef))
        .subscribe((data) => {
          this.pois = data.content;
          this.totalRecords = data.totalRecords;
        });
    }
  }

  onPage(event: any) {
    const page = event.first / event.rows;
    this.loadData(page, event.rows, this.currentSearchTerm);
  }

  onSearch(event: any) {
    const searchQuery: string = event;
    this.currentSearchTerm = searchQuery;

    if (searchQuery) {
      this.loadData(0, this.rows, searchQuery);
    } else {
      this.loadData(0, this.rows);
    }
  }

  showPoiDetails(clickedRowData: any) {
    this.router.navigate(['/pois', clickedRowData['id']]);
  }
}
