import {
  Component,
  DestroyRef,
  inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { take } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ActivatedRoute } from '@angular/router';
import { Location, Poi } from '../../../models/poi.model';
import { PoiAdapter } from '../../../poi.adapter';
import * as maplibregl from 'maplibre-gl';
import { MAP_STYLE_URL, MAP_ZOOM } from '@sw-code/urbo-cms-core';

@Component({
  selector: 'urbo-cms-pois-poi-details',
  styleUrl: 'poi-details.component.scss',
  templateUrl: './poi-details.component.html',
  standalone: false,
})
export class PoiDetailsComponent implements OnInit, OnDestroy {
  @ViewChild('map', { static: false }) map?: maplibregl.Map;
  protected readonly mapStyleUrl = inject(MAP_STYLE_URL);
  protected readonly mapZoom = inject(MAP_ZOOM);

  poi?: Poi;
  markerCoordinates: [number, number] = [0, 0];

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly poiService: PoiAdapter,
    private readonly destroyRef: DestroyRef
  ) {}

  ngOnInit() {
    this.loadPoi();
  }

  loadPoi() {
    this.poiService
      .getPoi(this.activatedRoute.snapshot.params['id'])
      .pipe(take(1), takeUntilDestroyed(this.destroyRef))
      .subscribe((poi: Poi) => {
        this.poi = poi;
        if (poi.location.coordinates.lat && poi.location.coordinates.long) {
          this.markerCoordinates = [
            poi.location.coordinates.long,
            poi.location.coordinates.lat,
          ];
        }
      });
  }

  ngOnDestroy() {
    if (this.map) {
      this.map.remove();
    }
  }

  generateGoogleMapsUrl(name: string, location: Location) {
    return `https://maps.google.de/maps?q=${location.coordinates?.lat},${location.coordinates?.long}(${name})`;
  }
}
