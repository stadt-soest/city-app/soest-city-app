import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PoisComponent } from './pois.component';
import { of } from 'rxjs';
import { TableModule } from 'primeng/table';
import { TableComponent } from '@sw-code/urbo-cms-ui';
import { ImagePipe } from '@sw-code/urbo-cms-core';
import { DatePipe } from '@angular/common';
import { PoiAdapter } from '../../poi.adapter';

describe('PoisComponent', () => {
  let component: PoisComponent;
  let fixture: ComponentFixture<PoisComponent>;
  let mockPoiService: any;

  beforeEach(async () => {
    mockPoiService = {
      getPois: jest.fn().mockReturnValue(
        of({
          content: [],
          totalRecords: 0,
        })
      ),
    };

    await TestBed.configureTestingModule({
      imports: [TableModule],
      declarations: [PoisComponent, TableComponent, ImagePipe],
      providers: [
        { provide: PoiAdapter, useValue: mockPoiService },
        DatePipe,
        ImagePipe,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(PoisComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should initialize cols and invoke loadData', () => {
      jest.spyOn(component, 'loadData');
      fixture.detectChanges();

      const expectedCols = [
        { field: 'name', header: 'Name', type: 'text' },
        { field: 'location.street', header: 'Street', type: 'text' },
        { field: 'location.postalCode', header: 'PLZ', type: 'text' },
        { field: 'location.city', header: 'Stadt', type: 'text' },
        {
          field: 'location.coordinates.lat',
          header: 'Breitengrad',
          type: 'text',
        },
        {
          field: 'location.coordinates.long',
          header: 'Längengrad',
          type: 'text',
        },
      ];
      expect(component.cols).toEqual(expectedCols);

      expect(component.loadData).toHaveBeenCalledWith(0, component.rows);
    });
  });

  describe('handlePageChange', () => {
    it('should correctly calculate page number and call loadData with appropriate arguments', () => {
      mockPoiService.getPois.mockReturnValue(
        of({
          content: [],
          totalRecords: 0,
        })
      );

      jest.spyOn(component, 'loadData');
      const mockPoi = {
        first: 10,
        rows: 5,
      };
      fixture.detectChanges();

      component.onPage(mockPoi);

      const expectedPage = mockPoi.first / mockPoi.rows;
      expect(component.loadData).toHaveBeenCalledWith(
        expectedPage,
        mockPoi.rows,
        ''
      );
    });
  });

  describe('Table rendering', () => {
    it('should render the Table', () => {
      fixture.detectChanges();
      const compiled = fixture.nativeElement;
      const tableElement = compiled.querySelector('p-table');
      expect(tableElement).toBeTruthy();
    });

    it('should render headers correctly', () => {
      fixture.detectChanges();
      const compiled = fixture.nativeElement;
      const headerCells = Array.from(
        compiled.querySelectorAll('th')
      ) as HTMLElement[];
      const headers = headerCells.map((cell) => cell.textContent!.trim());
      expect(headers.length).toBe(7);
      expect(headers).toEqual([
        'Name',
        'Street',
        'PLZ',
        'Stadt',
        'Breitengrad',
        'Längengrad',
        'Actions',
      ]);
    });
  });
});
