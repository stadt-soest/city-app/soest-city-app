import { Injectable } from '@angular/core';
import { Parking } from './models/parking.model';
import { Observable } from 'rxjs';
import {
  OpeningHoursMap,
  SpecialOpeningHoursMap,
} from '@sw-code/urbo-cms-core';

@Injectable()
export abstract class ParkingAdapter {
  abstract getParkingAreas(page?: number, size?: number): Observable<Parking[]>;

  abstract getParkingAreaById(id: string): Observable<Parking>;

  abstract updateRegularParkingOpeningTimes(
    parkingId: string,
    openingTimes: OpeningHoursMap
  ): Observable<Parking>;

  abstract updateSpecialParkingOpeningTimes(
    parkingId: string,
    openingTimes: SpecialOpeningHoursMap
  ): Observable<Parking>;
}
