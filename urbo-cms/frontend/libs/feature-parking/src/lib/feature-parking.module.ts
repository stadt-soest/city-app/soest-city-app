import {
  inject,
  ModuleWithProviders,
  NgModule,
  provideAppInitializer,
} from '@angular/core';
import {
  FeatureModule,
  MenuItem,
  ModuleCategory,
  ModuleInfo,
  ModuleRegistryService,
} from '@sw-code/urbo-cms-core';
import { UIModule } from '@sw-code/urbo-cms-ui';
import { ParkingAdapter } from './parking.adapter';
import { take } from 'rxjs';

const MODULES = [UIModule];

@NgModule({
  declarations: [],
  imports: [...MODULES],
  exports: [...MODULES],
})
export class FeatureParkingModule {
  readonly featureModule: FeatureModule;
  moduleName = 'Parkplätze';

  constructor(private readonly parkingAdapter: ParkingAdapter) {
    this.featureModule = {
      info: new ModuleInfo(
        this.moduleName,
        ModuleCategory.MODULES,
        'pi pi-car',
        'parking',
        [new MenuItem('Übersicht', 'pi pi-table', 'parking')]
      ),
      routes: [
        {
          path: 'parking',
          loadComponent: () =>
            import('./components/parking-dashboard.component').then(
              (m) => m.ParkingDashboardComponent
            ),
        },
        {
          path: 'parking/:id',
          loadComponent: () =>
            import(
              './components/parking-details/parking-details.component'
            ).then((m) => m.ParkingDetailsComponent),
        },
      ],
      dashboardData: {
        moduleName: this.moduleName,
        entityCount: 0,
      },
    };

    this.parkingAdapter
      .getParkingAreas()
      .pipe(take(1))
      .subscribe({
        next: (parkingAreas) => {
          if (this.featureModule.dashboardData && parkingAreas) {
            this.featureModule.dashboardData.entityCount = parkingAreas.length;
          }
        },
        error: (error) => {
          console.error('Error fetching parking count', error);
        },
      });
  }

  static forRoot(): ModuleWithProviders<FeatureParkingModule> {
    return {
      ngModule: FeatureParkingModule,
      providers: [
        provideAppInitializer(() => {
          const initializerFn = (
            (registry: ModuleRegistryService, module: FeatureParkingModule) =>
            () => {
              registry.registerFeature(module.featureModule);
            }
          )(inject(ModuleRegistryService), inject(FeatureParkingModule));
          return initializerFn();
        }),
      ],
    };
  }
}
