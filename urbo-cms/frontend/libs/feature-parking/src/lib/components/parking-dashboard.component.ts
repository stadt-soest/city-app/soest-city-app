import { Component, inject, OnInit } from '@angular/core';
import { UIModule } from '@sw-code/urbo-cms-ui';
import { take } from 'rxjs';
import { Parking } from '../models/parking.model';
import { ParkingAdapter } from '../parking.adapter';
import { Router } from '@angular/router';

@Component({
  templateUrl: './parking-dashboard.component.html',
  imports: [UIModule],
  standalone: true,
})
export class ParkingDashboardComponent implements OnInit {
  parkingAreas?: Parking[];
  rows = 10;

  baseCols = [
    { header: 'Name', field: 'displayName', type: 'string' },
    { header: 'Status', field: 'status', type: 'string' },
    { header: 'Kapazität', field: 'parkingCapacity.total', type: 'number' },
    { header: 'Verfügbar', field: 'parkingCapacity.remaining', type: 'number' },
    { header: 'Typ', field: 'parkingType', type: 'string' },
  ];

  private readonly parkingAdapter = inject(ParkingAdapter);
  private readonly router = inject(Router);

  ngOnInit() {
    this.loadData(0, this.rows);
  }

  loadData(page: number, rows: number) {
    this.parkingAdapter
      .getParkingAreas(page, rows)
      .pipe(take(1))
      .subscribe((parking) => {
        this.parkingAreas = parking;
      });
  }

  onPage(event: { first: number; rows: number }) {
    const page = event.first / event.rows;
    this.loadData(page, event.rows);
  }

  async showParkingDetails(clickedRowData: Parking) {
    await this.router.navigate(['/parking', clickedRowData['id']]);
  }
}
