import { Component, inject, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs';
import { Coordinates, Parking } from '../../models/parking.model';
import { ParkingAdapter } from '../../parking.adapter';
import { UIModule } from '@sw-code/urbo-cms-ui';
import {
  DayOfWeek,
  DEFAULT_OPENING_HOURS,
  formatDay,
  OpeningHoursMap,
  Permission,
  SpecialOpeningHoursMap,
} from '@sw-code/urbo-cms-core';

import { format, parse } from 'date-fns';
import { EditRegularParkingTimesComponent } from '../edit-regular-parking-times/edit-regular-parking-times.component';
import { EditSpecialOpeningTimesComponent } from '../edit-special-opening-times/edit-special-opening-times-component';

@Component({
  selector: 'urbo-cms-parking-details',
  styleUrl: 'parking-details.component.scss',
  templateUrl: './parking-details.component.html',
  imports: [
    UIModule,
    EditRegularParkingTimesComponent,
    EditSpecialOpeningTimesComponent,
  ],
  standalone: true,
})
export class ParkingDetailsComponent implements OnInit {
  parkingArea?: Parking;
  selectedFile?: File;
  displayDialog = false;
  openingHoursList: {
    day: string;
    from: string;
    to: string;
    isAllDay: boolean;
  }[] = [];
  specialOpeningHoursList: {
    date: string;
    from: string;
    to: string;
    isAllDay: boolean;
    isClosed: boolean;
  }[] = [];

  specialOpeningHoursMap: SpecialOpeningHoursMap = {};

  @ViewChild(EditRegularParkingTimesComponent)
  editParkingTimesComponent!: EditRegularParkingTimesComponent;

  @ViewChild(EditSpecialOpeningTimesComponent)
  editSpecialOpeningTimesComponent!: EditSpecialOpeningTimesComponent;

  protected readonly DEFAULT_OPENING_HOURS = DEFAULT_OPENING_HOURS;
  protected readonly Permission = Permission;

  private readonly activatedRoute = inject(ActivatedRoute);
  private readonly parkingAdapter = inject(ParkingAdapter);

  ngOnInit() {
    this.loadDetailsNews();
  }

  loadDetailsNews() {
    this.parkingAdapter
      .getParkingAreaById(this.activatedRoute.snapshot.params['id'])
      .pipe(take(1))
      .subscribe((parkingArea: Parking) => {
        this.parkingArea = parkingArea;
        console.log(this.parkingArea);
        this.openingHoursList = this.mapOpeningHours(parkingArea.openingHours);
        this.specialOpeningHoursMap = parkingArea.specialOpeningHours ?? {};
        this.specialOpeningHoursList = this.mapSpecialOpeningHours(
          this.specialOpeningHoursMap
        );
      });
  }

  generateGoogleMapsUrl(coordinates: Coordinates) {
    return `https://maps.google.de/maps?q=${coordinates.lat},${coordinates.long}(${this.parkingArea?.name})`;
  }

  private mapOpeningHours(
    openingHours: OpeningHoursMap
  ): { day: string; from: string; to: string; isAllDay: boolean }[] {
    return Object.entries(openingHours).map(([day, hours]: any) => {
      const isAllDay = hours.from === '00:00:00' && hours.to === '23:59:00';
      return {
        day: formatDay(day as DayOfWeek),
        from: hours.from ? this.formatTime(hours.from) : '',
        to: hours.to ? this.formatTime(hours.to) : '',
        isAllDay,
      };
    });
  }

  private mapSpecialOpeningHours(
    specialOpeningHours: SpecialOpeningHoursMap
  ): {
    date: string;
    from: string;
    to: string;
    isAllDay: boolean;
    isClosed: boolean;
  }[] {
    return Object.entries(specialOpeningHours).map(([dateKey, hours]) => {
      const from = hours.from ?? '';
      const to = hours.to ?? '';

      const isClosed = from === '00:00:00' && to === '00:00:00';
      const isAllDay = from === '00:00:00' && to === '23:59:00';

      return {
        date: this.formatDate(dateKey),
        from: from ? this.formatTime(from) : '',
        to: to ? this.formatTime(to) : '',
        isAllDay,
        isClosed,
      };
    });
  }

  onOpeningHoursUpdated(updatedHours: OpeningHoursMap) {
    if (this.parkingArea) {
      this.parkingArea.openingHours = updatedHours;
      this.openingHoursList = this.mapOpeningHours(updatedHours);
    }
  }

  onSpecialOpeningHoursUpdated(updatedHours: SpecialOpeningHoursMap) {
    if (this.parkingArea) {
      this.parkingArea.specialOpeningHours = updatedHours;
      this.specialOpeningHoursMap = updatedHours;
      this.specialOpeningHoursList = this.mapSpecialOpeningHours(updatedHours);
    }
  }

  protected formatTime(time: string): string {
    const parsedTime = parse(time, 'HH:mm:ss', new Date());
    return `${format(parsedTime, 'HH:mm')} Uhr`;
  }

  protected formatDate(dateStr: string): string {
    const parsedDate = parse(dateStr, 'yyyy-MM-dd', new Date());
    return format(parsedDate, 'dd.MM.yyyy');
  }

  showRegularDialog() {
    if (this.editParkingTimesComponent) {
      this.editParkingTimesComponent.showDialog();
    } else {
      console.error('EditRegularParkingTimesComponent is not available.');
    }
  }

  showSpecialDialog() {
    if (this.editSpecialOpeningTimesComponent) {
      this.editSpecialOpeningTimesComponent.showDialog();
    } else {
      console.error('EditSpecialOpeningTimesComponent is not available.');
    }
  }
}
