import {
  Component,
  EventEmitter,
  inject,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { format, parse } from 'date-fns';
import {
  OpeningHoursMap,
  DateRange,
  DEFAULT_OPENING_HOURS,
  DayOfWeek,
  formatDay,
} from '@sw-code/urbo-cms-core';
import { ParkingAdapter } from '../../parking.adapter';
import { UIModule } from '@sw-code/urbo-cms-ui';
import { take } from 'rxjs';

@Component({
  selector: 'urbo-cms-parking-edit-regular-times',
  standalone: true,
  imports: [UIModule],
  templateUrl: `./edit-regular-parking-times.component.html`,
})
export class EditRegularParkingTimesComponent implements OnInit {
  @Input() openingHours: OpeningHoursMap = DEFAULT_OPENING_HOURS;
  @Input() parkingId = '';
  @Output() openingHoursUpdated = new EventEmitter<OpeningHoursMap>();

  displayDialog = false;
  errorMessage = '';
  allDaysAllDay = false;

  days: DayOfWeek[] = [
    DayOfWeek.MONDAY,
    DayOfWeek.TUESDAY,
    DayOfWeek.WEDNESDAY,
    DayOfWeek.THURSDAY,
    DayOfWeek.FRIDAY,
    DayOfWeek.SATURDAY,
    DayOfWeek.SUNDAY,
  ];

  editedOpeningHours: {
    [day in DayOfWeek]: {
      closed: boolean;
      from: Date | null;
      to: Date | null;
    };
  } = {} as any;

  protected readonly formatDay = formatDay;
  private readonly parkingAdapter = inject(ParkingAdapter);

  ngOnInit() {
    this.initializeEditedHours();
  }

  private initializeEditedHours(): void {
    this.days.forEach((day) => {
      const hours: DateRange = this.openingHours?.[day] || { from: '', to: '' };
      const isClosed = !hours.from?.trim() && !hours.to?.trim();

      this.editedOpeningHours[day] = {
        closed: isClosed,
        from: isClosed ? null : parse(hours.from, 'HH:mm:ss', new Date()),
        to: isClosed ? null : parse(hours.to, 'HH:mm:ss', new Date()),
      };
    });

    this.allDaysAllDay = this.days.every(
      (day) =>
        this.editedOpeningHours[day].from?.getHours() === 0 &&
        this.editedOpeningHours[day].from?.getMinutes() === 0 &&
        this.editedOpeningHours[day].to?.getHours() === 23 &&
        this.editedOpeningHours[day].to?.getMinutes() === 59
    );
  }

  handleAllDaysAllDayChange(): void {
    this.days.forEach((day) => {
      if (this.allDaysAllDay) {
        this.editedOpeningHours[day].closed = false;
        this.editedOpeningHours[day].from = parse(
          '00:00:00',
          'HH:mm:ss',
          new Date()
        );
        this.editedOpeningHours[day].to = parse(
          '23:59:00',
          'HH:mm:ss',
          new Date()
        );
      } else {
        this.editedOpeningHours[day].from = null;
        this.editedOpeningHours[day].to = null;
      }
    });
  }

  handleClosedChange(day: DayOfWeek): void {
    if (this.editedOpeningHours[day].closed) {
      this.editedOpeningHours[day].from = null;
      this.editedOpeningHours[day].to = null;
    }
  }

  saveChanges(): void {
    this.errorMessage = '';

    for (const day of this.days) {
      const { closed, from, to } = this.editedOpeningHours[day];
      if (!closed && (!from || !to || to <= from)) {
        this.errorMessage = `Die Endzeit für ${formatDay(
          day
        )} muss später als die Startzeit sein.`;
        return;
      }
    }

    const updatedHours: OpeningHoursMap = {} as any;
    this.days.forEach((day) => {
      const { closed, from, to } = this.editedOpeningHours[day];
      updatedHours[day] = closed
        ? { from: '', to: '' }
        : {
            from: from ? format(from, 'HH:mm:ss') : '',
            to: to ? format(to, 'HH:mm:ss') : '',
          };
    });

    this.updateRegularParkingOpeningTimes(updatedHours);
  }

  private updateRegularParkingOpeningTimes(updatedHours: OpeningHoursMap) {
    this.parkingAdapter
      .updateRegularParkingOpeningTimes(this.parkingId, updatedHours)
      .pipe(take(1))
      .subscribe((response) => {
        this.openingHoursUpdated.emit(response.openingHours);
        this.displayDialog = false;
      });
  }

  showDialog(): void {
    this.errorMessage = '';
    this.initializeEditedHours();
    this.displayDialog = true;
  }

  cancel(): void {
    this.displayDialog = false;
  }
}
