import {
  Component,
  EventEmitter,
  inject,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { format, parse } from 'date-fns';
import { Permission, SpecialOpeningHoursMap } from '@sw-code/urbo-cms-core';
import { UIModule } from '@sw-code/urbo-cms-ui';
import { ParkingAdapter } from '../../parking.adapter';
import { take } from 'rxjs';

@Component({
  selector: 'urbo-cms-parking-edit-special-times',
  standalone: true,
  imports: [UIModule],
  providers: [ConfirmationService],
  templateUrl: './edit-special-opening-times.component.html',
  styleUrls: ['./edit-special-opening-times.component.scss'],
})
export class EditSpecialOpeningTimesComponent implements OnInit {
  @Input() specialOpeningHours: SpecialOpeningHoursMap = {};
  @Input() parkingId = '';
  @Output() specialOpeningHoursUpdated =
    new EventEmitter<SpecialOpeningHoursMap>();

  today = new Date();
  displayDialog = false;
  errorMessage = '';
  activeAccordionIndex: number[] = [];
  editableHours: {
    dateKey: string;
    date: Date | null;
    from: Date | null;
    to: Date | null;
    closed: boolean;
    allDay: boolean;
    descriptionDe: string;
    descriptionEn: string;
  }[] = [];

  protected readonly Permission = Permission;
  private readonly confirmationService = inject(ConfirmationService);
  private readonly parkingAdapter = inject(ParkingAdapter);

  ngOnInit(): void {
    this.initializeEditableHours();
  }

  initializeEditableHours(): void {
    this.editableHours = Object.entries(this.specialOpeningHours).map(
      ([dateKey, { from, to, descriptions }]) => {
        const descriptionDe =
          descriptions.find((d) => d.language === 'DE')?.value ?? '';
        const descriptionEn =
          descriptions.find((d) => d.language === 'EN')?.value ?? '';

        const isClosed = from === '00:00:00' && to === '00:00:00';
        const isAllDay = from === '00:00:00' && to === '23:59:00';

        let parsedFrom = null;
        let parsedTo = null;

        if (!isClosed && !isAllDay) {
          parsedFrom = from ? parse(from, 'HH:mm:ss', new Date()) : null;
          parsedTo = to ? parse(to, 'HH:mm:ss', new Date()) : null;
        }

        return {
          dateKey,
          date: dateKey ? parse(dateKey, 'yyyy-MM-dd', new Date()) : null,
          from: parsedFrom,
          to: parsedTo,
          closed: isClosed,
          allDay: isAllDay,
          descriptionDe,
          descriptionEn,
        };
      }
    );

    if (this.editableHours.length) {
      this.activeAccordionIndex = [];
    }
  }

  handleClosedChange(entry: any): void {
    if (entry.closed) {
      entry.allDay = false;
      entry.from = parse('00:00:00', 'HH:mm:ss', new Date());
      entry.to = parse('00:00:00', 'HH:mm:ss', new Date());
    } else {
      entry.from = null;
      entry.to = null;
    }
  }

  handleAllDayChange(entry: any): void {
    if (entry.allDay) {
      entry.closed = false;
      entry.from = parse('00:00:00', 'HH:mm:ss', new Date());
      entry.to = parse('23:59:00', 'HH:mm:ss', new Date());
    } else {
      entry.from = null;
      entry.to = null;
    }
  }

  showDialog(): void {
    this.errorMessage = '';
    this.initializeEditableHours();
    this.displayDialog = true;
  }

  cancel(): void {
    this.displayDialog = false;
  }

  addNewOpeningHour(): void {
    this.editableHours.push({
      dateKey: '',
      date: null,
      from: null,
      to: null,
      closed: false,
      allDay: false,
      descriptionDe: '',
      descriptionEn: '',
    });
    this.activeAccordionIndex = [
      ...this.activeAccordionIndex,
      this.editableHours.length - 1,
    ];
  }

  confirmRemoveOpeningHour(index: number): void {
    this.confirmationService.confirm({
      message: 'Möchtest du diesen Eintrag wirklich löschen?',
      header: 'Löschen bestätigen',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Ja, löschen',
      rejectLabel: 'Abbrechen',
      accept: () => {
        this.removeOpeningHour(index);
      },
    });
  }

  handleFromTimeSelect(hour: { from: Date | null; to: Date | null }): void {
    if (hour.from && !hour.to) {
      hour.to = new Date(hour.from.getTime() + 60 * 60 * 1000);
    }
  }

  removeOpeningHour(index: number): void {
    this.editableHours.splice(index, 1);

    this.activeAccordionIndex = this.activeAccordionIndex
      .filter((i) => i !== index)
      .map((i) => (i > index ? i - 1 : i));
  }

  validateEntry(entry: {
    date: Date | null;
    from: Date | null;
    to: Date | null;
    closed: boolean;
    allDay: boolean;
    descriptionDe: string;
    descriptionEn: string;
  }): string | null {
    if (!entry.date) {
      return 'Bitte gib ein Datum an.';
    }

    if (!entry.closed && !entry.allDay) {
      if (!entry.from || !entry.to) {
        return 'Bitte gib sowohl Start- als auch Endzeit an.';
      }

      if (entry.to <= entry.from) {
        return 'Die Endzeit muss später als die Startzeit sein.';
      }
    }

    if (!entry.descriptionDe.trim()) {
      return 'Bitte gib eine Beschreibung auf Deutsch an.';
    }

    if (!entry.descriptionEn.trim()) {
      return 'Bitte gib eine Beschreibung auf Englisch an.';
    }

    return null;
  }

  saveChanges(): void {
    this.errorMessage = '';
    const updatedHours: SpecialOpeningHoursMap = {};

    for (const entry of this.editableHours) {
      if (entry.closed) {
        entry.from = parse('00:00:00', 'HH:mm:ss', new Date());
        entry.to = parse('00:00:00', 'HH:mm:ss', new Date());
      }

      if (entry.allDay) {
        entry.from = parse('00:00:00', 'HH:mm:ss', new Date());
        entry.to = parse('23:59:00', 'HH:mm:ss', new Date());
      }

      const validationError = this.validateEntry(entry);
      if (validationError) {
        this.errorMessage = validationError;
        return;
      }

      const dateKey = format(entry.date as Date, 'yyyy-MM-dd');

      updatedHours[dateKey] = {
        from: format(entry.from as Date, 'HH:mm:ss'),
        to: format(entry.to as Date, 'HH:mm:ss'),
        descriptions: [
          { value: entry.descriptionDe, language: 'DE' },
          { value: entry.descriptionEn, language: 'EN' },
        ],
      };
    }

    this.updateSpecialParkingOpeningTimes(updatedHours);
  }

  private updateSpecialParkingOpeningTimes(
    updatedHours: SpecialOpeningHoursMap
  ) {
    this.parkingAdapter
      .updateSpecialParkingOpeningTimes(this.parkingId, updatedHours)
      .pipe(take(1))
      .subscribe((response) => {
        this.specialOpeningHoursUpdated.emit(response.specialOpeningHours);
        this.displayDialog = false;
      });
  }
}
