/* eslint @typescript-eslint/naming-convention : 0 */
import {
  OpeningHoursMap,
  SpecialOpeningHoursMap,
} from '@sw-code/urbo-cms-core';

export class Parking {
  id: string;
  name: string;
  displayName: string;
  parkingType: ParkingType;
  status: ParkingStatus;
  coordinates: Coordinates;
  openingHours: OpeningHoursMap;
  specialOpeningHours?: SpecialOpeningHoursMap;
  parkingCapacity: ParkingCapacity;
  address: ParkingAddress;
  maximumAllowedHeight?: string | null;

  constructor(options: ParkingOptions) {
    this.id = options.id;
    this.name = options.name;
    this.status = options.status;
    this.coordinates = options.coordinates;
    this.openingHours = options.openingHours;
    this.specialOpeningHours = options.specialOpeningHours;
    this.parkingCapacity = options.parkingCapacity;
    this.address = options.address;
    this.maximumAllowedHeight = options.maximumAllowedHeight;

    this.displayName = options.displayName;
    this.parkingType = options.parkingType;
  }
}

interface ParkingAddress {
  street: string;
  postalCode: string;
  city: string;
}

export type ParkingType =
  | 'GARAGE'
  | 'PARKING_HOUSE'
  | 'UNDERGROUND'
  | 'PARKING_SPOT';

export enum ParkingStatus {
  OPEN = 'OPEN',
  CLOSED = 'CLOSED',
  UNKNOWN = 'UNKNOWN',
}

export type Coordinates = { lat: number; long: number };

interface ParkingOptions {
  id: string;
  name: string;
  status: ParkingStatus;
  coordinates: Coordinates;
  openingHours: OpeningHoursMap;
  specialOpeningHours?: SpecialOpeningHoursMap;
  parkingCapacity: ParkingCapacity;
  address: ParkingAddress;
  maximumAllowedHeight?: string | null;
  displayName: string;
  parkingType: ParkingType;
}

interface ParkingCapacity {
  total: number;
  remaining: number;
}
