export { FeatureNewsModule } from './lib/feature-news.module';
export * from './lib/models/news.model';
export * from './lib/models/news-categories.model';
export { NewsAdapter } from './lib/news.adapter';
