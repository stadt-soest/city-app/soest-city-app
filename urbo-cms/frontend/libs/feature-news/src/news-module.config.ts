import { InjectionToken } from '@angular/core';

export interface NewsModuleConfig {
  apiBaseUrl: string;
}

export const NEWS_MODULE_CONFIG = new InjectionToken<NewsModuleConfig>(
  'NewsModuleConfig'
);
