import { Image } from '@sw-code/urbo-cms-core';

export interface News {
  id: string;
  title: string;
  editable: boolean;
  subTitle?: string;
  date: Date;
  modified?: Date;
  detailedCategories: DetailedCategory[];
  source: string;
  content: string;
  excerpt?: string;
  link?: string;
  authors: string[];
  categories: string[];
  image: Image | null;
  highlight: boolean;
}

export interface DetailedCategory {
  id: string;
  sourceCategory: string;
  localizedNames: { language: string; value: string }[];
}
