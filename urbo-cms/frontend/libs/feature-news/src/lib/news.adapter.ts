import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { News } from './models/news.model';
import { NewsCategory } from './models/news-categories.model';
import { CreateNewsDto } from '@sw-code/urbo-cms-backend-api';

@Injectable()
export abstract class NewsAdapter {
  abstract getNews(
    page: number,
    rows: number,
    sourceType: 'EXTERNAL' | 'INTERNAL' | null,
    sortField?: string,
    sortOrder?: string,
    highlighted?: boolean
  ): Observable<{ content: News[]; totalRecords: number }>;

  abstract updateNews(id: string, newsForm: CreateNewsDto): Observable<News>;

  abstract updateNewsHighlightStatus(
    id: string,
    highlightStatus: boolean
  ): Observable<News>;

  abstract deleteNews(id: string): Observable<void>;

  abstract getBySearchTerm(
    searchTerm: string,
    page?: number,
    size?: number,
    sortField?: string,
    sortOrder?: string
  ): Observable<{ total: number; items: News[] }>;

  abstract getDetailNews(id: string): Observable<News>;

  abstract getCategories(): Observable<NewsCategory[]>;

  abstract createCategory(category: NewsCategory): Observable<NewsCategory>;

  abstract getSourceCategories(): Observable<string[]>;

  abstract getUnmappedSourceCategories(): Observable<string[]>;

  abstract updateCategory(category: NewsCategory): Observable<NewsCategory>;

  abstract deleteCategory(category: NewsCategory): Observable<void>;

  abstract uploadFile(file: Blob): Observable<string>;

  abstract createNews(newsForm: CreateNewsDto): Observable<News>;
}
