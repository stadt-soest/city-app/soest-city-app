import {
  ModuleWithProviders,
  NgModule,
  inject,
  provideAppInitializer,
} from '@angular/core';
import {
  CoreModule,
  FeatureModule,
  MenuItem,
  ModuleCategory,
  ModuleInfo,
  ModuleRegistryService,
  Permission,
} from '@sw-code/urbo-cms-core';
import { NewsComponent } from './components/news/news.component';
import { NewsDetailsComponent } from './components/news/news-details/news-details.component';
import { CreateNewsComponent } from './components/news/create-news/create-news.component';
import { NewsFormComponent } from './components/news/news-form/news-form.component';
import { take } from 'rxjs';
import { DropdownModule } from 'primeng/dropdown';
import { NewsAdapter } from './news.adapter';
import { UIModule } from '@sw-code/urbo-cms-ui';
import { NEWS_MODULE_CONFIG, NewsModuleConfig } from '../news-module.config';

const MODULES = [CoreModule, UIModule];
const COMPONENTS = [
  NewsComponent,
  NewsDetailsComponent,
  CreateNewsComponent,
  NewsFormComponent,
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES, DropdownModule],
  exports: [...MODULES],
})
export class FeatureNewsModule {
  readonly featureModule: FeatureModule;
  moduleName = 'Neuigkeiten';

  constructor(private readonly newsService: NewsAdapter) {
    this.featureModule = {
      info: new ModuleInfo(
        this.moduleName,
        ModuleCategory.MODULES,
        'pi pi-megaphone',
        'news',
        [
          new MenuItem(this.moduleName, 'pi pi-table', 'news'),
          new MenuItem(
            'Kategorien',
            'pi pi-folder-open',
            'news/categories',
            [],
            [Permission.CategoryRead]
          ),
        ]
      ),
      routes: [
        {
          path: 'news',
          loadChildren: () =>
            import('./components/routing.module').then((m) => m.RoutingModule),
        },
      ],
      dashboardData: {
        moduleName: this.moduleName,
        entityCount: 0,
      },
    };

    this.newsService
      .getNews(0, 10, null)
      .pipe(take(1))
      .subscribe(
        (response) => {
          if (this.featureModule.dashboardData) {
            this.featureModule.dashboardData.entityCount =
              response.totalRecords;
          }
        },
        (error) => {
          console.error('Error fetching news count', error);
        }
      );
  }

  static forRoot(
    config: NewsModuleConfig
  ): ModuleWithProviders<FeatureNewsModule> {
    return {
      ngModule: FeatureNewsModule,
      providers: [
        { provide: NEWS_MODULE_CONFIG, useValue: config },
        provideAppInitializer(() => {
          const initializerFn = (
            (registry: ModuleRegistryService, module: FeatureNewsModule) =>
            () => {
              registry.registerFeature(module.featureModule);
            }
          )(inject(ModuleRegistryService), inject(FeatureNewsModule));
          return initializerFn();
        }),
      ],
    };
  }
}
