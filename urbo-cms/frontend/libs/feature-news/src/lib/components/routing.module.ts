import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CategoriesComponent } from './categories/categories.component';
import { CardModule } from 'primeng/card';
import { AccordionModule } from 'primeng/accordion';
import { InputTextModule } from 'primeng/inputtext';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ChipModule } from 'primeng/chip';
import { MultiSelectModule } from 'primeng/multiselect';
import { ButtonModule } from 'primeng/button';
import { NewCategoryComponent } from './categories/new-category/new-category.component';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { UnmappedCategoriesComponent } from './categories/unmapped-categories/unmapped-categories.component';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { NewsComponent } from './news/news.component';
import { NewsDetailsComponent } from './news/news-details/news-details.component';
import { UIModule } from '@sw-code/urbo-cms-ui';

const routes: Routes = [
  {
    path: '',
    component: NewsComponent,
  },
  {
    path: 'categories',
    component: CategoriesComponent,
  },
  {
    path: ':id',
    component: NewsDetailsComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    CardModule,
    AccordionModule,
    InputTextModule,
    FormsModule,
    ChipModule,
    MultiSelectModule,
    ButtonModule,
    ProgressSpinnerModule,
    ConfirmDialogModule,
    UIModule,
  ],
  exports: [RouterModule],
  declarations: [
    CategoriesComponent,
    NewCategoryComponent,
    UnmappedCategoriesComponent,
  ],
  providers: [ConfirmationService],
})
export class RoutingModule {}
