import { Component, EventEmitter, Output } from '@angular/core';
import { catchError, finalize, take } from 'rxjs';
import { NewsCategory } from '../../../models/news-categories.model';
import { NewsAdapter } from '../../../news.adapter';

@Component({
  selector: 'urbo-cms-news-new-category',
  templateUrl: './new-category.component.html',
  standalone: false,
})
export class NewCategoryComponent {
  @Output() created: EventEmitter<void> = new EventEmitter<void>();

  newCategory: NewsCategory = new NewsCategory();
  loading = false;
  displayDialog = false;

  constructor(private readonly newsCategoryService: NewsAdapter) {}

  createCategory() {
    this.loading = true;
    this.newsCategoryService
      .createCategory(this.newCategory)
      .pipe(
        take(1),
        catchError((err) => {
          return err;
        }),
        finalize(() => {
          this.loading = false;
          this.displayDialog = false;
        })
      )
      .subscribe(() => {
        this.created.emit();
        this.newCategory = new NewsCategory();
      });
  }

  showDialog() {
    this.displayDialog = true;
  }
}
