import { Component, Input } from '@angular/core';

@Component({
  selector: 'urbo-cms-news-unmapped-categories',
  templateUrl: './unmapped-categories.component.html',
  styleUrls: ['./unmapped-categories.component.scss'],
  standalone: false,
})
export class UnmappedCategoriesComponent {
  @Input() unmappedSourceCategories!: string[];
}
