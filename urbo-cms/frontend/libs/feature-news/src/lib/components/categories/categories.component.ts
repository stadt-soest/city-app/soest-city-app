import { Component, OnInit, ViewChild } from '@angular/core';
import { catchError, take } from 'rxjs';
import { ConfirmationService } from 'primeng/api';
import {
  MultiSelectNewsCategory,
  NewsCategory,
} from '../../models/news-categories.model';
import { NewsAdapter } from '../../news.adapter';
import { Permission } from '@sw-code/urbo-cms-core';
import { NewCategoryComponent } from './new-category/new-category.component';

@Component({
  selector: 'urbo-cms-news-categories',
  templateUrl: './categories.component.html',
  standalone: false,
})
export class CategoriesComponent implements OnInit {
  @ViewChild(NewCategoryComponent)
  createNewCategoryComponent?: NewCategoryComponent;
  categories: NewsCategory[] = [];
  protected readonly Permission = Permission;
  unmappedSourceCategories: string[] = [];
  allSourceCategories: MultiSelectNewsCategory[] = [];
  loading = false;

  constructor(
    private readonly newsCategoryService: NewsAdapter,
    private readonly confirmationService: ConfirmationService
  ) {}

  ngOnInit(): void {
    this.loadContent();
  }

  loadContent() {
    this.loadCategories();
    this.loadUnmappedCategories();
    this.loadAllSourceCategories();
  }

  private loadCategories() {
    this.newsCategoryService
      .getCategories()
      .pipe(take(1))
      .subscribe((categories) => {
        this.categories = categories;
      });
  }

  private loadUnmappedCategories() {
    this.newsCategoryService
      .getUnmappedSourceCategories()
      .pipe(take(1))
      .subscribe((unmappedSourceCategories) => {
        this.unmappedSourceCategories = unmappedSourceCategories;
      });
  }

  updateCategory(category: NewsCategory) {
    this.loading = true;
    this.newsCategoryService
      .updateCategory(category)
      .pipe(
        take(1),
        catchError((err) => {
          this.loading = false;
          return err;
        })
      )
      .subscribe(() => {
        this.loadContent();
        this.loading = false;
      });
  }

  private deleteCategory(category: NewsCategory) {
    this.loading = true;
    this.newsCategoryService
      .deleteCategory(category)
      .pipe(
        take(1),
        catchError((err) => {
          this.loading = false;
          return err;
        })
      )
      .subscribe(() => {
        this.loadContent();
        this.loading = false;
      });
  }

  confirmDeleteCategory(category: NewsCategory) {
    this.confirmationService.confirm({
      message: 'Bist du dir sicher, dass du diese Kategorie löschen möchtest?',
      header: 'Bestätigung',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Ja',
      rejectLabel: 'Abbrechen',
      acceptButtonStyleClass: 'p-button-danger',
      rejectButtonStyleClass: 'p-button-default',
      accept: () => this.deleteCategory(category),
    });
  }

  private loadAllSourceCategories() {
    this.newsCategoryService
      .getSourceCategories()
      .pipe(take(1))
      .subscribe((allSourceCategories) => {
        this.allSourceCategories = allSourceCategories.map(
          (category) => new MultiSelectNewsCategory(category, category)
        );
      });
  }

  showDialog() {
    this.createNewCategoryComponent?.showDialog();
  }
}
