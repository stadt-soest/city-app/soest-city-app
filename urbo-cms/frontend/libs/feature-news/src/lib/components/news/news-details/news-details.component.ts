import { Component, inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { FormGroup } from '@angular/forms';
import { catchError, of, switchMap, take } from 'rxjs';
import { News } from '../../../models/news.model';
import { NewsAdapter } from '../../../news.adapter';
import { Permission } from '@sw-code/urbo-cms-core';
import { NEWS_MODULE_CONFIG } from '../../../../news-module.config';

@Component({
  selector: 'urbo-cms-news-details',
  styleUrl: 'news-details.component.scss',
  templateUrl: './news-details.component.html',
  standalone: false,
})
export class NewsDetailsComponent implements OnInit {
  news?: News;
  selectedFile?: File;
  displayDialog = false;
  imageChanged = false;
  altDescription = '';

  private readonly activatedRoute = inject(ActivatedRoute);
  private readonly newsService = inject(NewsAdapter);
  private readonly newsAdapter = inject(NewsAdapter);
  private readonly confirmationService = inject(ConfirmationService);
  private readonly router = inject(Router);
  private readonly newsConfig = inject(NEWS_MODULE_CONFIG);

  ngOnInit() {
    this.loadDetailsNews();
  }

  loadDetailsNews() {
    this.newsService
      .getDetailNews(this.activatedRoute.snapshot.params['id'])
      .pipe(take(1))
      .subscribe((news: News) => {
        this.news = news;
        this.altDescription = this.news.image?.description ?? '';
      });
  }

  editNews() {
    this.displayDialog = true;
  }

  onFileSelected(file: File) {
    this.selectedFile = file;
    this.imageChanged = true;
  }

  onSubmit(newsForm: FormGroup) {
    if (newsForm.valid) {
      if (this.selectedFile) {
        this.uploadFileAndUpdateNews(newsForm);
      } else {
        this.updateNews(newsForm);
      }
    }
  }

  uploadFileAndUpdateNews(newsForm: FormGroup) {
    if (!this.selectedFile) {
      console.error('No file selected for upload');
      return;
    }

    this.newsAdapter
      .uploadFile(this.selectedFile)
      .pipe(
        take(1),
        switchMap((fileName: string) => {
          const fileUrl = `${this.newsConfig.apiBaseUrl}/files/${fileName}`;
          newsForm.patchValue({ imageUrl: fileUrl });
          if (this.news?.id) {
            return this.newsService
              .updateNews(this.news.id, newsForm.value)
              .pipe(take(1));
          } else {
            return of(null);
          }
        }),
        catchError((error) => {
          console.error('Failed to upload file or update news', error);
          return of(null);
        })
      )
      .subscribe({
        next: () => {
          this.resetFormAndClose();
        },
        error: (error: Error) => {
          console.error('Failed to update news', error);
        },
      });
  }

  updateNews(newsForm: FormGroup) {
    const payload = { ...newsForm.value };

    if (!this.imageChanged) {
      delete payload.imageUrl;
    }
    if (this.news?.id) {
      this.newsService
        .updateNews(this.news.id, payload)
        .pipe(take(1))
        .subscribe({
          next: () => {
            this.resetFormAndClose();
          },
          error: (error) => {
            console.error('Failed to update news', error);
          },
        });
    }
  }

  confirmDelete() {
    this.confirmationService.confirm({
      message: 'Bist du sicher, dass du diese News löschen möchtest?',
      accept: () => {
        this.deleteNews();
      },
      rejectLabel: 'Nein',
      acceptLabel: 'Ja',
    });
  }

  deleteNews() {
    if (this.news) {
      this.newsService
        .deleteNews(this.news.id)
        .pipe(take(1))
        .subscribe({
          next: () => {
            this.router.navigate(['/news-list']);
          },
        });
    }
  }

  onCancel() {
    this.resetFormAndClose();
  }

  resetFormAndClose() {
    this.selectedFile = undefined;
    this.imageChanged = false;
    this.displayDialog = false;
    this.loadDetailsNews();
  }

  protected readonly Permission = Permission;

  highlightNews($event: { highlight: boolean; id: string }) {
    if (this.news) {
      this.news.highlight = $event.highlight;

      this.newsService
        .updateNewsHighlightStatus($event.id, this.news.highlight)
        .pipe(take(1))
        .subscribe({
          error: (err) =>
            console.error('Failed to update news highlight:', err),
        });
    }
  }
}
