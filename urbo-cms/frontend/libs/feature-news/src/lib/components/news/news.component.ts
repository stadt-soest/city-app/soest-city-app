import { Component, inject, OnInit } from '@angular/core';
import { News } from '../../models/news.model';
import { CurrentSort, Permission, SortService } from '@sw-code/urbo-cms-core';
import { Router } from '@angular/router';
import { take } from 'rxjs';
import { NewsAdapter } from '../../news.adapter';

@Component({
  selector: 'urbo-cms-news-dashboard',
  templateUrl: './news.component.html',
  standalone: false,
})
export class NewsComponent implements OnInit {
  searchSelected = false;
  baseCols = [
    { header: 'Bild', field: 'image', type: 'image' },
    { header: 'Titel', field: 'title', type: 'text' },
    { header: 'Untertitel', field: 'subtitle', type: 'truncated-text' },
    { header: 'Quelle', field: 'link', type: 'link' },
    { header: 'Autoren', field: 'authors', type: 'text' },
    { header: 'Kategorien', field: 'categories', type: 'text' },
    { header: 'Datum', field: 'date', type: 'datetime' },
  ];
  news?: News[];
  totalRecords!: number;
  rows = 10;
  currentSearchTerm = '';
  filterOptions = [
    { label: 'Alle', value: null },
    { label: 'Erstellt', value: 'created' },
    { label: 'Markiert', value: 'highlight' },
  ];
  selectedFilter: string | null = null;
  displayDialog = false;
  protected readonly Permission = Permission;
  private currentSort: CurrentSort = { field: '', order: 'asc' };
  private readonly SORTABLE_FIELDS_BASE = [
    'title',
    'subtitle',
    'link',
    'authors',
    'date',
  ];
  private readonly SORTABLE_FIELDS_SEARCH = ['date'];
  private readonly newsService = inject(NewsAdapter);
  private readonly router = inject(Router);
  private readonly sortService = inject(SortService);

  ngOnInit() {
    this.loadData(0, this.rows);
  }

  get cols() {
    return this.sortService.getColumns(this.baseCols, (field: string) => {
      return this.searchSelected
        ? this.SORTABLE_FIELDS_SEARCH.includes(field)
        : this.SORTABLE_FIELDS_BASE.includes(field);
    });
  }

  loadData(
    page: number,
    rows: number,
    searchTerm?: string,
    sortField?: string,
    sortOrder?: string
  ) {
    if (searchTerm) {
      this.fetchNewsBySearchTerm(searchTerm, page, rows, sortField, sortOrder);
    } else if (this.selectedFilter === 'highlight') {
      this.fetchHighlightedNews(page, rows, sortField, sortOrder);
    } else {
      this.fetchFilteredNews(page, rows, sortField, sortOrder);
    }
  }

  onPage(event: { first: number; rows: number }) {
    const page = event.first / event.rows;
    this.loadData(
      page,
      event.rows,
      this.currentSearchTerm,
      this.currentSort?.field,
      this.currentSort?.order
    );
  }

  onSearch(searchQuery: string) {
    this.currentSearchTerm = searchQuery;

    if (searchQuery) {
      this.searchSelected = true;
      this.loadData(0, this.rows, searchQuery);
    } else {
      this.searchSelected = false;
      this.loadData(0, this.rows);
    }
  }

  async showNewsDetails(clickedRowData: News) {
    await this.router.navigate(['/news', clickedRowData['id']]);
  }

  handleFilterChange(filter: string | null) {
    this.selectedFilter = filter;
    this.loadData(0, this.rows, this.currentSearchTerm);
  }

  showDialog() {
    this.displayDialog = true;
  }

  hideDialog() {
    this.displayDialog = false;
    this.loadData(0, this.rows);
  }

  private getSourceTypeFilter(): 'EXTERNAL' | 'INTERNAL' | null {
    if (this.selectedFilter === 'created') {
      return 'INTERNAL';
    } else if (this.selectedFilter === 'external') {
      return 'EXTERNAL';
    }
    return null;
  }

  highlightNews(news: News) {
    const newsItem = this.news?.find((item) => item.id === news.id);

    if (newsItem) {
      newsItem.highlight = !newsItem.highlight;

      this.newsService
        .updateNewsHighlightStatus(news.id, newsItem.highlight)
        .pipe(take(1))
        .subscribe({
          error: (err) =>
            console.error('Failed to update news highlight:', err),
        });
    }
  }

  private fetchNewsBySearchTerm(
    searchTerm: string,
    page: number,
    rows: number,
    sortField?: string,
    sortOrder?: string
  ) {
    this.newsService
      .getBySearchTerm(searchTerm, page + 1, rows, sortField, sortOrder)
      .pipe(take(1))
      .subscribe((data) => {
        if (this.selectedFilter === 'highlight') {
          this.news = data.items.filter((news) => news.highlight);
        } else {
          this.news = data.items;
        }
        this.totalRecords = data.total;
      });
  }

  private fetchHighlightedNews(
    page: number,
    rows: number,
    sortField?: string,
    sortOrder?: string
  ) {
    this.newsService
      .getNews(
        page,
        rows,
        this.getSourceTypeFilter(),
        sortField,
        sortOrder,
        true
      )
      .pipe(take(1))
      .subscribe((data) => {
        this.news = data.content;
        this.totalRecords = data.totalRecords;
      });
  }

  private fetchFilteredNews(
    page: number,
    rows: number,
    sortField?: string,
    sortOrder?: string
  ) {
    this.newsService
      .getNews(page, rows, this.getSourceTypeFilter(), sortField, sortOrder)
      .pipe(take(1))
      .subscribe((data) => {
        this.news = data.content;
        this.totalRecords = data.totalRecords;
      });
  }

  onSort(sortEvent: CurrentSort) {
    this.currentSort = this.sortService.handleSort(sortEvent);
    this.loadData(
      0,
      this.rows,
      this.currentSearchTerm,
      this.currentSort.field,
      this.currentSort.order
    );
  }
}
