import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { catchError, of, switchMap, take } from 'rxjs';
import { NewsAdapter } from '../../../news.adapter';
import {
  NEWS_MODULE_CONFIG,
  NewsModuleConfig,
} from '../../../../news-module.config';

@Component({
  selector: 'urbo-cms-news-create',
  templateUrl: './create-news.component.html',
  standalone: false,
})
export class CreateNewsComponent {
  sourceCategories: string[] = [];
  selectedFile?: File;
  displayDialog = false;
  @Output() close = new EventEmitter<void>();

  constructor(
    private readonly newsAdapter: NewsAdapter,
    @Inject(NEWS_MODULE_CONFIG) private readonly config: NewsModuleConfig
  ) {}

  onFileSelected(file: File) {
    this.selectedFile = file;
  }

  showDialog() {
    this.displayDialog = true;
  }

  onSubmit(newsForm: FormGroup) {
    if (newsForm.valid) {
      if (this.selectedFile) {
        this.uploadFileAndCreateNews(newsForm);
      } else {
        this.createNews(newsForm);
      }
    }
  }

  uploadFileAndCreateNews(newsForm: FormGroup) {
    if (!this.selectedFile) {
      console.error('No file selected for upload');
      return;
    }

    this.newsAdapter
      .uploadFile(this.selectedFile)
      .pipe(
        take(1),
        switchMap((fileName: string) => {
          const fileUrl = `${this.config.apiBaseUrl}/files/${fileName}`;
          newsForm.patchValue({ imageUrl: fileUrl });

          return this.newsAdapter.createNews(newsForm.value).pipe(take(1));
        }),
        catchError((error) => {
          console.error('Failed to upload file or create news', error);
          return of(null);
        })
      )
      .subscribe({
        next: () => this.resetFormAndClose(),
        error: (error: Error) => {
          console.error('Failed to create news', error);
        },
      });
  }

  createNews(newsForm: FormGroup) {
    this.newsAdapter
      .createNews(newsForm.value)
      .pipe(take(1))
      .subscribe({
        next: () => this.resetFormAndClose(),
        error: (error: Error) => {
          console.error('Failed to create news', error);
        },
      });
  }

  resetFormAndClose() {
    this.selectedFile = undefined;
    this.displayDialog = false;
    this.close.emit();
  }

  onCancel() {
    this.resetFormAndClose();
  }
}
