import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { News } from '../../../models/news.model';
import { take } from 'rxjs';
import { NewsAdapter } from '../../../news.adapter';
import { NewsCategory } from '../../../models/news-categories.model';

@Component({
  selector: 'urbo-cms-news-form',
  templateUrl: './news-form.component.html',
  standalone: false,
})
export class NewsFormComponent implements OnInit, OnChanges {
  @Input() newsData?: News;
  @Output() formSubmit = new EventEmitter<FormGroup>();
  @Output() fileSelected = new EventEmitter<File>();
  @Output() cancelAction = new EventEmitter<void>();

  newsForm: FormGroup;
  selectedFile?: File;
  categories: { label: string; value: string }[] = [];
  newsSources: { label: string; value: string }[] = [];
  altDescription = 'Current Image';

  constructor(
    private readonly fb: FormBuilder,
    private readonly newsAdapter: NewsAdapter
  ) {
    this.newsForm = this.fb.group({
      title: ['', Validators.required],
      subtitle: [''],
      content: ['', Validators.required],
      source: ['', Validators.required],
      link: ['', Validators.pattern(/^(http|https):\/\/[^ "]+$/)],
      imageUrl: [''],
      categoryIds: [[], Validators.required],
      authors: [[]],
    });
  }

  ngOnInit(): void {
    this.loadCategories();
    this.loadNewsSources();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['newsData'] && this.newsData) {
      this.mapNewsToForm(this.newsData);
      this.updateFormStateBasedOnEditable();
    }
  }

  loadCategories() {
    this.newsAdapter
      .getCategories()
      .pipe(take(1))
      .subscribe((categories) => {
        this.categories = categories.map((category: NewsCategory) => ({
          label: category.title.de ?? '',
          value: category.id ?? '',
        }));

        if (this.newsData) {
          const selectedCategoryIds =
            this.newsData.detailedCategories
              ?.filter((dc) => dc.id)
              .map((dc) => dc.id) ?? [];
          this.newsForm.patchValue({ categoryIds: selectedCategoryIds });
        }
      });
  }

  loadNewsSources() {
    this.newsAdapter
      .getSourceCategories()
      .pipe(take(1))
      .subscribe((sources: string[]) => {
        this.newsSources = sources.map((source) => ({
          label: source,
          value: source,
        }));

        if (this.newsData) {
          this.newsForm.patchValue({ source: this.newsData.source });
        }
      });
  }

  mapNewsToForm(news: News) {
    const formValues = {
      title: news.title,
      subtitle: news.subTitle ?? '',
      content: news.content,
      source: news.source,
      link: news.link ?? '',
      imageUrl: news.image?.thumbnail?.filename ?? '',
      categoryIds: news.detailedCategories?.map((dc) => dc.id) ?? [],
      authors: news.authors || [],
    };

    this.newsForm.patchValue(formValues);
  }

  updateFormStateBasedOnEditable() {
    if (this.newsData && !this.newsData.editable) {
      this.newsForm.disable();
    } else {
      this.newsForm.enable();
    }
  }

  onFileSelected(file: File) {
    this.selectedFile = file;
    this.fileSelected.emit(file);
  }

  onSubmit() {
    if (this.newsForm.valid) {
      this.formSubmit.emit(this.newsForm);
    }
  }

  onFormCancel() {
    this.cancelAction.emit();
    this.newsForm.reset();
  }
}
