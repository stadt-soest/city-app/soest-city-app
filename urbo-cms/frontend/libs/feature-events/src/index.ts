export { FeatureEventModule } from './lib/feature-event.module';
export * from './lib/models/event.model';
export * from './lib/models/event-categories.model';
export { EventAdapter } from './lib/event.adapter';
