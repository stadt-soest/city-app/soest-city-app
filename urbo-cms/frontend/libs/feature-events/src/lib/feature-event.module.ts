import {
  ModuleWithProviders,
  NgModule,
  inject,
  provideAppInitializer,
} from '@angular/core';
import { take } from 'rxjs';
import {
  CoreModule,
  FeatureModule,
  MenuItem,
  ModuleCategory,
  ModuleInfo,
  ModuleRegistryService,
  Permission,
} from '@sw-code/urbo-cms-core';
import { EventsComponent } from './components/events/events.component';
import { EventDetailsComponent } from './components/events/event-details/event-details.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { NewCategoryComponent } from './components/categories/new-category/new-category.component';
import { UnmappedCategoriesComponent } from './components/categories/unmapped-categories/unmapped-categories.component';
import { DatePipe } from '@angular/common';
import { ConfirmationService } from 'primeng/api';
import { EventAdapter } from './event.adapter';
import { UIModule } from '@sw-code/urbo-cms-ui';

const MODULES = [CoreModule, UIModule];
const COMPONENTS = [
  EventsComponent,
  EventDetailsComponent,
  CategoriesComponent,
  NewCategoryComponent,
  UnmappedCategoriesComponent,
];
const PROVIDERS = [DatePipe, ConfirmationService];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES],
  exports: [...MODULES],
  providers: [...PROVIDERS],
})
export class FeatureEventModule {
  readonly featureModule: FeatureModule;
  moduleName = 'Veranstaltungen';

  constructor(private readonly eventAdapter: EventAdapter) {
    this.featureModule = {
      info: new ModuleInfo(
        this.moduleName,
        ModuleCategory.MODULES,
        'pi pi-calendar',
        'events',
        [
          new MenuItem(this.moduleName, 'pi pi-table', 'events'),
          new MenuItem(
            'Kategorien',
            'pi pi-folder-open',
            'events/categories',
            [],
            [Permission.CategoryRead]
          ),
        ]
      ),
      routes: [
        {
          path: 'events',
          loadChildren: () =>
            import('./components/routing.module').then((m) => m.RoutingModule),
        },
      ],
      dashboardData: {
        moduleName: this.moduleName,
        entityCount: 0,
      },
    };

    this.eventAdapter
      .getEvents(0, 10)
      .pipe(take(1))
      .subscribe(
        (response: { totalRecords: any }) => {
          this.featureModule.dashboardData!.entityCount = response.totalRecords;
        },
        (error: any) => {
          console.error('Error fetching events count', error);
        }
      );
  }

  static forRoot(): ModuleWithProviders<FeatureEventModule> {
    return {
      ngModule: FeatureEventModule,
      providers: [
        provideAppInitializer(() => {
          const initializerFn = (
            (registry: ModuleRegistryService, module: FeatureEventModule) =>
            () => {
              registry.registerFeature(module.featureModule);
            }
          )(inject(ModuleRegistryService), inject(FeatureEventModule));
          return initializerFn();
        }),
      ],
    };
  }
}
