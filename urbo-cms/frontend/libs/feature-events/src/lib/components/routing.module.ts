import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { EventsComponent } from './events/events.component';
import { CategoriesComponent } from './categories/categories.component';
import { EventDetailsComponent } from './events/event-details/event-details.component';

const routes: Routes = [
  {
    path: '',
    component: EventsComponent,
  },
  {
    path: 'categories',
    component: CategoriesComponent,
  },
  {
    path: ':id',
    component: EventDetailsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {}
