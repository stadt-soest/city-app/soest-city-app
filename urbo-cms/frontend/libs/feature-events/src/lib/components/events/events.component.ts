import { Router } from '@angular/router';
import { Component, inject, OnInit } from '@angular/core';
import { Event } from '../../models/event.model';
import { EventAdapter } from '../../event.adapter';

@Component({
  selector: 'urbo-cms-events',
  templateUrl: './events.component.html',
  standalone: false,
})
export class EventsComponent implements OnInit {
  readonly rows: number = 10;
  events: Event[] = [];
  totalRecords = 0;
  currentSearchTerm = '';
  startDate: Date | undefined;
  endDate: Date | undefined;

  cols: any[] = [
    { field: 'image', header: 'Bild', type: 'image' },
    { field: 'title', header: 'Title', type: 'text' },
    { field: 'subTitle', header: 'Untertitel', type: 'truncated-text' },
    { field: 'description', header: 'Beschreibung', type: 'truncated-text' },
    { field: 'address.locationName', header: 'Location', type: 'text' },
  ];

  private readonly eventService = inject(EventAdapter);
  private readonly router = inject(Router);

  ngOnInit() {
    this.loadData(1, this.rows);
  }

  loadData(page: number, rows: number, searchTerm?: string) {
    this.eventService
      .getEvents(page, rows, searchTerm, this.startDate, this.endDate)
      .subscribe((data) => {
        this.events = data.content;
        this.totalRecords = data.totalRecords;
      });
  }

  onPage(event: any) {
    const page = event.first / event.rows + 1;
    this.loadData(page, event.rows, this.currentSearchTerm);
  }

  onSearch(event: any) {
    const searchQuery: string = event;
    this.currentSearchTerm = searchQuery;
    this.loadData(1, this.rows, searchQuery);
  }

  showEventDetails(clickedRowData: any) {
    this.router.navigate(['/events', clickedRowData['id']]);
  }

  onDateRangeFilterSelected($event: Date[]) {
    this.startDate = $event[0];
    this.endDate = $event[1];
    this.loadData(1, this.rows, this.currentSearchTerm);
  }
}
