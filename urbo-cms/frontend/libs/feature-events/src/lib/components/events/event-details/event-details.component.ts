import { Component, DestroyRef, OnInit } from '@angular/core';
import { take } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ActivatedRoute } from '@angular/router';
import { Address, Event } from '../../../models/event.model';
import { EventAdapter } from '../../../event.adapter';

@Component({
  selector: 'urbo-cms-events-event-details',
  styleUrl: 'event-details.component.scss',
  templateUrl: './event-details.component.html',
  standalone: false,
})
export class EventDetailsComponent implements OnInit {
  event?: Event;
  altDescription = '';

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly eventService: EventAdapter,
    private readonly destroyRef: DestroyRef
  ) {}

  ngOnInit() {
    this.loadEvent();
  }

  loadEvent() {
    this.eventService
      .getEvent(this.activatedRoute.snapshot.params['id'])
      .pipe(take(1), takeUntilDestroyed(this.destroyRef))
      .subscribe((event: Event) => {
        this.event = event;
        this.altDescription = this.event.image?.description ?? '';
      });
  }

  generateGoogleMapsUrl(address: Address) {
    return `https://maps.google.de/maps?q=${address.coordinates?.lat},${address.coordinates?.long}(${address.locationName})`;
  }
}
