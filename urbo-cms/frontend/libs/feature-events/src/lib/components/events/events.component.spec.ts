import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { DatePipe } from '@angular/common';
import { EventsComponent } from './events.component';
import { TableComponent, UITestModule } from '@sw-code/urbo-cms-ui';
import { ImagePathPipe, ImagePipe } from '@sw-code/urbo-cms-core';
import { EventAdapter } from '../../event.adapter';

describe('EventsComponent', () => {
  let component: EventsComponent;
  let fixture: ComponentFixture<EventsComponent>;
  let mockEventService: any;

  beforeEach(async () => {
    mockEventService = {
      getEvents: jest.fn().mockReturnValue(
        of({
          content: [],
          totalRecords: 0,
        })
      ),
    };

    await TestBed.configureTestingModule({
      imports: [UITestModule.forRoot()],
      declarations: [EventsComponent, TableComponent, ImagePipe, ImagePathPipe],
      providers: [
        { provide: EventAdapter, useValue: mockEventService },
        DatePipe,
        ImagePipe,
        ImagePathPipe,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(EventsComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should initialize cols and invoke loadData', () => {
      jest.spyOn(component, 'loadData');
      fixture.detectChanges();

      const expectedCols = [
        { field: 'image', header: 'Bild', type: 'image' },
        { field: 'title', header: 'Title', type: 'text' },
        { field: 'subTitle', header: 'Untertitel', type: 'truncated-text' },
        {
          field: 'description',
          header: 'Beschreibung',
          type: 'truncated-text',
        },
        { field: 'address.locationName', header: 'Location', type: 'text' },
      ];
      expect(component.cols).toEqual(expectedCols);

      expect(component.loadData).toHaveBeenCalledWith(1, component.rows);
    });
  });

  describe('handlePageChange', () => {
    it('should correctly calculate page number and call loadData with appropriate arguments', () => {
      mockEventService.getEvents.mockReturnValue(
        of({
          content: [],
          totalRecords: 0,
        })
      );

      jest.spyOn(component, 'loadData');
      const mockEvent = {
        first: 10,
        rows: 5,
      };
      fixture.detectChanges();

      component.onPage(mockEvent);

      const expectedPage = mockEvent.first / mockEvent.rows + 1;
      expect(component.loadData).toHaveBeenCalledWith(
        expectedPage,
        mockEvent.rows,
        ''
      );
    });
  });

  describe('Table rendering', () => {
    it('should render the Table', () => {
      fixture.detectChanges();
      const compiled = fixture.nativeElement;
      const tableElement = compiled.querySelector('p-table');
      expect(tableElement).toBeTruthy();
    });

    it('should render headers correctly', () => {
      fixture.detectChanges();
      const compiled = fixture.nativeElement;
      const headerCells = Array.from(
        compiled.querySelectorAll('th')
      ) as HTMLElement[];
      const headers = headerCells.map((cell) => cell.textContent!.trim());
      expect(headers.length).toBe(6);
      expect(headers).toEqual([
        'Bild',
        'Title',
        'Untertitel',
        'Beschreibung',
        'Location',
        'Actions',
      ]);
    });
  });
});
