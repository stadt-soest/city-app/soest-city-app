import { Component, EventEmitter, Output } from '@angular/core';
import { catchError, finalize, take } from 'rxjs';
import { EventCategory } from '../../../models/event-categories.model';
import { EventAdapter } from '../../../event.adapter';

@Component({
  selector: 'urbo-cms-events-event-new-category',
  templateUrl: './new-category.component.html',
  standalone: false,
})
export class NewCategoryComponent {
  @Output() created: EventEmitter<void> = new EventEmitter<void>();
  newCategory: EventCategory = new EventCategory();
  loading = false;
  displayDialog = false;

  constructor(private readonly eventCategoryService: EventAdapter) {}

  createCategory() {
    this.loading = true;
    this.eventCategoryService
      .createCategory(this.newCategory)
      .pipe(
        take(1),
        catchError((err) => {
          return err;
        }),
        finalize(() => {
          this.loading = false;
          this.displayDialog = false;
        })
      )
      .subscribe(() => {
        this.created.emit();
        this.newCategory = new EventCategory();
      });
  }

  showDialog() {
    this.displayDialog = true;
  }
}
