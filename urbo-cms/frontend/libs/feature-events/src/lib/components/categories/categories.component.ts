import { Component, OnInit, ViewChild } from '@angular/core';
import { catchError, take } from 'rxjs';
import { ConfirmationService } from 'primeng/api';
import {
  EventCategory,
  MultiSelectEventCategory,
} from '../../models/event-categories.model';
import { EventAdapter } from '../../event.adapter';
import { Permission } from '@sw-code/urbo-cms-core';
import { NewCategoryComponent } from './new-category/new-category.component';

@Component({
  selector: 'urbo-cms-events-event-categories',
  templateUrl: './categories.component.html',
  standalone: false,
})
export class CategoriesComponent implements OnInit {
  @ViewChild(NewCategoryComponent)
  createNewCategoryComponent?: NewCategoryComponent;

  categories: EventCategory[] = [];
  protected readonly Permission = Permission;
  unmappedSourceCategories: string[] = [];
  allSourceCategories: MultiSelectEventCategory[] = [];
  loading = false;

  constructor(
    private readonly eventCategoryService: EventAdapter,
    private readonly confirmationService: ConfirmationService
  ) {}

  ngOnInit(): void {
    this.loadContent();
  }

  loadContent() {
    this.loadCategories();
    this.loadUnmappedCategories();
    this.loadAllSourceCategories();
  }

  private loadCategories() {
    this.eventCategoryService
      .getCategories()
      .pipe(take(1))
      .subscribe((categories) => {
        this.categories = categories;
      });
  }

  private loadUnmappedCategories() {
    this.eventCategoryService
      .getUnmappedSourceCategories()
      .pipe(take(1))
      .subscribe((unmappedSourceCategories) => {
        this.unmappedSourceCategories = unmappedSourceCategories;
      });
  }

  updateCategory(category: EventCategory) {
    this.loading = true;
    this.eventCategoryService
      .updateCategory(category)
      .pipe(
        take(1),
        catchError((err) => {
          this.loading = false;
          return err;
        })
      )
      .subscribe(() => {
        this.loadContent();
        this.loading = false;
      });
  }

  private deleteCategory(category: EventCategory) {
    this.loading = true;
    this.eventCategoryService
      .deleteCategory(category)
      .pipe(
        take(1),
        catchError((err) => {
          this.loading = false;
          return err;
        })
      )
      .subscribe(() => {
        this.loadContent();
        this.loading = false;
      });
  }

  confirmDeleteCategory(category: EventCategory) {
    this.confirmationService.confirm({
      message: 'Bist du dir sicher, dass du diese Kategorie löschen möchtest?',
      header: 'Bestätigung',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Ja',
      rejectLabel: 'Abbrechen',
      acceptButtonStyleClass: 'p-button-danger',
      rejectButtonStyleClass: 'p-button-default',
      accept: () => this.deleteCategory(category),
    });
  }

  private loadAllSourceCategories() {
    this.eventCategoryService
      .getSourceCategories()
      .pipe(take(1))
      .subscribe((allSourceCategories) => {
        this.allSourceCategories = allSourceCategories.map(
          (category) => new MultiSelectEventCategory(category, category)
        );
      });
  }

  showDialog() {
    this.createNewCategoryComponent?.showDialog();
  }
}
