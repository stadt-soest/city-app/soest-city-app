import { Observable } from 'rxjs';
import { Event } from './models/event.model';
import { EventCategory } from './models/event-categories.model';

export abstract class EventAdapter {
  abstract getEvents(
    page: number,
    rows: number,
    searchTerm?: string,
    startDate?: Date,
    endDate?: Date
  ): Observable<{ content: Event[]; totalRecords: number }>;

  abstract getEvent(id: string): Observable<Event>;

  abstract getCategories(): Observable<EventCategory[]>;

  abstract createCategory(category: EventCategory): Observable<EventCategory>;

  abstract getSourceCategories(): Observable<string[]>;

  abstract getUnmappedSourceCategories(): Observable<string[]>;

  abstract updateCategory(category: EventCategory): Observable<EventCategory>;

  abstract deleteCategory(category: EventCategory): Observable<void>;
}
