import { Image } from '@sw-code/urbo-cms-core';

export interface Event {
  id: string;
  title: string;
  address: Address;
  contactPoint: ContactPoint;
  dateModified: Date | null;
  categories: string[];
  subTitle: string | null;
  occurrences: EventOccurrence[];
  description: string | null;
  image: Image | null;
}

export class EventOccurrence {
  id: string;
  status: string;
  source: string;
  dateCreated: Date;
  start: Date;
  end: Date;

  constructor(
    id: string,
    status: string,
    source: string,
    dateCreated: Date,
    start: Date,
    end: Date
  ) {
    this.id = id;
    this.status = status;
    this.source = source;
    this.dateCreated = dateCreated;
    this.start = start;
    this.end = end;
  }
}

export interface DateRange {
  startDate?: string;
  endDate?: string;
}

export interface Address {
  locationName: string;
  street?: string;
  postalCode?: string;
  city?: string;
  coordinates?: Coordinates | null;
}

export interface Coordinates {
  lat: number | null;
  long: number | null;
}

export interface ContactPoint {
  telephone: string | null;
  email: string | null;
}
