import { I18nString } from '@sw-code/urbo-cms-core';

export class EventCategory {
  id?: string;
  title: I18nString;
  sourceCategories: string[];

  constructor(title?: I18nString, sourceCategories?: string[], id?: string) {
    this.title = title ?? new I18nString('', '');
    this.sourceCategories = sourceCategories ?? [];
    this.id = id;
  }
}

export class MultiSelectEventCategory {
  label: string;
  value: string;

  constructor(label: string, value: string) {
    this.label = label;
    this.value = value;
  }
}
