export interface DeadLetterTask {
  id: string;
  payload: string;
  createdAt: string;
  failureReason: string;
}
