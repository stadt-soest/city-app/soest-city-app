import { FileUploadModule } from 'primeng/fileupload';
import { UserListComponent } from './components/user/user-list/user-list.component';
import { UserCreateComponent } from './components/user/user-create/user-create.component';
import { RoleListComponent } from './components/role/role-list/role-list.component';
import { RoleCreateComponent } from './components/role/role-create/role-create.component';
import { DeadLetterTasksComponent } from './components/dead-letter-queue/dead-letter-tasks.component';
import {
  ModuleWithProviders,
  NgModule,
  inject,
  provideAppInitializer,
} from '@angular/core';
import {
  FeatureModule,
  MenuItem,
  ModuleCategory,
  ModuleInfo,
  ModuleRegistryService,
  Permission,
} from '@sw-code/urbo-cms-core';
import { UIModule } from '@sw-code/urbo-cms-ui';

const MODULES = [FileUploadModule, UIModule];
const COMPONENTS = [
  UserListComponent,
  UserCreateComponent,
  RoleListComponent,
  RoleCreateComponent,
  DeadLetterTasksComponent,
];
const PROVIDERS: never[] = [];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES],
  exports: [...MODULES],
  providers: [...PROVIDERS],
})
export class FeatureAdminModule {
  readonly featureModule: FeatureModule;

  constructor() {
    this.featureModule = {
      info: new ModuleInfo(
        'Administration',
        ModuleCategory.MODULES,
        'pi pi-shield',
        'admin',
        [
          new MenuItem('Benutzer', 'pi pi-users', 'admin/user', [
            new MenuItem('Liste', 'pi pi-list', 'admin/user/list'),
            new MenuItem('Erstellen', 'pi pi-plus', 'admin/user/create'),
          ]),
          new MenuItem('Rollen', 'pi pi-id-card', 'admin/role', [
            new MenuItem('Liste', 'pi pi-list', 'admin/role/list'),
            new MenuItem('Erstellen', 'pi pi-plus', 'admin/role/create'),
          ]),
          new MenuItem(
            'DeadLetter Queue',
            'pi pi-exclamation-triangle',
            'admin/dead-letter',
            [new MenuItem('Liste', 'pi pi-list', 'admin/dead-letter')]
          ),
        ],
        [Permission.Administration]
      ),
      routes: [
        {
          path: 'admin',
          loadChildren: () =>
            import('./components/routing.module').then((m) => m.RoutingModule),
        },
      ],
    };
  }

  static forRoot(): ModuleWithProviders<FeatureAdminModule> {
    return {
      ngModule: FeatureAdminModule,
      providers: [
        provideAppInitializer(() => {
          const initializerFn = (
            (registry: ModuleRegistryService, module: FeatureAdminModule) =>
            () => {
              registry.registerFeature(module.featureModule);
            }
          )(inject(ModuleRegistryService), inject(FeatureAdminModule));
          return initializerFn();
        }),
      ],
    };
  }
}
