import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AdminAdapter } from '../../../admin.adapter';

@Component({
  selector: 'urbo-cms-admin-role-create',
  templateUrl: './role-create.component.html',
  standalone: false,
})
export class RoleCreateComponent {
  roleForm: FormGroup;
  permissions: { label: string; value: string }[] = [];

  constructor(
    private readonly adminAdapter: AdminAdapter,
    private readonly fb: FormBuilder,
    private readonly router: Router
  ) {
    this.roleForm = this.fb.group({
      name: ['', Validators.required],
      permissions: [[], Validators.required],
    });

    this.loadPermissions();
  }

  loadPermissions() {
    this.adminAdapter.getPermissions().subscribe((permissions: string[]) => {
      this.permissions = permissions.map((p) => ({
        label: p.toString(),
        value: p,
      }));
    });
  }

  onCreateRole() {
    if (this.roleForm?.valid) {
      const roleCreateDto = {
        name: this.roleForm.value.name,
        permissions: this.roleForm.value.permissions,
      };

      this.adminAdapter.createRole(roleCreateDto).subscribe({
        next: () => {
          this.router.navigate(['/admin/role/list']);
        },
      });
    }
  }
}
