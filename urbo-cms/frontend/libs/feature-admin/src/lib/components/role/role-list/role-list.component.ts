import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Permission } from '@sw-code/urbo-cms-core';
import { Role } from '../../../model/role.model';
import { AdminAdapter } from '../../../admin.adapter';

@Component({
  selector: 'urbo-cms-admin-role-list',
  templateUrl: './role-list.component.html',
  standalone: false,
})
export class RoleListComponent implements OnInit {
  roles: Role[] = [];
  protected readonly Permission = Permission;
  selectedRole?: Role;
  roleDialog = false;
  deleteRoleDialog = false;
  editMode = false;
  permissions: { label: string; value: string }[] = [];
  totalRecords = 0;
  rows = 10;

  cols = [
    { header: 'Rollenname', field: 'name', type: 'text' },
    { header: 'Berechtigungen', field: 'permissions', type: 'text' },
  ];

  constructor(
    private readonly adminAdapter: AdminAdapter,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.loadRoles();
    this.loadPermissions();
  }

  loadRoles() {
    this.adminAdapter.getRoles().subscribe((roles) => {
      this.roles = roles;
      this.totalRecords = roles.length;
    });
  }

  loadPermissions() {
    this.adminAdapter.getPermissions().subscribe((permissions: string[]) => {
      this.permissions = permissions.map((p) => ({
        label: p.toString(),
        value: p,
      }));
    });
  }

  editRole(role: Role) {
    this.selectedRole = { ...role };
    this.editMode = true;
    this.roleDialog = true;
  }

  deleteRole(role: Role) {
    this.selectedRole = { ...role };
    this.deleteRoleDialog = true;
  }

  confirmDelete() {
    if (this.selectedRole) {
      this.adminAdapter.deleteRole(this.selectedRole.id).subscribe({
        next: () => {
          this.deleteRoleDialog = false;
          this.loadRoles();
        },
      });
    }
  }

  hideDialog() {
    this.roleDialog = false;
    this.selectedRole = undefined;
  }

  saveRole() {
    if (this.editMode && this.selectedRole) {
      this.adminAdapter
        .updateRole({
          id: this.selectedRole.id,
          name: this.selectedRole.name,
          permissions: this.selectedRole.permissions,
        })
        .subscribe({
          next: () => {
            this.roleDialog = false;
            this.loadRoles();
          },
        });
    }
  }

  navigateToCreateRole() {
    this.router.navigate(['/admin/role/create']);
  }
}
