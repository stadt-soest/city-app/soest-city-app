import { Component, inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { DatePipe } from '@angular/common';
import { UIModule } from '@sw-code/urbo-cms-ui';
import { AdminAdapter } from '../../../admin.adapter';
import { DeadLetterTask } from '../../../model/dead-letter-task.model';

@Component({
  selector: 'urbo-cms-admin-dead-letter-task-detail',
  imports: [DatePipe, UIModule],
  templateUrl: './dead-letter-task-detail.component.html',
  styleUrl: './dead-letter-task-detail.component.scss',
})
export class DeadLetterTaskDetailComponent implements OnInit {
  task?: DeadLetterTask;
  private readonly activatedRoute = inject(ActivatedRoute);
  private readonly router = inject(Router);
  private readonly confirmationService = inject(ConfirmationService);
  private readonly adminAdapter = inject(AdminAdapter);

  ngOnInit() {
    this.loadTask();
  }

  confirmDelete() {
    this.confirmationService.confirm({
      message:
        'Bist du dir sicher, dass du diesen Eintrag löschen möchtest? Dies kann nicht rückgängig gemacht werden.',
      header: 'Löschen bestätigen',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Ja',
      rejectLabel: 'Nein',
      accept: () => {
        this.deleteTask();
      },
    });
  }

  confirmRequeue() {
    this.confirmationService.confirm({
      message:
        'Bist du dir sicher, dass du diesen Eintrag erneut in die Warteschlange stellen möchtest?',
      header: 'Warteschlange bestätigen',
      acceptLabel: 'Ja',
      rejectLabel: 'Nein',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.requeueTask();
      },
    });
  }

  loadTask() {
    const taskId = this.activatedRoute.snapshot.params['id'];
    this.adminAdapter.fetchDeadLetterTask(taskId).subscribe((task) => {
      this.task = task;
    });
  }

  deleteTask() {
    if (this.task) {
      this.adminAdapter
        .deleteDeadLetterTask(this.task.id)
        .subscribe(async () => {
          await this.router.navigate(['/admin/dead-letter']);
        });
    }
  }

  requeueTask() {
    if (this.task) {
      this.adminAdapter
        .requeueDeadLetterTask(this.task.id)
        .subscribe(async () => {
          await this.router.navigate(['/admin/dead-letter']);
        });
    }
  }
}
