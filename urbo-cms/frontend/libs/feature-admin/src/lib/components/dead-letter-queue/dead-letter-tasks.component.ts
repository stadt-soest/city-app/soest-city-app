import { Component, inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Permission } from '@sw-code/urbo-cms-core';
import { AdminAdapter } from '../../admin.adapter';
import { DeadLetterTask } from '../../model/dead-letter-task.model';

@Component({
  selector: 'urbo-cms-admin-dead-letter-queue',
  template: `
    <urbo-cms-ui-table
      [data]="dataSource"
      [columns]="cols"
      [rows]="rows"
      [totalRecords]="totalRecords"
      [paginator]="true"
      [lazy]="false"
      (pageChange)="onPageChange($event)"
      (view)="showTaskDetails($event)"
      [viewPermission]="Permission.Administration"
    ></urbo-cms-ui-table>
  `,
  standalone: false,
})
export class DeadLetterTasksComponent implements OnInit {
  cols = [
    { header: 'ID', field: 'id' },
    { header: 'Payload', field: 'payload' },
    { header: 'Erstellungsdatum', field: 'createdAt' },
  ];
  protected readonly Permission = Permission;
  dataSource: DeadLetterTask[] = [];
  totalRecords = 0;
  rows = 10;
  currentPage = 0;
  private readonly router = inject(Router);
  private readonly adminAdapter = inject(AdminAdapter);

  ngOnInit() {
    this.loadData();
  }

  async showTaskDetails(clickedRowData: DeadLetterTask) {
    await this.router.navigate(['admin/dead-letter', clickedRowData['id']]);
  }

  loadData() {
    this.adminAdapter.fetchDeadLetterTasks().subscribe((data) => {
      this.dataSource = data;
      this.totalRecords = data.length;
    });
  }

  onPageChange(event: { first: number; rows: number }) {
    this.currentPage = event.first;
    this.rows = event.rows;
    this.loadData();
  }
}
