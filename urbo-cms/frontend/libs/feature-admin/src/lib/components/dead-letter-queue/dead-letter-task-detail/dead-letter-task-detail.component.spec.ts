import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DeadLetterTaskDetailComponent } from './dead-letter-task-detail.component';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  Router,
} from '@angular/router';
import { of } from 'rxjs';
import { ConfirmationService } from 'primeng/api';
import { AdminAdapter } from '../../../admin.adapter';

describe('DeadLetterTaskDetailComponent', () => {
  let component: DeadLetterTaskDetailComponent;
  let fixture: ComponentFixture<DeadLetterTaskDetailComponent>;
  let mockDeadLetterQueueService: jest.Mocked<AdminAdapter>;
  let mockRouter: Partial<Router>;
  let mockActivatedRoute: Partial<ActivatedRoute>;

  const mockSnapshot: Partial<ActivatedRouteSnapshot> = {
    params: { id: '123' },
  };

  beforeEach(async () => {
    mockDeadLetterQueueService = {
      fetchDeadLetterTask: jest.fn().mockReturnValue(of()),
      deleteDeadLetterTask: jest.fn().mockReturnValue(of()),
      requeueDeadLetterTask: jest.fn().mockReturnValue(of()),
    } as unknown as jest.Mocked<AdminAdapter>;
    mockRouter = {
      navigate: jest.fn(),
    };
    mockActivatedRoute = {
      snapshot: mockSnapshot as ActivatedRouteSnapshot,
    };

    await TestBed.configureTestingModule({
      imports: [DeadLetterTaskDetailComponent],
      providers: [
        { provide: AdminAdapter, useValue: mockDeadLetterQueueService },
        { provide: Router, useValue: mockRouter },
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        ConfirmationService,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(DeadLetterTaskDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load task on init', () => {
    const task = {
      id: '123',
      payload: 'Data',
      createdAt: '2021-07-19T07:22:00Z',
      failureReason: 'Error',
    };
    mockDeadLetterQueueService.fetchDeadLetterTask.mockReturnValue(of(task));
    component.ngOnInit();

    expect(mockDeadLetterQueueService.fetchDeadLetterTask).toHaveBeenCalledWith(
      '123'
    );
    expect(component.task).toEqual(task);
  });

  it('should delete the task and navigate', () => {
    component.task = {
      id: '123',
      payload: '',
      createdAt: new Date().toISOString(),
      failureReason: '',
    };
    mockDeadLetterQueueService.deleteDeadLetterTask.mockReturnValue(
      of(undefined)
    );
    component.deleteTask();

    expect(
      mockDeadLetterQueueService.deleteDeadLetterTask
    ).toHaveBeenCalledWith('123');
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/admin/dead-letter']);
  });

  it('should requeue the task and navigate', async () => {
    component.task = {
      id: '123',
      payload: '',
      createdAt: new Date().toISOString(),
      failureReason: '',
    };
    mockDeadLetterQueueService.requeueDeadLetterTask.mockReturnValue(
      of({
        id: '123',
        payload: '',
        createdAt: new Date().toISOString(),
        failureReason: '',
      })
    );
    component.requeueTask();

    expect(
      mockDeadLetterQueueService.requeueDeadLetterTask
    ).toHaveBeenCalledWith('123');
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/admin/dead-letter']);
  });
});
