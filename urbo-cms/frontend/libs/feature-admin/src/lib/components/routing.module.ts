import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { RoleListComponent } from './role/role-list/role-list.component';
import { RoleCreateComponent } from './role/role-create/role-create.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { UserCreateComponent } from './user/user-create/user-create.component';
import { DeadLetterTasksComponent } from './dead-letter-queue/dead-letter-tasks.component';
import { DeadLetterTaskDetailComponent } from './dead-letter-queue/dead-letter-task-detail/dead-letter-task-detail.component';

const routes: Routes = [
  {
    path: '',
    data: { breadcrumb: 'Admin' },
    children: [
      {
        path: 'user',
        data: { breadcrumb: 'User' },
        children: [
          {
            path: 'list',
            component: UserListComponent,
            data: { breadcrumb: 'Liste' },
          },
          {
            path: 'create',
            component: UserCreateComponent,
            data: { breadcrumb: 'Erstellen' },
          },
        ],
      },
      {
        path: 'role',
        data: { breadcrumb: 'Role' },
        children: [
          {
            path: 'list',
            component: RoleListComponent,
            data: { breadcrumb: 'Liste' },
          },
          {
            path: 'create',
            component: RoleCreateComponent,
            data: { breadcrumb: 'Erstellen' },
          },
        ],
      },
      {
        path: 'dead-letter',
        data: { breadcrumb: 'DeadLetter Queue' },
        children: [
          {
            path: '',
            component: DeadLetterTasksComponent,
            data: { breadcrumb: 'Tasks' },
          },
          {
            path: ':id',
            component: DeadLetterTaskDetailComponent,
            data: { breadcrumb: 'Task Detail' },
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {}
