import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { take } from 'rxjs';
import { AdminAdapter } from '../../../admin.adapter';
import { Role } from '../../../model/role.model';

@Component({
  templateUrl: './user-create.component.html',
  standalone: false,
})
export class UserCreateComponent implements OnInit {
  userForm: FormGroup;
  roles: Role[] = [];

  constructor(
    private readonly adminAdapter: AdminAdapter,
    private readonly fb: FormBuilder,
    private readonly router: Router
  ) {
    this.userForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      roles: [[]],
    });
  }

  ngOnInit() {
    this.loadRoles();
  }

  onCreateUser() {
    if (this.userForm?.valid) {
      const userCreateDto = {
        firstName: this.userForm.value.firstName,
        lastName: this.userForm.value.lastName,
        email: this.userForm.value.email,
        roles: this.userForm.value.roles,
      };

      this.adminAdapter
        .createUser(userCreateDto)
        .pipe(take(1))
        .subscribe({
          next: () => {
            this.router.navigate(['/admin/user/list']);
          },
        });
    }
  }

  private loadRoles() {
    this.adminAdapter.getRoles().subscribe((roles) => {
      this.roles = roles;
    });
  }
}
