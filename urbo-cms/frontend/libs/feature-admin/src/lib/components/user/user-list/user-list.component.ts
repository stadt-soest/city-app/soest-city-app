import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs';
import { TableColumn } from '@sw-code/urbo-cms-ui';
import { AdminAdapter } from '../../../admin.adapter';
import { Role } from '../../../model/role.model';
import { User } from '../../../model/user.model';
import { Permission } from '@sw-code/urbo-cms-core';

@Component({
  templateUrl: './user-list.component.html',
  standalone: false,
})
export class UserListComponent implements OnInit {
  users: User[] = [];
  protected readonly Permission = Permission;
  roles: Role[] = [];
  selectedUser?: User;
  userDialog = false;
  deleteUserDialog = false;
  editMode = false;
  resetPasswordDialog = false;
  columns: TableColumn[] = [
    { field: 'firstName', header: 'Vorname', type: 'text' },
    { field: 'lastName', header: 'Nachname', type: 'text' },
    { field: 'email', header: 'E-mail', type: 'text' },
    { field: 'roles', header: 'Rollen', type: 'text' },
  ];

  constructor(
    private readonly router: Router,
    private readonly adminAdapter: AdminAdapter
  ) {}

  ngOnInit() {
    this.loadUsers();
    this.loadRoles();
  }

  loadUsers() {
    this.adminAdapter.getUsers().subscribe((users: User[]) => {
      this.users = users;
    });
  }

  loadRoles() {
    this.adminAdapter.getRoles().subscribe((roles) => {
      this.roles = roles;
    });
  }

  editUser(user: User) {
    this.selectedUser = { ...user };
    this.selectedUser.roles = [...user.roles];
    this.editMode = true;
    this.userDialog = true;
  }

  deleteUser(user: User) {
    this.selectedUser = { ...user };
    this.deleteUserDialog = true;
  }

  confirmDelete() {
    if (this.selectedUser?.id) {
      this.adminAdapter
        .deleteUser(this.selectedUser.id)
        .pipe(take(1))
        .subscribe({
          next: () => {
            this.deleteUserDialog = false;
            this.loadUsers();
          },
        });
    } else {
      this.deleteUserDialog = false;
    }
  }

  hideDialog() {
    this.userDialog = false;
    this.selectedUser = undefined;
  }

  saveUser() {
    if (this.editMode && this.selectedUser) {
      this.adminAdapter
        .updateUser(this.selectedUser.id, this.selectedUser)
        .pipe(take(1))
        .subscribe({
          next: () => {
            this.userDialog = false;
            this.loadUsers();
          },
        });
    }
  }

  confirmResetPassword(user: User) {
    this.selectedUser = { ...user };
    this.resetPasswordDialog = true;
  }

  resetPassword() {
    if (this.selectedUser) {
      this.adminAdapter.sendPasswordResetEmail(this.selectedUser.id).subscribe({
        next: () => {
          this.resetPasswordDialog = false;
        },
      });
    }
  }

  navigateToCreateUser() {
    this.router.navigate(['/admin/user/create']);
  }
}
