import { Permission } from '@sw-code/urbo-cms-core';
import { Observable } from 'rxjs';
import { Role } from './model/role.model';
import { Injectable } from '@angular/core';
import { DeadLetterTask } from './model/dead-letter-task.model';
import { User } from './model/user.model';

@Injectable()
export abstract class AdminAdapter {
  abstract getUsers(): Observable<User[]>;

  abstract createUser(user: Partial<User>): Observable<User>;

  abstract deleteUser(id: string): Observable<User>;

  abstract updateUser(id: string, user: User): Observable<User>;

  abstract sendPasswordResetEmail(id: string): Observable<void>;

  abstract getRoles(): Observable<Role[]>;

  abstract getPermissions(): Observable<string[]>;

  abstract deleteRole(id: string): Observable<void>;

  abstract createRole(role: {
    name: string;
    permissions: Array<Permission>;
  }): Observable<Role>;

  abstract updateRole(role: Role): Observable<Role>;

  abstract fetchDeadLetterTasks(): Observable<DeadLetterTask[]>;

  abstract fetchDeadLetterTask(id: string): Observable<DeadLetterTask>;

  abstract deleteDeadLetterTask(id: string): Observable<void>;

  abstract requeueDeadLetterTask(id: string): Observable<DeadLetterTask>;
}
