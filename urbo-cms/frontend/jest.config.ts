import { getJestProjectsAsync } from '@nx/jest';

export default async () => ({
  projects: await getJestProjectsAsync(),
  transformIgnorePatterns: ['node_modules/(?!keycloak-js)'],
});
