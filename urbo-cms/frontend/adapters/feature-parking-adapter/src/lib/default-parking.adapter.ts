import { inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import {
  ParkingApiClient,
  V1ParkingAreasGet200Response,
} from '@sw-code/urbo-cms-backend-api';
import { Parking, ParkingAdapter } from '@sw-code/urbo-cms-feature-parking';
import { ParkingMapper } from './mapper/parking.mapper';
import {
  OpeningHoursMap,
  SpecialOpeningHoursMap,
} from '@sw-code/urbo-cms-core';

@Injectable()
export class DefaultParkingAdapter implements ParkingAdapter {
  private readonly parkingApiClient = inject(ParkingApiClient);
  private readonly parkingMapper = inject(ParkingMapper);

  getParkingAreas(page?: number, size?: number): Observable<Parking[]> {
    const pageNumber = page ?? 0;
    const pageSize = size ?? 10;
    return this.parkingApiClient.v1ParkingAreasGet(pageNumber, pageSize).pipe(
      take(1),
      map((response: V1ParkingAreasGet200Response) =>
        this.parkingMapper.mapToParking(response.content ?? [])
      )
    );
  }

  getParkingAreaById(id: string): Observable<Parking> {
    return this.parkingApiClient.v1ParkingAreasIdGet(id).pipe(
      take(1),
      map((response) =>
        this.parkingMapper.transformParkingAreaResponseToParking(response)
      )
    );
  }

  updateRegularParkingOpeningTimes(
    parkingId: string,
    openingTimes: OpeningHoursMap
  ): Observable<Parking> {
    return this.parkingApiClient
      .v1ParkingAreasIdOpeningTimesPut(
        parkingId,
        this.parkingMapper.mapToUpdateParkingAreaOpeningTimesRequest(
          openingTimes
        )
      )
      .pipe(
        take(1),
        map((response) =>
          this.parkingMapper.transformParkingAreaResponseToParking(response)
        )
      );
  }

  updateSpecialParkingOpeningTimes(
    parkingId: string,
    openingTimes: SpecialOpeningHoursMap
  ): Observable<Parking> {
    return this.parkingApiClient
      .v1ParkingAreasIdSpecialOpeningTimesPut(
        parkingId,
        this.parkingMapper.mapToUpdateSpecialParkingAreaOpeningTimesRequest(
          openingTimes
        )
      )
      .pipe(
        take(1),
        map((response) =>
          this.parkingMapper.transformParkingAreaResponseToParking(response)
        )
      );
  }
}
