import { Injectable } from '@angular/core';
import { Parking, ParkingStatus } from '@sw-code/urbo-cms-feature-parking';
import {
  ParkingAreaResponseDto,
  ParkingOpeningHours,
  ParkingSpecialOpeningHoursResponse,
  UpdateParkingAreaOpeningTimesRequest,
  UpdateParkingAreaSpecialOpeningTimesRequest,
} from '@sw-code/urbo-cms-backend-api';
import {
  DayOfWeek,
  OpeningHoursMap,
  SpecialOpeningHoursMap,
} from '@sw-code/urbo-cms-core';

@Injectable({ providedIn: 'root' })
export class ParkingMapper {
  private readonly statusMap = {
    OPEN: ParkingStatus.OPEN,
    CLOSED: ParkingStatus.CLOSED,
    DISTURBANCE: ParkingStatus.UNKNOWN,
  };

  mapToParking(parkingAreas: ParkingAreaResponseDto[]): Parking[] {
    return parkingAreas.map((area) =>
      this.transformParkingAreaResponseToParking(area)
    );
  }

  transformParkingAreaResponseToParking(area: ParkingAreaResponseDto): Parking {
    return new Parking({
      id: area.id,
      name: area.name,
      status: this.statusMap[area.status],
      coordinates: {
        lat: area.location.coordinates.latitude,
        long: area.location.coordinates.longitude,
      },
      openingHours: this.mapRegularOpeningHours(area.openingTimes),
      specialOpeningHours: area.specialOpeningTimes
        ? this.mapSpecialOpeningHours(area.specialOpeningTimes)
        : undefined,
      parkingCapacity: {
        total: area.capacity.total,
        remaining: area.capacity.remaining,
      },
      address: {
        street: area.location.address.street,
        postalCode: area.location.address.postalCode,
        city: area.location.address.city,
      },
      maximumAllowedHeight: area.maximumAllowedHeight ?? null,
      displayName: area.displayName,
      parkingType: area.parkingType,
    });
  }

  private mapRegularOpeningHours(openingTimes: {
    [key: string]: ParkingOpeningHours;
  }): OpeningHoursMap {
    return Object.values(DayOfWeek).reduce((acc, day) => {
      const hours = openingTimes[day];
      acc[day] = { from: hours?.from ?? '', to: hours?.to ?? '' };
      return acc;
    }, {} as OpeningHoursMap);
  }

  private mapSpecialOpeningHours(
    specialOpeningTimes: ParkingSpecialOpeningHoursResponse[]
  ): SpecialOpeningHoursMap {
    const mappedSpecialOpeningHours: SpecialOpeningHoursMap = {};

    specialOpeningTimes.forEach(({ date, from, to, descriptions }) => {
      mappedSpecialOpeningHours[date] = {
        from: from ?? '',
        to: to ?? '',
        descriptions: descriptions ?? [],
      };
    });

    return mappedSpecialOpeningHours;
  }

  mapToUpdateParkingAreaOpeningTimesRequest(
    openingHours: OpeningHoursMap
  ): UpdateParkingAreaOpeningTimesRequest {
    const mappedOpeningTimes: { [key: string]: ParkingOpeningHours } = {};

    Object.entries(openingHours).forEach(([day, { from, to }]) => {
      if (from || to) {
        mappedOpeningTimes[day] = { from, to };
      }
    });

    return { openingTimes: mappedOpeningTimes };
  }

  mapToUpdateSpecialParkingAreaOpeningTimesRequest(
    openingTimes: SpecialOpeningHoursMap
  ): UpdateParkingAreaSpecialOpeningTimesRequest {
    const mappedSpecialOpeningTimes = Object.entries(openingTimes).map(
      ([date, { from, to, descriptions }]) => ({
        date,
        from,
        to,
        descriptions: descriptions ?? [],
      })
    );

    return { specialOpeningTimes: mappedSpecialOpeningTimes };
  }
}
