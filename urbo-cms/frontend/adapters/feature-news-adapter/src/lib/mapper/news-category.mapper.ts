import { Injectable } from '@angular/core';
import { AbstractMapper } from '@sw-code/urbo-cms-core';
import {
  CategoryCreateDto,
  CategoryResponseDto,
  CategoryType,
  CategoryUpdateDto,
  Language,
} from '@sw-code/urbo-cms-backend-api';
import { NewsCategory } from '@sw-code/urbo-cms-feature-news';
import { NewsI18nStringMapper } from './news-i18n-string-mapper';

@Injectable({ providedIn: 'root' })
export class NewsCategoryMapper extends AbstractMapper<
  CategoryResponseDto,
  NewsCategory
> {
  constructor(private readonly i18nStringMapper: NewsI18nStringMapper) {
    super();
  }

  mapToModel(dto: CategoryResponseDto): NewsCategory {
    const category = new NewsCategory();
    category.id = dto.id;
    category.sourceCategories = dto.sourceCategories ?? [];
    category.title = this.i18nStringMapper.mapToModel(dto.localizedNames);
    return category;
  }

  mapToDto(model: NewsCategory): CategoryResponseDto {
    return {
      id: model.id!,
      type: CategoryType.News,
      localizedNames: this.i18nStringMapper.mapToDto(model.title),
      sourceCategories: model.sourceCategories,
    };
  }

  mapToCreateDto(model: NewsCategory): CategoryCreateDto {
    return {
      type: CategoryType.News,
      localizedNames: [
        { language: Language.De, value: model.title.de! },
        { language: Language.En, value: model.title.en! },
      ],
    };
  }

  mapToUpdateDto(model: NewsCategory): CategoryUpdateDto {
    return {
      localizedNames: [
        { language: Language.De, value: model.title.de! },
        { language: Language.En, value: model.title.en! },
      ],
      sourceCategories: model.sourceCategories,
    };
  }
}
