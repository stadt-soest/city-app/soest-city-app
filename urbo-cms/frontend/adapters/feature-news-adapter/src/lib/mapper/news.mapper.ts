import { Injectable } from '@angular/core';
import { AbstractMapper } from '@sw-code/urbo-cms-core';
import {
  Language,
  MappedCategoryDto,
  NewsResponseDto,
} from '@sw-code/urbo-cms-backend-api';
import { ImageMapper } from './image.mapper';
import { DetailedCategory, News } from '@sw-code/urbo-cms-feature-news';

@Injectable({ providedIn: 'root' })
export class NewsMapper extends AbstractMapper<NewsResponseDto, News> {
  constructor(private readonly imageMapper: ImageMapper) {
    super();
  }

  mapToModel(dto: NewsResponseDto): News {
    try {
      return {
        id: dto.id,
        title: dto.title,
        editable: dto.editable,
        subTitle: dto.subtitle,
        source: dto.source,
        date: new Date(dto.date),
        modified: dto.modified ? new Date(dto.modified) : undefined,
        content: dto.content,
        excerpt: dto.excerpt,
        link: dto.link ?? undefined,
        authors: dto.authors,
        categories: this.mapCategoriesToJoinedString(dto.categories),
        detailedCategories: this.mapToDetailedCategories(dto.categories),
        image: dto.image?.imageSource
          ? this.imageMapper.mapToModel(dto.image)
          : null,
        highlight: dto.highlight ?? false,
      };
    } catch (error) {
      console.error('Mapping of News failed in the NewsMapper:', error);
      throw new Error('Error while mapping News.');
    }
  }

  private mapCategoriesToJoinedString(dtos: MappedCategoryDto[]): string[] {
    if (!dtos) return [];

    return dtos.map((categoryDto) => {
      const germanValue = categoryDto.localizedNames?.find(
        (localization) => localization.language === Language.De
      );
      return (
        germanValue?.value ?? `${categoryDto.sourceCategory} (Quellkategorie)`
      );
    });
  }

  private mapToDetailedCategories(
    dtos: MappedCategoryDto[]
  ): DetailedCategory[] {
    return dtos.map((categoryDto) => ({
      id: categoryDto.id ?? '',
      sourceCategory: categoryDto.sourceCategory ?? '',
      localizedNames:
        categoryDto.localizedNames?.map((localization) => ({
          language: localization.language,
          value: localization.value,
        })) ?? [],
    }));
  }

  mapToDto(_model: News): NewsResponseDto {
    throw new Error('Method not implemented.');
  }
}
