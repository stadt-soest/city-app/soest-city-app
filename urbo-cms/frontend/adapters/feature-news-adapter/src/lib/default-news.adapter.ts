import { inject, Injectable } from '@angular/core';
import {
  News,
  NewsAdapter,
  NewsCategory,
} from '@sw-code/urbo-cms-feature-news';
import { NewsMapper } from './mapper/news.mapper';
import { NewsCategoryMapper } from './mapper/news-category.mapper';
import { catchError, map, Observable, of, take } from 'rxjs';
import {
  CategoryApiClient,
  CategoryType,
  CreateNewsDto,
  FileApiClient,
  HighlightNewsDto,
  NewsApiClient,
  NewsResponseDto,
  UpdateNewsDto,
  V1NewsGet200Response,
} from '@sw-code/urbo-cms-backend-api';
import { NewsApiClient as NewsSearchApiClient } from '@sw-code/urbo-cms-search-api';

@Injectable()
export class DefaultNewsAdapter implements NewsAdapter {
  private readonly newsMapper = inject(NewsMapper);
  private readonly newsCategoryMapper = inject(NewsCategoryMapper);
  private readonly newsApiClient = inject(NewsApiClient);
  private readonly newsSearchApiClient = inject(NewsSearchApiClient);
  private readonly categoryApiClient = inject(CategoryApiClient);
  private readonly fileApiClient = inject(FileApiClient);

  deleteNews(id: string): Observable<void> {
    return this.newsApiClient.v1NewsIdDelete(id).pipe(take(1));
  }

  getBySearchTerm(
    searchTerm: string,
    page?: number,
    size?: number,
    sortField?: string,
    sortOrder?: string
  ): Observable<{
    total: number;
    items: News[];
  }> {
    const pageNumber = page ?? 1;
    const pageSize = size ?? 10;

    return this.newsSearchApiClient
      .indexesNewsSearchPost({
        q: searchTerm,
        page: pageNumber,
        hitsPerPage: pageSize,
        sort:
          sortField && sortOrder ? [`${sortField}:${sortOrder}`] : undefined,
      })
      .pipe(
        map((response) => {
          const items = this.newsMapper.mapToModelArray(response.hits ?? []);
          const total = response.totalHits ?? 0;
          return { items, total };
        }),
        catchError(() => {
          return of({ items: [], total: 0 });
        })
      );
  }

  getDetailNews(id: string): Observable<News> {
    return this.newsApiClient.v1NewsIdGet(id).pipe(
      take(1),
      map((response: NewsResponseDto) => this.newsMapper.mapToModel(response))
    );
  }

  getNews(
    page: number,
    rows: number,
    sourceType: 'EXTERNAL' | 'INTERNAL' | null,
    sortField = 'date',
    sortOrder = 'desc',
    highlighted?: boolean
  ): Observable<{
    content: News[];
    totalRecords: number;
  }> {
    return this.newsApiClient
      .v1NewsGet(
        page,
        rows,
        [`${sortField},${sortOrder}`],
        undefined,
        undefined,
        undefined,
        highlighted ?? undefined,
        sourceType ?? undefined
      )
      .pipe(
        map((response: V1NewsGet200Response) => {
          const citizenServices = this.newsMapper.mapToModelArray(
            response.content!
          );
          return {
            content: citizenServices,
            totalRecords: response.page?.totalElements ?? 0,
          };
        })
      );
  }

  updateNews(id: string, newsForm: UpdateNewsDto): Observable<News> {
    const updateNewsDto: UpdateNewsDto = {
      title: newsForm.title,
      subtitle: newsForm.subtitle,
      content: newsForm.content,
      source: newsForm.source,
      link: newsForm.link,
      imageUrl: newsForm.imageUrl,
      categoryIds: newsForm.categoryIds,
      authors: newsForm.authors,
      highlight: newsForm.highlight,
    };
    return this.newsApiClient.v1NewsIdPut(id, updateNewsDto).pipe(
      take(1),
      map((dto) => {
        return this.newsMapper.mapToModel(dto);
      })
    );
  }

  updateNewsHighlightStatus(
    id: string,
    highlightStatus: boolean
  ): Observable<News> {
    const updateNewsDto: HighlightNewsDto = {
      highlight: highlightStatus,
    };
    return this.newsApiClient.v1NewsIdHighlightPut(id, updateNewsDto).pipe(
      take(1),
      map((dto) => this.newsMapper.mapToModel(dto))
    );
  }

  createCategory(category: NewsCategory): Observable<NewsCategory> {
    return this.categoryApiClient
      .v1CategoriesPost(this.newsCategoryMapper.mapToCreateDto(category))
      .pipe(
        take(1),
        map((dto) => this.newsCategoryMapper.mapToModel(dto))
      );
  }

  deleteCategory(category: NewsCategory): Observable<void> {
    return this.categoryApiClient
      .v1CategoriesIdDelete(category.id!)
      .pipe(take(1));
  }

  getCategories(): Observable<NewsCategory[]> {
    return this.categoryApiClient.v1CategoriesGet(CategoryType.News).pipe(
      take(1),
      map((categories) => this.newsCategoryMapper.mapToModelArray(categories))
    );
  }

  getSourceCategories(): Observable<string[]> {
    return this.categoryApiClient
      .v1SourceCategoriesGet(CategoryType.News)
      .pipe(take(1));
  }

  getUnmappedSourceCategories(): Observable<string[]> {
    return this.categoryApiClient
      .v1SourceCategoriesGet(CategoryType.News, false)
      .pipe(take(1));
  }

  updateCategory(category: NewsCategory): Observable<NewsCategory> {
    return this.categoryApiClient
      .v1CategoriesIdPut(
        category.id!,
        this.newsCategoryMapper.mapToUpdateDto(category)
      )
      .pipe(
        take(1),
        map((dto) => this.newsCategoryMapper.mapToModel(dto))
      );
  }

  uploadFile(file: Blob): Observable<string> {
    return this.fileApiClient.filesPost(file).pipe(take(1));
  }

  createNews(newsForm: CreateNewsDto): Observable<News> {
    return this.newsApiClient.v1NewsPost(newsForm).pipe(
      take(1),
      map((dto) => this.newsMapper.mapToModel(dto))
    );
  }
}
