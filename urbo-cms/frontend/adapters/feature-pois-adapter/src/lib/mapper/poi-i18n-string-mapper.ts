import { Language, LocalizationDto } from '@sw-code/urbo-cms-backend-api';
import { AbstractMapper, I18nString } from '@sw-code/urbo-cms-core';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class PoiI18nStringMapper extends AbstractMapper<
  LocalizationDto[],
  I18nString
> {
  override mapToDto(model: I18nString): LocalizationDto[] {
    return [
      {
        value: model.de ?? '',
        language: Language.De,
      },
      {
        value: model.en ?? '',
        language: Language.En,
      },
    ];
  }

  override mapToModel(dto: LocalizationDto[]): I18nString {
    const i18nString = new I18nString();
    i18nString.de = this.findLanguageValue(dto, Language.De);
    i18nString.en = this.findLanguageValue(dto, Language.En);
    return i18nString;
  }

  private findLanguageValue(
    localizations: LocalizationDto[],
    language: Language
  ): string {
    return (
      localizations.find((localization) => localization.language === language)
        ?.value ?? ''
    );
  }
}
