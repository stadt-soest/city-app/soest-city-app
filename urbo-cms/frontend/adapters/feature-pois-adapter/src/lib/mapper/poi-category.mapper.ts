import { Injectable } from '@angular/core';
import {
  CategoryCreateDto,
  CategoryResponseDto,
  CategoryType,
  CategoryUpdateDto,
  Language,
} from '@sw-code/urbo-cms-backend-api';
import { AbstractMapper } from '@sw-code/urbo-cms-core';
import { PoiCategory } from '@sw-code/urbo-cms-feature-pois';
import { PoiI18nStringMapper } from './poi-i18n-string-mapper';

@Injectable({ providedIn: 'root' })
export class PoiCategoryMapper extends AbstractMapper<
  CategoryResponseDto,
  PoiCategory
> {
  constructor(private readonly i18nStringMapper: PoiI18nStringMapper) {
    super();
  }

  mapToModel(dto: CategoryResponseDto): PoiCategory {
    return new PoiCategory(
      this.i18nStringMapper.mapToModel(dto.localizedNames),
      dto.sourceCategories ?? [],
      dto.id,
      dto.categoryGroup?.id,
      undefined,
      dto.colors ?? undefined,
      dto.customIcon?.filename
    );
  }

  mapToDto(model: PoiCategory): CategoryResponseDto {
    return {
      id: model.id!,
      type: CategoryType.Pois,
      localizedNames: this.i18nStringMapper.mapToDto(model.title),
      sourceCategories: model.sourceCategories,
    };
  }

  mapToCreateDto(model: PoiCategory): CategoryCreateDto {
    return {
      type: CategoryType.Pois,
      localizedNames: [
        { language: Language.De, value: model.title.de! },
        { language: Language.En, value: model.title.en! },
      ],
    };
  }

  mapToUpdateDto(model: PoiCategory): CategoryUpdateDto {
    return {
      localizedNames: [
        { language: Language.De, value: model.title.de! },
        { language: Language.En, value: model.title.en! },
      ].filter((name) => name.value),
      sourceCategories: model.sourceCategories,
      categoryGroupId: model.groupCategoryId,
      filename: model.filename,
      colors: model.colors,
    };
  }
}
