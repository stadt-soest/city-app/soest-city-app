import { Injectable } from '@angular/core';
import { AbstractMapper } from '@sw-code/urbo-cms-core';
import {
  GeoCoordinates,
  Language,
  MappedCategoryDto,
  PoiContactPointDto,
  PoiLocationDto,
  PoiResponseDto,
} from '@sw-code/urbo-cms-backend-api';
import {
  ContactPoint,
  Coordinates,
  Location,
  Poi,
} from '@sw-code/urbo-cms-feature-pois';

@Injectable({ providedIn: 'root' })
export class PoiMapper extends AbstractMapper<PoiResponseDto, Poi> {
  mapToModel(dto: PoiResponseDto): Poi {
    try {
      return {
        id: dto.id,
        name: dto.name,
        location: this.mapLocation(dto.location),
        contactPoint: this.mapContactPoint(dto.contactPoint),
        dateModified: new Date(dto.dateModified),
        url: dto.url,
        categories: this.mapCategoriesToJoinedString(dto.categories),
      };
    } catch (error) {
      console.error('Mapping of Poi failed in the PoiMapper:', error);
      throw new Error('Error while mapping Poi.');
    }
  }

  mapToDto(model: Poi): PoiResponseDto {
    throw new Error('Method not implemented.');
  }

  private mapLocation(location: PoiLocationDto): Location {
    return {
      postalCode: location.postalCode,
      street: location.street,
      city: location.city,
      coordinates: this.mapCoordinates(location.coordinates),
    };
  }

  private mapCoordinates(coordinates: GeoCoordinates): Coordinates {
    return {
      lat: coordinates?.latitude ?? null,
      long: coordinates?.longitude ?? null,
    };
  }

  private mapContactPoint(contactPoint: PoiContactPointDto): ContactPoint {
    return {
      telephone: contactPoint.telephone,
      email: contactPoint.email,
      contactPerson: contactPoint.contactPerson,
    };
  }

  private mapCategoriesToJoinedString(dtos: MappedCategoryDto[]): string[] {
    if (!dtos) return [];

    return dtos.map((categoryDto) => {
      const germanValue = categoryDto.localizedNames?.find(
        (localization) => localization.language === Language.De
      );
      return (
        germanValue?.value ?? `${categoryDto.sourceCategory} (Quellkategorie)`
      );
    });
  }
}
