import { Injectable } from '@angular/core';
import {
  CategoryGroupCreateDto,
  CategoryGroupResponseDto,
  CategoryType,
  Language,
} from '@sw-code/urbo-cms-backend-api';
import { AbstractMapper } from '@sw-code/urbo-cms-core';
import { PoiGroupCategory } from '@sw-code/urbo-cms-feature-pois';
import { PoiI18nStringMapper } from './poi-i18n-string-mapper';

@Injectable({ providedIn: 'root' })
export class PoiCategoryGroupMapper extends AbstractMapper<
  CategoryGroupResponseDto,
  PoiGroupCategory
> {
  constructor(private readonly i18nStringMapper: PoiI18nStringMapper) {
    super();
  }

  mapToModel(dto: CategoryGroupResponseDto): PoiGroupCategory {
    return new PoiGroupCategory(
      dto.id,
      this.i18nStringMapper.mapToModel(dto.localizedNames)
    );
  }

  mapToDto(model: PoiGroupCategory): CategoryGroupResponseDto {
    return {
      type: CategoryType.Pois,
      id: model.id!,
      localizedNames: this.i18nStringMapper.mapToDto(model.title),
    };
  }

  mapToCreateDto(model: PoiGroupCategory): CategoryGroupCreateDto {
    return {
      type: CategoryType.Pois,
      localizedNames: [
        { language: Language.De, value: model.title.de! },
        { language: Language.En, value: model.title.en! },
      ],
    };
  }
}
