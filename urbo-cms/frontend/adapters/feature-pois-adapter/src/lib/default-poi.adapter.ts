import { inject, Injectable } from '@angular/core';
import {
  Poi,
  PoiAdapter,
  PoiCategory,
  PoiGroupCategory,
} from '@sw-code/urbo-cms-feature-pois';
import { Observable, take } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  CategoryApiClient,
  CategoryGroupApiClient,
  CategoryType,
  FileApiClient,
  PoisApiClient,
  V1PoisGet200Response,
} from '@sw-code/urbo-cms-backend-api';
import {
  PoiResponseDto,
  PoisApiClient as SearchPoiClient,
} from '@sw-code/urbo-cms-search-api';
import { PoiMapper } from './mapper/poi.mapper';
import { PoiCategoryMapper } from './mapper/poi-category.mapper';
import { PoiCategoryGroupMapper } from './mapper/poi-category-group.mapper';

@Injectable()
export class DefaultPoiAdapter implements PoiAdapter {
  private readonly poiApiClient = inject(PoisApiClient);
  private readonly categoryApiClient = inject(CategoryApiClient);
  private readonly searchPoiClient = inject(SearchPoiClient);
  private readonly poiMapper = inject(PoiMapper);
  private readonly poiCategoryMapper = inject(PoiCategoryMapper);
  private readonly categoryGroupApiClient = inject(CategoryGroupApiClient);
  private readonly poiGroupCategoryMapper = inject(PoiCategoryGroupMapper);
  private readonly fileApiClient = inject(FileApiClient);

  getPoi(id: string): Observable<Poi> {
    return this.poiApiClient.v1PoisIdGet(id).pipe(
      take(1),
      map((response: PoiResponseDto) => this.poiMapper.mapToModel(response))
    );
  }

  getPois(
    page: number,
    rows: number
  ): Observable<{ content: Poi[]; totalRecords: number }> {
    return this.poiApiClient.v1PoisGet(page, rows).pipe(
      take(1),
      map((response: V1PoisGet200Response) => {
        const pois = this.poiMapper.mapToModelArray(response.content!);
        return {
          content: pois,
          totalRecords: response.page?.totalElements ?? 0,
        };
      })
    );
  }

  searchPois(
    searchTerm: string,
    page?: number,
    size?: number
  ): Observable<{ items: Poi[]; total: number }> {
    const pageNumber = page ?? 1;
    const pageSize = size ?? 10;

    return this.searchPoiClient
      .indexesPoisSearchPost({
        hitsPerPage: pageSize,
        page: pageNumber,
        q: searchTerm,
      })
      .pipe(
        map((response) => {
          const items = this.poiMapper.mapToModelArray(response.hits!) ?? [];
          const total = response.totalHits ?? 0;
          return { items, total };
        })
      );
  }

  createCategory(category: PoiCategory): Observable<PoiCategory> {
    return this.categoryApiClient
      .v1CategoriesPost(this.poiCategoryMapper.mapToCreateDto(category))
      .pipe(
        take(1),
        map((dto) => this.poiCategoryMapper.mapToModel(dto))
      );
  }

  createCategoryGroup(
    categoryGroup: PoiGroupCategory
  ): Observable<PoiGroupCategory> {
    return this.categoryGroupApiClient
      .v1CategoryGroupsPost(
        this.poiGroupCategoryMapper.mapToCreateDto(categoryGroup)
      )
      .pipe(
        map((dto) => this.poiCategoryMapper.mapToModel(dto)),
        take(1)
      );
  }

  deleteCategory(category: PoiCategory): Observable<void> {
    return this.categoryApiClient
      .v1CategoriesIdDelete(category.id!)
      .pipe(take(1));
  }

  deleteCategoryGroup(categoryGroupId: string): Observable<void> {
    return this.categoryGroupApiClient
      .v1CategoryGroupsIdDelete(categoryGroupId)
      .pipe(take(1));
  }

  getCategories(): Observable<PoiCategory[]> {
    return this.categoryApiClient.v1CategoriesGet(CategoryType.Pois).pipe(
      take(1),
      map((categories) => this.poiCategoryMapper.mapToModelArray(categories))
    );
  }

  getCategoryGroups(): Observable<PoiGroupCategory[]> {
    return this.categoryGroupApiClient
      .v1CategoryGroupsGet(CategoryType.Pois)
      .pipe(
        take(1),
        map((categoryGroups) =>
          this.poiGroupCategoryMapper.mapToModelArray(categoryGroups)
        )
      );
  }

  getSourceCategories(): Observable<string[]> {
    return this.categoryApiClient
      .v1SourceCategoriesGet(CategoryType.Pois)
      .pipe(take(1));
  }

  getUnmappedSourceCategories(): Observable<string[]> {
    return this.categoryApiClient
      .v1SourceCategoriesGet(CategoryType.Pois, false)
      .pipe(take(1));
  }

  updateCategory(category: PoiCategory): Observable<PoiCategory> {
    return this.categoryApiClient
      .v1CategoriesIdPut(
        category.id!,
        this.poiCategoryMapper.mapToUpdateDto(category)
      )
      .pipe(
        take(1),
        map((dto) => this.poiCategoryMapper.mapToModel(dto))
      );
  }

  updateCategoryGroup(
    categoryGroup: PoiGroupCategory
  ): Observable<PoiGroupCategory> {
    return this.categoryGroupApiClient
      .v1CategoryGroupsIdPut(
        categoryGroup.id!,
        this.poiGroupCategoryMapper.mapToCreateDto(categoryGroup)
      )
      .pipe(
        take(1),
        map((dto) => this.poiCategoryMapper.mapToModel(dto))
      );
  }

  updateCategoryGroupOrder(
    groupCategories: { id: string; order: number }[]
  ): Observable<void> {
    return this.categoryGroupApiClient
      .v1CategoryGroupsOrderPatch(groupCategories)
      .pipe(take(1));
  }

  updateCategoryOrder(
    categories: { id: string; order: number }[]
  ): Observable<void> {
    return this.categoryApiClient
      .v1CategoriesOrderPatch(categories)
      .pipe(take(1));
  }

  uploadFile(file: Blob): Observable<string> {
    return this.fileApiClient.filesPost(file).pipe(take(1));
  }
}
