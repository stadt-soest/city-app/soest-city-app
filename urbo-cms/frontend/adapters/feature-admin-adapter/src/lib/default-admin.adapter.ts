import { inject, Injectable } from '@angular/core';
import {
  AdminAdapter,
  DeadLetterTask,
  Role,
  User,
} from '@sw-code/urbo-cms-feature-admin';
import { Observable, take } from 'rxjs';
import { Permission } from '@sw-code/urbo-cms-core';
import { RoleApiClient, UserApiClient } from '@sw-code/urbo-cms-auth-api';
import { DeadLetterApiClient } from '@sw-code/urbo-cms-queue-api';

@Injectable()
export class DefaultAdminAdapter implements AdminAdapter {
  private readonly roleApiClient = inject(RoleApiClient);
  private readonly deadLetterApiClient = inject(DeadLetterApiClient);
  private readonly userApiClient = inject(UserApiClient);

  getPermissions(): Observable<string[]> {
    return this.roleApiClient.v1PermissionsGet().pipe(take(1));
  }

  getRoles(): Observable<Role[]> {
    return this.roleApiClient.getRoles().pipe(take(1));
  }

  deleteRole(id: string): Observable<void> {
    return this.roleApiClient.deleteRole(id).pipe(take(1));
  }

  updateRole(role: Role): Observable<Role> {
    return this.roleApiClient
      .updateRole(role.id, {
        name: role.name,
        permissions: role.permissions,
      })
      .pipe(take(1));
  }

  deleteDeadLetterTask(id: string): Observable<void> {
    return this.deadLetterApiClient.deleteDeadLetterTask(id).pipe(take(1));
  }

  fetchDeadLetterTasks(): Observable<DeadLetterTask[]> {
    return this.deadLetterApiClient.getDeadLetterTasks().pipe(take(1));
  }

  requeueDeadLetterTask(id: string): Observable<DeadLetterTask> {
    return this.deadLetterApiClient.requeueDeadLetterTask(id).pipe(take(1));
  }

  fetchDeadLetterTask(id: string): Observable<DeadLetterTask> {
    return this.deadLetterApiClient.fetchDeadLetterTask(id).pipe(take(1));
  }

  deleteUser(id: string): Observable<User> {
    return this.userApiClient.v1UsersIdDelete(id).pipe(take(1));
  }

  getUsers(): Observable<User[]> {
    return this.userApiClient.v1UsersGet().pipe(take(1));
  }

  updateUser(id: string, user: User): Observable<User> {
    return this.userApiClient.v1UsersIdPut(id, user).pipe(take(1));
  }

  sendPasswordResetEmail(id: string): Observable<void> {
    return this.userApiClient.sendPasswordResetEmail(id).pipe(take(1));
  }

  createUser(user: User): Observable<User> {
    return this.userApiClient.v1UsersPost(user).pipe(take(1));
  }

  createRole(role: {
    name: string;
    permissions: Array<Permission>;
  }): Observable<Role> {
    return this.roleApiClient.createRole(role).pipe(take(1));
  }
}
