import { inject, Injectable } from '@angular/core';
import { CoreAdapter } from '@sw-code/urbo-cms-core';
import { firstValueFrom } from 'rxjs';
import { UserApiClient } from '@sw-code/urbo-cms-auth-api';

@Injectable()
export class DefaultCoreAdapter implements CoreAdapter {
  private readonly userApiClient = inject(UserApiClient);

  async fetchCurrentUserPermissions(): Promise<string[]> {
    const { permissions } = await firstValueFrom(this.userApiClient.v1MeGet());
    return permissions;
  }
}
