import {
  WasteAdapter,
  WasteDate,
  WasteLocation,
  WasteType,
  WasteTypeDescriptions,
} from '@sw-code/urbo-cms-feature-waste';
import { WasteMapper } from './mapper/waste.mapper';
import { inject } from '@angular/core';
import { map, Observable } from 'rxjs';
import {
  V1WasteCollectionsGet200Response,
  WasteApiClient,
} from '@sw-code/urbo-cms-backend-api';
import {
  IndexesWasteCollectionLocationsSearchPost200Response,
  WasteCollectionsApiClient,
} from '@sw-code/urbo-cms-search-api';

export class DefaultWasteAdapter implements WasteAdapter {
  private readonly wasteMapper = inject(WasteMapper);
  private readonly wasteCollectionsApiClient = inject(
    WasteCollectionsApiClient
  );
  private readonly wasteApiClient = inject(WasteApiClient);

  formatDate(dateString: string): string {
    const date = new Date(dateString);
    return date.toISOString().split('T')[0];
  }

  formatWasteDates(wasteDates: WasteDate[]): string {
    return wasteDates
      .map(
        (wasteDate) =>
          `${this.getWasteTypeDescription(
            wasteDate.wasteType
          )} (${this.formatDate(wasteDate.date)})`
      )
      .join(', ');
  }

  getWaste(
    page: number,
    rows: number,
    searchTerm?: string
  ): Observable<{
    content: WasteLocation[];
    totalRecords: number;
  }> {
    const searchRequestDto = {
      page: page,
      hitsPerPage: rows,
      q: searchTerm ?? undefined,
    };

    return this.wasteCollectionsApiClient
      .indexesWasteCollectionLocationsSearchPost(searchRequestDto)
      .pipe(
        map(
          (response: IndexesWasteCollectionLocationsSearchPost200Response) => {
            const wasteLocations = this.wasteMapper.mapToModelArray(
              response.hits ?? []
            );
            return {
              content: wasteLocations,
              totalRecords: response.totalHits ?? 0,
            };
          }
        )
      );
  }

  getWasteDetailByIdAndStreet(street: string): Observable<WasteLocation[]> {
    return this.wasteApiClient
      .v1WasteCollectionsGet(undefined, street, undefined, 0, 10)
      .pipe(
        map((response: V1WasteCollectionsGet200Response) =>
          this.wasteMapper.mapToModelArray(response.content!)
        )
      );
  }

  getWasteTypeDescription(wasteType: WasteType): string {
    return WasteTypeDescriptions[wasteType];
  }
}
