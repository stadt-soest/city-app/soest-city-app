import { WasteDateDTO, WasteResponseDto } from '@sw-code/urbo-cms-backend-api';
import { AbstractMapper } from '@sw-code/urbo-cms-core';
import {
  WasteDate,
  WasteLocation,
  WasteType,
} from '@sw-code/urbo-cms-feature-waste';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class WasteMapper extends AbstractMapper<
  WasteResponseDto,
  WasteLocation
> {
  mapToModel(dto: WasteResponseDto): WasteLocation {
    try {
      return {
        id: dto.id ?? '',
        city: dto.city ?? '',
        street: dto.street ?? '',
        wasteDates:
          dto.wasteDates?.map((wasteDateDto) =>
            this.mapWasteDate(wasteDateDto)
          ) ?? [],
      };
    } catch (error) {
      console.error('Mapping of Waste failed in the WasteMapper:', error);
      throw new Error('Error while mapping Waste.');
    }
  }

  mapToDto(_model: WasteLocation): WasteResponseDto {
    throw new Error('Method not implemented.');
  }

  private mapWasteDate(wasteDateDto: WasteDateDTO): WasteDate {
    return {
      date: wasteDateDto.date ?? '',
      wasteType: (wasteDateDto.wasteType ?? 'WASTEPAPER') as WasteType,
    };
  }
}
