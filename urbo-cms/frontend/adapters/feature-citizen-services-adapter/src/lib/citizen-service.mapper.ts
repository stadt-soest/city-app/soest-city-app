import { AbstractMapper } from '@sw-code/urbo-cms-core';
import { CitizenServiceResponseDto } from '@sw-code/urbo-cms-backend-api';
import { CitizenService } from '@sw-code/urbo-cms-feature-citizen-services';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class CitizenServiceMapper extends AbstractMapper<
  CitizenServiceResponseDto,
  CitizenService
> {
  override mapToDto(_model: CitizenService): CitizenServiceResponseDto {
    throw new Error('Method not implemented.');
  }

  mapToModel(dto: CitizenServiceResponseDto): CitizenService {
    try {
      return {
        id: dto.id,
        favorite: dto.favorite,
        visibility: dto.visible,
        title: dto.title,
        description: dto.description,
        url: dto.url,
        tags: dto.tags,
        editable: dto.editable,
      };
    } catch (error) {
      throw new Error('Error while mapping CitizenService.');
    }
  }
}
