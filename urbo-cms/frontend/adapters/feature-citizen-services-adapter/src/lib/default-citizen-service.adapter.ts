import { inject, Injectable } from '@angular/core';
import {
  CitizenService,
  CitizenServiceAdapter,
  Tag,
} from '@sw-code/urbo-cms-feature-citizen-services';
import {
  CitizenServiceApiClient,
  CreateCitizenServiceDto,
  CreateTagDto,
  UpdateCitizenServiceDto,
  V1CitizenServicesGet200Response,
} from '@sw-code/urbo-cms-backend-api';
import { forkJoin, map, Observable, take } from 'rxjs';
import { CitizenServicesApiClient as CitizenServiceSearchApiClient } from '@sw-code/urbo-cms-search-api';
import { CitizenServiceMapper } from './citizen-service.mapper';

@Injectable()
export class DefaultCitizenServiceAdapter implements CitizenServiceAdapter {
  private readonly citizenServiceApiClient = inject(CitizenServiceApiClient);
  private readonly citizenServiceSearchApiClient = inject(
    CitizenServiceSearchApiClient
  );
  private readonly citizenServiceMapper = inject(CitizenServiceMapper);

  createOrUpdateTag(createTagDto: CreateTagDto): Observable<Tag> {
    return this.citizenServiceApiClient
      .createOrUpdateTag(createTagDto)
      .pipe(take(1));
  }

  deleteCitizenService(citizenServiceId: string): Observable<void> {
    return this.citizenServiceApiClient
      .deleteCitizenService(citizenServiceId)
      .pipe(take(1));
  }

  deleteTag(tagId: string): Observable<void> {
    return this.citizenServiceApiClient.deleteTag(tagId).pipe(take(1));
  }

  getBySearchTerm(
    searchTerm: string,
    page?: number,
    size?: number,
    sortField?: string,
    sortOrder?: string
  ): Observable<{
    total: number;
    items: CitizenService[];
  }> {
    const pageNumber = page ?? 1;
    const pageSize = size ?? 10;

    return this.citizenServiceSearchApiClient
      .indexesCitizenServicesSearchPost({
        q: searchTerm,
        page: pageNumber,
        hitsPerPage: pageSize,
        sort:
          sortField && sortOrder ? [`${sortField}:${sortOrder}`] : undefined,
      })
      .pipe(
        map((response) => {
          const items = this.citizenServiceMapper.mapToModelArray(
            response.hits ?? []
          );
          const total = response.totalHits ?? 0;
          return { items, total };
        })
      );
  }

  getCitizenService(id: string): Observable<CitizenService> {
    return this.citizenServiceApiClient.v1CitizenServicesIdGet(id).pipe(
      take(1),
      map((response) => {
        return this.citizenServiceMapper.mapToModel(response);
      })
    );
  }

  getTags(): Observable<Tag[]> {
    return this.citizenServiceApiClient
      .v1CitizenServicesTagsGet()
      .pipe(take(1));
  }

  searchCitizenServices(
    search: string,
    page: number,
    rows: number
  ): Observable<{
    content: CitizenService[];
    totalRecords: number;
  }> {
    return this.citizenServiceApiClient
      .v1CitizenServicesSearchGet(search, page, rows)
      .pipe(
        map((response: V1CitizenServicesGet200Response) => {
          const citizenServices = this.citizenServiceMapper.mapToModelArray(
            response.content!
          );
          return {
            content: citizenServices,
            totalRecords: response.page?.totalElements ?? 0,
          };
        })
      );
  }

  updateCitizenService(
    citizenServiceId: string,
    dto: UpdateCitizenServiceDto
  ): Observable<CitizenService> {
    return this.citizenServiceApiClient
      .updateCitizenService(citizenServiceId, dto)
      .pipe(
        take(1),
        map((response) => {
          return this.citizenServiceMapper.mapToModel(response);
        })
      );
  }

  updateFavorite(
    citizenServiceId: string,
    favorite: boolean
  ): Observable<CitizenService> {
    return this.citizenServiceApiClient
      .updateCitizenServiceFavorite(citizenServiceId, favorite)
      .pipe(take(1));
  }

  updateTagAssignments(tagId: string, serviceIds: string[]): Observable<void> {
    return this.citizenServiceApiClient
      .updateTagAssignments(tagId, serviceIds)
      .pipe(take(1));
  }

  updateVisibility(
    citizenServiceId: string,
    visible: boolean
  ): Observable<CitizenService> {
    return this.citizenServiceApiClient
      .updateCitizenServiceVisibility(citizenServiceId, visible)
      .pipe(take(1));
  }

  updateVisibilityAndFavorite(
    citizenServiceId: string,
    visible: boolean,
    favorite: boolean
  ): Observable<{
    visibility: CitizenService;
    favorite: CitizenService;
  }> {
    const visibility$ = this.updateVisibility(citizenServiceId, visible);
    const favorite$ = this.updateFavorite(citizenServiceId, favorite);

    return forkJoin({
      visibility: visibility$,
      favorite: favorite$,
    }).pipe(take(1));
  }

  createCitizenService(
    createCitizenServiceDto: CreateCitizenServiceDto
  ): Observable<CitizenService> {
    return this.citizenServiceApiClient
      .createCitizenService(createCitizenServiceDto)
      .pipe(
        map((dto) => this.citizenServiceMapper.mapToModel(dto)),
        take(1)
      );
  }

  getCitizenServices(
    page: number,
    rows: number,
    options?: {
      visible?: boolean | null;
      favorite?: boolean | null;
      withoutTag?: boolean;
      onlyExternal?: 'EXTERNAL' | 'INTERNAL' | null;
      sortField?: string;
      sortOrder?: string;
    }
  ): Observable<{ content: CitizenService[]; totalRecords: number }> {
    const sort =
      options?.sortField && options?.sortOrder
        ? [`${options.sortField},${options.sortOrder}`]
        : undefined;

    return this.citizenServiceApiClient
      .v1CitizenServicesGet(
        undefined,
        options?.withoutTag ?? false,
        options?.visible ?? undefined,
        options?.favorite ?? undefined,
        options?.onlyExternal ?? undefined,
        page,
        rows,
        sort
      )
      .pipe(
        map((response: V1CitizenServicesGet200Response) => {
          const citizenServices = this.citizenServiceMapper.mapToModelArray(
            response.content!
          );
          return {
            content: citizenServices,
            totalRecords: response.page?.totalElements ?? 0,
          };
        })
      );
  }
}
