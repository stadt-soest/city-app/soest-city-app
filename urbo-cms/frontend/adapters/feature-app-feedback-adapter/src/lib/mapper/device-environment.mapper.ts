import { Injectable } from '@angular/core';
import { AbstractMapper } from '@sw-code/urbo-cms-core';
import { EnvironmentDto } from '@sw-code/urbo-cms-backend-api';
import { DeviceEnvironment } from '@sw-code/urbo-cms-feature-app-feedback';

@Injectable({ providedIn: 'root' })
export class DeviceEnvironmentMapper extends AbstractMapper<
  EnvironmentDto,
  DeviceEnvironment
> {
  mapToModel(dto?: EnvironmentDto): DeviceEnvironment {
    try {
      return new DeviceEnvironment(
        dto?.deviceModelName ?? '',
        dto?.operatingSystemName ?? '',
        dto?.operatingSystemVersion ?? '',
        dto?.appVersion ?? '',
        dto?.browserName ?? '',
        dto?.usedAs ?? '',
        dto?.appSettings ? JSON.parse(dto?.appSettings) : {}
      );
    } catch (error) {
      console.error(
        'Mapping of DeviceEnvironments failed in the DeviceEnvironmentMapper:',
        error
      );
      throw new Error('Error while mapping DeviceEnvironments.');
    }
  }

  mapToDto(_model: DeviceEnvironment): EnvironmentDto {
    throw new Error('Method not implemented.');
  }
}
