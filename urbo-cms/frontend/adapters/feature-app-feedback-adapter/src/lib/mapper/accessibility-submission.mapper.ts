import { Injectable } from '@angular/core';
import { AbstractMapper } from '@sw-code/urbo-cms-core';
import { AccessibilityFeedbackDto } from '@sw-code/urbo-cms-backend-api';
import { DeviceEnvironmentMapper } from './device-environment.mapper';
import { AccessibilitySubmission } from '@sw-code/urbo-cms-feature-app-feedback';

@Injectable({ providedIn: 'root' })
export class AccessibilitySubmissionMapper extends AbstractMapper<
  AccessibilityFeedbackDto,
  AccessibilitySubmission
> {
  constructor(
    private readonly deviceEnvironmentMapper: DeviceEnvironmentMapper
  ) {
    super();
  }

  mapToModel(dto: AccessibilityFeedbackDto): AccessibilitySubmission {
    try {
      return new AccessibilitySubmission(
        dto.id,
        new Date(dto.createdAt),
        dto.answer,
        this.deviceEnvironmentMapper.mapToModel(dto.environment),
        dto.imageFilenames?.map((imageFilename: string) => ({
          filename: imageFilename,
        })) ?? []
      );
    } catch (error) {
      console.error(
        'Mapping of AccessibilitySubmissions failed in the AccessibilitySubmissionMapper:',
        error
      );
      throw new Error('Error while mapping AccessibilitySubmissions.');
    }
  }

  mapToDto(_model: AccessibilitySubmission): AccessibilityFeedbackDto {
    throw new Error('Method not implemented.');
  }
}
