import { Injectable } from '@angular/core';
import { AbstractMapper } from '@sw-code/urbo-cms-core';
import {
  FeedbackAnswerResponseDto,
  FeedbackSubmissionDto,
  Language,
} from '@sw-code/urbo-cms-backend-api';
import { DeviceEnvironmentMapper } from './device-environment.mapper';
import {
  FeedbackSubmission,
  FeedbackSubmissionAnswer,
} from '@sw-code/urbo-cms-feature-app-feedback';

@Injectable({ providedIn: 'root' })
export class FeedbackSubmissionMapper extends AbstractMapper<
  FeedbackSubmissionDto,
  FeedbackSubmission
> {
  constructor(
    private readonly deviceEnvironmentMapper: DeviceEnvironmentMapper
  ) {
    super();
  }

  mapToModel(dto: FeedbackSubmissionDto): FeedbackSubmission {
    try {
      return new FeedbackSubmission(
        dto.id,
        new Date(dto.createdAt),
        dto.deduplicationKey,
        this.deviceEnvironmentMapper.mapToModel(dto.environment),
        this.mapAnswers(dto.answers)
      );
    } catch (error) {
      console.error(
        'Mapping of FeedbackSubmission failed in the FeedbackSubmissionMapper:',
        error
      );
      throw new Error('Error while mapping FeedbackSubmission.');
    }
  }

  mapToDto(_model: FeedbackSubmission): FeedbackSubmissionDto {
    throw new Error('Method not implemented.');
  }

  private mapAnswers(
    answerDtos: Array<FeedbackAnswerResponseDto>
  ): FeedbackSubmissionAnswer[] {
    return answerDtos.map(
      (answerDto: FeedbackAnswerResponseDto) =>
        new FeedbackSubmissionAnswer(
          answerDto.question.questionName?.find(
            (questionName) => questionName.language === Language.De
          )?.value ?? '',
          answerDto.selectedOption?.localizedTexts?.find(
            (selectedOption) => selectedOption.language === Language.De
          )?.value ?? '',
          answerDto.freeText ?? ''
        )
    );
  }
}
