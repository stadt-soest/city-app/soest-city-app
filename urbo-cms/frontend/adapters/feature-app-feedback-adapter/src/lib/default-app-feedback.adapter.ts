import { inject, Injectable } from '@angular/core';
import {
  AccessibilitySubmission,
  AppFeedbackAdapter,
  FeedbackSubmission,
} from '@sw-code/urbo-cms-feature-app-feedback';
import { map, Observable, take } from 'rxjs';
import {
  AccessibilityFeedbackApiClient,
  AccessibilityFeedbackDto,
  FeedbackApiClient,
  FeedbackSubmissionDto,
  V1AccessibilityFeedbackSubmissionsGet200Response,
  V1FeedbackSubmissionsGet200Response,
} from '@sw-code/urbo-cms-backend-api';
import { FeedbackSubmissionMapper } from './mapper/feedback-submission.mapper';
import { AccessibilitySubmissionMapper } from './mapper/accessibility-submission.mapper';

@Injectable()
export class DefaultAppFeedbackAdapter implements AppFeedbackAdapter {
  private readonly feedbackApiClient = inject(FeedbackApiClient);
  private readonly feedbackSubmissionMapper = inject(FeedbackSubmissionMapper);
  private readonly accessibilityApiClient = inject(
    AccessibilityFeedbackApiClient
  );
  private readonly accessibilitySubmissionMapper = inject(
    AccessibilitySubmissionMapper
  );

  getAccessibilitySubmission(
    submissionId: string
  ): Observable<AccessibilitySubmission> {
    return this.accessibilityApiClient
      .v1AccessibilityFeedbackSubmissionsIdGet(submissionId)
      .pipe(
        map((submissionDto: AccessibilityFeedbackDto) =>
          this.accessibilitySubmissionMapper.mapToModel(submissionDto)
        )
      );
  }

  getAccessibilitySubmissions(
    page: number,
    rows: number,
    sortField = 'createdAt',
    sortOrder = 'desc'
  ): Observable<{
    content: AccessibilitySubmission[];
    totalRecords: number;
  }> {
    return this.accessibilityApiClient
      .v1AccessibilityFeedbackSubmissionsGet(page, rows, [
        `${sortField},${sortOrder}`,
      ])
      .pipe(
        map((response: V1AccessibilityFeedbackSubmissionsGet200Response) => {
          const accessibilitySubmissions =
            this.accessibilitySubmissionMapper.mapToModelArray(
              response.content!
            );
          return {
            content: accessibilitySubmissions,
            totalRecords: response.page?.totalElements ?? 0,
          };
        })
      );
  }

  getFeedbackSubmission(submissionId: string): Observable<FeedbackSubmission> {
    return this.feedbackApiClient
      .v1FeedbackSubmissionsIdGet(submissionId)
      .pipe(
        map((submissionDto: FeedbackSubmissionDto) =>
          this.feedbackSubmissionMapper.mapToModel(submissionDto)
        )
      );
  }

  getFeedbackSubmissions(
    page: number,
    rows: number,
    sortField = 'submissionTime',
    sortOrder = 'desc'
  ): Observable<{
    content: FeedbackSubmission[];
    totalRecords: number;
  }> {
    return this.feedbackApiClient
      .v1FeedbackSubmissionsGet(page, rows, [`${sortField},${sortOrder}`])
      .pipe(
        take(1),
        map((response: V1FeedbackSubmissionsGet200Response) => {
          const appFeedbackSubmissions =
            this.feedbackSubmissionMapper.mapToModelArray(response.content!);
          return {
            content: appFeedbackSubmissions,
            totalRecords: response.page?.totalElements ?? 0,
          };
        })
      );
  }

  getFeedbackSubmissionsCount(): Observable<number> {
    return this.feedbackApiClient.v1FeedbackSubmissionsGet(0, 10).pipe(
      map((response: V1FeedbackSubmissionsGet200Response) => {
        return response.page?.totalElements ?? 0;
      })
    );
  }

  getAccessibilitySubmissionsCount(): Observable<number> {
    return this.accessibilityApiClient
      .v1AccessibilityFeedbackSubmissionsGet(0, 10)
      .pipe(
        map((response: V1AccessibilityFeedbackSubmissionsGet200Response) => {
          return response.page?.totalElements ?? 0;
        })
      );
  }
}
