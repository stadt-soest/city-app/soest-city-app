import { inject, Injectable } from '@angular/core';
import {
  Event,
  EventAdapter,
  EventCategory,
} from '@sw-code/urbo-cms-feature-events';
import { catchError, Observable, of, take } from 'rxjs';
import { map } from 'rxjs/operators';
import { GroupedFeedEventsApiClient as EventSearchApiClient } from '@sw-code/urbo-cms-search-api';
import {
  CategoryApiClient,
  CategoryType,
  EventApiClient,
  EventResponseDto,
} from '@sw-code/urbo-cms-backend-api';
import { EventMapper } from './mapper/event.mapper';
import { EventCategoryMapper } from './mapper/event-category.mapper';
import { endOfDay, getUnixTime, startOfDay } from 'date-fns';

@Injectable()
export class DefaultEventAdapter implements EventAdapter {
  private readonly eventSearchApiClient = inject(EventSearchApiClient);
  private readonly eventApiClient = inject(EventApiClient);
  private readonly categoryApiClient = inject(CategoryApiClient);
  private readonly eventMapper = inject(EventMapper);
  private readonly eventCategoryMapper = inject(EventCategoryMapper);

  createCategory(category: EventCategory): Observable<EventCategory> {
    return this.categoryApiClient
      .v1CategoriesPost(this.eventCategoryMapper.mapToCreateDto(category))
      .pipe(
        take(1),
        map((category) => {
          return this.eventCategoryMapper.mapToModel(category);
        })
      );
  }

  deleteCategory(category: EventCategory): Observable<void> {
    return this.categoryApiClient
      .v1CategoriesIdDelete(category.id!)
      .pipe(take(1));
  }

  getCategories(): Observable<EventCategory[]> {
    return this.categoryApiClient
      .v1CategoriesGet(CategoryType.Events)
      .pipe(
        map((categories) =>
          this.eventCategoryMapper.mapToModelArray(categories)
        )
      );
  }

  getEvent(id: string): Observable<Event> {
    return this.eventApiClient
      .v1EventsIdGet(id)
      .pipe(
        map((response: EventResponseDto) =>
          this.eventMapper.mapToModel(response)
        )
      );
  }

  getEvents(
    page: number,
    rows: number,
    searchTerm?: string,
    startDate?: Date,
    endDate?: Date
  ): Observable<{ content: Event[]; totalRecords: number }> {
    const query = searchTerm?.trim() ? searchTerm : undefined;
    const filters = [];

    if (startDate) {
      filters.push(`startAtUnix >= ${getUnixTime(startOfDay(startDate))}`);
    }
    if (endDate) {
      filters.push(`endAtUnix <= ${getUnixTime(endOfDay(endDate))}`);
    }
    const filter = filters.length ? filters.join(' AND ') : undefined;

    return this.eventSearchApiClient
      .indexesGroupedFeedEventsSearchPost({
        page,
        hitsPerPage: rows,
        sort: ['startAt:asc'],
        q: query,
        filter: filter,
      })
      .pipe(
        map((response) => {
          const events: Event[] = this.eventMapper.mapFeedDtoToModelArray(
            response.hits ?? []
          );
          const totalRecords = response.totalHits ?? 0;
          return { content: events, totalRecords };
        }),
        catchError(() => {
          return of({ content: [], totalRecords: 0 });
        })
      );
  }

  getSourceCategories(): Observable<string[]> {
    return this.categoryApiClient
      .v1SourceCategoriesGet(CategoryType.Events)
      .pipe(take(1));
  }

  getUnmappedSourceCategories(): Observable<string[]> {
    return this.categoryApiClient
      .v1SourceCategoriesGet(CategoryType.Events, false)
      .pipe(take(1));
  }

  updateCategory(category: EventCategory): Observable<EventCategory> {
    return this.categoryApiClient
      .v1CategoriesIdPut(
        category.id!,
        this.eventCategoryMapper.mapToUpdateDto(category)
      )
      .pipe(
        take(1),
        map((dto) => this.eventCategoryMapper.mapToModel(dto))
      );
  }
}
