import { AbstractMapper } from '@sw-code/urbo-cms-core';
import { Injectable } from '@angular/core';
import {
  ContactPointResponseDto,
  EventLocationDto,
  EventLocationDtoCoordinates,
  EventResponseDto,
  Language,
  MappedCategoryDto,
  OccurrenceDto,
} from '@sw-code/urbo-cms-backend-api';
import { EventFeedDto } from '@sw-code/urbo-cms-search-api';
import { ImageMapper } from './image.mapper';
import {
  Address,
  ContactPoint,
  Coordinates,
  Event,
  EventOccurrence,
} from '@sw-code/urbo-cms-feature-events';

@Injectable({ providedIn: 'root' })
export class EventMapper extends AbstractMapper<EventResponseDto, Event> {
  constructor(private readonly imageMapper: ImageMapper) {
    super();
  }

  mapToModel(dto: EventResponseDto): Event {
    try {
      return {
        id: dto.id,
        title: dto.title,
        subTitle: dto.subTitle ?? null,
        description: dto.description ?? null,
        address: this.mapLocation(dto.location),
        contactPoint: this.mapContactPoint(dto.contactPoint),
        dateModified: dto.latestDateCreated
          ? new Date(dto.latestDateCreated)
          : null,
        categories: this.mapCategoriesToJoinedString(dto.categories),
        image: dto.image ? this.imageMapper.mapToModel(dto.image) : null,
        occurrences: this.mapOccurrences(dto.occurrences),
      };
    } catch (error) {
      console.error('Mapping of Event failed in the EventMapper:', error);
      throw new Error('Error while mapping Event.');
    }
  }

  mapToDto(model: Event): EventResponseDto {
    throw new Error('Method not implemented.');
  }

  mapFeedDtoToModelArray(dtos: EventFeedDto[]): Event[] {
    return dtos.map((dto: EventFeedDto) => this.mapFeedDtoToModel(dto));
  }

  private mapFeedDtoToModel(dto: EventFeedDto): Event {
    try {
      return {
        id: dto.id,
        title: dto.title,
        subTitle: dto.subTitle ?? null,
        description: dto.description ?? null,
        address: this.mapLocation(dto.location),
        contactPoint: { telephone: null, email: null },
        dateModified: new Date(dto.dateCreated),
        categories: [],
        image: dto.image ? this.imageMapper.mapToModel(dto.image) : null,
        occurrences: [],
      };
    } catch (error) {
      console.error('Error while mapping EventFeedDto:', error, dto);
      throw new Error('Mapping failed in mapFeedDtoToModel.');
    }
  }

  private mapLocation(location: EventLocationDto | undefined): Address {
    if (!location) {
      return {
        locationName: '',
        postalCode: '',
        street: '',
        city: '',
        coordinates: null,
      };
    }

    return {
      locationName: location.locationName ?? '',
      postalCode: location.postalCode ?? '',
      street: location.address ?? '',
      city: location.city ?? '',
      coordinates: location.coordinates
        ? this.mapCoordinates(location.coordinates)
        : null,
    };
  }

  private mapCoordinates(
    coordinates: EventLocationDtoCoordinates
  ): Coordinates {
    return {
      lat: coordinates?.latitude ?? null,
      long: coordinates?.longitude ?? null,
    };
  }

  private mapContactPoint(
    contactPoint?: ContactPointResponseDto
  ): ContactPoint {
    return {
      telephone: contactPoint?.telephone ?? null,
      email: contactPoint?.email ?? null,
    };
  }

  private mapCategoriesToJoinedString(dtos: MappedCategoryDto[]): string[] {
    if (!dtos) return [];

    return dtos.map((categoryDto) => {
      const germanValue = categoryDto.localizedNames?.find(
        (localization) => localization.language === Language.De
      );
      return (
        germanValue?.value ?? `${categoryDto.sourceCategory} (Quellkategorie)`
      );
    });
  }

  private mapOccurrences(occurrences: OccurrenceDto[]) {
    return occurrences.map(
      (occurrence: OccurrenceDto) =>
        new EventOccurrence(
          occurrence.id,
          occurrence.status,
          occurrence.source,
          new Date(occurrence.dateCreated),
          new Date(occurrence.start),
          new Date(occurrence.end)
        )
    );
  }
}
