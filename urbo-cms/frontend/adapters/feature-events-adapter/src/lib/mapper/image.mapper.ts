import { Injectable } from '@angular/core';
import { ImageDto } from '@sw-code/urbo-cms-backend-api';
import { AbstractMapper, Image } from '@sw-code/urbo-cms-core';

@Injectable({ providedIn: 'root' })
export class ImageMapper extends AbstractMapper<ImageDto, Image | null> {
  mapToModel(image: ImageDto): Image | null {
    if (!image) return null;

    const imageModel: Image = {
      originalSource: image.imageSource,
      copyright: image.copyrightHolder ?? undefined,
      description: image.description ?? undefined,
    };

    if (image.thumbnail) {
      imageModel.thumbnail = {
        filename: image.thumbnail.filename,
        width: image.thumbnail.width,
        height: image.thumbnail.height,
      };
    }

    if (image.highRes) {
      imageModel.highRes = {
        filename: image.highRes?.filename,
        width: image.highRes?.width,
        height: image.highRes?.height,
      };
    }

    return imageModel;
  }

  mapToDto(model: Image): ImageDto {
    throw new Error('Not implemented yet');
  }
}
