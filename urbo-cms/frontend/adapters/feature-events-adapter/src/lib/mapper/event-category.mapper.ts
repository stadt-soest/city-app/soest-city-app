import { AbstractMapper } from '@sw-code/urbo-cms-core';
import { Injectable } from '@angular/core';
import {
  CategoryCreateDto,
  CategoryResponseDto,
  CategoryType,
  CategoryUpdateDto,
  Language,
} from '@sw-code/urbo-cms-backend-api';
import { EventCategory } from '@sw-code/urbo-cms-feature-events';
import { EventI18nStringMapper } from './event-i18n-string-mapper';

@Injectable({ providedIn: 'root' })
export class EventCategoryMapper extends AbstractMapper<
  CategoryResponseDto,
  EventCategory
> {
  constructor(private readonly i18nStringMapper: EventI18nStringMapper) {
    super();
  }

  mapToModel(dto: CategoryResponseDto): EventCategory {
    const category = new EventCategory();
    category.id = dto.id;
    category.sourceCategories = dto.sourceCategories ?? [];
    category.title = this.i18nStringMapper.mapToModel(dto.localizedNames);
    return category;
  }

  mapToDto(model: EventCategory): CategoryResponseDto {
    return {
      id: model.id!,
      type: CategoryType.Events,
      localizedNames: this.i18nStringMapper.mapToDto(model.title),
      sourceCategories: model.sourceCategories,
    };
  }

  mapToCreateDto(model: EventCategory): CategoryCreateDto {
    return {
      type: CategoryType.Events,
      localizedNames: [
        { language: Language.De, value: model.title.de! },
        { language: Language.En, value: model.title.en! },
      ],
    };
  }

  mapToUpdateDto(model: EventCategory): CategoryUpdateDto {
    return {
      localizedNames: [
        { language: Language.De, value: model.title.de! },
        { language: Language.En, value: model.title.en! },
      ],
      sourceCategories: model.sourceCategories,
    };
  }
}
