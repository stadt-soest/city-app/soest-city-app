#!/bin/sh

MAX_SIZE=30

while ! curl -s http://orion:1026/version; do
  echo "Waiting for Orion Broker..."
  sleep 1
done

for file in /data/*.json; do
  service_path=$(basename "$file" .json)
  total_entities=$(jq '.entities | length' "$file")

  if [ $total_entities -gt 0 ]; then
    parts=$(((total_entities + MAX_SIZE - 1) / MAX_SIZE))

    i=0
    while [ $i -lt $parts ]; do
      start_index=$((i * MAX_SIZE))
      end_index=$(((i + 1) * MAX_SIZE - 1))
      if [ $end_index -ge $total_entities ]; then
        end_index=$((total_entities - 1))
      fi

      echo "Processing $file: part $i from index $start_index to $end_index"
      jq ".entities |= .[$start_index:$end_index]" $file >"/tmp/part_$i.json"
      curl -X POST -H 'Content-Type: application/json' -H 'Fiware-Service: soest_dataspace' -H "Fiware-ServicePath: /$service_path" -d @"/tmp/part_$i.json" http://orion:1026/v2/op/update?options=keyValues

      rm "/tmp/part_$i.json"

      i=$((i + 1))
    done

  else
    echo "No entities to process in $file."
  fi
done
