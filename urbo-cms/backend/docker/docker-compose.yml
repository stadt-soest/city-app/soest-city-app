version: '3.8'

services:
  meilisearch:
    container_name: urbo-backend-meilisearch
    image: getmeili/meilisearch:v1.7.6
    environment:
      - http_proxy
      - https_proxy
      - MEILI_MASTER_KEY=${MEILI_MASTER_KEY:-masterKey}
      - MEILI_NO_ANALYTICS=${MEILI_NO_ANALYTICS:-true}
      - MEILI_ENV=${MEILI_ENV:-development}
      - MEILI_LOG_LEVEL
      - MEILI_DB_PATH=${MEILI_DB_PATH:-/data.ms}
    ports:
      - "7700:7700"
    restart: unless-stopped

  urbo-cms-postgres:
    container_name: urbo-backend-postgres
    image: postgres:14.1-alpine
    restart: unless-stopped
    healthcheck:
      test: pg_isready -U postgres
      interval: 5s
      timeout: 5s
      retries: 10
      start_period: 5s
    volumes:
      - ./create-databases.sh:/docker-entrypoint-initdb.d/create-databases.sh
      - urbo-cms-data:/var/lib/postgresql/data
    ports:
      - "5432:5432"
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: postgres
      POSTGRES_DB: urbo
      POSTGRES_DB2: keycloak

  keycloak:
    container_name: urbo-backend-keycloak
    image: quay.io/keycloak/keycloak:24.0.4
    hostname: "keycloak-local"
    healthcheck:
      test: [ "CMD-SHELL", "exec 3<>/dev/tcp/127.0.0.1/8080;echo -e \"GET /health/ready HTTP/1.1\r\nhost: http://localhost\r\nConnection: close\r\n\r\n\" >&3;grep \"HTTP/1.1 200 OK\" <&3" ]
      interval: 10s
      timeout: 3s
      retries: 10
    command: start-dev -Dkeycloak.profile.feature.upload_scripts=enabled --import-realm -Dkeycloak.migration.strategy=OVERWRITE_EXISTING
    restart: unless-stopped
    stop_grace_period: 30s
    volumes:
      - ./imports:/opt/keycloak/data/import
    logging:
      driver: "json-file"
      options:
        max-size: "20m"
        max-file: "10"
    depends_on:
      urbo-cms-postgres:
        condition: service_healthy
    ports:
      - "8090:8080/tcp"
      - "8090:8080/udp"
    labels:
      name: "keycloak-local"
    environment:
      KC_HOSTNAME: localhost
      KC_HOSTNAME_PORT: 8090
      KC_HOSTNAME_STRICT_BACKCHANNEL: "true"
      KC_DB: postgres
      KC_DB_URL: jdbc:postgresql://urbo-cms-postgres:5432/keycloak?characterEncoding=UTF-8
      KC_DB_USERNAME: postgres
      KC_DB_PASSWORD: postgres
      KEYCLOAK_ADMIN: admin
      KEYCLOAK_ADMIN_PASSWORD: admin
      KC_HEALTH_ENABLED: "true"
      KC_LOG_LEVEL: info

  orion:
    container_name: urbo-backend-orion
    image: fiware/orion:3.12.0
    ports:
      - "1026:1026"
    command: -dbhost mongodb

  populate-orion:
    container_name: urbo-backend-populate-orion
    build: ./populate-orion
    image: populate-orion:latest
    volumes:
      - ./populate-orion/data:/data
    depends_on:
      - orion

  kong-dbless:
    container_name: urbo-backend-kong-dbless
    image: quay.io/fiware/kong:0.5.3
    depends_on:
      - orion
      - keycloak
    volumes:
      - ./kong.yml:/usr/local/kong/declarative/kong.yml
    environment:
      - KONG_DATABASE=off
      - KONG_DECLARATIVE_CONFIG=/usr/local/kong/declarative/kong.yml
      - KONG_ADMIN_ACCESS_LOG=/dev/stdout
      - KONG_PROXY_ERROR_LOG=/dev/stderr
      - KONG_ADMIN_ERROR_LOG=/dev/stderr
      - KONG_ADMIN_LISTEN=0.0.0.0:8001
      - KONG_LICENSE_DATA
      - KONG_LOG_LEVEL=info
      - KONG_PLUGINS=bundled,pep-plugin
      - KONG_PLUGINSERVER_NAMES=pep-plugin
      - KONG_PLUGINSERVER_PEP_PLUGIN_QUERY_CMD=/go-plugins/pep-plugin -dump
      - KONG_PLUGINSERVER_PEP_PLUGIN_START_CMD=/go-plugins/pep-plugin
    ports:
      - "8000:8000"
      - "8001:8001"

  mongodb:
    container_name: urbo-backend-mongodb
    image: mongo:5.0
    command: --nojournal
    volumes:
      - mongo-db:/data/db

  localstack:
    container_name: urbo-backend-localstack
    image: localstack/localstack
    ports:
      - "4566:4566"
    environment:
      - SERVICES=s3

volumes:
  urbo-cms-data:
  mongo-db:
  node-red-data:

networks:
  default:
    name: urbo-backend-network