plugins {
    id("org.jetbrains.kotlinx.kover")
    id("io.swcode.urbo.sonar-scan")
}

repositories {
    gradlePluginPortal()
}

dependencies {
    project.subprojects.forEach {
        if (it.getTasksByName("koverXmlReport", false).isNotEmpty()) {
            // Add kover report dependency for each subproject which has the "koverXmlReport" task available
            kover(project(it.path))
        }
    }
}