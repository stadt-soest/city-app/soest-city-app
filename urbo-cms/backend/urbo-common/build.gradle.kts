plugins {
    id("io.swcode.urbo.library-conventions")
    kotlin("kapt")
}

dependencies {
    api(libs.slf4j.api)
    api(libs.guava)
}

