package io.swcode.urbo.common.utils

import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldMatch
import org.junit.jupiter.api.Test


class FileUtilsKtTest {

    @Test
    fun `should return correct extension when file has extension`() {
        val filename = "example.txt"
        val extension = filename.getFileExtension()
        extension shouldBe "txt"
    }

    @Test
    fun `should return empty string when file has no extension`() {
        val filename = "example"
        val extension = filename.getFileExtension()
        extension shouldBe ""
    }

    @Test
    fun `should return empty string when filename is null`() {
        val filename: String? = null
        val extension = filename.getFileExtension()
        extension shouldBe ""
    }

    @Test
    fun `should generate UUID filename with extension`() {
        val filename = "example.txt"
        val generatedFilename = filename.generateUUIDFilename()
        val uuidPattern = "^[\\da-f]{8}(-[\\da-f]{4}){3}-[\\da-f]{12}\\.txt$".toRegex()
        generatedFilename shouldMatch uuidPattern
    }

    @Test
    fun `should generate UUID filename without extension`() {
        val filename = "example"
        val generatedFilename = filename.generateUUIDFilename()
        val uuidPattern = "^[\\da-f]{8}(-[\\da-f]{4}){3}-[\\da-f]{12}$".toRegex()
        generatedFilename shouldMatch uuidPattern
    }

    @Test
    fun `should generate UUID filename when filename is null`() {
        val filename: String? = null
        val generatedFilename = filename.generateUUIDFilename()
        val uuidPattern = "^[\\da-f]{8}(-[\\da-f]{4}){3}-[\\da-f]{12}$".toRegex()
        generatedFilename shouldMatch uuidPattern
    }

    @Test
    fun `should return correct content type for svg`() {
        val filename = "example.svg"
        val contentType = filename.determineContentType()
        contentType shouldBe "image/svg+xml"
    }

    @Test
    fun `should return correct content type for jpg`() {
        val filename = "example.jpg"
        val contentType = filename.determineContentType()
        contentType shouldBe "image/jpeg"
    }

    @Test
    fun `should return correct content type for jpeg`() {
        val filename = "example.jpeg"
        val contentType = filename.determineContentType()
        contentType shouldBe "image/jpeg"
    }

    @Test
    fun `should return correct content type for png`() {
        val filename = "example.png"
        val contentType = filename.determineContentType()
        contentType shouldBe "image/png"
    }

    @Test
    fun `should return octet-stream content type for unknown extension`() {
        val filename = "example.unknown"
        val contentType = filename.determineContentType()
        contentType shouldBe "application/octet-stream"
    }

    @Test
    fun `should return octet-stream content type when no extension`() {
        val filename = "example"
        val contentType = filename.determineContentType()
        contentType shouldBe "application/octet-stream"
    }
}