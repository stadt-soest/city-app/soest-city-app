package io.swcode.urbo.common.error

class NoOpException(message: String = "This operation is a no-op and should not be called.") : RuntimeException(message)
