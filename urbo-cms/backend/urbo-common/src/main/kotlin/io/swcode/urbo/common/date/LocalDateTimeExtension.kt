package io.swcode.urbo.common.date

import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

fun LocalDateTime.toBerlinInstant(): Instant {
    return this.atZone(APPLICATION_ZONE_ID).toInstant()
}

fun LocalDate.atEndOfDay(): LocalDateTime {
    return this.atTime(LocalTime.MAX)
}
