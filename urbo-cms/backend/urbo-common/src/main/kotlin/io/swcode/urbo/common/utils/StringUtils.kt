package io.swcode.urbo.common.utils

import java.net.URI

fun String.extractHost(): String {
    return try {
        URI(this).host ?: ""
    } catch (e: IllegalArgumentException) {
        ""
    }
}