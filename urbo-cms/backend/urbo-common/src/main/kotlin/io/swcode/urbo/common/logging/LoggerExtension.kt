package io.swcode.urbo.common.logging

import org.slf4j.Logger
import org.slf4j.LoggerFactory

inline fun <reified R : Any> R.createLogger(): Logger =
    LoggerFactory.getLogger(this::class.java.name.substringBefore("\$Companion"))