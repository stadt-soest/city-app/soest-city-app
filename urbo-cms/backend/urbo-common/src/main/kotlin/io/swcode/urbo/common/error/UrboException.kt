package io.swcode.urbo.common.error

enum class ErrorType(val userFriendlyMessage: String) {
    UNEXPECTED("An unexpected error occurred."),
    RESOURCE_NOT_FOUND("Resource was not found.")
}

class UrboException(
    message: String,
    val errorType: ErrorType = ErrorType.UNEXPECTED,
    cause: Throwable? = null
) : RuntimeException(message, cause)