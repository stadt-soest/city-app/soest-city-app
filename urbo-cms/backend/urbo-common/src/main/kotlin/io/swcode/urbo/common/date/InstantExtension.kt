package io.swcode.urbo.common.date

import java.time.Instant
import java.time.LocalDateTime

fun Instant.toBerlinInstant(): Instant {
    return this.atZone(APPLICATION_ZONE_ID).toInstant()
}

fun Instant.toBerlinLocalDateTime(): LocalDateTime {
    return LocalDateTime.ofInstant(this, APPLICATION_ZONE_ID)
}