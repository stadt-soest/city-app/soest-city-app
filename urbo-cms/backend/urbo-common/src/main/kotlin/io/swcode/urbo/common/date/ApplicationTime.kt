package io.swcode.urbo.common.date

import java.time.Clock
import java.time.ZoneId

val APPLICATION_ZONE_ID: ZoneId = ZoneId.of("Europe/Berlin")

object ApplicationClock {
    fun get(): Clock = Clock.system(APPLICATION_ZONE_ID)
}