package io.swcode.urbo.common.utils

import java.util.*

fun String?.getFileExtension(): String {
    return this?.substringAfterLast('.', "") ?: ""
}

fun String?.generateUUIDFilename(): String {
    val fileExtension = this.getFileExtension()
    return UUID.randomUUID().toString() + if (fileExtension.isNotEmpty()) ".$fileExtension" else ""
}

fun String.determineContentType(): String {
    return when (this.getFileExtension().lowercase()) {
        "svg" -> "image/svg+xml"
        "jpg", "jpeg" -> "image/jpeg"
        "png" -> "image/png"
        else -> "application/octet-stream"
    }
}