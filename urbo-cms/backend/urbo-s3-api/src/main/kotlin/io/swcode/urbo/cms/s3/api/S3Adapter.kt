package io.swcode.urbo.cms.s3.api

import java.io.InputStream
import java.net.URL
import java.time.Duration

interface S3Adapter {
    fun putS3Object(inputStream: InputStream, fileName: String)
    fun deleteS3Object(deletingObject: DeletingObject)
    fun requestSignedDownloadUrl(key: String, duration: Duration): URL
}