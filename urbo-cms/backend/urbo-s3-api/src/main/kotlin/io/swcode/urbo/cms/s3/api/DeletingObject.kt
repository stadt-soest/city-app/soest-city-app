package io.swcode.urbo.cms.s3.api

data class DeletingObject(
    val key: String,
)