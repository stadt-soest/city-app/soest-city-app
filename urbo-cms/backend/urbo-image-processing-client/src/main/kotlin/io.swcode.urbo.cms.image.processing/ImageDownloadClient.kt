package io.swcode.urbo.cms.image.processing

import io.swcode.urbo.common.utils.getFileExtension
import okhttp3.OkHttpClient
import okhttp3.Request
import org.springframework.stereotype.Component
import java.io.File
import java.io.FileOutputStream

@Component
class ImageDownloadClient(
    private val httpClient: OkHttpClient
) {
    fun downloadImage(url: String): LocalTemporaryImageFile {
        return httpClient.newCall(Request.Builder().url(url).build()).execute().use { response ->
            if (!response.isSuccessful) {
                throw Exception("Image download failed for command: $url")
            }
            val responseBody = response.body ?: throw Exception("Response body is null")

            responseBody.use {
                val tempFile = File.createTempFile("image", url.getFileExtension())

                FileOutputStream(tempFile).use { fileOutputStream ->
                    responseBody.byteStream().use {
                        it.transferTo(fileOutputStream)
                    }
                }
                LocalTemporaryImageFile(tempFile)
            }
        }
    }
}


