package io.swcode.urbo.cms.image.processing

import io.swcode.urbo.cms.s3.api.S3Adapter
import io.swcode.urbo.common.error.ErrorType
import io.swcode.urbo.common.error.UrboException
import io.swcode.urbo.common.logging.createLogger
import io.swcode.urbo.core.api.image.ImageProcessingAdapter
import io.swcode.urbo.core.api.image.ImageProcessingFileDto
import io.swcode.urbo.core.api.image.ImageProcessingResult
import io.swcode.urbo.core.api.image.ImageSpecs
import org.springframework.stereotype.Component
import java.io.File
import java.util.UUID

@Component
class DefaultImageProcessingAdapter(
    private val imageDownloadClient: ImageDownloadClient,
    private val s3Adapter: S3Adapter,
    private val imaginaryClient: ImaginaryClient
) : ImageProcessingAdapter {
    companion object {
        private val logger = createLogger()
    }

    override fun downloadAndProcessImage(imageUrl: String): ImageProcessingResult {
        return imageDownloadClient.downloadImage(imageUrl).use { localImageFile ->
            val thumbnailImageFile = convertImage(localImageFile.file, ImageSpecs.THUMBNAIL_RESOLUTION)
            val highResolutionImageFile = convertImage(localImageFile.file, ImageSpecs.HIGH_RESOLUTION)

            ImageProcessingResult(imageUrl, thumbnailImageFile, highResolutionImageFile)
        }
    }

    private fun convertImage(imageFile: File, targetSpecs: ImageSpecs): ImageProcessingFileDto {
        return try {
            imageFile.inputStream().use { inputStream ->
                imaginaryClient.processImage(inputStream, targetSpecs).use { convertedImageFile ->
                    val fileName = "${UUID.randomUUID()}.${convertedImageFile.file.extension}"

                    convertedImageFile.file.inputStream().use {
                        s3Adapter.putS3Object(it, fileName)
                    }

                    convertedImageFile.file.inputStream().use {
                        val dimensions = imaginaryClient.getImageDimensions(it)
                        ImageProcessingFileDto(fileName, dimensions.width, dimensions.height)
                    }
                }
            }
        } catch (e: Exception) {
            logger.debug("Error processing image: ${e.message}", e)
            throw UrboException("Error processing image",ErrorType.UNEXPECTED , e)
        }
    }
}