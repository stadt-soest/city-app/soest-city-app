package io.swcode.urbo.cms.image.processing

import com.google.gson.Gson
import io.swcode.urbo.core.api.image.ImageDimension
import io.swcode.urbo.core.api.image.ImageSpecs
import io.swcode.urbo.core.api.image.ProcessingType
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okio.BufferedSink
import okio.source
import org.springframework.stereotype.Component
import java.io.File
import java.io.IOException
import java.io.InputStream

data class ImageDimensionsResponse(val width: Int, val height: Int)

@Component
class ImaginaryClient(private val baseUrl: String = "https://imaginary.swcode.io") {
    private val client = OkHttpClient()
    private val gson = Gson()

    fun processImage(inputStream: InputStream, imageSpecs: ImageSpecs): LocalTemporaryImageFile {
        val url = when (imageSpecs.processingType) {
            ProcessingType.FIT -> "$baseUrl/fit?width=${imageSpecs.width}&height=${imageSpecs.height}&quality=${imageSpecs.quality}&interlace=true&type=jpeg&background=${imageSpecs.backgroundColor}&norotation=${imageSpecs.noRotation}"
            ProcessingType.RESIZE -> "$baseUrl/resize?width=${imageSpecs.width}&height=${imageSpecs.height}&quality=${imageSpecs.quality}&interlace=true&type=jpeg&background=${imageSpecs.backgroundColor}&norotation=${imageSpecs.noRotation}"
        }

        return makeRequest(inputStream, url).use { response ->
            if (!response.isSuccessful) {
                throw IOException("Unexpected response: $response")
            }

            response.body?.byteStream()?.use { responseBody ->
                val tmpFile = File.createTempFile("image", ".jpeg")
                tmpFile.outputStream().use { outputStream ->
                    responseBody.transferTo(outputStream)
                }
                LocalTemporaryImageFile(tmpFile)
            } ?: throw IOException("Response body is null")
        }
    }

    fun getImageDimensions(inputStream: InputStream): ImageDimension {
        val url = "$baseUrl/info"
        return makeRequest(inputStream, url).use { response ->
            val body = response.body?.string() ?: throw IOException("Response body is null")

            val dimensionsResponse = gson.fromJson(body, ImageDimensionsResponse::class.java)
                ?: throw IOException("Error parsing JSON response")

            ImageDimension(dimensionsResponse.width, dimensionsResponse.height)
        }
    }

    private fun makeRequest(inputStream: InputStream, url: String): Response {
        val requestBody = object : RequestBody() {
            override fun contentType(): MediaType? = "image/jpeg".toMediaTypeOrNull()
            override fun contentLength(): Long = -1
            override fun writeTo(sink: BufferedSink) {
                sink.writeAll(inputStream.source())
            }
        }

        val request = Request.Builder()
            .url(url)
            .post(requestBody)
            .build()

        return client.newCall(request).execute()
    }
}
