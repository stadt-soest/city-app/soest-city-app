package io.swcode.urbo.cms.image.processing


import okhttp3.OkHttpClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan
class ImageProcessingConfig {
    @Bean
    fun okHttpClien(): OkHttpClient {
        return OkHttpClient()
    }
}