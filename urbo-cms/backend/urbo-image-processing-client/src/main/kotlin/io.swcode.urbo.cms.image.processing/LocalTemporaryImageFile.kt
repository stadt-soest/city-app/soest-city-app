package io.swcode.urbo.cms.image.processing

import io.swcode.urbo.common.logging.createLogger
import java.io.File

data class LocalTemporaryImageFile(val file: File) : AutoCloseable {
    companion object {
        private val logger = createLogger()
    }

    override fun close() {
        if(!file.delete()) {
            logger.warn("Failed to delete file ${file.absolutePath}")
        }
    }
}