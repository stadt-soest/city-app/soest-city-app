package io.swcode.urbo.cms.image.processing

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe
import io.swcode.urbo.cms.image.processing.testutil.loadImageFromResources
import io.swcode.urbo.core.api.image.ImageSpecs
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.ByteArrayInputStream
import java.io.IOException

class ImageProcessingClientTest {

    private lateinit var imaginaryClient: ImaginaryClient

    @BeforeEach
    fun setUp() {
        imaginaryClient = ImaginaryClient(ImaginaryTestContainer.getUrl())
    }

    @Test
    fun `should process and verify dimensions`() {
        val imageInputStream = loadImageFromResources("response.jpeg").inputStream()

        val result = imaginaryClient.processImage(
            imageInputStream,
            ImageSpecs.THUMBNAIL_RESOLUTION
        )

        imaginaryClient.getImageDimensions(result.file.inputStream()).height.shouldBe(1024)
        imaginaryClient.getImageDimensions(result.file.inputStream()).width.shouldBe(1024)
    }

    @Test
    fun `process image should throw exception on error`() {
        val invalidInputStream = ByteArrayInputStream("123".toByteArray())

        shouldThrow<IOException> {
            imaginaryClient.processImage(
                invalidInputStream,
                ImageSpecs.THUMBNAIL_RESOLUTION
            )
        }
    }
}
