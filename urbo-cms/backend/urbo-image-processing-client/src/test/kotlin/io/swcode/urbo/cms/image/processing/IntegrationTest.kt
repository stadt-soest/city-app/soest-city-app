package io.swcode.urbo.cms.image.processing

import io.swcode.urbo.cms.s3.S3Configuration
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension


@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@ExtendWith(SpringExtension::class)
@ContextConfiguration(classes = [TestConfig::class, S3Configuration::class, ImageProcessingConfig::class])
annotation class IntegrationTest