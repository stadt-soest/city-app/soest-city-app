package io.swcode.urbo.cms.image.processing.testutil


fun loadImageFromResources(imageName: String): ByteArray {
    val classLoader = Thread.currentThread().contextClassLoader
    val inputStream = classLoader.getResourceAsStream(imageName)
        ?: throw IllegalArgumentException("Image not found: $imageName")

    return inputStream.readBytes()
}


