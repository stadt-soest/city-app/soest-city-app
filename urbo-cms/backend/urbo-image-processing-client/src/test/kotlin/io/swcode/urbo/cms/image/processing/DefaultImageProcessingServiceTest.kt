package io.swcode.urbo.cms.image.processing

import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.swcode.urbo.core.api.image.ImageProcessingAdapter
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.Buffer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ClassPathResource
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.localstack.LocalStackContainer
import org.testcontainers.utility.DockerImageName


@IntegrationTest
class DefaultImageProcessingServiceTest {

    private lateinit var mockWebServer: MockWebServer

    @Autowired
    private lateinit var sut: ImageProcessingAdapter

    @BeforeEach
    fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
    }

    @AfterEach
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `should download image and upload thumbnail and high resolution to s3`() {
        val resource = ClassPathResource("response.jpeg")
        val buffer = Buffer()
        buffer.write(resource.inputStream.readAllBytes())
        mockWebServer.enqueue(
            MockResponse()
                .setBody(buffer)
                .addHeader("Content-Type", "image/jpeg")
        )

        val imageUrl = mockWebServer.url("/response.jpeg").toString()

        val actual = sut.downloadAndProcessImage(imageUrl)
        actual.thumbnail.shouldNotBeNull()
        actual.thumbnail!!.width.shouldBe(1024)
        actual.thumbnail!!.height.shouldBe(1024)

        actual.highRes.shouldNotBeNull()
        actual.highRes!!.width.shouldBe(1024)
        actual.highRes!!.height.shouldBe(1024)
    }

    companion object {
        private val localStackContainer = LocalStackContainer(DockerImageName.parse("localstack/localstack:latest"))
            .withServices(LocalStackContainer.Service.S3)
            .withReuse(true)

        @DynamicPropertySource
        @JvmStatic
        fun registerDynamicProperties(registry: DynamicPropertyRegistry) {
            localStackContainer.start()
            val endpointUrl = localStackContainer.getEndpointOverride(LocalStackContainer.Service.S3).toString()
            val bucketName = "test-bucket"
            registry.add("aws.s3.bucketName") { bucketName }
            registry.add("aws.s3.endpoint") { endpointUrl }
        }
    }
}