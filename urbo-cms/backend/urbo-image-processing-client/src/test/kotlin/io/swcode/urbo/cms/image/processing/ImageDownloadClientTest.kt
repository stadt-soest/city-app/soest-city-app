package io.swcode.urbo.cms.image.processing

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.Buffer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.core.io.ClassPathResource


class ImageDownloadClientTest {
    private lateinit var imageDownloadClient: ImageDownloadClient

    private lateinit var mockWebServer: MockWebServer

    @BeforeEach
    fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
        imageDownloadClient = ImageDownloadClient(OkHttpClient())
    }

    @AfterEach
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `should return InputStream of image when successful response`() {
        val resource = ClassPathResource("response.jpeg")
        val buffer = Buffer()
        buffer.write(resource.inputStream.readAllBytes())
        mockWebServer.enqueue(
            MockResponse()
                .setBody(buffer)
                .addHeader("Content-Type", "image/jpeg")
        )

        val imageUrl = mockWebServer.url("/response.jpeg").toString()

        val actual = imageDownloadClient.downloadImage(imageUrl).file.inputStream().readAllBytes()

        val resourceBytes = resource.inputStream.use { it.readAllBytes() }

        actual shouldBe resourceBytes
    }

    @Test
    fun `should throw Exception when Client error`() {
        mockWebServer.enqueue(
            MockResponse().setResponseCode(400)
        )

        val imageUrl = mockWebServer.url("/response.jpeg").toString()

        shouldThrow<Exception> {
            imageDownloadClient.downloadImage(imageUrl)
        }
    }
}