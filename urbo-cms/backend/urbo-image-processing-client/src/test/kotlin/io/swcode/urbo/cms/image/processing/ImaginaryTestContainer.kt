package io.swcode.urbo.cms.image.processing

import org.testcontainers.containers.GenericContainer

object ImaginaryTestContainer {
    private val HTTP_PORT = 8080

    val container = GenericContainer("h2non/imaginary")

    init {
        container.addExposedPort(HTTP_PORT)
        container.addEnv("PORT", "$HTTP_PORT")
        container.start()
    }

    fun getUrl() = "http://${container.host}:${container.getMappedPort(HTTP_PORT)}"
}