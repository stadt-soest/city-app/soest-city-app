plugins {
    id("io.swcode.urbo.library-conventions")
}

dependencies {
    implementation(project(":urbo-common"))
    implementation(project(":urbo-s3-api"))
    implementation(project(":urbo-core-api"))
    implementation(libs.spring.boot)
    implementation(libs.okhttp)
    implementation(libs.gson)

    testImplementation(project(":urbo-s3"))
    testImplementation(libs.okhttp.mockwebserver)
    testImplementation(libs.spring.boot.starter.test)
    testImplementation(libs.testcontainers)
    testImplementation(libs.testcontainers.localstack)
}
