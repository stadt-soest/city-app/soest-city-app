package io.swcode.urbo.core.api.auth

import java.util.*

data class IdpUserDto(
    val id: UUID,
    val email: String
)