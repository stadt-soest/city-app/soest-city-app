package io.swcode.urbo.core.api.auth

interface AuthUserAdapter {
    fun createUser(email: String, firstName: String, lastName: String): IdpUserDto
    fun deleteUserById(userId: String)
    fun updateUser(userId: String, firstName: String, lastName: String)
    fun resetPasswordByEmail(email: String)
}