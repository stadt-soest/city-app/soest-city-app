package io.swcode.urbo.command.api

import kotlin.reflect.KClass

interface CommandRegistry {
    fun getCommandClass(commandName: String): KClass<out Command>
    fun getAllCommandClasses(): Collection<KClass<out Command>>
    fun registerCommand(commandClass: KClass<out Command>)
}