package io.swcode.urbo.command.api

fun interface CommandHandler<C : Command> {
    fun handle(command: C)
}
