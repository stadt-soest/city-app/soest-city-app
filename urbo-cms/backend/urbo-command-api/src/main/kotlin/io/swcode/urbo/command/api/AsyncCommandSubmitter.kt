package io.swcode.urbo.command.api

fun interface AsyncCommandSubmitter {
    fun submit(command: WrappedAsyncCommand<Command>)
}