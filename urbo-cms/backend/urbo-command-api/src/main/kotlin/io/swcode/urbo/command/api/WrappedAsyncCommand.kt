package io.swcode.urbo.command.api

import java.time.Duration
import java.util.UUID

class WrappedAsyncCommand<T : Command>(val command: T, val delay: Duration? = null, val commandId: UUID = UUID.randomUUID())

