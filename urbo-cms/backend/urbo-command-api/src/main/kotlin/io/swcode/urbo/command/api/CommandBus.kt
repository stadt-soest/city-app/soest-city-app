package io.swcode.urbo.command.api

import java.time.Duration
import java.util.UUID

interface CommandBus {
    fun execute(commandId: UUID, command: Command)
    fun submitAsyncCommand(command: Command)
    fun submitCommandWithDelay(command: Command, delay: Duration)
}