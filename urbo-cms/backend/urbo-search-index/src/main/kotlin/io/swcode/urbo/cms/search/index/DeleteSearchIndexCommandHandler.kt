package io.swcode.urbo.cms.search.index

import io.swcode.urbo.cms.search.meili.api.SearchIndexerAdapter
import io.swcode.urbo.command.api.CommandHandler
import org.springframework.stereotype.Component

@Component
class DeleteSearchIndexCommandHandler(
    private val searchIndexerAdapter: SearchIndexerAdapter
) : CommandHandler<DeleteSearchIndexCommand> {

    override fun handle(command: DeleteSearchIndexCommand) {
        searchIndexerAdapter.deleteFromIndex(command.indexId, command.entityId)
    }
}