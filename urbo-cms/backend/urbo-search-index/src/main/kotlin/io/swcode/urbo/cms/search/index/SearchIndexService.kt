package io.swcode.urbo.cms.search.index

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.convertValue
import io.swcode.urbo.cms.search.meili.api.SearchIndexConfig
import io.swcode.urbo.cms.search.meili.api.SearchIndexedEntity
import io.swcode.urbo.command.api.CommandBus
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service

@Service
class SearchIndexService(
    @Lazy private val commandBus: CommandBus,
    private val objectMapper: ObjectMapper,
    private val searchIndexConfig: SearchIndexConfig
) {

    fun updateOrRemoveSearchIndex(entity: SearchIndexedEntity) {
        if (entity.visible) {
            updateSearchIndex(entity)
        } else {
            removeFromSearchIndex(entity)
        }
    }

    fun removeFromSearchIndex(entity: SearchIndexedEntity) {
        val indexes = searchIndexConfig.getIndexesByEntityName(entity::class.simpleName!!)
        indexes.forEach { index ->
            commandBus.submitAsyncCommand(
                DeleteSearchIndexCommand(index.uid, index.idDeletionSelector(entity))
            )
        }
    }

    private fun updateSearchIndex(entity: SearchIndexedEntity) {
        val indexes = searchIndexConfig.getIndexesByEntityName(entity::class.simpleName!!)
        indexes.forEach { index ->
            val dto = index.mapper(entity)
            val payload = objectMapper.convertValue<Map<String, Any>>(dto)
            commandBus.submitAsyncCommand(
                UpdateSearchIndexCommand(index.uid, payload)
            )
        }
    }
}