package io.swcode.urbo.cms.search.index

import io.swcode.urbo.cms.search.meili.api.SearchIndexedEntity
import jakarta.persistence.PostPersist
import jakarta.persistence.PostRemove
import jakarta.persistence.PostUpdate
import org.springframework.stereotype.Component

@Component
class SearchIndexEntityListener(
    private val searchIndexService: SearchIndexService
) {
    @PostPersist
    fun onPersist(entity: SearchIndexedEntity) = searchIndexService.updateOrRemoveSearchIndex(entity)

    @PostUpdate
    fun onUpdate(entity: SearchIndexedEntity) = searchIndexService.updateOrRemoveSearchIndex(entity)

    @PostRemove
    fun onRemove(entity: SearchIndexedEntity) = searchIndexService.removeFromSearchIndex(entity)
}