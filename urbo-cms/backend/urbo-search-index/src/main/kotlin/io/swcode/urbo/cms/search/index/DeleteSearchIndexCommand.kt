package io.swcode.urbo.cms.search.index

import io.swcode.urbo.command.api.Command

data class DeleteSearchIndexCommand(
    val indexId: String,
    val entityId: String
): Command