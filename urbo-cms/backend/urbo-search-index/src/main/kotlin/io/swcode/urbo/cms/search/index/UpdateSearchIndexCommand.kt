package io.swcode.urbo.cms.search.index

import io.swcode.urbo.command.api.Command

data class UpdateSearchIndexCommand(
    val indexId: String,
    val payload: Map<String, Any?>
) : Command