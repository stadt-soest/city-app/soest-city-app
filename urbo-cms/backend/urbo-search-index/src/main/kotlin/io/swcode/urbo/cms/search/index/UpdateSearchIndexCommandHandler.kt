package io.swcode.urbo.cms.search.index

import io.swcode.urbo.cms.search.meili.api.SearchIndexerAdapter
import io.swcode.urbo.command.api.CommandHandler
import org.springframework.stereotype.Component

@Component
class UpdateSearchIndexCommandHandler(
    private val searchIndexerAdapter: SearchIndexerAdapter
) : CommandHandler<UpdateSearchIndexCommand> {

    override fun handle(command: UpdateSearchIndexCommand) {
        searchIndexerAdapter.updateIndex(command.indexId, command.payload)
    }
}