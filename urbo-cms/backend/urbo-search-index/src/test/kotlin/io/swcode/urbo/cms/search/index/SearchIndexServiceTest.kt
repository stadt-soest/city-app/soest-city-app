package io.swcode.urbo.cms.search.index

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.swcode.urbo.cms.search.meili.api.SearchIndexConfig
import io.swcode.urbo.cms.search.meili.api.SearchIndexedDto
import io.swcode.urbo.cms.search.meili.api.SearchIndexedEntity
import io.swcode.urbo.command.api.CommandBus
import org.junit.jupiter.api.Test

class SearchIndexServiceTest {
    private val mockCommandBus = mockk<CommandBus>(relaxed = true)
    private val objectMapper = jacksonObjectMapper()
    private val mockIndexConfig = mockk<SearchIndexConfig>()
    private val service = SearchIndexService(mockCommandBus, objectMapper, mockIndexConfig)

    @Test
    fun `updateOrRemoveSearchIndex should update when visible`() {
        val entity = mockk<SearchIndexedEntity> {
            every { visible } returns true
        }

        val entityClassName = entity::class.simpleName!!
        val mockIndex = mockk<SearchIndexConfig.Index>()
        val mockDto = mockk<SearchIndexedDto>()

        every { mockIndex.uid } returns "test-uid"
        every { mockIndex.mapper(entity) } returns mockDto
        every { mockIndex.idDeletionSelector(entity) } returns "test-id"

        every { mockIndexConfig.getIndexesByEntityName(entityClassName) } returns listOf(mockIndex)

        service.updateOrRemoveSearchIndex(entity)

        verify(exactly = 1) { mockCommandBus.submitAsyncCommand(ofType(UpdateSearchIndexCommand::class)) }
        verify(exactly = 0) { mockCommandBus.submitAsyncCommand(ofType(DeleteSearchIndexCommand::class)) }
    }

    @Test
    fun `updateOrRemoveSearchIndex should remove when not visible`() {
        val entity = mockk<SearchIndexedEntity> {
            every { visible } returns false
        }

        val entityClassName = entity::class.simpleName!!
        val mockIndex = mockk<SearchIndexConfig.Index>()

        every { mockIndex.uid } returns "test-uid"
        every { mockIndex.idDeletionSelector(entity) } returns "test-id"

        every { mockIndexConfig.getIndexesByEntityName(entityClassName) } returns listOf(mockIndex)

        service.updateOrRemoveSearchIndex(entity)

        verify(exactly = 1) { mockCommandBus.submitAsyncCommand(ofType(DeleteSearchIndexCommand::class)) }
        verify(exactly = 0) { mockCommandBus.submitAsyncCommand(ofType(UpdateSearchIndexCommand::class)) }
    }
}