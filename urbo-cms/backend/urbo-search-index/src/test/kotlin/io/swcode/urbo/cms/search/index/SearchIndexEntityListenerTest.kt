package io.swcode.urbo.cms.search.index

import io.mockk.mockk
import io.mockk.verify
import io.swcode.urbo.cms.search.meili.api.SearchIndexedEntity
import org.junit.jupiter.api.Test

class SearchIndexEntityListenerTest {
    private val mockService = mockk<SearchIndexService>(relaxed = true)
    private val listener = SearchIndexEntityListener(mockService)

    @Test
    fun `onPersist should trigger updateOrRemove`() {
        val entity = mockk<SearchIndexedEntity>()

        listener.onPersist(entity)

        verify { mockService.updateOrRemoveSearchIndex(entity) }
    }

    @Test
    fun `onUpdate should trigger updateOrRemove`() {
        val entity = mockk<SearchIndexedEntity>()

        listener.onUpdate(entity)

        verify { mockService.updateOrRemoveSearchIndex(entity) }
    }

    @Test
    fun `onRemove should trigger removeFromSearchIndex`() {
        val entity = mockk<SearchIndexedEntity>()

        listener.onRemove(entity)

        verify { mockService.removeFromSearchIndex(entity) }
    }
}