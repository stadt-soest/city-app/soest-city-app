plugins {
    id("io.swcode.urbo.library-conventions")
}

dependencies {
    implementation(libs.spring.boot.starter.web)
    implementation(libs.jakarta.persistence.api)
    implementation(libs.jackson.module.kotlin)
    implementation(project(":urbo-command-api"))
    implementation(project(":urbo-meilisearch-api"))

    testImplementation(libs.mockk)
    testImplementation(libs.springmockk)
}