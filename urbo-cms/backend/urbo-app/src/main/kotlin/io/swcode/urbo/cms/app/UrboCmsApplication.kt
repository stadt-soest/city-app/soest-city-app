package io.swcode.urbo.cms.app

import de.hypertegrity.orion.HypertegrityConfig
import io.swcode.urbo.cms.CoreConfiguration
import io.swcode.urbo.cms.adapter.AdapterConfig
import io.swcode.urbo.cms.image.processing.ImageProcessingConfig
import io.swcode.urbo.cms.queue.QueueConfiguration
import io.swcode.urbo.cms.s3.S3Configuration
import io.swcode.urbo.cms.search.meili.MeiliSearchConfig
import io.swcode.urbo.command.CommandConfiguration
import io.swcode.urbo.keycloak.KeycloakAdapterConfiguration
import io.swcode.urbo.web.UrboWebConfig
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Import
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@Import(
    CoreConfiguration::class,
    AdapterConfig::class,
    HypertegrityConfig::class,
    CommandConfiguration::class,
    S3Configuration::class,
    ImageProcessingConfig::class,
    MeiliSearchConfig::class,
    QueueConfiguration::class,
    KeycloakAdapterConfiguration::class,
    UrboWebConfig::class,
)
@EnableScheduling
@EnableAsync
open class UrboCmsApplication

fun main(args: Array<String>) {
    runApplication<UrboCmsApplication>(*args)
}
