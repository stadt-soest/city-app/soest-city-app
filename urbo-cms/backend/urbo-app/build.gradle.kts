plugins {
    id("io.swcode.urbo.application-conventions")
}

dependencies {
    implementation(project(":urbo-hypertegrity-adapter"))
    implementation(project(":urbo-command"))
    implementation(project(":urbo-web"))
    implementation(project(":urbo-core"))
    implementation(project(":urbo-hypertegrity"))
    implementation(project(":urbo-queue"))
    implementation(project(":urbo-s3"))
    implementation(project(":urbo-image-processing-client"))
    implementation(project(":urbo-search-meilisearch"))
    implementation(project(":urbo-keycloak-adapter"))

    implementation(libs.spring.boot)
    implementation(libs.spring.boot.autoconfigure)
}

tasks.bootJar {
    enabled = true
}

tasks.jar {
    enabled = false
}
