# Urbo CMS Backend

Welcome to the Urbo CMS Backend! This guide will help you get started with setting up and running the application.

[![Build Status](https://dev.azure.com/spotar-io/spotar/_apis/build/status%2Furbo.cms-backend?branchName=main)](https://dev.azure.com/spotar-io/spotar/_build/latest?definitionId=71&branchName=main) [![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sw-code_Urbo&metric=alert_status&token=fc1210a2a2e66da300700189a0e7e32151768630)](https://sonarcloud.io/summary/new_code?id=sw-code_Urbo)

## Requirements

Before you begin, please ensure you have the following installed:

* [Docker](https://www.docker.com/products/docker-desktop)
* [JDK 21](https://jdk.java.net/21/) (required only if you are not using Docker to run the Spring application.)

## Setting Up

1. **Docker Setup**: Before running the application, start the necessary Docker containers:

   * Navigate to the `/docker` directory:

    ```shell
    cd docker
    ```

   * To start the containers without the Spring application:

    ```shell
    docker-compose up
    ```
   > **Caution:** Ensure Docker containers are completely started and all services are up before proceeding to run the application. Some services might take longer to initialize, so be patient.

   * To start the containers including the Spring application:

    ```shell
    docker-compose -f ./docker-compose.yml -f ./spring/docker-compose.spring.yml up
    ```
   > **Note:** If you use the option to start the containers including the Spring application, the next steps for running the application are not necessary.

2. **Running the Application**: You have two options:
   * **Using Your IDE**:
      * Locate the class `io.swcode.urbo.cms.app.UrboCmsApplication`.
      * Execute the `main` method within the class.

   * **Using Gradle**:
      * Run the application using the Gradle wrapper command:

    ```shell
    ./gradlew :urbo-app:bootRun
    ```

## Feedback and Contributions

If you encounter any issues or would like to contribute to the project, please feel free to open a pull request or an issue. We appreciate your feedback and contributions!

Thank you for your interest in the Urbo CMS Backend. Happy coding!