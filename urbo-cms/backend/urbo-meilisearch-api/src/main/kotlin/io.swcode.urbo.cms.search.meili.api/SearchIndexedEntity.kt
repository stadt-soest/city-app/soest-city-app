package io.swcode.urbo.cms.search.meili.api

import java.util.*

interface SearchIndexedEntity {
    val id: UUID
    val visible: Boolean
}