package io.swcode.urbo.cms.search.meili.api

interface SearchIndexerAdapter {
    fun updateIndex(indexId: String, payload: Map<String, Any?>)
    fun deleteFromIndex(indexId: String, documentId: String)
}