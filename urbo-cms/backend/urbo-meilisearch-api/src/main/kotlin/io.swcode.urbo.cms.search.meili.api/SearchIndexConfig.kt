package io.swcode.urbo.cms.search.meili.api

data class SearchIndexConfig(val indexes: List<Index>) {
    data class Index(
        val entityNames: List<String>,
        val uid: String,
        val sort: List<String>? = null,
        val distinct: String? = null,
        val primaryKey: String = "id",
        val filterableAttributes: List<String>? = null,
        val mapper: (SearchIndexedEntity) -> SearchIndexedDto,
        val idDeletionSelector: (SearchIndexedEntity) -> String = { entity -> entity.id.toString() }
    )

    fun getIndexesByEntityName(entityName: String): List<Index> {
        return indexes.filter { it.entityNames.contains(entityName) }
    }
}