package io.swcode.urbo

plugins {
    id("org.sonarqube")
}

sonar {
    properties {
        property("sonar.projectKey", "sw-code_Urbo")

        property("sonar.host.url", "https://sonarcloud.io")
        property("sonar.projectName", "urbo-cms-backend")
        property("sonar.organization", "spotar")
        property("sonar.forceAuthentication", "true")
        property("sonar.sonar.sourceEncoding", "UTF-8")
        property("sonar.language", "kotlin")
        property("sonar.java.coveragePlugin", "jacoco")
        property("sonar.scm.provider", "git")
        property(
            "sonar.coverage.jacoco.xmlReportPaths",
            "${project.layout.buildDirectory.get().asFile}/reports/kover/report.xml"
        )
        property("sonar.coverage.exclusions", "**src/test/**/*.*")
    }

    if (project.hasProperty("pullRequest.key") && project.property("pullRequest.key").toString().isNotBlank()) {
        // Its a Pull Request
        properties {
            property("sonar.pullrequest.key", "${project.property("pullRequest.key")}")
            property("sonar.pullrequest.branch", "${project.property("pullRequest.branch")}")
            property("sonar.pullrequest.base", "${project.property("pullRequest.target")}")
        }
    } else if (project.hasProperty("branch.name")) {
        // Branch analysis
        properties {
            property("sonar.branch.name", "${project.property("branch.name")}")
        }
    }
}
