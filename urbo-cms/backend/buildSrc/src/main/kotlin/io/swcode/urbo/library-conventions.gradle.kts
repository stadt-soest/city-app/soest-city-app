package io.swcode.urbo

plugins {
    // Apply the common convention plugin for shared build configuration between library and application projects.
    id("io.swcode.urbo.common-conventions")

    // Apply the java-library plugin for API and implementation separation.
    `java-library`
    `java-test-fixtures`
    id("org.springframework.boot")
    id("io.spring.dependency-management")
    id("org.jetbrains.kotlin.plugin.spring")
    id("io.swcode.urbo.github-publish")
}

tasks.bootJar {
    enabled = false
}

tasks.jar {
    enabled = true
}