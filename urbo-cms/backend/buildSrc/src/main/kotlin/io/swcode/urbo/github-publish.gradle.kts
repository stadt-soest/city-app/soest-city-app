package io.swcode.urbo

plugins {
    id("maven-publish")
}

version = findProperty("appVersion")?.toString()
    ?: "0.0.0-SNAPSHOT"

configure<PublishingExtension> {
    repositories {
        maven {
            name = "GitHubPackages"
            url = uri("https://maven.pkg.github.com/sw-code/Urbo")
            credentials {
                username = System.getenv("GITHUB_ACTOR")
                password = System.getenv("GITHUB_TOKEN")
            }
        }
    }

    publications {
        create<MavenPublication>("gpr") {
            from(components["java"])
        }
    }
}
