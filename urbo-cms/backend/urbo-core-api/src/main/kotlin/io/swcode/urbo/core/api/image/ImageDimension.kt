package io.swcode.urbo.core.api.image

data class ImageDimension (
    val width: Int,
    val height: Int
)