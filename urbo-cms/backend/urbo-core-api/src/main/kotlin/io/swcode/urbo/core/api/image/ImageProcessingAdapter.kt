package io.swcode.urbo.core.api.image

interface ImageProcessingAdapter {
    fun downloadAndProcessImage(imageUrl: String): ImageProcessingResult
}