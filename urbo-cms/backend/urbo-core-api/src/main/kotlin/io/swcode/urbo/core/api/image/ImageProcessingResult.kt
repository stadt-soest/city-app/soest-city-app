package io.swcode.urbo.core.api.image

data class ImageProcessingResult(
    val imageSource: String,
    val thumbnail: ImageProcessingFileDto? = null,
    val highRes: ImageProcessingFileDto? = null,
    val description: String? = null,
    val copyrightHolder: String? = null
)

data class ImageProcessingFileDto(
    val filename: String,
    var width: Int? = null,
    var height: Int? = null
)


