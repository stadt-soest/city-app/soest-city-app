package io.swcode.urbo.core.api.image

enum class ImageSpecs(
    val width: Int,
    val height: Int,
    val quality: Int,
    val backgroundColor: String,
    val noRotation: Boolean,
    val processingType: ProcessingType
) {
    THUMBNAIL_RESOLUTION(1024, 2048, 60, "255,255,255", true, ProcessingType.FIT),
    HIGH_RESOLUTION(4000, 4000, 80, "255,255,255", true, ProcessingType.FIT)

}

enum class ProcessingType {
    FIT,
    RESIZE
}
