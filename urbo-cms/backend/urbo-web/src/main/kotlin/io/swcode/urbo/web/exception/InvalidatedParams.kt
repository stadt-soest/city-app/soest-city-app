package io.swcode.urbo.web.exception

data class InvalidatedParams(val cause: String, val attribute: String)
