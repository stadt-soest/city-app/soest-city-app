package io.swcode.urbo.web.exception

import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class RestErrorHandler : ResponseEntityExceptionHandler()