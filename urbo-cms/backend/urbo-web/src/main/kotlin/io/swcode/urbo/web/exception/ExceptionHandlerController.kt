package io.swcode.urbo.web.exception

import io.swcode.urbo.common.error.ErrorType
import io.swcode.urbo.common.error.UrboException
import io.swcode.urbo.common.logging.createLogger
import jakarta.validation.ConstraintViolationException
import org.springframework.http.HttpStatus
import org.springframework.http.ProblemDetail
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
class ExceptionHandlerController {

    @ExceptionHandler(UrboException::class)
    private fun handleUrboException(e: UrboException): ResponseEntity<ProblemDetail> {
        val status = when (e.errorType) {
            ErrorType.RESOURCE_NOT_FOUND -> HttpStatus.NOT_FOUND
            else -> {
                logger.error("UrboException (unexpected): {}", e.message, e)
                HttpStatus.INTERNAL_SERVER_ERROR
            }
        }

        val problemDetail = ProblemDetail.forStatusAndDetail(status, e.errorType.userFriendlyMessage)
        problemDetail.title = e.errorType.name
        problemDetail.setProperty("errorType", e.errorType)

        return ResponseEntity(problemDetail, status)
    }

    @ExceptionHandler(ConstraintViolationException::class)
    fun handleConstraintViolationException(e: ConstraintViolationException): ProblemDetail {
        val errors = e.constraintViolations.toSet()
        val validationResponse = errors.map { err ->
            InvalidatedParams(
                cause = err.message,
                attribute = err.propertyPath.toString()
            )
        }

        val problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, "Request validation failed")
        problemDetail.title = "Validation Failed"
        problemDetail.setProperty("invalidParams", validationResponse)
        return problemDetail
    }

    companion object {
        private val logger = createLogger()
    }
}
