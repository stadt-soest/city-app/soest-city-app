package io.swcode.urbo.web.security

import org.springframework.core.convert.converter.Converter
import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken

class KeycloakTokenConverter : Converter<Jwt, AbstractAuthenticationToken> {
    override fun convert(source: Jwt): AbstractAuthenticationToken {
        return KeycloakAuthenticationToken(source)
    }
}

private class KeycloakAuthenticationToken(jwt: Jwt) :
    JwtAuthenticationToken(jwt, extractAuthorities(jwt), getUserName(jwt)) {

    companion object {
        private const val CLAIM_USER_NAME = "preferred_username"
        private const val CLAIM_REALM_ACCESS = "realm_access"
        private const val CLAIM_ROLES = "roles"

        private fun getRealmRoles(jwt: Jwt): Collection<String> {
            val claimRealmAccess = jwt.getClaimAsMap(CLAIM_REALM_ACCESS).toMap()
            return getRoles(claimRealmAccess)
        }

        private fun getRoles(claim: Map<*, *>): Collection<String> {
            val rolesArray = claim[CLAIM_ROLES]
            if (rolesArray !is List<*>) return emptyList()

            return rolesArray.mapNotNull { it.toString() }
        }

        private fun extractAuthorities(jwt: Jwt): Collection<GrantedAuthority> {
            return getRealmRoles(jwt).map { role -> SimpleGrantedAuthority("ROLE_$role") }
        }

        private fun getUserName(jwt: Jwt): String? = jwt.getClaimAsString(CLAIM_USER_NAME)
    }
}
