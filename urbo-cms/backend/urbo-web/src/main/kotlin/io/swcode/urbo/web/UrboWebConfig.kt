package io.swcode.urbo.web

import io.swcode.urbo.web.exception.ExceptionHandlerController
import io.swcode.urbo.web.exception.RestErrorHandler
import io.swcode.urbo.web.security.SecurityConfiguration
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@Import(RestErrorHandler::class, SecurityConfiguration::class, ExceptionHandlerController::class)
class UrboWebConfig