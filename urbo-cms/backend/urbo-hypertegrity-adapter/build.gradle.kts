plugins {
    id("io.swcode.urbo.library-conventions")
}

dependencies {
    implementation(project(":urbo-common"))
    implementation(project(":urbo-core"))
    implementation(project(":urbo-meilisearch-api"))
    implementation(project(":urbo-core-api"))

    implementation(project(":urbo-hypertegrity"))
    implementation(libs.jackson.databind)
    implementation(libs.spring.boot.starter.data.jpa)
    implementation(libs.spring.boot.starter.web)

    testImplementation(libs.mockk)
}