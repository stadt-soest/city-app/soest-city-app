package io.swcode.urbo.cms.adapter.waste

import de.hypertegrity.orion.dto.WasteCollectionOrionDto
import io.swcode.urbo.cms.domain.model.waste.WasteCollectionLocation
import io.swcode.urbo.cms.domain.model.waste.WasteCollectionDate
import io.swcode.urbo.cms.domain.model.waste.WasteType
import java.time.Instant
import java.util.*

object OrionWasteAssembler {
    fun toEntity(dtos: List<WasteCollectionOrionDto>): List<WasteCollectionLocation> =
        dtos.map { it.toWaste() }

    private fun WasteCollectionOrionDto.toWaste(): WasteCollectionLocation {
        val wasteCollectionLocation = WasteCollectionLocation(
            id = UUID.randomUUID(),
            wasteId = id,
            city = address.addressLocality,
            street = address.streetAddress,
            wasteCollectionDates = mutableListOf()
        )

        val wasteDates = listOf(
            mapToWasteDates(schedule.wastepaper, wasteCollectionLocation, WasteType.WASTEPAPER),
            mapToWasteDates(schedule.biowaste, wasteCollectionLocation, WasteType.BIOWASTE),
            mapToWasteDates(schedule.yellowBag, wasteCollectionLocation, WasteType.YELLOW_BAG),
            mapToWasteDates(schedule.residualWaste14daily, wasteCollectionLocation, WasteType.RESIDUAL_WASTE_14DAILY),
            mapToWasteDates(schedule.residualWaste4weekly, wasteCollectionLocation, WasteType.RESIDUAL_WASTE_4WEEKLY),
            mapToWasteDates(schedule.diaperWaste, wasteCollectionLocation, WasteType.DIAPER_WASTE)
        ).flatten()

        wasteCollectionLocation.wasteCollectionDates.addAll(wasteDates)
        return wasteCollectionLocation
    }

    private fun mapToWasteDates(dates: List<Instant>?, wasteCollectionLocation: WasteCollectionLocation, type: WasteType): List<WasteCollectionDate> =
        dates?.map { createWasteDate(it, wasteCollectionLocation, type) } ?: emptyList()

    private fun createWasteDate(date: Instant, wasteCollectionLocation: WasteCollectionLocation, type: WasteType): WasteCollectionDate =
        WasteCollectionDate(UUID.randomUUID(), date, type, wasteCollectionLocation)
}
