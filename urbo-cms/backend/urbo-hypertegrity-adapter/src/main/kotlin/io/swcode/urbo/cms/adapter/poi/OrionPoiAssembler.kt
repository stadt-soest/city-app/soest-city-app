package io.swcode.urbo.cms.adapter.poi

import de.hypertegrity.orion.dto.PoiOrionDto
import io.swcode.urbo.cms.domain.model.category.CategoryService
import io.swcode.urbo.cms.domain.model.category.CategoryType
import io.swcode.urbo.cms.domain.model.poi.Poi
import io.swcode.urbo.cms.domain.model.shared.Address
import io.swcode.urbo.cms.domain.model.shared.ContactPoint
import io.swcode.urbo.cms.domain.model.shared.GeoCoordinates
import org.springframework.stereotype.Component
import java.util.UUID

@Component
class OrionPoiAssembler(private val categoryService: CategoryService) {
    fun toEntity(dtos: List<PoiOrionDto>): List<Poi> {
        return dtos.map {
            Poi(
                UUID.randomUUID(),
                true,
                it.id,
                it.name,
                ContactPoint(it.contactPoint.telephone, it.contactPoint.email, it.contactPoint.contactPerson),
                Address(it.address.streetAddress, "59494", "Soest"),
                categoryService.findOrCreateSourceCategories(it.category, CategoryType.POIS),
                GeoCoordinates(it.location.coordinates[1], it.location.coordinates[0]),
                it.dateModified,
                it.additionalInfoURL
            )
        }
    }
}