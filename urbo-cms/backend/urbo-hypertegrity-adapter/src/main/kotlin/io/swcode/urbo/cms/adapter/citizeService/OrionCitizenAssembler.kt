package io.swcode.urbo.cms.adapter.citizeService

import de.hypertegrity.orion.dto.CitizenServiceOrionDto
import io.swcode.urbo.cms.domain.model.citizenService.CitizenService
import java.util.*

object OrionCitizenAssembler {
    fun toEntity(dtos: List<CitizenServiceOrionDto>): List<CitizenService> {
        return dtos.map { dto ->
            CitizenService(
                id = UUID.randomUUID(),
                externalId = dto.id,
                title = dto.title,
                description = dto.description,
                url = dto.url,
            )
        }
    }
}
