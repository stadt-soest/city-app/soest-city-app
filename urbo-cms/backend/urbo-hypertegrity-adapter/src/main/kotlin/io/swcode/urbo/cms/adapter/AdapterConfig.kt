package io.swcode.urbo.cms.adapter

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan
class AdapterConfig