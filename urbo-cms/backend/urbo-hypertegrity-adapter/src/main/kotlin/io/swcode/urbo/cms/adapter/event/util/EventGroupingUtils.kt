package io.swcode.urbo.cms.adapter.event.util

import de.hypertegrity.orion.dto.EventOrionDto
import io.swcode.urbo.cms.domain.model.event.EventService

fun List<EventOrionDto>.groupByRelationships(): List<Set<EventOrionDto>> {
    val eventMap = this.associateBy { it.id }
    val connectionMap = buildConnectionMap(eventMap)

    val groupedEventIds = findGroups(connectionMap)
    return groupedEventIds.map { group -> group.mapNotNull { eventMap[it] }.toSet() }
}

private fun buildConnectionMap(eventMap: Map<String, EventOrionDto>): Map<String, MutableSet<String>> {
    val connectionMap = mutableMapOf<String, MutableSet<String>>()

    eventMap.values.forEach { event ->

        event.subEvent?.forEach { subEventId ->
            addConnection(connectionMap, event.id, subEventId)
        }

        event.superEvent?.let { superEventId ->
            addConnection(connectionMap, event.id, superEventId)
        }

        connectionMap.putIfAbsent(event.id, mutableSetOf(event.id))
    }

    return connectionMap
}

private fun addConnection(connectionMap: MutableMap<String, MutableSet<String>>, eventId: String, relatedId: String) {
    connectionMap.getOrPut(eventId) { mutableSetOf() }.add(relatedId)
    connectionMap.getOrPut(relatedId) { mutableSetOf() }.add(eventId)
}

private fun findGroups(connectionMap: Map<String, MutableSet<String>>): List<Set<String>> {
    val visited = mutableSetOf<String>()
    val groups = mutableListOf<Set<String>>()

    connectionMap.keys.forEach { eventId ->
        if (!visited.contains(eventId)) {
            val group = mutableSetOf<String>()
            depthFirstSearch(eventId, connectionMap, visited, group)
            groups.add(group)
        }
    }

    return groups
}

private fun depthFirstSearch(
    currentId: String,
    connectionMap: Map<String, Set<String>>,
    visited: MutableSet<String>,
    group: MutableSet<String>
) {
    if (visited.add(currentId)) {
        group.add(currentId)
        connectionMap[currentId]?.forEach { neighborId ->
            depthFirstSearch(neighborId, connectionMap, visited, group)
        }
    }
}

fun List<EventOrionDto>.filterValidEvents(): List<EventOrionDto> {
    return this.filter { event ->
        val startDate = event.startDate
        val endDate = event.endDate
        !endDate.isBefore(startDate) && !event.deleted
    }
}

fun List<EventOrionDto>.handleDeletedEvents(eventService: EventService): List<EventOrionDto> {
    val deletedEvents = this.filter { it.deleted }
    deletedEvents.forEach { eventService.deleteOccurrence(it.id) }
    return this.filterNot { it.deleted }
}