package io.swcode.urbo.cms.adapter.news

import de.hypertegrity.orion.dto.NewsOrionDto
import io.swcode.urbo.cms.domain.model.category.CategoryService
import io.swcode.urbo.cms.domain.model.category.CategoryType
import io.swcode.urbo.cms.domain.model.news.News
import io.swcode.urbo.cms.domain.model.shared.Image
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.*

@Component
class OrionNewsAssembler(
    private val categoryService: CategoryService
) {
    fun toEntityOrNull(newsOrionDto: NewsOrionDto): News? {
        return newsOrionDto.takeIf { it.isValidForNews() }?.let { validNewsDto ->
            News(
                id = UUID.randomUUID(),
                externalId = validNewsDto.id,
                title = validNewsDto.title ?: "",
                subtitle = validNewsDto.subtitle,
                date = validNewsDto.date ?: Instant.now(),
                content = validNewsDto.content ?: "",
                link = validNewsDto.link.takeIf { !it.isNullOrEmpty() },
                authors = validNewsDto.authors?.map { it.name }?.toMutableList(),
                sourceCategories = categoryService.findOrCreateSourceCategories(
                    validNewsDto.categories?.map { it.name } ?: emptyList(),
                    CategoryType.NEWS
                ).toMutableList(),
                modified = validNewsDto.modified,
                image = validNewsDto.image?.url?.let {
                    Image(
                        source = it,
                        copyrightHolder = validNewsDto.image?.copyright,
                        description = validNewsDto.image?.description
                    )
                },
                source = validNewsDto.source,
                highlight = false
            )
        }
    }

    private fun NewsOrionDto.isValidForNews(): Boolean {
        return !title.isNullOrEmpty() && !content.isNullOrEmpty() && date != null
    }
}
