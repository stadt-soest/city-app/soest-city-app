package io.swcode.urbo.cms.adapter.citizeService

import de.hypertegrity.orion.client.CitizenServiceClient
import io.swcode.urbo.cms.application.citizenService.CitizenAdapter
import io.swcode.urbo.cms.domain.model.citizenService.CitizenService
import org.springframework.stereotype.Component

@Component
class HypertegrityCitizenAdapter(
    private val citizenServiceClient: CitizenServiceClient,
) : CitizenAdapter {
    override fun getCitizenServices(): List<CitizenService> {
        return OrionCitizenAssembler.toEntity(citizenServiceClient.getAllCitizenServices())
    }
}
