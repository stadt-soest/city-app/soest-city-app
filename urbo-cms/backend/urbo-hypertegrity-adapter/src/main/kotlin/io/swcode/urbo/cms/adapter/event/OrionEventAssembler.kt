package io.swcode.urbo.cms.adapter.event

import de.hypertegrity.orion.dto.EventOrionDto
import io.swcode.urbo.cms.adapter.event.util.*
import io.swcode.urbo.cms.domain.model.event.*
import io.swcode.urbo.cms.domain.model.category.CategoryService
import io.swcode.urbo.cms.domain.model.category.CategoryType
import org.springframework.stereotype.Component

@Component
class OrionEventAssembler(
    private val categoryService: CategoryService,
    private val eventService: EventService,
) {

    fun toEntity(orionEvents: List<EventOrionDto>): List<Event> {
        return orionEvents
            .handleDeletedEvents(eventService)
            .filterValidEvents()
            .groupByRelationships()
            .map { group -> createEventFromGroup(group) }
    }

    private fun createEventFromGroup(group: Set<EventOrionDto>): Event {
        val uniqueCategories = group.flatMap { it.category }.distinct()
        val categories = categoryService.findOrCreateSourceCategories(uniqueCategories, CategoryType.EVENTS)
        return group.toEventWithOccurrences(categories)
    }
}

