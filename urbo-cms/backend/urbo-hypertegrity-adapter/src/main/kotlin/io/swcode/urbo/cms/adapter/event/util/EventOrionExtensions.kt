package io.swcode.urbo.cms.adapter.event.util

import de.hypertegrity.orion.dto.EventImageOrionDto
import de.hypertegrity.orion.dto.EventOrionDto
import de.hypertegrity.orion.dto.OfferOrionDto
import io.swcode.urbo.cms.domain.model.category.SourceCategory
import io.swcode.urbo.cms.domain.model.event.*
import io.swcode.urbo.cms.domain.model.shared.GeoCoordinates
import io.swcode.urbo.cms.domain.model.shared.Image
import io.swcode.urbo.common.date.toBerlinInstant
import java.util.*

fun EventImageOrionDto.toImage() = this.contentUrl?.let { url ->
    Image(
        url,
        description = this.description,
        copyrightHolder = this.copyrightHolder
    )
}

fun EventOrionDto.toEventLocation() = EventLocation(
    locationName = this.areaServed,
    streetAddress = this.address?.streetAddress,
    postalCode = this.address?.postalCode,
    city = this.address?.addressLocality,
    coordinates = this.location?.coordinates?.let { coords ->
        coords.takeIf { it.size >= 2 }?.let {
            val lat = it[0]
            val long = it[1]
            if (lat != null && long != null) GeoCoordinates(lat, long) else null
        }
    }
)

fun EventOrionDto.toEventContactPoint() = EventContactPoint(
    telephone = this.contactPoint.telephone,
    email = this.contactPoint.email
)

fun OfferOrionDto.toEventOffer() = EventOffer(
    url = this.url,
    price = this.price,
    availability = this.availability,
    description = this.description
)

fun EventOrionDto.toOccurrence(event: Event) = EventOccurrence(
    id = UUID.randomUUID(),
    externalId = this.id,
    status = this.eventStatus.toEventStatus(),
    startAt = this.startDate,
    endAt = this.endDate,
    dateCreated = this.dateCreated.toBerlinInstant(),
    source = this.source,
    event = event
)

fun String.toEventStatus(): EventStatus {
    return when (this.uppercase()) {
        "SCHEDULED" -> EventStatus.SCHEDULED
        "MOVEDONLINE" -> EventStatus.MOVED_ONLINE
        "RESCHEDULED" -> EventStatus.RESCHEDULED
        "CANCELLED" -> EventStatus.CANCELLED
        "POSTPONED" -> EventStatus.POSTPONED
        else -> EventStatus.SCHEDULED
    }
}

fun EventOrionDto.toEvent(categories: List<SourceCategory>): Event {
    return Event(
        id = UUID.randomUUID(),
        title = this.name,
        subTitle = this.alternateName,
        description = this.description,
        image = this.image.toImage(),
        location = this.toEventLocation(),
        contactPoint = this.toEventContactPoint(),
        offer = this.offers?.toEventOffer(),
        sourceCategories = categories.toMutableList(),
        occurrences = mutableListOf()
    )
}

fun Set<EventOrionDto>.toEventWithOccurrences(categories: List<SourceCategory>): Event {
    val representativeDto = this.first()
    return representativeDto.toEvent(categories).apply {
        this@toEventWithOccurrences.forEach { dto ->
            occurrences.add(dto.toOccurrence(this))
        }
    }
}
