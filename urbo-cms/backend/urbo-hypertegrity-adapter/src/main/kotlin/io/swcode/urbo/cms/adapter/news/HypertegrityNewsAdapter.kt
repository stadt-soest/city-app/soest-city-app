package io.swcode.urbo.cms.adapter.news

import de.hypertegrity.orion.client.NewsClient
import io.swcode.urbo.cms.application.news.NewsAdapter
import io.swcode.urbo.cms.domain.model.news.News
import org.springframework.stereotype.Component

@Component
class HypertegrityNewsAdapter(
    private val newsClient: NewsClient,
    private val orionNewsAssembler: OrionNewsAssembler,
) : NewsAdapter {
    override fun getNews(): List<News> {
        return newsClient.getAllNews().mapNotNull { orionNewsAssembler.toEntityOrNull(it) }
    }
}
