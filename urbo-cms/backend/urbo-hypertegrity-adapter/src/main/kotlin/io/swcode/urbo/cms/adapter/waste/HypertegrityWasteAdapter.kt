package io.swcode.urbo.cms.adapter.waste

import de.hypertegrity.orion.client.WasteClient
import io.swcode.urbo.cms.application.waste.WasteAdapter
import io.swcode.urbo.cms.domain.model.waste.WasteCollectionLocation
import org.springframework.stereotype.Component

@Component
class HypertegrityWasteAdapter(
    private val wasteClient: WasteClient,
) : WasteAdapter {
    override fun getWasteCollections(): List<WasteCollectionLocation> {
        return OrionWasteAssembler.toEntity(wasteClient.getAllWastes())
    }
}
