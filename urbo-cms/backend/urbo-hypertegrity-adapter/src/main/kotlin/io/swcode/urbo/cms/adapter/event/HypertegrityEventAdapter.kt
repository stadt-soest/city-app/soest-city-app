package io.swcode.urbo.cms.adapter.event

import de.hypertegrity.orion.client.EventClient
import io.swcode.urbo.cms.application.event.EventAdapter
import io.swcode.urbo.cms.domain.model.event.Event
import org.springframework.stereotype.Component

@Component
class HypertegrityEventAdapter(
    private val eventClient: EventClient,
    private val orionEventAssembler: OrionEventAssembler
) : EventAdapter {
    override fun getEvents(): List<Event> {
        return orionEventAssembler.toEntity(eventClient.getAllEvents())
    }
}
