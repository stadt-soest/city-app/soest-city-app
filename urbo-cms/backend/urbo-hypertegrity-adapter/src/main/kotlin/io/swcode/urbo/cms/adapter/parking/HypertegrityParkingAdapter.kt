package io.swcode.urbo.cms.adapter.parking

import de.hypertegrity.orion.client.OffStreetParkingClient
import de.hypertegrity.orion.dto.OffStreetParkingOrionDto
import de.hypertegrity.orion.dto.OpeningHoursSpecificationOrionDto
import de.hypertegrity.orion.dto.OrionParkingType
import io.swcode.urbo.cms.application.parking.ParkingAdapter
import io.swcode.urbo.cms.domain.model.parking.*
import io.swcode.urbo.cms.domain.model.shared.Address
import io.swcode.urbo.cms.domain.model.shared.GeoCoordinates
import io.swcode.urbo.cms.domain.model.shared.Language
import io.swcode.urbo.cms.domain.model.shared.Localization
import io.swcode.urbo.common.date.toBerlinInstant
import org.springframework.stereotype.Component
import java.time.DayOfWeek
import java.time.LocalTime
import java.util.*

@Component
class HypertegrityParkingAdapter(
    private val parkingClient: OffStreetParkingClient,
) : ParkingAdapter {
    override fun getParkingAreas(): List<ParkingArea> {
        return toEntity(parkingClient.getAllParkingAreas())
    }
}

internal fun toEntity(parkingAreaDtos: List<OffStreetParkingOrionDto>): List<ParkingArea> {
    return parkingAreaDtos.map {
        ParkingArea(
            id = UUID.randomUUID(),
            externalId = it.id,
            name = it.name,
            location = ParkingLocation(
                address = Address(
                    street = it.address.streetAddress + " " + it.address.streetNr,
                    postalCode = it.address.postalCode,
                    city = it.address.addressLocality
                ),
                coordinates = GeoCoordinates(
                    longitude = it.location.coordinates[0],
                    latitude = it.location.coordinates[1]
                ),
            ),
            capacity = ParkingCapacity(
                total = it.totalSpotNumber,
                remaining = it.availableSpotNumber,
                modifiedAt = it.occupancyModified.toBerlinInstant(),
            ),
            openingTimes = mapOpeningHours(it.openingHoursSpecification),
            status = it.status[0].toString().let { status ->
                when (status) {
                    "open" -> ParkingStatus.OPEN
                    "closed" -> ParkingStatus.CLOSED
                    "disturbance" -> ParkingStatus.DISTURBANCE
                    else -> throw IllegalArgumentException("Unknown status: $status")
                }
            },
            maximumAllowedHeight = it.maximumAllowedHeight,
            displayName = it.displayName,
            parkingType = when (it.parkingType) {
                OrionParkingType.GARAGE -> ParkingType.GARAGE
                OrionParkingType.PARKING_HOUSE -> ParkingType.PARKING_HOUSE
                OrionParkingType.UNDERGROUND -> ParkingType.UNDERGROUND
                OrionParkingType.PARKING_SPOT -> ParkingType.PARKING_SPOT
            },
            description = it.description?.let { description ->
                mutableListOf(
                    Localization(description.de, Language.DE),
                    Localization(description.en, Language.EN)
                )
            } ?: mutableListOf(),
            priceDescription = mutableListOf(
                Localization(it.priceDescription.de, Language.DE),
                Localization(it.priceDescription.en, Language.EN)
            )
        )
    }
}

private fun mapOpeningHours(openingHoursSpecifications: List<OpeningHoursSpecificationOrionDto>): Map<DayOfWeek, ParkingOpeningHours> {
    val openingTimes = mutableMapOf<DayOfWeek, ParkingOpeningHours>()

    openingHoursSpecifications.forEach { hours ->
        hours.dayOfWeek.map { day ->
            when (day) {
                "Mo" -> DayOfWeek.MONDAY
                "Tu" -> DayOfWeek.TUESDAY
                "We" -> DayOfWeek.WEDNESDAY
                "Th" -> DayOfWeek.THURSDAY
                "Fr" -> DayOfWeek.FRIDAY
                "Sa" -> DayOfWeek.SATURDAY
                "Su" -> DayOfWeek.SUNDAY
                else -> throw IllegalArgumentException("Invalid day of week: $day")
            }.also { dayOfWeek ->
                openingTimes[dayOfWeek] = ParkingOpeningHours(LocalTime.parse(hours.opens), LocalTime.parse(hours.closes))
            }
        }
    }

    return openingTimes
}


