package io.swcode.urbo.cms.adapter.poi

import de.hypertegrity.orion.client.PoiClient
import io.swcode.urbo.cms.application.poi.PoiAdapter
import io.swcode.urbo.cms.domain.model.poi.Poi
import org.springframework.stereotype.Component

@Component
class HypertegrityPoiAdapter(private val poiClient: PoiClient, private val assembler: OrionPoiAssembler) :
    PoiAdapter {
    override fun getPois(): List<Poi> {
        return poiClient.getAllPois().let {
            assembler.toEntity(it)
        }
    }
}