package util

import de.hypertegrity.orion.dto.*
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*

fun mockEventOrionDto(): EventOrionDto {
    return EventOrionDto(
        id = UUID.randomUUID().toString(),
        areaServed = "Test Area",
        superEvent = null,
        subEvent = listOf("subEvent1", "subEvent2"),
        name = "Test Event",
        alternateName = "Test Alternate Name",
        description = "Test Description",
        source = "https://event.url",
        location = LocationOrionDto(listOf(1.0, 2.0)),
        address = AddressOrionDto("Test Street Address", "Test Locality", "Test Region", "12345"),
        category = listOf("category1", "category2"),
        eventStatus = "scheduled",
        startDate = Instant.now(),
        endDate = Instant.now().plus(1, ChronoUnit.DAYS),
        contactPoint = ContactPointOrionDto("123456789", "email@example.com"),
        image = EventImageOrionDto("https://image.url", "Image Description", "Image Copyright"),
        offers = OfferOrionDto("https://offer.url", "Offer Description", "100", "In Stock"),
        dateCreated = Instant.now(),
        dateModified = Instant.now().plus(1, ChronoUnit.DAYS),
        deleted = false
    )
}



