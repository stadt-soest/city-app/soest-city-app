package io.swcode.urbo.cms.adapter.event.util

import io.kotest.matchers.shouldBe
import io.swcode.urbo.cms.domain.model.event.EventStatus
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

class EventStatusTest {

    @Test
    fun `test toEventStatus with valid statuses`() {
        val testCases = mapOf(
            "scheduled" to EventStatus.SCHEDULED,
            "SCHEDULED" to EventStatus.SCHEDULED,
            "movedOnline" to EventStatus.MOVED_ONLINE,
            "MOVEDONLINE" to EventStatus.MOVED_ONLINE,
            "rescheduled" to EventStatus.RESCHEDULED,
            "RESCHEDULED" to EventStatus.RESCHEDULED,
            "cancelled" to EventStatus.CANCELLED,
            "CANCELLED" to EventStatus.CANCELLED,
            "postponed" to EventStatus.POSTPONED,
            "POSTPONED" to EventStatus.POSTPONED
        )

        testCases.forEach { (input, expected) ->
            input.toEventStatus() shouldBe expected
        }
    }

    @ParameterizedTest
    @ValueSource(strings = ["unknown", "invalid", "123", "status!", "", "   "])
    fun `test toEventStatus with invalid inputs throws exception`(input: String) {
        input.toEventStatus() shouldBe EventStatus.SCHEDULED
    }
}