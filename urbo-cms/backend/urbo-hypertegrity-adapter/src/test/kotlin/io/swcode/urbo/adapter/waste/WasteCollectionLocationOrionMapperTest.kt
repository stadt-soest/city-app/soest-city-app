package io.swcode.urbo.adapter.waste

import de.hypertegrity.orion.dto.WasteAddress
import de.hypertegrity.orion.dto.WasteCollectionOrionDto
import de.hypertegrity.orion.dto.WasteSchedule
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.swcode.urbo.cms.adapter.waste.OrionWasteAssembler
import io.swcode.urbo.cms.domain.model.waste.WasteType
import org.junit.jupiter.api.Test
import java.time.Instant

class WasteCollectionLocationOrionMapperTest {

    @Test
    fun `should map Orion WasteCollection Dto to WasteCollection`() {

        val orionWasteCollection = WasteCollectionOrionDto(
            id = "id",
            address = WasteAddress(
                streetAddress = "street",
                addressLocality = "city"
            ),
            schedule = WasteSchedule(
                biowaste = listOf(Instant.MIN),
                yellowBag = listOf(Instant.MIN),
                wastepaper = listOf(Instant.MIN),
                residualWaste14daily = listOf(Instant.MIN),
                residualWaste4weekly = listOf(Instant.MIN),
                diaperWaste = listOf(Instant.MIN),
            ),
        )

        val orionWaste = OrionWasteAssembler.toEntity(listOf(orionWasteCollection))

        orionWaste.size.shouldNotBeNull()

        orionWaste[0].should {
            it.wasteId shouldBe "id"
            it.street shouldBe "street"
            it.city shouldBe "city"
            it.wasteCollectionDates.size shouldBe 6
            it.wasteCollectionDates.any { wasteDate ->
                wasteDate.date == Instant.MIN && wasteDate.wasteType == WasteType.WASTEPAPER
            } shouldBe true
        }
    }
}