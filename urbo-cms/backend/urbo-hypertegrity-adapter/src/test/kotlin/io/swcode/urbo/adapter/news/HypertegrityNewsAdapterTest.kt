package io.swcode.urbo.adapter.news

import de.hypertegrity.orion.client.NewsClient
import de.hypertegrity.orion.dto.AuthorOrionDto
import de.hypertegrity.orion.dto.FeaturedMediaOrionDto
import de.hypertegrity.orion.dto.NewsOrionDto
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.swcode.urbo.cms.adapter.news.HypertegrityNewsAdapter
import io.swcode.urbo.cms.adapter.news.OrionNewsAssembler
import io.swcode.urbo.cms.application.news.NewsAdapter
import io.swcode.urbo.cms.domain.model.category.CategoryService
import io.swcode.urbo.cms.domain.model.category.CategoryType
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Instant

class HypertegrityNewsAdapterTest {

    private lateinit var newsClient: NewsClient

    private lateinit var newsAdapter: NewsAdapter

    private val categoryService = mockk<CategoryService>()

    @BeforeEach
    fun setup() {
        newsClient = mockk<NewsClient>()
        newsAdapter = HypertegrityNewsAdapter(
            newsClient,
            orionNewsAssembler = OrionNewsAssembler(categoryService),
        )

        every { categoryService.findOrCreateSourceCategories(listOf(), CategoryType.NEWS) } returns listOf()
    }

    @Test
    fun `getNews should return a list of News`() {
        val news = NewsOrionDto(
            id = "kreis-soest-1131858",
            authors =
            mutableListOf(
                AuthorOrionDto(
                    name = "pressestelle@kreis-soest.de"
                )
            ),
            categories = mutableListOf(),
            content = "Auf Auslandsreisen ist es insbesondere für chronisch kranke Menschen wichtig, eine kleine Reiseapotheke mitzunehmen, damit man vorbereitet ist und nicht erst vor Ort Medikamente besorgen muss. Das Gesundheitsamt weist darauf hin, dass Ärzte und Apotheken gerne bei der Zusammenstellung der persönlichen Reiseapotheke helfen. Eine Checkliste ist auch auf der Homepage unter www.kreis-soest.de/medikamente-auf-reisen abrufbar.",
            date = Instant.now(),
            link = "https://www.presse-service.de/meldung.aspx?ID\\u003D1131858",
            image = FeaturedMediaOrionDto(
                url = "url",
                copyright = "copyright",
                description = "description"
            ),
            title = "Reiseapotheke für den Urlaub erstellen - Kreisgesundheitsamt, Ärzte und Apotheken helfen",
            modified = Instant.now(),
            source = "Stadt",
            subtitle = "sub title"
        )

        val newsFromClient = listOf(news)
        every {
            newsClient.getAllNews()
        } returns newsFromClient

        val newsFromAdapter = newsAdapter.getNews()

        newsFromClient.size.shouldBe(newsFromAdapter.size)
    }
}
