package io.swcode.urbo.adapter.citizenservice

import de.hypertegrity.orion.client.CitizenServiceClient
import de.hypertegrity.orion.dto.CitizenServiceOrionDto
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.swcode.urbo.cms.adapter.citizeService.HypertegrityCitizenAdapter
import io.swcode.urbo.cms.application.citizenService.CitizenAdapter
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test


class HypertegrityCitizenAdapterTest {

    private lateinit var citizenServiceClient: CitizenServiceClient

    private lateinit var citizenAdapter: CitizenAdapter

    @BeforeEach
    fun setup() {
        citizenServiceClient = mockk<CitizenServiceClient>()
        citizenAdapter = HypertegrityCitizenAdapter(citizenServiceClient)
    }

    @Test
    fun `getCitizenServices should return a list of CitizenService`() {

        val citizenService = CitizenServiceOrionDto(
            id = "e51b22e8-40e6-4f7b-86e2-4e66eec7d48a",
            description = "Suche im Meldebestand der Stadt Soest. Bitte achten Sie auf die richtige Schreibweise der gesuchten Meldedaten. Eine durchgeführte Abfrage kostet in jedem Fall, egal ob ein Trefferergebnis bei der Suche erzielt wird oder nicht, eine Gebühr von 11 EUR.",
            title = "Einfache Melderegisterauskunft",
            url = "https://portal.soest.de/einfache-melderegisterauskunft"
        )

        val citizenServicesFromClient = listOf(citizenService)
        every {
            citizenServiceClient.getAllCitizenServices()
        } returns citizenServicesFromClient

        val citizenServices = citizenAdapter.getCitizenServices()

        citizenServicesFromClient.should {
            it.size shouldBe citizenServices.size
        }
    }
}
