package io.swcode.urbo.adapter.news

import de.hypertegrity.orion.dto.AuthorOrionDto
import de.hypertegrity.orion.dto.FeaturedMediaOrionDto
import de.hypertegrity.orion.dto.NewsCategoryOrionDto
import de.hypertegrity.orion.dto.NewsOrionDto
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.swcode.urbo.cms.adapter.news.OrionNewsAssembler
import io.swcode.urbo.cms.domain.model.category.SourceCategory
import io.swcode.urbo.cms.domain.model.category.CategoryService
import io.swcode.urbo.cms.domain.model.category.CategoryType
import org.junit.jupiter.api.Test
import java.time.Instant

class OrionNewsAssemblerTest {
    private val categoryService = mockk<CategoryService>()
    private val orionNewsAssembler = OrionNewsAssembler(categoryService)

    @Test
    fun `should map Orion News Dto to News`() {
        val sourceCategory = SourceCategory("category", type = CategoryType.NEWS)

        every { categoryService.findOrCreateSourceCategories(listOf(sourceCategory.name), CategoryType.NEWS) } returns listOf(sourceCategory)

        val orionNews = NewsOrionDto(
            id = "id",
            authors = listOf(AuthorOrionDto(name = "pressestelle@kreis-soest.de")),
            categories = mutableListOf(NewsCategoryOrionDto(name = "category")),
            content = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam",
            date = Instant.now(),
            image = FeaturedMediaOrionDto(url = "https://www.presse-service.de/data.aspx/medien/191899P.jpg", "", ""),
            link = "https://www.soest.de/news/news-detail/sommergruss-des-buergermeister",
            modified = Instant.now(),
            title = "Sommergruß des Bürgermeister",
            source = "Stadt",
            subtitle = "sub title"
        )

        val news = orionNewsAssembler.toEntityOrNull(orionNews)

        news.should {
            it!!.externalId shouldBe "id"
            it.title shouldBe "Sommergruß des Bürgermeister"
            it.subtitle shouldBe "sub title"
            it.content shouldBe "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam"
            it.link shouldBe "https://www.soest.de/news/news-detail/sommergruss-des-buergermeister"
            it.authors!![0] shouldBe "pressestelle@kreis-soest.de"
            it.date shouldBe orionNews.date
            it.modified shouldBe orionNews.modified
            it.image!!.source shouldBe "https://www.presse-service.de/data.aspx/medien/191899P.jpg"
            it.image!!.highRes shouldBe null
            it.image!!.thumbnail shouldBe null
            it.image!!.copyrightHolder shouldBe ""
            it.image!!.description shouldBe ""
            it.source shouldBe "Stadt"
            it.sourceCategories.stream().findFirst().get().name shouldBe "category"
        }
    }
}
