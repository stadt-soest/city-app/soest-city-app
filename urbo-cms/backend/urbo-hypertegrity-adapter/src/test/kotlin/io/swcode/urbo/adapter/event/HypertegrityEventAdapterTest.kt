package io.swcode.urbo.adapter.event

import de.hypertegrity.orion.client.EventClient
import io.kotest.matchers.equals.shouldBeEqual
import io.mockk.every
import io.mockk.mockk
import io.swcode.urbo.cms.adapter.event.HypertegrityEventAdapter
import io.swcode.urbo.cms.adapter.event.OrionEventAssembler
import io.swcode.urbo.cms.application.event.EventAdapter
import io.swcode.urbo.cms.domain.model.event.Event
import org.junit.jupiter.api.Test
import util.mockEventOrionDto

class HypertegrityEventAdapterTest {

    private val eventClient: EventClient = mockk<EventClient>()
    private val orionEventAssembler = mockk<OrionEventAssembler>()


    private val eventAdapter: EventAdapter = HypertegrityEventAdapter(eventClient, orionEventAssembler)

    @Test
    fun `should return a list of Events`() {
        val eventDto = mockEventOrionDto().copy(
            id = "dfx-15013",
            name = "KAISERREIHE: Hans Kaiser im Dialog with Liat Yossifor"
        )

        val eventResponseFromClient = listOf(eventDto)

        val events = mockk<List<Event>>()

        every {
            eventClient.getAllEvents()
        } returns eventResponseFromClient

        every {
            orionEventAssembler.toEntity(eventResponseFromClient)
        } returns events

        val actual = eventAdapter.getEvents()

        actual shouldBeEqual events
    }
}
