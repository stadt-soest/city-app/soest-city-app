package io.swcode.urbo.cms.adapter.poi

import de.hypertegrity.orion.dto.ContactPoint
import de.hypertegrity.orion.dto.PoiAddress
import de.hypertegrity.orion.dto.PoiLocation
import de.hypertegrity.orion.dto.PoiOrionDto
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.swcode.urbo.cms.domain.model.category.CategoryService
import io.swcode.urbo.cms.domain.model.category.CategoryType
import io.swcode.urbo.cms.domain.model.category.SourceCategory
import org.junit.jupiter.api.Test
import java.time.Instant

class OrionPoiAssemblerTest {
    private val categoryServiceMock = mockk<CategoryService>()
    private val sut = OrionPoiAssembler(categoryServiceMock)

    @Test
    fun `should map dtos to entities`() {
        every { categoryServiceMock.findOrCreateSourceCategories(any(), CategoryType.POIS) } returns listOf(
            SourceCategory("Gericht", CategoryType.POIS)
        )


        val dtos = listOf(
            PoiOrionDto(
                "42",
                "Amtsgericht",
                PoiAddress("Nöttenstraße 28"),
                listOf("Gericht"),
                PoiLocation(
                    listOf(8.100795626541759, 51.570983009120525)
                ),
                ContactPoint("02921/398-0", "poststelle@ag-soest.nrw.de", "Der Direktor des Amtsgerichts"),
                Instant.parse("2017-07-21T17:32:28Z"),
                "http://www.ag-soest.nrw.de/"
            )
        )

        val actual = sut.toEntity(dtos)

        actual.shouldHaveSize(1)
        actual[0].apply {
            externalId.shouldBe("42")
            name.shouldBe("Amtsgericht")
            address.city.shouldBe("Soest")
            address.postalCode.shouldBe("59494")
            address.street.shouldBe("Nöttenstraße 28")
            sourceCategories.shouldHaveSize(1)
            sourceCategories[0].name.shouldBe("Gericht")
            sourceCategories[0].type.shouldBe(CategoryType.POIS)
            coordinates.longitude.shouldBe(8.100795626541759)
            coordinates.latitude.shouldBe(51.570983009120525)
            dateModified.shouldBe(Instant.parse("2017-07-21T17:32:28Z"))
            url.shouldBe("http://www.ag-soest.nrw.de/")
        }
    }
}