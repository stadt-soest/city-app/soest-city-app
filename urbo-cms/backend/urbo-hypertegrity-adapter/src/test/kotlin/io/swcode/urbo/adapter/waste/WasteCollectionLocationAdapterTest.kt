package io.swcode.urbo.adapter.waste

import de.hypertegrity.orion.client.WasteClient
import de.hypertegrity.orion.dto.WasteAddress
import de.hypertegrity.orion.dto.WasteCollectionOrionDto
import de.hypertegrity.orion.dto.WasteSchedule
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.swcode.urbo.cms.adapter.waste.HypertegrityWasteAdapter
import io.swcode.urbo.cms.application.waste.WasteAdapter
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class WasteCollectionLocationAdapterTest {

    private lateinit var wasteClient: WasteClient

    private lateinit var wasteAdapter: WasteAdapter

    @BeforeEach
    fun setup() {
        wasteClient = mockk<WasteClient>()
        wasteAdapter = HypertegrityWasteAdapter(
            wasteClient,
        )
    }

    @Test
    fun `getNews should return a list of WasteCollection`() {
        val wasteCollectionOrionDto = WasteCollectionOrionDto(
            id = "id",
            address = WasteAddress(
                streetAddress = "street",
                addressLocality = "city"
            ),
            schedule = WasteSchedule(
                biowaste = emptyList(),
                diaperWaste = emptyList(),
                residualWaste4weekly = emptyList(),
                residualWaste14daily = emptyList(),
                wastepaper = emptyList(),
                yellowBag = emptyList()
            ),
        )

        val newsFromClient = listOf(wasteCollectionOrionDto)
        every {
            wasteClient.getAllWastes()
        } returns newsFromClient

        val newsFromAdapter = wasteAdapter.getWasteCollections()

        newsFromClient.size.shouldBe(newsFromAdapter.size)
    }
}
