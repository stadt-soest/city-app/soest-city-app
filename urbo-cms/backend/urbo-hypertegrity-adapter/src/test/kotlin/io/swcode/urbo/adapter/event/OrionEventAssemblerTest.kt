package io.swcode.urbo.adapter.event

import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.collections.shouldContainExactlyInAnyOrder
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.swcode.urbo.cms.adapter.event.OrionEventAssembler
import io.swcode.urbo.cms.domain.model.category.CategoryService
import io.swcode.urbo.cms.domain.model.category.CategoryType
import io.swcode.urbo.cms.domain.model.category.SourceCategory
import io.swcode.urbo.cms.domain.model.event.EventService
import io.swcode.urbo.cms.domain.model.event.EventStatus
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import util.mockEventOrionDto
import java.time.Instant

class OrionEventAssemblerTest {

    private val categoryServiceMock = mockk<CategoryService>()
    private val eventServiceMock = mockk<EventService>()
    private val sut = OrionEventAssembler(categoryServiceMock, eventServiceMock)

    private val sourceCategories = listOf(SourceCategory("category-1", CategoryType.EVENTS))

    @BeforeEach
    fun setup() {
        every { categoryServiceMock.findOrCreateSourceCategories(any(), CategoryType.EVENTS) }.returns(sourceCategories)
        every { eventServiceMock.deleteOccurrence(any())}.returns(Unit)
    }

    @Test
    fun `should correctly convert EventOrionDto to Event`() {
        val eventOrionDto = mockEventOrionDto()
        val events = sut.toEntity(listOf(eventOrionDto))

        val event = events.first()
        event.title shouldBe eventOrionDto.name
        event.subTitle shouldBe eventOrionDto.alternateName
        event.description shouldBe eventOrionDto.description
        event.location.streetAddress shouldBe eventOrionDto.address?.streetAddress
        event.location.city shouldBe eventOrionDto.address?.addressLocality
        event.location.postalCode shouldBe eventOrionDto.address?.postalCode
        event.location.coordinates!!.latitude shouldBe eventOrionDto.location!!.coordinates?.first()
        event.location.coordinates!!.longitude shouldBe eventOrionDto.location!!.coordinates?.last()
        event.contactPoint!!.telephone shouldBe eventOrionDto.contactPoint.telephone
        event.contactPoint!!.email shouldBe eventOrionDto.contactPoint.email
        event.sourceCategories.shouldContainExactly(sourceCategories)
        event.occurrences.forEach { it.status.shouldBe(EventStatus.SCHEDULED) }
    }

    @Test
    fun `should correctly assemble events with complex subEvents and shared occurrences`() {
        val baseEvent1 = mockEventOrionDto().copy(id = "baseEvent1", subEvent = listOf("sharedSubEvent"))
        val baseEvent2 =
            mockEventOrionDto().copy(id = "baseEvent2", subEvent = listOf("sharedSubEvent", "uniqueSubEvent2"))
        val sharedSubEvent = mockEventOrionDto().copy(
            id = "sharedSubEvent",
            superEvent = "baseEvent1",
            subEvent = listOf("nestedSubEvent")
        )
        val uniqueSubEvent2 =
            mockEventOrionDto().copy(id = "uniqueSubEvent2", superEvent = "baseEvent2", subEvent = listOf())
        val nestedSubEvent =
            mockEventOrionDto().copy(id = "nestedSubEvent", superEvent = "sharedSubEvent", subEvent = listOf())

        val events = sut.toEntity(listOf(baseEvent1, baseEvent2, sharedSubEvent, uniqueSubEvent2, nestedSubEvent))

        events.size shouldBe 1
        events.flatMap { it.occurrences }.size shouldBe 5

    }

    @Test
    fun `should include all occurrences of superEvents and subEvents`() {
        val superEvent = mockEventOrionDto().copy(id = "event", subEvent = listOf("subEvent", "subEvent2"))
        val subEvent = mockEventOrionDto().copy(id = "subEvent", superEvent = "event", subEvent = listOf())
        val duplicatedSubEvent = mockEventOrionDto().copy(id = "subEvent2", superEvent = "event", subEvent = listOf())

        val events = sut.toEntity(listOf(superEvent, subEvent, duplicatedSubEvent))

        events.size shouldBe 1
        events[0].occurrences.size shouldBe 3
    }

    @Test
    fun `should handle superEvent without subEvents correctly`() {
        val superEvent = mockEventOrionDto().copy(id = "superEvent", subEvent = emptyList())

        val events = sut.toEntity(listOf(superEvent))

        events.size shouldBe 1
        val event = events.first()
        event.occurrences.size shouldBe 1
        val occurrence = event.occurrences.first()
        occurrence.externalId shouldBe "superEvent"
        occurrence.startAt shouldBe superEvent.startDate
        occurrence.endAt shouldBe superEvent.endDate
    }

    @Test
    fun `complex indirect circular and nested relationships are handled correctly`() {
        val eventA = mockEventOrionDto().copy(id = "EventA", subEvent = listOf("EventB"))
        val eventB = mockEventOrionDto().copy(id = "EventB", superEvent = "EventA", subEvent = listOf("EventC"))
        val eventC = mockEventOrionDto().copy(id = "EventC", superEvent = "EventB", subEvent = listOf())
        val eventD = mockEventOrionDto().copy(id = "EventD", subEvent = listOf("EventE"))
        val eventE = mockEventOrionDto().copy(
            id = "EventE",
            superEvent = "EventD",
            subEvent = listOf("EventA")
        )

        val events = sut.toEntity(listOf(eventA, eventB, eventC, eventD, eventE))

        events.size shouldBe 1
        events.flatMap { it.occurrences }.size shouldBe 5
    }

    @Test
    fun `should correctly group events with indirect relationships`() {
        val eventA = mockEventOrionDto().copy(id = "eventA", subEvent = listOf("eventB"))
        val eventB = mockEventOrionDto().copy(id = "eventB", superEvent = "eventA", subEvent = listOf("eventC"))
        val eventC = mockEventOrionDto().copy(id = "eventC", superEvent = "eventB", subEvent = listOf())

        val events = sut.toEntity(listOf(eventA, eventB, eventC))

        events.size shouldBe 1
        events.first().occurrences.size shouldBe 3
        events.first().occurrences.map { it.externalId }
            .shouldContainExactlyInAnyOrder(listOf("eventA", "eventB", "eventC"))
    }

    @Test
    fun `should handle missing event objects gracefully`() {
        val orionEvents = listOf(
            mockEventOrionDto().copy(
                id = "NewSuperEvent",
                subEvent = listOf("subEvent 2", "subEvent 3")
            ),
            mockEventOrionDto().copy(
                id = "subEvent 3",
                superEvent = "subEvent 2",
                subEvent = listOf()
            ),
        )

        val events = sut.toEntity(orionEvents)

        events.size shouldBe 1
        val event = events.first()
        event.occurrences.size shouldBe 2
        event.occurrences.map { it.externalId } shouldContainExactly listOf("NewSuperEvent", "subEvent 3")
    }

    @Test
    fun `sub-event data is correctly associated with its occurrence`() {
        val baseEvent = mockEventOrionDto().copy(id = "baseEvent")
        val subEvent1 = mockEventOrionDto().copy(id = "subEvent1", superEvent = "baseEvent", name = "Sub Event 1")
        val subEvent2 = mockEventOrionDto().copy(id = "subEvent2", superEvent = "baseEvent", name = "Sub Event 2")
        val superEvent = baseEvent.copy(id = "baseEvent", subEvent = listOf("subEvent1", "subEvent2"))

        val events = sut.toEntity(listOf(superEvent, subEvent1, subEvent2))

        val occurrences = events.first().occurrences
        occurrences.any { it.externalId == "subEvent1" } shouldBe true
        occurrences.any { it.externalId == "subEvent2" } shouldBe true
    }

    @Test
    fun `should correctly assemble events with nested subEvents`() {
        val superEvent = mockEventOrionDto().copy(id = "superEvent", subEvent = listOf("subEvent"))
        val subEvent =
            mockEventOrionDto().copy(id = "subEvent", superEvent = "superEvent", subEvent = listOf("nestedSubEvent"))
        val nestedSubEvent = mockEventOrionDto().copy(id = "nestedSubEvent", superEvent = "subEvent")

        val events = sut.toEntity(listOf(superEvent, subEvent, nestedSubEvent))

        events.size shouldBe 1
        events.first().occurrences.size shouldBe 3
    }

    @Test
    fun `should handle sub-events without start or end dates correctly`() {
        val superEvent =
            mockEventOrionDto().copy(id = "superEvent", startDate = Instant.MIN, endDate = Instant.MIN)
        val subEvent = mockEventOrionDto().copy(
            id = "subEvent",
            superEvent = "superEvent",
            startDate = Instant.MIN,
            endDate = Instant.MIN
        )

        val events = sut.toEntity(listOf(superEvent, subEvent))

        events.size shouldBe 1
        val event = events.first()
        event.occurrences.forEach {
            it.startAt shouldBe Instant.MIN
            it.endAt shouldBe Instant.MIN
        }
    }

    @Test
    fun `should handle sub-events with same superEvent but superEvent not present`() {
        val subEvent = mockEventOrionDto().copy(id = "subEvent", superEvent = "superEvent")
        val subEvent1 = mockEventOrionDto().copy(id = "subEvent1", superEvent = "superEvent")
        val subEvent2 = mockEventOrionDto().copy(id = "subEvent2", superEvent = "superEvent")
        val subEvent3 = mockEventOrionDto().copy(id = "subEvent3", superEvent = "superEvent")

        val events = sut.toEntity(listOf(subEvent, subEvent1, subEvent2, subEvent3))

        events.size shouldBe 1
    }

    @Test
    fun `should handle circular event references gracefully`() {
        val eventA = mockEventOrionDto().copy(id = "eventA", subEvent = listOf("eventB"))
        val eventB = mockEventOrionDto().copy(id = "eventB", subEvent = listOf("eventA"))

        val events = sut.toEntity(listOf(eventA, eventB))

        events.size shouldBe 1
        events.flatMap { it.occurrences }.size shouldBe 2
    }

    @Test
    fun `should correctly group interconnected event clusters`() {
        val eventA = mockEventOrionDto().copy(id = "A", subEvent = listOf("B", "C"))
        val eventB = mockEventOrionDto().copy(id = "B", subEvent = listOf("D"), superEvent = "A")
        val eventC = mockEventOrionDto().copy(id = "C", subEvent = listOf("D"), superEvent = "A")
        val eventD = mockEventOrionDto().copy(id = "D", subEvent = listOf("A"), superEvent = "B")

        val events = sut.toEntity(listOf(eventA, eventB, eventC, eventD))

        events.size shouldBe 1
        events.flatMap { it.occurrences }.size shouldBe 4
    }

    @Test
    fun `should handle shared sub-events across unrelated super-events correctly`() {
        val eventA = mockEventOrionDto().copy(id = "EventA", subEvent = listOf("EventB"), superEvent = null)
        val eventB = mockEventOrionDto().copy(id = "EventB", subEvent = listOf(), superEvent = null)
        val eventC = mockEventOrionDto().copy(id = "EventC", subEvent = listOf("EventB"), superEvent = null)
        val eventD = mockEventOrionDto().copy(id = "EventD", subEvent = listOf(), superEvent = null)

        val events = sut.toEntity(listOf(eventA, eventB, eventC, eventD))

        events.size shouldBe 2
    }
}
