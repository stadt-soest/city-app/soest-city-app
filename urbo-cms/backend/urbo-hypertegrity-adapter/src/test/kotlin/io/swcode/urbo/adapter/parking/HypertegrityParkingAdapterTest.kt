package io.swcode.urbo.adapter.parking

import de.hypertegrity.orion.client.OffStreetParkingClient
import de.hypertegrity.orion.dto.*
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import io.mockk.every
import io.mockk.mockk
import io.swcode.urbo.cms.adapter.parking.HypertegrityParkingAdapter
import io.swcode.urbo.cms.application.parking.ParkingAdapter
import io.swcode.urbo.cms.domain.model.parking.ParkingType
import io.swcode.urbo.cms.domain.model.shared.Language
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.DayOfWeek
import java.time.LocalDateTime
import java.time.LocalTime

class HypertegrityParkingAdapterTest {

    private lateinit var parkingClient: OffStreetParkingClient
    private lateinit var parkingAdapter: ParkingAdapter

    @BeforeEach
    fun setup() {
        parkingClient = mockk()
        parkingAdapter = HypertegrityParkingAdapter(parkingClient)
    }

    @Test
    fun `getParkingAreas should return a list of ParkingAreas`() {
        val parkingAreaDto = OffStreetParkingOrionDto(
            id = "parkingArea1",
            name = "Parking Area 1",
            address = ParkingAddressOrionDto(
                addressCounty = "County",
                addressLocality = "City",
                addressRegion = "Region",
                postalCode = "12345",
                streetAddress = "Street",
                streetNr = "1"
            ),
            allowedVehicleType = listOf(),
            availableSpotNumber = 50,
            occupancyModified = LocalDateTime.now(),
            occupiedSpotNumber = 50,
            status = listOf(ParkingStateOrionDto.closed),
            openingHoursSpecification = listOf(
                OpeningHoursSpecificationOrionDto(
                    dayOfWeek = listOf("Mo", "Tu"),
                    opens = LocalTime.of(8, 0).toString(),
                    closes = LocalTime.of(18, 0).toString()
                )
            ),
            totalSpotNumber = 100,
            unclassifiedSlots = UnclassifiedSlotsOrionDto(
                totalSpotNumber = 100,
                availableSpotNumber = 50,
                occupiedSpotNumber = 50
            ),
            location = ParkingLocationOrionDto(
                coordinates = arrayOf(0.0, 0.0)
            ),
            maximumAllowedHeight = "105m",
            priceDescription = ParkingDescription(
                de = "Preisbeschreibung",
                en = "Price description"
            ),
            description = ParkingDescription(
                de = "Beschreibung",
                en = "Description"
            ),
            parkingType = OrionParkingType.PARKING_HOUSE,
            displayName = "Parking Area 1"
        )

        every { parkingClient.getAllParkingAreas() } returns listOf(parkingAreaDto)

        val parkingAreas = parkingAdapter.getParkingAreas()

        parkingAreas.size shouldBe 1

        with(parkingAreas.first()) {
            id shouldNotBe null
            externalId shouldBe "parkingArea1"
            name shouldBe "Parking Area 1"
            location.address.street shouldBe "Street 1"
            location.address.postalCode shouldBe "12345"
            location.address.city shouldBe "City"
            location.coordinates.latitude shouldBe 0.0
            location.coordinates.longitude shouldBe 0.0
            capacity.total shouldBe 100
            capacity.remaining shouldBe 50
            capacity.modifiedAt shouldNotBe null
            displayName shouldBe "Parking Area 1"
            parkingType shouldBe ParkingType.PARKING_HOUSE
            maximumAllowedHeight shouldBe "105m"
            description.find { it.language == Language.DE }?.value shouldBe "Beschreibung"
            description.find { it.language == Language.EN }?.value shouldBe "Description"
            priceDescription.find { it.language == Language.DE }?.value shouldBe "Preisbeschreibung"
            priceDescription.find { it.language == Language.EN }?.value shouldBe "Price description"

            openingTimes[DayOfWeek.MONDAY]!!.from shouldBe LocalTime.of(8, 0)
            openingTimes[DayOfWeek.MONDAY]!!.to shouldBe LocalTime.of(18, 0)
            openingTimes[DayOfWeek.TUESDAY]!!.from shouldBe LocalTime.of(8, 0)
            openingTimes[DayOfWeek.TUESDAY]!!.to shouldBe LocalTime.of(18, 0)

            openingTimes[DayOfWeek.WEDNESDAY] shouldBe null
            openingTimes[DayOfWeek.THURSDAY] shouldBe null
            openingTimes[DayOfWeek.FRIDAY] shouldBe null
            openingTimes[DayOfWeek.SATURDAY] shouldBe null
            openingTimes[DayOfWeek.SUNDAY] shouldBe null
        }
    }
}
