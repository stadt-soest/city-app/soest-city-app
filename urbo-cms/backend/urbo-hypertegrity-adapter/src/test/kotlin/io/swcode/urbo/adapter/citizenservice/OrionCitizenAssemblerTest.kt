package io.swcode.urbo.adapter.citizenservice

import de.hypertegrity.orion.dto.CitizenServiceOrionDto
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.swcode.urbo.cms.adapter.citizeService.OrionCitizenAssembler
import org.junit.jupiter.api.Test


class OrionCitizenAssemblerTest {

    @Test
    fun `should map Orion CitizenService Dto to CitizenService`() {
        val citizenServiceOrionDto = CitizenServiceOrionDto(
            id = "b2137d62-2aaa-449b-87f0-f688941ea7ba",
            title = "Terminvereinbarung B\u00fcrger B\u00fcro",
            description = "Hier k\u00f6nnen Sie einen Termin f\u00fcr Ihre Anliegen im B\u00fcrger B\u00fcro der Stadt Soest vereinbaren.",
            url = "https://portal.soest.de/externe-services/call/44",
        )

        val citizenServices = OrionCitizenAssembler.toEntity(listOf(citizenServiceOrionDto))
        val citizenService = citizenServices[0]

        citizenService.shouldNotBeNull()
        citizenService.should {
            it.externalId shouldBe "b2137d62-2aaa-449b-87f0-f688941ea7ba"
            it.title shouldBe "Terminvereinbarung Bürger Büro"
            it.url shouldBe "https://portal.soest.de/externe-services/call/44"
        }
    }
}

