package de.hypertegrity.orion.security

import com.github.tomakehurst.wiremock.WireMockServer
import com.nimbusds.oauth2.sdk.ErrorObject
import com.nimbusds.oauth2.sdk.TokenErrorResponse
import com.nimbusds.oauth2.sdk.token.BearerAccessToken
import com.nimbusds.oauth2.sdk.token.RefreshToken
import de.hypertegrity.orion.BaseTest
import de.hypertegrity.orion.client.NewsApi
import de.hypertegrity.orion.feign.interceptor.FeignInterceptor
import feign.RequestTemplate
import feign.RetryableException
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.equals.shouldNotBeEqual
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import io.kotest.matchers.string.beBlank
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.util.ReflectionTestUtils
import testutil.*
import java.time.Instant

class TokenManagerTest : BaseTest() {

    @Autowired
    lateinit var newsApiTest: NewsApi

    @Autowired
    lateinit var wireMockServer: WireMockServer

    @Autowired
    lateinit var tokenManager: TokenManager

    @BeforeEach
    fun setup() {
        wireMockServer.stubSuccessfulTokenResponse(TestUtil.readFixtureResource("/stubs/token/sample-token.json"))
    }

    @Test
    fun `Test fetching token and making requests`() {
        val token = tokenManager.getToken()

        val feignInterceptor = FeignInterceptor(tokenManager)

        val requestTemplate = RequestTemplate()
        feignInterceptor.apply(requestTemplate)
        val authorizationHeader = requestTemplate.headers()["Authorization"]?.first()

        authorizationHeader.should {
            it shouldBe "Bearer ${token.value}"
        }

    }

    @Test
    fun `Test automatic token refresh`() {
        val oldToken = BearerAccessToken("oldToken", 3600, null)

        ReflectionTestUtils.setField(tokenManager, "accessToken", oldToken)
        ReflectionTestUtils.setField(tokenManager, "accessTokenExpiryTime", Instant.now().epochSecond - 60)
        ReflectionTestUtils.setField(tokenManager, "refreshToken", RefreshToken("validRefreshToken"))

        val refreshedToken = tokenManager.getToken()

        refreshedToken shouldNotBeEqual oldToken
    }

    @Test
    fun `Test new token after Invalid Token 401 response`() {

        wireMockServer.stub401InvalidTokenResponseApi()

        shouldThrow<RetryableException> {

            newsApiTest.getNews(
                fiwareService = "soest_dataspace",
                fiwareServicePath = "/news",
                limit = 100,
                offset = 0,
                options = "count"
            )
        }

    }

    @Test
    fun `Test token refresh with invalid refresh token response from Keycloak server`() {
        ReflectionTestUtils.setField(tokenManager, "refreshToken", RefreshToken("InvalidRefreshToken"))

        wireMockServer.stubInvalidRefreshTokenFlow()

        val newToken = tokenManager.getToken()

        newToken.should {
            it shouldNotBe null
            it.value shouldNotBe beBlank()
        }
    }

    @Test
    fun `processResponse with Invalid refresh token error should refresh token`() {

        val errorResponse = TokenErrorResponse(
            ErrorObject(
                "invalid_grant",
                "Invalid refresh token",
                401
            )
        )

        val initialAccessToken = BearerAccessToken("initial access token", 3600, null)
        val initialRefreshToken = RefreshToken("initial refresh token")

        ReflectionTestUtils.setField(tokenManager, "accessToken", initialAccessToken)
        ReflectionTestUtils.setField(tokenManager, "refreshToken", initialRefreshToken)

        tokenManager.processResponse(errorResponse)

        val accessToken = ReflectionTestUtils.getField(tokenManager, "accessToken")
        val refreshToken = ReflectionTestUtils.getField(tokenManager, "refreshToken")

        accessToken.should {
            it shouldNotBe null
            it shouldNotBe initialAccessToken
        }

        refreshToken should {
            it shouldNotBe null
            it shouldNotBe initialRefreshToken
        }
    }

    @Test
    fun `processResponse with Invalid user credentials error should throw Exception`() {

        val errorResponse = TokenErrorResponse(
            ErrorObject(
                "invalid_grant",
                "Invalid user credentials",
                401
            )
        )

        val exception = shouldThrow<Exception> { tokenManager.processResponse(errorResponse) }

        exception.message shouldBe "Invalid user credentials"

    }

    @Test
    fun `processResponse with any other error should throw Exception`() {

        val errorResponse = TokenErrorResponse(
            ErrorObject(
                "invalid_grant",
                "Some other error",
                401
            )
        )

        val exception = shouldThrow<Exception> { tokenManager.processResponse(errorResponse) }

        exception.message shouldBe "Some other error"
    }


    @Test
    fun `Test token refresh due to upcoming expiry`() {
        val almostExpiredToken = BearerAccessToken("almostExpiredToken", 3600, null)
        val gracePeriodInSeconds = 60L

        ReflectionTestUtils.setField(tokenManager, "accessToken", almostExpiredToken)
        ReflectionTestUtils.setField(
            tokenManager,
            "accessTokenExpiryTime",
            Instant.now().epochSecond + gracePeriodInSeconds - 1
        )
        ReflectionTestUtils.setField(tokenManager, "refreshToken", RefreshToken("validRefreshToken"))

        val newToken = tokenManager.getToken()

        newToken shouldNotBeEqual almostExpiredToken
    }

    @Test
    fun `Token not refreshed when expiry is beyond grace period`() {
        val validToken = BearerAccessToken("validToken", 3600, null)
        val gracePeriodInSeconds = 60L

        ReflectionTestUtils.setField(tokenManager, "accessToken", validToken)
        ReflectionTestUtils.setField(
            tokenManager,
            "accessTokenExpiryTime",
            Instant.now().epochSecond + gracePeriodInSeconds + 1
        )
        ReflectionTestUtils.setField(tokenManager, "refreshToken", RefreshToken("validRefreshToken"))

        val fetchedToken = tokenManager.getToken()

        fetchedToken shouldBe validToken
    }

    @Test
    fun `Token refreshed when it's already expired`() {
        val expiredToken = BearerAccessToken("expiredToken", 3600, null)

        ReflectionTestUtils.setField(tokenManager, "accessToken", expiredToken)
        ReflectionTestUtils.setField(
            tokenManager,
            "accessTokenExpiryTime",
            Instant.now().epochSecond - 1
        )
        ReflectionTestUtils.setField(tokenManager, "refreshToken", RefreshToken("validRefreshToken"))

        val newToken = tokenManager.getToken()

        newToken shouldNotBeEqual expiredToken
    }
}
