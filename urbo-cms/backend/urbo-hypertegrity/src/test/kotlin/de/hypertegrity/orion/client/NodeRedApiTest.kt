package de.hypertegrity.orion.client

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.*
import com.github.tomakehurst.wiremock.http.Fault
import de.hypertegrity.orion.BaseTest
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import testutil.TestUtil
import testutil.stubSuccessfulTokenResponse

class NodeRedApiTest : BaseTest() {

    @Autowired
    lateinit var wireMockServer: WireMockServer

    @Autowired
    lateinit var nodeRedApi: NodeRedApi

    @Autowired
    lateinit var nodeRedClient: NodeRedClient

    @BeforeEach
    fun setup() {
        wireMockServer.stubSuccessfulTokenResponse(TestUtil.readFixtureResource("/stubs/token/sample-token.json"))
    }

    @Test
    fun `should return 200 OK and body when Node-RED is reachable`() {
        wireMockServer.stubFor(
            get(urlEqualTo("/"))
                .willReturn(
                    aResponse()
                        .withStatus(200)
                        .withBody("Node-RED is reachable")
                )
        )

        val response = nodeRedApi.getNodeRedStatus()
        response.statusCode.shouldBe(HttpStatus.OK)
        response.body.shouldBe("Node-RED is reachable")

        nodeRedClient.checkStatus().shouldBe(true)
    }

    @Test
    fun `should return false when Node-RED is unreachable (500 or any error)`() {
        wireMockServer.stubFor(
            get(urlEqualTo("/"))
                .willReturn(
                    aResponse()
                        .withStatus(500)
                        .withBody("Internal Server Error")
                )
        )

        nodeRedClient.checkStatus().shouldBe(false)
    }

    @Test
    fun `should return false when Node-RED is unreachable (connection refused)`() {
        wireMockServer.stubFor(
            get(urlEqualTo("/"))
                .willReturn(aResponse().withFault(Fault.CONNECTION_RESET_BY_PEER))
        )

        nodeRedClient.checkStatus().shouldBe(false)
    }
}
