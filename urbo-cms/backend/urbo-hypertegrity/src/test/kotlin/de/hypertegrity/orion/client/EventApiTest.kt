package de.hypertegrity.orion.client

import com.github.tomakehurst.wiremock.WireMockServer
import de.hypertegrity.orion.BaseTest
import io.kotest.matchers.collections.shouldHaveSize
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import testutil.TestUtil
import testutil.stubPaginatedResponseFromFile
import testutil.stubSuccessfulTokenResponse


class EventApiTest : BaseTest() {

    @Autowired
    lateinit var wireMockServer: WireMockServer

    @Autowired
    lateinit var eventApi: EventApi

    @Autowired
    lateinit var eventClient: EventClient

    @BeforeEach
    fun setup() {
        wireMockServer.stubSuccessfulTokenResponse(TestUtil.readFixtureResource("/stubs/token/sample-token.json"))
    }

    @Test
    fun `should fetch events`() {
        wireMockServer.stubPaginatedResponseFromFile("/events", "/stubs/event/sample-events.json")

        val events = eventApi.getEvents(
            fiwareService = "soest_dataspace",
            fiwareServicePath = "/events",
            limit = 100,
            offset = 0,
            options = "count",
        )

        events.body!!.shouldHaveSize(2)
    }

    @Test
    fun `should fetch all events`() {
        wireMockServer.stubPaginatedResponseFromFile("/events", "/stubs/event/sample-events.json")

        val events = eventClient.getAllEvents()

        events.shouldHaveSize(2)
    }
}
