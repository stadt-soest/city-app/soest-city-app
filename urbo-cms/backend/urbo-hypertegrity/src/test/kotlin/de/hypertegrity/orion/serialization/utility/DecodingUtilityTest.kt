package de.hypertegrity.orion.serialization.utility

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

class DecodingUtilityTest {

    @Test
    fun `decodeEscapedCharacters should replace escaped characters correctly`() {
        val input = "\\u003C\\u003E\\u0022\\u0027\\u003D\\u003B\\u0028\\u0029"
        val expectedOutput = "<>\"'=;()"

        val result = DecodingUtility.decodeEscapedCharacters(input)

        assertEquals(expectedOutput, result)
    }

    @Test
    fun `decodeEscapedCharacters should not alter non-escaped strings`() {
        val input = "This is a normal string without escaped characters."

        val result = DecodingUtility.decodeEscapedCharacters(input)

        assertEquals(input, result)
    }


}