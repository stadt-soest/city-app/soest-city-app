package de.hypertegrity.orion.client

import com.github.tomakehurst.wiremock.WireMockServer
import de.hypertegrity.orion.BaseTest
import io.kotest.matchers.collections.shouldHaveSize
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import testutil.TestUtil
import testutil.stubPaginatedResponseFromFile
import testutil.stubSuccessfulTokenResponse

class OffStreetParkingClientTest : BaseTest() {

    @Autowired
    lateinit var wireMockServer: WireMockServer

    @Autowired
    lateinit var parkingApi: OffStreetParkingApi

    @Autowired
    lateinit var parkingClient: OffStreetParkingClient

    @BeforeEach
    fun setup() {
        wireMockServer.stubSuccessfulTokenResponse(TestUtil.readFixtureResource("/stubs/token/sample-token.json"))
    }

    @Test
    fun `should get off-street parking areas`() {
        wireMockServer.stubPaginatedResponseFromFile("/parking", "/stubs/parking/sample-parking.json")

        val parking = parkingApi.getParkingAreas(
            fiwareService = "soest_dataspace",
            fiwareServicePath = "/parking",
            limit = 100,
            offset = 0,
            options = "count"
        )

        parking.body!!.shouldHaveSize(2)
    }

    @Test
    fun `should get all off-street parking areas`() {
        wireMockServer.stubPaginatedResponseFromFile("/parking", "/stubs/parking/sample-parking.json")

        val parking = parkingClient.getAllParkingAreas()

        parking.shouldHaveSize(2)
    }
}
