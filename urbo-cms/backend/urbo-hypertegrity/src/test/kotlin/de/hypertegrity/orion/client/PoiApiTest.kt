package de.hypertegrity.orion.client

import com.github.tomakehurst.wiremock.WireMockServer
import de.hypertegrity.orion.BaseTest
import io.kotest.matchers.collections.shouldHaveSize
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import testutil.TestUtil
import testutil.stubPaginatedResponseFromFile
import testutil.stubSuccessfulTokenResponse

class PoiApiTest : BaseTest() {

    @Autowired
    lateinit var wireMockServer: WireMockServer

    @Autowired
    lateinit var poiClient: PoiClient


    @BeforeEach
    fun setup() {
        wireMockServer.stubSuccessfulTokenResponse(TestUtil.readFixtureResource("/stubs/token/sample-token.json"))
    }


    @Test
    fun `should fetch pois`() {
        wireMockServer.stubPaginatedResponseFromFile("/pois", "/stubs/pois/sample-pois.json")

        val events = poiClient.getAllPois()

        events.shouldHaveSize(2)
    }
}