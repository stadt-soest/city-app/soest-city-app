package de.hypertegrity.orion.serialization.deserializer

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.ObjectMapper
import de.hypertegrity.orion.BaseTest
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe
import io.swcode.urbo.common.error.UrboException
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

class DecodingStringDeserializerTest : BaseTest() {

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Test
    fun `should decode escaped characters for string values`() {
        val json = "\"\\u003CHello\\u003E\""
        val result = objectMapper.readValue(json, String::class.java)

        result shouldBe "<Hello>"
    }

    @Test
    fun `should decode escaped characters for string arrays`() {
        val json = "[\"\\u003CHello\\u003E\", \"\\u0022World\\u0022\"]"
        val result = objectMapper.readValue(json, List::class.java)

        result shouldBe listOf("<Hello>", "\"World\"")
    }

    @Test
    fun `should throw exception for unsupported tokens`() {
        val json = "1234"
        val testMapper = ObjectMapper().registerModule(DecodingStringDeserializer.createModule())

        shouldThrow<UrboException> {
            testMapper.readValue(json, String::class.java)
        }
    }
}