package de.hypertegrity.orion.util

import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity

class FetchAllPaginatedDataKtTest {
    val fetchDataMock = mockk<(Int) -> ResponseEntity<List<String>>>(relaxed = true)

    @Test
    fun testFetchAllPaginatedData() {

        val headersWithTotalCount = HttpHeaders()
        headersWithTotalCount.add("Fiware-Total-Count", "4")

        val firstPage = ResponseEntity.ok().headers(headersWithTotalCount).body(listOf("Item1", "Item2"))
        val secondPage = ResponseEntity.ok().headers(headersWithTotalCount).body(listOf("Item3", "Item4"))

        every { fetchDataMock(0) } returns firstPage
        every { fetchDataMock(100) } returns secondPage

        val result = fetchAllPaginatedData(fetchDataMock)

        result shouldBe listOf("Item1", "Item2", "Item3", "Item4")

        verify(exactly = 1) { fetchDataMock(0) }
        verify(exactly = 1) { fetchDataMock(100) }
        verify(exactly = 0) { fetchDataMock(200) }
    }

    @Test
    fun `test with empty data response`() {
        every { fetchDataMock(any()) } returns ResponseEntity.ok(emptyList<String>())

        val result = fetchAllPaginatedData(fetchDataMock)

        result shouldBe emptyList()
    }

    @Test
    fun `test with single page of data`() {
        every { fetchDataMock(0) } returns ResponseEntity.ok(listOf("Item1", "Item2", "Item3", "Item4"))

        val result = fetchAllPaginatedData(fetchDataMock)

        result shouldBe listOf("Item1", "Item2", "Item3", "Item4")
    }

    @Test
    fun `test error handling`() {
        every { fetchDataMock(any()) } throws RuntimeException("Error fetching data")

        val result = runCatching { fetchAllPaginatedData(fetchDataMock) }

        result.isFailure shouldBe true
    }

    @Test
    fun `test without total count header`() {
        every { fetchDataMock(any()) } returns ResponseEntity.ok().body(listOf("Item"))

        val result = fetchAllPaginatedData(fetchDataMock)

        result shouldBe listOf("Item")
    }
}
