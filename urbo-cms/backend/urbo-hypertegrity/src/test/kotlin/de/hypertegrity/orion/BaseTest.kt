package de.hypertegrity.orion

import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@ActiveProfiles("test")
@EnableConfigurationProperties
@SpringBootTest(classes = [TestConfig::class], webEnvironment = SpringBootTest.WebEnvironment.NONE)
class BaseTest
