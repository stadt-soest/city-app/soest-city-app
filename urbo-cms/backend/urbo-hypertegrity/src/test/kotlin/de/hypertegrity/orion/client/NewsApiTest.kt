package de.hypertegrity.orion.client

import com.github.tomakehurst.wiremock.WireMockServer
import de.hypertegrity.orion.BaseTest
import io.kotest.matchers.collections.shouldHaveSize
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import testutil.TestUtil
import testutil.stubPaginatedResponseFromFile
import testutil.stubSuccessfulTokenResponse

class NewsApiTest : BaseTest() {

    @Autowired
    lateinit var wireMockServer: WireMockServer

    @Autowired
    lateinit var newsApi: NewsApi

    @Autowired
    lateinit var newsClient: NewsClient

    @BeforeEach
    fun setup() {
        wireMockServer.stubSuccessfulTokenResponse(TestUtil.readFixtureResource("/stubs/token/sample-token.json"))
    }

    @Test
    fun `should fetch news`() {
        wireMockServer.stubPaginatedResponseFromFile("/news", "/stubs/news/sample-news.json")

        val news = newsApi.getNews(
            fiwareService = "soest_dataspace",
            fiwareServicePath = "/news",
            limit = 100,
            offset = 0,
            options = "count"
        )

        news.body!!.shouldHaveSize(2)
    }

    @Test
    fun `should fetch all news`() {
        wireMockServer.stubPaginatedResponseFromFile("/news", "/stubs/news/sample-news.json")

        val news = newsClient.getAllNews()

        news.shouldHaveSize(2)
    }
}
