package de.hypertegrity.orion.client

import com.github.tomakehurst.wiremock.WireMockServer
import de.hypertegrity.orion.BaseTest
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import testutil.TestUtil
import testutil.stubPaginatedResponseFromFile
import testutil.stubSuccessfulTokenResponse

class CitizenServiceApiTest : BaseTest() {

    @Autowired
    lateinit var wireMockServer: WireMockServer

    @Autowired
    lateinit var citizenServiceApi: CitizenServiceApi

    @Autowired
    lateinit var citizenServiceClient: CitizenServiceClient

    @BeforeEach
    fun setup() {
        wireMockServer.stubSuccessfulTokenResponse(TestUtil.readFixtureResource("/stubs/token/sample-token.json"))
    }

    @Test
    fun `should fetch citizenServices`() {
        wireMockServer.stubPaginatedResponseFromFile("/citizenServices", "/stubs/citizen/sample-citizen-services.json")


        val citizenServices = citizenServiceApi.getCitizenServices(
            fiwareService = "soest_dataspace",
            fiwareServicePath = "/citizenServices",
            limit = 100,
            offset = 0,
            options = "count"
        )

        citizenServices.body!!.shouldHaveSize(2)
    }

    @Test
    fun `should fetch all citizenServices`() {
        wireMockServer.stubPaginatedResponseFromFile("/citizenServices", "/stubs/citizen/sample-citizen-services.json")

        val citizenServices = citizenServiceClient.getAllCitizenServices()

        citizenServices.shouldHaveSize(2)
    }

    @Test
    fun `should decode encoded values`() {
        wireMockServer.stubPaginatedResponseFromFile("/citizenServices", "/stubs/citizen/sample-citizen-services.json")

        val citizenServices = citizenServiceApi.getCitizenServices(
            fiwareService = "soest_dataspace",
            fiwareServicePath = "/citizenServices",
            limit = 100,
            offset = 0,
            options = "count"
        )

        citizenServices.body!!.shouldHaveSize(2)

        val firstService = citizenServices.body!![0]
        firstService.title.shouldBe("<Hello>")
        firstService.description.shouldBe("\"Sample\" description")
        firstService.url.shouldBe("http://sample.com;")

        val secondService = citizenServices.body!![1]
        secondService.title.shouldBe("Another <Service>")
    }
}
