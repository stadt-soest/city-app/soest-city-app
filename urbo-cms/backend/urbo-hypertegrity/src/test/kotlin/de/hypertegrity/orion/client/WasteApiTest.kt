package de.hypertegrity.orion.client

import com.github.tomakehurst.wiremock.WireMockServer
import de.hypertegrity.orion.BaseTest
import io.kotest.matchers.collections.shouldHaveSize
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import testutil.TestUtil
import testutil.stubPaginatedResponseFromFile
import testutil.stubSuccessfulTokenResponse

class WasteApiTest : BaseTest() {
    @Autowired
    lateinit var wireMockServer: WireMockServer

    @Autowired
    lateinit var wasteApi: WasteApi

    @Autowired
    lateinit var wasteClient: WasteClient

    @BeforeEach
    fun setup() {
        wireMockServer.stubSuccessfulTokenResponse(TestUtil.readFixtureResource("/stubs/token/sample-token.json"))

    }

    @Test
    fun `should fetch wasteCollections`() {
        wireMockServer.stubPaginatedResponseFromFile("/waste", "/stubs/waste/sample-waste.json")

        val wasteCollection =
            wasteApi.getWasteCollections(
                fiwareServicePath = "/waste",
                fiwareService = "soest_dataspace",
                limit = 100,
                offset = 0,
                options = "count"
            )

        wasteCollection.body!!.shouldHaveSize(3)
    }

    @Test
    fun `should fetch all wasteCollections`() {
        wireMockServer.stubPaginatedResponseFromFile("/waste", "/stubs/waste/sample-waste.json")

        val wasteCollection = wasteClient.getAllWastes()

        wasteCollection.shouldHaveSize(3)
    }
}
