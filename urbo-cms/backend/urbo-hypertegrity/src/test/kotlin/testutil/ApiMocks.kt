package testutil

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.MappingBuilder
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock.aResponse
import com.github.tomakehurst.wiremock.matching.AnythingPattern
import com.github.tomakehurst.wiremock.matching.EqualToPattern
import com.github.tomakehurst.wiremock.stubbing.Scenario
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

fun WireMockServer.stubPaginatedResponseFromFile(servicePath: String, filePath: String) {
    stubPaginatedResponse(servicePath, TestUtil.readFixtureResource(filePath))
}

fun WireMockServer.stubSuccessfulTokenResponse(responseBody: String) {
    stubFor(
        WireMock.post(WireMock.urlEqualTo("/realms/orion/protocol/openid-connect/token"))
            .withRequestBody(AnythingPattern()).willReturnJsonBody(HttpStatus.OK, responseBody)
    )
}

fun WireMockServer.stub401InvalidTokenResponseApi() {
    stubFor(
        WireMock.get(WireMock.urlPathEqualTo("/"))
            .withHeader("Fiware-ServicePath", EqualToPattern("/news"))
            .withHeader("Fiware-Service", EqualToPattern("soest_dataspace"))
            .willReturnJsonBody(HttpStatus.UNAUTHORIZED, "{\"active\": false}")
    )
}

fun WireMockServer.stubInvalidRefreshTokenFlow() {
    stubFor(
        WireMock.post(WireMock.urlEqualTo("/realms/orion/protocol/openid-connect/token"))
            .inScenario("Token Refresh Scenario")
            .whenScenarioStateIs(Scenario.STARTED)
            .withRequestBody(AnythingPattern())
            .willReturnJsonBody(
                HttpStatus.BAD_REQUEST,
                """
                {
                    "error": "invalid_grant",
                    "error_description": "Invalid refresh token"
                }
                """.trimIndent()
            )
            .willSetStateTo("Second Request")
    )
    stubFor(
        WireMock.post(WireMock.urlEqualTo("/realms/orion/protocol/openid-connect/token"))
            .inScenario("Token Refresh Scenario")
            .whenScenarioStateIs("Second Request")
            .withRequestBody(AnythingPattern())
            .willReturnJsonBody(HttpStatus.OK, TestUtil.readFixtureResource("/stubs/token/sample-token.json"))
            .willSetStateTo(Scenario.STARTED)
    )
}

private fun WireMockServer.stubPaginatedResponse(servicePath: String, responseBody: String) {
    stubFor(
        WireMock.get(WireMock.urlPathEqualTo("/"))
            .withQueryParam("limit", EqualToPattern("100"))
            .withQueryParam("offset", EqualToPattern("0"))
            .withHeader("Fiware-Service", EqualToPattern("soest_dataspace"))
            .withHeader("Fiware-ServicePath", EqualToPattern(servicePath))
            .willReturnJsonBody(HttpStatus.OK, responseBody)
    )
}

private fun <T : MappingBuilder> T.willReturnJsonBody(status: HttpStatus, jsonBody: String): T {
    @Suppress("UNCHECKED_CAST")
    return willReturn(
        aResponse().withStatus(status.value())
            .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE).withBody(jsonBody)
    ) as T
}
