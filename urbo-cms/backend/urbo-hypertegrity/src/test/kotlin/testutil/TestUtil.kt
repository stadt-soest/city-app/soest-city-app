package testutil

import java.nio.charset.StandardCharsets

object TestUtil {
    fun readFixtureResource(filePath: String): String {
        return this::class.java.getResource(filePath)!!.readText(StandardCharsets.UTF_8)
    }
}