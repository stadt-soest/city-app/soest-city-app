package de.hypertegrity.orion.dto

import java.time.Instant

data class WasteCollectionOrionDto(
    val id: String,
    val address: WasteAddress,
    val schedule: WasteSchedule,
)

data class WasteSchedule(
    val wastepaper: List<Instant>?,
    val biowaste: List<Instant>?,
    val yellowBag: List<Instant>?,
    val residualWaste14daily: List<Instant>?,
    val residualWaste4weekly: List<Instant>?,
    val diaperWaste: List<Instant>?
)

data class WasteAddress(
    val streetAddress: String,
    val addressLocality: String
)