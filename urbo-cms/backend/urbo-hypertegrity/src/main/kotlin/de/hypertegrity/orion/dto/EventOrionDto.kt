package de.hypertegrity.orion.dto

import java.time.Instant

data class EventOrionDto(
    val id: String,
    val areaServed: String?,
    val superEvent: String?,
    val subEvent: List<String>?,
    val name: String,
    val alternateName: String?,
    val deleted: Boolean,
    val description: String?,
    val source: String,
    val location: LocationOrionDto?,
    val address: AddressOrionDto?,
    val category: List<String>,
    val eventStatus: String,
    val startDate: Instant,
    val endDate: Instant,
    val contactPoint: ContactPointOrionDto,
    val image: EventImageOrionDto,
    val offers: OfferOrionDto?,
    val dateCreated: Instant,
    val dateModified: Instant?,
)

data class AddressOrionDto(
    val streetAddress: String?,
    val addressLocality: String?,
    val addressRegion: String?,
    val postalCode: String?,
)

data class ContactPointOrionDto(
    val telephone: String?,
    val email: String?
)

data class LocationOrionDto(
    val coordinates: List<Double?>?
)

data class OfferOrionDto(
    val url: String?,
    val description: String?,
    val price: String?,
    val availability: String?,
)

data class EventImageOrionDto(
    val contentUrl: String?,
    val description: String?,
    val copyrightHolder: String?
)





