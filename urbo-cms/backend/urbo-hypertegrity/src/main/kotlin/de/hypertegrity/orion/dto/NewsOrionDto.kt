package de.hypertegrity.orion.dto

import java.time.Instant

data class NewsOrionDto(
    val id: String,
    val title: String?,
    val subtitle: String?,
    val authors: List<AuthorOrionDto>? = emptyList(),
    val categories: List<NewsCategoryOrionDto>? = emptyList(),
    val content: String?,
    val date: Instant?,
    val image: FeaturedMediaOrionDto? = null,
    val link: String?,
    val modified: Instant?,
    val source: String
)

data class NewsCategoryOrionDto(
    val name: String,
)

data class FeaturedMediaOrionDto(
    val url: String?,
    val copyright: String?,
    val description: String?
)

data class AuthorOrionDto(
    val name: String,
)






