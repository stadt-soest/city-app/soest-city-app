package de.hypertegrity.orion.client

import de.hypertegrity.orion.feign.config.FeignConfig
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping

@FeignClient(
    name = "nodeRedApi",
    url = "\${nodered.url}",
    configuration = [FeignConfig::class]
)
fun interface NodeRedApi {
    @GetMapping
    fun getNodeRedStatus(): ResponseEntity<String>
}

@Component
class NodeRedClient(
    private val nodeRedApi: NodeRedApi
) {
    fun checkStatus(): Boolean {
        return try {
            val response = nodeRedApi.getNodeRedStatus()
            response.statusCode.is2xxSuccessful
        } catch (ex: Exception) {
            false
        }
    }
}