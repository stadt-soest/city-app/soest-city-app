package de.hypertegrity.orion.dto

data class CitizenServiceOrionDto(
    val id: String,
    val title: String,
    val description: String,
    val url: String,
)
