package de.hypertegrity.orion.client

import de.hypertegrity.orion.dto.WasteCollectionOrionDto
import de.hypertegrity.orion.feign.config.FeignConfig
import de.hypertegrity.orion.util.fetchAllPaginatedData
import org.springframework.beans.factory.annotation.Value
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestParam


@FeignClient(
    name = "waste",
    url = "\${orion.api.waste-collection.url}",
    configuration = [FeignConfig::class]
)
fun interface WasteApi {
    @GetMapping
    fun getWasteCollections(
        @RequestHeader("Fiware-Service") fiwareService: String,
        @RequestHeader("Fiware-ServicePath") fiwareServicePath: String,
        @RequestParam("limit") limit: Int,
        @RequestParam("offset") offset: Int,
        @RequestParam("options") options: String,
    ): ResponseEntity<List<WasteCollectionOrionDto>>
}

@Component
class WasteClient(
    private val wasteApi: WasteApi,
    @Value("\${orion.fiware.service.path.waste-collection}") private val fiwareServicePath: String,
    @Value("\${orion.fiware.service}") private val fiwareService: String,
    @Value("\${orion.fetch.limit}") private val fetchLimit: Int
) {
    fun getAllWastes(): List<WasteCollectionOrionDto> {
        return fetchAllPaginatedData(
            fetchData = { offset ->
                wasteApi.getWasteCollections(
                    fiwareService,
                    fiwareServicePath,
                    fetchLimit,
                    offset = offset,
                    options = "count"
                )
            },
            fetchLimit = fetchLimit
        )
    }
}


