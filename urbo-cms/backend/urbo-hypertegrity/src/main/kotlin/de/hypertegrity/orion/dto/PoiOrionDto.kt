package de.hypertegrity.orion.dto

import java.time.Instant

data class PoiOrionDto(
    val id: String,
    val name: String,
    val address: PoiAddress,
    val category: List<String>,
    val location: PoiLocation,
    val contactPoint: ContactPoint,
    val dateModified: Instant,
    val additionalInfoURL: String?
)

data class PoiAddress(val streetAddress: String)
data class PoiLocation(val coordinates: List<Double>)
data class ContactPoint(val telephone: String, val email: String, val contactPerson: String)
