package de.hypertegrity.orion.serialization.deserializer

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.module.SimpleModule
import de.hypertegrity.orion.serialization.utility.DecodingUtility
import io.swcode.urbo.common.error.UrboException

class DecodingStringDeserializer : JsonDeserializer<String>() {

    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): String {
        if (p.currentToken == JsonToken.VALUE_STRING) {
            return DecodingUtility.decodeEscapedCharacters(p.text)
        }
        throw UrboException("Unsupported token for string deserialization: ${p.currentToken}")
    }

    companion object {
        fun createModule(): SimpleModule {
            val module = SimpleModule()
            module.addDeserializer(String::class.java, DecodingStringDeserializer())
            return module
        }
    }
}
