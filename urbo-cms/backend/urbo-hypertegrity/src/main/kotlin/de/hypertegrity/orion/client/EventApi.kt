package de.hypertegrity.orion.client

import de.hypertegrity.orion.dto.EventOrionDto
import de.hypertegrity.orion.feign.config.FeignConfig
import de.hypertegrity.orion.util.fetchAllPaginatedData
import org.springframework.beans.factory.annotation.Value
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestParam

@FeignClient(
    name = "events",
    url = "\${orion.api.event.url}",
    configuration = [FeignConfig::class]
)
fun interface EventApi {
    @GetMapping
    fun getEvents(
        @RequestHeader("Fiware-Service") fiwareService: String,
        @RequestHeader("Fiware-ServicePath") fiwareServicePath: String,
        @RequestParam("limit") limit: Int,
        @RequestParam("offset") offset: Int,
        @RequestParam("options") options: String,
    ): ResponseEntity<List<EventOrionDto>>
}

@Component
class EventClient(
    private val eventApi: EventApi,
    @Value("\${orion.fiware.service.path.events}") private val fiwareServicePath: String,
    @Value("\${orion.fiware.service}") private val fiwareService: String,
    @Value("\${orion.fetch.limit}") private val fetchLimit: Int
) {
    fun getAllEvents(): List<EventOrionDto> {
        return fetchAllPaginatedData(
            fetchData = { offset ->
                eventApi.getEvents(
                    fiwareService,
                    fiwareServicePath,
                    fetchLimit,
                    offset = offset,
                    options = "count",
                )
            },
            fetchLimit = fetchLimit
        )
    }
}
