package de.hypertegrity.orion

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.context.annotation.Configuration

@SpringBootApplication
@Configuration
@EnableFeignClients
class HypertegrityConfig