package de.hypertegrity.orion.client

import de.hypertegrity.orion.dto.PoiOrionDto
import de.hypertegrity.orion.feign.config.FeignConfig
import de.hypertegrity.orion.util.fetchAllPaginatedData
import org.springframework.beans.factory.annotation.Value
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestParam

@FeignClient(
    name = "pois",
    url = "\${orion.api.url}",
    configuration = [FeignConfig::class]
)
fun interface PoiApi {
    @GetMapping
    fun getPois(
        @RequestHeader("Fiware-Service") fiwareService: String,
        @RequestHeader("Fiware-ServicePath") fiwareServicePath: String,
        @RequestParam("limit") limit: Int,
        @RequestParam("offset") offset: Int,
        @RequestParam("options") options: String,
    ): ResponseEntity<List<PoiOrionDto>>
}

@Component
class PoiClient(
    private val poiApi: PoiApi,
    @Value("\${orion.fiware.service.path.pois}") private val fiwareServicePath: String,
    @Value("\${orion.fiware.service}") private val fiwareService: String,
    @Value("\${orion.fetch.limit}") private val fetchLimit: Int
) {
    fun getAllPois(): List<PoiOrionDto> {
        return fetchAllPaginatedData(
            fetchData = { offset ->
                poiApi.getPois(
                    fiwareService,
                    fiwareServicePath,
                    fetchLimit,
                    offset = offset,
                    options = "count",
                )
            },
            fetchLimit = fetchLimit
        )
    }
}