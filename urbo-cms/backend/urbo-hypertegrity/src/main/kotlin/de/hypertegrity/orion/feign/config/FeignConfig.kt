package de.hypertegrity.orion.feign.config

import de.hypertegrity.orion.feign.decoder.FeignErrorDecoder
import de.hypertegrity.orion.serialization.CustomObjectMapper
import feign.codec.Decoder
import feign.codec.ErrorDecoder
import org.springframework.beans.factory.ObjectProvider
import org.springframework.boot.autoconfigure.http.HttpMessageConverters
import org.springframework.cloud.openfeign.support.HttpMessageConverterCustomizer
import org.springframework.cloud.openfeign.support.ResponseEntityDecoder
import org.springframework.cloud.openfeign.support.SpringDecoder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter

@Configuration
class FeignConfig {

    @Bean
    fun feignErrorDecoder(): ErrorDecoder {
        return FeignErrorDecoder()
    }

    @Bean
    fun feignDecoder(customizers: ObjectProvider<HttpMessageConverterCustomizer>): Decoder {
        val jacksonConverter = MappingJackson2HttpMessageConverter(CustomObjectMapper.create())
        val converters: MutableList<HttpMessageConverter<*>> = mutableListOf(jacksonConverter)
        val httpMessageConverters = HttpMessageConverters(*converters.toTypedArray())
        return ResponseEntityDecoder(SpringDecoder({ httpMessageConverters }, customizers))
    }
}
