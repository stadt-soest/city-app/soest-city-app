package de.hypertegrity.orion.serialization

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import de.hypertegrity.orion.serialization.deserializer.DecodingStringDeserializer

object CustomObjectMapper {
    fun create(): ObjectMapper {
        return ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .registerModule(DecodingStringDeserializer.createModule())
            .registerKotlinModule()
            .registerModule(JavaTimeModule())
    }
}
