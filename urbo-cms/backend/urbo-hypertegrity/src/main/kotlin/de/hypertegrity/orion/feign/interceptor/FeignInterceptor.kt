package de.hypertegrity.orion.feign.interceptor

import de.hypertegrity.orion.security.TokenManager
import feign.RequestInterceptor
import feign.RequestTemplate
import org.springframework.stereotype.Component

@Component
class FeignInterceptor(private val tokenManager: TokenManager) : RequestInterceptor {

    override fun apply(requestTemplate: RequestTemplate) {
        val token = tokenManager.getToken()
        requestTemplate.header("Authorization", "Bearer $token")
    }
}
