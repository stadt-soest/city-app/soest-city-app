package de.hypertegrity.orion.security

import com.nimbusds.oauth2.sdk.*
import com.nimbusds.oauth2.sdk.auth.ClientAuthentication
import com.nimbusds.oauth2.sdk.auth.ClientSecretBasic
import com.nimbusds.oauth2.sdk.auth.Secret
import com.nimbusds.oauth2.sdk.id.ClientID
import com.nimbusds.oauth2.sdk.token.AccessToken
import com.nimbusds.oauth2.sdk.token.RefreshToken
import io.swcode.urbo.common.error.UrboException
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.net.URI
import java.time.Instant

@Service
class TokenManager {

    @Value("\${orion.clientId}")
    private lateinit var orionClientId: String

    @Value("\${orion.username}")
    private lateinit var orionUsername: String

    @Value("\${orion.password}")
    private lateinit var orionPassword: String

    @Value("\${orion.secret}")
    private lateinit var orionSecret: String

    @Value("\${orion.scope}")
    private lateinit var orionScope: List<String>

    @Value("\${orion.api.auth-token.url}")
    private lateinit var orionTokenUrl: String

    private var accessToken: AccessToken? = null
    private var refreshToken: RefreshToken? = null
    private var accessTokenExpiryTime: Long = 0

    private val gracePeriod: Long = 60

    @Synchronized
    fun getToken(): AccessToken {
        if (accessToken == null || Instant.now().epochSecond >= (accessTokenExpiryTime - gracePeriod)) {
            refreshToken()
        }
        return accessToken ?: throw UrboException("Access token is null")
    }

    private fun refreshToken() {
        val clientAuth: ClientAuthentication = ClientSecretBasic(ClientID(orionClientId), Secret(orionSecret))
        val tokenRequest = tokenRequest(clientAuth)
        val response = TokenResponse.parse(tokenRequest.toHTTPRequest().send())
        processResponse(response)
    }

    private fun tokenRequest(clientAuth: ClientAuthentication): TokenRequest {
        val endpointURI = URI(orionTokenUrl)
        return if (refreshToken != null) {
            val refreshGrant = RefreshTokenGrant(refreshToken)
            TokenRequest.Builder(endpointURI, clientAuth, refreshGrant).build()
        } else {
            val passwordGrant = ResourceOwnerPasswordCredentialsGrant(orionUsername, Secret(orionPassword))
            val scope = Scope(*orionScope.toTypedArray())
            TokenRequest.Builder(endpointURI, clientAuth, passwordGrant)
                .scope(scope)
                .build()
        }
    }

    fun processResponse(tokenResponse: TokenResponse) {
        if (tokenResponse.indicatesSuccess()) {
            val successResponse = tokenResponse.toSuccessResponse()
            accessToken = successResponse.tokens.accessToken
            refreshToken = successResponse.tokens.refreshToken
            accessTokenExpiryTime = Instant.now().epochSecond + successResponse.tokens.accessToken.lifetime
        } else {
            val errorResponse = tokenResponse.toErrorResponse()
            val errorObject = errorResponse.errorObject
            val description = errorObject.description.lowercase()
            when (errorObject.code) {
                "invalid_grant" -> {
                    when (description) {
                        "invalid user credentials" -> throw UrboException(errorObject.description)
                        "invalid refresh token", "token is not active", "session not active" -> {
                            accessToken = null
                            refreshToken = null
                            getToken()
                        }
                        else -> throw UrboException(errorObject.description)
                    }
                }
                else -> throw UrboException("${errorObject.code}: ${errorObject.description}")
            }
        }
    }
}
