package de.hypertegrity.orion.serialization.utility

object DecodingUtility {
    fun decodeEscapedCharacters(value: String): String {
        return value
            .replace("\\u003C", "<")
            .replace("\\u003E", ">")
            .replace("\\u0022", "\"")
            .replace("\\u0027", "'")
            .replace("\\u003D", "=")
            .replace("\\u003B", ";")
            .replace("\\u0028", "(")
            .replace("\\u0029", ")")
    }
}