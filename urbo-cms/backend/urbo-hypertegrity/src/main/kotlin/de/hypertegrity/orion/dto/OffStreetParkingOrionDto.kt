package de.hypertegrity.orion.dto

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime
import java.time.LocalTime

data class OffStreetParkingOrionDto(
    val id: String,
    val address: ParkingAddressOrionDto,
    val displayName: String,
    val parkingType: OrionParkingType,
    val location: ParkingLocationOrionDto,
    val allowedVehicleType: List<ParkingVehicleTypeOrionDto>,
    val availableSpotNumber: Int,
    val name: String,
    val occupancyModified: LocalDateTime,
    val occupiedSpotNumber: Int,
    val status: List<ParkingStateOrionDto>,
    val openingHoursSpecification: List<OpeningHoursSpecificationOrionDto>,
    val totalSpotNumber: Int,
    val unclassifiedSlots: UnclassifiedSlotsOrionDto,
    val maximumAllowedHeight: String?,
    val priceDescription: ParkingDescription,
    val description: ParkingDescription?,
)

data class ParkingLocationOrionDto(
    val coordinates: Array<Double>,
)

enum class OrionParkingType {
    GARAGE,
    PARKING_HOUSE,
    UNDERGROUND,
    PARKING_SPOT
}

data class ParkingAddressOrionDto(
    val addressCounty: String,
    val addressLocality: String,
    val addressRegion: String,
    val postalCode: String,
    val streetAddress: String,
    val streetNr: String,
)

data class StructuredOpeningHoursOrionDto(
    val from: LocalTime,
    val to: LocalTime,
)

data class OpeningHoursSpecificationOrionDto(
    val dayOfWeek: List<String>,
    val opens: String,
    val closes: String
)

data class UnclassifiedSlotsOrionDto(
    val totalSpotNumber: Int,
    val availableSpotNumber: Int,
    val occupiedSpotNumber: Int,
)

data class ParkingDescription(
    @JsonProperty("DE") val de: String,
    @JsonProperty("EN") val en: String
)

enum class ParkingVehicleTypeOrionDto {
    car
}


enum class ParkingStateOrionDto {
    open,
    closed,
    disturbance
}