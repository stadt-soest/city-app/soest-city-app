package de.hypertegrity.orion.util

import org.springframework.http.ResponseEntity

fun <T> fetchAllPaginatedData(
    fetchData: (Int) -> ResponseEntity<List<T>>,
    fetchLimit: Int = 100
): List<T> {
    val allData = mutableListOf<T>()
    var offset = 0
    var totalCount: Int? = null
    var fetchedCount = 0

    do {
        val response = fetchData(offset)
        val data = response.body ?: emptyList()
        val dataSize = data.size
        allData.addAll(data)

        if (totalCount == null) {
            totalCount = response.headers["Fiware-Total-Count"]?.firstOrNull()?.toInt()
        }

        fetchedCount += dataSize

        if (dataSize == 0) {
            break
        }

        offset += fetchLimit
    } while (fetchedCount < (totalCount ?: 0))

    return allData
}

