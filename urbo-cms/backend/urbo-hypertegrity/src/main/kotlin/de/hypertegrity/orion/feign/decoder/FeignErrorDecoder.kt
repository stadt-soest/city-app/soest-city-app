package de.hypertegrity.orion.feign.decoder

import com.fasterxml.jackson.databind.ObjectMapper
import feign.Response
import feign.RetryableException
import feign.codec.ErrorDecoder
import java.nio.charset.StandardCharsets
import java.util.*

class FeignErrorDecoder : ErrorDecoder {
    private val defaultErrorDecoder = ErrorDecoder.Default()
    private val objectMapper = ObjectMapper()

    override fun decode(methodKey: String?, response: Response): Exception {
        val errorResponse = parseErrorResponse(response)
        if (response.status() == 403 || (response.status() == 401 && errorResponse.containsKey("active"))) {
            return retry(response)
        }
        return defaultErrorDecoder.decode(methodKey, response)
    }

    private fun parseErrorResponse(response: Response): Map<*, *> {
        response.body().asReader(StandardCharsets.UTF_8).use { reader ->
            return objectMapper.readValue(reader, Map::class.java)
        }
    }

    private fun retry(response: Response): RetryableException {
        return RetryableException(
            response.status(),
            "$response",
            response.request().httpMethod(),
            null,
            Date(),
            response.request()
        )
    }
}
