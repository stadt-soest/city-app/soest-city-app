plugins {
    id("io.swcode.urbo.library-conventions")
}

dependencies {
    implementation(project(":urbo-common"))

    implementation(libs.jackson.databind)
    implementation(libs.jackson.module.kotlin)
    implementation(libs.jackson.datatype.js310)
    implementation(libs.spring.boot)

    // Feign Client
    implementation(libs.spring.cloud.starter.openfeign)
    implementation(libs.oauth2.oidc.sdk)

    implementation(libs.jackson.module.kotlin)
    implementation(libs.jackson.core)


    testImplementation(libs.spring.cloud.wiremock)
    testImplementation(libs.mockk)
    testImplementation(libs.spring.boot.starter.test)
}
