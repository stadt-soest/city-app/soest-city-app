plugins {
    id("io.swcode.urbo.library-conventions")
}

dependencies {
    implementation(project(":urbo-common"))
    implementation(project(":urbo-auth-adapter"))

    implementation(libs.spring.boot.starter.web)
    implementation(libs.keycloak.admin.client)

    testImplementation(libs.spring.boot.starter.test)
    testImplementation(libs.testcontainers.keycloak)
    testImplementation(libs.testcontainers.junit.jupiter)
}