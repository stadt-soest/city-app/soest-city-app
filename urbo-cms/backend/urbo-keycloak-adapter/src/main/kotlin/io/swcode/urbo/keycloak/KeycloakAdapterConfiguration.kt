package io.swcode.urbo.keycloak

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan
class KeycloakAdapterConfiguration

