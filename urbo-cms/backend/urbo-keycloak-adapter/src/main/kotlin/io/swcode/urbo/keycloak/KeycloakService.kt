package io.swcode.urbo.keycloak

import io.swcode.urbo.common.error.ErrorType
import io.swcode.urbo.common.error.UrboException
import io.swcode.urbo.common.logging.createLogger
import io.swcode.urbo.core.api.auth.AuthUserAdapter
import io.swcode.urbo.core.api.auth.IdpUserDto
import jakarta.ws.rs.NotFoundException
import org.keycloak.OAuth2Constants
import org.keycloak.admin.client.CreatedResponseUtil
import org.keycloak.admin.client.KeycloakBuilder
import org.keycloak.representations.idm.UserRepresentation
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.util.*

@Service
class KeycloakService(
    @Value("\${keycloak.management.url}") private val keycloakUrl: String,
    @Value("\${keycloak.management.realm}") private val keycloakRealm: String,
    @Value("\${keycloak.management.client-id}") private val keycloakClientId: String,
    @Value("\${keycloak.management.client-secret}") private val keycloakClientSecret: String
) : AuthUserAdapter {

    private val keycloak by lazy {
        KeycloakBuilder.builder()
            .serverUrl(keycloakUrl)
            .realm(keycloakRealm)
            .clientId(keycloakClientId)
            .clientSecret(keycloakClientSecret)
            .grantType(OAuth2Constants.CLIENT_CREDENTIALS)
            .build()
    }

    override fun createUser(email: String, firstName: String, lastName: String): IdpUserDto {
        val existingUsers = safeRequest {
            keycloak.realm(keycloakRealm).users().search(email, true)
        }

        if (existingUsers.isNotEmpty()) {
            val existingUser = existingUsers[0]
            return IdpUserDto(
                id = UUID.fromString(existingUser.id),
                email = existingUser.email
            )
        }

        val userId = safeRequest {
            val userRepresentation = UserRepresentation().apply {
                this.username = email
                this.email = email
                this.firstName = firstName
                this.lastName = lastName
                this.isEmailVerified = false
                this.isEnabled = true
                requiredActions = listOf(USER_ACTION_UPDATE_PASSWORD, USER_ACTION_VERIFY_EMAIL)
            }

            val response = keycloak.realm(keycloakRealm).users().create(userRepresentation)
            if (response.status != 201) {
                throw UrboException("Failed to create user: ${response.status}")
            }

            CreatedResponseUtil.getCreatedId(response)
        }

        return IdpUserDto(
            id = UUID.fromString(userId),
            email = email,
        )
    }

    override fun deleteUserById(userId: String) {
        safeRequest {
            val response = keycloak.realm(keycloakRealm).users().delete(userId)
            if (response.status != 204) {
                throw UrboException("Failed to delete user with ID: $userId")
            }
        }
    }

    override fun updateUser(userId: String, firstName: String, lastName: String) {
        safeRequest {
            try {
                val userResource = keycloak.realm(keycloakRealm).users().get(userId)
                val userRepresentation = userResource.toRepresentation()

                userRepresentation.firstName = firstName
                userRepresentation.lastName = lastName

                userResource.update(userRepresentation)
            } catch (e: NotFoundException) {
                throw UrboException("User with ID: $userId not found", ErrorType.RESOURCE_NOT_FOUND, e)
            }
        }
    }

    override fun resetPasswordByEmail(email: String) {
        safeRequest {
            val users = keycloak.realm(keycloakRealm).users().search(email)
            if (users.isEmpty()) {
                throw UrboException("User with email $email not found", ErrorType.RESOURCE_NOT_FOUND)
            }

            val userRepresentation = users[0]
            val userId = userRepresentation.id

            keycloak.realm(keycloakRealm).users().get(userId)
                .executeActionsEmail(listOf(USER_ACTION_UPDATE_PASSWORD))

            logger.info("Password reset email sent to $email")
        }
    }

    private fun <T> safeRequest(block: () -> T): T {
        return try {
            block()
        } catch (exception: Exception) {
            logger.error("Error during Keycloak API access", exception)
            throw UrboException("Keycloak API error", ErrorType.UNEXPECTED, exception)
        }
    }

    companion object {
        private val logger = createLogger()
        private const val USER_ACTION_UPDATE_PASSWORD = "UPDATE_PASSWORD"
        private const val USER_ACTION_VERIFY_EMAIL = "VERIFY_EMAIL"
    }
}