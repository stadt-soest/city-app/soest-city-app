package io.swcode.urbo.keycloak.adapter

import dasniko.testcontainers.keycloak.KeycloakContainer
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.TestInstance
import org.keycloak.admin.client.Keycloak
import org.keycloak.admin.client.KeycloakBuilder
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers

@Testcontainers
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
abstract class BaseTest {

    protected lateinit var keycloakAdmin: Keycloak

    companion object {

        @Container
        private val keycloakContainer = KeycloakContainer("quay.io/keycloak/keycloak:25.0.5")
            .withAdminUsername("admin")
            .withAdminPassword("admin")
            .withRealmImportFile("/test-realm.json")

        @JvmStatic
        @DynamicPropertySource
        fun registerDynamicProperties(registry: DynamicPropertyRegistry) {
            keycloakContainer.start()
            registry.add("keycloak.management.url") { keycloakContainer.authServerUrl }
            registry.add("keycloak.management.realm") { "test" }
            registry.add("keycloak.management.client-id") { "urbo-api" }
            registry.add("keycloak.management.client-secret") { "secret" }
        }
    }

    @BeforeAll
    fun baseSetup() {
        keycloakAdmin = KeycloakBuilder.builder()
            .serverUrl(keycloakContainer.authServerUrl)
            .realm("test")
            .clientId("urbo-api")
            .clientSecret("secret")
            .grantType("client_credentials")
            .build()
    }

    @AfterAll
    fun baseTearDown() {
        keycloakContainer.stop()
    }
}
