package io.swcode.urbo.keycloak.adapter

import io.kotest.matchers.shouldBe
import io.swcode.urbo.keycloak.KeycloakAdapterConfiguration
import io.swcode.urbo.keycloak.KeycloakService
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.testcontainers.junit.jupiter.Testcontainers

@Testcontainers
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(classes = [KeycloakAdapterConfiguration::class])
class KeycloakServiceTest : BaseTest() {

    @Autowired
    private lateinit var keycloakService: KeycloakService

    @Test
    fun `should create a user`() {
        val userId = keycloakService.createUser("john.doe@example.com", "firstName", "lastName")

        val createdUser = keycloakAdmin
            .realm(REALM)
            .users()
            .get(userId.id.toString())
            .toRepresentation()

        createdUser.username shouldBe "john.doe@example.com"
        createdUser.email shouldBe "john.doe@example.com"
    }

    @Test
    fun `should update a user`() {
        val userId = keycloakService.createUser("jane.doe@example.com", "firstName", "lastName")

        keycloakService.updateUser(userId.id.toString(), "updatedFirstName", "updatedLastName")

        val updatedUser = keycloakAdmin
            .realm(REALM)
            .users()
            .get(userId.id.toString())
            .toRepresentation()

        updatedUser.firstName shouldBe "updatedFirstName"
        updatedUser.lastName shouldBe "updatedLastName"
    }

    @Test
    fun `should delete a user`() {
        val userId = keycloakService.createUser("mark.twain@example.com", "firstName", "lastName")

        keycloakService.deleteUserById(userId.id.toString())

        val users = keycloakAdmin
            .realm(REALM)
            .users()
            .search("mark.twain@example.com")

        users.size shouldBe 0
    }

    companion object {
        private const val REALM = "test"
    }
}
