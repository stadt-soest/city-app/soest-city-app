plugins {
    id("io.swcode.urbo.library-conventions")
}

dependencies {
    implementation(project(":urbo-common"))
    implementation(project(":urbo-meilisearch-api"))
    implementation(libs.spring.boot)
    implementation(libs.slf4j.api)
    implementation(libs.meilisearch)
    implementation(libs.jackson.core)
    implementation(libs.jackson.databind)

    testImplementation(libs.spring.boot.starter.test)
    testImplementation(libs.testcontainers)
    testImplementation(libs.mockk)
}