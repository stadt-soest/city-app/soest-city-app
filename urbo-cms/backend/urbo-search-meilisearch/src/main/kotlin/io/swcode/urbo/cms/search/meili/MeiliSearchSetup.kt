package io.swcode.urbo.cms.search.meili

import com.meilisearch.sdk.Client
import com.meilisearch.sdk.exceptions.MeilisearchException
import com.meilisearch.sdk.model.TaskStatus
import io.swcode.urbo.cms.search.meili.api.SearchIndexConfig
import io.swcode.urbo.common.logging.createLogger
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component

@Component
class MeiliSearchSetup(
    private val searchIndexConfig: SearchIndexConfig,
    private val client: Client,
) {

    companion object {
        private val logger = createLogger()
        private const val MAX_RETRIES = 10
    }

    @Async
    @EventListener(ApplicationReadyEvent::class)
    fun onApplicationReady() {
        try {
            searchIndexConfig.indexes.forEach { index ->
                createIndex(index.uid, index.primaryKey)
                configureIndex(index)
            }
        } catch (e: Exception) {
            logger.error("Failed to create or configure index", e)
        }
    }

    private fun createIndex(uid: String, primaryKey: String) {
        try {
            client.getIndex(uid)
        } catch (e: MeilisearchException) {
            logger.info("Create search index for $uid")
            val taskInfo = client.createIndex(uid, primaryKey)

            var retries = 0
            while (client.getTask(taskInfo.taskUid).status != TaskStatus.SUCCEEDED && retries < MAX_RETRIES) {
                logger.info("Create search index for $uid not finished yet, taskId=${taskInfo.taskUid}")
                Thread.sleep(200)
                retries++
            }
        }
    }

    private fun configureIndex(index: SearchIndexConfig.Index) {
        try {
            val filterables = (index.filterableAttributes ?: listOf()).plus("tenantIdentifier").distinct()

            client.index(index.uid).apply {
                updateSortableAttributesSettings(index.sort?.toTypedArray() ?: arrayOf())
                updateDistinctAttributeSettings(index.distinct)
                updateFilterableAttributesSettings(filterables.toTypedArray())
            }
            logger.info("Configured index ${index.uid} with tenantIdentifier as filterable")
        } catch (e: Exception) {
            logger.error("Failed to configure index ${index.uid}", e)
        }
    }
}