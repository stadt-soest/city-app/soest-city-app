package io.swcode.urbo.cms.search.meili

import com.meilisearch.sdk.Client
import com.meilisearch.sdk.Config
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan
class MeiliSearchConfig {

    @Bean
    fun client(
        @Value("\${swcode.meilisearch.url}") url: String,
        @Value("\${swcode.meilisearch.apiKey}") apiKey: String
    ): Client {
        return Client(Config(url, apiKey))
    }
}