package io.swcode.urbo.cms.search.meili

import com.fasterxml.jackson.databind.ObjectMapper
import com.meilisearch.sdk.Client
import io.swcode.urbo.cms.search.meili.api.SearchIndexerAdapter
import io.swcode.urbo.common.error.ErrorType
import io.swcode.urbo.common.error.UrboException
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class MeiliSearchAdapter(
    private val client: Client,
    private val objectMapper: ObjectMapper,
    @Value("\${swcode.meilisearch.tenantIdentifier}") private val tenantIdentifier: String
) : SearchIndexerAdapter {
    init {
        require(tenantIdentifier.isNotBlank()) { "tenantIdentifier must not be blank" }
        require(tenantIdentifier.matches(Regex("^[a-zA-Z0-9-_]+$"))) { "tenantIdentifier contains invalid characters" }
    }

    override fun updateIndex(indexId: String, payload: Map<String, Any?>) {
        try {
            val enrichedPayload = enrichWithTenantIdentifier(payload, tenantIdentifier)
            val serializedPayload = objectMapper.writeValueAsString(enrichedPayload)
            client.index(indexId).addDocuments(serializedPayload)
        } catch (e: Exception) {
            throw UrboException("Failed to enrich or index payload", ErrorType.UNEXPECTED, e)
        }
    }

    override fun deleteFromIndex(indexId: String, documentId: String) {
        client.index(indexId).deleteDocument(documentId)
    }

    private fun enrichWithTenantIdentifier(payload: Map<String, Any?>, tenantIdentifier: String): Map<String, Any?> {
        if (payload.containsKey("tenantIdentifier")) {
            throw UrboException("Document already contains tenantIdentifier field")
        }
        return payload + ("tenantIdentifier" to tenantIdentifier)
    }
}