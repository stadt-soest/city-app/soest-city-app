package io.swcode.urbo.cms.search.meili

import com.fasterxml.jackson.databind.ObjectMapper
import io.swcode.urbo.cms.search.meili.api.SearchIndexConfig
import io.swcode.urbo.cms.search.meili.api.SearchIndexedDto
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class TestConfig {

    @Bean
    fun objectMapper(): ObjectMapper {
        return ObjectMapper()
    }

    @Bean
    fun searchIndexConfig(): SearchIndexConfig {
        return SearchIndexConfig(
            listOf(
                SearchIndexConfig.Index(
                    listOf("MyEntity"),
                    "my-index",
                    listOf("id", "startAt"),
                    "title",
                    mapper = { NoopDto("test-id") }
                )
            )
        )
    }

    private data class NoopDto(val id: String) : SearchIndexedDto
}