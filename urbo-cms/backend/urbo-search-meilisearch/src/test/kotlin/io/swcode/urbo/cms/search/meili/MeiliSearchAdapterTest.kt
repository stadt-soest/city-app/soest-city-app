package io.swcode.urbo.cms.search.meili

import com.meilisearch.sdk.Client
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.collections.shouldContainExactlyInAnyOrder
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.swcode.urbo.common.error.UrboException
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.testcontainers.containers.GenericContainer
import org.testcontainers.shaded.org.awaitility.Awaitility.await
import org.testcontainers.utility.DockerImageName
import java.util.concurrent.TimeUnit

@ContextConfiguration
@Import(MeiliSearchConfig::class)
@ExtendWith(SpringExtension::class)
class MeiliSearchAdapterTest {

    @Autowired
    private lateinit var meiliSearchAdapter: MeiliSearchAdapter

    @Autowired
    private lateinit var meiliSearchSetup: MeiliSearchSetup

    @Autowired
    private lateinit var client: Client

    @Test
    fun `should add and delete document`() {
        val document = mapOf("id" to "42")

        meiliSearchAdapter.updateIndex("MyEntity", document)

        await().atMost(5, TimeUnit.SECONDS).untilAsserted {
            client.index("MyEntity").search("42").apply {
                hits.shouldHaveSize(1)
            }
        }

        meiliSearchAdapter.deleteFromIndex("MyEntity", "42")

        await().atMost(5, TimeUnit.SECONDS).untilAsserted {
            client.index("MyEntity").search("42").apply {
                hits.shouldBeEmpty()
            }
        }
    }

    @Test
    fun `should setup attributes`() {
        meiliSearchSetup.onApplicationReady()

        await().atMost(5, TimeUnit.SECONDS).untilAsserted {
            client.index("my-index").sortableAttributesSettings.shouldContainExactlyInAnyOrder(arrayOf("id", "startAt"))
            client.index("my-index").distinctAttributeSettings.shouldBe("title")
        }
    }

    @Test
    fun `should enrich document with tenantIdentifier`() {
        val document = mapOf("id" to "42", "title" to "Test Document")

        meiliSearchAdapter.updateIndex("MyEntity", document)

        await().atMost(5, TimeUnit.SECONDS).untilAsserted {
            val indexedDocument = client.index("MyEntity").getDocument("42", Map::class.java) as Map<String, Any>
            indexedDocument["tenantIdentifier"] shouldBe "tenantIdentifier"
            indexedDocument["id"] shouldBe "42"
            indexedDocument["title"] shouldBe "Test Document"
        }
    }

    @Test
    fun `should throw exception if payload enrichment fails due to existing tenantIdentifier`() {
        val invalidDocument = mapOf(
            "id" to "42",
            "title" to "Test Document",
            "tenantIdentifier" to "Test Identifier"
        )

        shouldThrow<UrboException> {
            meiliSearchAdapter.updateIndex("MyEntity", invalidDocument)
        }
    }

    companion object {
        private const val MASTER_KEY = "masterKey"
        private const val PORT = 7700

        private val meilisearchContainer = GenericContainer(DockerImageName.parse("getmeili/meilisearch:v1.7.6"))
            .withEnv("MEILI_MASTER_KEY", MASTER_KEY)
            .withExposedPorts(PORT)

        @DynamicPropertySource
        @JvmStatic
        fun registerDynamicProperties(registry: DynamicPropertyRegistry) {
            registry.add("swcode.meilisearch.tenantIdentifier") { "tenantIdentifier" }
            meilisearchContainer.start()
            val port = meilisearchContainer.getMappedPort(PORT)
            registry.add("swcode.meilisearch.url") { "http://localhost:$port" }
            registry.add("swcode.meilisearch.apiKey") { MASTER_KEY }
            registry.add("swcode.meilisearch.index[0].uid") { "my-index" }
        }
    }
}