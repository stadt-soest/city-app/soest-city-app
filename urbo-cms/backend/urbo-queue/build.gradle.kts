plugins {
    id("io.swcode.urbo.library-conventions")
    alias(libs.plugins.kotlin.jpa)
}

dependencies {
    implementation(libs.spring.boot.starter.web)
    implementation(libs.spring.boot.starter.data.jpa)
    implementation(libs.spring.security.config)
    implementation(project(":urbo-permission-adapter"))
    implementation(project(":urbo-common"))
    implementation(project(":urbo-command-api"))
    implementation(libs.spring.boot)
    implementation(libs.db.queue.core)
    implementation(libs.db.queue.spring)
    implementation(libs.jackson.databind)
    implementation(libs.jackson.core)
    implementation(libs.jackson.module.kotlin)
    implementation(libs.javax.annoations.api)

    implementation(libs.opentelemetry.api)

    testImplementation(libs.swagger.request.validator.mockmvc)
    testImplementation(libs.spring.boot.starter.test)
    testImplementation(libs.spring.security.test)
    testImplementation(libs.mockk)
}