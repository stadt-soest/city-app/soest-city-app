package io.swcode.urbo.cms.queue.command

import io.swcode.urbo.command.api.AsyncCommandSubmitter
import io.swcode.urbo.command.api.Command
import io.swcode.urbo.command.api.WrappedAsyncCommand
import org.springframework.stereotype.Component
import ru.yoomoney.tech.dbqueue.api.EnqueueParams
import ru.yoomoney.tech.dbqueue.api.impl.ShardingQueueProducer
import ru.yoomoney.tech.dbqueue.spring.dao.SpringDatabaseAccessLayer

@Component
class WrappedAsyncCommandListener(private val sharded: ShardingQueueProducer<Command, SpringDatabaseAccessLayer>) :
    AsyncCommandSubmitter {

    override fun submit(command: WrappedAsyncCommand<Command>) {
        val params = EnqueueParams.create(command.command)
        params.withExtData("command_id", command.commandId.toString())
        params.withExtData("command_name", command.command::class.simpleName)
        command.delay?.also {
            params.withExecutionDelay(it)
        }
        sharded.enqueue(params)
    }
}