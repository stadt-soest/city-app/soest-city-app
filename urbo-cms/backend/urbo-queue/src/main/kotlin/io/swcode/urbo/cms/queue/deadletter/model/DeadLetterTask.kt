package io.swcode.urbo.cms.queue.deadletter.model

import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table
import java.time.LocalDateTime
import java.util.*

@Entity
@Table(name = "dead_letter_tasks")
data class DeadLetterTask(
    @Id
    val id: UUID,
    val commandId: UUID,
    val payload: String,
    val createdAt: LocalDateTime,
    val failureReason: String,
) {
    companion object {
        fun fromTask(
            commandId: UUID,
            payload: String,
            failureReason: String?
        ): DeadLetterTask {
            return DeadLetterTask(
                id = UUID.randomUUID(),
                commandId = commandId,
                payload = payload,
                createdAt = LocalDateTime.now(),
                failureReason = failureReason ?: "Unknown Error",
            )
        }
    }
}