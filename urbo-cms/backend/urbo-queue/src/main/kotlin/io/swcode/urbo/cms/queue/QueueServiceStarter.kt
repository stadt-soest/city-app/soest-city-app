package io.swcode.urbo.cms.queue

import io.swcode.urbo.cms.queue.command.CommandQueueConsumer
import io.swcode.urbo.cms.queue.lifecycle.TaskLifeCycleLogger
import io.swcode.urbo.cms.queue.lifecycle.TheadLifeCycleLogger
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import ru.yoomoney.tech.dbqueue.config.QueueService
import ru.yoomoney.tech.dbqueue.config.QueueShard
import ru.yoomoney.tech.dbqueue.spring.dao.SpringDatabaseAccessLayer
import javax.annotation.PreDestroy

@Component
class QueueServiceStarter(
    queueShard: QueueShard<SpringDatabaseAccessLayer>,
    private val commandQueueConsumer: CommandQueueConsumer
) {

    private val queueService: QueueService = QueueService(
        listOf(queueShard),
        TheadLifeCycleLogger.INSTANCE,
        TaskLifeCycleLogger.INSTANCE
    )

    @EventListener(ApplicationStartedEvent::class)
    fun applicationStarted() {
        queueService.registerQueue(commandQueueConsumer)
        queueService.start()
    }

    @PreDestroy
    fun preDestroy() {
        queueService.shutdown()
    }
}