package io.swcode.urbo.cms.queue.deadletter

import io.swcode.urbo.permission.adapter.Permission
import io.swcode.urbo.permission.adapter.UserManagementPermissionProvider
import org.springframework.stereotype.Component

@Component
class QueuePermissionProvider : UserManagementPermissionProvider {
    override val permissions: List<Permission> = listOf(QueuePermission.QUEUE_MANAGE)
}