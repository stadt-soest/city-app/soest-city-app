package io.swcode.urbo.cms.queue.lifecycle

import io.opentelemetry.api.GlobalOpenTelemetry
import io.opentelemetry.api.trace.Span
import io.opentelemetry.api.trace.SpanKind
import io.opentelemetry.api.trace.StatusCode
import io.opentelemetry.api.trace.Tracer
import io.opentelemetry.context.Context
import io.swcode.urbo.common.logging.createLogger
import ru.yoomoney.tech.dbqueue.api.TaskExecutionResult
import ru.yoomoney.tech.dbqueue.api.TaskRecord
import ru.yoomoney.tech.dbqueue.config.QueueShardId
import ru.yoomoney.tech.dbqueue.config.TaskLifecycleListener
import ru.yoomoney.tech.dbqueue.settings.QueueLocation


class TaskLifeCycleLogger : TaskLifecycleListener {
    private val tracer: Tracer = GlobalOpenTelemetry.getTracer("urbo-queue")

    companion object {
        private val log = createLogger()
        val INSTANCE = TaskLifeCycleLogger()
    }

    override fun picked(shardId: QueueShardId, location: QueueLocation, taskRecord: TaskRecord, pickTaskTime: Long) {
        log.trace("picked: {}, {}, {}, pickTaskTime={}", shardId, location, taskRecord, pickTaskTime)
    }

    override fun started(shardId: QueueShardId, location: QueueLocation, taskRecord: TaskRecord) {
        val commandName = taskRecord.extData["command_name"] ?: "UNKNOWN command"
        tracer.spanBuilder(commandName)
            .setSpanKind(SpanKind.SERVER)
            .setAttribute("shardId", shardId.toString())
            .setAttribute("queueLocation", location.toString())
            .setAttribute("taskId", taskRecord.id.toString())
            .startSpan()
            .makeCurrent()

        log.trace("started: {}, {}, {}", shardId, location, taskRecord)
    }

    override fun executed(
        shardId: QueueShardId,
        location: QueueLocation,
        taskRecord: TaskRecord,
        executionResult: TaskExecutionResult,
        processTaskTime: Long
    ) {
        log.trace("executed taskId: " + taskRecord.id + " payload: ${taskRecord.payload}")
        Span.current().also {
            it.setAttribute("processTaskTime", processTaskTime)
            it.setAttribute("actionType", executionResult.actionType.name)
        }
    }

    override fun finished(shardId: QueueShardId, location: QueueLocation, taskRecord: TaskRecord) {
        log.trace("finished: {}, {}, {}", shardId, location, taskRecord)
        try {
            Span.current().end()
        } finally {
            // Explicitly close the scope by restoring the previous context
            Context.root().makeCurrent()
        }
    }

    override fun crashed(
        shardId: QueueShardId,
        location: QueueLocation,
        taskRecord: TaskRecord,
        exc: java.lang.Exception?
    ) {
        log.trace("crashed: {}, {}, {}", shardId, location, taskRecord, exc)
        if(exc != null) {
            Span.current().setStatus(StatusCode.ERROR).recordException(exc)
        }
    }
}