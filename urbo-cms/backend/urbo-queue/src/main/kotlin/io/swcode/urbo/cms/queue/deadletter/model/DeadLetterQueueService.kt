package io.swcode.urbo.cms.queue.deadletter.model

import io.swcode.urbo.cms.queue.command.WrappedAsyncCommandListener
import io.swcode.urbo.cms.queue.transformer.CommandPayloadTransformer
import io.swcode.urbo.command.api.WrappedAsyncCommand
import io.swcode.urbo.common.error.ErrorType
import io.swcode.urbo.common.error.UrboException
import jakarta.transaction.Transactional
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.*

@Service
class DeadLetterQueueService(
    private val deadLetterTaskRepository: DeadLetterTaskRepository,
    private val wrappedAsyncCommandListener: WrappedAsyncCommandListener,
    private val commandPayloadTransformer: CommandPayloadTransformer
) {

    fun getDeadLetterTask(taskId: UUID): DeadLetterTask = deadLetterTaskRepository.findByIdOrNull(taskId)
        ?: throw UrboException("Task with ID $taskId not found", ErrorType.RESOURCE_NOT_FOUND)

    fun getAllDeadLetterTasks(): List<DeadLetterTask> = deadLetterTaskRepository.findAll()

    fun deleteDeadLetterTask(taskId: UUID) = deadLetterTaskRepository.deleteById(taskId)

    @Transactional
    fun requeueTask(taskId: UUID): DeadLetterTask = deadLetterTaskRepository.findById(taskId)
        .orElseThrow { UrboException("Task not found") }
        .also { deadLetterTask ->
            val command = commandPayloadTransformer.toObject(deadLetterTask.payload)
            val wrappedCommand = WrappedAsyncCommand(command!!, commandId = deadLetterTask.commandId)
            wrappedAsyncCommandListener.submit(wrappedCommand)
            deadLetterTaskRepository.delete(deadLetterTask)
        }
}