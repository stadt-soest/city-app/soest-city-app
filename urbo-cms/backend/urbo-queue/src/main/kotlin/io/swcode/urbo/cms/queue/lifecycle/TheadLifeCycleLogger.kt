package io.swcode.urbo.cms.queue.lifecycle

import io.swcode.urbo.common.logging.createLogger
import ru.yoomoney.tech.dbqueue.config.QueueShardId
import ru.yoomoney.tech.dbqueue.config.ThreadLifecycleListener
import ru.yoomoney.tech.dbqueue.settings.QueueLocation

class TheadLifeCycleLogger : ThreadLifecycleListener {

    companion object {
        val INSTANCE = TheadLifeCycleLogger()
        private val log = createLogger()
    }

    override fun started(shardId: QueueShardId, location: QueueLocation) {
        log.trace("Started shardId={}, queueLocation={}", shardId, location)
    }

    override fun executed(
        shardId: QueueShardId?,
        location: QueueLocation?,
        taskProcessed: Boolean,
        threadBusyTime: Long
    ) {
        log.trace(
            "Executed shardId={}, queueLocation={}, taskProcessed={}, threadBusyTime={}",
            shardId,
            location,
            taskProcessed,
            threadBusyTime
        )
    }

    override fun finished(shardId: QueueShardId, location: QueueLocation) {
        log.trace("Finished shardId={}, queueLocation={}", shardId, location)
    }

    override fun crashed(shardId: QueueShardId, location: QueueLocation, exc: Throwable?) {
        log.error("Crashed shardId=$shardId, queueLocation=$location", exc)
    }
}