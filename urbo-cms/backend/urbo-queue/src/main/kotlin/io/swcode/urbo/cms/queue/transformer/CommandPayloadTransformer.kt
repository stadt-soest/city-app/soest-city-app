package io.swcode.urbo.cms.queue.transformer

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.swcode.urbo.command.api.Command
import io.swcode.urbo.command.api.CommandRegistry
import org.springframework.stereotype.Component
import ru.yoomoney.tech.dbqueue.api.TaskPayloadTransformer

@Component
class CommandPayloadTransformer(
    private val commandRegistry: CommandRegistry,
    private val objectMapper: ObjectMapper
) : TaskPayloadTransformer<Command> {

    companion object {
        private const val PAYLOAD_KEY = "payload"
        private const val COMMAND_NAME_KEY = "commandName"
    }

    override fun toObject(payload: String?): Command? {
        val map: Map<String, String> = objectMapper.readValue(payload!!)
        return objectMapper.readValue(
            map[PAYLOAD_KEY],
            commandRegistry.getCommandClass(map.getValue(COMMAND_NAME_KEY)).java
        )
    }

    override fun fromObject(payload: Command?): String? {
        val jsonCommand = objectMapper.writeValueAsString(payload!!)
        val commandName = payload::class.simpleName

        val map = mapOf(COMMAND_NAME_KEY to commandName, PAYLOAD_KEY to jsonCommand)

        return objectMapper.writeValueAsString(map)
    }
}