package io.swcode.urbo.cms.queue.deadletter

import io.swcode.urbo.cms.queue.deadletter.model.DeadLetterTask
import io.swcode.urbo.common.date.toBerlinInstant
import java.time.Instant
import java.util.*

data class DeadLetterTaskDto(
    val id: UUID,
    val payload: String,
    val createdAt: Instant,
    val failureReason: String,
) {
    companion object {
        fun fromDomain(task: DeadLetterTask): DeadLetterTaskDto {
            return DeadLetterTaskDto(
                task.id,
                task.payload,
                task.createdAt.toBerlinInstant(),
                task.failureReason,
            )
        }
    }
}