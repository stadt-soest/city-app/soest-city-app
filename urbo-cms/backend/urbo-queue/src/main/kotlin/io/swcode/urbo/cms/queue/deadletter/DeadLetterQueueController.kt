package io.swcode.urbo.cms.queue.deadletter

import io.swcode.urbo.cms.queue.deadletter.model.DeadLetterQueueService
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/v1")
@PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.queue.deadletter.QueuePermission).QUEUE_MANAGE)")
class DeadLetterQueueController(private val deadLetterQueueService: DeadLetterQueueService) {

    @GetMapping("/dead-letter-tasks")
    fun getAllDeadLetterTasks(): ResponseEntity<List<DeadLetterTaskDto>> {
        return ResponseEntity.ok(deadLetterQueueService.getAllDeadLetterTasks().map {
            DeadLetterTaskDto.fromDomain(it)
        })
    }

    @GetMapping("/dead-letter-tasks/{taskId}")
    fun getTask(@PathVariable taskId: UUID): ResponseEntity<DeadLetterTaskDto> {
        deadLetterQueueService.getDeadLetterTask(taskId).also {
            return ResponseEntity.ok(DeadLetterTaskDto.fromDomain(it))
        }
    }

    @DeleteMapping("/dead-letter-tasks/{taskId}")
    fun deleteTask(@PathVariable taskId: UUID): ResponseEntity<Unit> {
        deadLetterQueueService.deleteDeadLetterTask(taskId).also {
            return ResponseEntity.ok().build()
        }
    }

    @PostMapping("/dead-letter-task-queue/{taskId}")
    fun requeueTask(@PathVariable taskId: UUID): ResponseEntity<DeadLetterTaskDto> {
        deadLetterQueueService.requeueTask(taskId).let {
            return ResponseEntity.ok(DeadLetterTaskDto.fromDomain(it))
        }
    }
}