package io.swcode.urbo.cms.queue.command

import io.swcode.urbo.cms.queue.deadletter.model.DeadLetterTask
import io.swcode.urbo.cms.queue.deadletter.model.DeadLetterTaskRepository
import io.swcode.urbo.cms.queue.transformer.CommandPayloadTransformer
import io.swcode.urbo.command.api.Command
import io.swcode.urbo.command.api.CommandBus
import io.swcode.urbo.common.error.UrboException
import io.swcode.urbo.common.logging.createLogger
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import ru.yoomoney.tech.dbqueue.api.QueueConsumer
import ru.yoomoney.tech.dbqueue.api.Task
import ru.yoomoney.tech.dbqueue.api.TaskExecutionResult
import ru.yoomoney.tech.dbqueue.api.TaskPayloadTransformer
import ru.yoomoney.tech.dbqueue.settings.QueueConfig
import java.util.*

@Component
class CommandQueueConsumer(
    private val queueConfig: QueueConfig,
    private val commandPayloadTransformer: CommandPayloadTransformer,
    private val commandBus: CommandBus,
    private val deadLetterTaskRepository: DeadLetterTaskRepository,
    @Value("\${queue.retry.maxAttempts}") private val maxRetryAttempts: Int
) : QueueConsumer<Command> {

    override fun execute(task: Task<Command>): TaskExecutionResult {
        val commandId =
            task.extData["command_id"]?.let(UUID::fromString) ?: throw UrboException("commandId is null: $task")
        return try {
            commandBus.execute(commandId, task.payloadOrThrow)
            TaskExecutionResult.finish()
        } catch (e: Exception) {
            handleFailure(task, commandId, e)
        }
    }

    override fun getQueueConfig(): QueueConfig = queueConfig

    override fun getPayloadTransformer(): TaskPayloadTransformer<Command> = commandPayloadTransformer

    private fun handleFailure(task: Task<Command>, commandId: UUID, e: Exception): TaskExecutionResult {
        val attempts = task.totalAttemptsCount + 1
        if (attempts >= maxRetryAttempts) {
            return moveToDeadLetter(commandId, task, e)
        }
        return TaskExecutionResult.fail()
    }

    private fun moveToDeadLetter(commandId: UUID, task: Task<Command>, e: Exception): TaskExecutionResult {
        DeadLetterTask.fromTask(
            commandId = commandId,
            payload = commandPayloadTransformer.fromObject(task.payloadOrThrow)!!,
            failureReason = e.message
        ).also {
            deadLetterTaskRepository.save(it)
            log.debug("Moved command: {} to dead letter", commandId)
            return TaskExecutionResult.finish()
        }
    }

    companion object {
        val log = createLogger()
    }
}