package io.swcode.urbo.cms.queue

import io.swcode.urbo.cms.queue.transformer.CommandPayloadTransformer
import io.swcode.urbo.command.api.Command
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.transaction.support.TransactionTemplate
import ru.yoomoney.tech.dbqueue.api.impl.ShardingQueueProducer
import ru.yoomoney.tech.dbqueue.api.impl.SingleQueueShardRouter
import ru.yoomoney.tech.dbqueue.config.DatabaseDialect
import ru.yoomoney.tech.dbqueue.config.QueueShard
import ru.yoomoney.tech.dbqueue.config.QueueShardId
import ru.yoomoney.tech.dbqueue.config.QueueTableSchema
import ru.yoomoney.tech.dbqueue.settings.*
import ru.yoomoney.tech.dbqueue.spring.dao.SpringDatabaseAccessLayer
import java.time.Duration

@Configuration
@ComponentScan
class QueueConfiguration {

    @Bean
    fun databaseAccessLayer(
        jdbcTemplate: JdbcTemplate,
        transactionTemplate: TransactionTemplate
    ): SpringDatabaseAccessLayer {
        return SpringDatabaseAccessLayer(
            DatabaseDialect.POSTGRESQL,
            QueueTableSchema.builder().withExtFields(listOf("command_id", "command_name")).build(),
            jdbcTemplate,
            transactionTemplate
        )
    }

    @Bean
    fun queueShard(databaseAccessLayer: SpringDatabaseAccessLayer): QueueShard<SpringDatabaseAccessLayer> {
        return QueueShard(
            QueueShardId("main"),
            databaseAccessLayer
        )
    }

    @Bean
    fun shardingQueueProducer(
        queueConfig: QueueConfig,
        queueShard: QueueShard<SpringDatabaseAccessLayer>,
        commandPayloadTransformer: CommandPayloadTransformer
    ): ShardingQueueProducer<Command, SpringDatabaseAccessLayer> {
        return ShardingQueueProducer(queueConfig, commandPayloadTransformer, SingleQueueShardRouter(queueShard))
    }

    @Bean
    fun queueConfig(): QueueConfig {
        return QueueConfig(
            QueueLocation.builder().withTableName("queue_tasks")
                .withQueueId(QueueId("command_queue")).build(),
            QueueSettings.builder()
                .withPollSettings(
                    PollSettings.builder()
                        .withBetweenTaskTimeout(Duration.ofMillis(0))
                        .withNoTaskTimeout(Duration.ofMillis(100))
                        .withFatalCrashTimeout(Duration.ofMillis(1000))
                        .build()
                )
                .withProcessingSettings(
                    ProcessingSettings.builder()
                        .withProcessingMode(ProcessingMode.SEPARATE_TRANSACTIONS)
                        .withThreadCount(4)
                        .build()
                )
                .withReenqueueSettings(
                    ReenqueueSettings.builder()
                        .withRetryType(ReenqueueRetryType.ARITHMETIC)
                        .withArithmeticStep(Duration.ofMillis(100))
                        .withInitialDelay(Duration.ofMillis(0))
                        .build()
                )
                .withFailureSettings(
                    FailureSettings.builder()
                        .withRetryType(FailRetryType.GEOMETRIC_BACKOFF)
                        .withRetryInterval(Duration.ofSeconds(1))
                        .build()
                )
                .withExtSettings(ExtSettings.builder().withSettings(mapOf()).build())
                .build()
        )
    }
}