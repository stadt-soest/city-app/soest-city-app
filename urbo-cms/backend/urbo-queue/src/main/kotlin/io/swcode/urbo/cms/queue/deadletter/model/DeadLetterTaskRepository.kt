package io.swcode.urbo.cms.queue.deadletter.model

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface DeadLetterTaskRepository : JpaRepository<DeadLetterTask, UUID>