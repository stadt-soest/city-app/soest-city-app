package io.swcode.urbo.cms.queue.deadletter

import io.swcode.urbo.permission.adapter.Permission

enum class QueuePermission(override val value: String) : Permission {
    QUEUE_MANAGE("QUEUE_MANAGE")
}