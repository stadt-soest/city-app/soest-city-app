CREATE TABLE IF NOT EXISTS queue_tasks
(
    id                BIGSERIAL PRIMARY KEY,
    queue_name        TEXT NOT NULL,
    payload           TEXT,
    created_at        TIMESTAMP WITH TIME ZONE DEFAULT now(),
    next_process_at   TIMESTAMP WITH TIME ZONE DEFAULT now(),
    attempt           INTEGER                  DEFAULT 0,
    reenqueue_attempt INTEGER                  DEFAULT 0,
    total_attempt     INTEGER                  DEFAULT 0
);

CREATE INDEX IF NOT EXISTS queue_tasks_name_time_desc_idx
    ON queue_tasks USING btree (queue_name, next_process_at, id DESC);


ALTER TABLE queue_tasks ADD COLUMN IF NOT EXISTS command_id TEXT;

CREATE TABLE IF NOT EXISTS dead_letter_tasks
(
    id                      UUID PRIMARY KEY,
    command_id              UUID         NOT NULL,
    payload                 TEXT         NOT NULL,
    created_at              TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    failure_reason          TEXT
);


CREATE INDEX IF NOT EXISTS idx_dead_letter_tasks_command_id ON dead_letter_tasks (command_id);
