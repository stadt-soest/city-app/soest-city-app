package io.swcode.urb.cms.queue.deadletter

import io.mockk.junit5.MockKExtension
import io.swcode.urbo.cms.queue.deadletter.DeadLetterQueueController
import io.swcode.urbo.cms.queue.deadletter.model.DeadLetterQueueService
import jakarta.annotation.PostConstruct
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import testutil.OpenApiResultHandler
import testutil.createQueueTask
import java.util.*


@ExtendWith(MockKExtension::class, SpringExtension::class)
@WebMvcTest(controllers = [DeadLetterQueueController::class])
@ContextConfiguration(classes = [DeadLetterQueueController::class])
class DeadLetterQueueControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var webApplicationContext: WebApplicationContext


    @MockBean
    private lateinit var deadLetterQueueService: DeadLetterQueueService

    @PostConstruct
    fun setupValidation() {
        mockMvc = MockMvcBuilders
            .webAppContextSetup(webApplicationContext)
            .alwaysDo<DefaultMockMvcBuilder>(OpenApiResultHandler("openapi-spec.yaml"))
            .build()
    }

    @Test
    fun `should get all dead letter tasks`() {
        val deadLetterTasks = listOf(createQueueTask())

        Mockito.`when`(deadLetterQueueService.getAllDeadLetterTasks()).thenReturn(deadLetterTasks)

        mockMvc.perform(get("/v1/dead-letter-tasks"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.size()").value(deadLetterTasks.size))
            .andExpect(jsonPath("$[0].id").value(deadLetterTasks[0].id.toString()))
            .andExpect(jsonPath("$[0].payload").value(deadLetterTasks[0].payload))
            .andExpect(jsonPath("$[0].failureReason").value(deadLetterTasks[0].failureReason))

        Mockito.verify(deadLetterQueueService, times(1)).getAllDeadLetterTasks()
    }

    @Test
    fun `should get a dead letter task by id`() {
        val taskId = UUID.randomUUID()
        val task = createQueueTask(id = taskId)

        Mockito.`when`(deadLetterQueueService.getDeadLetterTask(taskId)).thenReturn(task)

        mockMvc.perform(get("/v1/dead-letter-tasks/{taskId}", taskId))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(task.id.toString()))
            .andExpect(jsonPath("$.payload").value(task.payload))
            .andExpect(jsonPath("$.failureReason").value(task.failureReason))

        Mockito.verify(deadLetterQueueService, times(1)).getDeadLetterTask(taskId)
    }

    @Test
    fun `should delete a dead letter task`() {
        val taskId = UUID.randomUUID()

        mockMvc.perform(delete("/v1/dead-letter-tasks/{taskId}", taskId))
            .andExpect(status().isOk)

        Mockito.verify(deadLetterQueueService, times(1)).deleteDeadLetterTask(taskId)
    }

    @Test
    fun `should requeue a dead letter task`() {
        val taskId = UUID.randomUUID()
        val queueTask = createQueueTask()

        Mockito.`when`(deadLetterQueueService.requeueTask(taskId)).thenReturn(queueTask)

        mockMvc.perform(post("/v1/dead-letter-task-queue/{taskId}", taskId))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(queueTask.id.toString()))
            .andExpect(jsonPath("$.payload").value(queueTask.payload))
            .andExpect(jsonPath("$.failureReason").value(queueTask.failureReason))

        Mockito.verify(deadLetterQueueService, times(1)).requeueTask(taskId)
    }
}