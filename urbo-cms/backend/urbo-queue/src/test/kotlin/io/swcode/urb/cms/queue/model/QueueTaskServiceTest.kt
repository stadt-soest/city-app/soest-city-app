package io.swcode.urb.cms.queue.model

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.verify
import io.swcode.urbo.cms.queue.command.WrappedAsyncCommandListener
import io.swcode.urbo.cms.queue.deadletter.model.DeadLetterQueueService
import io.swcode.urbo.cms.queue.deadletter.model.DeadLetterTaskRepository
import io.swcode.urbo.cms.queue.transformer.CommandPayloadTransformer
import io.swcode.urbo.command.api.Command
import io.swcode.urbo.common.error.UrboException
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.data.repository.findByIdOrNull
import testutil.createQueueTask
import java.util.*

@ExtendWith(MockKExtension::class)
class QueueTaskServiceTest {

    @InjectMockKs
    lateinit var deadLetterQueueService: DeadLetterQueueService

    @MockK
    lateinit var deadLetterTaskRepository: DeadLetterTaskRepository

    @MockK
    lateinit var wrappedAsyncCommandListener: WrappedAsyncCommandListener

    @MockK
    lateinit var commandPayloadTransformer: CommandPayloadTransformer

    @Test
    fun `should get all dead letter tasks`() {
        val queueTask = createQueueTask()

        every { deadLetterTaskRepository.findAll() } returns listOf(queueTask)

        deadLetterQueueService.getAllDeadLetterTasks().also {
            it shouldHaveSize 1
            it[0] shouldBe queueTask
        }
    }

    @Test
    fun `should return dead letter task when found`() {
        val taskId = UUID.randomUUID()
        val expectedTask = createQueueTask(id = taskId)

        every { deadLetterTaskRepository.findByIdOrNull(taskId) } returns expectedTask

        val result = deadLetterQueueService.getDeadLetterTask(taskId)

        result shouldBe expectedTask
        verify(exactly = 1) { deadLetterTaskRepository.findByIdOrNull(taskId) }
    }

    @Test
    fun `should throw UrboException when task not found`() {
        val taskId = UUID.randomUUID()

        every { deadLetterTaskRepository.findByIdOrNull(taskId) } returns null

        val exception = shouldThrow<UrboException> {
            deadLetterQueueService.getDeadLetterTask(taskId)
        }

        exception.message shouldBe "Task with ID $taskId not found"
        verify(exactly = 1) { deadLetterTaskRepository.findByIdOrNull(taskId) }
    }

    @Test
    fun `should delete dead letter task`() {
        val taskId = UUID.randomUUID()
        every { deadLetterTaskRepository.deleteById(taskId) } returns Unit

        deadLetterQueueService.deleteDeadLetterTask(taskId)

        verify { deadLetterTaskRepository.deleteById(taskId) }
    }

    @Test
    fun `should requeue task`() {
        val taskId = UUID.randomUUID()
        val queueTask = createQueueTask(id = taskId)
        val command = mockk<Command>()

        every { deadLetterTaskRepository.findById(taskId) } returns Optional.of(queueTask)
        every { commandPayloadTransformer.toObject(queueTask.payload) } returns command
        every { deadLetterTaskRepository.delete(queueTask) } returns Unit
        every { wrappedAsyncCommandListener.submit(any()) } returns Unit

        deadLetterQueueService.requeueTask(taskId).also {
            it shouldBe queueTask
        }

        verify {
            deadLetterTaskRepository.findById(taskId)
            commandPayloadTransformer.toObject(queueTask.payload)
            wrappedAsyncCommandListener.submit(any())
            deadLetterTaskRepository.delete(queueTask)
        }
    }
}
