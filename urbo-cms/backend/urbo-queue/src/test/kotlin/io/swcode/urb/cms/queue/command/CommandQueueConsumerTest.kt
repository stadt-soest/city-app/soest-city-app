package io.swcode.urb.cms.queue.command

import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import io.swcode.urbo.cms.queue.command.CommandQueueConsumer
import io.swcode.urbo.cms.queue.deadletter.model.DeadLetterTask
import io.swcode.urbo.cms.queue.deadletter.model.DeadLetterTaskRepository
import io.swcode.urbo.cms.queue.transformer.CommandPayloadTransformer
import io.swcode.urbo.command.api.Command
import io.swcode.urbo.command.api.CommandBus
import org.junit.jupiter.api.Test
import ru.yoomoney.tech.dbqueue.api.Task
import ru.yoomoney.tech.dbqueue.api.TaskExecutionResult
import ru.yoomoney.tech.dbqueue.settings.QueueConfig
import java.util.*

class CommandQueueConsumerTest {

    private val commandBus = mockk<CommandBus>()
    private val queueConfig = mockk<QueueConfig>()
    private val deadLetterTaskRepository = mockk<DeadLetterTaskRepository>()
    private val commandPayloadTransformer = mockk<CommandPayloadTransformer>()
    private val maxRetryAttempts = 3

    private val commandQueueConsumer = CommandQueueConsumer(
        commandBus = commandBus,
        commandPayloadTransformer = commandPayloadTransformer,
        queueConfig = queueConfig,
        deadLetterTaskRepository = deadLetterTaskRepository,
        maxRetryAttempts = maxRetryAttempts,
    )

    @Test
    fun `should finish task successfully when command executes without error`() {
        val commandId = UUID.randomUUID()
        val command = mockk<Command>(relaxed = true)
        val task = mockk<Task<Command>>(relaxed = true)

        every { task.extData["command_id"] } returns commandId.toString()
        every { task.payloadOrThrow } returns command
        every { commandBus.execute(commandId, command) } returns Unit

        val result = commandQueueConsumer.execute(task)

        assert(result.actionType == TaskExecutionResult.Type.FINISH)
    }

    @Test
    fun `should move queue command to dead letter queue after max retries`() {
        val commandId = UUID.randomUUID()
        val command = mockk<Command>(relaxed = true)
        val task = mockk<Task<Command>>(relaxed = true)
        val capturedDeadLetterTask = slot<DeadLetterTask>()
        var attempts = 0

        every { task.extData["command_id"] } returns commandId.toString()
        every { task.payloadOrThrow } returns command
        every { task.totalAttemptsCount } answers { attempts.toLong() }
        every { commandBus.execute(commandId, command) } throws RuntimeException("Command execution failed")
        every { commandPayloadTransformer.fromObject(command) } returns "payloadString"
        every { deadLetterTaskRepository.save(capture(capturedDeadLetterTask)) } returnsArgument 0

        repeat(3) {
            attempts++
            commandQueueConsumer.execute(task)
        }

        verify(exactly = 1) {
            deadLetterTaskRepository.save(capturedDeadLetterTask.captured)
        }

        assert(capturedDeadLetterTask.captured.commandId == commandId)
        assert(capturedDeadLetterTask.captured.payload == "payloadString")
        assert(capturedDeadLetterTask.captured.failureReason == "Command execution failed")
    }
}