package testutil

import com.atlassian.oai.validator.OpenApiInteractionValidator
import com.atlassian.oai.validator.mockmvc.OpenApiValidationMatchers
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.ResultHandler

class OpenApiResultHandler(specPath: String) : ResultHandler {
    private val validator: OpenApiInteractionValidator = OpenApiInteractionValidator
        .createForSpecificationUrl(specPath)
        .withResolveCombinators(true)
        .build()

    override fun handle(mvcResult: MvcResult) {
        OpenApiValidationMatchers.openApi().isValid(validator).match(mvcResult)
    }
}