package testutil

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.swcode.urbo.cms.queue.deadletter.model.DeadLetterTask
import io.swcode.urbo.command.api.Command
import java.time.LocalDateTime
import java.util.*

class CommandTest : Command {
    val id: UUID = UUID.randomUUID()
}

fun createQueueTask(
    id: UUID = UUID.randomUUID(),
    payload: CommandTest = CommandTest()
): DeadLetterTask {
    val objectMapper = jacksonObjectMapper()
    val payloadJson = objectMapper.writeValueAsString(payload)

    return DeadLetterTask(
        id = id,
        commandId = UUID.randomUUID(),
        createdAt = LocalDateTime.now(),
        failureReason = "",
        payload = payloadJson,
    )
}