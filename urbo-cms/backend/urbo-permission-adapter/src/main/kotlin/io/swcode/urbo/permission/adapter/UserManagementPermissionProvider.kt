package io.swcode.urbo.permission.adapter

interface UserManagementPermissionProvider {
    val permissions: List<Permission>
}

interface Permission {
    val value: String
}