package io.swcode.urbo.user.management.domain.user.userContext

import io.swcode.urbo.permission.adapter.Permission
import io.swcode.urbo.user.management.domain.user.User
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.jwt.Jwt
import java.util.*

data class UserContext(
    val id: UUID,
    val email: String,
    val firstName: String,
    val lastName: String,
    val roles: List<String>,
    val permissions: List<Permission>
) {

    fun hasPermission(permission: Permission): Boolean {
        return permissions.map { it.value }.contains(permission.value)
    }

    companion object {
        fun findCurrentUserId(): UUID? {
            val authentication = SecurityContextHolder.getContext().authentication
            val principal = authentication?.principal as? Jwt
            return principal?.getClaimAsString("sub")?.let { UUID.fromString(it) }
        }

        fun of(user: User): UserContext {
            return UserContext(
                user.id,
                user.email,
                user.firstName,
                user.lastName,
                user.roles.map { it.name },
                user.getPermissions()
            )
        }
    }
}