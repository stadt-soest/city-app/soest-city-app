package io.swcode.urbo.user.management.domain.user

import io.swcode.urbo.permission.adapter.Permission
import io.swcode.urbo.user.management.domain.role.Role
import jakarta.persistence.*
import java.util.*

@Entity
@Table(name = "users")
data class User(
    @Id
    val id: UUID,
    var firstName: String,
    var lastName: String,

    @Column(unique = true)
    var email: String,

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "user_roles",
        joinColumns = [JoinColumn(name = "user_id")],
        inverseJoinColumns = [JoinColumn(name = "role_id")]
    )
    var roles: List<Role> = listOf()
) {
    fun update(firstName: String, lastName: String, roles: List<Role>) {
        this.firstName = firstName
        this.lastName = lastName
        this.roles = roles
    }

    fun getPermissions(): List<Permission> {
        return roles.flatMap { it.permissions }
    }
}