package io.swcode.urbo.user.management.application.role

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import io.swcode.urbo.common.error.UrboException
import io.swcode.urbo.user.management.domain.PermissionService
import org.springframework.stereotype.Component

@Component
class PermissionDeserializer(private val permissionService: PermissionService) :
    JsonDeserializer<List<String>>() {

    override fun deserialize(parser: JsonParser, ctxt: DeserializationContext): List<String> {
        val permissions: List<String> = parser.readValueAs(object : TypeReference<List<String>>() {})

        val availablePermissions = permissionService.getAllPermissions().map { it.value }
        val invalidPermissions = permissions.filterNot { availablePermissions.contains(it) }

        if (invalidPermissions.isNotEmpty()) {
            throw UrboException("Invalid permissions: $invalidPermissions")
        }

        return permissions
    }
}