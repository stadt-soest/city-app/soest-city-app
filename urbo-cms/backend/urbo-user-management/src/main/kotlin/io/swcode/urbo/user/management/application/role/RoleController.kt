package io.swcode.urbo.user.management.application.role

import io.swcode.urbo.user.management.domain.PermissionService
import io.swcode.urbo.user.management.domain.role.RoleService
import jakarta.validation.Valid
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/v1")
@PreAuthorize("hasAuthority(T(io.swcode.urbo.user.management.application.UserManagementPermission).ADMINISTRATION)")
class RoleController(private val roleService: RoleService, private val permissionService: PermissionService) {

    @PostMapping("/roles")
    fun createRole(@RequestBody @Valid request: RoleCreateDto): ResponseEntity<RoleResponseDto> {
        roleService.createRole(RoleCreateDto.toEntity(request)).also {
            return ResponseEntity.ok(RoleResponseDto.fromDomain(it))
        }
    }

    @PutMapping("/roles/{id}")
    fun updateRole(
        @PathVariable id: UUID,
        @RequestBody @Valid request: RoleUpdateDto
    ): ResponseEntity<RoleResponseDto> {
        roleService.updateRole(id, request.name, request.permissions).let {
            return ResponseEntity.ok(RoleResponseDto.fromDomain(it))
        }
    }

    @DeleteMapping("/roles/{id}")
    fun deleteRole(@PathVariable id: UUID): ResponseEntity<Unit> {
        roleService.deleteRole(id).let {
            return ResponseEntity.noContent().build()
        }
    }

    @GetMapping("/roles/{id}")
    fun getRole(@PathVariable id: UUID): ResponseEntity<RoleResponseDto> {
        roleService.getRoleById(id).let {
            return ResponseEntity.ok(RoleResponseDto.fromDomain(it))
        }
    }

    @GetMapping("/roles")
    fun getAllRoles(): ResponseEntity<List<RoleResponseDto>> {
        roleService.getAllRoles().let { roles ->
            return ResponseEntity.ok(roles.map { RoleResponseDto.fromDomain(it) })
        }
    }

    @GetMapping("/permissions")
    fun getPermissions(): ResponseEntity<List<String>> {
        return ResponseEntity.ok(permissionService.getAllPermissions().map { it.value })
    }
}