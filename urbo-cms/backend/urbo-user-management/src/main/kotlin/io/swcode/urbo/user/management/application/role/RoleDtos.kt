package io.swcode.urbo.user.management.application.role

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import io.swcode.urbo.user.management.domain.role.RolePermission
import io.swcode.urbo.user.management.domain.role.Role
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Size
import java.util.*

data class RoleCreateDto(
    @field:NotBlank(message = "Role name must not be blank")
    @field:Size(min = 3, max = 50, message = "Role name must be between 3 and 50 characters")
    val name: String,
    @JsonDeserialize(using = PermissionDeserializer::class)
    val permissions: List<String>
) {
    companion object {
        fun toEntity(roleCreateDto: RoleCreateDto): Role {
            return Role(
                UUID.randomUUID(),
                roleCreateDto.name,
                roleCreateDto.permissions.map { RolePermission(value = it) }
            )
        }
    }
}

data class RoleUpdateDto(
    @field:NotBlank(message = "Role name must not be blank")
    @field:Size(min = 3, max = 50, message = "Role name must be between 3 and 50 characters")
    val name: String,
    @JsonDeserialize(using = PermissionDeserializer::class)
    val permissions: List<String>
)

data class RoleResponseDto(
    val id: UUID,
    val name: String,
    val permissions: List<String>
) {
    companion object {
        fun fromDomain(role: Role): RoleResponseDto {
            return RoleResponseDto(
                id = role.id,
                name = role.name,
                permissions = role.permissions.map { it.value }
            )
        }
    }
}