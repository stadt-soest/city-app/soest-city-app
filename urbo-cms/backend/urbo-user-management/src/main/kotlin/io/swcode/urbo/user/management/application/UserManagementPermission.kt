package io.swcode.urbo.user.management.application

import io.swcode.urbo.permission.adapter.Permission

enum class UserManagementPermission(override val value: String) : Permission {
    ADMINISTRATION("ADMINISTRATION");

    companion object {
        fun getAll(): List<UserManagementPermission> = entries
    }
}