package io.swcode.urbo.user.management.domain.role

import jakarta.persistence.*
import java.util.*

@Entity
@Table(name = "roles")
data class Role(
    @Id
    val id: UUID,

    @Column(unique = true, nullable = false)
    var name: String,

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "role_permissions", joinColumns = [JoinColumn(name = "role_id")])
    @AttributeOverride(
        name = "value",
        column = Column(name = "permission")
    )
    var permissions: List<RolePermission> = mutableListOf()
)