package io.swcode.urbo.user.management.domain.user.userContext

fun interface UserContextService {
    fun findUserContext(): UserContext?
}