package io.swcode.urbo.user.management.domain.role

import io.swcode.urbo.permission.adapter.Permission
import jakarta.persistence.Embeddable

@Embeddable
data class RolePermission(override val value: String) : Permission {
    companion object {
        fun fromPermission(value: Permission): RolePermission {
            return RolePermission(value.value)
        }
    }
}