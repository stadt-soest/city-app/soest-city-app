package io.swcode.urbo.user.management.domain.role

import io.swcode.urbo.common.error.ErrorType
import io.swcode.urbo.common.error.UrboException
import org.springframework.stereotype.Service
import java.util.*

@Service
class RoleService(
    private val roleRepository: RoleRepository,
) {

    fun createRole(role: Role): Role {
        return roleRepository.save(role)
    }

    fun updateRole(roleId: UUID, name: String, permissions: List<String>): Role {
        roleRepository.findById(roleId).orElseThrow { UrboException(ROLE_NOT_FOUND, ErrorType.RESOURCE_NOT_FOUND) }
            .let { role ->
                role.name = name
                role.permissions = permissions.map { RolePermission(it) }
                return roleRepository.save(role)
            }
    }

    fun deleteRole(roleId: UUID) {
        roleRepository.findById(roleId).orElseThrow { UrboException(ROLE_NOT_FOUND, ErrorType.RESOURCE_NOT_FOUND) }
            .let {
                roleRepository.delete(it)
            }
    }

    fun getRoleById(roleId: UUID): Role {
        return roleRepository.findById(roleId)
            .orElseThrow { UrboException(ROLE_NOT_FOUND, ErrorType.RESOURCE_NOT_FOUND) }
    }

    fun getAllRoles(): List<Role> {
        return roleRepository.findAll()
    }

    companion object {
        const val ROLE_NOT_FOUND = "Role not found"
    }
}