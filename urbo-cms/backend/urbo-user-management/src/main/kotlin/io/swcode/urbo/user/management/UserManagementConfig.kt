package io.swcode.urbo.user.management

import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@Configuration
@ComponentScan(basePackages = ["io.swcode.urbo.user.management"])
@EntityScan(basePackages = ["io.swcode.urbo.user.management"])
@EnableJpaRepositories(basePackages = ["io.swcode.urbo.user.management"])
class UserManagementConfig
