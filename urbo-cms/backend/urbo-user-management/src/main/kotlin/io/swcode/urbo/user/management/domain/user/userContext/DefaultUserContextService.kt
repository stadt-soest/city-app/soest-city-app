package io.swcode.urbo.user.management.domain.user.userContext

import io.swcode.urbo.common.error.UrboException
import io.swcode.urbo.user.management.domain.user.UserService
import org.springframework.stereotype.Service

@Service
class DefaultUserContextService(private val userService: UserService) : UserContextService {
    override fun findUserContext(): UserContext? {
        val currentUserId = UserContext.findCurrentUserId() ?: return null
        val user = userService.getUser(currentUserId)
        return UserContext.of(user)
    }

    fun getUserContext(): UserContext {
        return findUserContext() ?: throw UrboException("User context not found")
    }
}