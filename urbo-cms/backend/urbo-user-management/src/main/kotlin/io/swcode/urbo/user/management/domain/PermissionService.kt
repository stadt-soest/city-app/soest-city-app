package io.swcode.urbo.user.management.domain

import io.swcode.urbo.permission.adapter.UserManagementPermissionProvider
import io.swcode.urbo.common.error.UrboException
import io.swcode.urbo.permission.adapter.Permission
import io.swcode.urbo.user.management.application.UserManagementPermission
import io.swcode.urbo.user.management.domain.role.RolePermission
import org.springframework.stereotype.Service

@Service
class PermissionService(private val permissionProviders: List<UserManagementPermissionProvider>) {

    private val permissions: List<Permission> by lazy {
        UserManagementPermission.getAll() + permissionProviders.flatMap { it.permissions }
    }

    init {
        validatePermissions(permissions)
    }

    fun getAllPermissions(): List<RolePermission> = permissions.map(RolePermission::fromPermission)

    private fun validatePermissions(permissions: List<Permission>) {
        val duplicateValues = permissions.groupingBy { it.value }
            .eachCount()
            .filterValues { it > 1 }
            .keys

        if (duplicateValues.isNotEmpty()) {
            throw UrboException("Duplicate permission values detected: ${duplicateValues.joinToString()}")
        }
    }
}

