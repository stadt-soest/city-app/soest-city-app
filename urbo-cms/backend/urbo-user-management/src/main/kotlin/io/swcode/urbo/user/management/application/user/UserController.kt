package io.swcode.urbo.user.management.application.user

import io.swcode.urbo.user.management.application.user.dto.UserCreateDto
import io.swcode.urbo.user.management.application.user.dto.UserResponseDto
import io.swcode.urbo.user.management.application.user.dto.UserUpdateDto
import io.swcode.urbo.user.management.domain.user.UserService
import io.swcode.urbo.user.management.domain.user.userContext.DefaultUserContextService
import jakarta.validation.Valid
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/v1")
@PreAuthorize("hasAuthority(T(io.swcode.urbo.user.management.application.UserManagementPermission).ADMINISTRATION)")
class UserController(
    private val userService: UserService,
    private val userContextService: DefaultUserContextService
) {

    @GetMapping("/users")
    fun getUsers(): ResponseEntity<List<UserResponseDto>> {
        return ResponseEntity.ok(userService.getUsers().map { UserResponseDto.fromDomain(it) })
    }

    @GetMapping("/users/{id}")
    fun getUser(@PathVariable id: UUID): ResponseEntity<UserResponseDto> {
        return ResponseEntity.ok(UserResponseDto.fromDomain(userService.getUser(id)))
    }

    @PostMapping("/users")
    fun createUser(@RequestBody @Valid request: UserCreateDto): ResponseEntity<UserResponseDto> {
        userService.createUser(request).let {
            return ResponseEntity.ok(UserResponseDto.fromDomain(it))
        }
    }

    @PutMapping("/users/{id}")
    fun updateUser(
        @PathVariable id: UUID,
        @RequestBody @Valid request: UserUpdateDto
    ): ResponseEntity<UserResponseDto> {
        userService.updateUser(id, request).let {
            return ResponseEntity.ok(UserResponseDto.fromDomain(it))
        }
    }

    @DeleteMapping("/users/{id}")
    fun deleteUser(@PathVariable id: UUID): ResponseEntity<Unit> {
        userService.deleteUser(id).let {
            return ResponseEntity.noContent().build()
        }
    }

    @PostMapping("/users/{id}/reset-password")
    fun sendPasswordResetEmail(@PathVariable id: UUID): ResponseEntity<Unit> {
        userService.resetPassword(id).let {
            return ResponseEntity.noContent().build()
        }
    }

    @GetMapping("/me")
    @PreAuthorize("isAuthenticated()")
    fun getCurrentUser(): ResponseEntity<UserResponseDto> {
        return userContextService.getUserContext().let {
            ResponseEntity.ok(UserResponseDto.fromDomain(it))
        }
    }
}

