package io.swcode.urbo.user.management.domain.user

import io.swcode.urbo.common.error.ErrorType
import io.swcode.urbo.common.error.UrboException
import io.swcode.urbo.core.api.auth.AuthUserAdapter
import io.swcode.urbo.user.management.application.user.dto.UserCreateDto
import io.swcode.urbo.user.management.application.user.dto.UserUpdateDto
import io.swcode.urbo.user.management.domain.role.RoleRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.*

@Service
class UserService(
    private val userRepository: UserRepository,
    private val roleRepository: RoleRepository,
    private val authUserAdapter: AuthUserAdapter
) {

    fun getUsers(): List<User> {
        return userRepository.findAll()
    }

    fun getUser(id: UUID): User {
        return userRepository.findByIdOrNull(id) ?: throw UrboException(
            "User id $id not found",
            ErrorType.RESOURCE_NOT_FOUND
        )
    }

    fun createUser(userCreateDto: UserCreateDto): User {
        authUserAdapter.createUser(userCreateDto.email, userCreateDto.firstName, userCreateDto.lastName).let {
            return userRepository.save(
                User(
                    id = it.id,
                    firstName = userCreateDto.firstName,
                    lastName = userCreateDto.lastName,
                    email = userCreateDto.email,
                    roles = roleRepository.findByNameIn(userCreateDto.roles)
                )
            )
        }
    }

    fun updateUser(userId: UUID, userUpdateDto: UserUpdateDto): User {
        return userRepository.findById(userId).orElseThrow {
            throw UrboException("user id $userId not found", ErrorType.RESOURCE_NOT_FOUND)
        }.also {
            authUserAdapter.updateUser(
                it.id.toString(),
                userUpdateDto.firstName,
                userUpdateDto.lastName,
            )
        }.apply {
            this.update(
                userUpdateDto.firstName,
                userUpdateDto.lastName,
                roleRepository.findByNameIn(userUpdateDto.roles)
            )
        }.let {
            userRepository.save(it)
        }
    }

    fun deleteUser(userId: UUID) {
        userRepository.findById(userId).orElseThrow {
            throw UrboException("user id $userId not found", ErrorType.RESOURCE_NOT_FOUND)
        }.also {
            authUserAdapter.deleteUserById(it.id.toString())
        }.let {
            userRepository.delete(it)
        }
    }

    fun resetPassword(id: UUID) {
        userRepository.findById(id).orElseThrow {
            throw UrboException("user id $id not found", ErrorType.RESOURCE_NOT_FOUND)
        }.also {
            authUserAdapter.resetPasswordByEmail(it.email)
        }
    }
}