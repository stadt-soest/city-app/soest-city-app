package io.swcode.urbo.user.management.application.user.dto

import io.swcode.urbo.user.management.domain.user.User
import io.swcode.urbo.user.management.domain.user.userContext.UserContext
import java.util.*

data class UserResponseDto(
    val id: UUID,
    val firstName: String,
    val lastName: String,
    val email: String,
    val roles: List<String>,
    val permissions: List<String>,
) {
    companion object {
        fun fromDomain(user: User): UserResponseDto {
            return UserResponseDto(
                user.id,
                user.firstName,
                user.lastName,
                user.email,
                user.roles.map { it.name },
                user.getPermissions().map { it.value },
            )
        }

        fun fromDomain(user: UserContext): UserResponseDto {
            return UserResponseDto(
                user.id,
                user.firstName,
                user.lastName,
                user.email,
                user.roles,
                user.permissions.map { it.value },
            )
        }
    }
}