package io.swcode.urbo.user.management.application.user.dto

import jakarta.validation.constraints.Size

data class UserCreateDto(
    @field:Size(max = 50)
    val firstName: String,
    @field:Size(max = 50)
    val lastName: String,
    @field:Size(max = 320)
    val email: String,
    val roles: List<@Size(max = 50, message = "Role name must be at most 50 characters long") String> = listOf()
)