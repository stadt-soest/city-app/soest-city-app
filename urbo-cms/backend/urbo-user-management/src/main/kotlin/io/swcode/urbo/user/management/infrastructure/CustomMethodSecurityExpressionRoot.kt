package io.swcode.urbo.user.management.infrastructure

import io.swcode.urbo.common.error.NoOpException
import io.swcode.urbo.permission.adapter.Permission
import io.swcode.urbo.user.management.domain.user.userContext.UserContextService
import org.aopalliance.intercept.MethodInvocation
import org.springframework.expression.spel.support.StandardEvaluationContext
import org.springframework.security.access.expression.SecurityExpressionRoot
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations
import org.springframework.security.authentication.AuthenticationTrustResolverImpl
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component
import java.util.function.Supplier

@Component
class CustomMethodSecurityExpressionHandler(
    private val userContextService: UserContextService
) : DefaultMethodSecurityExpressionHandler() {
    override fun createEvaluationContext(
        authentication: Supplier<Authentication>,
        invocation: MethodInvocation
    ): StandardEvaluationContext {
        val context = super.createEvaluationContext(authentication, invocation) as StandardEvaluationContext
        val customRoot = CustomMethodSecurityExpressionRoot(authentication.get(), userContextService)
        customRoot.setTrustResolver(AuthenticationTrustResolverImpl())
        customRoot.setRoleHierarchy(roleHierarchy)
        context.setRootObject(customRoot)
        return context
    }
}

class CustomMethodSecurityExpressionRoot(
    authentication: Authentication,
    private val userContextService: UserContextService
) : SecurityExpressionRoot(authentication), MethodSecurityExpressionOperations {
    fun hasAuthority(permission: Permission): Boolean =
        userContextService.findUserContext()?.hasPermission(permission) ?: false

    override fun getFilterObject(): Any? = null
    override fun getReturnObject(): Any? = null
    override fun setFilterObject(filterObject: Any?) = throw NoOpException()
    override fun setReturnObject(returnObject: Any?) = throw NoOpException()
    override fun getThis(): Any = this
}