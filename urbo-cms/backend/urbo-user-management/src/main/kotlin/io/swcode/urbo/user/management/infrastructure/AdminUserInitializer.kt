package io.swcode.urbo.user.management.infrastructure

import io.swcode.urbo.user.management.application.user.dto.UserCreateDto
import io.swcode.urbo.user.management.domain.PermissionService
import io.swcode.urbo.user.management.domain.role.Role
import io.swcode.urbo.user.management.domain.role.RoleService
import io.swcode.urbo.user.management.domain.user.UserService
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Component
import java.util.*

@Component
@ConditionalOnProperty(prefix = "admin.user", name = ["creation-enabled"], havingValue = "true")
class AdminUserInitializer(
    private val roleService: RoleService,
    private val userService: UserService,
    private val permissionService: PermissionService,
    @Value("\${admin.user.email}") private val email: String,
    @Value("\${admin.user.first-name}") private val firstName: String,
    @Value("\${admin.user.last-name}") private val lastName: String,
) : ApplicationRunner {

    override fun run(args: ApplicationArguments?) {
        if (userService.getUsers().any { it.email == email }) return

        val adminRole = roleService.getAllRoles().find { it.name == "Admin" } ?: createAdminRole()

        createUser(adminRole)
    }

    private fun createAdminRole(): Role {
        return roleService.createRole(
            Role(
                id = UUID.randomUUID(),
                name = "Admin",
                permissions = permissionService.getAllPermissions().toMutableList()
            )
        )
    }

    private fun createUser(adminRole: Role) {
        userService.createUser(
            UserCreateDto(
                firstName = firstName,
                lastName = lastName,
                email = email,
                roles = listOf(adminRole.name)
            )
        )
    }
}