openapi: 3.0.3
info:
  title: OpenAPI definition
  version: v1.0.0
servers:
  - url: 'http://localhost:8080'
    description: development url

paths:
  '/v1/roles':
    post:
      tags:
        - Role
      summary: Create a new role
      operationId: createRole
      description: Creates a new role with the specified name and permissions.
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RoleCreateDto'
      responses:
        '200':
          description: Role successfully created.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RoleResponseDto'
        '400':
          description: Bad Request
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
    get:
      tags:
        - Role
      summary: Retrieve all roles
      operationId: getRoles
      description: Returns a list of all roles.
      responses:
        '200':
          description: A list of roles.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/RoleResponseDto'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden

  '/v1/roles/{id}':
    get:
      tags:
        - Role
      summary: Retrieve a role by ID
      operationId: getRole
      description: Returns a role by its ID.
      parameters:
        - $ref: '#/components/parameters/idParam'
      responses:
        '200':
          description: Role successfully retrieved.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RoleResponseDto'
        '404':
          description: Role not found
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
    put:
      tags:
        - Role
      summary: Update an existing role
      operationId: updateRole
      description: Updates the details of an existing role.
      parameters:
        - $ref: '#/components/parameters/idParam'
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RoleUpdateDto'
      responses:
        '200':
          description: Role successfully updated.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RoleResponseDto'
        '404':
          description: Role not found
        '400':
          description: Bad Request
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
    delete:
      operationId: deleteRole
      tags:
        - Role
      summary: Delete a role
      description: Deletes an existing role by its ID.
      parameters:
        - $ref: '#/components/parameters/idParam'
      responses:
        '204':
          description: No Content
        '404':
          description: Role not found
        '401':
          description: Unauthorized
        '403':
          description: Forbidden

  /v1/permissions:
    get:
      tags:
        - Role
      summary: Fetch possible permissions
      description: Returns a list of all possible permissions that can be assigned to a user.
      responses:
        '200':
          description: A list of possible permissions.
          content:
            application/json:
              schema:
                type: array
                items:
                  type: string
        default:
          $ref: '#/components/responses/defaultErrors'
        '403':
          description: Forbidden

  /v1/users:
    get:
      tags:
        - User
      summary: Retrieve a list of users
      description: Fetch a list of all users.
      responses:
        '200':
          description: A list of users.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/UserResponseDto'
        '401':
          description: Unauthorized
        '403':
          description: Access Denied
        default:
          $ref: '#/components/responses/defaultErrors'
    post:
      tags:
        - User
      summary: Create a new user
      description: Create a new user with the provided details.
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UserCreateDto'
      responses:
        '200':
          description: User successfully created.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserResponseDto'
        '401':
          description: Unauthorized
        '403':
          description: Access Denied
        default:
          $ref: '#/components/responses/defaultErrors'

  /v1/users/{id}:
    get:
      tags:
        - User
      summary: Fetch an existing user
      parameters:
        - $ref: '#/components/parameters/idParam'
      responses:
        '200':
          description: User successfully fetched.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserResponseDto'
        '401':
          description: Unauthorized
        '403':
          description: Access Denied
        default:
          $ref: '#/components/responses/defaultErrors'
    put:
      tags:
        - User
      summary: Update an existing user
      description: Update the details of an existing user by ID.
      parameters:
        - $ref: '#/components/parameters/idParam'
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UserUpdateDto'
      responses:
        '200':
          description: User successfully updated.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserResponseDto'
        '401':
          description: Unauthorized
        '403':
          description: Access Denied
        default:
          $ref: '#/components/responses/defaultErrors'
    delete:
      tags:
        - User
      summary: Delete an existing user
      description: Delete an existing user by ID.
      parameters:
        - $ref: '#/components/parameters/idParam'
      responses:
        '204':
          description: User successfully deleted.
        '401':
          description: Unauthorized
        '403':
          description: Access Denied
        default:
          $ref: '#/components/responses/defaultErrors'

  '/v1/users/{id}/reset-password':
    post:
      tags:
        - User
      summary: Send password reset email
      description: Sends a password reset email to the user specified by the ID.
      operationId: sendPasswordResetEmail
      parameters:
        - $ref: '#/components/parameters/idParam'
      responses:
        '204':
          description: No Content - Password reset email sent successfully.
        default:
          $ref: '#/components/responses/defaultErrors'

  /v1/me:
    get:
      tags:
        - User
      summary: Get the current authenticated user's details
      description: Returns the details of the current authenticated user, including roles and permissions.
      responses:
        '200':
          description: The authenticated user's details.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserResponseDto'
        '401':
          description: Unauthorized
        default:
          $ref: '#/components/responses/defaultErrors'

components:
  parameters:
    idParam:
      name: id
      in: path
      required: true
      schema:
        type: string
        format: uuid
  responses:
    defaultErrors:
      description: "Error"
      content:
        application/problem+json:
          schema:
            $ref: '#/components/schemas/ProblemDetail'
  schemas:
    ProblemDetail:
      type: object
      properties:
        title:
          type: string
        invalidParams:
          type: array
          items:
            type: object
            properties:
              cause:
                type: string
              attribute:
                type: string
          nullable: true
        type:
          type: string
        status:
          type: integer
        detail:
          type: string
        instance:
          type: string

    RoleCreateDto:
      type: object
      required:
        - name
        - permissions
      properties:
        name:
          type: string
          description: The name of the role.
        permissions:
          type: array
          items:
            type: string

    RoleUpdateDto:
      type: object
      required:
        - name
        - permissions
      properties:
        name:
          type: string
          description: The name of the role.
        permissions:
          type: array
          items:
            type: string

    UserCreateDto:
      type: object
      required:
        - firstName
        - lastName
        - email
      properties:
        firstName:
          type: string
          maxLength: 50
        lastName:
          type: string
          maxLength: 50
        email:
          type: string
          format: email
        roles:
          type: array
          items:
            type: string

    UserUpdateDto:
      type: object
      required:
        - firstName
        - lastName
      properties:
        firstName:
          type: string
          maxLength: 50
        lastName:
          type: string
          maxLength: 50
        roles:
          type: array
          items:
            type: string

    UserResponseDto:
      type: object
      required:
        - id
        - firstName
        - lastName
        - email
        - roles
        - permissions
      properties:
        id:
          type: string
          format: uuid
        firstName:
          type: string
        lastName:
          type: string
        email:
          type: string
          format: email
        roles:
          type: array
          items:
            type: string
            example: ROLE_ADMIN
          description: A list of role names assigned to the user
        permissions:
          type: array
          items:
            type: string

    RoleResponseDto:
      type: object
      required:
        - id
        - name
        - permissions
      properties:
        id:
          type: string
          format: uuid
          description: The unique identifier of the role.
        name:
          type: string
          description: The name of the role.
        permissions:
          type: array
          items:
            type: string
          description: A list of permissions associated with the role.

