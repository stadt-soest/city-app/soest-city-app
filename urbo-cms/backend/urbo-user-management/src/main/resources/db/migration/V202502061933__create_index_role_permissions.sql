CREATE UNIQUE INDEX IF NOT EXISTS ux_role_permissions
    ON role_permissions (role_id, permission);
