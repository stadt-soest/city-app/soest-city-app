package io.swcode.urbo.user.management.infrastructure

import io.mockk.*
import io.swcode.urbo.user.management.domain.role.RolePermission
import io.swcode.urbo.user.management.domain.PermissionService
import io.swcode.urbo.user.management.domain.role.Role
import io.swcode.urbo.user.management.domain.role.RoleService
import io.swcode.urbo.user.management.domain.user.User
import io.swcode.urbo.user.management.domain.user.UserService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.boot.ApplicationArguments
import java.util.*

internal class AdminUserInitializerTest {

    private val roleService: RoleService = mockk()
    private val userService: UserService = mockk()
    private val permissionService: PermissionService = mockk()

    private val email = "admin@example.com"
    private val firstName = "Admin"
    private val lastName = "User"

    private lateinit var initializer: AdminUserInitializer

    @BeforeEach
    fun setup() {
        initializer = AdminUserInitializer(
            roleService = roleService,
            userService = userService,
            permissionService = permissionService,
            email = email,
            firstName = firstName,
            lastName = lastName,
        )
    }

    @Test
    fun `should skip creation if user already exists`() {
        val existingUser = User(
            id = UUID.randomUUID(),
            firstName = firstName,
            lastName = lastName,
            email = email,
            roles = emptyList()
        )
        every { userService.getUsers() } returns listOf(existingUser)

        initializer.run(mockk<ApplicationArguments>())

        verify(exactly = 1) { userService.getUsers() }
        verify(exactly = 0) { roleService.getAllRoles() }
        verify(exactly = 0) { userService.createUser(any()) }
    }

    @Test
    fun `should create admin role if not found and create user`() {
        val noExistingUsers = emptyList<User>()
        every { userService.getUsers() } returns noExistingUsers

        every { roleService.getAllRoles() } returns emptyList()

        val allPermissions = listOf("READ_USER", "WRITE_USER").map { RolePermission(it) }
        every { permissionService.getAllPermissions() } returns allPermissions

        val createdRole = Role(
            id = UUID.randomUUID(),
            name = "Admin",
            permissions = allPermissions.toMutableList()
        )
        every { roleService.createRole(any()) } returns createdRole

        coEvery { userService.createUser(any()) } returns User(
            roles = listOf(createdRole),
            firstName = firstName,
            id = UUID.randomUUID(),
            email = email,
            lastName = lastName
        )

        initializer.run(mockk<ApplicationArguments>())

        verify(exactly = 1) { userService.getUsers() }
        verify(exactly = 1) { roleService.getAllRoles() }

        verify(exactly = 1) {
            roleService.createRole(match {
                it.name == "Admin" &&
                        it.permissions.containsAll(allPermissions)
            })
        }

        verify(exactly = 1) {
            userService.createUser(match { userCreateDto ->
                userCreateDto.email == email &&
                        userCreateDto.firstName == firstName &&
                        userCreateDto.lastName == lastName &&
                        userCreateDto.roles.contains("Admin")
            })
        }
    }

    @Test
    fun `should use existing admin role if found`() {
        every { userService.getUsers() } returns emptyList()

        val adminRole = Role(UUID.randomUUID(), "Admin", mutableListOf(RolePermission("PERM_A")))
        every { roleService.getAllRoles() } returns listOf(adminRole)

        coEvery { userService.createUser(any()) } returns User(
            roles = listOf(adminRole),
            firstName = firstName,
            id = UUID.randomUUID(),
            email = email,
            lastName = lastName
        )

        initializer.run(mockk<ApplicationArguments>())

        verify(exactly = 1) { userService.getUsers() }
        verify(exactly = 1) { roleService.getAllRoles() }
        verify(exactly = 0) { roleService.createRole(any()) }
        verify(exactly = 1) {
            userService.createUser(match {
                it.roles.contains("Admin")
            })
        }
    }
}
