package io.swcode.urbo.user.management.domain.user

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.just
import io.mockk.runs
import io.swcode.urbo.cms.BaseTest
import io.swcode.urbo.common.error.UrboException
import io.swcode.urbo.core.api.auth.AuthUserAdapter
import io.swcode.urbo.core.api.auth.IdpUserDto
import io.swcode.urbo.user.management.application.user.dto.UserCreateDto
import io.swcode.urbo.user.management.application.user.dto.UserUpdateDto
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import io.swcode.urbo.user.management.createNewUser
import java.util.*

class UserServiceTest : BaseTest() {

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var userRepository: UserRepository

    @Autowired
    private lateinit var authUserAdapter: AuthUserAdapter

    @BeforeEach
    fun setup() {
        every { authUserAdapter.createUser(any(), any(), any()) } answers {
            IdpUserDto(UUID.randomUUID(), firstArg())
        }
        every { authUserAdapter.deleteUserById(any()) } just runs
        every { authUserAdapter.updateUser(any(), any(), any()) } just runs
        every { authUserAdapter.resetPasswordByEmail(any()) } just runs
    }

    @Test
    fun `should get users`() {
        val user = userRepository.save(createNewUser())

        val users = userService.getUsers()

        users.shouldNotBeNull()
        users shouldContainExactly listOf(user)
    }

    @Test
    fun `should create user`() {
        val userCreateDto = UserCreateDto("John", "Doe", "john.doe@example.com")
        val newUserSid = IdpUserDto(
            UUID.randomUUID(),
            "email"
        )

        every {
            authUserAdapter.createUser(
                userCreateDto.email,
                userCreateDto.firstName,
                userCreateDto.lastName
            )
        } returns newUserSid

        val createdUser = userService.createUser(userCreateDto)

        val savedUser = userRepository.findById(createdUser.id)

        savedUser.isPresent shouldBe true
        savedUser.get().shouldBe(createdUser)
    }

    @Test
    fun `should update user`() {
        val user = userRepository.save(createNewUser())
        val userUpdateDto = UserUpdateDto("John", "Smith")

        every {
            authUserAdapter.updateUser(
                user.id.toString(),
                userUpdateDto.firstName,
                userUpdateDto.lastName,
            )
        } just runs

        val updatedUser = userService.updateUser(user.id, userUpdateDto)

        val savedUser = userRepository.findById(updatedUser.id)

        savedUser.isPresent shouldBe true
        savedUser.get().firstName shouldBe "John"
        savedUser.get().lastName shouldBe "Smith"
        savedUser.get().email shouldBe "john.doe@example.com"
    }

    @Test
    fun `should throw UserNotFoundException when updating non-existing user`() {
        val nonExistentUserId = UUID.randomUUID()
        val userUpdateDto = UserUpdateDto("John", "Smith")

        shouldThrow<UrboException> {
            userService.updateUser(nonExistentUserId, userUpdateDto)
        }
    }

    @Test
    fun `should delete user`() {
        val user = userRepository.save(createNewUser())

        every { authUserAdapter.deleteUserById(user.id.toString()) } just runs


        userService.deleteUser(user.id)

        val deletedUser = userRepository.findById(user.id)

        deletedUser.isPresent shouldBe false
    }

    @Test
    fun `should throw UrboException when deleting non-existing user`() {
        val nonExistentUserId = UUID.randomUUID()

        shouldThrow<UrboException> {
            userService.deleteUser(nonExistentUserId)
        }
    }
}
