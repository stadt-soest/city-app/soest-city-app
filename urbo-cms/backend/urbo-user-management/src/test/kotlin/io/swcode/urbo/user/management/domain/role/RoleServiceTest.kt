package io.swcode.urbo.user.management.domain.role

import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import io.mockk.*
import io.swcode.urbo.common.error.UrboException
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.*

class RoleServiceTest {

    private lateinit var roleService: RoleService
    private lateinit var roleRepository: RoleRepository

    @BeforeEach
    fun setUp() {
        roleRepository = mockk()
        roleService = RoleService(roleRepository)
    }

    @Test
    fun `should create a role`() {
        val role = createNewRole()
        every { roleRepository.save(role) } returns role

        val createdRole = roleService.createRole(role)

        createdRole shouldNotBe null
        createdRole.name shouldBe role.name
        createdRole.permissions shouldBe role.permissions
        verify { roleRepository.save(role) }
    }

    @Test
    fun `should update a role`() {
        val roleId = UUID.randomUUID()
        val existingRole = createNewRole().copy(id = roleId)
        val updatedPermissions = listOf("NEWS_WRITE")
        val updatedName = "Updated Role"

        every { roleRepository.findById(roleId) } returns Optional.of(existingRole)
        every { roleRepository.save(any()) } returns existingRole.copy(
            name = updatedName,
            permissions = updatedPermissions.map { RolePermission(it) })

        val updatedRole = roleService.updateRole(roleId, updatedName, updatedPermissions)

        updatedRole shouldNotBe null
        updatedRole.name shouldBe updatedName
        updatedRole.permissions.map { it.value } shouldBe updatedPermissions
        verify { roleRepository.findById(roleId) }
        verify { roleRepository.save(any()) }
    }

    @Test
    fun `should throw exception when updating non-existent role`() {
        val roleId = UUID.randomUUID()
        every { roleRepository.findById(roleId) } returns Optional.empty()

        val exception = assertThrows<UrboException> {
            roleService.updateRole(roleId, "Non-existent", listOf())
        }

        exception.message shouldBe "Role not found"
        verify { roleRepository.findById(roleId) }
        verify(exactly = 0) { roleRepository.save(any()) }
    }

    @Test
    fun `should delete a role`() {
        val roleId = UUID.randomUUID()
        val role = createNewRole().copy(id = roleId)

        every { roleRepository.findById(roleId) } returns Optional.of(role)
        every { roleRepository.delete(role) } just runs

        roleService.deleteRole(roleId)

        verify { roleRepository.findById(roleId) }
        verify { roleRepository.delete(role) }
    }

    @Test
    fun `should throw exception when deleting non-existent role`() {
        val roleId = UUID.randomUUID()
        every { roleRepository.findById(roleId) } returns Optional.empty()

        val exception = assertThrows<UrboException> {
            roleService.deleteRole(roleId)
        }

        exception.message shouldBe "Role not found"
        verify { roleRepository.findById(roleId) }
        verify(exactly = 0) { roleRepository.delete(any()) }
    }

    @Test
    fun `should get a role by id`() {
        val roleId = UUID.randomUUID()
        val role = createNewRole().copy(id = roleId)

        every { roleRepository.findById(roleId) } returns Optional.of(role)

        val foundRole = roleService.getRoleById(roleId)

        foundRole shouldNotBe null
        foundRole.id shouldBe roleId
        verify { roleRepository.findById(roleId) }
    }

    @Test
    fun `should throw exception when getting non-existent role by id`() {
        val roleId = UUID.randomUUID()
        every { roleRepository.findById(roleId) } returns Optional.empty()

        val exception = assertThrows<UrboException> {
            roleService.getRoleById(roleId)
        }

        exception.message shouldBe "Role not found"
        verify { roleRepository.findById(roleId) }
    }

    @Test
    fun `should get all roles`() {
        val role1 = createNewRole().copy(id = UUID.randomUUID(), name = "Role1")
        val role2 = createNewRole().copy(id = UUID.randomUUID(), name = "Role2")

        every { roleRepository.findAll() } returns listOf(role1, role2)

        val roles = roleService.getAllRoles()

        roles shouldNotBe null
        roles.size shouldBe 2
        verify { roleRepository.findAll() }
    }

    private fun createNewRole(): Role {
        return Role(
            id = UUID.randomUUID(),
            name = "Admin",
            permissions = mutableListOf(RolePermission("NEWS_WRITE"))
        )
    }
}
