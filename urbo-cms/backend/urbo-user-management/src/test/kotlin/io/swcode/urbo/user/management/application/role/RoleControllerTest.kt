package io.swcode.urbo.user.management.application.role

import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.just
import io.mockk.runs
import io.mockk.verify
import io.swcode.urbo.cms.BaseControllerTest
import io.swcode.urbo.user.management.application.UserManagementPermission
import io.swcode.urbo.user.management.domain.PermissionService
import io.swcode.urbo.user.management.domain.role.RolePermission
import io.swcode.urbo.user.management.domain.role.Role
import io.swcode.urbo.user.management.domain.role.RoleService
import io.swcode.urbo.user.management.WithUserContext
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.Matchers.containsInAnyOrder
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.*

@WithUserContext(permissions = ["ADMINISTRATION"])
class RoleControllerTest : BaseControllerTest() {

    @MockkBean
    private lateinit var roleService: RoleService

    @MockkBean
    private lateinit var permissionService: PermissionService

    @BeforeEach
    fun setup() {
        every { permissionService.getAllPermissions() } returns listOf(
            *UserManagementPermission.getAll().toTypedArray(),
            "NEWS_WRITE",
            "ACCESSIBILITY_SUBMISSION_READ"
        ).map { RolePermission(it.toString()) }
    }

    @Test
    fun `should get roles`() {
        val role = createNewRole()
        every { roleService.getAllRoles() } returns listOf(role)

        mockMvc.get("/v1/roles")
            .andExpect {
                status { isOk() }
                jsonPath("$[0].id", `is`(role.id.toString()))
                jsonPath("$[0].name", `is`(role.name))
                jsonPath("$[0].permissions", containsInAnyOrder(*role.permissions.map { it.value }.toTypedArray()))
            }

        verify { roleService.getAllRoles() }
    }

    @Test
    fun `should get single role`() {
        val roleId = UUID.randomUUID()
        val role = createNewRole().copy(id = roleId)

        every { roleService.getRoleById(roleId) } returns role

        mockMvc.get("/v1/roles/{id}", roleId)
            .andExpect {
                status { isOk() }
                jsonPath("$.id", `is`(role.id.toString()))
                jsonPath("$.name", `is`(role.name))
                jsonPath("$.permissions", containsInAnyOrder(*role.permissions.map { it.value }.toTypedArray()))
            }

        verify { roleService.getRoleById(roleId) }
    }

    @Test
    fun `should create role`() {
        val roleCreateDto = RoleCreateDto("Admin", listOf("NEWS_WRITE"))
        val role = createNewRole()

        every { roleService.createRole(any()) } returns role

        mockMvc.post("/v1/roles") {
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(roleCreateDto)
        }.andExpect {
            status { isOk() }
            jsonPath("$.id", `is`(role.id.toString()))
            jsonPath("$.name", `is`("Admin"))
            jsonPath("$.permissions", `is`(listOf("NEWS_WRITE")))
        }

        verify { roleService.createRole(any()) }
    }

    @Test
    fun `should update role`() {
        val roleId = UUID.randomUUID()
        val roleUpdateDto = RoleUpdateDto(
            name = "SuperAdmin",
            permissions = listOf("NEWS_WRITE")
        )
        val updatedRole = createNewRole().copy(
            id = roleId,
            name = "SuperAdmin",
            permissions = mutableListOf(
                "NEWS_WRITE",
                "ACCESSIBILITY_SUBMISSION_READ"
            ).map { RolePermission(it) }.toMutableList()
        )

        every {
            roleService.updateRole(
                roleId,
                roleUpdateDto.name,
                roleUpdateDto.permissions
            )
        } returns updatedRole

        mockMvc.put("/v1/roles/{id}", roleId) {
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(roleUpdateDto)
        }.andExpect {
            status { isOk() }
            jsonPath("$.id", `is`(roleId.toString()))
            jsonPath("$.name", `is`("SuperAdmin"))
            jsonPath(
                "$.permissions",
                `is`(
                    listOf(
                        "NEWS_WRITE",
                        "ACCESSIBILITY_SUBMISSION_READ"
                    )
                )
            )
        }

        verify {
            roleService.updateRole(
                roleId,
                roleUpdateDto.name,
                roleUpdateDto.permissions
            )
        }
    }

    @Test
    fun `should delete role`() {
        val roleId = UUID.randomUUID()

        every { roleService.deleteRole(roleId) } just runs

        mockMvc.delete("/v1/roles/{id}", roleId)
            .andExpect { status { isNoContent() } }

        verify { roleService.deleteRole(roleId) }
    }

    @Test
    @WithUserContext
    fun `should deny access to all role endpoints without admin role`() {
        testForbiddenAccessForEndpoints(
            listOf(
                "GET" to "/v1/roles",
                "GET" to "/v1/roles/{id}",
                "POST" to "/v1/roles",
                "PUT" to "/v1/roles/{id}",
                "DELETE" to "/v1/roles/{id}"
            ),
        )
    }

    private fun createNewRole(): Role {
        return Role(
            id = UUID.randomUUID(),
            name = "Admin",
            permissions = mutableListOf(RolePermission("NEWS_WRITE"))
        )
    }

    private fun testForbiddenAccessForEndpoints(urlTemplates: List<Pair<String, String>>) {
        urlTemplates.forEach { (method, urlTemplate) ->
            val roleId = UUID.randomUUID()
            val requestBuilder = when (method) {
                "GET" -> get(urlTemplate, roleId)
                "POST" -> post(urlTemplate).contentType(MediaType.APPLICATION_JSON)
                    .content(
                        objectMapper.writeValueAsString(
                            RoleCreateDto(
                                "RoleName",
                                listOf("NEWS_WRITE")
                            )
                        )
                    )

                "PUT" -> put(urlTemplate, roleId).contentType(MediaType.APPLICATION_JSON)
                    .content(
                        objectMapper.writeValueAsString(
                            RoleUpdateDto(
                                "RoleName",
                                listOf("NEWS_WRITE")
                            )
                        )
                    )

                "DELETE" -> delete(urlTemplate, roleId)
                else -> throw IllegalArgumentException("Unsupported method: $method")
            }

            mockMvc.perform(requestBuilder)
                .andExpect {
                    status().isForbidden()
                }
        }
    }
}