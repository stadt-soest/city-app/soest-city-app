package io.swcode.urbo.user.management.application.role

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.DeserializationContext
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.swcode.urbo.common.error.UrboException
import io.swcode.urbo.user.management.domain.role.RolePermission
import io.swcode.urbo.user.management.domain.PermissionService
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class PermissionDeserializerTest {

    private val permissionService: PermissionService = mockk()
    private val deserializer = PermissionDeserializer(permissionService)

    @Test
    fun `should return successfully when all permissions are valid`() {
        val parser = mockk<JsonParser>()
        val context = mockk<DeserializationContext>()
        val providedPermissions = listOf("READ", "WRITE")
        val availablePermissions = listOf("READ", "WRITE", "DELETE").map { RolePermission(it) }

        every {
            parser.readValueAs<List<String>>(any<TypeReference<List<String>>>())
        } returns providedPermissions

        every {
            permissionService.getAllPermissions()
        } returns availablePermissions

        val result = deserializer.deserialize(parser, context)

        assertEquals(providedPermissions, result)
        verify(exactly = 1) { permissionService.getAllPermissions() }
    }

    @Test
    fun `should throw UrboException when some permissions are invalid`() {
        val parser = mockk<JsonParser>()
        val context = mockk<DeserializationContext>()
        val providedPermissions = listOf("READ", "INVALID_ONE")
        val availablePermissions = listOf("READ", "WRITE").map { RolePermission(it) }

        every {
            parser.readValueAs<List<String>>(any<TypeReference<List<String>>>())
        } returns providedPermissions

        every {
            permissionService.getAllPermissions()
        } returns availablePermissions

        val exception = assertThrows(UrboException::class.java) {
            deserializer.deserialize(parser, context)
        }
        assertTrue(exception.message!!.contains("Invalid permissions: [INVALID_ONE]"))

        verify(exactly = 1) { permissionService.getAllPermissions() }
    }
}
