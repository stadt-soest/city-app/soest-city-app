package io.swcode.urbo.user.management.application.user

import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.just
import io.mockk.runs
import io.mockk.verify
import io.swcode.urbo.cms.BaseControllerTest
import io.swcode.urbo.user.management.application.user.dto.UserCreateDto
import io.swcode.urbo.user.management.application.user.dto.UserUpdateDto
import io.swcode.urbo.user.management.domain.user.UserService
import org.hamcrest.CoreMatchers.`is`
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import io.swcode.urbo.user.management.WithUserContext
import io.swcode.urbo.user.management.createNewUser
import java.util.*

@WithUserContext(permissions = ["ADMINISTRATION"])
class UserControllerTest : BaseControllerTest() {

    @MockkBean
    private lateinit var userService: UserService

    @Test
    fun `should get users`() {
        val user = createNewUser()
        every { userService.getUsers() } returns listOf(user)

        mockMvc.get("/v1/users")
            .andExpect {
                status { isOk() }
                jsonPath("$[0].id", `is`(user.id.toString()))
                jsonPath("$[0].firstName", `is`(user.firstName))
                jsonPath("$[0].lastName", `is`(user.lastName))
                jsonPath("$[0].email", `is`(user.email))
            }

        verify { userService.getUsers() }
    }

    @Test
    fun `should get single user`() {
        val userId = UUID.randomUUID()
        val user = createNewUser().copy(id = userId)

        every { userService.getUser(userId) } returns user

        mockMvc.get("/v1/users/{id}", userId)
            .andExpect {
                status { isOk() }
                jsonPath("$.id", `is`(user.id.toString()))
                jsonPath("$.firstName", `is`(user.firstName))
                jsonPath("$.lastName", `is`(user.lastName))
                jsonPath("$.email", `is`(user.email))
            }

        verify { userService.getUser(userId) }
    }

    @Test
    fun `should create user`() {
        val userCreateDto = UserCreateDto("John", "Doe", "john.doe@example.com")
        val user = createNewUser()

        every { userService.createUser(userCreateDto) } returns user

        mockMvc.post("/v1/users") {
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(userCreateDto)
        }.andExpect {
            status { isOk() }
            jsonPath("$.id", `is`(user.id.toString()))
            jsonPath("$.firstName", `is`("John"))
            jsonPath("$.lastName", `is`("Doe"))
            jsonPath("$.email", `is`("john.doe@example.com"))
        }

        verify { userService.createUser(userCreateDto) }
    }

    @Test
    fun `should update user`() {
        val userId = UUID.randomUUID()
        val userUpdateDto = UserUpdateDto(firstName = "John", lastName = "Smith")
        val updatedUser = createNewUser().copy(id = userId)

        every { userService.updateUser(any(), any()) } returns updatedUser

        mockMvc.put("/v1/users/{id}", userId) {
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(userUpdateDto)
        }.andExpect {
            status { isOk() }
            jsonPath("$.id", `is`(userId.toString()))
            jsonPath("$.firstName", `is`("John"))
            jsonPath("$.lastName", `is`("Doe"))
            jsonPath("$.email", `is`("john.doe@example.com"))
        }

        verify {
            userService.updateUser(eq(userId), match {
                it.firstName == "John" && it.lastName == "Smith"
            })
        }
    }

    @Test
    fun `should delete user`() {
        val userId = UUID.randomUUID()

        every { userService.deleteUser(userId) } just runs

        mockMvc.delete("/v1/users/{id}", userId)
            .andExpect { status { isNoContent() } }

        verify { userService.deleteUser(userId) }
    }

    @Test
    @WithUserContext
    fun `should deny access to all user endpoints without admin role`() {
        testForbiddenAccessForEndpoints(
            listOf(
                "GET" to "/v1/users",
                "GET" to "/v1/users/{id}",
                "POST" to "/v1/users",
                "PUT" to "/v1/users/{id}",
                "DELETE" to "/v1/users/{id}"
            ),
        )
    }

    @Test
    @WithMockUser(roles = ["admin"])
    fun `should validate create user request`() {
        val invalidUserCreateDto = UserCreateDto(
            firstName = "J".repeat(51),
            lastName = "D",
            email = "invalid-email-format"
        )

        mockMvc.post("/v1/users") {
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(invalidUserCreateDto)
        }.andExpect {
            status { isBadRequest() }
        }
    }

    @Test
    @WithMockUser(roles = ["admin"])
    fun `should validate update user request`() {
        val userId = UUID.randomUUID()
        val invalidUserUpdateDto = UserUpdateDto(
            firstName = "J".repeat(51),
            lastName = "S"
        )

        every { userService.updateUser(any(), any()) } returns createNewUser()

        mockMvc.put("/v1/users/{id}", userId) {
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(invalidUserUpdateDto)
        }.andExpect {
            status { isBadRequest() }
        }
    }

    @Test
    @WithMockUser(roles = ["admin"])
    fun `should send password reset email`() {
        val userId = UUID.randomUUID()

        every { userService.resetPassword(userId) } just runs

        mockMvc.post("/v1/users/{id}/reset-password", userId)
            .andExpect {
                status { isNoContent() }
            }

        verify { userService.resetPassword(userId) }
    }

    private fun testForbiddenAccessForEndpoints(urlTemplates: List<Pair<String, String>>) {
        urlTemplates.forEach { (method, urlTemplate) ->
            val userId = UUID.randomUUID()
            val requestBuilder = when (method) {
                "GET" -> get(urlTemplate, userId)
                "POST" -> post(urlTemplate).contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(UserCreateDto("John", "Doe", "john.doe@example.com")))

                "PUT" -> put(urlTemplate, userId).contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(UserUpdateDto("John", "Smith")))

                "DELETE" -> delete(urlTemplate, userId)
                else -> throw IllegalArgumentException("Unsupported method: $method")
            }

            mockMvc.perform(requestBuilder)
                .andExpect {
                    status().isUnauthorized()
                }
        }
    }
}