package io.swcode.urbo.user.management

import com.ninjasquad.springmockk.MockkBean
import io.swcode.urbo.core.api.auth.AuthUserAdapter
import io.swcode.urbo.user.management.domain.user.userContext.UserContextService
import io.swcode.urbo.web.UrboWebConfig
import org.springframework.boot.SpringBootConfiguration
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.Primary
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity

@SpringBootConfiguration
@EnableAutoConfiguration
@EnableWebSecurity
@Import(value = [UserManagementConfig::class, UrboWebConfig::class])
@EnableMethodSecurity(prePostEnabled = true, jsr250Enabled = true)
class TestConfig {

    @MockkBean
    private lateinit var authUserAdapter: AuthUserAdapter

    @Bean
    @Primary
    fun testUserContextService(): UserContextService {
        return TestUserContextService()
    }
}