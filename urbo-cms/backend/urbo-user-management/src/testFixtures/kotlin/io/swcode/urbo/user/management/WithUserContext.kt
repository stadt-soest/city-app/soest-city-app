package io.swcode.urbo.user.management

import org.junit.jupiter.api.extension.ExtendWith

@UserContextMarker
@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
@ExtendWith(UserContextSetupExtension::class)
annotation class WithUserContext(
    val permissions: Array<String> = []
)