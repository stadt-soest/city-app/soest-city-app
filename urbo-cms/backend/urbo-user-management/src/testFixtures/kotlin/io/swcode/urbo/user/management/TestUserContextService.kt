package io.swcode.urbo.user.management

import io.swcode.urbo.user.management.domain.user.User
import io.swcode.urbo.user.management.domain.user.userContext.UserContext
import io.swcode.urbo.user.management.domain.user.userContext.UserContextService
import java.util.*

class TestUserContextService : UserContextService {
    private var testUserContext: UserContext? = null

    fun setTestUserContext(userContext: UserContext) {
        this.testUserContext = userContext
    }

    override fun findUserContext(): UserContext? {
        return testUserContext
    }
}

fun createNewUser(initializer: User.() -> Unit = {}): User {
    return User(
        id = UUID.randomUUID(),
        firstName = "John",
        lastName = "Doe",
        email = "john.doe@example.com",
    ).apply(initializer)
}