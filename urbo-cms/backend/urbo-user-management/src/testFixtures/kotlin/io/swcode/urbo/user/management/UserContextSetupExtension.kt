package io.swcode.urbo.user.management

import io.swcode.urbo.user.management.domain.role.RolePermission
import io.swcode.urbo.user.management.domain.user.userContext.UserContext
import io.swcode.urbo.user.management.domain.user.userContext.UserContextService
import org.junit.jupiter.api.extension.AfterEachCallback
import org.junit.jupiter.api.extension.BeforeEachCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.lang.reflect.Method
import java.time.Instant
import java.util.*

class UserContextSetupExtension : BeforeEachCallback, AfterEachCallback {

    override fun beforeEach(context: ExtensionContext) {
        val applicationContext = SpringExtension.getApplicationContext(context)
        val testUserContextService =
            applicationContext.getBean(UserContextService::class.java) as TestUserContextService

        val userContextAnnotation = findUserContextAnnotation(context)
            ?: throw IllegalStateException("No user context annotation found on test method or class.")

        val permissionsList = extractPermissions(userContextAnnotation)

        val userContext = UserContext(
            id = UUID.randomUUID(),
            email = "test@test.com",
            firstName = "firstName",
            lastName = "lastName",
            roles = listOf(),
            permissions = permissionsList.map { RolePermission(it) }
        )

        testUserContextService.setTestUserContext(userContext)

        val mockJwt = createMockJwt(userContext)

        val authenticationToken = JwtAuthenticationToken(
            mockJwt,
            extractAuthorities(userContext.permissions.map { it.value }),
            userContext.email
        )

        SecurityContextHolder.getContext().authentication = authenticationToken
    }

    private fun createMockJwt(userContext: UserContext): Jwt {
        return Jwt.withTokenValue("mock-token")
            .header("alg", "none")
            .issuedAt(Instant.now())
            .expiresAt(Instant.now().plusSeconds(3600))
            .claim("sub", userContext.id.toString())
            .claim("email", userContext.email)
            .claim("permissions", userContext.permissions.map { it.value })
            .build()
    }

    private fun extractAuthorities(permissions: List<String>): Collection<GrantedAuthority> {
        return permissions.map { SimpleGrantedAuthority(it) }
    }

    override fun afterEach(context: ExtensionContext) {
        SecurityContextHolder.clearContext()
    }

    private fun findUserContextAnnotation(context: ExtensionContext): Annotation? {
        context.element.orElse(null)?.annotations?.forEach { annotation ->
            if (annotation.annotationClass.java.isAnnotationPresent(UserContextMarker::class.java)) {
                return annotation
            }
        }
        context.testClass.orElse(null)?.annotations?.forEach { annotation ->
            if (annotation.annotationClass.java.isAnnotationPresent(UserContextMarker::class.java)) {
                return annotation
            }
        }
        return null
    }

    private fun extractPermissions(annotation: Annotation): List<String> {
        val permissionsMethod: Method = try {
            annotation.annotationClass.java.getMethod("permissions")
        } catch (e: NoSuchMethodException) {
            throw IllegalStateException(
                "The annotation ${annotation.annotationClass} does not have a permissions() method", e
            )
        }

        return when (val rawPermissions = permissionsMethod.invoke(annotation)) {
            is Array<*> -> {
                if (rawPermissions.isNotEmpty()) {
                    val firstElement = rawPermissions[0]
                    when (firstElement) {
                        is String -> rawPermissions.filterIsInstance<String>()
                        is Enum<*> -> rawPermissions.filterIsInstance<Enum<*>>().map { it.name }
                        else -> emptyList()
                    }
                } else {
                    emptyList()
                }
            }

            else -> emptyList()
        }
    }
}
