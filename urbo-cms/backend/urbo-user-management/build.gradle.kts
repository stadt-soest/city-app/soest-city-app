plugins {
    id("io.swcode.urbo.library-conventions")
    alias(libs.plugins.kotlin.jpa)
}

dependencies {
    runtimeOnly(libs.postgresql)

    implementation(project(":urbo-common"))
    implementation(project(":urbo-permission-adapter"))
    implementation(project(":urbo-auth-adapter"))

    implementation(libs.spring.boot.starter.data.jpa)
    implementation(libs.spring.boot.starter.web)
    implementation(libs.jackson.databind)
    implementation(libs.jackson.core)
    implementation(libs.jackson.datatype.js310)
    implementation(libs.jackson.module.kotlin)
    implementation(libs.jakarta.persistence.api)
    implementation(libs.hypersistence.utils.hibernate.get63())
    implementation(libs.spring.security.config)
    implementation(libs.spring.security.oauth2.jose)
    implementation(libs.spring.security.oauth2.resource.server)
    implementation(libs.kotlin.stdlib.jdk8)
    implementation(libs.spring.boot.starter.validation)

    testImplementation(project(":urbo-web"))
    testImplementation(testFixtures(project(":urbo-core")))
    testImplementation(libs.spring.boot.starter.test)
    testImplementation(libs.flyway.database.postgresql)
    testImplementation(libs.swagger.request.validator.mockmvc)
    testImplementation(libs.spring.security.test)
    testImplementation(libs.springmockk)
    testImplementation(libs.mockk)
    testImplementation(libs.testcontainers.postgresql)

    testFixturesImplementation(project(":urbo-permission-adapter"))
    testFixturesImplementation(libs.spring.boot.starter.test)
    testFixturesImplementation(libs.spring.security.test)
    testFixturesImplementation(libs.spring.security.oauth2.resource.server)
    testFixturesImplementation(libs.spring.security.oauth2.jose)
}