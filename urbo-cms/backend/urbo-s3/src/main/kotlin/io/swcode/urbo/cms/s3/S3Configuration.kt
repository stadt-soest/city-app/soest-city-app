package io.swcode.urbo.cms.s3

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.S3AsyncClient
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.presigner.S3Presigner
import software.amazon.awssdk.transfer.s3.S3TransferManager
import java.net.URI

@Configuration
@ComponentScan
class S3Configuration {

    @Value("\${aws.s3.endpoint}")
    private val s3Endpoint: String = ""

    @Value("\${aws.s3.accessKey}")
    private val awsAccessKeyId: String = ""

    @Value("\${aws.s3.secretKey}")
    private val awsSecretAccessKey: String = ""

    @Bean
    fun s3Client(): S3Client {
        return S3Client.builder()
            .region(Region.US_EAST_1)
            .endpointOverride(s3Endpoint.let { URI.create(it) })
            .credentialsProvider(
                StaticCredentialsProvider.create(
                    AwsBasicCredentials.create(
                        awsAccessKeyId,
                        awsSecretAccessKey
                    )
                )
            )
            .build()
    }

    @Bean
    fun s3AsyncClient(): S3AsyncClient {
        return S3AsyncClient.crtBuilder()
            .region(Region.US_EAST_1)
            .checksumValidationEnabled(false)
            .endpointOverride(s3Endpoint.let { URI.create(it) })
            .credentialsProvider(
                StaticCredentialsProvider.create(
                    AwsBasicCredentials.create(
                        awsAccessKeyId,
                        awsSecretAccessKey
                    )
                )
            )
            .build()
    }

    @Bean
    fun s3Presigner(): S3Presigner {
        return S3Presigner.builder()
            .region(Region.US_EAST_1)
            .endpointOverride(s3Endpoint.let { URI.create(it) })
            .credentialsProvider(
                StaticCredentialsProvider.create(
                    AwsBasicCredentials.create(
                        awsAccessKeyId,
                        awsSecretAccessKey
                    )
                )
            )
            .build()
    }

    @Bean
    fun s3TransferManager(s3AsyncClient: S3AsyncClient): S3TransferManager {
        return S3TransferManager.builder()
            .s3Client(s3AsyncClient)
            .build()
    }
}