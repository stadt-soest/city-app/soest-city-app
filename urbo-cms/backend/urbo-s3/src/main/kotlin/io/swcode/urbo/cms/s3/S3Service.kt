package io.swcode.urbo.cms.s3

import io.swcode.urbo.cms.s3.api.DeletingObject
import io.swcode.urbo.cms.s3.api.S3Adapter
import io.swcode.urbo.common.utils.determineContentType
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import software.amazon.awssdk.core.async.AsyncRequestBody
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest
import software.amazon.awssdk.services.s3.model.GetObjectRequest
import software.amazon.awssdk.services.s3.model.HeadBucketRequest
import software.amazon.awssdk.services.s3.model.S3Exception
import software.amazon.awssdk.services.s3.presigner.S3Presigner
import software.amazon.awssdk.services.s3.presigner.model.GetObjectPresignRequest
import software.amazon.awssdk.transfer.s3.S3TransferManager
import software.amazon.awssdk.transfer.s3.model.CompletedUpload
import java.io.InputStream
import java.net.URL
import java.time.Duration

@Service
class S3Service(
    private val s3Client: S3Client,
    private val transferManager: S3TransferManager,
    private val s3Presigner: S3Presigner
) : S3Adapter {

    @Value("\${aws.s3.bucketName}")
    lateinit var bucketName: String

    fun createBucket() {
        if (!doesBucketExist()) {
            s3Client.createBucket { it.bucket(bucketName) }
        }
    }

    fun doesBucketExist(): Boolean {
        val headBucketRequest = HeadBucketRequest.builder()
            .bucket(bucketName)
            .build()

        return try {
            s3Client.headBucket(headBucketRequest)
            true
        } catch (e: S3Exception) {
            false
        }
    }

    fun getObjectBytes(key: String): InputStream {
        val getObjectRequest = GetObjectRequest.builder()
            .bucket(bucketName)
            .key(key)
            .build()

        return s3Client.getObject(getObjectRequest)
    }

    fun uploadStream(inputStream: InputStream, fileName: String, contentType: String): CompletedUpload {
        val body = AsyncRequestBody.forBlockingInputStream(null)
        val upload = transferManager.upload { builder ->
            builder.requestBody(body)
                .putObjectRequest { req -> req.bucket(bucketName).key(fileName).contentType(contentType) }
                .build()
        }

        body.writeInputStream(inputStream)
        return upload.completionFuture().join()
    }

    override fun putS3Object(inputStream: InputStream, fileName: String) {
        createBucket()
        uploadStream(inputStream, fileName, fileName.determineContentType())
    }

    override fun deleteS3Object(deletingObject: DeletingObject) {
        val deleteObjectRequest = DeleteObjectRequest.builder()
            .bucket(bucketName)
            .key(deletingObject.key)
            .build()

        s3Client.deleteObject(deleteObjectRequest)
    }

    override fun requestSignedDownloadUrl(key: String, duration: Duration): URL {
        val getObjectRequest = GetObjectRequest.builder()
            .bucket(bucketName)
            .key(key)
            .build()

        val presignRequest = GetObjectPresignRequest.builder()
            .signatureDuration(duration)
            .getObjectRequest(getObjectRequest)
            .build()

        return s3Presigner.presignGetObject(presignRequest).url()
    }
}
