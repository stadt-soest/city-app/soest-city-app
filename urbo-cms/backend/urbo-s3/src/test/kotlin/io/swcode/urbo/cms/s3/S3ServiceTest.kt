package io.swcode.urbo.cms.s3

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe
import io.swcode.urbo.cms.s3.api.DeletingObject
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.testcontainers.containers.localstack.LocalStackContainer
import org.testcontainers.utility.DockerImageName
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.time.Duration

@ContextConfiguration
@Import(S3Configuration::class)
@ExtendWith(SpringExtension::class)
class S3ServiceTest {

    @Autowired
    private lateinit var s3StorageService: S3Service

    @BeforeEach
    fun setup() {
        s3StorageService.createBucket()
    }

    @Test
    fun `should save image to S3`() {
        val imageBytes = "test image".byteInputStream()
        val imageName = "test.jpg"

        s3StorageService.putS3Object(imageBytes, imageName)

        val retrievedImageStream = s3StorageService.getObjectBytes(imageName)
        val retrievedImageContent = BufferedReader(InputStreamReader(retrievedImageStream)).readText()

        retrievedImageContent shouldBe "test image"
    }

    @Test
    fun `should delete image from S3`() {
        val imageName = "test.jpg"
        s3StorageService.deleteS3Object(DeletingObject(imageName))

        shouldThrow<Exception> { s3StorageService.getObjectBytes(imageName) }
    }

    @Test
    fun `test does bucket exist`() {
        s3StorageService.doesBucketExist() shouldBe true
    }

    @Test
    fun `should presign url`() {
        val imageName = "test.jpg"
        val imageContent = "test image"
        s3StorageService.putS3Object(imageContent.byteInputStream(), imageName)

        val url = s3StorageService.requestSignedDownloadUrl(imageName, Duration.ofMinutes(10))

        val connection = url.openConnection() as HttpURLConnection
        connection.requestMethod = "GET"
        val responseCode = connection.responseCode

        responseCode shouldBe HttpURLConnection.HTTP_OK

        val inputStream = connection.inputStream
        val content = BufferedReader(InputStreamReader(inputStream)).readText()
        content shouldBe imageContent
    }

    companion object {
        private val localStackContainer = LocalStackContainer(DockerImageName.parse("localstack/localstack:latest"))
            .withServices(LocalStackContainer.Service.S3)
            .withReuse(true)

        @DynamicPropertySource
        @JvmStatic
        fun registerDynamicProperties(registry: DynamicPropertyRegistry) {
            localStackContainer.start()
            val endpointUrl = localStackContainer.getEndpointOverride(LocalStackContainer.Service.S3).toString()
            val bucketName = "test-bucket"
            registry.add("aws.s3.bucketName") { bucketName }
            registry.add("aws.s3.endpoint") { endpointUrl }
        }
    }
}
