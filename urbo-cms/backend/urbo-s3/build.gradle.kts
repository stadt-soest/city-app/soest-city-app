plugins {
    id("io.swcode.urbo.library-conventions")
    alias(libs.plugins.kotlin.jpa)
}

dependencies {
    implementation(project(":urbo-common"))
    implementation(project(":urbo-s3-api"))

    implementation(libs.jackson.databind)
    implementation(libs.spring.boot)
    implementation(libs.awssdk.s3)
    implementation(libs.awssdk.s3.transfermanager)
    implementation(libs.awssdk.crt)

    testImplementation(libs.mockk)
    testImplementation(libs.spring.cloud.wiremock)
    testImplementation(libs.spring.boot.starter.test)
    testImplementation(libs.testcontainers.localstack)
}