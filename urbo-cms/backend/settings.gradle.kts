rootProject.name = "urbo-cms"

plugins {
    // Apply the foojay-resolver plugin to allow automatic download of JDKs
    id("org.gradle.toolchains.foojay-resolver-convention") version "0.9.0"
    id("de.fayard.refreshVersions") version "0.60.5"
}

refreshVersions {
    rejectVersionIf {
        candidate.stabilityLevel.isLessStableThan(de.fayard.refreshVersions.core.StabilityLevel.Stable)
    }
}

include(
    "urbo-app",
    "urbo-command",
    "urbo-common",
    "urbo-core",
    "urbo-core-api",
    "urbo-hypertegrity",
    "urbo-hypertegrity-adapter",
    "urbo-image-processing-client",
    "urbo-queue",
    "urbo-s3",
    "urbo-search-meilisearch",
    "urbo-keycloak-adapter",
    "urbo-auth-adapter",
    "urbo-user-management",
    "urbo-permission-adapter",
    "urbo-web",
    "urbo-meilisearch-api",
    "urbo-s3-api",
    "urbo-command-api",
    "urbo-search-index",
    "urbo-search-index-starter"
)
