package io.swcode.urbo.cms.search.index.starter

import com.fasterxml.jackson.databind.ObjectMapper
import io.swcode.urbo.cms.search.index.*
import io.swcode.urbo.cms.search.meili.api.SearchIndexConfig
import io.swcode.urbo.cms.search.meili.api.SearchIndexerAdapter
import io.swcode.urbo.command.api.CommandBus
import io.swcode.urbo.command.api.CommandRegistry
import jakarta.annotation.PostConstruct
import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Lazy

@AutoConfiguration
@ConditionalOnBean(SearchIndexConfig::class)
class SearchIndexAutoConfiguration(
    private val commandRegistry: CommandRegistry
) {

    @Bean
    fun searchIndexService(
        @Lazy commandBus: CommandBus,
        objectMapper: ObjectMapper,
        searchIndexConfig: SearchIndexConfig,
    ) = SearchIndexService(commandBus, objectMapper, searchIndexConfig)

    @Bean
    fun searchIndexEntityListener(service: SearchIndexService) = SearchIndexEntityListener(service)

    @Bean
    fun updateSearchIndexCommandHandler(searchIndexerAdapter: SearchIndexerAdapter) =
        UpdateSearchIndexCommandHandler(searchIndexerAdapter)

    @Bean
    fun deleteSearchIndexCommandHandler(searchIndexerAdapter: SearchIndexerAdapter) =
        DeleteSearchIndexCommandHandler(searchIndexerAdapter)

    @PostConstruct
    fun registerSearchIndexCommands() {
        commandRegistry.registerCommand(UpdateSearchIndexCommand::class)
        commandRegistry.registerCommand(DeleteSearchIndexCommand::class)
    }
}