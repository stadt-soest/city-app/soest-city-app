plugins {
    id("io.swcode.urbo.library-conventions")
}

dependencies {
    implementation(libs.spring.boot.starter.web)
    api(project(":urbo-search-index"))
    implementation(project(":urbo-command-api"))
    implementation(project(":urbo-meilisearch-api"))
}