ALTER TABLE parking_area
    ADD COLUMN display_name TEXT NOT NULL default '',
    ADD COLUMN parking_type TEXT NOT NULL default 'GARAGE';

ALTER TABLE parking_area
    ALTER COLUMN parking_type TYPE VARCHAR(255) USING parking_type::VARCHAR,
    ADD CONSTRAINT chk_parking_type CHECK (parking_type IN ('GARAGE', 'PARKING_HOUSE', 'UNDERGROUND', 'PARKING_SPOT'));

CREATE TABLE parking_description
(
    parking_id UUID        NOT NULL,
    language   VARCHAR(10) NOT NULL,
    value      TEXT        NOT NULL,
    CONSTRAINT fk_parking_description_parking_id FOREIGN KEY (parking_id) REFERENCES parking_area (id),
    PRIMARY KEY (parking_id, language)
);