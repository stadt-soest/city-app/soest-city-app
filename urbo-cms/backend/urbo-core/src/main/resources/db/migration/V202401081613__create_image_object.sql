ALTER TABLE event
    ADD COLUMN image_thumbnail_width  INT,
    ADD COLUMN image_thumbnail_height INT,
    ADD COLUMN image_high_res_width    INT,
    ADD COLUMN image_high_res_height   INT;

UPDATE event
SET image_thumbnail_width  = NULL,
    image_thumbnail_height = NULL,
    image_high_res_width    = NULL,
    image_high_res_height   = NULL;

ALTER TABLE news
    ADD COLUMN image_thumbnail_width  INT,
    ADD COLUMN image_thumbnail_height INT,
    ADD COLUMN image_high_res_width    INT,
    ADD COLUMN image_high_res_height   INT;

UPDATE news
SET image_thumbnail_width  = NULL,
    image_thumbnail_height = NULL,
    image_high_res_width    = NULL,
    image_high_res_height   = NULL;

