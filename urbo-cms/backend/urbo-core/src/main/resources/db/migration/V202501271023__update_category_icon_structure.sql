ALTER TABLE category
    DROP COLUMN IF EXISTS icon;

ALTER TABLE category
    RENAME COLUMN custom_icon_filename TO icon_filename;

