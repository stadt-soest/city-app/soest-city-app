INSERT INTO waste_collection_date (id, date, waste_type, waste_collection_location_id)
SELECT gen_random_uuid(),
       date,
       'CHRISTMAS_TREE',
       waste_collection_location_id
FROM waste_collection_date
WHERE date >= '2025-01-13'
  AND date <  '2025-01-18'
  AND waste_type = 'BIOWASTE';