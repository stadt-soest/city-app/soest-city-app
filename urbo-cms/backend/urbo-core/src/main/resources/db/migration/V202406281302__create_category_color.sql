ALTER TABLE category
    ADD colors_light_theme_hex VARCHAR(7),
    ADD colors_dark_theme_hex VARCHAR(7);

ALTER TABLE category
    ADD CONSTRAINT light_theme_hex_color_constraint
        CHECK (colors_light_theme_hex IS NULL OR colors_light_theme_hex ~* '^#[a-f0-9]{6}$'),
    ADD CONSTRAINT dark_theme_hex_color_constraint
        CHECK (colors_dark_theme_hex IS NULL OR colors_dark_theme_hex ~* '^#[a-f0-9]{6}$');
