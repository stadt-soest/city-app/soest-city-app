CREATE TABLE accessibility_feedback_image_filenames
(
    accessibility_feedback_id UUID NOT NULL,
    image_filename TEXT NOT NULL,
    CONSTRAINT fk_accessibility_feedback FOREIGN KEY (accessibility_feedback_id) REFERENCES accessibility_feedback (id) ON DELETE CASCADE
);

CREATE INDEX idx_feedback_id ON accessibility_feedback_image_filenames (accessibility_feedback_id);

