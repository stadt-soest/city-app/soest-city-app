ALTER TABLE event_occurrence
    ALTER COLUMN start_at TYPE TIMESTAMP WITH TIME ZONE
        USING start_at AT TIME ZONE 'Europe/Berlin';

ALTER TABLE event_occurrence
    ALTER COLUMN end_at TYPE TIMESTAMP WITH TIME ZONE
        USING end_at AT TIME ZONE 'Europe/Berlin';