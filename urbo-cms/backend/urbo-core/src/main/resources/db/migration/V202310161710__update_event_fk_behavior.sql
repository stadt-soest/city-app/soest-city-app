ALTER TABLE event
    DROP CONSTRAINT event_super_event_id_fkey;

ALTER TABLE event
    ADD CONSTRAINT event_super_event_id_fkey FOREIGN KEY (super_event_id) REFERENCES event (id) ON DELETE SET NULL;
