ALTER TABLE citizen_service ADD COLUMN visible BOOLEAN DEFAULT true;
UPDATE citizen_service SET visible = true;
ALTER TABLE citizen_service ALTER COLUMN visible SET NOT NULL;

ALTER TABLE event_occurrence ADD COLUMN visible BOOLEAN DEFAULT true;
UPDATE event_occurrence SET visible = true;
ALTER TABLE event_occurrence ALTER COLUMN visible SET NOT NULL;

ALTER TABLE news ADD COLUMN visible BOOLEAN DEFAULT true;
UPDATE news SET visible = true;
ALTER TABLE news ALTER COLUMN visible SET NOT NULL;

ALTER TABLE poi ADD COLUMN visible BOOLEAN DEFAULT true;
UPDATE poi SET visible = true;
ALTER TABLE poi ALTER COLUMN visible SET NOT NULL;

ALTER TABLE waste_collection_location ADD COLUMN visible BOOLEAN DEFAULT true;
UPDATE waste_collection_location SET visible = true;
ALTER TABLE waste_collection_location ALTER COLUMN visible SET NOT NULL;