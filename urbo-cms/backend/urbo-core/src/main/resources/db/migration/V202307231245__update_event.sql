ALTER TABLE event
    DROP COLUMN content_url,
    DROP COLUMN alternate_name,
    DROP COLUMN name,
    DROP COLUMN area_served,
    DROP COLUMN website,
    DROP COLUMN date_created;

