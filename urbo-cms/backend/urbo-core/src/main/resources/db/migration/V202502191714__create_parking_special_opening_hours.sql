CREATE TABLE parking_special_opening_hours
(
    parking_id UUID NOT NULL,
    date       DATE NOT NULL,
    open_from  TIME WITHOUT TIME ZONE,
    open_to    TIME WITHOUT TIME ZONE,
    PRIMARY KEY (parking_id, date),
    FOREIGN KEY (parking_id) REFERENCES parking_area (id)
);