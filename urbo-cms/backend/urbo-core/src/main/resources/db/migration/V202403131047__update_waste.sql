ALTER TABLE waste RENAME TO waste_collection_location;

ALTER TABLE waste_date DROP CONSTRAINT IF EXISTS fk_waste_date_waste;

ALTER TABLE waste_date RENAME TO waste_collection_date;

ALTER TABLE waste_collection_date RENAME COLUMN waste_id TO waste_collection_location_id;

ALTER TABLE waste_collection_date ADD CONSTRAINT fk_waste_collection_date_waste_collection_location FOREIGN KEY (waste_collection_location_id) REFERENCES waste_collection_location(id);
