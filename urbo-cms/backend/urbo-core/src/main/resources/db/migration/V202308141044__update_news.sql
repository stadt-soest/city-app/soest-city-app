DROP TABLE IF EXISTS news CASCADE;
DROP TABLE IF EXISTS news_author CASCADE;
DROP TABLE IF EXISTS news_category CASCADE;
DROP TABLE IF EXISTS news_s3image CASCADE;
DROP TABLE IF EXISTS news_image CASCADE;

CREATE TABLE news
(
    id       UUID PRIMARY KEY,
    news_id  VARCHAR(255) UNIQUE,
    title    VARCHAR(255) NOT NULL,
    date     TIMESTAMP    NOT NULL,
    modified TIMESTAMP,
    content  TEXT         NOT NULL,
    excerpt  TEXT,
    link     VARCHAR(255) NOT NULL
);

CREATE TABLE news_images
(
    news_id      UUID REFERENCES news (id),
    image_source VARCHAR(255) NOT NULL,
    file_name    VARCHAR(255)
);

CREATE TABLE news_categories
(
    news_id  UUID REFERENCES news (id),
    category VARCHAR(255) NOT NULL
);

CREATE TABLE news_authors
(
    news_id UUID REFERENCES news (id),
    author  VARCHAR(255) NOT NULL
);





