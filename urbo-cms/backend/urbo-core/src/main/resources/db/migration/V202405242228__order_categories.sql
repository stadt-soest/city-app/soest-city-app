ALTER TABLE category
    ADD COLUMN "sort_order" INT DEFAULT 0;

ALTER TABLE category_group
    ADD COLUMN "sort_order" INT DEFAULT 0;

UPDATE category
SET "sort_order" = COALESCE("sort_order", 0);
UPDATE category_group
SET "sort_order" = COALESCE("sort_order", 0);

CREATE INDEX idx_category_order ON category ("sort_order");

CREATE INDEX idx_category_group_order ON category_group ("sort_order");
