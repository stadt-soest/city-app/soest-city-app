CREATE TABLE news
(
    id       UUID PRIMARY KEY NOT NULL,
    news_id  VARCHAR(255),
    title    VARCHAR(255)     NOT NULL,
    date     TIMESTAMP        NOT NULL,
    modified TIMESTAMP,
    content  TEXT             NOT NULL,
    excerpt  TEXT,
    link     VARCHAR(255)     NOT NULL
);

CREATE TABLE news_image
(
    id      UUID PRIMARY KEY NOT NULL,
    image   VARCHAR(255),
    news_id UUID,
    FOREIGN KEY (news_id) REFERENCES news (id)
);

CREATE TABLE news_s3image
(
    id      UUID PRIMARY KEY NOT NULL,
    s3image VARCHAR(255),
    news_id UUID,
    FOREIGN KEY (news_id) REFERENCES news (id)
);

CREATE TABLE news_author
(
    id      UUID PRIMARY KEY NOT NULL,
    author  VARCHAR(255)     NOT NULL,
    news_id UUID,
    FOREIGN KEY (news_id) REFERENCES news (id)
);

CREATE TABLE news_category
(
    id       UUID PRIMARY KEY NOT NULL,
    category VARCHAR(255)     NOT NULL,
    news_id  UUID,
    FOREIGN KEY (news_id) REFERENCES news (id)
);


