CREATE TABLE dead_letter_tasks
(
    id                      UUID PRIMARY KEY,
    command_id              UUID         NOT NULL,
    payload                 TEXT         NOT NULL,
    created_at              TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    failure_reason          TEXT
);

CREATE INDEX idx_dead_letter_tasks_command_id ON dead_letter_tasks (command_id);
