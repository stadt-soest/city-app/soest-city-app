CREATE TABLE citizen_service
(
    id               UUID PRIMARY KEY NOT NULL,
    citizen_id       TEXT,
    title            TEXT             NOT NULL,
    description      TEXT             NOT NULL,
    url              TEXT,
    is_info_service  BOOLEAN,
    is_external_link BOOLEAN
);


