CREATE TABLE parking_area
(
    id                  UUID PRIMARY KEY,
    external_id         TEXT NOT NULL UNIQUE,
    name                TEXT NOT NULL,
    location_address_street TEXT NOT NULL,
    location_address_postal_code TEXT NOT NULL,
    location_address_city TEXT NOT NULL,
    location_coordinates_latitude DOUBLE PRECISION NOT NULL,
    location_coordinates_longitude DOUBLE PRECISION NOT NULL,
    capacity_total INT NOT NULL,
    capacity_remaining INT NOT NULL,
    capacity_modified_at TIMESTAMP WITHOUT TIME ZONE NOT NULL
);

CREATE TABLE parking_opening_hours
(
    day             TEXT NOT NULL,
    parking_area_id UUID NOT NULL,
    open_from            TIME,
    open_to              TIME,
    FOREIGN KEY (parking_area_id) REFERENCES parking_area (id) ON DELETE CASCADE,
    UNIQUE (day, parking_area_id)
);


