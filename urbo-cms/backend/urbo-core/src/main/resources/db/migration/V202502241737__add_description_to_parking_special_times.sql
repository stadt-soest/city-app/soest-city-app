DROP TABLE IF EXISTS parking_special_opening_hours_description;
DROP TABLE IF EXISTS parking_special_opening_hours;

CREATE TABLE parking_special_opening_hours
(
    id         UUID PRIMARY KEY,
    parking_id UUID NOT NULL,
    date       DATE NOT NULL,
    open_from  TIME,
    open_to    TIME,
    CONSTRAINT fk_special_opening_hours_parking
        FOREIGN KEY (parking_id)
            REFERENCES parking_area (id)
            ON DELETE CASCADE
);

CREATE TABLE parking_special_opening_hours_description
(
    special_opening_hours_id UUID        NOT NULL,
    language                 VARCHAR(10) NOT NULL,
    value                    VARCHAR(1024) NOT NULL,
    PRIMARY KEY (special_opening_hours_id, language),
    CONSTRAINT fk_special_hours_desc
        FOREIGN KEY (special_opening_hours_id)
            REFERENCES parking_special_opening_hours (id)
            ON DELETE CASCADE
);
