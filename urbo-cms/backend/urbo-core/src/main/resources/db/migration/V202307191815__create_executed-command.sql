CREATE TABLE IF NOT EXISTS Executed_Command
(
    id          uuid PRIMARY KEY,
    created_at  timestamp NOT NULL,
    command_id  uuid      NOT NULL,
    command_name text      NOT NULL,
    payload     text      NOT NULL
);