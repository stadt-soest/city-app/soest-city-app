ALTER TABLE news ADD COLUMN highlight boolean;
UPDATE news SET highlight = FALSE;
ALTER TABLE news ALTER COLUMN highlight set NOT NULL;