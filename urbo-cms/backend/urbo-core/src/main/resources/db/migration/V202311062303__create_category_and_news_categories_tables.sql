ALTER TABLE news_categories
    RENAME TO temp_news_categories;

CREATE TABLE IF NOT EXISTS category
(
    id            UUID PRIMARY KEY,
    original_name VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS mapped_category
(
    id             UUID PRIMARY KEY,
    language       VARCHAR(255) NOT NULL,
    localized_name VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS category_mapped_category
(
    category_id        UUID NOT NULL,
    mapped_category_id UUID NOT NULL,
    PRIMARY KEY (category_id, mapped_category_id),
    FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE,
    FOREIGN KEY (mapped_category_id) REFERENCES mapped_category (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS news_categories
(
    news_id     UUID NOT NULL,
    category_id UUID NOT NULL,
    PRIMARY KEY (news_id, category_id),
    FOREIGN KEY (news_id) REFERENCES news (id) ON DELETE CASCADE,
    FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE
);

INSERT INTO category (id, original_name)
SELECT DISTINCT ON (temp.category) COALESCE(cat.id, gen_random_uuid()), temp.category
FROM temp_news_categories temp
         LEFT JOIN category cat ON temp.category = cat.original_name;

INSERT INTO news_categories (news_id, category_id)
SELECT temp.news_id, cat.id
FROM temp_news_categories temp
         JOIN category cat ON temp.category = cat.original_name;

DROP TABLE temp_news_categories;
