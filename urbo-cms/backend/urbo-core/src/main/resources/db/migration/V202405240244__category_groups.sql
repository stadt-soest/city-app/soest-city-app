CREATE TABLE category_group
(
    id UUID PRIMARY KEY,
    type TEXT
);

CREATE TABLE category_group_name
(
    category_group_id UUID         NOT NULL,
    language          VARCHAR(255) NOT NULL,
    value             VARCHAR(255) NOT NULL,
    PRIMARY KEY (category_group_id, language),
    FOREIGN KEY (category_group_id) REFERENCES category_group (id) ON DELETE CASCADE
);

ALTER TABLE category
    ADD COLUMN category_group_id UUID;

ALTER TABLE category
    ADD CONSTRAINT fk_category_group
        FOREIGN KEY (category_group_id)
            REFERENCES category_group (id) ON DELETE SET NULL;
