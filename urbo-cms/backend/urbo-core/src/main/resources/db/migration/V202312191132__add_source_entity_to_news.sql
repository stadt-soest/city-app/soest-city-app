CREATE TABLE news_source
(
    id   UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    name VARCHAR(255) UNIQUE NOT NULL
);

INSERT INTO news_source (name)
SELECT DISTINCT source
FROM news
WHERE source IS NOT NULL;

UPDATE news
SET source = 'Stadt Soest'
WHERE source = 'Soest';
UPDATE news
SET source = 'KlinikumStadtSoest'
WHERE source = 'Klinikumstadt';

ALTER TABLE news
    ADD COLUMN source_id UUID;

UPDATE news
SET source_id = news_source.id
FROM news_source
WHERE news.source = news_source.name;

ALTER TABLE news
    DROP COLUMN source;

ALTER TABLE news
    ADD CONSTRAINT fk_news_source FOREIGN KEY (source_id) REFERENCES news_source (id);
