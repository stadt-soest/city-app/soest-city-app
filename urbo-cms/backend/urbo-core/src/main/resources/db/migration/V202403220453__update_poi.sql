ALTER TABLE poi ADD COLUMN contact_point_telephone VARCHAR(255) NULL;
ALTER TABLE poi ADD COLUMN contact_point_email VARCHAR(255) NULL;
ALTER TABLE poi ADD COLUMN contact_point_contact_person VARCHAR(255) NULL;