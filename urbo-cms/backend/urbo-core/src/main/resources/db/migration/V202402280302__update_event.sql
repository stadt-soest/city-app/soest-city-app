DROP TABLE event_categories;
DROP TABLE event;

CREATE TABLE event
(
    id                             UUID PRIMARY KEY,
    title                          TEXT      NOT NULL,
    sub_title                      TEXT,
    description                    TEXT,
    latest_date_created            TIMESTAMP NOT NULL,
    latest_start_date_date         DATE,
    latest_start_date_time         TIME,
    image_source                   TEXT,
    image_copyright_holder         TEXT,
    image_description              TEXT,
    image_thumbnail_filename       TEXT,
    image_thumbnail_width          INT,
    image_thumbnail_height         INT,
    image_high_res_filename        TEXT,
    image_high_res_width           INT,
    image_high_res_height          INT,
    location_location_name         TEXT,
    location_street_address        TEXT,
    location_postal_code           TEXT,
    location_city                  TEXT,
    location_coordinates_latitude  DOUBLE PRECISION,
    location_coordinates_longitude DOUBLE PRECISION,
    contact_point_telephone        TEXT,
    contact_point_email            TEXT,
    offer_url                      TEXT,
    offer_price                    TEXT,
    offer_availability             TEXT,
    offer_description              TEXT
);

CREATE TABLE event_occurrence
(
    id           UUID PRIMARY KEY,
    event_id     UUID      NOT NULL,
    external_id  TEXT      NOT NULL,
    status       TEXT      NOT NULL,
    start_date   DATE,
    start_time   TIME,
    end_date     DATE,
    end_time     TIME,
    date_created TIMESTAMP NOT NULL,
    source       TEXT      NOT NULL,
    FOREIGN KEY (event_id) REFERENCES event (id) ON DELETE CASCADE
);

CREATE TABLE event_categories
(
    event_id UUID NOT NULL,
    category TEXT NOT NULL,
    FOREIGN KEY (event_id) REFERENCES event (id) ON DELETE CASCADE
);

CREATE INDEX idx_event_categories_event_id ON event_categories (event_id);

ALTER TABLE event_categories
    ADD CONSTRAINT event_categories_unique UNIQUE (event_id, category);

ALTER TABLE news
    ADD COLUMN image_copyright_holder TEXT,
    ADD COLUMN image_description      TEXT;
