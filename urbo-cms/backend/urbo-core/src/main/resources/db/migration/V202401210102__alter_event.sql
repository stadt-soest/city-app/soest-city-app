DROP TABLE event_date_ranges;

ALTER TABLE event
    ADD COLUMN start_date DATE,
    ADD COLUMN start_time TIME,
    ADD COLUMN end_date DATE,
    ADD COLUMN end_time TIME,
    ADD COLUMN offer_url TEXT,
    ADD COLUMN offer_price TEXT,
    ADD COLUMN offer_availability TEXT,
    ADD COLUMN offer_description TEXT;




