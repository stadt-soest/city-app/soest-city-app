ALTER TABLE parking_area
    ADD COLUMN maximum_allowed_height TEXT;

CREATE TABLE parking_price_description
(
    parking_id UUID         NOT NULL,
    value      TEXT         NOT NULL,
    language   VARCHAR(255) NOT NULL CHECK (language IN ('EN', 'DE')),
    CONSTRAINT fk_parking_price_description_parking_area FOREIGN KEY (parking_id) REFERENCES parking_area (id) ON DELETE CASCADE
);

CREATE INDEX idx_parking_price_description_parking_id ON parking_price_description (parking_id);

ALTER TABLE parking_price_description ADD CONSTRAINT unique_parking_language UNIQUE (parking_id, language);
