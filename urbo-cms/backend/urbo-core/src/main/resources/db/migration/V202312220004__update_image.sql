ALTER table news RENAME image_file_name TO image_thumbnail_filename;
ALTER table news ADD image_high_res_filename varchar(255);

ALTER table event RENAME image_file_name TO image_thumbnail_filename;
ALTER table event ADD image_high_res_filename varchar(255);
