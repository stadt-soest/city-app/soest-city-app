CREATE TABLE waste
(
    id                  UUID PRIMARY KEY,
    waste_id TEXT NOT NULL,
    city                TEXT NOT NULL,
    street              TEXT NOT NULL
);

CREATE TABLE waste_date
(
    id                  UUID PRIMARY KEY,
    date                DATE NOT NULL,
    waste_type          TEXT NOT NULL,
    waste_id UUID,
    FOREIGN KEY (waste_id) REFERENCES waste (id) ON DELETE CASCADE
);