ALTER TABLE news
    ADD COLUMN image_source VARCHAR,
    ADD COLUMN file_name    VARCHAR,
    DROP COLUMN excerpt;

UPDATE news n
SET image_source = ni.image_source,
    file_name    = ni.file_name
FROM news_images ni
WHERE n.news_id = ni.news_id::TEXT;

DROP TABLE news_images;