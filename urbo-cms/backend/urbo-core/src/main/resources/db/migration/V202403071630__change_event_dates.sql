alter table event
    add column latest_start_at timestamp without time zone;

UPDATE event
SET latest_start_at = TO_TIMESTAMP(
        CONCAT(CAST(latest_start_date_date AS TEXT), ' ', COALESCE(CAST(latest_start_date_time AS TEXT), '00:00:00')),
        'YYYY-MM-DD HH24:MI:SS');

alter table event alter column latest_start_at set not null;
alter table event drop column latest_start_date_date;
alter table event drop column latest_start_date_time;


alter table event_occurrence
    add column start_at timestamp without time zone;

UPDATE event_occurrence
SET start_at = TO_TIMESTAMP(
        CONCAT(CAST(start_date AS TEXT), ' ', COALESCE(CAST(start_time AS TEXT), '00:00:00')),
        'YYYY-MM-DD HH24:MI:SS');

alter table event_occurrence alter column start_at set not null;
alter table event_occurrence drop column start_date;
alter table event_occurrence drop column start_time;


alter table event_occurrence
    add column end_at timestamp without time zone;

UPDATE event_occurrence
SET end_at = TO_TIMESTAMP(
        CONCAT(CAST(end_date AS TEXT), ' ', COALESCE(CAST(end_time AS TEXT), '23:59:59')),
        'YYYY-MM-DD HH24:MI:SS');

alter table event_occurrence alter column end_at set not null;
alter table event_occurrence drop column end_date;
alter table event_occurrence drop column end_time;



