CREATE TABLE event_date_ranges
(
    event_id   UUID                     NOT NULL,
    date_ranges_start_date TIMESTAMP WITH TIME ZONE NOT NULL,
    date_ranges_end_date   TIMESTAMP WITH TIME ZONE NOT NULL,
    PRIMARY KEY (event_id, date_ranges_start_date, date_ranges_end_date),
    CONSTRAINT fk_event
        FOREIGN KEY (event_id)
            REFERENCES event (id)
            ON DELETE CASCADE
);

CREATE TEMPORARY TABLE temp_event_date_pairs AS
SELECT id AS event_id, start_date, end_date
FROM event
WHERE super_event_id IS NULL
UNION
SELECT super_event_id AS event_id, start_date, end_date
FROM event
WHERE super_event_id IS NOT NULL;

INSERT INTO event_date_ranges (event_id, date_ranges_start_date, date_ranges_end_date)
SELECT event_id, start_date, end_date
FROM temp_event_date_pairs;


ALTER TABLE event
    DROP COLUMN IF EXISTS start_date,
    DROP COLUMN IF EXISTS end_date,
    DROP COLUMN IF EXISTS super_event_id;

DROP TABLE temp_event_date_pairs;

ALTER TABLE event
    RENAME COLUMN address_locality TO address_address_locality;
ALTER TABLE event
    RENAME COLUMN postal_code TO address_postal_code;
ALTER TABLE event
    RENAME COLUMN street_address TO address_street_address;
ALTER TABLE event
    RENAME COLUMN address_region TO address_address_region;

ALTER TABLE event
    RENAME COLUMN lat TO coordinates_latitude;
ALTER TABLE event
    RENAME COLUMN long TO coordinates_longitude;

ALTER TABLE event
    RENAME COLUMN telephone TO contact_point_telephone;
ALTER TABLE event
    RENAME COLUMN email TO contact_point_email;

ALTER TABLE event
    RENAME COLUMN file_name TO image_file_name;

ALTER table news
    RENAME COLUMN file_name TO image_file_name;



