UPDATE event
SET super_event_id = NULL
FROM event AS super
WHERE event.super_event_id = super.id
  AND event.event_id = super.event_id;