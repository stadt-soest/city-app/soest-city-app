DROP TABLE IF EXISTS event CASCADE;
DROP TABLE IF EXISTS event_categories CASCADE;
DROP TABLE IF EXISTS event_address CASCADE;
DROP TABLE IF EXISTS event_coordinates CASCADE;
DROP TABLE IF EXISTS event_contact_point CASCADE;
DROP TABLE IF EXISTS event_category CASCADE;

CREATE TABLE event
(
    id               UUID PRIMARY KEY,
    event_id         VARCHAR(255) NOT NULL UNIQUE,
    title            VARCHAR(255) NOT NULL,
    super_event_id   UUID,
    see_also         VARCHAR(255),
    location_name    VARCHAR(255),

    -- columns for EventAddress
    address_locality VARCHAR(255),
    postal_code      VARCHAR(255),
    street_address   VARCHAR(255),
    address_region   VARCHAR(255),

    -- columns for EventCoordinates
    lat              DOUBLE PRECISION,
    long             DOUBLE PRECISION,

    -- columns for EventContactPoint
    telephone        VARCHAR(255),
    email            VARCHAR(255),

    -- columns for EventImage
    image_source     VARCHAR(255),
    file_name        VARCHAR(255),

    date_modified    TIMESTAMP,
    event_status     VARCHAR(255) NOT NULL,
    slogan           VARCHAR(255),
    start_date       TIMESTAMP    NOT NULL,
    end_date         TIMESTAMP    NOT NULL,
    source           VARCHAR(255) NOT NULL,
    description      TEXT,


    FOREIGN KEY (super_event_id) REFERENCES event (id)
);

CREATE TABLE event_categories
(
    event_id UUID,
    category VARCHAR(255) NOT NULL,
    FOREIGN KEY (event_id) REFERENCES event (id)
);
