ALTER TABLE feedback_submission ADD COLUMN environment_app_settings JSONB NOT NULL DEFAULT '{}'::JSONB;
ALTER TABLE accessibility_feedback ADD COLUMN environment_app_settings JSONB NOT NULL DEFAULT '{}'::JSONB;
