CREATE TABLE event
(
    id               UUID PRIMARY KEY NOT NULL,
    event_id         VARCHAR(255),
    super_event_id   UUID,
    name             VARCHAR(255)     NOT NULL,
    alternate_name   VARCHAR(255),
    see_also         VARCHAR(255),
    location_name    VARCHAR(255),
    area_served      VARCHAR(255),
    address_id       UUID,
    coordinates_id   UUID,
    contact_point_id UUID,
    date_created     TIMESTAMP        NOT NULL,
    date_modified    TIMESTAMP,
    event_status     VARCHAR(255)     NOT NULL,
    title            VARCHAR(255)     NOT NULL,
    slogan           VARCHAR(255),
    start_date       TIMESTAMP        NOT NULL,
    end_date         TIMESTAMP        NOT NULL,
    website          VARCHAR(255),
    source           VARCHAR(255)     NOT NULL,
    content_url      VARCHAR(255),
    description      TEXT,
    image            VARCHAR(255),
    s3image          VARCHAR(255)
);

CREATE TABLE event_address
(
    id               UUID PRIMARY KEY NOT NULL,
    address_locality VARCHAR(255),
    postal_code      VARCHAR(255),
    street_address   VARCHAR(255),
    address_region   VARCHAR(255)
);

CREATE TABLE event_contact_point
(
    id        UUID PRIMARY KEY NOT NULL,
    telephone VARCHAR(255),
    email     VARCHAR(255)
);

CREATE TABLE event_coordinates
(
    id   UUID PRIMARY KEY NOT NULL,
    lat  DOUBLE PRECISION,
    long DOUBLE PRECISION
);

CREATE TABLE event_category
(
    id   UUID PRIMARY KEY NOT NULL,
    name VARCHAR(255)     NOT NULL
);

CREATE TABLE event_categories
(
    event_id    UUID,
    category_id UUID,
    PRIMARY KEY (event_id, category_id),
    FOREIGN KEY (event_id) REFERENCES event (id),
    FOREIGN KEY (category_id) REFERENCES event_category (id)
);

ALTER TABLE event
    ADD FOREIGN KEY (super_event_id) REFERENCES event (id),
    ADD FOREIGN KEY (address_id) REFERENCES event_address (id),
    ADD FOREIGN KEY (coordinates_id) REFERENCES event_coordinates (id),
    ADD FOREIGN KEY (contact_point_id) REFERENCES event_contact_point (id);
