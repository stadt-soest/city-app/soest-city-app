CREATE TABLE feedback_form
(
    id      UUID PRIMARY KEY,
    version VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE feedback_question
(
    id               UUID PRIMARY KEY,
    feedback_form_id UUID        NOT NULL,
    question_type    VARCHAR(31) NOT NULL,
    FOREIGN KEY (feedback_form_id) REFERENCES feedback_form (id)
);

CREATE TABLE feedback_question_localized_name
(
    question_id            UUID        NOT NULL,
    question_name_language VARCHAR(10) NOT NULL,
    question_name_value    TEXT        NOT NULL,
    FOREIGN KEY (question_id) REFERENCES feedback_question (id)
);

CREATE TABLE feedback_option
(
    id          UUID PRIMARY KEY,
    question_id UUID NOT NULL,
    FOREIGN KEY (question_id) REFERENCES feedback_question (id)
);

CREATE TABLE feedback_option_localized_text
(
    option_id       UUID        NOT NULL,
    localized_texts_language        VARCHAR(10) NOT NULL,
    localized_texts_value TEXT        NOT NULL,
    FOREIGN KEY (option_id) REFERENCES feedback_option (id)
);

CREATE TABLE feedback_submission
(
    id                                   UUID PRIMARY KEY,
    submission_time                      TIMESTAMP    NOT NULL,
    deduplication_key                    VARCHAR(255) NOT NULL,
    environment_device_model_name        VARCHAR(255),
    environment_operating_system_name    VARCHAR(255),
    environment_operating_system_version VARCHAR(255),
    environment_app_version              VARCHAR(255),
    environment_app_browser_name         VARCHAR(255),
    environment_app_used_as              VARCHAR(255),
    feedback_form_id                     UUID         NOT NULL,
    FOREIGN KEY (feedback_form_id) REFERENCES feedback_form (id)
);


CREATE TABLE feedback_answer
(
    id                   UUID PRIMARY KEY,
    feedback_submission_id UUID NOT NULL,
    question_id          UUID NOT NULL,
    selected_option_id   UUID,
    free_answer_or_empty TEXT,
    FOREIGN KEY (feedback_submission_id) REFERENCES feedback_submission (id),
    FOREIGN KEY (question_id) REFERENCES feedback_question (id),
    FOREIGN KEY (selected_option_id) REFERENCES feedback_option (id)
);

CREATE UNIQUE INDEX idx_feedback_form_version ON feedback_form (version);
CREATE UNIQUE INDEX idx_feedback_submission_deduplication_key ON feedback_submission (deduplication_key, feedback_form_id);
