CREATE TABLE if not exists accessibility_feedback
(
    id                                   UUID PRIMARY KEY,
    answer                               TEXT      NOT NULL,
    environment_device_model_name        VARCHAR(255),
    environment_operating_system_name    VARCHAR(255),
    environment_operating_system_version VARCHAR(255),
    environment_app_version              VARCHAR(255),
    environment_app_browser_name         VARCHAR(255),
    environment_app_used_as              VARCHAR(255),
    created_at                           TIMESTAMP NOT NULL
);
