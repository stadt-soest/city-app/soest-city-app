create table if not exists poi
(
    id                      uuid not null primary key,
    external_id             text not null,
    name                    text not null,
    address_street          text,
    address_postal_code     text,
    address_city            text,
    coordinates_latitude    double precision,
    coordinates_longitude   double precision,
    date_modified           timestamp,
    url                     text
);

create table if not exists poi_source_category
(
    poi_id                  uuid not null,
    source_category_name    text not null,
    foreign key (source_category_name) references source_category(name) on delete cascade,
    foreign key (poi_id) references poi(id) on delete cascade
);

create unique index poi_source_category__poi_id__source_category_name__idx on poi_source_category (poi_id, source_category_name);