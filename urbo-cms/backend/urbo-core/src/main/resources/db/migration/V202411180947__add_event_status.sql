ALTER TABLE event_occurrence
    DROP COLUMN IF EXISTS status;

ALTER TABLE event_occurrence
    ADD COLUMN status VARCHAR(255) DEFAULT 'SCHEDULED' NOT NULL;

ALTER TABLE event_occurrence
    ADD CONSTRAINT check_event_status
        CHECK (status IN ('SCHEDULED', 'MOVED_ONLINE', 'RESCHEDULED', 'CANCELLED', 'POSTPONED'));
