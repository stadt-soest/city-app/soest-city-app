ALTER TABLE source_category ADD COLUMN type text;
UPDATE source_category SET type = 'NEWS';
ALTER TABLE source_category ALTER COLUMN type SET NOT NULL;

ALTER TABLE category ADD COLUMN type text;
UPDATE category SET type = 'NEWS';
ALTER TABLE category ALTER COLUMN type SET NOT NULL;

CREATE TABLE IF NOT EXISTS event_source_category
(
    event_id                UUID NOT NULL,
    source_category_name    text NOT NULL,
    FOREIGN KEY (source_category_name) REFERENCES source_category(name) ON DELETE CASCADE,
    FOREIGN KEY (event_id) REFERENCES event(id) ON DELETE CASCADE
);

CREATE UNIQUE INDEX event_source_category__event_id__source_category_name__idx ON event_source_category (event_id, source_category_name);