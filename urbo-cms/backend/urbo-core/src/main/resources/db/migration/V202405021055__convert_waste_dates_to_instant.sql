ALTER TABLE waste_collection_date ADD COLUMN tmp_date TIMESTAMPTZ;

UPDATE waste_collection_date
SET tmp_date = (date::text || 'T00:00:00')::TIMESTAMP AT TIME ZONE 'Europe/Berlin';

ALTER TABLE waste_collection_date DROP COLUMN date;

ALTER TABLE waste_collection_date RENAME COLUMN tmp_date TO date;