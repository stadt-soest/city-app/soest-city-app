DROP TABLE category_mapped_category;
DROP TABLE news_categories;
DROP TABLE category;
DROP TABLE mapped_category;

CREATE TABLE IF NOT EXISTS category
(
    id  UUID PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS category_name
(
    category_id     UUID NOT NULL,
    language        text NOT NULL,
    value           text not null,
    FOREIGN KEY (category_id) REFERENCES category(id) ON DELETE CASCADE
);

CREATE INDEX category_name__category_id__idx ON category_name(category_id);

CREATE TABLE IF NOT EXISTS source_category
(
    name text PRIMARY KEY,
    category_id UUID,
    FOREIGN KEY (category_id) REFERENCES category(id) ON DELETE SET NULL
);

CREATE UNIQUE INDEX source_category__category_id__idx ON source_category(category_id);

CREATE TABLE IF NOT EXISTS news_source_category
(
    news_id                 UUID NOT NULL,
    source_category_name    text NOT NULL,
    FOREIGN KEY (source_category_name) REFERENCES source_category(name) ON DELETE CASCADE,
    FOREIGN KEY (news_id) REFERENCES news(id) ON DELETE CASCADE
);

CREATE UNIQUE INDEX news_source_category__news_id__source_category_name__idx ON news_source_category (news_id, source_category_name);
CREATE UNIQUE INDEX news_source_category__news_id__idx ON news_source_category(news_id);
CREATE UNIQUE INDEX news_source_category__source_category_name__idx ON news_source_category(source_category_name);