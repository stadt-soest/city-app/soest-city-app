ALTER TABLE feedback_form
    ADD COLUMN new_version INT;


WITH OrderedForms AS (SELECT id, ROW_NUMBER() OVER (ORDER BY id) as row_num
                      FROM feedback_form)
UPDATE feedback_form
SET new_version = OrderedForms.row_num
FROM OrderedForms
WHERE feedback_form.id = OrderedForms.id;

ALTER TABLE feedback_form DROP COLUMN version;
ALTER TABLE feedback_form RENAME COLUMN new_version TO version;

ALTER TABLE feedback_form ADD CONSTRAINT version_unique UNIQUE (version);