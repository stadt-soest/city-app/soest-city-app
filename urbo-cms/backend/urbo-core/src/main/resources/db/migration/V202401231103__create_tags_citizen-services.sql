CREATE TABLE IF NOT EXISTS tag
(
    id UUID PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS tag_localized_names
(
    tag_id   UUID NOT NULL,
    language VARCHAR(255) NOT NULL,
    value    VARCHAR(255) NOT NULL,
    PRIMARY KEY (tag_id, language),
    FOREIGN KEY (tag_id) REFERENCES tag (id)
);

CREATE TABLE IF NOT EXISTS citizen_service_tags
(
    citizen_service_id UUID NOT NULL,
    tag_id             UUID NOT NULL,
    PRIMARY KEY (citizen_service_id, tag_id),
    FOREIGN KEY (citizen_service_id) REFERENCES citizen_service (id),
    FOREIGN KEY (tag_id) REFERENCES tag (id)
);
