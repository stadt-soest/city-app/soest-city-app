package io.swcode.urbo.cms.domain.model.waste.repository

import io.swcode.urbo.cms.domain.model.waste.WasteCollectionLocation
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import java.util.*

interface WasteCollectionLocationRepository : JpaRepository<WasteCollectionLocation, UUID>, JpaSpecificationExecutor<WasteCollectionLocation> {
    fun findByWasteId(id: String): WasteCollectionLocation?
    fun existsByWasteId(id: String): Boolean
}