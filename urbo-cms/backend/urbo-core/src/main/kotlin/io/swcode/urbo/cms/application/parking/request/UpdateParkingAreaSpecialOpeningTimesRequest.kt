package io.swcode.urbo.cms.application.parking.request

import io.swcode.urbo.cms.application.shared.image.localization.LocalizationDto
import jakarta.validation.Valid
import java.time.LocalDate
import java.time.LocalTime

data class UpdateParkingAreaSpecialOpeningTimesRequest(
    @field:Valid
    val specialOpeningTimes: List<ParkingSpecialOpeningHoursDto>
)

data class ParkingSpecialOpeningHoursDto(
    val date: LocalDate,
    val from: LocalTime,
    val to: LocalTime,

    @field:Valid
    val descriptions: List<LocalizationDto>
)