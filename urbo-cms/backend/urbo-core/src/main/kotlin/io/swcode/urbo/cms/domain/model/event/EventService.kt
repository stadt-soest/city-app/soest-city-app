package io.swcode.urbo.cms.domain.model.event

import io.swcode.urbo.cms.application.event.response.EventResponseDto
import io.swcode.urbo.cms.domain.command.DownloadEventImageCommand
import io.swcode.urbo.command.api.CommandBus
import io.swcode.urbo.common.error.ErrorType
import io.swcode.urbo.common.error.UrboException
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
class EventService(
    private val eventRepository: EventRepository,
    private val commandBus: CommandBus,
) {

    @Transactional(readOnly = true)
    fun getEvents(
        pageable: Pageable,
        categoryIds: List<UUID>,
        exclude: List<UUID>,
        status: String,
        searchKeyword: String,
    ): Page<EventResponseDto> {
        val specification = Specification.where<Event>(null)
            .and(EventSpecification.byCategoryIds(categoryIds))
            .and(EventSpecification.excludeEventIds(exclude))
            .and(EventSpecification.byStatus(status))
            .and(EventSpecification.searchByKeyword(searchKeyword))
            .and(EventSpecification.byFutureOccurrenceCondition())
        return eventRepository.findAll(specification, pageable).map { EventResponseDto.fromEntity(it) }
    }

    @Transactional(readOnly = true)
    fun getEvent(id: UUID): EventResponseDto {
        val event = eventRepository.findByIdOrNull(id) ?: throw UrboException(
            "Event not found with id: $id",
            ErrorType.RESOURCE_NOT_FOUND
        )
        return EventResponseDto.fromEntity(event)
    }

    @Transactional(readOnly = true)
    fun getEventByOccurrenceId(id: UUID): EventResponseDto {
        val event = eventRepository.findDistinctByOccurrencesId(id)
            ?: throw UrboException("Event not found with id: $id", ErrorType.RESOURCE_NOT_FOUND)
        return EventResponseDto.fromEntity(event)
    }

    @Transactional
    fun save(eventRequest: Event): Event {
        val existingEvent =
            eventRepository.findDistinctByOccurrencesExternalIdIn(eventRequest.occurrences.map { it.externalId })

        return if (existingEvent != null) {
            return eventRepository.save(existingEvent.updateFrom(eventRequest))
        } else {
            val savedEvent = eventRepository.save(eventRequest)

            if (savedEvent.image != null) {
                commandBus.submitAsyncCommand(
                    DownloadEventImageCommand(
                        eventId = savedEvent.id,
                        image = savedEvent.image!!
                    )
                )
            }
            savedEvent
        }
    }

    @Transactional
    fun deleteOccurrence(externalId: String) {
        eventRepository.findByOccurrencesExternalId(externalId)?.let { event ->
            event.removeOccurrence(externalId)
            if (event.occurrences.isEmpty()) {
                eventRepository.delete(event)
            } else {
                eventRepository.save(event)
            }
        }
    }
}
