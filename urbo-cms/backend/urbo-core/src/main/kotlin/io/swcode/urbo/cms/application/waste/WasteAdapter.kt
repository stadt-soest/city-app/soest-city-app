package io.swcode.urbo.cms.application.waste

import io.swcode.urbo.cms.domain.model.waste.WasteCollectionLocation

fun interface WasteAdapter {
    fun getWasteCollections(): List<WasteCollectionLocation>
}