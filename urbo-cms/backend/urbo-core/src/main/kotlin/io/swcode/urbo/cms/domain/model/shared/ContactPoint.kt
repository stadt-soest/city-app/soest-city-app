package io.swcode.urbo.cms.domain.model.shared

import jakarta.persistence.Embeddable

@Embeddable
data class ContactPoint(
    val telephone: String,
    val email: String,
    val contactPerson: String
)