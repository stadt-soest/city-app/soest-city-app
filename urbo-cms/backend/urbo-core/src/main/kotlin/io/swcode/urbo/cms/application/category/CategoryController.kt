package io.swcode.urbo.cms.application.category

import io.swcode.urbo.cms.domain.model.category.CategoryColors
import io.swcode.urbo.cms.domain.model.category.CategoryService
import io.swcode.urbo.cms.domain.model.category.CategoryType
import jakarta.validation.Valid
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@Validated
class CategoryController(private val categoryService: CategoryService) {

    @GetMapping("/v1/categories")
    fun getAllCategories(@RequestParam type: CategoryType): ResponseEntity<List<CategoryResponseDto>> {
        val categories = categoryService.findCategories(type).map {
            CategoryResponseDto.fromEntity(it)
        }
        return ResponseEntity.ok(categories)
    }

    @PostMapping("/v1/categories")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).CATEGORY_WRITE)")
    fun createCategory(@RequestBody @Valid request: CategoryCreateDto): ResponseEntity<CategoryResponseDto> {
        return categoryService.createCategory(
            request.localizedNames.map { it.toLocalization() },
            request.type,
            request.categoryGroupId,
        ).let {
            ResponseEntity.ok(CategoryResponseDto.fromEntity(it))
        }
    }

    @GetMapping("/v1/categories/{id}")
    fun getCategory(@PathVariable id: UUID): ResponseEntity<CategoryResponseDto> {
        return categoryService.getCategory(id).let {
            ResponseEntity.ok(CategoryResponseDto.fromEntity(it))
        }
    }

    @PutMapping("/v1/categories/{id}")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).CATEGORY_WRITE)")
    fun updateCategory(
        @PathVariable id: UUID,
        @RequestBody @Valid request: CategoryUpdateDto
    ): ResponseEntity<CategoryResponseDto> {
        return categoryService.updateCategory(
            id,
            request.localizedNames.map { it.toLocalization() },
            request.sourceCategories,
            request.categoryGroupId,
            request.filename,
            request.colors?.let { CategoryColors(it.lightThemeHexColor,it.darkThemeHexColor) },
        ).let {
            ResponseEntity.ok(CategoryResponseDto.fromEntity(it))
        }
    }

    @DeleteMapping("/v1/categories/{id}")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).CATEGORY_DELETE)")
    fun deleteCategory(@PathVariable id: UUID): ResponseEntity<Unit> {
        categoryService.deleteCategory(id)
        return ResponseEntity.noContent().build()
    }

    @GetMapping("/v1/source-categories")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).CATEGORY_READ)")
    fun getSourceCategories(
        @RequestParam type: CategoryType,
        @RequestParam(required = false) mapped: Boolean?
    ): ResponseEntity<List<String>> {
        return categoryService.getSourceCategories(type, mapped).map { it.name }.let {
            ResponseEntity.ok(it)
        }
    }

    @PatchMapping("/v1/categories/order")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).CATEGORY_WRITE)")
    fun updateCategoryOrder(@RequestBody @Valid categories: List<CategoryUpdateOrderDto>): ResponseEntity<Unit> {
        categoryService.updateCategoryOrder(categories.map { it.toEntity() })
        return ResponseEntity.noContent().build()
    }
}