package io.swcode.urbo.cms.domain.model.shared

enum class Language {
    EN,
    DE,
}
