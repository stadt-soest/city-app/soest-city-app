package io.swcode.urbo.cms.application.feed

import io.swcode.urbo.cms.domain.model.event.feed.EventFeedRepository
import io.swcode.urbo.cms.domain.model.event.feed.EventFilterCriteria
import io.swcode.urbo.cms.domain.model.event.feed.EventGroupByStartAtCriteria
import jakarta.validation.constraints.Size
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.web.PageableDefault
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.Instant
import java.util.UUID

@RestController
@Validated
class FeedController(private val eventFeedRepository: EventFeedRepository) {

    @GetMapping("/v1/feed-events")
    fun findAllEvents(
        @RequestParam("minStartAt", required = false) minStartAt: Instant?,
        @RequestParam("maxEndAt", required = false) maxEndAt: Instant?,
        @RequestParam("sourceNames", required = false, defaultValue = "") sourceNames: List<@Size(max = 100) String>,
        @RequestParam("occurrenceIds", required = false, defaultValue = "") eventOccurrenceIds: List<UUID>,
        @RequestParam("categoryIds", required = false, defaultValue = "") categoryIds: List<UUID>,
        @RequestParam("excludingEventIds", required = false, defaultValue = "") excludedEventIds: List<UUID>,
        @RequestParam("excludingOccurrenceIds", required = false, defaultValue = "") excludedOccurrenceIds: List<UUID>,
        @PageableDefault(sort = ["startAt"], direction = Sort.Direction.DESC) pageable: Pageable
    ): ResponseEntity<Page<EventFeedResponseDto>> {
        val criteria = EventFilterCriteria(
            minStartAt = minStartAt,
            maxEndAt = maxEndAt,
            sourceNames = sourceNames,
            eventOccurrenceIds = eventOccurrenceIds,
            categoryIds = categoryIds,
            excludedEventIds = excludedEventIds,
            excludedOccurrenceIds = excludedOccurrenceIds
        )

        return eventFeedRepository.findAll(pageable, criteria).map { EventFeedResponseDto.fromDomain(it) }.let {
            ResponseEntity.ok(it)
        }
    }

    @GetMapping("/v1/grouped-feed-events")
    fun findAllGroupedByStartAt(
        @RequestParam("searchTerm", required = false) @Size(max = 500) searchTerm: String?,
        @RequestParam("minStartAt", required = false) minStartAt: Instant?,
        @RequestParam("sourceNames", required = false, defaultValue = "") sourceNames: List<@Size(max = 100) String>,
        @RequestParam("eventIds", required = false, defaultValue = "") eventIds: List<UUID>,
        @RequestParam("categoryIds", required = false, defaultValue = "") categoryIds: List<UUID>,
        @RequestParam("excludingEventIds", required = false, defaultValue = "") excludedEventIds: List<UUID>,
        @RequestParam("excludingOccurrenceIds", required = false, defaultValue = "") excludedOccurrenceIds: List<UUID>,
        @PageableDefault(sort = ["startAt"], direction = Sort.Direction.DESC) pageable: Pageable
    ): ResponseEntity<Page<EventFeedResponseDto>> {
        val criteria = EventGroupByStartAtCriteria(
            searchTerm = searchTerm,
            minStartAt = minStartAt,
            sourceNames = sourceNames,
            eventIds = eventIds,
            categoryIds = categoryIds,
            excludedEventIds = excludedEventIds,
            excludedOccurrenceIds = excludedOccurrenceIds
        )

        return eventFeedRepository.findAllGroupByStartAt(pageable, criteria).map { EventFeedResponseDto.fromDomain(it) }
            .let {
                ResponseEntity.ok(it)
            }
    }
}