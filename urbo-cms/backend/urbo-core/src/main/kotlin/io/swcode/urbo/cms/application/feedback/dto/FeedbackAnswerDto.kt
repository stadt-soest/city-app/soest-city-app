package io.swcode.urbo.cms.application.feedback.dto

import io.swcode.urbo.cms.application.feedback.assembler.FeedbackAssembler
import io.swcode.urbo.cms.domain.model.feedback.response.Environment
import io.swcode.urbo.cms.domain.model.feedback.response.FeedbackAnswer
import io.swcode.urbo.cms.domain.model.feedback.response.FeedbackSubmission
import jakarta.validation.constraints.NotEmpty
import jakarta.validation.constraints.NotNull
import jakarta.validation.constraints.Size
import java.time.Instant
import java.util.*

data class FeedbackSubmissionResponseDto(
    val id: UUID,
    val createdAt: Instant,
    val deduplicationKey: String,
    val environment: EnvironmentDto,
    val answers: List<FeedbackAnswerResponseDto>
) {
    companion object {
        fun fromDomain(entity: FeedbackSubmission): FeedbackSubmissionResponseDto {
            return FeedbackSubmissionResponseDto(
                entity.id,
                entity.submissionTime.toInstant(),
                entity.deduplicationKey,
                EnvironmentDto.fromDomain(entity.environment),
                entity.answers.map {
                    FeedbackAnswerResponseDto.fromDomain(it)
                })
        }
    }
}

data class FeedbackSubmissionRequestDto(
    val version: Int?,
    @NotNull @Size(min = 1, max = 100) val deduplicationKey: String,
    @NotNull val environment: EnvironmentDto,
    @NotNull @NotEmpty val answers: List<FeedbackAnswerRequestDto>
)

data class FeedbackAnswerRequestDto(
    @NotNull val questionId: UUID,
    val selectedOptionId: UUID?,
    @Size(max = 500) val freeText: String? = null
)

data class FeedbackAnswerResponseDto(
    val question: FeedbackQuestionDto,
    val selectedOption: FeedbackOptionDto?,
    val freeText: String?
) {
    companion object {
        fun fromDomain(answer: FeedbackAnswer): FeedbackAnswerResponseDto {
            return FeedbackAnswerResponseDto(FeedbackAssembler.mapToDto(answer.question),
                answer.selectedOption?.let { FeedbackAssembler.mapOptionToDto(it) },
                answer.freeAnswerOrEmpty)
        }
    }
}

data class EnvironmentDto(
    @NotNull @Size(max = 100) val deviceModelName: String,
    @NotNull @Size(max = 100) val operatingSystemName: String,
    @NotNull @Size(max = 50) val operatingSystemVersion: String,
    @NotNull @Size(max = 500) val appSettings: String,
    @NotNull @Size(max = 100) val appVersion: String,
    @NotNull @Size(max = 100) val browserName: String,
    @NotNull @Size(max = 100) val usedAs: String
) {
    companion object {
        fun fromDomain(environment: Environment): EnvironmentDto {
            return EnvironmentDto(
                environment.deviceModelName,
                environment.operatingSystem.name,
                environment.operatingSystem.version,
                environment.appSettings,
                environment.app.version,
                environment.app.browserName,
                environment.app.usedAs,
            )
        }
    }
}
