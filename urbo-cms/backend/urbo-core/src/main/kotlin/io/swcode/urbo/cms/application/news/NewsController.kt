package io.swcode.urbo.cms.application.news

import io.swcode.urbo.cms.application.citizenService.SourceType
import io.swcode.urbo.cms.application.news.dto.CreateNewsDto
import io.swcode.urbo.cms.application.news.dto.NewsHighlightRequestDto
import io.swcode.urbo.cms.application.news.dto.NewsResponseDto
import io.swcode.urbo.cms.application.news.dto.UpdateNewsDto
import io.swcode.urbo.cms.domain.model.news.NewsService
import jakarta.validation.Valid
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Size
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.web.SortDefault
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/v1")
class NewsController(private val newsService: NewsService) {

    @PostMapping("/news")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).NEWS_WRITE)")
    fun createNews(@RequestBody @Valid createNewsDto: CreateNewsDto): ResponseEntity<NewsResponseDto> {
        newsService.createNews(createNewsDto).also {
            return ResponseEntity.ok(NewsResponseDto.fromEntity(it))
        }
    }

    @PutMapping("/news/{id}/highlight")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).NEWS_HIGHLIGHT)")
    fun highlightNews(
        @PathVariable id: UUID,
        @RequestBody @Valid updateNewsDto: NewsHighlightRequestDto
    ): ResponseEntity<NewsResponseDto> {
        newsService.highlightNews(id, updateNewsDto.highlight).also {
            return ResponseEntity.ok(NewsResponseDto.fromEntity(it))
        }
    }

    @PutMapping("/news/{id}")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).NEWS_WRITE)")
    fun updateNews(
        @PathVariable id: UUID,
        @RequestBody @Valid updateNewsDto: UpdateNewsDto
    ): ResponseEntity<NewsResponseDto> {
        newsService.updateNews(id, updateNewsDto).also {
            return ResponseEntity.ok(NewsResponseDto.fromEntity(it))
        }
    }


    @DeleteMapping("/news/{id}")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).NEWS_DELETE)")
    fun deleteNews(@PathVariable id: UUID): ResponseEntity<Unit> {
        newsService.deleteNews(id).also {
            return ResponseEntity.noContent().build()
        }
    }

    @GetMapping("/news")
    fun getAllNews(
        pageable: Pageable,
        @SortDefault(sort = ["date"], direction = Sort.Direction.DESC)
        @RequestParam(value = "categoryIds", required = false) categoryIds: List<UUID>?,
        @RequestParam(value = "sources", required = false) sources: List<@Size(min = 1, max = 50) @NotBlank String>?,
        @RequestParam(value = "exclude", required = false) exclude: List<UUID>?,
        @RequestParam(value = "highlight", required = false) highlight: Boolean?,
        @RequestParam(required = false) sourceType: SourceType?,
    ): ResponseEntity<Page<NewsResponseDto>> {
        newsService.getAllNews(pageable, categoryIds, sources, exclude, sourceType, highlight).also {
            return ResponseEntity.ok(it)
        }
    }

    @GetMapping("/news/{id}")
    fun getNews(@PathVariable id: UUID): ResponseEntity<NewsResponseDto> {
        newsService.getNews(id).also {
            return ResponseEntity.ok(it)
        }
    }

    @GetMapping("/news/search")
    fun searchNews(
        @RequestParam(value = "keyword", required = false) @Size(max = 100) keyword: String?,
        @SortDefault(sort = ["date"], direction = Sort.Direction.DESC)
        pageable: Pageable
    ): ResponseEntity<Page<NewsResponseDto>> {
        newsService.searchNews(keyword, pageable).also {
            return ResponseEntity.ok(it)
        }
    }

    @GetMapping("/news-sources")
    fun getAllNewsSources(): ResponseEntity<List<String>> {
        newsService.getAllSources().also {
            return ResponseEntity.ok(it)
        }
    }
}
