package io.swcode.urbo.cms.domain.command.handler

import io.swcode.urbo.cms.domain.command.DeleteImageCommand
import io.swcode.urbo.cms.s3.api.DeletingObject
import io.swcode.urbo.cms.s3.api.S3Adapter
import io.swcode.urbo.command.api.CommandHandler
import org.springframework.stereotype.Component

@Component
class DeleteImageCommandHandler(
    private val s3Adapter: S3Adapter
) : CommandHandler<DeleteImageCommand> {
    override fun handle(command: DeleteImageCommand) {
        sequenceOf(command.image.thumbnail?.filename, command.image.highRes?.filename)
            .filterNotNull()
            .forEach { s3Adapter.deleteS3Object(DeletingObject(key = it)) }
    }
}
