package io.swcode.urbo.cms.domain.model.category

import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface SourceCategoryRepository : JpaRepository<SourceCategory, UUID> {
    fun findAllByTypeAndNameIn(type: CategoryType, ids: List<String>): List<SourceCategory>
    fun findAllByTypeAndCategoryIsNull(type: CategoryType): List<SourceCategory>
    fun findAllByTypeAndCategoryIsNotNull(type: CategoryType): List<SourceCategory>
    fun findAllByType(type: CategoryType): List<SourceCategory>
}
