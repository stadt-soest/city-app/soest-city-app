package io.swcode.urbo.cms.application.citizenService

enum class SourceType {
    INTERNAL,
    EXTERNAL
}