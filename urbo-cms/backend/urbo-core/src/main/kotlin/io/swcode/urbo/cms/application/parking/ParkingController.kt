package io.swcode.urbo.cms.application.parking

import io.swcode.urbo.cms.application.parking.request.UpdateParkingAreaOpeningTimesRequest
import io.swcode.urbo.cms.application.parking.request.UpdateParkingAreaSpecialOpeningTimesRequest
import io.swcode.urbo.cms.application.parking.response.ParkingAreaResponseDto
import io.swcode.urbo.cms.domain.model.parking.ParkingOpeningHours
import io.swcode.urbo.cms.domain.model.parking.ParkingService
import jakarta.validation.Valid
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.time.LocalDate
import java.util.*

@RestController
@RequestMapping("/v1/parkingAreas")
class ParkingController(private val parkingService: ParkingService) {
    @GetMapping
    fun getAllParkingAreas(pageable: Pageable): Page<ParkingAreaResponseDto> {
        return parkingService.getAllParkingAreas(pageable)
    }

    @GetMapping("/{id}")
    fun getParkingArea(
        @PathVariable id: UUID,
        @RequestParam(required = false)
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
        fromDateSpecialOpeningTimes: LocalDate?
    ): ResponseEntity<ParkingAreaResponseDto> {
        val parkingArea = parkingService.getParkingArea(id, fromDateSpecialOpeningTimes)
        return ResponseEntity.ok(parkingArea)
    }

    @PutMapping("/{id}/opening-times")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).PARKING_AREA_WRITE)")
    fun updateOpeningTimes(
        @PathVariable id: UUID,
        @RequestBody @Valid request: UpdateParkingAreaOpeningTimesRequest
    ): ResponseEntity<ParkingAreaResponseDto> {
        val openingTimes = request.openingTimes.mapValues { (_, dto) ->
            ParkingOpeningHours(from = dto.from, to = dto.to)
        }
        return ResponseEntity.ok(parkingService.updateOpeningTimes(id, openingTimes))
    }

    @PutMapping("/{id}/special-opening-times")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).PARKING_AREA_WRITE)")
    fun updateSpecialOpeningTimes(
        @PathVariable id: UUID,
        @RequestBody @Valid request: UpdateParkingAreaSpecialOpeningTimesRequest
    ): ResponseEntity<ParkingAreaResponseDto> {
        val updated = parkingService.updateSpecialOpeningTimes(id, request.specialOpeningTimes)
        return ResponseEntity.ok(updated)
    }
}
