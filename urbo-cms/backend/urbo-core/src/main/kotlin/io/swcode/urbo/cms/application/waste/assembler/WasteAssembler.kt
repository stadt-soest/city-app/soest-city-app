package io.swcode.urbo.cms.application.waste.assembler

import io.swcode.urbo.cms.application.waste.response.WasteDateDTO
import io.swcode.urbo.cms.application.waste.response.WasteResponseDto
import io.swcode.urbo.cms.domain.model.waste.WasteCollectionLocation
import org.springframework.stereotype.Component

@Component
object WasteAssembler {
    fun toResponseDto(wasteCollectionLocation: WasteCollectionLocation): WasteResponseDto {
        return WasteResponseDto(
            id = wasteCollectionLocation.id,
            city = wasteCollectionLocation.city,
            street = wasteCollectionLocation.street,
            wasteDates = wasteCollectionLocation.wasteCollectionDates.map {
                WasteDateDTO(
                    id = it.id,
                    date = it.date,
                    wasteType = it.wasteType
                )
            }
        )
    }
}