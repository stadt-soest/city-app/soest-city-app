package io.swcode.urbo.cms.application.poi

import io.swcode.urbo.cms.application.poi.dto.PoiResponseDto
import io.swcode.urbo.cms.domain.model.poi.PoiService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.web.PageableDefault
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.UUID

@RestController
@Validated
class PoiController(private val poiService: PoiService) {

    @GetMapping("/v1/pois/{id}")
    fun getPoi(@PathVariable id: UUID): ResponseEntity<PoiResponseDto> {
        return poiService.findById(id)?.let {
            ResponseEntity.ok(PoiResponseDto.fromDomain(it))
        } ?: ResponseEntity.notFound().build()
    }

    @GetMapping("/v1/pois")
    fun findPois(
        @RequestParam(required = false, defaultValue = "") categoryIds: List<UUID>,
        @PageableDefault(
            size = 10,
            sort = ["dateModified"],
            direction = Sort.Direction.DESC
        ) pageable: Pageable
    ): ResponseEntity<Page<PoiResponseDto>> {
        return poiService
            .findAll(categoryIds, pageable)
            .map { PoiResponseDto.fromDomain(it) }
            .let { ResponseEntity.ok(it) }
    }
}