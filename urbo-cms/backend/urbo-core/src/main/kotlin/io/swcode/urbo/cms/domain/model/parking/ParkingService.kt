package io.swcode.urbo.cms.domain.model.parking

import io.swcode.urbo.cms.application.parking.request.ParkingSpecialOpeningHoursDto
import io.swcode.urbo.cms.application.parking.response.ParkingAreaResponseDto
import io.swcode.urbo.cms.domain.model.shared.Localization
import io.swcode.urbo.common.error.ErrorType
import io.swcode.urbo.common.error.UrboException
import jakarta.transaction.Transactional
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.time.DayOfWeek
import java.time.LocalDate
import java.util.*

@Service
@Transactional
class ParkingService(
    private val parkingAreaRepository: ParkingAreaRepository
) {

    fun save(parkingArea: ParkingArea) {
        val foundParkingArea = parkingAreaRepository.findByExternalId(parkingArea.externalId)

        if (foundParkingArea != null) {
            parkingAreaRepository.save(foundParkingArea.also { it.updateFrom(parkingArea) })
        } else {
            parkingAreaRepository.save(parkingArea)
        }
    }

    fun getAllParkingAreas(pageable: Pageable): Page<ParkingAreaResponseDto> {
        return parkingAreaRepository.findAll(pageable).map {
            ParkingAreaResponseDto.fromDomain(it)
        }
    }

    fun getParkingArea(id: UUID, fromDateSpecialOpeningTimes: LocalDate? = null): ParkingAreaResponseDto {
        val parkingArea = parkingAreaRepository.findByIdOrNull(id)
            ?: throw UrboException("ParkingArea not found with id: $id", ErrorType.RESOURCE_NOT_FOUND)

        if (fromDateSpecialOpeningTimes != null) {
            parkingArea.specialOpeningTimes = parkingArea.specialOpeningTimes
                .filterKeys { it >= fromDateSpecialOpeningTimes }
                .toMutableMap()
        }

        return ParkingAreaResponseDto.fromDomain(parkingArea)
    }

    fun updateOpeningTimes(id: UUID, openingTimes: Map<DayOfWeek, ParkingOpeningHours>): ParkingAreaResponseDto {
        val parkingArea = parkingAreaRepository.findByIdOrNull(id) ?: throw UrboException(
            "ParkingArea not found with id: $id",
            ErrorType.RESOURCE_NOT_FOUND
        )
        parkingArea.openingTimes = openingTimes.toMutableMap()
        return ParkingAreaResponseDto.fromDomain(parkingAreaRepository.save(parkingArea))
    }

    fun updateSpecialOpeningTimes(
        id: UUID,
        dtos: List<ParkingSpecialOpeningHoursDto>
    ): ParkingAreaResponseDto {
        val parkingArea = parkingAreaRepository.findByIdOrNull(id)
            ?: throw UrboException("ParkingArea not found with id: $id", ErrorType.RESOURCE_NOT_FOUND)

        val newEntries = dtos.associateBy(
            keySelector = { it.date },
            valueTransform = { dto ->
                ParkingSpecialOpeningHours(
                    date = dto.date,
                    from = dto.from,
                    to = dto.to,
                    description = dto.descriptions.map { Localization(it.value, it.language) }
                )
            }
        )

        parkingArea.updateSpecialOpeningTimes(newEntries)

        val updated = parkingAreaRepository.save(parkingArea)

        return ParkingAreaResponseDto.fromDomain(updated)
    }
}
