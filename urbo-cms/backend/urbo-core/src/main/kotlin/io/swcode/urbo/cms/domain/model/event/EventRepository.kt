package io.swcode.urbo.cms.domain.model.event

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import java.util.UUID

interface EventRepository : JpaRepository<Event, UUID>,
    JpaSpecificationExecutor<Event> {
    fun findDistinctByOccurrencesExternalIdIn(externalIds: List<String>): Event?
    fun findDistinctByOccurrencesId(occurrenceId: UUID): Event?
    fun findByOccurrencesExternalId(externalId: String): Event?
}