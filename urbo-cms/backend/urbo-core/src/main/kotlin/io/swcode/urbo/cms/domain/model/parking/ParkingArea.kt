package io.swcode.urbo.cms.domain.model.parking

import io.swcode.urbo.cms.domain.model.shared.Address
import io.swcode.urbo.cms.domain.model.shared.GeoCoordinates
import io.swcode.urbo.cms.domain.model.shared.Localization
import jakarta.persistence.*
import java.time.DayOfWeek
import java.time.Instant
import java.time.LocalDate
import java.time.LocalTime
import java.util.*

@Entity
data class ParkingArea(
    @Id
    val id: UUID,

    val externalId: String,

    @Enumerated(EnumType.STRING)
    var status: ParkingStatus,

    @Enumerated(EnumType.STRING)
    var parkingType: ParkingType,

    var name: String,

    var displayName: String,

    @Embedded
    var location: ParkingLocation,

    @Embedded
    var capacity: ParkingCapacity,

    var maximumAllowedHeight: String?,

    @ElementCollection
    @CollectionTable(name = "parking_description", joinColumns = [JoinColumn(name = "parking_id")])
    @AttributeOverrides(
        AttributeOverride(name = "language", column = Column(name = "language")),
        AttributeOverride(name = "value", column = Column(name = "value"))
    )
    var description: MutableList<Localization>,

    @ElementCollection
    @CollectionTable(name = "parking_price_description", joinColumns = [JoinColumn(name = "parking_id")])
    @AttributeOverrides(
        AttributeOverride(name = "language", column = Column(name = "language")),
        AttributeOverride(name = "value", column = Column(name = "value"))
    )
    var priceDescription: MutableList<Localization>,

    @ElementCollection(targetClass = ParkingOpeningHours::class)
    @MapKeyColumn(name = "day")
    @MapKeyEnumerated(EnumType.STRING)
    @CollectionTable(name = "parking_opening_hours")
    var openingTimes: Map<DayOfWeek, ParkingOpeningHours>,

    @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true)
    @JoinColumn(name = "parking_id", nullable = false)
    @MapKey(name = "date")
    var specialOpeningTimes: MutableMap<LocalDate, ParkingSpecialOpeningHours> = mutableMapOf()
) {
    fun updateFrom(other: ParkingArea) {
        name = other.name
        displayName = other.displayName
        location = other.location
        capacity = other.capacity
        status = other.status
        parkingType = other.parkingType
        maximumAllowedHeight = other.maximumAllowedHeight
        description = other.description
        priceDescription = other.priceDescription
    }

    fun updateSpecialOpeningTimes(newEntries: Map<LocalDate, ParkingSpecialOpeningHours>) {
        specialOpeningTimes.clear()
        specialOpeningTimes.putAll(newEntries)
    }
}

@Embeddable
data class ParkingLocation(
    @Embedded
    val address: Address,

    @Embedded
    val coordinates: GeoCoordinates,
)

enum class ParkingStatus {
    OPEN,
    CLOSED,
    DISTURBANCE
}

@Embeddable
data class ParkingCapacity(
    val total: Int,
    val remaining: Int,
    val modifiedAt: Instant,
)

@Embeddable
data class ParkingOpeningHours(
    @Column(name = "open_from")
    val from: LocalTime,
    @Column(name = "open_to")
    val to: LocalTime,
)

enum class ParkingType {
    GARAGE,
    PARKING_HOUSE,
    UNDERGROUND,
    PARKING_SPOT
}

@Entity
data class ParkingSpecialOpeningHours(
    @Id
    val id: UUID = UUID.randomUUID(),

    val date: LocalDate,

    @Column(name = "open_from")
    val from: LocalTime,

    @Column(name = "open_to")
    val to: LocalTime,

    @ElementCollection
    @CollectionTable(
        name = "parking_special_opening_hours_description",
        joinColumns = [JoinColumn(name = "special_opening_hours_id", referencedColumnName = "id")]
    )
    @AttributeOverrides(
        AttributeOverride(name = "language", column = Column(name = "language")),
        AttributeOverride(name = "value", column = Column(name = "value"))
    )
    val description: List<Localization> = emptyList()
)



