package io.swcode.urbo.cms.infrastructure.search.index

import io.swcode.urbo.cms.application.feed.EventFeed
import io.swcode.urbo.cms.application.feed.NewsFeed
import io.swcode.urbo.cms.domain.model.citizenService.CitizenService
import io.swcode.urbo.cms.domain.model.event.EventOccurrence
import io.swcode.urbo.cms.domain.model.news.News
import io.swcode.urbo.cms.domain.model.poi.Poi
import io.swcode.urbo.cms.domain.model.waste.WasteCollectionLocation
import io.swcode.urbo.cms.search.meili.api.SearchIndexConfig
import io.swcode.urbo.common.error.UrboException
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class SearchIndexConfigurer {

    @Bean
    fun searchIndexedConfig(): SearchIndexConfig {
        return SearchIndexConfig(
            listOf(
                SearchIndexConfig.Index(
                    entityNames = listOf(WasteCollectionLocation::class.simpleName!!),
                    uid = "waste-collection-locations",
                    mapper = SearchIndexEntityMapper::map
                ),
                SearchIndexConfig.Index(
                    entityNames = listOf(EventOccurrence::class.simpleName!!),
                    uid = "grouped-feed-events",
                    sort = listOf("startAt", "title", "endAt", "startAtUnix", "endAtUnix"),
                    filterableAttributes = listOf("startAtUnix", "title", "endAtUnix"),
                    distinct = "id",
                    primaryKey = "eventOccurrenceId",
                    mapper = SearchIndexEntityMapper::map,
                ),
                SearchIndexConfig.Index(
                    entityNames = listOf(News::class.simpleName!!),
                    uid = "news",
                    sort = listOf("date"),
                    mapper = SearchIndexEntityMapper::map
                ),
                SearchIndexConfig.Index(
                    entityNames = listOf(Poi::class.simpleName!!),
                    uid = "pois",
                    mapper = SearchIndexEntityMapper::map
                ),
                SearchIndexConfig.Index(
                    entityNames = listOf(CitizenService::class.simpleName!!),
                    uid = "citizen-services",
                    sort = listOf("title", "url", "description"),
                    mapper = SearchIndexEntityMapper::map
                ),
                SearchIndexConfig.Index(
                    entityNames = listOf(EventOccurrence::class.simpleName!!, News::class.simpleName!!),
                    uid = "feed",
                    sort = listOf("publishedDate"),
                    filterableAttributes = listOf("source", "categoryIds", "type"),
                    mapper = { entity ->
                        when (entity) {
                            is EventOccurrence -> EventFeed.fromDomain(entity)
                            is News -> NewsFeed.fromDomain(entity)
                            else -> throw UrboException("Unsupported entity type")
                        }
                    },
                    idDeletionSelector = { entity ->
                        when (entity) {
                            is EventOccurrence -> entity.event.id.toString()
                            else -> entity.id.toString()
                        }
                    }
                )
            )
        )
    }
}