package io.swcode.urbo.cms.domain.model.waste.repository

import io.swcode.urbo.cms.domain.model.waste.WasteCollectionDate
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import java.util.*

interface WasteCollectionDateRepository : JpaRepository<WasteCollectionDate, UUID>, JpaSpecificationExecutor<WasteCollectionDate>
