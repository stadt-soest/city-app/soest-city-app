package io.swcode.urbo.cms.domain.event.image

import io.swcode.urbo.cms.domain.event.shared.DomainEvent
import io.swcode.urbo.cms.domain.model.shared.Image
import java.util.*

data class ImageSourceChangedEvent(
    val id: UUID,
    val entityType: EntityType,
    val image: Image?,
    val imageRequest: Image?
) : DomainEvent

enum class EntityType {
    NEWS,
    EVENT
}