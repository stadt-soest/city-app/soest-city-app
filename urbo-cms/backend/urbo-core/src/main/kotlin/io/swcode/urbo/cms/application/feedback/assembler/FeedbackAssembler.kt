package io.swcode.urbo.cms.application.feedback.assembler

import io.swcode.urbo.cms.application.feedback.dto.*
import io.swcode.urbo.cms.application.shared.image.localization.LocalizationDto
import io.swcode.urbo.cms.domain.model.feedback.FeedbackForm
import io.swcode.urbo.cms.domain.model.feedback.question.FeedbackQuestion
import io.swcode.urbo.cms.domain.model.feedback.response.*
import io.swcode.urbo.cms.domain.model.shared.Localization
import io.swcode.urbo.common.error.ErrorType
import io.swcode.urbo.common.error.UrboException
import java.util.*

object FeedbackAssembler {
    fun mapToDto(feedbackForm: FeedbackForm): FeedbackFormDto =
        FeedbackFormDto(
            id = feedbackForm.id,
            version = feedbackForm.version,
            questions = feedbackForm.questions.map { mapToDto(it) }
        )

    fun mapToDto(question: FeedbackQuestion): FeedbackQuestionDto =
        FeedbackQuestionDto(
            id = question.id,
            questionType = question.questionType.name,
            questionName = question.questionName.map { mapLocalizationToDto(it) },
            options = question.possibleOptions.map { mapOptionToDto(it) }
        )

    fun mapOptionToDto(option: FeedbackOption): FeedbackOptionDto =
        FeedbackOptionDto(
            id = option.id,
            localizedTexts = option.localizedTexts.map { mapLocalizationToDto(it) }
        )

    private fun mapLocalizationToDto(localization: Localization): LocalizationDto =
        LocalizationDto(
            language = localization.language,
            value = localization.value
        )

    fun mapToEntity(
        feedbackSubmissionRequestDto: FeedbackSubmissionRequestDto,
        feedbackForm: FeedbackForm
    ): FeedbackSubmission {
        val feedbackSubmission = FeedbackSubmission(
            id = UUID.randomUUID(),
            submissionTime = Date(),
            deduplicationKey = feedbackSubmissionRequestDto.deduplicationKey,
            environment = mapToEnvironment(feedbackSubmissionRequestDto.environment),
            feedbackForm = feedbackForm
        )

        feedbackSubmissionRequestDto.answers.forEach { dto ->
            val question = feedbackForm.findQuestionById(dto.questionId)
                ?: throw UrboException("Question with ID ${dto.questionId} not found", ErrorType.RESOURCE_NOT_FOUND)

            val selectedOption = dto.selectedOptionId?.let {
                question.findOptionById(it)
            }

            feedbackSubmission.addAnswer(question, selectedOption, dto.freeText ?: "")
        }

        return feedbackSubmission
    }

    private fun mapToEnvironment(environmentDto: EnvironmentDto): Environment {
        return Environment(
            deviceModelName = environmentDto.deviceModelName,
            operatingSystem = OperatingSystem(
                name = environmentDto.operatingSystemName,
                version = environmentDto.operatingSystemVersion
            ),
            app = AppDetails(
                version = environmentDto.appVersion,
                browserName = environmentDto.browserName,
                usedAs = environmentDto.usedAs
            ),
            appSettings = environmentDto.appSettings
        )
    }
}
