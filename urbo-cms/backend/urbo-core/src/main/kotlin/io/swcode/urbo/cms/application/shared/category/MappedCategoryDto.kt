package io.swcode.urbo.cms.application.shared.category

import io.swcode.urbo.cms.application.category.CategoryColorsDto
import io.swcode.urbo.cms.application.category.CustomIconDto
import io.swcode.urbo.cms.application.category.IconDto
import io.swcode.urbo.cms.application.shared.image.localization.LocalizationDto
import io.swcode.urbo.cms.application.shared.image.localization.toDto
import io.swcode.urbo.cms.domain.model.category.SourceCategory
import java.util.*

data class MappedCategoryDto(
    val sourceCategory: String,
    @Deprecated("Replaced by CustomIcon, remove after migrating to customIcon")
    val icon: IconDto?,
    val customIcon: CustomIconDto?,
    val id: UUID?,
    val localizedNames: List<LocalizationDto>,
    val colors: CategoryColorsDto?,
) {
    companion object {
        fun of(sourceCategory: SourceCategory): MappedCategoryDto {
            return MappedCategoryDto(
                sourceCategory.name,
                sourceCategory.category?.icon?.key?.let { IconDto(it) },
                sourceCategory.category?.icon?.let { CustomIconDto(it.filename) },
                sourceCategory.category?.id,
                sourceCategory.category?.let { category -> category.localizedNames.map { it.toDto() } }
                    ?: listOf(),
                sourceCategory.category?.colors?.let { CategoryColorsDto(it.lightThemeHex, it.darkThemeHex) }
            )
        }
    }
}