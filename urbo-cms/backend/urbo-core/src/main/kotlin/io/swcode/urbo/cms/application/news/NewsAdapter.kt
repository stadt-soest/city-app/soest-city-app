package io.swcode.urbo.cms.application.news

import io.swcode.urbo.cms.domain.model.news.News

fun interface NewsAdapter {
    fun getNews(): List<News>
}
