package io.swcode.urbo.cms.domain.model.poi

import io.swcode.urbo.cms.domain.model.category.SourceCategory
import io.swcode.urbo.cms.domain.model.shared.Address
import io.swcode.urbo.cms.domain.model.shared.ContactPoint
import io.swcode.urbo.cms.domain.model.shared.GeoCoordinates
import io.swcode.urbo.cms.search.index.SearchIndexEntityListener
import io.swcode.urbo.cms.search.meili.api.SearchIndexedEntity
import jakarta.persistence.*
import java.time.Instant
import java.util.UUID

@Entity
@EntityListeners(SearchIndexEntityListener::class)
data class Poi(
    @Id
    @Column(name = "id")
    override val id: UUID,
    override var visible: Boolean = true,
    var externalId: String,
    var name: String,
    var contactPoint: ContactPoint,
    var address: Address,
    @ManyToMany(cascade = [CascadeType.ALL])
    @JoinTable(
        name = "poi_source_category",
        joinColumns = [JoinColumn(name = "poi_id")],
        inverseJoinColumns = [JoinColumn(name = "source_category_name")]
    )
    var sourceCategories: List<SourceCategory> = mutableListOf(),
    var coordinates: GeoCoordinates,
    var dateModified: Instant,
    var url: String?,
): SearchIndexedEntity {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Poi

        return id == other.id
    }
    
    override fun hashCode(): Int {
        return id.hashCode()
    }

    fun updateFrom(poi: Poi): Poi {
        this.name = poi.name
        this.address = poi.address
        this.sourceCategories = poi.sourceCategories
        this.contactPoint = poi.contactPoint
        this.coordinates = poi.coordinates
        this.dateModified = poi.dateModified
        this.url = poi.url
        return this
    }
}