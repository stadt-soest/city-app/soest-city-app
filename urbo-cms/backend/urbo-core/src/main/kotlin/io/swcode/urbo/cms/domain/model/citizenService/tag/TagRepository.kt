package io.swcode.urbo.cms.domain.model.citizenService.tag

import io.swcode.urbo.cms.domain.model.citizenService.Tag
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface TagRepository : JpaRepository<Tag, UUID> {}
