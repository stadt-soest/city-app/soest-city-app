package io.swcode.urbo.cms.application.citizenService

import io.swcode.urbo.cms.domain.model.citizenService.CitizenService

fun interface CitizenAdapter {
    fun getCitizenServices(): List<CitizenService>
}