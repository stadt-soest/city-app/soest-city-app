package io.swcode.urbo.cms.domain.model.citizenService

import io.swcode.urbo.cms.domain.model.shared.Localization
import io.swcode.urbo.cms.search.index.SearchIndexEntityListener
import io.swcode.urbo.cms.search.meili.api.SearchIndexedEntity
import jakarta.persistence.*
import java.util.*

@Entity
@EntityListeners(SearchIndexEntityListener::class)
data class CitizenService(

    @Id
    @Column(name = "id")
    override val id: UUID,

    override var visible: Boolean = true,

    var favorite: Boolean = false,

    var externalId: String? = null,

    var title: String,

    var description: String,

    var url: String,

    @OneToMany(
        fetch = FetchType.LAZY,
        cascade = [CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH]
    )
    @JoinTable(
        name = "citizen_service_tags",
        joinColumns = [JoinColumn(name = "citizen_service_id")],
        inverseJoinColumns = [JoinColumn(name = "tag_id")]
    )
    var tags: MutableList<Tag> = mutableListOf()
) : SearchIndexedEntity {
    fun updateFrom(citizenService: CitizenService, preserveVisibilityAndFavorite: Boolean): CitizenService {
        externalId = citizenService.externalId
        title = citizenService.title
        description = citizenService.description
        url = citizenService.url

        if (!preserveVisibilityAndFavorite) {
            visible = citizenService.visible
            favorite = citizenService.favorite
        }

        return this
    }

    fun addTag(tag: Tag) {
        if (!tags.contains(tag)) {
            tags.add(tag)
        }
    }

    fun removeTag(tag: Tag) {
        tags.remove(tag)
    }
}

@Entity
data class Tag(

    @Id
    @Column(name = "id")
    val id: UUID,

    @ElementCollection
    @CollectionTable(name = "tag_localized_names", joinColumns = [JoinColumn(name = "tag_id")])
    @AttributeOverrides(
        AttributeOverride(name = "language", column = Column(name = "language")),
        AttributeOverride(name = "value", column = Column(name = "value"))
    )
    var localizedNames: List<Localization> = mutableListOf(),
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Tag) return false
        return id == other.id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}

