package io.swcode.urbo.cms.domain.model.category.group

import io.swcode.urbo.cms.application.category.CategoryGroupCreateDto
import io.swcode.urbo.cms.domain.model.category.CategoryType
import io.swcode.urbo.cms.domain.model.shared.Localization
import io.swcode.urbo.common.error.ErrorType
import io.swcode.urbo.common.error.UrboException
import jakarta.transaction.Transactional
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.*

@Service
class CategoryGroupService(private val categoryGroupRepository: CategoryGroupRepository) {

    fun getCategoryGroups(type: CategoryType): List<CategoryGroup> {
        return categoryGroupRepository.findAllByTypeOrderBySortOrderAsc(type)
    }

    fun createCategoryGroup(categoryGroupCreateDto: CategoryGroupCreateDto): CategoryGroup {
        return categoryGroupCreateDto.toEntity().also {
            categoryGroupRepository.save(it)
        }
    }

    fun updateCategoryGroup(id: UUID, localizedNames: List<Localization>): CategoryGroup {
        return categoryGroupRepository.findByIdOrNull(id)?.update(localizedNames)
            ?.let { categoryGroupRepository.save(it) }
            ?: throw UrboException("category group id $id not found.", ErrorType.RESOURCE_NOT_FOUND)
    }

    @Transactional
    fun updateCategoryGroupOrder(categoryGroups: List<CategoryGroup>) {
        categoryGroups.forEachIndexed { index, categoryGroup ->
            val existingCategoryGroup = categoryGroupRepository.findByIdOrNull(categoryGroup.id)
                ?: throw UrboException("categoryGroup.id=${categoryGroup.id} not found", ErrorType.RESOURCE_NOT_FOUND)
            existingCategoryGroup.sortOrder = index
            categoryGroupRepository.save(existingCategoryGroup)
        }
    }

    fun deleteCategoryGroup(id: UUID) {
        categoryGroupRepository.deleteById(id)
    }
}