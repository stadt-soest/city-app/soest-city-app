package io.swcode.urbo.cms.application.event

import io.swcode.urbo.cms.application.event.response.EventResponseDto
import io.swcode.urbo.cms.domain.model.event.EventService
import jakarta.validation.constraints.Size
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.web.PageableDefault
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping
@Validated
class EventController(private val eventService: EventService) {

    @GetMapping("/v1/events")
    fun getEvents(
        @PageableDefault(
            size = 20,
            sort = ["title"],
            direction = Sort.Direction.ASC
        ) pageable: Pageable,
        @RequestParam(required = false, defaultValue = "") categoryIds: List<UUID>,
        @RequestParam(required = false, defaultValue = "") @Size(max = 100) status: String,
        @RequestParam(required = false, defaultValue = "") @Size(max = 100) searchKeyword: String,
        @RequestParam(required = false, defaultValue = "") exclude: List<UUID>,
    ): Page<EventResponseDto> {
        return eventService.getEvents(pageable, categoryIds, exclude, status, searchKeyword)
    }

    @GetMapping("/v1/events/{id}")
    fun getEvent(@PathVariable id: UUID): ResponseEntity<EventResponseDto> {
        val eventResponse = eventService.getEvent(id)
        return ResponseEntity.ok(eventResponse)
    }

    @GetMapping("/v1/event-occurrences/{occurrenceId}/event")
    fun getEventByOccurrenceId(@PathVariable occurrenceId: UUID): ResponseEntity<EventResponseDto> {
        val eventResponse = eventService.getEventByOccurrenceId(occurrenceId)
        return ResponseEntity.ok(eventResponse)
    }
}
