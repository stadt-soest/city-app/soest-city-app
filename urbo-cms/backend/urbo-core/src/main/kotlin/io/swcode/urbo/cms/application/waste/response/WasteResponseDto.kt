package io.swcode.urbo.cms.application.waste.response

import io.swcode.urbo.cms.domain.model.waste.WasteCollectionDate
import io.swcode.urbo.cms.domain.model.waste.WasteType
import io.swcode.urbo.cms.search.meili.api.SearchIndexedDto
import java.time.Instant
import java.util.*

data class WasteResponseDto(
    val id: UUID,
    val city: String,
    val street: String,
    val wasteDates: List<WasteDateDTO>
): SearchIndexedDto

data class WasteDateDTO(
    val id: UUID,
    val date: Instant,
    val wasteType: WasteType
)

data class UpcomingWastesDto(
    val allUpcomingDates: List<WasteDateInfo>,
    val upcomingDatesByType: Map<WasteType, List<WasteDateInfo>>
)

data class WasteDateInfo(
    val id: UUID,
    val date: Instant,
    val wasteType: WasteType
) {
    companion object {
        fun fromWasteDate(wasteCollectionDate: WasteCollectionDate): WasteDateInfo =
            WasteDateInfo(
                id = wasteCollectionDate.id,
                date = wasteCollectionDate.date,
                wasteType = wasteCollectionDate.wasteType
            )
    }
}
