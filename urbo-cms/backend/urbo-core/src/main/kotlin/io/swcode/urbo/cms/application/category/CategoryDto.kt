package io.swcode.urbo.cms.application.category

import io.swcode.urbo.cms.application.shared.image.localization.LocalizationDto
import io.swcode.urbo.cms.application.shared.image.localization.toDto
import io.swcode.urbo.cms.domain.model.category.Category
import io.swcode.urbo.cms.domain.model.category.CategoryType
import io.swcode.urbo.cms.domain.model.category.Icon
import io.swcode.urbo.cms.domain.model.category.group.CategoryGroup
import jakarta.validation.Valid
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Pattern
import jakarta.validation.constraints.Size
import java.util.*

data class CategoryResponseDto(
    val id: UUID,
    val localizedNames: List<LocalizationDto>,
    val sourceCategories: List<String>,
    @Deprecated("Replaced by CustomIcon, remove after migrating to customIcon")
    val icon: IconDto?,
    val customIcon: CustomIconDto?,
    val type: CategoryType,
    val categoryGroup: CategoryGroupResponseDto?,
    val colors: CategoryColorsDto?
) {
    companion object {
        fun fromEntity(entity: Category): CategoryResponseDto {
            return CategoryResponseDto(
                id = entity.id,
                localizedNames = entity.localizedNames.map { it.toDto() },
                icon = entity.icon?.key?.let { key -> IconDto(key) },
                sourceCategories = entity.sourceCategories.map { it.name },
                customIcon = entity.icon?.let {
                    CustomIconDto(it.filename)
                },
                type = entity.type,
                categoryGroup = entity.categoryGroup?.let { CategoryGroupResponseDto.fromEntity(it) },
                colors = entity.colors?.let { CategoryColorsDto(it.lightThemeHex, it.darkThemeHex) },
            )
        }
    }
}

data class CategoryColorsDto(
    @field:Size(min = 7, max = 7) @field:Pattern(
        regexp = "^#[0-9a-fA-F]{6}$",
        message = "Invalid hex color format"
    )
    val lightThemeHexColor: String,
    @field:Size(min = 7, max = 7) @field:Pattern(
        regexp = "^#[0-9a-fA-F]{6}$",
        message = "Invalid hex color format"
    )
    val darkThemeHexColor: String
)

data class CategoryGroupResponseDto(
    val id: UUID,
    val type: CategoryType,
    val localizedNames: List<LocalizationDto>
) {
    companion object {
        fun fromEntity(entity: CategoryGroup): CategoryGroupResponseDto {
            return CategoryGroupResponseDto(
                id = entity.id,
                localizedNames = entity.localizedNames.map { it.toDto() },
                type = entity.type,
            )
        }
    }
}

data class CategoryCreateDto(
    @field:Valid val localizedNames: List<LocalizationDto>,
    val type: CategoryType,
    val categoryGroupId: UUID? = null,
    @field:Valid val colors: CategoryColorsDto? = null
)

@Deprecated("Replaced by CustomIcon, remove after migrating to customIcon")
data class IconDto(@field:Size(min = 1, max = 50) @field:NotBlank val key: String)

data class CategoryUpdateDto(
    @field:Valid val localizedNames: List<LocalizationDto>,
    @field:Size(max = 100) val filename: String?,
    @field:Size(max = 200) val sourceCategories: List<@Size(min = 1, max = 255) @NotBlank String>,
    val categoryGroupId: UUID? = null,
    @field:Valid val colors: CategoryColorsDto? = null
)

data class CustomIconDto(
    @field:Size(min = 1, max = 50) @field:NotBlank val filename: String
)

data class CategoryGroupCreateDto(
    val type: CategoryType,
    @field:Valid val localizedNames: List<LocalizationDto>
) {
    fun toEntity(): CategoryGroup {
        return CategoryGroup(
            UUID.randomUUID(),
            type,
            localizedNames.map { it.toLocalization() }
        )
    }
}

data class CategoryUpdateOrderDto(
    val id: UUID,
    val order: Int
) {
    fun toEntity(): Category {
        return Category(id, listOf(), listOf(), CategoryType.NEWS, null, null, order)
    }
}

data class CategoryGroupUpdateOrderDto(
    val id: UUID,
    val order: Int
) {
    fun toEntity(): CategoryGroup {
        return CategoryGroup(id, CategoryType.NEWS, listOf(), mutableListOf(), order)
    }
}
