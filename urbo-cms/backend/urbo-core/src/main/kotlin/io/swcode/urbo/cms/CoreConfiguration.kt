package io.swcode.urbo.cms

import io.swcode.urbo.user.management.UserManagementConfig
import net.javacrumbs.shedlock.spring.annotation.EnableSchedulerLock
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Import
import org.springframework.data.jpa.repository.config.EnableJpaAuditing
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.data.web.config.EnableSpringDataWebSupport

@ComponentScan
@EntityScan
@EnableJpaAuditing
@EnableJpaRepositories
@Import(value = [UserManagementConfig::class])
@EnableSchedulerLock(defaultLockAtMostFor = "\${urbo.scheduler.lock.default-locked-at-most-for}")
@EnableSpringDataWebSupport(pageSerializationMode = EnableSpringDataWebSupport.PageSerializationMode.VIA_DTO)
class CoreConfiguration