package io.swcode.urbo.cms.application.event

import io.swcode.urbo.cms.domain.model.event.Event

fun interface EventAdapter {
    fun getEvents(): List<Event>
}