package io.swcode.urbo.cms.application.news.dto


data class NewsHighlightRequestDto(val highlight: Boolean)