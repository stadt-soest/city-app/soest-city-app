package io.swcode.urbo.cms.domain.model.citizenService

import io.swcode.urbo.cms.application.citizenService.SourceType
import jakarta.persistence.criteria.CriteriaBuilder
import jakarta.persistence.criteria.CriteriaQuery
import jakarta.persistence.criteria.Predicate
import jakarta.persistence.criteria.Root
import org.springframework.data.jpa.domain.Specification
import java.util.*

object CitizenServiceSpecification {

    fun withFilters(
        tagId: UUID?,
        visible: Boolean?,
        favorite: Boolean?,
        withoutTag: Boolean,
        sourceType: SourceType?
    ): Specification<CitizenService> {
        return Specification { root: Root<CitizenService>, _: CriteriaQuery<*>?, criteriaBuilder: CriteriaBuilder ->

            val predicates = mutableListOf<Predicate>()

            tagId?.let {
                val tagJoin = root.join<CitizenService, Tag>("tags")
                predicates.add(criteriaBuilder.equal(tagJoin.get<UUID>("id"), it))
            }

            visible?.let {
                predicates.add(criteriaBuilder.equal(root.get<Boolean>("visible"), it))
            }

            favorite?.let {
                predicates.add(criteriaBuilder.equal(root.get<Boolean>("favorite"), it))
            }

            if (withoutTag) {
                predicates.add(criteriaBuilder.isEmpty(root.get<Set<Tag>>("tags")))
            }

            sourceType?.let {
                when (it) {
                    SourceType.EXTERNAL -> predicates.add(criteriaBuilder.isNotNull(root.get<String>("externalId")))
                    SourceType.INTERNAL -> predicates.add(criteriaBuilder.isNull(root.get<String>("externalId")))
                }
            }

            criteriaBuilder.and(*predicates.toTypedArray())
        }
    }

    fun searchByKeyword(keyword: String?): Specification<CitizenService> {
        return Specification { root, _, criteriaBuilder ->
            if (keyword.isNullOrEmpty()) {
                criteriaBuilder.conjunction()
            } else {
                val titlePredicate =
                    criteriaBuilder.like(criteriaBuilder.lower(root["title"]), "%${keyword.lowercase()}%")
                val descriptionPredicate = criteriaBuilder.like(
                    criteriaBuilder.lower(root["description"]),
                    "%${keyword.lowercase()}%"
                )
                val urlPredicate =
                    criteriaBuilder.like(criteriaBuilder.lower(root["url"]), "%${keyword.lowercase()}%")

                criteriaBuilder.or(titlePredicate, descriptionPredicate, urlPredicate)
            }
        }
    }
}
