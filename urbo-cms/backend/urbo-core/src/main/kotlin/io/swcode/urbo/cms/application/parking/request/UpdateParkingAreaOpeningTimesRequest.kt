package io.swcode.urbo.cms.application.parking.request

import jakarta.validation.Valid
import jakarta.validation.constraints.NotEmpty
import java.time.DayOfWeek
import java.time.LocalTime

data class UpdateParkingAreaOpeningTimesRequest(
    @field:NotEmpty(message = "Opening times must not be empty")
    @field:Valid
    val openingTimes: Map<DayOfWeek, ParkingOpeningHoursDto>
)

data class ParkingOpeningHoursDto(
    val from: LocalTime,
    val to: LocalTime
) {
    init {
        require(from.isBefore(to)) { "From time must be before To time" }
    }
}