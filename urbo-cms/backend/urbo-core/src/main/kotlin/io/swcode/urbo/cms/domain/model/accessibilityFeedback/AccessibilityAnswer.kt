package io.swcode.urbo.cms.domain.model.accessibilityFeedback

import io.swcode.urbo.cms.domain.model.feedback.response.Environment
import jakarta.persistence.*
import java.time.Instant
import java.util.UUID

@Entity
class AccessibilityFeedback(
    @Id
    val id: UUID,

    val answer: String,

    val createdAt: Instant,

    @ElementCollection(fetch = FetchType.LAZY)
    @Column(name = "image_filename")
    var imageFilenames: MutableList<String> = mutableListOf(),

    @Embedded
    val environment: Environment,
)