package io.swcode.urbo.cms.application.parking

import io.swcode.urbo.cms.domain.model.parking.ParkingArea

fun interface ParkingAdapter {
    fun getParkingAreas(): List<ParkingArea>
}
