package io.swcode.urbo.cms.domain.model.category

import io.swcode.urbo.cms.domain.model.category.group.CategoryGroup
import io.swcode.urbo.cms.domain.model.shared.Localization
import jakarta.persistence.*
import java.util.*

@Entity
data class Category(
    @Id val id: UUID,

    @ElementCollection
    @CollectionTable(name = "category_name", joinColumns = [JoinColumn(name = "category_id")])
    @AttributeOverrides(
        AttributeOverride(name = "language", column = Column(name = "language")),
        AttributeOverride(name = "value", column = Column(name = "value"))
    )
    var localizedNames: List<Localization>,

    @OneToMany(cascade = [CascadeType.REFRESH, CascadeType.DETACH])
    @JoinColumn(name = "category_id")
    var sourceCategories: List<SourceCategory>,

    @Enumerated(EnumType.STRING)
    val type: CategoryType,

    @Embedded
    var icon: Icon?,

    @ManyToOne
    @JoinColumn(name = "category_group_id")
    var categoryGroup: CategoryGroup? = null,

    @Column(name = "sort_order")
    var sortOrder: Int = 0,

    @Embedded
    var colors: CategoryColors? = null
) {
    fun update(
        localizedNames: List<Localization>,
        sourceCategories: List<SourceCategory>,
        categoryGroup: CategoryGroup?,
        icon: Icon? = null,
        colors: CategoryColors? = null,
    ): Category {
        this.localizedNames = localizedNames
        this.sourceCategories = sourceCategories
        this.categoryGroup = categoryGroup
        if (icon != null) {
            this.icon = this.icon?.copy(filename = icon.filename) ?: icon
        }
        this.colors = colors
        return this
    }

    companion object {
        fun of(
            localizedNames: List<Localization>,
            categoryType: CategoryType,
            categoryGroup: CategoryGroup?
        ): Category {
            return Category(UUID.randomUUID(), localizedNames, mutableListOf(), categoryType, null, categoryGroup)
        }
    }
}

@Embeddable
data class Icon(
    val filename: String,
    @Deprecated("Remove after migrating to filename")
    val key: String? = null,
)

@Embeddable
data class CategoryColors(
    val lightThemeHex: String,
    val darkThemeHex: String
)