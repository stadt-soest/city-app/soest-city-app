package io.swcode.urbo.cms.domain.model.waste.service

import io.swcode.urbo.cms.application.waste.assembler.WasteAssembler
import io.swcode.urbo.cms.application.waste.response.UpcomingWastesDto
import io.swcode.urbo.cms.application.waste.response.WasteDateInfo
import io.swcode.urbo.cms.application.waste.response.WasteResponseDto
import io.swcode.urbo.cms.domain.model.waste.WasteCollectionDateSpecifications
import io.swcode.urbo.cms.domain.model.waste.WasteCollectionLocation
import io.swcode.urbo.cms.domain.model.waste.WasteSpecifications
import io.swcode.urbo.cms.domain.model.waste.repository.WasteCollectionDateRepository
import io.swcode.urbo.cms.domain.model.waste.repository.WasteCollectionLocationRepository
import io.swcode.urbo.common.error.ErrorType
import io.swcode.urbo.common.error.UrboException
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.util.*

@Service
class WasteService(
    private val wasteCollectionLocationRepository: WasteCollectionLocationRepository,
    private val wasteCollectionDateRepository: WasteCollectionDateRepository,
    private val wasteAssembler: WasteAssembler
) {

    @Transactional
    fun save(wasteCollectionLocationRequest: WasteCollectionLocation): WasteCollectionLocation {
        val existingWaste = wasteCollectionLocationRepository.existsByWasteId(wasteCollectionLocationRequest.wasteId)

        return if (existingWaste) {
            update(wasteCollectionLocationRequest)
        } else {
            wasteCollectionLocationRepository.save(wasteCollectionLocationRequest)
        }
    }

    @Transactional
    fun update(wasteCollectionLocationRequest: WasteCollectionLocation): WasteCollectionLocation {
        val waste = wasteCollectionLocationRepository.findByWasteId(wasteCollectionLocationRequest.wasteId)
            ?: throw UrboException(
                "Waste not found with wasteId: ${wasteCollectionLocationRequest.wasteId}",
                ErrorType.RESOURCE_NOT_FOUND
            )
        return wasteCollectionLocationRepository.save(waste.updateFrom(wasteCollectionLocationRequest))

    }

    @Transactional
    fun getWasteCollections(
        city: String? = null, street: String? = null, searchKeyword: String? = null, pageable: Pageable
    ): Page<WasteResponseDto> {
        val spec = Specification.where(WasteSpecifications.withCity(city)).and(WasteSpecifications.withStreet(street))
            .and(WasteSpecifications.withSearchKeyword(searchKeyword))

        return wasteCollectionLocationRepository.findAll(spec, pageable).map {
            wasteAssembler.toResponseDto(it)
        }
    }

    fun getWasteDatesForWaste(
        wasteId: UUID,
        startDate: Instant,
        endDate: Instant,
        wasteTypes: List<String>
    ): UpcomingWastesDto =
        Specification
            .where(WasteCollectionDateSpecifications.withWasteId(wasteId))
            .and(WasteCollectionDateSpecifications.betweenDates(startDate, endDate))
            .and(WasteCollectionDateSpecifications.withWasteType(wasteTypes))
            .let(wasteCollectionDateRepository::findAll)
            .map(WasteDateInfo::fromWasteDate)
            .sortedBy(WasteDateInfo::date)
            .groupBy(WasteDateInfo::wasteType)
            .let { groupedDates ->
                UpcomingWastesDto(allUpcomingDates = groupedDates.values.flatten(), upcomingDatesByType = groupedDates)
            }
}