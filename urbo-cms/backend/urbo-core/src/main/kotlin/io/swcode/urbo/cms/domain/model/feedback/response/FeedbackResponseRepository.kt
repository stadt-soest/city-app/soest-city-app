package io.swcode.urbo.cms.domain.model.feedback.response

import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface FeedbackResponseRepository : JpaRepository<FeedbackSubmission, UUID>
