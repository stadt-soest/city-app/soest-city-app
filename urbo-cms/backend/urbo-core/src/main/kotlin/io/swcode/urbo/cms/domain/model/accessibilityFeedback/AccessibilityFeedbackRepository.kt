package io.swcode.urbo.cms.domain.model.accessibilityFeedback

import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface AccessibilityFeedbackRepository : JpaRepository<AccessibilityFeedback, UUID>