package io.swcode.urbo.cms.application.citizenService

import io.swcode.urbo.cms.application.citizenService.dto.*
import io.swcode.urbo.cms.domain.model.citizenService.CitizenServiceService
import io.swcode.urbo.cms.domain.model.citizenService.tag.TagService
import jakarta.validation.Valid
import jakarta.validation.constraints.Size
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.web.SortDefault
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/v1")
@Validated
class CitizenController(
    private val citizenServiceService: CitizenServiceService,
    private val tagService: TagService
) {

    @PostMapping("/citizenServices")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).CITIZEN_SERVICE_WRITE)")
    fun createCitizenService(@RequestBody @Valid createCitizenServiceDto: CreateCitizenServiceDto): ResponseEntity<CitizenServiceResponseDto> {
        citizenServiceService.createCitizenService(createCitizenServiceDto).also {
            return ResponseEntity.ok(CitizenServiceResponseDto.fromDomain(it))
        }
    }

    @PutMapping("/citizenServices/{id}")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).CITIZEN_SERVICE_WRITE)")
    fun updateCitizenService(
        @PathVariable id: UUID,
        @RequestBody @Valid updateCitizenServiceDto: UpdateCitizenServiceDto
    ): ResponseEntity<CitizenServiceResponseDto> {
        updateCitizenServiceDto.toEntity(id).also {
            citizenServiceService.update(it)
            return ResponseEntity.ok(CitizenServiceResponseDto.fromDomain(it))
        }
    }

    @DeleteMapping("/citizenServices/{id}")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).CITIZEN_SERVICE_DELETE)")
    fun deleteCitizenService(@PathVariable id: UUID): ResponseEntity<Unit> {
        citizenServiceService.delete(id).also {
            return ResponseEntity.noContent().build()
        }
    }

    @GetMapping("/citizenServices")
    fun getCitizenServices(
        @RequestParam(required = false) tagId: @org.hibernate.validator.constraints.UUID UUID?,
        @RequestParam(required = false) visible: Boolean?,
        @RequestParam(required = false) favorite: Boolean?,
        @RequestParam(required = false, defaultValue = "false") withoutTag: Boolean = false,
        @RequestParam(required = false) sourceType: SourceType?,
        pageable: Pageable
    ): Page<CitizenServiceResponseDto> {
        return citizenServiceService.getCitizenServices(pageable, tagId, visible, favorite, withoutTag, sourceType)
    }

    @PatchMapping("/citizenServices/{id}/visibility")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).CITIZEN_SERVICE_WRITE)")
    fun updateCitizenServiceVisibility(@PathVariable id: UUID, @RequestParam visible: Boolean): ResponseEntity<Unit> {
        citizenServiceService.updateVisibility(id, visible)
        return ResponseEntity.noContent().build()
    }

    @PatchMapping("/citizenServices/{id}/favorite")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).CITIZEN_SERVICE_WRITE)")
    fun updateCitizenServiceFavorite(@PathVariable id: UUID, @RequestParam favorite: Boolean): ResponseEntity<Unit> {
        citizenServiceService.updateFavorite(id, favorite).also {
            return ResponseEntity.noContent().build()
        }
    }

    @GetMapping("/citizenServices/{id}")
    fun getCitizenService(@PathVariable id: UUID): ResponseEntity<CitizenServiceResponseDto> {
        val citizenServiceResponse = citizenServiceService.getCitizenService(id)
        return ResponseEntity.ok(citizenServiceResponse)
    }

    @GetMapping("/citizenServices/search")
    fun searchCitizenServices(
        @SortDefault(sort = ["dateCreated"], direction = Sort.Direction.DESC)
        @RequestParam(required = false) @Size(max = 200) keyword: String?,
        pageable: Pageable
    ): Page<CitizenServiceResponseDto> {
        return citizenServiceService.searchCitizenServices(keyword, pageable)
    }

    @GetMapping("/citizenServices/tags")
    fun getAllTags(): List<TagDto> {
        return tagService.getAllTags().map { TagDto.fromEntity(it) }
    }

    @GetMapping("/citizenServices/tags/{id}")
    fun getTagById(@PathVariable id: UUID): ResponseEntity<TagDto> {
        val tag = tagService.getTagById(id)
        return ResponseEntity.ok(TagDto.fromEntity(tag))
    }

    @PostMapping("/citizen-service-tags")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).CITIZEN_SERVICE_WRITE)")
    fun createTag(@RequestBody tagDto: CreateTagDto): ResponseEntity<TagDto> {
        tagService.createTag(CreateTagDto.toEntity(tagDto)).also { tag ->
            return ResponseEntity.ok(TagDto.fromEntity(tag))
        }
    }

    @PatchMapping("/tags/{tagId}/citizen-services")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).CITIZEN_SERVICE_WRITE)")
    fun assignCitizenServicesToTag(
        @PathVariable tagId: UUID,
        @RequestBody citizenServiceIds: List<UUID>
    ): ResponseEntity<Unit> {
        citizenServiceService.updateTagAssignments(tagId, citizenServiceIds).also {
            return ResponseEntity.noContent().build()
        }
    }

    @DeleteMapping("/citizen-service-tags/{id}")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).CITIZEN_SERVICE_DELETE)")
    fun deleteTag(@PathVariable id: UUID): ResponseEntity<Unit> {
        tagService.deleteTag(id).also {
            return ResponseEntity.noContent().build()
        }
    }

    @PutMapping("/citizen-service-tags")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).CITIZEN_SERVICE_WRITE)")
    fun createOrUpdateTag(@RequestBody tagDto: CreateTagDto): ResponseEntity<TagDto> {
        tagService.createOrUpdateTag(CreateTagDto.toEntity(tagDto)).also {
            return ResponseEntity.ok(TagDto.fromEntity(it))
        }
    }
}
