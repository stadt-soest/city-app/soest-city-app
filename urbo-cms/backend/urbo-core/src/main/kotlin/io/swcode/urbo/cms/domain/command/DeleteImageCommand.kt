package io.swcode.urbo.cms.domain.command

import io.swcode.urbo.cms.domain.model.shared.Image
import io.swcode.urbo.command.api.Command

data class DeleteImageCommand(
    val image: Image
) : Command