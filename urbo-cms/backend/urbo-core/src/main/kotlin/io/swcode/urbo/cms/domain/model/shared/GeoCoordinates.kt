package io.swcode.urbo.cms.domain.model.shared

import jakarta.persistence.Embeddable

@Embeddable
data class GeoCoordinates(
    val latitude: Double,
    val longitude: Double,
)
