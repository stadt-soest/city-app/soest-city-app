package io.swcode.urbo.cms.application.shared.image

import io.swcode.urbo.cms.domain.model.shared.Image


data class ImageDto(
    val imageSource: String,
    val thumbnail: ImageFileDto? = null,
    val highRes: ImageFileDto? = null,
    val description: String? = null,
    val copyrightHolder: String? = null
) {
    companion object {
        fun fromEntity(image: Image): ImageDto {
            return ImageDto(
                imageSource = image.source,
                thumbnail = image.thumbnail?.let {
                    ImageFileDto(
                        filename = it.filename,
                        width = it.width,
                        height = it.height,
                    )
                },
                highRes = image.highRes?.let {
                    ImageFileDto(
                        filename = it.filename,
                        width = it.width,
                        height = it.height,
                    )
                },
                description = image.description,
                copyrightHolder = image.copyrightHolder
            )
        }
    }
}

data class ImageFileDto(
    val filename: String,
    var width: Int? = null,
    var height: Int? = null
)
