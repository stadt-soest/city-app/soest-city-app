package io.swcode.urbo.cms.domain.command.handler

import io.swcode.urbo.cms.domain.command.DownloadEventImageCommand
import io.swcode.urbo.cms.domain.model.event.Event
import io.swcode.urbo.cms.domain.model.event.EventRepository
import io.swcode.urbo.cms.search.index.SearchIndexService
import io.swcode.urbo.command.api.CommandHandler
import io.swcode.urbo.common.logging.createLogger
import io.swcode.urbo.core.api.image.ImageProcessingAdapter
import org.springframework.stereotype.Component

@Component
class DownloadEventImageCommandHandler(
    private val eventRepository: EventRepository,
    private val searchIndexService: SearchIndexService,
    private val imageProcessingAdapter: ImageProcessingAdapter
) : CommandHandler<DownloadEventImageCommand> {

    companion object {
        private val logger = createLogger()
    }

    override fun handle(command: DownloadEventImageCommand) {
        eventRepository.findById(command.eventId).orElse(null)?.let { event ->

            val processingResult = imageProcessingAdapter.downloadAndProcessImage(command.image.source)
            event.image = command.image.from(processingResult)
            eventRepository.save(event)
            triggerSearchIndexUpdateForOccurrences(event)
        } ?: logger.info("Event ${command.eventId} not found, no image download required")
    }

    private fun triggerSearchIndexUpdateForOccurrences(event: Event) {
        event.occurrences.forEach { occurrence ->
            searchIndexService.updateOrRemoveSearchIndex(occurrence)
        }
    }
}
