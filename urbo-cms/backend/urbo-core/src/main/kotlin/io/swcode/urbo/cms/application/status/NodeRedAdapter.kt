package io.swcode.urbo.cms.application.status

fun interface NodeRedAdapter {
    fun getNodeRedStatus(): NodeRedStatus
}

enum class NodeRedStatus {
    UP,
    DOWN,
}