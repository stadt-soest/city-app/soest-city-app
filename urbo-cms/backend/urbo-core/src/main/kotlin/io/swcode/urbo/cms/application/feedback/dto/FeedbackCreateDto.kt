package io.swcode.urbo.cms.application.feedback.dto

import io.swcode.urbo.cms.application.shared.image.localization.LocalizationDto
import io.swcode.urbo.cms.domain.model.feedback.question.QuestionType

data class FeedbackFormCreateDto(
    val questions: List<FeedbackQuestionCreateDto>
)

data class FeedbackQuestionCreateDto(
    val questionType: QuestionType,
    val questionName: List<LocalizationDto>,
    val possibleOptions: List<FeedbackOptionDto>
)

