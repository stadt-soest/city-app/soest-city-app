package io.swcode.urbo.cms.domain.model.waste

import org.springframework.data.jpa.domain.Specification
import java.time.Instant
import java.util.*

object WasteCollectionDateSpecifications {

    fun withWasteId(wasteId: UUID): Specification<WasteCollectionDate> = Specification { root, _, cb ->
        cb.equal(root.get<UUID>("wasteCollectionLocation").get<UUID>("id"), wasteId)
    }

    fun betweenDates(startDate: Instant, endDate: Instant): Specification<WasteCollectionDate> = Specification { root, _, cb ->
        cb.between(root.get("date"), startDate, endDate)
    }

    fun withWasteType(wasteTypes: List<String>?): Specification<WasteCollectionDate>? = wasteTypes?.let { types ->
        Specification { root, _, cb ->
            val criteria = types.mapNotNull { try { WasteType.valueOf(it) } catch (e: IllegalArgumentException) { null } }
            if (criteria.isNotEmpty()) {
                root.get<WasteType>("wasteType").`in`(criteria)
            } else {
                cb.conjunction()
            }
        }
    }
}