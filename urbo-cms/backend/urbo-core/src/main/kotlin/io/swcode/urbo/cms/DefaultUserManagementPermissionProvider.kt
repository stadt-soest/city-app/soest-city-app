package io.swcode.urbo.cms

import io.swcode.urbo.cms.application.CorePermission
import io.swcode.urbo.permission.adapter.UserManagementPermissionProvider
import org.springframework.stereotype.Service

@Service
class DefaultUserManagementPermissionProvider : UserManagementPermissionProvider {
    override val permissions = CorePermission.entries
}

