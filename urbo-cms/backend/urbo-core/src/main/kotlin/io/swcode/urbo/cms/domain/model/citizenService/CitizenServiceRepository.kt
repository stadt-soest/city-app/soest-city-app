package io.swcode.urbo.cms.domain.model.citizenService

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface CitizenServiceRepository : JpaRepository<CitizenService, UUID>, JpaSpecificationExecutor<CitizenService> {
    fun findByExternalId(id: String): CitizenService?

    @Query("SELECT cs FROM CitizenService cs JOIN cs.tags t WHERE t.id = :tagId")
    fun findByTagId(tagId: UUID, pageable: Pageable): Page<CitizenService>

    @Modifying(clearAutomatically = true)
    @Query(
        value = "DELETE FROM citizen_service_tags WHERE tag_id = :tagId",
        nativeQuery = true
    )
    fun bulkRemoveTagAssociations(@Param("tagId") tagId: UUID)
}

