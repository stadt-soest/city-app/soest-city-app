package io.swcode.urbo.cms.domain.event.image

import io.swcode.urbo.cms.domain.command.DeleteImageCommand
import io.swcode.urbo.cms.domain.command.DownloadEventImageCommand
import io.swcode.urbo.cms.domain.command.DownloadNewsImageCommand
import io.swcode.urbo.command.api.CommandBus
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

@Component
class ImageSourceChangedEventHandler(private val commandBus: CommandBus) {

    @EventListener
    fun handleImageChanged(imageChangedEvent: ImageSourceChangedEvent) {
        when (imageChangedEvent.entityType) {
            EntityType.NEWS -> handleNewsImageChange(imageChangedEvent)
            EntityType.EVENT -> handleEventImageChange(imageChangedEvent)
        }
    }

    private fun handleNewsImageChange(event: ImageSourceChangedEvent) {
        event.image?.let {
            commandBus.submitAsyncCommand(DeleteImageCommand(image = it))
        }

        event.imageRequest?.also { requestImage ->
            commandBus.submitAsyncCommand(
                DownloadNewsImageCommand(
                    newsId = event.id,
                    image = requestImage
                )
            )
        }
    }

    private fun handleEventImageChange(event: ImageSourceChangedEvent) {
        event.image?.let {
            commandBus.submitAsyncCommand(DeleteImageCommand(image = it))
        }

        event.imageRequest?.also { requestImage ->
            commandBus.submitAsyncCommand(
                DownloadEventImageCommand(
                    eventId = event.id,
                    image = requestImage
                )
            )
        }
    }
}
