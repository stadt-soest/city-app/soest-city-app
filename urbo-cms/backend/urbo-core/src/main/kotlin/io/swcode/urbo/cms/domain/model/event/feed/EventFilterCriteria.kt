package io.swcode.urbo.cms.domain.model.event.feed

import java.time.Instant
import java.util.*

data class EventFilterCriteria(
    val minStartAt: Instant? = null,
    val maxEndAt: Instant? = null,
    val sourceNames: List<String> = listOf(),
    val eventOccurrenceIds: List<UUID> = listOf(),
    val categoryIds: List<UUID> = listOf(),
    val excludedEventIds: List<UUID> = listOf(),
    val excludedOccurrenceIds: List<UUID> = listOf()
)

data class EventGroupByStartAtCriteria(
    val searchTerm: String? = null,
    val minStartAt: Instant? = null,
    val sourceNames: List<String> = listOf(),
    val eventIds: List<UUID> = listOf(),
    val categoryIds: List<UUID> = listOf(),
    val excludedEventIds: List<UUID> = listOf(),
    val excludedOccurrenceIds: List<UUID> = listOf()
)