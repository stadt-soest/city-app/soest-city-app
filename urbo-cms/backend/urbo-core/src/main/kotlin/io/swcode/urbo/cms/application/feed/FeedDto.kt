package io.swcode.urbo.cms.application.feed

import io.swcode.urbo.cms.domain.model.event.EventOccurrence
import io.swcode.urbo.cms.domain.model.event.EventStatus
import io.swcode.urbo.cms.domain.model.news.News
import io.swcode.urbo.cms.domain.model.shared.Image
import io.swcode.urbo.cms.search.meili.api.SearchIndexedDto
import io.swcode.urbo.common.utils.extractHost
import java.time.Instant
import java.util.*

data class EventFeed(
    override val id: UUID,
    override val source: String,
    override val categoryIds: List<UUID>,
    override val type: FeedType = FeedType.EVENTS,
    override val title: String,
    override val publishedDate: Instant,
    override val image: Image?,
    val startAt: Instant,
    val eventOccurrenceId: UUID,
    val status: EventStatus,
) : Feed {
    companion object {
        fun fromDomain(entity: EventOccurrence): EventFeed {
            val earliestOccurrence = entity.event.occurrences.minByOrNull { it.startAt }
            return EventFeed(
                id = entity.event.id,
                categoryIds = entity.event.sourceCategories.mapNotNull { it.category?.id },
                source = entity.source.extractHost(),
                title = entity.event.title,
                publishedDate = entity.dateCreated,
                startAt = earliestOccurrence?.startAt ?: entity.startAt,
                eventOccurrenceId = earliestOccurrence?.id ?: entity.id,
                image = entity.event.image,
                status = earliestOccurrence?.status ?: EventStatus.SCHEDULED,
            )
        }
    }
}

data class NewsFeed(
    override val id: UUID,
    override val categoryIds: List<UUID>,
    override val source: String,
    override val type: FeedType = FeedType.NEWS,
    override val image: Image?,
    override val title: String,
    override val publishedDate: Instant
) : Feed {
    companion object {
        fun fromDomain(entity: News): NewsFeed {
            return NewsFeed(
                id = entity.id,
                categoryIds = entity.sourceCategories.mapNotNull { it.category?.id },
                source = entity.source,
                title = entity.title,
                publishedDate = entity.date,
                image = entity.image
            )
        }
    }
}

sealed interface Feed : SearchIndexedDto {
    val id: UUID
    val categoryIds: List<UUID>
    val source: String
    val image: Image?
    val type: FeedType
    val title: String
    val publishedDate: Instant
}

enum class FeedType {
    NEWS, EVENTS
}