package io.swcode.urbo.cms.domain.model.citizenService

import io.swcode.urbo.cms.application.citizenService.SourceType
import io.swcode.urbo.cms.application.citizenService.dto.CitizenServiceResponseDto
import io.swcode.urbo.cms.application.citizenService.dto.CreateCitizenServiceDto
import io.swcode.urbo.cms.domain.model.citizenService.CitizenServiceSpecification.searchByKeyword
import io.swcode.urbo.cms.domain.model.citizenService.tag.TagService
import io.swcode.urbo.common.error.ErrorType
import io.swcode.urbo.common.error.UrboException
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
@Transactional
class CitizenServiceService(
    private val citizenServiceRepository: CitizenServiceRepository,
    private val tagService: TagService,
) {

    fun createCitizenService(createCitizenServiceDto: CreateCitizenServiceDto): CitizenService =
        citizenServiceRepository.save(createCitizenServiceDto.toEntity())

    fun delete(id: UUID) {
        val citizenService = citizenServiceRepository.findByIdOrNull(id)
            ?: throw UrboException("CitizenService not found with id: $id", ErrorType.RESOURCE_NOT_FOUND)
        citizenServiceRepository.delete(citizenService)
    }

    fun save(citizenServiceRequest: CitizenService, preserveVisibilityAndFavorite: Boolean = false): CitizenService {
        val existingCitizenService =
            citizenServiceRequest.externalId?.let { citizenServiceRepository.findByExternalId(it) }

        return if (existingCitizenService != null) {
            update(citizenServiceRequest, preserveVisibilityAndFavorite)
        } else {
            citizenServiceRepository.save(citizenServiceRequest)
        }
    }

    fun update(citizenServiceRequest: CitizenService, preserveVisibilityAndFavorite: Boolean = false): CitizenService {
        val citizenService = findCitizenService(citizenServiceRequest)
            ?: throw UrboException(
                "CitizenService not found with externalId: ${citizenServiceRequest.externalId} or id: ${citizenServiceRequest.id}",
                ErrorType.RESOURCE_NOT_FOUND
            )

        return saveUpdatedCitizenService(citizenService, citizenServiceRequest, preserveVisibilityAndFavorite)
    }

    fun getCitizenServices(
        pageable: Pageable,
        tagId: UUID? = null,
        visible: Boolean?,
        favorite: Boolean?,
        withoutTag: Boolean = false,
        sourceType: SourceType? = null
    ): Page<CitizenServiceResponseDto> {
        return citizenServiceRepository.findAll(
            CitizenServiceSpecification.withFilters(tagId, visible, favorite, withoutTag, sourceType), pageable
        ).map(CitizenServiceResponseDto::fromDomain)
    }

    fun searchCitizenServices(
        keyword: String?,
        pageable: Pageable
    ): Page<CitizenServiceResponseDto> {
        return citizenServiceRepository.findAll(searchByKeyword(keyword), pageable)
            .map(CitizenServiceResponseDto::fromDomain)
    }

    fun getCitizenService(id: UUID): CitizenServiceResponseDto {
        val citizenService = citizenServiceRepository.findByIdOrNull(id)
            ?: throw UrboException("citizenService not found with id: $id", ErrorType.RESOURCE_NOT_FOUND)
        return CitizenServiceResponseDto.fromDomain(citizenService)
    }

    fun getCitizenServicesByTag(tagId: UUID, pageable: Pageable): Page<CitizenServiceResponseDto> {
        return citizenServiceRepository.findByTagId(tagId, pageable).map {
            CitizenServiceResponseDto.fromDomain(it)
        }
    }

    fun updateVisibility(id: UUID, visible: Boolean) {
        val citizenService = citizenServiceRepository.findByIdOrNull(id)
            ?: throw UrboException("citizenService not found with id: $id", ErrorType.RESOURCE_NOT_FOUND)
        citizenService.visible = visible
        citizenServiceRepository.save(citizenService)
    }

    fun updateFavorite(id: UUID, favorite: Boolean) {
        val citizenService = citizenServiceRepository.findByIdOrNull(id)
            ?: throw UrboException("citizenService not found with id: $id", ErrorType.RESOURCE_NOT_FOUND)
        citizenService.favorite = favorite
        citizenServiceRepository.save(citizenService)
    }

    fun updateTagAssignments(tagId: UUID, citizenServiceIds: List<UUID>) {
        val tag = tagService.getTagById(tagId)

        citizenServiceRepository.findAll()
            .filter { it.tags.contains(tag) }
            .onEach { it.removeTag(tag) }
            .also { citizenServiceRepository.saveAll(it) }

        citizenServiceRepository.findAllById(citizenServiceIds)
            .takeIf { it.size == citizenServiceIds.size }
            ?.onEach { it.addTag(tag) }
            ?.also { citizenServiceRepository.saveAll(it) }
            ?: throw UrboException("One or more Citizen Services not found", ErrorType.RESOURCE_NOT_FOUND)
    }

    private fun findCitizenService(citizenServiceRequest: CitizenService): CitizenService? {
        return citizenServiceRequest.externalId?.let { findByExternalId(it) } ?: findById(citizenServiceRequest.id)
    }

    private fun findByExternalId(externalId: String): CitizenService? {
        return citizenServiceRepository.findByExternalId(externalId)
    }

    private fun findById(id: UUID): CitizenService? {
        return citizenServiceRepository.findById(id).orElse(null)
    }

    private fun saveUpdatedCitizenService(
        existingService: CitizenService,
        updatedService: CitizenService,
        preserveVisibilityAndFavorite: Boolean = false,
    ): CitizenService {
        return citizenServiceRepository.save(existingService.updateFrom(updatedService, preserveVisibilityAndFavorite))
    }
}
