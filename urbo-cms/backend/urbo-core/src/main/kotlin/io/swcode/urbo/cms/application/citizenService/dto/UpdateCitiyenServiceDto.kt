package io.swcode.urbo.cms.application.citizenService.dto

import io.swcode.urbo.cms.domain.model.citizenService.CitizenService
import java.util.*

data class UpdateCitizenServiceDto(
    val title: String,
    val description: String,
    val url: String,
    val visible: Boolean,
    val favorite: Boolean,
) {
    fun toEntity(id: UUID): CitizenService {
        return CitizenService(
            id = id,
            title = title,
            description = description,
            url = url,
            visible = visible,
            favorite = favorite
        )
    }
}