package io.swcode.urbo.cms.application.citizenService.dto

import io.swcode.urbo.cms.domain.model.citizenService.CitizenService
import java.util.*

data class CreateCitizenServiceDto(
    val title: String,
    val description: String,
    val url: String,
    val visible: Boolean,
    val favorite: Boolean,
) {
    fun toEntity(): CitizenService {
        return CitizenService(
            id = UUID.randomUUID(),
            title = title,
            description = description,
            url = url,
            visible = visible,
            favorite = favorite,
        )
    }
}