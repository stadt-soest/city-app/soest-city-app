package io.swcode.urbo.cms.application.feed

import io.swcode.urbo.cms.application.shared.image.ImageDto
import io.swcode.urbo.cms.domain.model.event.EventStatus
import io.swcode.urbo.cms.domain.model.event.feed.EventFeedDto
import io.swcode.urbo.common.date.toBerlinInstant
import java.time.Instant
import java.util.UUID

data class EventFeedResponseDto(
    val id: UUID,
    val eventOccurrenceId: UUID,
    val title: String,
    val status: EventStatus,
    val startAt: Instant,
    val dateCreated: Instant,
    val image: ImageDto?
) {
    companion object {
        fun fromDomain(eventFeedDto: EventFeedDto): EventFeedResponseDto {
            return EventFeedResponseDto(
                eventFeedDto.id,
                eventFeedDto.eventOccurrenceId,
                eventFeedDto.title,
                eventFeedDto.status,
                eventFeedDto.startAt.toBerlinInstant(),
                eventFeedDto.dateCreated,
                eventFeedDto.image?.let { ImageDto.fromEntity(it) }
            )
        }
    }
}