package io.swcode.urbo.cms.domain.model.news

import io.swcode.urbo.cms.application.citizenService.SourceType
import io.swcode.urbo.cms.application.news.dto.CreateNewsDto
import io.swcode.urbo.cms.application.news.dto.NewsResponseDto
import io.swcode.urbo.cms.application.news.dto.UpdateNewsDto
import io.swcode.urbo.cms.domain.command.DownloadNewsImageCommand
import io.swcode.urbo.cms.domain.model.category.Category
import io.swcode.urbo.cms.domain.model.category.CategoryRepository
import io.swcode.urbo.cms.domain.model.category.CategoryType
import io.swcode.urbo.cms.domain.model.category.SourceCategory
import io.swcode.urbo.command.api.CommandBus
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.UUID

@Service
class NewsService(
    private val newsRepository: NewsRepository,
    private val categoryRepository: CategoryRepository,
    private val commandBus: CommandBus
) {

    @Transactional
    fun save(news: News): News {
        val existingNews = news.externalId?.let {
            newsRepository.findNewsByExternalId(news.externalId)
        }

        return if (existingNews != null) {
            newsRepository.save(existingNews.updateFrom(news))
        } else {
            val savedNews = newsRepository.save(news)
            savedNews.image?.let { image ->
                commandBus.submitAsyncCommand(
                    DownloadNewsImageCommand(
                        newsId = savedNews.id,
                        image = image
                    )
                )
            }
            savedNews
        }
    }

    @Transactional
    fun createNews(createNewsDto: CreateNewsDto): News {
        val categories = getCategories(createNewsDto.categoryIds)
        val sourceCategories = mapToSourceCategories(categories,createNewsDto.title)
        return save(createNewsDto.toEntity(sourceCategories))
    }

    @Transactional
    fun updateNews(id: UUID, updateNewsDto: UpdateNewsDto): News {
        val news = newsRepository.getNewsById(id)

        val categories = getCategories(updateNewsDto.categoryIds)
        val sourceCategories = mapToSourceCategories(categories,updateNewsDto.title)
        return save(updateNewsDto.updateEntity(news, sourceCategories))
    }

    @Transactional
    fun deleteNews(id: UUID) {
        newsRepository.getNewsById(id).let {
            newsRepository.delete(it)
        }
    }

    @Transactional(readOnly = true)
    fun getAllNews(
        pageable: Pageable,
        categoryIds: List<UUID>? = emptyList(),
        sources: List<String>? = emptyList(),
        exclude: List<UUID>? = emptyList(),
        sourceType: SourceType? = null,
        highlight: Boolean? = null,
    ): Page<NewsResponseDto> {
        val spec = Specification.where<News>(null)
            .and(NewsSpecification.byCategoryIds(categoryIds))
            .and(NewsSpecification.bySources(sources))
            .and(NewsSpecification.excludeIds(exclude))
            .and(NewsSpecification.bySourceType(sourceType))
            .and(NewsSpecification.byHighlight(highlight))

        return newsRepository.findAll(spec, pageable).map(NewsResponseDto::fromEntity)
    }


    @Transactional(readOnly = true)
    fun getNews(id: UUID): NewsResponseDto {
        val news = newsRepository.getNewsById(id)
        return NewsResponseDto.fromEntity(news)
    }

    @Transactional(readOnly = true)
    fun searchNews(keyword: String?, pageable: Pageable): Page<NewsResponseDto> {
        val spec = NewsSpecification.searchByKeyword(keyword)
        return newsRepository.findAll(spec, pageable).map(NewsResponseDto::fromEntity)
    }

    fun getAllSources(): List<String> = newsRepository.findDistinctSources()

    private fun getCategories(categoryIds: List<UUID>): List<Category> = categoryRepository.findAllById(categoryIds)

    private fun mapToSourceCategories(categories: List<Category>, name: String): List<SourceCategory> {
        return categories.map { category ->
            SourceCategory(name, type = CategoryType.NEWS, category = category)
        }
    }

    fun highlightNews(id: UUID, highlight: Boolean): News {
        return newsRepository.getNewsById(id).let { news ->
            news.highlight = highlight
            newsRepository.save(news)
        }
    }
}

