package io.swcode.urbo.cms.domain.model.waste

import io.swcode.urbo.cms.search.index.SearchIndexEntityListener
import io.swcode.urbo.cms.search.meili.api.SearchIndexedEntity
import jakarta.persistence.*
import java.time.Instant
import java.util.*

@Entity
@EntityListeners(SearchIndexEntityListener::class)
data class WasteCollectionLocation(
    @Id
    @Column(name = "id")
    override val id: UUID,

    override var visible: Boolean = true,

    @Column(name = "waste_id")
    val wasteId: String,

    @Column(name = "city")
    var city: String,

    @Column(name = "street")
    var street: String,

    @OneToMany(cascade = [CascadeType.ALL], mappedBy = "wasteCollectionLocation", orphanRemoval = true)
    @OrderBy("date ASC")
    var wasteCollectionDates: MutableList<WasteCollectionDate> = mutableListOf()
) : SearchIndexedEntity {
    fun updateFrom(wasteCollectionLocation: WasteCollectionLocation): WasteCollectionLocation {
        city = wasteCollectionLocation.city
        street = wasteCollectionLocation.street

        wasteCollectionDates.removeIf { it.wasteType != WasteType.CHRISTMAS_TREE }
        wasteCollectionLocation
            .wasteCollectionDates
            .filter { it.wasteType != WasteType.CHRISTMAS_TREE }
            .forEach { newDate ->
                newDate.wasteCollectionLocation = this
                wasteCollectionDates.add(newDate)
            }

        return this
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as WasteCollectionLocation

        return wasteId == other.wasteId
    }

    override fun hashCode(): Int {
        return wasteId.hashCode()
    }

    override fun toString(): String {
        return "WasteCollectionLocation(id=$id, wasteId='$wasteId', city='$city', street='$street')"
    }
}

@Entity
data class WasteCollectionDate(

    @Id
    @Column(name = "id")
    val id: UUID,

    @Column(name = "date")
    val date: Instant,

    @Column(name = "waste_type")
    @Enumerated(EnumType.STRING)
    val wasteType: WasteType,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "waste_collection_location_id", nullable = false)
    var wasteCollectionLocation: WasteCollectionLocation
) {
    override fun toString(): String {
        return "WasteCollectionDate(id=$id, date=$date, wasteType=$wasteType)"
    }
}

enum class WasteType {
    WASTEPAPER,
    YELLOW_BAG,
    BIOWASTE,
    RESIDUAL_WASTE_14DAILY,
    RESIDUAL_WASTE_4WEEKLY,
    DIAPER_WASTE,
    CHRISTMAS_TREE,
}
