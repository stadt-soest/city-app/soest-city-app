package io.swcode.urbo.cms.domain.model.feedback

import io.swcode.urbo.cms.domain.model.feedback.question.FeedbackQuestion
import jakarta.persistence.*
import java.util.*

@Entity
class FeedbackForm(
    @Id val id: UUID,

    @Column(unique = true)
    val version: Int,

    @OneToMany(
        mappedBy = "feedbackForm",
        cascade = [CascadeType.ALL]
    ) val questions: MutableList<FeedbackQuestion> = mutableListOf()
) {
    fun findQuestionById(questionId: UUID): FeedbackQuestion? {
        return questions.find { it.id == questionId }
    }
}
