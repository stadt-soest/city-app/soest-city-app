package io.swcode.urbo.cms.application.waste

import io.swcode.urbo.cms.domain.model.waste.service.WasteService
import io.swcode.urbo.common.logging.createLogger
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class WasteScheduler(
    private val wasteAdapter: WasteAdapter,
    private val wasteService: WasteService
) {

    @Scheduled(fixedRateString = "\${urbo.scheduler.citizen-service.fixed-rate}")
    @SchedulerLock(
        name = "wasteCollection",
        lockAtMostFor = "\${urbo.scheduler.waste-collection.lock.at-most}",
        lockAtLeastFor = "\${urbo.scheduler.waste-collection.lock.at-least}"
    )
    fun getAndCreateWasteCollection() {
        try {
            wasteAdapter.getWasteCollections().forEach {
                wasteService.save(it)
            }
        } catch (ex: Exception) {
            logger.error("Exception occurred in getAndCreateWasteCollection: {}", ex.message, ex)
        }
    }

    companion object {
        private val logger = createLogger()
    }
}