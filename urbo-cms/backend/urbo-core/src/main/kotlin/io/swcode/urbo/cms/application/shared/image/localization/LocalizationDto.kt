package io.swcode.urbo.cms.application.shared.image.localization

import io.swcode.urbo.cms.domain.model.shared.Language
import io.swcode.urbo.cms.domain.model.shared.Localization
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Size

data class LocalizationDto(
    @field:Size(max = 255) @field:NotBlank val value: String,
    val language: Language
) {
    fun toLocalization() = Localization(value, language)
}

fun Localization.toDto() = LocalizationDto(value, language)