package io.swcode.urbo.cms.domain.model.category

import jakarta.persistence.CascadeType
import jakarta.persistence.Entity
import jakarta.persistence.EnumType
import jakarta.persistence.Enumerated
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne

@Entity
data class SourceCategory(
    @Id
    val name: String,
    @Enumerated(EnumType.STRING)
    val type: CategoryType,
    @ManyToOne(cascade = [CascadeType.MERGE, CascadeType.REFRESH])
    @JoinColumn(name = "category_id")
    val category: Category? = null,
) {
    override fun hashCode(): Int {
        return name.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other == null || other !is SourceCategory) {
            return false
        }
        return name == other.name
    }
}