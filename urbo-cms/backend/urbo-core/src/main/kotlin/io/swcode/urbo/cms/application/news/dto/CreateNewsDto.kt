package io.swcode.urbo.cms.application.news.dto

import io.swcode.urbo.cms.domain.model.category.SourceCategory
import io.swcode.urbo.cms.domain.model.news.News
import io.swcode.urbo.cms.domain.model.shared.Image
import io.swcode.urbo.common.date.toBerlinInstant
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotEmpty
import jakarta.validation.constraints.Size
import java.time.Instant
import java.util.*

data class CreateNewsDto(
    @field:NotBlank
    @field:Size(max = 100, message = "Title must be at most 100 characters.")
    val title: String,

    @field:Size(max = 200, message = "Subtitle must be at most 200 characters.")
    val subtitle: String? = null,

    @field:NotBlank
    @field:Size(max = 10000, message = "Content must be at most 10,000 characters.")
    val content: String,

    @field:NotBlank
    @field:Size(max = 200, message = "Source must be at most 200 characters.")
    val source: String,

    @field:Size(max = 200, message = "Link must be at most 200 characters.")
    val link: String? = null,

    @field:Size(max = 2000, message = "Image URL must be at most 2000 characters.")
    val imageUrl: String? = null,

    @field:NotEmpty(message = "At least one category must be selected.")
    val categoryIds: List<UUID> = listOf(),

    @field:Size(max = 100, message = "Each author name must be at most 100 characters.")
    val authors: List<@Size(max = 100) String>? = listOf(),

    val highlight: Boolean? = null
) {
    fun toEntity(categories: List<SourceCategory>): News {
        return News(
            id = UUID.randomUUID(),
            title = this.title,
            subtitle = this.subtitle,
            date = Instant.now().toBerlinInstant(),
            content = this.content,
            source = this.source,
            link = this.link,
            image = this.imageUrl?.let { Image(it) },
            sourceCategories = categories.toMutableList(),
            authors = this.authors?.toMutableList() ?: mutableListOf(),
            highlight = highlight ?: false
        )
    }
}
