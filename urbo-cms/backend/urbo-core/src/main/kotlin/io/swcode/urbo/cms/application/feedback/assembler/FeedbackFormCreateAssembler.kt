package io.swcode.urbo.cms.application.feedback.assembler

import io.swcode.urbo.cms.application.feedback.dto.FeedbackFormCreateDto
import io.swcode.urbo.cms.application.feedback.dto.FeedbackOptionDto
import io.swcode.urbo.cms.application.feedback.dto.FeedbackQuestionCreateDto
import io.swcode.urbo.cms.application.shared.image.localization.LocalizationDto
import io.swcode.urbo.cms.domain.model.feedback.FeedbackForm
import io.swcode.urbo.cms.domain.model.feedback.question.FeedbackQuestion
import io.swcode.urbo.cms.domain.model.feedback.response.FeedbackOption
import io.swcode.urbo.cms.domain.model.shared.Localization
import java.util.*


fun FeedbackFormCreateDto.assembleFeedbackForm(version: Int): FeedbackForm {
    val feedbackForm = FeedbackForm(
        id = UUID.randomUUID(),
        version = version,
        questions = mutableListOf()
    )

    feedbackForm.questions.addAll(this.questions.map { assembleFeedbackQuestion(it, feedbackForm) })
    return feedbackForm
}

private fun assembleFeedbackQuestion(dto: FeedbackQuestionCreateDto, feedbackForm: FeedbackForm): FeedbackQuestion {
    val question = FeedbackQuestion(
        id = UUID.randomUUID(),
        questionType = dto.questionType,
        questionName = dto.questionName.map { assembleLocalization(it) },
        possibleOptions = mutableListOf(),
        feedbackForm = feedbackForm
    )

    question.possibleOptions.addAll(dto.possibleOptions.map { assembleFeedbackOption(it, question) })
    return question
}

private fun assembleFeedbackOption(dto: FeedbackOptionDto, question: FeedbackQuestion): FeedbackOption {
    return FeedbackOption(
        id = UUID.randomUUID(),
        localizedTexts = dto.localizedTexts.map { assembleLocalization(it) },
        question = question
    )
}

private fun assembleLocalization(dto: LocalizationDto): Localization {
    return Localization(
        value = dto.value,
        language = dto.language
    )
}