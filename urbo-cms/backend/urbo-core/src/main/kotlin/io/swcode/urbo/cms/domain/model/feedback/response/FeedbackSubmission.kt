package io.swcode.urbo.cms.domain.model.feedback.response

import io.swcode.urbo.cms.domain.model.feedback.FeedbackForm
import io.swcode.urbo.cms.domain.model.feedback.question.FeedbackQuestion
import io.swcode.urbo.cms.domain.model.shared.Localization
import jakarta.persistence.*
import java.util.*

@Entity
class FeedbackSubmission(
    @Id
    val id: UUID,

    val submissionTime: Date,

    val deduplicationKey: String,

    @Embedded
    val environment: Environment,

    @ManyToOne
    val feedbackForm: FeedbackForm,

    @OneToMany(mappedBy = "feedbackSubmission", cascade = [CascadeType.ALL])
    val answers: MutableList<FeedbackAnswer> = mutableListOf()
) {
    fun addAnswer(question: FeedbackQuestion, selectedOption: FeedbackOption?, freeText: String) {
        val answer = FeedbackAnswer(
            id = UUID.randomUUID(),
            feedbackSubmission = this,
            question = question,
            selectedOption = selectedOption,
            freeAnswerOrEmpty = freeText
        )
        answers.add(answer)
    }
}

@Entity
class FeedbackOption(
    @Id
    val id: UUID,

    @ManyToOne
    val question: FeedbackQuestion,

    @ElementCollection
    @CollectionTable(
        name = "feedback_option_localized_text",
        joinColumns = [JoinColumn(name = "option_id")]
    )
    val localizedTexts: List<Localization>
)


@Entity
class FeedbackAnswer(
    @Id
    val id: UUID,

    @ManyToOne
    var feedbackSubmission: FeedbackSubmission? = null,

    @ManyToOne
    val question: FeedbackQuestion,

    @ManyToOne
    val selectedOption: FeedbackOption?,

    val freeAnswerOrEmpty: String
)

@Embeddable
data class Environment(
    val deviceModelName: String,
    val operatingSystem: OperatingSystem,
    val app: AppDetails,
    val appSettings: String
)

@Embeddable
data class OperatingSystem(
    val name: String,
    val version: String
)

@Embeddable
data class AppDetails(
    val version: String,
    val browserName: String,
    val usedAs: String
)
