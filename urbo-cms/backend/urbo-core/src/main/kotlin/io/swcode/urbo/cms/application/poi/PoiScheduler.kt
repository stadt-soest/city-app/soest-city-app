package io.swcode.urbo.cms.application.poi

import io.swcode.urbo.common.logging.createLogger
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class PoiScheduler(private val poiUpdateService: PoiUpdateService) {

    @Scheduled(fixedRateString = "\${urbo.scheduler.poi.fixed-rate}")
    @SchedulerLock(
        name = "poi",
        lockAtMostFor = "\${urbo.scheduler.poi.lock.at-most}",
        lockAtLeastFor = "\${urbo.scheduler.poi.lock.at-least}"
    )
    fun getAndCreatePois() {
        try {
            poiUpdateService.update()
        } catch (ex: Exception) {
            logger.error("Exception occurred in getAndCreatePois: {}", ex.message, ex)
        }
    }

    companion object {
        private val logger = createLogger()
    }
}
