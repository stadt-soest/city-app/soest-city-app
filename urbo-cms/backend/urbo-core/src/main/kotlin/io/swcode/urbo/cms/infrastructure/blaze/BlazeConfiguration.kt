package io.swcode.urbo.cms.infrastructure.blaze

import com.blazebit.persistence.Criteria
import com.blazebit.persistence.CriteriaBuilderFactory
import jakarta.persistence.EntityManagerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class BlazeConfiguration {
    @Bean
    fun criteriaBuilderFactory(entityManagerFactory: EntityManagerFactory): CriteriaBuilderFactory? {
        return Criteria.getDefault().createCriteriaBuilderFactory(entityManagerFactory)
    }
}