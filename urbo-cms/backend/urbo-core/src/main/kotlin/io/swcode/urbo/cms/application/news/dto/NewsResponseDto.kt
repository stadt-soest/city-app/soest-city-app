package io.swcode.urbo.cms.application.news.dto

import io.swcode.urbo.cms.application.shared.category.MappedCategoryDto
import io.swcode.urbo.cms.application.shared.image.ImageDto
import io.swcode.urbo.cms.application.shared.image.ImageFileDto
import io.swcode.urbo.cms.domain.model.news.News
import io.swcode.urbo.cms.search.meili.api.SearchIndexedDto
import java.time.Instant
import java.util.*

data class NewsResponseDto(
    val id: UUID,
    val title: String,
    val subtitle: String?,
    val date: Instant,
    val modified: Instant?,
    val content: String,
    val source: String,
    val link: String?,
    val authors: List<String> = mutableListOf(),
    val categories: List<MappedCategoryDto> = mutableListOf(),
    val image: ImageDto? = null,
    val editable: Boolean,
    val highlight: Boolean
) : SearchIndexedDto {
    companion object {
        fun fromEntity(
            news: News
        ) = NewsResponseDto(
            id = news.id,
            title = news.title,
            subtitle = news.subtitle,
            date = news.date,
            modified = news.modified,
            content = news.content,
            source = news.source,
            link = news.link,
            authors = news.authors?.toList() ?: mutableListOf(),
            categories = news.sourceCategories.map { MappedCategoryDto.of(it) },
            image = news.image?.let { image ->
                ImageDto(
                    imageSource = image.source,
                    thumbnail = image.thumbnail?.let {
                        ImageFileDto(it.filename, it.width, it.height)
                    },
                    highRes = image.highRes?.let {
                        ImageFileDto(it.filename, it.width, it.height)
                    },
                    copyrightHolder = image.copyrightHolder,
                    description = image.description
                )
            },
            editable = news.externalId.isNullOrEmpty(),
            highlight = news.highlight
        )
    }
}
