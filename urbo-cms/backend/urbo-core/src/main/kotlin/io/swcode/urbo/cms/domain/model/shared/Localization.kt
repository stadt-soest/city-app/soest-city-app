package io.swcode.urbo.cms.domain.model.shared

import jakarta.persistence.Embeddable
import jakarta.persistence.EnumType
import jakarta.persistence.Enumerated

@Embeddable
class Localization(
    val value: String,
    @Enumerated(EnumType.STRING)
    val language: Language
)