package io.swcode.urbo.cms.domain.model.event

import io.swcode.urbo.cms.domain.event.image.EntityType
import io.swcode.urbo.cms.domain.event.image.ImageSourceChangedEvent
import io.swcode.urbo.cms.domain.model.category.SourceCategory
import io.swcode.urbo.cms.domain.model.shared.GeoCoordinates
import io.swcode.urbo.cms.domain.model.shared.Image
import io.swcode.urbo.cms.search.index.SearchIndexEntityListener
import io.swcode.urbo.cms.search.meili.api.SearchIndexedEntity
import jakarta.persistence.*
import org.hibernate.annotations.SQLRestriction
import org.springframework.data.domain.AbstractAggregateRoot
import java.time.Instant
import java.util.*

@Entity
data class Event(
    @Id val id: UUID,

    var title: String,

    var subTitle: String?,

    var description: String?,

    @Embedded var image: Image? = null,

    @Embedded var location: EventLocation,

    @Embedded var contactPoint: EventContactPoint?,

    @Embedded var offer: EventOffer? = null,

    @ManyToMany(cascade = [CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE])
    @JoinTable(
        name = "event_source_category",
        joinColumns = [JoinColumn(name = "event_id")],
        inverseJoinColumns = [JoinColumn(name = "source_category_name")]
    )
    @SQLRestriction(value = "type='EVENTS'")
    var sourceCategories: MutableList<SourceCategory> = mutableListOf(),

    @OneToMany(mappedBy = "event", cascade = [CascadeType.ALL], orphanRemoval = true)
    @OrderBy("startAt ASC, endAt ASC")
    var occurrences: MutableList<EventOccurrence> = mutableListOf()

) : AbstractAggregateRoot<Event>() {
    fun updateFrom(other: Event): Event {
        this.title = other.title
        this.description = other.description
        this.location = other.location
        this.contactPoint = other.contactPoint
        this.offer = other.offer
        this.subTitle = other.subTitle
        this.sourceCategories = other.sourceCategories.toMutableList()

        updateOccurrences(other.occurrences)

        val currentImage = image
        val requestImage = other.image



        if (image?.source != requestImage?.source) {
            registerEvent(
                ImageSourceChangedEvent(
                    id = id,
                    EntityType.EVENT,
                    image = currentImage,
                    imageRequest = requestImage
                )
            )
        }

        return this
    }

    fun removeOccurrence(externalId: String) {
        occurrences.removeIf { it.externalId == externalId }
    }

    private fun updateOccurrences(other: List<EventOccurrence>) {
        val existingOccurrences = this.occurrences.associateBy { it.externalId }
        val newOccurrences = other.associateBy { it.externalId }

        this.occurrences.removeAll { it.externalId !in newOccurrences }

        newOccurrences.values.forEach { newOccurrence ->
            val existingOccurrence = existingOccurrences[newOccurrence.externalId]
            if (existingOccurrence != null) {
                existingOccurrence.updateFrom(newOccurrence)
            } else {
                newOccurrence.event = this
                this.occurrences.add(newOccurrence)
            }
        }
    }
}

@Entity
@EntityListeners(SearchIndexEntityListener::class)
class EventOccurrence(
    @Id override val id: UUID,

    override var visible: Boolean = true,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "event_id")
    var event: Event,

    val externalId: String,

    val dateCreated: Instant,

    @Enumerated(EnumType.STRING)
    var status: EventStatus,

    var startAt: Instant,

    var endAt: Instant,

    var source: String,
) : SearchIndexedEntity {
    fun updateFrom(other: EventOccurrence) {
        this.status = other.status
        this.startAt = other.startAt
        this.endAt = other.endAt
        this.source = other.source
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as EventOccurrence

        return externalId == other.externalId
    }

    override fun hashCode(): Int {
        return externalId.hashCode()
    }
}

@Embeddable
class EventLocation(
    val locationName: String?,
    var streetAddress: String?,
    var postalCode: String?,
    var city: String?,
    @Embedded var coordinates: GeoCoordinates? = null,
)

@Embeddable
class EventContactPoint(
    var telephone: String?,
    var email: String?
)

@Embeddable
class EventOffer(
    var url: String? = null,
    var price: String? = null,
    var availability: String? = null,
    var description: String? = null
)

enum class EventStatus {
    SCHEDULED, MOVED_ONLINE, RESCHEDULED, CANCELLED, POSTPONED
}
