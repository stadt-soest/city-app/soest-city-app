package io.swcode.urbo.cms.application.file

import io.swcode.urbo.cms.s3.api.S3Adapter
import io.swcode.urbo.common.utils.generateUUIDFilename
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.io.IOException
import java.time.Duration

@RestController
@RequestMapping("/files")
class FileController(private val s3Adapter: S3Adapter) {

    @GetMapping("/{fileName}")
    fun downloadVisualFile(@PathVariable fileName: String): ResponseEntity<Unit> {
        val signedUrl = s3Adapter.requestSignedDownloadUrl(fileName, Duration.ofDays(1))
        return ResponseEntity.status(302).location(signedUrl.toURI()).build()
    }

    @PostMapping
    fun uploadFile(@RequestParam("file") file: MultipartFile): ResponseEntity<String> {
        return try {
            file.originalFilename.generateUUIDFilename().let { filename ->
                s3Adapter.putS3Object(file.inputStream, filename)
                ResponseEntity.ok(filename)
            }
        } catch (e: IOException) {
            ResponseEntity.badRequest().build()
        }
    }
}