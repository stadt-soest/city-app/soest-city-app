package io.swcode.urbo.cms.infrastructure.search.index

import io.swcode.urbo.cms.application.citizenService.dto.CitizenServiceResponseDto
import io.swcode.urbo.cms.application.news.dto.NewsResponseDto
import io.swcode.urbo.cms.application.poi.dto.PoiResponseDto
import io.swcode.urbo.cms.application.waste.assembler.WasteAssembler
import io.swcode.urbo.cms.domain.model.citizenService.CitizenService
import io.swcode.urbo.cms.domain.model.event.EventOccurrence
import io.swcode.urbo.cms.domain.model.event.feed.EventFeedDto
import io.swcode.urbo.cms.domain.model.news.News
import io.swcode.urbo.cms.domain.model.poi.Poi
import io.swcode.urbo.cms.domain.model.waste.WasteCollectionLocation
import io.swcode.urbo.cms.search.meili.api.SearchIndexedDto
import io.swcode.urbo.cms.search.meili.api.SearchIndexedEntity
import io.swcode.urbo.common.error.UrboException

object SearchIndexEntityMapper {
    fun map(entity: SearchIndexedEntity): SearchIndexedDto {
        return when (entity) {
            is EventOccurrence -> EventFeedDto.fromDomain(entity)
            is News -> NewsResponseDto.fromEntity(entity)
            is WasteCollectionLocation -> WasteAssembler.toResponseDto(entity)
            is Poi -> PoiResponseDto.fromDomain(entity)
            is CitizenService -> CitizenServiceResponseDto.fromDomain(entity)
            else -> throw UrboException("Unsupported search index entity")
        }
    }
}