package io.swcode.urbo.cms.domain.model.event.feed

import com.blazebit.persistence.CTE
import com.blazebit.persistence.CriteriaBuilder
import com.blazebit.persistence.CriteriaBuilderFactory
import com.blazebit.persistence.FullSelectCTECriteriaBuilder
import io.swcode.urbo.cms.application.event.response.EventLocationDto
import io.swcode.urbo.cms.domain.model.event.Event
import io.swcode.urbo.cms.domain.model.event.EventLocation
import io.swcode.urbo.cms.domain.model.event.EventOccurrence
import io.swcode.urbo.cms.domain.model.event.EventStatus
import io.swcode.urbo.cms.domain.model.shared.Image
import jakarta.persistence.Entity
import jakarta.persistence.EntityManager
import jakarta.persistence.Id
import jakarta.persistence.Tuple
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Repository
import java.time.Instant
import java.util.*

private const val EO_START_AT = "eo.startAt"
private const val EO_END_AT = "eo.endAt"
private const val SUB_EO_START_AT = "subEo.startAt"
private const val EO_EVENT_ID = "eo.event.id"
private const val SUB_EO_EVENT_ID = "subEo.event.id"

@Repository
class EventFeedRepository(
    private val builderFactory: CriteriaBuilderFactory,
    private val em: EntityManager
) {

    private fun createBuilder(): CriteriaBuilder<Tuple> {
        return builderFactory.create(em, Tuple::class.java)
            .from(EventOccurrence::class.java, "eo")
            .select("e.id", "id")
            .select("e.title", "title")
            .select("e.image", "image")
            .select(EO_START_AT, "startAt")
            .select(EO_END_AT, "endAt")
            .select("eo.dateCreated", "dateCreated")
            .select("eo.id", "eoId")
            .select("e.location", "location")
            .select("e.subTitle", "subTitle")
            .select("e.description", "description")
            .select("eo.status", "status")
    }

    fun findAll(
        pageable: Pageable,
        criteria: EventFilterCriteria
    ): Page<EventFeedDto> {
        val cb = createBuilder()
            .innerJoinOn(Event::class.java, "e")
            .on("e.id").eqExpression(EO_EVENT_ID)
            .end()
            .andMinStartAtFilter(criteria.minStartAt)
            .andMaxEndAtFilter(criteria.maxEndAt)
            .andEventOccurrenceIdsFilter(criteria.eventOccurrenceIds)
            .andCategoryFilter(criteria.categoryIds)
            .andSourceFilter(criteria.sourceNames)
            .where("e.id").notIn(criteria.excludedEventIds)
            .where("eo.id").notIn(criteria.excludedOccurrenceIds)

        return cb.paginated(pageable)
    }

    fun findAllGroupByStartAt(
        pageable: Pageable,
        criteria: EventGroupByStartAtCriteria
    ): Page<EventFeedDto> {
        val cb = createBuilder()
            .innerJoinOnSubquery(SubQueryCTE::class.java, "cte")
            .from(EventOccurrence::class.java, "subEo")
            .bind("eventId").select(SUB_EO_EVENT_ID)
            .bind("startAt").select("MIN($SUB_EO_START_AT)")
            .andMinStartAtFilter(criteria.minStartAt)
            .andCategoryFilter(criteria.categoryIds)
            .andSourceFilter(criteria.sourceNames)
            .andEventIdsFilter(criteria.eventIds)
            .groupBy(SUB_EO_EVENT_ID)
            .orderByAsc("MIN($SUB_EO_START_AT)")
            .orderByAsc(SUB_EO_EVENT_ID)
            .end()
            .on("cte.eventId").eqExpression(EO_EVENT_ID)
            .on("cte.startAt").eqExpression(EO_START_AT)
            .end()
            .innerJoinOn(Event::class.java, "e")
            .on("e.id").eqExpression(EO_EVENT_ID)
            .end()
            .where("eo.id").notIn(criteria.excludedOccurrenceIds)
            .where("e.id").notIn(criteria.excludedEventIds)
            .andSearchFilter(criteria.searchTerm)

        return cb.paginated(pageable)
    }
}

@Entity
@CTE
class SubQueryCTE(@Id val eventId: UUID, val startAt: Instant)

private fun CriteriaBuilder<Tuple>.paginated(pageable: Pageable): Page<EventFeedDto> {
    pageable.sort.forEach { sort: Sort.Order ->
        if (sort.isAscending) {
            orderByAsc(sort.property)
        } else {
            orderByDesc(sort.property)
        }
    }

    orderByAsc("e.id")
    orderByAsc("eo.id")

    return page(pageable.pageNumber * pageable.pageSize, pageable.pageSize).resultList.let { pagedList ->
        PageImpl(pagedList.map { tuple ->
            val startAt = tuple["startAt", Instant::class.java]
            val endAt = tuple["endAt", Instant::class.java]
            EventFeedDto(
                tuple["id", UUID::class.java],
                tuple["eoId", UUID::class.java],
                tuple["title", String::class.java],
                tuple["status", EventStatus::class.java],
                startAt = startAt,
                startAtUnix = startAt.toEpochMilli(),
                endAt = endAt,
                endAtUnix = endAt.toEpochMilli(),
                tuple["dateCreated", Instant::class.java],
                tuple["image", Image::class.java],
                EventLocationDto.fromEntity(tuple["location", EventLocation::class.java]),
                tuple["subTitle", String::class.java],
                tuple["description", String::class.java]
            )
        }, pageable, pagedList.totalSize)
    }
}

private fun <X> CriteriaBuilder<X>.andSearchFilter(term: String?): CriteriaBuilder<X> {
    return term?.let {
        "%$it%"
    }?.let {
        whereOr()
            .where("e.title").like(false).value(it).noEscape()
            .where("e.subTitle").like(false).value(it).noEscape()
            .where("e.description").like(false).value(it).noEscape()
            .endOr()

    } ?: this
}

private fun <X> FullSelectCTECriteriaBuilder<X>.andMinStartAtFilter(minStartAt: Instant?): FullSelectCTECriteriaBuilder<X> {
    if (minStartAt == null) {
        return this
    }

    return where(SUB_EO_START_AT).ge(minStartAt)
}

private fun <X> FullSelectCTECriteriaBuilder<X>.andSourceFilter(sourceNames: List<String>): FullSelectCTECriteriaBuilder<X> {
    if (sourceNames.isEmpty()) {
        return this
    }

    return where("subEo.source").`in`(sourceNames)
}

private fun <X> FullSelectCTECriteriaBuilder<X>.andCategoryFilter(categoryIds: List<UUID>): FullSelectCTECriteriaBuilder<X> {
    if (categoryIds.isEmpty()) {
        return this
    }

    return whereExists()
        .from(Event::class.java, "e2")
        .innerJoinOn("e2.sourceCategories", "sourceCategory")
        .on("sourceCategory.category.id").`in`(categoryIds)
        .end()
        .where(SUB_EO_EVENT_ID).eqExpression("e2.id")
        .end()
}

private fun <X> CriteriaBuilder<X>.andMinStartAtFilter(minStartAt: Instant?): CriteriaBuilder<X> {
    if (minStartAt == null) {
        return this
    }

    return where("eo.endAt").ge(minStartAt)
}

private fun <X> CriteriaBuilder<X>.andMaxEndAtFilter(maxEndAt: Instant?): CriteriaBuilder<X> {
    if (maxEndAt == null) {
        return this
    }

    return where(EO_START_AT).le(maxEndAt)
}

private fun <X> CriteriaBuilder<X>.andSourceFilter(sourceNames: List<String>): CriteriaBuilder<X> {
    if (sourceNames.isEmpty()) {
        return this
    }

    return where("eo.source").`in`(sourceNames)
}

private fun <X> CriteriaBuilder<X>.andCategoryFilter(categoryIds: List<UUID>): CriteriaBuilder<X> {
    if (categoryIds.isEmpty()) {
        return this
    }

    return whereExists()
        .from(Event::class.java, "e2")
        .innerJoinOn("e2.sourceCategories", "sourceCategory")
        .on("sourceCategory.category.id").`in`(categoryIds)
        .end()
        .where(EO_EVENT_ID).eqExpression("e2.id")
        .end()
}

private fun <X> FullSelectCTECriteriaBuilder<X>.andEventIdsFilter(eventIds: List<UUID>): FullSelectCTECriteriaBuilder<X> {

    if (eventIds.isEmpty()) {
        return this
    }
    return where(SUB_EO_EVENT_ID).`in`(eventIds)
}

private fun <X> CriteriaBuilder<X>.andEventOccurrenceIdsFilter(eventOccurrenceIds: List<UUID>): CriteriaBuilder<X> {
    if (eventOccurrenceIds.isEmpty()) {
        return this
    }
    return where("eo.id").`in`(eventOccurrenceIds)
}