package io.swcode.urbo.cms.application.feedback

import io.swcode.urbo.cms.application.feedback.assembler.FeedbackAssembler
import io.swcode.urbo.cms.application.feedback.dto.FeedbackFormCreateDto
import io.swcode.urbo.cms.application.feedback.dto.FeedbackFormDto
import io.swcode.urbo.cms.application.feedback.dto.FeedbackSubmissionRequestDto
import io.swcode.urbo.cms.application.feedback.dto.FeedbackSubmissionResponseDto
import io.swcode.urbo.cms.domain.model.feedback.FeedbackService
import jakarta.transaction.Transactional
import jakarta.validation.Valid
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@Transactional
class FeedbackController(
    private val feedbackService: FeedbackService
) {

    @GetMapping("/v1/feedback-form")
    fun getFeedbackForm(@RequestParam(required = false) version: Int?): ResponseEntity<FeedbackFormDto> {
        val feedbackForm = feedbackService.getFeedbackForm(version)
        val feedbackFormDTO = FeedbackAssembler.mapToDto(feedbackForm)
        return ResponseEntity.ok(feedbackFormDTO)
    }

    @PostMapping("/v1/feedback-form")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).FEEDBACK_SUBMISSION_WRITE)")
    fun createFeedbackForm(
        @RequestBody @Valid feedbackFormDto: FeedbackFormCreateDto,
    ): ResponseEntity<FeedbackFormDto> {
            val feedbackForm = feedbackService.createFeedbackForm(feedbackFormDto)
            return ResponseEntity.ok(FeedbackAssembler.mapToDto(feedbackForm))
    }

    @PostMapping("/v1/feedback-submissions")
    fun submitFeedback(
        @RequestBody @Valid feedbackSubmissionRequestDto: FeedbackSubmissionRequestDto
    ): ResponseEntity<Unit> {
        feedbackService.submitFeedback(feedbackSubmissionRequestDto)
        return ResponseEntity.ok().build()
    }

    @GetMapping("/v1/feedback-submissions/{id}")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).FEEDBACK_SUBMISSION_READ)")
    fun getFeedback(@PathVariable id: UUID): ResponseEntity<FeedbackSubmissionResponseDto> {
        return feedbackService.getById(id).let {
            ResponseEntity.ok(FeedbackSubmissionResponseDto.fromDomain(it))
        }
    }

    @GetMapping("/v1/feedback-submissions")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).FEEDBACK_SUBMISSION_READ)")
    fun findAllFeedbacks(pageable: Pageable): ResponseEntity<Page<FeedbackSubmissionResponseDto>> {
        return feedbackService.findAllFeedbacks(pageable).let { page ->
            ResponseEntity.ok(page.map { FeedbackSubmissionResponseDto.fromDomain(it) })
        }
    }
}

