package io.swcode.urbo.cms.application.news.dto

import io.swcode.urbo.cms.domain.model.category.SourceCategory
import io.swcode.urbo.cms.domain.model.news.News
import io.swcode.urbo.cms.domain.model.shared.Image
import io.swcode.urbo.common.date.toBerlinInstant
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Size
import java.time.Instant
import java.util.*

data class UpdateNewsDto(
    @field:NotBlank
    @field:Size(max = 100, message = "Title must be at most 100 characters.")
    val title: String,

    @field:Size(max = 200, message = "Subtitle must be at most 200 characters.")
    val subtitle: String?,

    @field:NotBlank
    @field:Size(max = 10000, message = "Content must be at most 10,000 characters.")
    val content: String,

    @field:NotBlank
    @field:Size(max = 200, message = "Source must be at most 200 characters.")
    val source: String,

    @field:Size(max = 200, message = "Link must be at most 200 characters.")
    val link: String?,

    @field:Size(max = 2000, message = "Image URL must be at most 2000 characters.")
    val imageUrl: String?,

    val categoryIds: List<UUID>,

    @field:Size(max = 100, message = "Each author name must be at most 100 characters.")
    val authors: List<@Size(max = 100) String>?,

    val highlight: Boolean? = null
) {
    fun updateEntity(news: News, categories: List<SourceCategory>): News {
        news.title = this.title
        news.subtitle = this.subtitle
        news.content = this.content
        news.source = this.source
        news.link = this.link
        news.highlight = this.highlight ?: false

        if (this.imageUrl != null) {
            news.image = Image(this.imageUrl)
        }

        news.sourceCategories = categories.toMutableList()
        news.authors = this.authors?.toMutableList() ?: mutableListOf()
        news.modified = Instant.now().toBerlinInstant()
        return news
    }
}
