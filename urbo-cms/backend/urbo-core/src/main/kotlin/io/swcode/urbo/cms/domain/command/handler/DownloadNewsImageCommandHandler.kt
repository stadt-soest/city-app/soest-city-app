package io.swcode.urbo.cms.domain.command.handler

import io.swcode.urbo.cms.domain.command.DownloadNewsImageCommand
import io.swcode.urbo.cms.domain.model.news.NewsRepository
import io.swcode.urbo.command.api.CommandHandler
import io.swcode.urbo.common.logging.createLogger
import io.swcode.urbo.core.api.image.ImageProcessingAdapter
import org.springframework.stereotype.Component

@Component
class DownloadNewsImageCommandHandler(
    private val newsRepository: NewsRepository,
    private val imageProcessingAdapter: ImageProcessingAdapter
) : CommandHandler<DownloadNewsImageCommand> {

    companion object {
        private val logger = createLogger()
    }
    override fun handle(command: DownloadNewsImageCommand) {
        newsRepository.findById(command.newsId).orElse(null)?.also { news ->
            val imageProcessingResult = imageProcessingAdapter.downloadAndProcessImage(command.image.source)
            news.image = command.image.from(imageProcessingResult)
            newsRepository.save(news)
        } ?: logger.info("News entity not found ${command.newsId}, no image download required")
    }
}
