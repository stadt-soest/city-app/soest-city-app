package io.swcode.urbo.cms.domain.command.util

import java.io.File
import java.io.InputStream

object TempFileUtil {
    fun createTempFile(inputStream: InputStream): File {
        return File.createTempFile("image", null).apply {
            outputStream().use { fos -> inputStream.copyTo(fos) }
        }
    }
}

