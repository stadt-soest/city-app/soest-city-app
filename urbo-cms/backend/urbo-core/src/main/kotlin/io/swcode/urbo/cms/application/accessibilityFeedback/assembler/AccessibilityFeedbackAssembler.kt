package io.swcode.urbo.cms.application.accessibilityFeedback.assembler

import io.swcode.urbo.cms.application.accessibilityFeedback.dto.AccessibilityRequestDto
import io.swcode.urbo.cms.domain.model.accessibilityFeedback.AccessibilityFeedback
import io.swcode.urbo.cms.domain.model.feedback.response.AppDetails
import io.swcode.urbo.cms.domain.model.feedback.response.Environment
import io.swcode.urbo.cms.domain.model.feedback.response.OperatingSystem
import io.swcode.urbo.common.date.toBerlinInstant
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.*

@Component
object AccessibilityFeedbackAssembler {
    fun toEntity(accessibilityRequestDto: AccessibilityRequestDto): AccessibilityFeedback {
        return AccessibilityFeedback(
            id = UUID.randomUUID(),
            answer = accessibilityRequestDto.answer,
            createdAt = Instant.now().toBerlinInstant(),
            environment = Environment(
                deviceModelName = accessibilityRequestDto.environment.deviceModelName,
                app = AppDetails(
                    version = accessibilityRequestDto.environment.appVersion,
                    browserName = accessibilityRequestDto.environment.browserName,
                    usedAs = accessibilityRequestDto.environment.usedAs
                ),
                operatingSystem = OperatingSystem(
                    name = accessibilityRequestDto.environment.operatingSystemName,
                    version = accessibilityRequestDto.environment.operatingSystemVersion
                ),
                appSettings = accessibilityRequestDto.environment.appSettings
            ),
            imageFilenames = accessibilityRequestDto.imageFilenames.toMutableList()
        )
    }
}
