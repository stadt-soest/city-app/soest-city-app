package io.swcode.urbo.cms.domain.model.category

import io.swcode.urbo.cms.domain.model.category.group.CategoryGroup
import io.swcode.urbo.cms.domain.model.category.group.CategoryGroupRepository
import io.swcode.urbo.cms.domain.model.shared.Localization
import io.swcode.urbo.common.error.ErrorType
import io.swcode.urbo.common.error.UrboException
import jakarta.transaction.Transactional
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.*

@Service
class CategoryService(
    private val categoryRepository: CategoryRepository,
    private val sourceCategoryRepository: SourceCategoryRepository,
    private val categoryGroupRepository: CategoryGroupRepository
) {

    fun createCategory(
        localizedNames: List<Localization>,
        categoryType: CategoryType,
        categoryGroupId: UUID?,
    ): Category {
        return categoryRepository.save(
            Category.of(
                localizedNames,
                categoryType,
                findCategoryGroupOrNull(categoryGroupId),
            )
        )
    }

    fun updateCategory(
        categoryId: UUID,
        localizedNames: List<Localization>,
        sourceCategoryIds: List<String>,
        categoryGroupId: UUID?,
        iconFilename: String?,
        hexColor: CategoryColors?,
    ): Category {
        val category = getCategory(categoryId)
        val categoryGroup = categoryGroupId?.let { categoryGroupRepository.findByIdOrNull(it) }

        return category.update(
            localizedNames,
            sourceCategoryRepository.findAllByTypeAndNameIn(category.type, sourceCategoryIds),
            categoryGroup,
            iconFilename?.let { Icon(it) },
            hexColor,
        ).also {
            categoryRepository.save(it)
        }
    }

    fun getCategory(categoryId: UUID): Category {
        return categoryRepository.findByIdOrNull(categoryId)
            ?: throw UrboException("category.id=$categoryId not found", ErrorType.RESOURCE_NOT_FOUND)
    }

    fun findCategories(categoryType: CategoryType): List<Category> {
        return categoryRepository.findAllByTypeOrderByCategoryGroupSortOrderAscSortOrderAsc(categoryType)
    }

    fun findOrCreateSourceCategories(sourceCategoryNames: List<String>, type: CategoryType): List<SourceCategory> {
        val existingCategories =
            sourceCategoryRepository.findAllByTypeAndNameIn(type, sourceCategoryNames).associateBy { it.name }
        return sourceCategoryNames.map { name ->
            existingCategories[name] ?: SourceCategory(name = name, type = type)
        }
    }

    fun deleteCategory(id: UUID) {
        categoryRepository.deleteById(id)
    }

    fun getSourceCategories(type: CategoryType, mapped: Boolean?): List<SourceCategory> {
        if (mapped != null && !mapped) {
            return sourceCategoryRepository.findAllByTypeAndCategoryIsNull(type)
        }
        if (mapped != null && mapped) {
            return sourceCategoryRepository.findAllByTypeAndCategoryIsNotNull(type)
        }
        return sourceCategoryRepository.findAllByType(type)
    }

    fun findCategoryGroupOrNull(groupId: UUID?): CategoryGroup? {
        return groupId?.let { categoryGroupRepository.findByIdOrNull(it) }
    }

    @Transactional
    fun updateCategoryOrder(categories: List<Category>) {
        categories.forEachIndexed { index, category ->
            val existingCategory = getCategory(category.id)
            existingCategory.sortOrder = index
            categoryRepository.save(existingCategory)
        }
    }
}
