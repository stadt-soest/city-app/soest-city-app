package io.swcode.urbo.cms.domain.model.feedback.question

import io.swcode.urbo.cms.domain.model.feedback.FeedbackForm
import io.swcode.urbo.cms.domain.model.feedback.response.FeedbackOption
import io.swcode.urbo.cms.domain.model.shared.Localization
import jakarta.persistence.*
import java.util.*

@Entity
class FeedbackQuestion(
    @Id
    val id: UUID,

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    val questionType: QuestionType,

    @ElementCollection
    @CollectionTable(name = "feedback_question_localized_name", joinColumns = [JoinColumn(name = "question_id")])
    val questionName: List<Localization>,

    @OneToMany(mappedBy = "question", cascade = [CascadeType.ALL])
    val possibleOptions: MutableList<FeedbackOption> = mutableListOf(),

    @ManyToOne
    val feedbackForm: FeedbackForm
) {
    fun findOptionById(optionId: UUID): FeedbackOption? {
        return possibleOptions.find { it.id == optionId }
    }
}

enum class QuestionType {
    SINGLE_CHOICE, FREE_TEXT
}
