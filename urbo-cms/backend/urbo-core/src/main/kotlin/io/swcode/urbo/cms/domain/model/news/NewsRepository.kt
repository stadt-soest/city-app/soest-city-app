package io.swcode.urbo.cms.domain.model.news

import io.swcode.urbo.common.error.ErrorType
import io.swcode.urbo.common.error.UrboException
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.findByIdOrNull
import java.util.*

interface NewsRepository : JpaRepository<News, UUID>, JpaSpecificationExecutor<News> {
    @Query("SELECT DISTINCT n.source FROM News n")
    fun findDistinctSources(): List<String>
    fun findNewsByExternalId(id: String): News?

    fun getNewsById(id: UUID): News {
        return findByIdOrNull(id)
            ?: throw UrboException("News not found with id: $id", ErrorType.RESOURCE_NOT_FOUND)
    }
}