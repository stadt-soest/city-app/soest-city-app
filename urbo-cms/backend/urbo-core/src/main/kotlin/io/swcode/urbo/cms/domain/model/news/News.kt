package io.swcode.urbo.cms.domain.model.news

import io.swcode.urbo.cms.domain.event.image.EntityType
import io.swcode.urbo.cms.domain.event.image.ImageSourceChangedEvent
import io.swcode.urbo.cms.domain.model.category.SourceCategory
import io.swcode.urbo.cms.domain.model.shared.Image
import io.swcode.urbo.cms.search.index.SearchIndexEntityListener
import io.swcode.urbo.cms.search.meili.api.SearchIndexedEntity
import jakarta.persistence.*
import org.springframework.data.domain.AbstractAggregateRoot
import java.time.Instant
import java.util.*

@Entity
@EntityListeners(SearchIndexEntityListener::class)
data class News(

    @Id
    @Column(name = "id")
    override var id: UUID,

    override var visible: Boolean = true,

    @Column(unique = true)
    val externalId: String? = null,

    var title: String,

    var subtitle: String?,

    var date: Instant,

    var modified: Instant? = null,

    var source: String,

    var content: String,

    var link: String?,

    var highlight: Boolean,

    @Embedded
    var image: Image? = null,

    @ManyToMany(cascade = [CascadeType.ALL])
    @JoinTable(
        name = "news_source_category",
        joinColumns = [JoinColumn(name = "news_id")],
        inverseJoinColumns = [JoinColumn(name = "source_category_name")]
    )
    @org.hibernate.annotations.Where(clause = "type='NEWS'")
    var sourceCategories: MutableList<SourceCategory> = mutableListOf(),

    @ElementCollection
    @CollectionTable(name = "news_authors", joinColumns = [JoinColumn(name = "news_id")])
    @Column(name = "author")
    var authors: MutableList<String>? = mutableListOf(),
) : AbstractAggregateRoot<News>(), SearchIndexedEntity {

    fun updateFrom(newsRequest: News): News {
        title = newsRequest.title
        subtitle = newsRequest.subtitle
        date = newsRequest.date
        modified = newsRequest.modified
        content = newsRequest.content
        link = newsRequest.link
        source = newsRequest.source

        if (newsRequest.image != null) {
            registerEvent(
                ImageSourceChangedEvent(
                    id,
                    EntityType.NEWS,
                    image = image,
                    imageRequest = newsRequest.image!!
                )
            )
        }

        authors = newsRequest.authors ?: mutableListOf()

        return this
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as News

        return id == other.id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}




