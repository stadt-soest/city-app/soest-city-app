package io.swcode.urbo.cms.domain.model.category

import org.springframework.data.jpa.repository.JpaRepository
import java.util.UUID

interface CategoryRepository : JpaRepository<Category, UUID>{
    fun findAllByTypeOrderByCategoryGroupSortOrderAscSortOrderAsc(categoryType: CategoryType): List<Category>
}