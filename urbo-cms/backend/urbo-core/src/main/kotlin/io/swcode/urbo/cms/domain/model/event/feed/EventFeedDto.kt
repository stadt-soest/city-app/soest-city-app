package io.swcode.urbo.cms.domain.model.event.feed

import io.swcode.urbo.cms.application.event.response.EventLocationDto
import io.swcode.urbo.cms.domain.model.event.EventOccurrence
import io.swcode.urbo.cms.domain.model.event.EventStatus
import io.swcode.urbo.cms.domain.model.shared.Image
import io.swcode.urbo.cms.search.meili.api.SearchIndexedDto
import java.time.Instant
import java.util.*

data class EventFeedDto(
    val id: UUID,
    val eventOccurrenceId: UUID,
    val title: String,
    val status: EventStatus,
    val startAt: Instant,
    val startAtUnix: Long,
    val endAt: Instant,
    val endAtUnix: Long,
    val dateCreated: Instant,
    val image: Image?,
    val location: EventLocationDto,
    val subTitle: String?,
    val description: String?,
) : SearchIndexedDto {
    companion object {
        fun fromDomain(entity: EventOccurrence) = EventFeedDto(
            entity.event.id,
            entity.id,
            entity.event.title,
            entity.status,
            entity.startAt,
            entity.startAt.epochSecond,
            entity.endAt,
            entity.endAt.epochSecond,
            entity.dateCreated,
            entity.event.image,
            EventLocationDto.fromEntity(entity.event.location),
            entity.event.subTitle,
            entity.event.description,
        )
    }
}