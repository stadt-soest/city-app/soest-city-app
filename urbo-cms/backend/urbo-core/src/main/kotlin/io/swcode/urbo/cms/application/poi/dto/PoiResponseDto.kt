package io.swcode.urbo.cms.application.poi.dto

import io.swcode.urbo.cms.application.event.response.CoordinatesResponseDto
import io.swcode.urbo.cms.application.shared.category.MappedCategoryDto
import io.swcode.urbo.cms.domain.model.poi.Poi
import io.swcode.urbo.cms.search.meili.api.SearchIndexedDto
import java.time.Instant
import java.util.*

data class PoiResponseDto(
    val id: UUID,
    val name: String,
    val location: PoiLocationDto,
    val dateModified: Instant,
    val url: String?,
    val contactPoint: PoiContactPointDto,
    val categories: List<MappedCategoryDto> = mutableListOf()
): SearchIndexedDto {
    companion object {
        fun fromDomain(poi: Poi): PoiResponseDto {
            return PoiResponseDto(
                poi.id,
                poi.name,
                PoiLocationDto(
                    poi.address.street,
                    poi.address.postalCode,
                    poi.address.city,
                    CoordinatesResponseDto(
                        poi.coordinates.latitude,
                        poi.coordinates.longitude
                    )
                ),
                poi.dateModified,
                poi.url,
                PoiContactPointDto(
                    poi.contactPoint.telephone,
                    poi.contactPoint.email,
                    poi.contactPoint.contactPerson
                ),
                poi.sourceCategories.map { MappedCategoryDto.of(it) }
            )
        }
    }
}

data class PoiLocationDto(
    val street: String?,
    val postalCode: String?,
    val city: String?,
    val coordinates: CoordinatesResponseDto
)

data class PoiContactPointDto(
    val telephone: String,
    val email: String,
    val contactPerson: String
)