package io.swcode.urbo.cms.application.news

import io.swcode.urbo.cms.domain.model.news.NewsService
import io.swcode.urbo.common.logging.createLogger
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class NewsScheduler(
    private val newsAdapter: NewsAdapter,
    private val newsService: NewsService
) {

    @Scheduled(fixedRateString = "\${urbo.scheduler.news.fixed-rate}")
    @SchedulerLock(
        name = "news",
        lockAtMostFor = "\${urbo.scheduler.news.lock.at-most}",
        lockAtLeastFor = "\${urbo.scheduler.news.lock.at-least}"
    )
    fun getAndCreateNews() {
        try {
            newsAdapter.getNews().forEach {
                newsService.save(it)
            }
        } catch (ex: Exception) {
            logger.error("Exception occurred in getAndCreateNews: {}", ex.message, ex)
        }
    }

    companion object {
        private val logger = createLogger()
    }
}
