package io.swcode.urbo.cms.domain.model.feedback

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface FeedbackFormRepository : JpaRepository<FeedbackForm, UUID> {
    fun findByVersion(version: Int): FeedbackForm?
    fun findTopByOrderByVersionDesc(): FeedbackForm?
    @Query("SELECT COALESCE(MAX(version), 0) FROM FeedbackForm")
    fun findMaxVersion(): Int
}
