package io.swcode.urbo.cms.application.citizenService

import io.swcode.urbo.cms.domain.model.citizenService.CitizenServiceService
import io.swcode.urbo.common.logging.createLogger
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class CitizenScheduler(
    private val citizenAdapter: CitizenAdapter,
    private val citizenServiceService: CitizenServiceService
) {

    @Scheduled(fixedRateString = "\${urbo.scheduler.citizen-service.fixed-rate}")
    @SchedulerLock(
        name = "citizenService",
        lockAtMostFor = "\${urbo.scheduler.citizen-service.lock.at-most}",
        lockAtLeastFor = "\${urbo.scheduler.citizen-service.lock.at-least}"
    )
    fun getAndCreateCitizenServices() {
        try {
            citizenAdapter.getCitizenServices().forEach { citizenService ->
                citizenServiceService.save(citizenService, true)
            }
        } catch (ex: Exception) {
            logger.error("Exception occurred in getAndCreateCitizenServices: ${ex.message}", ex)
        }
    }

    companion object {
        private val logger = createLogger()
    }
}
