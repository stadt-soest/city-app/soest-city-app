package io.swcode.urbo.cms.domain.model.waste

import org.springframework.data.jpa.domain.Specification

object WasteSpecifications {
    fun withStreet(street: String?): Specification<WasteCollectionLocation> = Specification { root, _, cb ->
        if (street != null) cb.equal(root.get<String>("street"), street) else cb.conjunction()
    }

    fun withCity(city: String?): Specification<WasteCollectionLocation> = Specification { root, _, cb ->
        if (city != null) cb.equal(root.get<String>("city"), city) else cb.conjunction()
    }

    fun withSearchKeyword(keyword: String?): Specification<WasteCollectionLocation> = Specification { root, _, cb ->
        if (!keyword.isNullOrEmpty()) {
            val predicates = listOfNotNull(
                cb.like(cb.lower(root.get("city")), "%${keyword.lowercase()}%"),
                cb.like(cb.lower(root.get("street")), "%${keyword.lowercase()}%")
            ).toTypedArray()

            cb.or(*predicates)
        } else {
            null
        }
    }
}
