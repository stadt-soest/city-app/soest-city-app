package io.swcode.urbo.cms.domain.model.parking

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import java.util.*

interface ParkingAreaRepository : JpaRepository<ParkingArea, UUID>, JpaSpecificationExecutor<ParkingArea>{
    fun findByExternalId(id: String): ParkingArea?
}
