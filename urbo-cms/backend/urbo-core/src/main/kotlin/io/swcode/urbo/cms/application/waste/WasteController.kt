package io.swcode.urbo.cms.application.waste

import io.swcode.urbo.cms.application.waste.response.UpcomingWastesDto
import io.swcode.urbo.cms.application.waste.response.WasteResponseDto
import io.swcode.urbo.cms.domain.model.waste.service.WasteService
import jakarta.validation.constraints.NotNull
import jakarta.validation.constraints.Size
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.time.Instant
import java.util.*

@RestController
@Validated
@RequestMapping()
class WasteController(
    private val wasteService: WasteService,
) {

    @GetMapping("/v1/waste-collections")
    fun getWasteCollections(
        @RequestParam(required = false) @Size(min = 1, max = 50) city: String?,
        @RequestParam(required = false) @Size(min = 1, max = 100) street: String?,
        @RequestParam(required = false) @Size(min = 1, max = 100) searchKeyword: String?,
        pageable: Pageable
    ): Page<WasteResponseDto> {
        return wasteService.getWasteCollections(city, street, searchKeyword, pageable)
    }

    @GetMapping("/v1_1/waste-collections/{wasteId}/upcoming-waste-collection-dates")
    fun getUpcomingWasteCollectionDates(
        @PathVariable(required = true) @NotNull wasteId: UUID,
        @RequestParam(required = false) startDate: Instant?,
        @RequestParam(required = false) endDate: Instant?,
        @RequestParam(required = false, defaultValue = "") @Size(min = 1, max = 6) wasteTypes: List<String>
    ): ResponseEntity<UpcomingWastesDto> {
        val upcomingWastesDto = wasteService.getWasteDatesForWaste(
            wasteId,
            startDate ?: Instant.now(),
            endDate ?: Instant.parse("9999-12-31T23:59:59.00Z"),
            wasteTypes
        )
        return ResponseEntity.ok(upcomingWastesDto)
    }
}
