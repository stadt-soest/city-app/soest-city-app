package io.swcode.urbo.cms.application.citizenService.dto

import io.swcode.urbo.cms.application.shared.image.localization.LocalizationDto
import io.swcode.urbo.cms.application.shared.image.localization.toDto
import io.swcode.urbo.cms.domain.model.citizenService.Tag
import io.swcode.urbo.cms.domain.model.shared.Localization
import java.util.*

data class TagDto(
    val id: UUID,
    val localizedNames: List<LocalizationDto>
) {
    companion object {
        fun fromEntity(entity: Tag): TagDto {
            return TagDto(
                id = entity.id,
                localizedNames = entity.localizedNames.map { it.toDto() }
            )
        }

        fun toEntity(tagDto: TagDto): Tag {
            return Tag(
                id = UUID.randomUUID(),
                localizedNames = tagDto.localizedNames.map { Localization(it.value, it.language) }
            )
        }
    }
}

data class CreateTagDto(
    val id: UUID?,
    val localizedNames: List<LocalizationDto>
) {
    companion object {
        fun toEntity(createTagDto: CreateTagDto): Tag {
            return Tag(
                id = createTagDto.id ?: UUID.randomUUID(),
                localizedNames = createTagDto.localizedNames.map { Localization(it.value, it.language) }
            )
        }
    }
}
