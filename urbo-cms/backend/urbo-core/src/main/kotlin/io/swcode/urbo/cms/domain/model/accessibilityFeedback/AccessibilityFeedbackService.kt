package io.swcode.urbo.cms.domain.model.accessibilityFeedback

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.util.UUID

@Service
class AccessibilityFeedbackService(
    private val accessibilityFeedbackRepository: AccessibilityFeedbackRepository,
) {
    fun submitAccessibilityFeedback(accessibilityFeedback: AccessibilityFeedback): AccessibilityFeedback {
        return accessibilityFeedbackRepository.save(accessibilityFeedback)
    }

    fun getById(id: UUID): AccessibilityFeedback {
        return accessibilityFeedbackRepository.getReferenceById(id)
    }

    fun findAll(pageable: Pageable): Page<AccessibilityFeedback> {
        return accessibilityFeedbackRepository.findAll(pageable)
    }
}