package io.swcode.urbo.cms.domain.model.news

import io.swcode.urbo.cms.application.citizenService.SourceType
import io.swcode.urbo.cms.domain.model.category.Category
import io.swcode.urbo.cms.domain.model.category.SourceCategory
import jakarta.persistence.criteria.CriteriaBuilder
import jakarta.persistence.criteria.CriteriaQuery
import jakarta.persistence.criteria.Predicate
import jakarta.persistence.criteria.Root
import org.springframework.data.jpa.domain.Specification
import java.util.*

object NewsSpecification {

    fun searchByKeyword(keyword: String?): Specification<News> {
        return Specification { root: Root<News>, _: CriteriaQuery<*>, criteriaBuilder: CriteriaBuilder ->
            if (keyword.isNullOrEmpty()) {
                criteriaBuilder.conjunction()
            } else {
                val titlePredicate: Predicate = criteriaBuilder.like(
                    criteriaBuilder.lower(root["title"]),
                    "%${keyword.lowercase()}%"
                )
                val contentPredicate: Predicate = criteriaBuilder.like(
                    criteriaBuilder.lower(root["content"]),
                    "%${keyword.lowercase()}%"
                )
                criteriaBuilder.or(titlePredicate, contentPredicate)
            }
        }
    }

    fun bySourceType(sourceType: SourceType?): Specification<News> {
        return Specification { root: Root<News>, _: CriteriaQuery<*>?, criteriaBuilder: CriteriaBuilder ->
            sourceType?.let {
                when (it) {
                    SourceType.EXTERNAL -> {
                        criteriaBuilder.isNotNull(root.get<String>("externalId"))
                    }
                    SourceType.INTERNAL -> {
                        criteriaBuilder.isNull(root.get<String>("externalId"))
                    }
                }
            } ?: criteriaBuilder.conjunction()
        }
    }

    fun byHighlight(highlight: Boolean?): Specification<News> {
        return Specification { root: Root<News>, _: CriteriaQuery<*>?, criteriaBuilder: CriteriaBuilder ->
            highlight?.let {
                criteriaBuilder.equal(root.get<Boolean>("highlight"), it)
            } ?: criteriaBuilder.conjunction()
        }
    }

    fun byCategoryIds(categoryIds: List<UUID>?): Specification<News> = Specification { root, query, cb ->
        if (!categoryIds.isNullOrEmpty()) {
            val sourceCategoryJoin = root.join<News, SourceCategory>("sourceCategories")
            val categoryJoin = sourceCategoryJoin.join<SourceCategory, Category>("category")
            query.distinct(true)
            categoryJoin.get<UUID>("id").`in`(categoryIds)
        } else {
            cb.conjunction()
        }
    }

    fun excludeIds(exclude: List<UUID>?): Specification<News> = Specification { root, _, cb ->
        if (!exclude.isNullOrEmpty()) {
            cb.not(root.get<UUID>("id").`in`(exclude))
        } else {
            cb.conjunction()
        }
    }

    fun bySources(sources: List<String>?): Specification<News> = Specification { root, _, cb ->
        if (!sources.isNullOrEmpty()) {
            root.get<String>("source").`in`(sources)
        } else {
            cb.conjunction()
        }
    }
}
