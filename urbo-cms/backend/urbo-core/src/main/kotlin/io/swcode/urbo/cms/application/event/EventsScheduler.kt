package io.swcode.urbo.cms.application.event

import io.swcode.urbo.cms.domain.model.event.EventService
import io.swcode.urbo.common.logging.createLogger
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class EventsScheduler(
    private val eventAdapter: EventAdapter,
    private val eventService: EventService
) {

    @Scheduled(fixedRateString = "\${urbo.scheduler.event.fixed-rate}")
    @SchedulerLock(
        name = "event",
        lockAtMostFor = "\${urbo.scheduler.event.lock.at-most}",
        lockAtLeastFor = "\${urbo.scheduler.event.lock.at-least}"
    )
    fun getAndCreateEvents() {
        eventAdapter.getEvents().forEach { event ->
            try {
                eventService.save(event)
            } catch (e: Exception) {
                val occurrenceIds = event.occurrences.joinToString(separator = ", ") { it.externalId }
                logger.error("Failed to save event: ${event.title} With occurrences: $occurrenceIds", e)
            }
        }
    }

    companion object {
        private val logger = createLogger()
    }
}
