package io.swcode.urbo.cms.application.poi

import io.swcode.urbo.cms.domain.model.poi.Poi

fun interface PoiAdapter {
    fun getPois(): List<Poi>
}