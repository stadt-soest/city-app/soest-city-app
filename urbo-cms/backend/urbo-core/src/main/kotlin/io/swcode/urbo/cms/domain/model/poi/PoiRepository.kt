package io.swcode.urbo.cms.domain.model.poi

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface PoiRepository : JpaRepository<Poi, UUID>, JpaSpecificationExecutor<Poi> {
    fun findByExternalId(externalId: String): Poi?
}