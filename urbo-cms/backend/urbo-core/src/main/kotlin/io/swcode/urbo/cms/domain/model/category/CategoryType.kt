package io.swcode.urbo.cms.domain.model.category

enum class CategoryType {
    NEWS, EVENTS, POIS
}