package io.swcode.urbo.cms.domain.model.category.group

import io.swcode.urbo.cms.domain.model.category.CategoryType
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface CategoryGroupRepository : JpaRepository<CategoryGroup, UUID> {
    fun findAllByTypeOrderBySortOrderAsc(categoryType: CategoryType): List<CategoryGroup>
}