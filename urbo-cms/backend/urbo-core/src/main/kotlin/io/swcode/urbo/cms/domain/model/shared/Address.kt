package io.swcode.urbo.cms.domain.model.shared

import jakarta.persistence.Embeddable

@Embeddable
data class Address(
    val street: String,
    val postalCode: String,
    val city: String,
)
