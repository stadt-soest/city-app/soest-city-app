package io.swcode.urbo.cms.application.feedback.dto

import io.swcode.urbo.cms.application.shared.image.localization.LocalizationDto
import java.util.*

data class FeedbackFormDto(
    val id: UUID,
    val version: Int,
    val questions: List<FeedbackQuestionDto> = emptyList()
)

data class FeedbackQuestionDto(
    val id: UUID,
    val questionType: String,
    val questionName: List<LocalizationDto> = emptyList(),
    val options: List<FeedbackOptionDto> = emptyList()
)

data class FeedbackOptionDto(
    val id: UUID? = null,
    val localizedTexts: List<LocalizationDto> = emptyList()
)
