package io.swcode.urbo.cms.domain.model.feedback

import io.swcode.urbo.cms.application.feedback.assembler.FeedbackAssembler
import io.swcode.urbo.cms.application.feedback.assembler.assembleFeedbackForm
import io.swcode.urbo.cms.application.feedback.dto.FeedbackFormCreateDto
import io.swcode.urbo.cms.application.feedback.dto.FeedbackSubmissionRequestDto
import io.swcode.urbo.cms.domain.model.feedback.response.FeedbackResponseRepository
import io.swcode.urbo.cms.domain.model.feedback.response.FeedbackSubmission
import io.swcode.urbo.common.error.ErrorType
import io.swcode.urbo.common.error.UrboException
import jakarta.validation.Valid
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.annotation.Validated
import java.util.*

@Service
@Validated
class FeedbackService(
    private val feedbackFormRepository: FeedbackFormRepository,
    private val feedbackResponseRepository: FeedbackResponseRepository
) {
    fun getFeedbackForm(version: Int?): FeedbackForm {
        return version?.let {
            getFeedbackFormByVersion(it)
        } ?: getLatestFeedbackForm()
    }

    fun createFeedbackForm(feedbackFormCreateDto: FeedbackFormCreateDto): FeedbackForm {
        val newVersion = feedbackFormRepository.findMaxVersion() + 1

        val feedbackForm = feedbackFormCreateDto.assembleFeedbackForm(newVersion)
        return feedbackFormRepository.save(feedbackForm)
    }

    @Transactional
    fun submitFeedback(@Valid feedbackSubmissionRequestDto: FeedbackSubmissionRequestDto): FeedbackSubmission {
        val feedbackForm = getFeedbackForm(feedbackSubmissionRequestDto.version)

        val feedbackResponse = FeedbackAssembler.mapToEntity(feedbackSubmissionRequestDto, feedbackForm)

        return feedbackResponseRepository.save(feedbackResponse)
    }

    fun getById(id: UUID): FeedbackSubmission {
        return feedbackResponseRepository.getReferenceById(id)
    }

    fun findAllFeedbacks(pageable: Pageable): Page<FeedbackSubmission> {
        return feedbackResponseRepository.findAll(pageable)
    }

    private fun getFeedbackFormByVersion(version: Int): FeedbackForm {
        return feedbackFormRepository.findByVersion(version)
            ?: throw UrboException("Feedback form with version $version not found", ErrorType.RESOURCE_NOT_FOUND)
    }

    private fun getLatestFeedbackForm(): FeedbackForm {
        return feedbackFormRepository.findTopByOrderByVersionDesc()
            ?: throw UrboException("No feedback forms are available", ErrorType.RESOURCE_NOT_FOUND)
    }
}

