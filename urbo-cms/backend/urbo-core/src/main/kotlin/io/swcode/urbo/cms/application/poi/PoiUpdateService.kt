package io.swcode.urbo.cms.application.poi

import io.swcode.urbo.cms.domain.model.poi.PoiService
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
class PoiUpdateService(private val poiAdapter: PoiAdapter,
                       private val poiService: PoiService) {

    @Transactional
    fun update() {
        poiAdapter.getPois().forEach {
            poiService.save(it)
        }
    }
}