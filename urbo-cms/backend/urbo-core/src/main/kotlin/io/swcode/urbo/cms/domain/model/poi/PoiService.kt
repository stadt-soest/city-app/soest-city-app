package io.swcode.urbo.cms.domain.model.poi

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Component
import java.util.*
import kotlin.jvm.optionals.getOrNull

@Component
class PoiService(private val poiRepository: PoiRepository) {

    fun save(poi: Poi): Poi {
        return poiRepository.save(poiRepository.findByExternalId(poi.externalId)?.updateFrom(poi) ?: poi)
    }

    fun findById(id: UUID): Poi? {
        return poiRepository.findById(id).getOrNull()
    }

    fun findAll(categoryIds: List<UUID>, pageable: Pageable): Page<Poi> {
        PoiSpecifications.withCategories(categoryIds).let {
            return poiRepository.findAll(it, pageable)
        }
    }
}