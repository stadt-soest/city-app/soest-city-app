package io.swcode.urbo.cms.application.category

import io.swcode.urbo.cms.domain.model.category.CategoryType
import io.swcode.urbo.cms.domain.model.category.group.CategoryGroupService
import jakarta.validation.Valid
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@Validated
class CategoryGroupController(private val categoryGroupService: CategoryGroupService) {

    @GetMapping("/v1/category-groups")
    fun getCategoryGroups(@RequestParam type: CategoryType): ResponseEntity<List<CategoryGroupResponseDto>> {
        return ResponseEntity.ok(categoryGroupService.getCategoryGroups(type).map {
            CategoryGroupResponseDto.fromEntity(it)
        })
    }

    @PostMapping("/v1/category-groups")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).CATEGORY_WRITE)")
    fun createCategoryGroup(@RequestBody @Valid requestDto: CategoryGroupCreateDto): ResponseEntity<CategoryGroupResponseDto> {
        categoryGroupService.createCategoryGroup(requestDto).also {
            return ResponseEntity.ok(CategoryGroupResponseDto.fromEntity(it))
        }
    }

    @PutMapping("/v1/category-groups/{id}")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).CATEGORY_WRITE)")
    fun updateCategoryGroup(
        @PathVariable id: UUID,
        @RequestBody @Valid request: CategoryGroupCreateDto
    ): ResponseEntity<CategoryGroupResponseDto> {
        categoryGroupService.updateCategoryGroup(
            id, request.localizedNames.map { it.toLocalization() }
        ).also {
            return ResponseEntity.ok(CategoryGroupResponseDto.fromEntity(it))
        }
    }

    @DeleteMapping("/v1/category-groups/{id}")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).CATEGORY_DELETE)")
    fun deleteCategoryGroup(
        @PathVariable id: UUID
    ): ResponseEntity<Unit> {
        categoryGroupService.deleteCategoryGroup(id).also {
            return ResponseEntity.noContent().build()
        }
    }

    @PatchMapping("/v1/category-groups/order")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).CATEGORY_WRITE)")
    fun updateCategoryGroupOrder(@RequestBody @Valid categoryGroups: List<CategoryGroupUpdateOrderDto>): ResponseEntity<Unit> {
        categoryGroupService.updateCategoryGroupOrder(categoryGroups.map { it.toEntity() })
        return ResponseEntity.noContent().build()
    }
}