package io.swcode.urbo.cms.domain.model.category.group

import io.swcode.urbo.cms.domain.model.category.Category
import io.swcode.urbo.cms.domain.model.category.CategoryType
import io.swcode.urbo.cms.domain.model.shared.Localization
import jakarta.persistence.*
import java.util.*

@Entity
data class CategoryGroup(
    @Id
    val id: UUID,

    @Enumerated(EnumType.STRING)
    val type: CategoryType,

    @ElementCollection
    @CollectionTable(name = "category_group_name", joinColumns = [JoinColumn(name = "category_group_id")])
    @AttributeOverrides(
        AttributeOverride(name = "language", column = Column(name = "language")),
        AttributeOverride(name = "value", column = Column(name = "value"))
    )
    var localizedNames: List<Localization>,

    @OneToMany(mappedBy = "categoryGroup")
    @OrderColumn(name = "sort_order")
    val categories: MutableList<Category> = mutableListOf(),

    @Column(name = "sort_order")
    var sortOrder: Int = 0
) {
    fun update(localizedNames: List<Localization>): CategoryGroup {
        this.localizedNames = localizedNames
        return this
    }
}