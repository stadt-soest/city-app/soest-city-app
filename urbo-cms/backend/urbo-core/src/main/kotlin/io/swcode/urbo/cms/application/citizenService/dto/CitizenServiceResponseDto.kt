package io.swcode.urbo.cms.application.citizenService.dto

import io.swcode.urbo.cms.application.shared.image.localization.LocalizationDto
import io.swcode.urbo.cms.application.shared.image.localization.toDto
import io.swcode.urbo.cms.domain.model.citizenService.CitizenService
import io.swcode.urbo.cms.domain.model.citizenService.Tag
import io.swcode.urbo.cms.search.meili.api.SearchIndexedDto
import java.util.*

data class CitizenServiceResponseDto(
    val id: UUID,
    val visible: Boolean,
    val favorite: Boolean,
    val title: String,
    val description: String,
    val url: String,
    val tags: Set<TagDto>,
    val editable: Boolean,
) : SearchIndexedDto {
    companion object {
        fun fromDomain(citizenService: CitizenService): CitizenServiceResponseDto {
            return CitizenServiceResponseDto(
                id = citizenService.id,
                visible = citizenService.visible,
                favorite = citizenService.favorite,
                title = citizenService.title,
                description = citizenService.description,
                url = citizenService.url,
                tags = citizenService.tags.map { TagDto.of(it) }.toSet(),
                editable = citizenService.externalId.isNullOrEmpty()
            )
        }
    }

    data class TagDto(
        val id: UUID,
        val localizedNames: List<LocalizationDto>
    ) {
        companion object {
            fun of(tag: Tag): TagDto {
                return TagDto(
                    id = tag.id,
                    localizedNames = tag.localizedNames.map { it.toDto() }
                )
            }
        }
    }
}
