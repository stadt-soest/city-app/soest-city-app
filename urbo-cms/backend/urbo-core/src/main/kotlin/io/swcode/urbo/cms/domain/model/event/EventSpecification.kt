package io.swcode.urbo.cms.domain.model.event

import io.swcode.urbo.cms.domain.model.category.Category
import io.swcode.urbo.cms.domain.model.category.SourceCategory
import jakarta.persistence.criteria.CriteriaBuilder
import jakarta.persistence.criteria.CriteriaQuery
import jakarta.persistence.criteria.Root
import org.springframework.data.jpa.domain.Specification
import java.time.Instant
import java.util.UUID

object EventSpecification {
    fun byCategoryIds(categoryIds: List<UUID>?): Specification<Event> = Specification { root, query, cb ->
        if (!categoryIds.isNullOrEmpty()) {
            val sourceCategoryJoin = root.join<Event, SourceCategory>("sourceCategories")
            val categoryJoin = sourceCategoryJoin.join<SourceCategory, Category>("category")
            query.distinct(true)
            cb.isTrue(categoryJoin.get<UUID>("id").`in`(categoryIds))
        } else {
            cb.conjunction()
        }
    }

    fun excludeEventIds(exclude: List<UUID>?): Specification<Event> = Specification { root, _, cb ->
        if (!exclude.isNullOrEmpty()) {
            cb.not(root.get<UUID>("id").`in`(exclude))
        } else {
            cb.conjunction()
        }
    }

    fun byStatus(status: String?): Specification<Event> = Specification { root, _, cb ->
        status?.takeUnless { it.isBlank() }?.let {
            val occurrencesJoin = root.join<Event, EventOccurrence>("occurrences")

            occurrencesJoin.on(cb.equal(occurrencesJoin.get<String>("status"), status)).on
        } ?: cb.conjunction()
    }

    fun searchByKeyword(keyword: String?): Specification<Event> {
        return Specification { root: Root<Event>, _: CriteriaQuery<*>, criteriaBuilder: CriteriaBuilder ->
            if (keyword.isNullOrEmpty()) {
                criteriaBuilder.conjunction()
            } else {
                val titlePredicate: jakarta.persistence.criteria.Predicate = criteriaBuilder.like(
                    criteriaBuilder.lower(root["title"]),
                    "%${keyword.lowercase()}%"
                )
                val contentPredicate: jakarta.persistence.criteria.Predicate = criteriaBuilder.like(
                    criteriaBuilder.lower(root["description"]),
                    "%${keyword.lowercase()}%"
                )
                criteriaBuilder.or(titlePredicate, contentPredicate)
            }
        }
    }

    fun byFutureOccurrenceCondition(): Specification<Event> = Specification { root, _, cb ->
        val occurrencesJoin = root.join<Event, EventOccurrence>("occurrences")
        cb.greaterThan(occurrencesJoin.get("startAt"), Instant.now())
    }
}