package io.swcode.urbo.cms.application.accessibilityFeedback.dto

import io.swcode.urbo.cms.application.feedback.dto.EnvironmentDto
import io.swcode.urbo.cms.domain.model.accessibilityFeedback.AccessibilityFeedback
import jakarta.validation.constraints.Size
import org.jetbrains.annotations.NotNull
import java.time.Instant
import java.util.UUID

data class AccessibilityRequestDto(
    @NotNull @Size(min = 1, max = 300) val answer: String,
    @NotNull val imageFilenames: List<@Size(max = 100) String>,
    @NotNull val environment: EnvironmentDto,
)

data class AccessibilityFeedbackDto(
    val id: UUID,
    val createdAt: Instant,
    val answer: String,
    val environment: EnvironmentDto,
    val imageFilenames: List<String>,
) {
    companion object {
        fun fromDomain(entity: AccessibilityFeedback): AccessibilityFeedbackDto {
            return AccessibilityFeedbackDto(
                entity.id,
                entity.createdAt,
                entity.answer,
                EnvironmentDto.fromDomain(entity.environment),
                entity.imageFilenames
            )
        }
    }
}