package io.swcode.urbo.cms.application.event.response

import io.swcode.urbo.cms.application.shared.category.MappedCategoryDto
import io.swcode.urbo.cms.application.shared.image.ImageDto
import io.swcode.urbo.cms.domain.model.event.Event
import io.swcode.urbo.cms.domain.model.event.EventLocation
import io.swcode.urbo.cms.domain.model.event.EventOffer
import io.swcode.urbo.cms.domain.model.event.EventStatus
import io.swcode.urbo.common.date.toBerlinInstant
import java.time.Instant
import java.util.UUID

data class EventResponseDto(
    val id: UUID,
    val title: String,
    val subTitle: String?,
    val description: String?,
    val image: ImageDto?,
    val location: EventLocationDto,
    val contactPoint: ContactPointResponseDto,
    val offer: EventOfferResponseDto?,
    val categories: List<MappedCategoryDto>,
    val occurrences: List<OccurrenceDto>,
) {
    companion object {
        fun fromEntity(event: Event): EventResponseDto {
            return EventResponseDto(
                event.id,
                event.title,
                event.subTitle,
                event.description,
                event.image?.let { ImageDto.fromEntity(it) },
                EventLocationDto.fromEntity(event.location),
                contactPoint = ContactPointResponseDto(
                    telephone = event.contactPoint?.telephone,
                    email = event.contactPoint?.email
                ),
                EventOfferResponseDto.fromEntity(event.offer),
                event.sourceCategories.map { MappedCategoryDto.of(it) },
                event.occurrences.map {
                    OccurrenceDto(
                        it.id,
                        it.status,
                        it.dateCreated,
                        it.startAt.toBerlinInstant(),
                        it.endAt.toBerlinInstant(),
                        it.source
                    )
                },
            )
        }
    }
}

data class OccurrenceDto(
    val id: UUID,
    val status: EventStatus,
    val dateCreated: Instant,
    var start: Instant,
    var end: Instant,
    val source: String,
)

data class CoordinatesResponseDto(
    val latitude: Double,
    val longitude: Double
)

data class EventLocationDto(
    val locationName: String?,
    val address: String?,
    val postalCode: String?,
    val city: String?,
    val coordinates: CoordinatesResponseDto?,
) {
    companion object {
        fun fromEntity(eventLocation: EventLocation?): EventLocationDto {
            return EventLocationDto(
                eventLocation?.locationName,
                eventLocation?.streetAddress,
                eventLocation?.postalCode,
                eventLocation?.city,
                eventLocation?.coordinates?.let { CoordinatesResponseDto(it.latitude, it.longitude) }
            )
        }
    }
}

data class ContactPointResponseDto(
    val telephone: String? = null,
    val email: String? = null
)

data class EventOfferResponseDto(
    var url: String? = null,
    var price: String? = null,
    var availability: String? = null,
    var description: String? = null
) {
    companion object {
        fun fromEntity(eventOffer: EventOffer?): EventOfferResponseDto {
            return EventOfferResponseDto(
                eventOffer?.url,
                eventOffer?.price,
                eventOffer?.availability,
                eventOffer?.description
            )
        }
    }
}

