package io.swcode.urbo.cms.application.status

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1")
class StatusController(
    private val nodeRedAdapter: NodeRedAdapter
){

    @GetMapping("/node-red-status")
    fun getNodeRedStatus(): ResponseEntity<String> {
        return if (nodeRedAdapter.getNodeRedStatus() == NodeRedStatus.UP) {
            ResponseEntity.ok().build()
        } else {
            ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build()
        }
    }
}