package io.swcode.urbo.cms.application.parking.response

import io.swcode.urbo.cms.application.shared.image.localization.LocalizationDto
import io.swcode.urbo.cms.domain.model.parking.*
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalTime
import java.util.*

data class ParkingAreaResponseDto(
    val id: UUID,
    val name: String,
    val location: ParkingLocation,
    val status: ParkingStatus,
    val capacity: ParkingCapacity,
    val openingTimes: Map<DayOfWeek, ParkingOpeningHours>,
    val specialOpeningTimes: List<ParkingSpecialOpeningHoursResponseDto>,
    val description: Set<LocalizationDto>,
    val displayName: String,
    val parkingType: ParkingType,
    val maximumAllowedHeight: String?,
    val priceDescription: Set<LocalizationDto>,
) {
    companion object {
        fun fromDomain(parkingArea: ParkingArea): ParkingAreaResponseDto {
            return ParkingAreaResponseDto(
                id = parkingArea.id,
                name = parkingArea.name,
                location = parkingArea.location,
                capacity = parkingArea.capacity,
                openingTimes = parkingArea.openingTimes,
                specialOpeningTimes = parkingArea.specialOpeningTimes.values
                    .sortedWith(compareBy<ParkingSpecialOpeningHours> { it.date }.thenBy { it.from })
                    .map { ParkingSpecialOpeningHoursResponseDto.fromDomain(it) },
                status = parkingArea.status,
                maximumAllowedHeight = parkingArea.maximumAllowedHeight,
                displayName = parkingArea.displayName,
                parkingType = parkingArea.parkingType,
                description = parkingArea.description.map {
                    LocalizationDto(
                        language = it.language,
                        value = it.value
                    )
                }.toSet(),
                priceDescription = parkingArea.priceDescription.map {
                    LocalizationDto(
                        language = it.language,
                        value = it.value
                    )
                }.toSet()
            )
        }
    }
}

data class ParkingSpecialOpeningHoursResponseDto(
    val date: LocalDate,
    val from: LocalTime,
    val to: LocalTime,
    val descriptions: List<LocalizationDto>
) {
    companion object {
        fun fromDomain(hours: ParkingSpecialOpeningHours): ParkingSpecialOpeningHoursResponseDto {
            return ParkingSpecialOpeningHoursResponseDto(
                date = hours.date,
                from = hours.from,
                to = hours.to,
                descriptions = hours.description.map { LocalizationDto(it.value, it.language) }
            )
        }
    }
}