package io.swcode.urbo.cms.application.accessibilityFeedback

import io.swcode.urbo.cms.application.accessibilityFeedback.assembler.AccessibilityFeedbackAssembler
import io.swcode.urbo.cms.application.accessibilityFeedback.dto.AccessibilityFeedbackDto
import io.swcode.urbo.cms.application.accessibilityFeedback.dto.AccessibilityRequestDto
import io.swcode.urbo.cms.domain.model.accessibilityFeedback.AccessibilityFeedbackService
import jakarta.validation.Valid
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.util.UUID

@RestController
@Validated
class AccessibilityFeedbackController(
    val accessibilityFeedbackService: AccessibilityFeedbackService,
    val accessibilityFeedbackAssembler: AccessibilityFeedbackAssembler
) {

    @PostMapping("/v1/accessibility-feedback-submissions")
    fun submitFeedback(
        @RequestBody @Valid accessibilityRequestDto: AccessibilityRequestDto,
    ): ResponseEntity<Unit> {
        val accessibilityFeedback = accessibilityFeedbackAssembler.toEntity(accessibilityRequestDto)
        accessibilityFeedbackService.submitAccessibilityFeedback(accessibilityFeedback)
        return ResponseEntity.ok().build()
    }

    @GetMapping("/v1/accessibility-feedback-submissions")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).ACCESSIBILITY_SUBMISSION_READ)")
    fun findAll(pageable: Pageable): ResponseEntity<Page<AccessibilityFeedbackDto>> {
        return accessibilityFeedbackService.findAll(pageable).let { page ->
            ResponseEntity.ok(page.map { AccessibilityFeedbackDto.fromDomain(it) })
        }
    }

    @GetMapping("/v1/accessibility-feedback-submissions/{id}")
    @PreAuthorize("hasAuthority(T(io.swcode.urbo.cms.application.CorePermission).ACCESSIBILITY_SUBMISSION_READ)")
    fun getById(@PathVariable id: @org.hibernate.validator.constraints.UUID UUID): ResponseEntity<AccessibilityFeedbackDto> {
        return accessibilityFeedbackService.getById(id).let {
            ResponseEntity.ok(AccessibilityFeedbackDto.fromDomain(it))
        }
    }
}