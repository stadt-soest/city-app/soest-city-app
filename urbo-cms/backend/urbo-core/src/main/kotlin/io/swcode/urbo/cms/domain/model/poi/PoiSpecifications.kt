package io.swcode.urbo.cms.domain.model.poi

import io.swcode.urbo.cms.domain.model.category.Category
import io.swcode.urbo.cms.domain.model.category.SourceCategory
import org.springframework.data.jpa.domain.Specification
import java.util.*

object PoiSpecifications {
    fun withCategories(categoryIds: List<UUID>): Specification<Poi> {
        return Specification { root, query, criteriaBuilder ->
            query.distinct(true)

            if (categoryIds.isEmpty()) {
                criteriaBuilder.conjunction()
            } else {
                val sourceCategoryJoin = root.join<Poi, SourceCategory>("sourceCategories")
                val categoryJoin = sourceCategoryJoin.join<SourceCategory, Category>("category")
                categoryJoin.get<UUID>("id").`in`(categoryIds)
            }
        }
    }
}