package io.swcode.urbo.cms.domain.model.citizenService.tag

import io.swcode.urbo.cms.domain.model.citizenService.CitizenServiceRepository
import io.swcode.urbo.cms.domain.model.citizenService.Tag
import io.swcode.urbo.common.error.ErrorType
import io.swcode.urbo.common.error.UrboException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
@Transactional
class TagService(
    private val tagRepository: TagRepository,
    private val citizenServiceRepository: CitizenServiceRepository
) {
    fun getAllTags(): List<Tag> = tagRepository.findAll()
    fun getTagById(id: UUID): Tag =
        tagRepository.findById(id).orElseThrow { UrboException("Tag not found with id: $id", ErrorType.RESOURCE_NOT_FOUND) }

    fun createTag(tag: Tag): Tag = tagRepository.save(tag)
    fun createOrUpdateTag(tag: Tag): Tag = tagRepository.save(tag)

    fun deleteTag(id: UUID) {
        val tag = tagRepository.findById(id)
            .orElseThrow { UrboException("Tag not found with id: $id", ErrorType.RESOURCE_NOT_FOUND) }

        citizenServiceRepository.bulkRemoveTagAssociations(id)

        tagRepository.delete(tag)
    }
}
