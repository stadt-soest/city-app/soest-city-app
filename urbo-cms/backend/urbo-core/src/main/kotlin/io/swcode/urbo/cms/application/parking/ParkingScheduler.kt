package io.swcode.urbo.cms.application.parking

import io.swcode.urbo.cms.domain.model.parking.ParkingService
import io.swcode.urbo.common.logging.createLogger
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class ParkingScheduler(
    private val parkingAdapter: ParkingAdapter,
    private val parkingService: ParkingService,
) {

    @Scheduled(fixedRateString = "\${urbo.scheduler.parking.fixed-rate}")
    @SchedulerLock(
        name = "parking",
        lockAtMostFor = "\${urbo.scheduler.parking.lock.at-most}",
        lockAtLeastFor = "\${urbo.scheduler.parking.lock.at-least}"
    )
    fun getAndCreateParkingAreas() {
        try {
            parkingAdapter.getParkingAreas().forEach {
                parkingService.save(it)
            }
        } catch (ex: Exception) {
            logger.error("Exception occurred in getAndCreateParkingAreas: {}", ex.message, ex)
        }
    }

    companion object {
        private val logger = createLogger()
    }
}
