package io.swcode.urbo.cms.domain.command

import io.swcode.urbo.cms.domain.model.shared.Image
import io.swcode.urbo.command.api.Command
import java.util.*

interface DownloadImageCommand : Command {
    val image: Image
}

data class DownloadEventImageCommand(
    val eventId: UUID,
    override val image: Image
) : DownloadImageCommand

data class DownloadNewsImageCommand(
    val newsId: UUID,
    override val image: Image
) : DownloadImageCommand