package io.swcode.urbo.cms.application

import io.swcode.urbo.permission.adapter.Permission

enum class CorePermission : Permission {
    CITIZEN_SERVICE_WRITE,
    CITIZEN_SERVICE_DELETE,
    CATEGORY_READ,
    CATEGORY_WRITE,
    CATEGORY_DELETE,
    NEWS_WRITE,
    NEWS_DELETE,
    NEWS_HIGHLIGHT,
    FEEDBACK_SUBMISSION_READ,
    FEEDBACK_SUBMISSION_WRITE,
    ACCESSIBILITY_SUBMISSION_READ,
    PARKING_AREA_WRITE;

    override val value: String
        get() = name
}