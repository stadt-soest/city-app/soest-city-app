package io.swcode.urbo.cms.domain.model.shared

import io.swcode.urbo.core.api.image.ImageProcessingFileDto
import io.swcode.urbo.core.api.image.ImageProcessingResult
import jakarta.persistence.Embeddable
import jakarta.persistence.Embedded

@Embeddable
data class Image(
    val source: String,
    @Embedded
    var thumbnail: ImageFile? = null,

    @Embedded
    var highRes: ImageFile? = null,

    val description: String? = null,
    val copyrightHolder: String? = null,
) {
    fun from(imageProcessingResult: ImageProcessingResult): Image {
        return copy(
            thumbnail = imageProcessingResult.thumbnail?.let {
                ImageFile.of(it)
            },
            highRes = imageProcessingResult.highRes?.let {
                ImageFile.of(it)
            })
    }
}

@Embeddable
data class ImageFile(
    val filename: String,
    var width: Int? = null,
    var height: Int? = null
) {
    companion object {
        fun of(dto: ImageProcessingFileDto): ImageFile {
            return ImageFile(
                dto.filename,
                dto.width,
                dto.height
            )
        }
    }
}