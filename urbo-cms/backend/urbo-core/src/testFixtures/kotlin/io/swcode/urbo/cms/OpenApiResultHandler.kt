package io.swcode.urbo.cms

import com.atlassian.oai.validator.OpenApiInteractionValidator
import com.atlassian.oai.validator.mockmvc.OpenApiValidationMatchers
import com.atlassian.oai.validator.report.LevelResolver
import com.atlassian.oai.validator.report.ValidationReport
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.ResultHandler

class OpenApiResultHandler(specPath: String) : ResultHandler {
    private val validator: OpenApiInteractionValidator = OpenApiInteractionValidator
        .createForSpecificationUrl(specPath)
        .withResolveCombinators(true)
        .withLevelResolver(
            LevelResolver.create()
                .withLevel("validation.request", ValidationReport.Level.WARN)
                .build()
        )
        .build()

    override fun handle(mvcResult: MvcResult) {
        val skipValidation = mvcResult.request.getAttribute("skipOpenApiValidation") as? Boolean ?: false
        if (!skipValidation) {
            OpenApiValidationMatchers.openApi().isValid(validator).match(mvcResult)
        }
    }
}