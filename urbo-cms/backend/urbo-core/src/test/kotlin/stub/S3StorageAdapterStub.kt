package stub

import io.swcode.urbo.cms.s3.api.DeletingObject
import io.swcode.urbo.cms.s3.api.S3Adapter
import java.io.InputStream
import java.net.URI
import java.net.URL
import java.time.Duration

class S3StorageAdapterStub : S3Adapter {
    override fun putS3Object(inputStream: InputStream, fileName: String) {}
    override fun deleteS3Object(deletingObject: DeletingObject) {}
    override fun requestSignedDownloadUrl(key: String, duration: Duration): URL {
        return URI.create("https://example.com").toURL()
    }
}
