package stub

import io.swcode.urbo.command.api.Command
import io.swcode.urbo.command.api.CommandBus
import java.time.Duration
import java.util.*


class CommandBusStub : CommandBus {
    private val commandList = mutableListOf<Command>()

    fun getCommands(): MutableList<Command> {
        return commandList
    }

    fun clearCommands() {
        commandList.clear()
    }

    override fun execute(commandId: UUID, command: Command) {
        commandList.add(command)
    }

    override fun submitAsyncCommand(command: Command) {
        commandList.add(command)
    }

    override fun submitCommandWithDelay(command: Command, delay: Duration) {
        commandList.add(command)
    }
}