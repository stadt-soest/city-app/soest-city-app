package io.swcode.urbo.cms.application.waste

import io.swcode.urbo.cms.BaseControllerTest
import io.swcode.urbo.cms.domain.model.waste.WasteType
import io.swcode.urbo.cms.domain.model.waste.repository.WasteCollectionLocationRepository
import io.swcode.urbo.cms.domain.model.waste.service.WasteService
import org.hamcrest.Matchers.`is`
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.test.web.servlet.get
import testUtil.createWaste
import testUtil.createWasteDate
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*

class WasteCollectionLocationControllerTest : BaseControllerTest() {

    @Autowired
    private lateinit var wasteService: WasteService

    @Autowired
    private lateinit var wasteCollectionLocationRepository: WasteCollectionLocationRepository

    @BeforeEach
    fun setup() {
        wasteCollectionLocationRepository.deleteAll()
    }

    @Test
    fun `should get wasteCollections`() {

        val waste = wasteService.save(createWaste())

        mockMvc
            .get("/v1/waste-collections")
            .andExpect { status { HttpStatus.OK } }
            .andExpect {
                jsonPath("$.content.size()", `is`(1))
                jsonPath("$.content.[0].id", `is`(waste.id.toString()))
                jsonPath("$.content.[0].city", `is`("city"))
                jsonPath("$.content.[0].street", `is`("street"))
            }
    }

    @Test
    fun `should return correct number of Wastes for given page size`() {
        val totalWastes = 10
        repeat(totalWastes) { i ->
            val entity = createWaste().copy(wasteId = "$i", id = UUID.randomUUID(), city = "Title ${i + 1}")
            wasteService.save(entity)
        }

        mockMvc
            .get("/v1/waste-collections?page=0&size=5")
            .andExpect { status { isOk() } }
            .andExpect { jsonPath("$.content.size()", `is`(5)) }
    }

    @Test
    fun `should return correct Wastes for given page number`() {
        val totalWastes = 10
        repeat(totalWastes) { i ->
            val entity = createWaste().copy(wasteId = "$i", id = UUID.randomUUID(), city = "Title ${i + 1}")
            wasteService.save(entity)
        }

        mockMvc
            .get("/v1/waste-collections?page=1&size=5")
            .andExpect { status { isOk() } }
            .andExpect { jsonPath("$.content[0].city", `is`("Title 6")) }
    }

    @Test
    fun `should reject request with too long city name`() {
        mockMvc.get("/v1/waste-collections?city=" + "a".repeat(51))
            .andExpect {
                status { isBadRequest() }
                jsonPath("$.invalidParams[0].attribute", `is`("getWasteCollections.city"))
                jsonPath("$.invalidParams[0].cause", `is`("size must be between 1 and 50"))
            }
    }

    @Test
    fun `should reject request with too long street name`() {
        mockMvc.get("/v1/waste-collections?street=" + "a".repeat(101))
            .andExpect {
                status { isBadRequest() }
                jsonPath("$.invalidParams[0].attribute", `is`("getWasteCollections.street"))
                jsonPath("$.invalidParams[0].cause", `is`("size must be between 1 and 100"))
            }
    }

    @Test
    fun `should reject request with too long search keyword`() {
        mockMvc.get("/v1/waste-collections?searchKeyword=" + "a".repeat(101))
            .andExpect {
                status { isBadRequest() }
                jsonPath("$.invalidParams[0].attribute", `is`("getWasteCollections.searchKeyword"))
                jsonPath("$.invalidParams[0].cause", `is`("size must be between 1 and 100"))
            }
    }

    @Test
    fun `should fetch upcoming waste collection dates filtered by types`() {
        val wasteLocation = createWaste()

        val startDate = Instant.now().plus(5, ChronoUnit.DAYS)
        val endDate = Instant.now().plus(10, ChronoUnit.DAYS)

        val wasteDate1 = createWasteDate().copy(
            date = startDate.plus(1, ChronoUnit.DAYS),
            wasteType = WasteType.DIAPER_WASTE,
            wasteCollectionLocation = wasteLocation
        )

        val wasteDate2 = createWasteDate().copy(
            date = endDate.minus(1, ChronoUnit.DAYS),
            wasteType = WasteType.WASTEPAPER,
            wasteCollectionLocation = wasteLocation
        )

        wasteLocation.wasteCollectionDates.addAll(listOf(wasteDate1, wasteDate2))

        wasteService.save(wasteLocation)

        mockMvc.get("/v1_1/waste-collections/${wasteLocation.id}/upcoming-waste-collection-dates?wasteTypes=DIAPER_WASTE") {
            param("startDate", startDate.toString())
            param("endDate", endDate.toString())
        }.andExpect {
            status { isOk() }
            jsonPath("$.upcomingDatesByType.DIAPER_WASTE.size()", `is`(1))
        }
    }
}
