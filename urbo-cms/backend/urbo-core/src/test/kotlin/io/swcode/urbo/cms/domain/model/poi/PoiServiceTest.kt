package io.swcode.urbo.cms.domain.model.poi

import io.kotest.matchers.shouldBe
import io.swcode.urbo.cms.BaseTest
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import testUtil.createNewPoi
import java.util.UUID

class PoiServiceTest : BaseTest() {
    @Autowired
    private lateinit var poiService: PoiService

    @Autowired
    private lateinit var poiRepository: PoiRepository

    @Test
    fun `should create new poi`() {
        poiService.save(createNewPoi())

        poiRepository.count().shouldBe(1)
    }

    @Test
    fun `should update existing poi`() {
        val existingPoi = poiService.save(createNewPoi())

        existingPoi.copy(id = UUID.randomUUID(), name = "Other name").let {
            poiService.save(it)
        }

        poiRepository.count().shouldBe(1)
        poiRepository.findAll().first().name.shouldBe("Other name")
    }
}