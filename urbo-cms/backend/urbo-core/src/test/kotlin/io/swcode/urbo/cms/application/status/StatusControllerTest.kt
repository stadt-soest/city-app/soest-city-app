package io.swcode.urbo.cms.application.status

import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.verify
import io.swcode.urbo.cms.BaseControllerTest
import org.junit.jupiter.api.Test
import org.springframework.test.web.servlet.get

class StatusControllerTest : BaseControllerTest() {

    @MockkBean
    private lateinit var nodeRedAdapter: NodeRedAdapter

    @Test
    fun `should return OK when Node-RED is up`() {
        every { nodeRedAdapter.getNodeRedStatus() } returns NodeRedStatus.UP

        mockMvc.get("/v1/node-red-status")
            .andExpect {
                status { isOk() }
            }

        verify { nodeRedAdapter.getNodeRedStatus() }
    }

    @Test
    fun `should return SERVICE_UNAVAILABLE when Node-RED is down`() {
        every { nodeRedAdapter.getNodeRedStatus() } returns NodeRedStatus.DOWN

        mockMvc.get("/v1/node-red-status")
            .andExpect {
                status { isServiceUnavailable() }
            }

        verify { nodeRedAdapter.getNodeRedStatus() }
    }
}