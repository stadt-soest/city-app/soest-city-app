package io.swcode.urbo.cms.application.event

import io.swcode.urbo.cms.BaseControllerTest
import io.swcode.urbo.cms.testutil.TestDataProvider
import org.hamcrest.Matchers.`is`
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.get
import java.time.Instant
import java.time.temporal.ChronoUnit

class EventControllerTest : BaseControllerTest() {

    @Autowired
    private lateinit var testDataProvider: TestDataProvider

    @Test
    fun `should get events`() {
        val event = testDataProvider.createEvent(1, Instant.now().plus(1, ChronoUnit.DAYS))

        mockMvc
            .get("/v1/events")
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.content.size()", `is`(1))
                jsonPath("$.content[0].id", `is`(event.id.toString()))
            }
    }

    @Test
    fun `should get event by id`() {
        val event = testDataProvider.createEvent(1)

        mockMvc
            .get("/v1/events/{id}", event.id)
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.id", `is`(event.id.toString()))
                jsonPath("$.title", `is`(event.title))
            }
    }

    @Test
    fun `should validate categoryIds as UUIDs`() {
        val invalidUuid = "invalid-uuid"

        mockMvc
            .get("/v1/events?categoryIds=$invalidUuid")
            .andExpect { status { isBadRequest() } }
            .andExpect {
                jsonPath("$.detail", `is`("Failed to convert 'categoryIds' with value: 'invalid-uuid'"))
            }
    }

    @Test
    fun `should validate exclude as UUIDs`() {
        val invalidUuid = "invalid-uuid"

        mockMvc
            .get("/v1/events?exclude=$invalidUuid")
            .andExpect { status { isBadRequest() } }
            .andExpect {
                jsonPath("$.detail", `is`("Failed to convert 'exclude' with value: 'invalid-uuid'"))
            }
    }

    @Test
    fun `should get event by occurrenceId`() {
        val event = testDataProvider.createEvent(1)

        mockMvc
            .get("/v1/event-occurrences/{occurrenceId}/event", event.occurrences[0].id)
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.id", `is`(event.id.toString()))
                jsonPath("$.title", `is`(event.title))
            }
    }
}