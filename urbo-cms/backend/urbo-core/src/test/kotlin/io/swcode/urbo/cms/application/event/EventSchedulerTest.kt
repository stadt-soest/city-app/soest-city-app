package io.swcode.urbo.cms.application.event

import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.swcode.urbo.cms.BaseTest
import io.swcode.urbo.cms.domain.model.event.EventRepository
import io.swcode.urbo.cms.domain.model.event.EventService
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import testUtil.createEventEntity

class EventSchedulerTest : BaseTest() {

    private final val mockEventAdapter = mockk<EventAdapter>()

    @Autowired
    private lateinit var eventService: EventService

    @Autowired
    private lateinit var eventRepository: EventRepository

    @Test
    fun `getAndCreateEvents should process every Event from the adapter`() {

        val eventsScheduler = EventsScheduler(mockEventAdapter, eventService)

        val mockEvents = listOf(createEventEntity(), createEventEntity())

        every { mockEventAdapter.getEvents() } returns mockEvents

        eventsScheduler.getAndCreateEvents()

        eventRepository.findAll().toList() should {
            it.size shouldBe 2
        }
    }
}
