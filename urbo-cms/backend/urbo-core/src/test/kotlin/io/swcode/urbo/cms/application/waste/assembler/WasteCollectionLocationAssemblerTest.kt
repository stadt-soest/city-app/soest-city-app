package io.swcode.urbo.cms.application.waste.assembler

import io.kotest.matchers.shouldBe
import io.swcode.urbo.cms.domain.model.waste.WasteCollectionLocation
import io.swcode.urbo.cms.domain.model.waste.WasteCollectionDate
import io.swcode.urbo.cms.domain.model.waste.WasteType
import org.junit.jupiter.api.Test
import java.time.Instant
import java.util.*

class WasteCollectionLocationAssemblerTest {

    @Test
    fun `should convert Waste entity to WasteResponseDto`() {

        val wasteCollectionLocation = WasteCollectionLocation(
            id = UUID.randomUUID(),
            wasteId = "someWasteId",
            city = "SampleCity",
            street = "SampleStreet",
            wasteCollectionDates = mutableListOf()
        )

        val wasteCollectionDate = WasteCollectionDate(
            id = UUID.randomUUID(),
            date = Instant.now(),
            wasteType = WasteType.BIOWASTE,
            wasteCollectionLocation = wasteCollectionLocation
        )

        wasteCollectionLocation.wasteCollectionDates.add(wasteCollectionDate)

        val wasteDto = WasteAssembler.toResponseDto(wasteCollectionLocation)

        wasteDto.id shouldBe wasteCollectionLocation.id
        wasteDto.city shouldBe wasteCollectionLocation.city
        wasteDto.street shouldBe wasteCollectionLocation.street
        wasteDto.wasteDates.size shouldBe 1
        wasteDto.wasteDates[0].id shouldBe wasteCollectionDate.id
        wasteDto.wasteDates[0].date shouldBe wasteCollectionDate.date
        wasteDto.wasteDates[0].wasteType shouldBe wasteCollectionDate.wasteType
    }
}
