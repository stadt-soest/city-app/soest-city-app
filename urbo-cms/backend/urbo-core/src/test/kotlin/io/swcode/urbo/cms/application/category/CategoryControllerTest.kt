package io.swcode.urbo.cms.application.category

import io.swcode.urbo.cms.WithUserContext
import io.swcode.urbo.cms.BaseControllerTest
import io.swcode.urbo.cms.application.CorePermission
import io.swcode.urbo.cms.application.shared.image.localization.LocalizationDto
import io.swcode.urbo.cms.domain.model.category.*
import io.swcode.urbo.cms.domain.model.shared.Language
import io.swcode.urbo.cms.domain.model.shared.Localization
import org.hamcrest.Matchers.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.*
import testUtil.createEventCategory
import testUtil.createNewsCategory

@WithUserContext(permissions = [CorePermission.CATEGORY_READ, CorePermission.CATEGORY_WRITE, CorePermission.CATEGORY_DELETE])
class CategoryControllerTest : BaseControllerTest() {

    @Autowired
    private lateinit var sourceCategoryRepository: SourceCategoryRepository

    @Autowired
    private lateinit var categoryRepository: CategoryRepository

    lateinit var eventCategory: Category
    lateinit var newsCategory: Category


    @BeforeEach
    fun setup() {
        val eventSourceCategory = sourceCategoryRepository.save(SourceCategory("Musik", CategoryType.EVENTS))

        eventCategory = createEventCategory {
            update(
                listOf(
                    Localization("Music", Language.EN),
                    Localization("Musik", Language.DE)
                ),
                listOf(eventSourceCategory),
                null,
                Icon("filename.jpg"),
            )
        }.let {
            categoryRepository.save(it)
        }

        val newsSourceCategory = sourceCategoryRepository.save(SourceCategory("Technologie", CategoryType.NEWS))

        newsCategory = createNewsCategory {
            update(
                listOf(
                    Localization("Technology", Language.EN),
                    Localization("Technologie", Language.DE)
                ),
                listOf(newsSourceCategory),
                null,
                Icon("filename.jpg"),
            )
        }.let {
            categoryRepository.save(it)
        }
    }

    @Test
    fun `should get event categories`() {
        mockMvc.get("/v1/categories") {
            param("type", "EVENTS")
        }
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.size()", `is`(1))
                jsonPath("$.[0].id", `is`(eventCategory.id.toString()))
                jsonPath("$.[0].localizedNames.size()", `is`(2))
                jsonPath(
                    "$.[0].localizedNames",
                    containsInAnyOrder(
                        mapOf("language" to "DE", "value" to "Musik"),
                        mapOf("language" to "EN", "value" to "Music")
                    )
                )
                jsonPath("$.[0].customIcon.filename", `is`("filename.jpg"))
                jsonPath("$.[0].sourceCategories.size()", `is`(1))
            }
    }

    @Test
    fun `should get category`() {
        mockMvc.get("/v1/categories/${eventCategory.id}")
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.id", `is`(eventCategory.id.toString()))
                jsonPath("$.localizedNames.size()", `is`(2))
                jsonPath(
                    "$.localizedNames",
                    containsInAnyOrder(
                        mapOf("language" to "DE", "value" to "Musik"),
                        mapOf("language" to "EN", "value" to "Music")
                    )
                )
                jsonPath("$.sourceCategories.size()", `is`(1))
            }
    }

    @Test
    fun `should delete category`() {
        mockMvc.delete("/v1/categories/${eventCategory.id}")
            .andExpect { status { isNoContent() } }
    }

    @Test
    fun `should get news categories`() {
        mockMvc.get("/v1/categories") {
            param("type", "NEWS")
        }
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.size()", `is`(1))
                jsonPath("$.[0].id", `is`(newsCategory.id.toString()))
                jsonPath("$.[0].localizedNames.size()", `is`(2))
                jsonPath(
                    "$.[0].localizedNames",
                    containsInAnyOrder(
                        mapOf("language" to "DE", "value" to "Technologie"),
                        mapOf("language" to "EN", "value" to "Technology")
                    )
                )
                jsonPath("$.[0].sourceCategories.size()", `is`(1))
            }
    }

    @Test
    fun `should create news category`() {
        val request = CategoryCreateDto(listOf(LocalizationDto("Technologie", Language.DE)), CategoryType.NEWS)

        mockMvc.post("/v1/categories") {
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(request)
            accept = MediaType.APPLICATION_JSON
        }
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.id", notNullValue())
                jsonPath("$.localizedNames.size()", `is`(1))
                jsonPath("$.type", `is`("NEWS"))
                jsonPath(
                    "$.localizedNames",
                    containsInAnyOrder(
                        mapOf("language" to "DE", "value" to "Technologie"),
                    )
                )
                jsonPath("$.sourceCategories.size()", `is`(0))
            }
    }

    @Test
    fun `should create event category`() {
        val request = CategoryCreateDto(listOf(LocalizationDto("Musik", Language.DE)), CategoryType.EVENTS)

        mockMvc.post("/v1/categories") {
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(request)
            accept = MediaType.APPLICATION_JSON
        }
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.id", notNullValue())
                jsonPath("$.localizedNames.size()", `is`(1))
                jsonPath("$.type", `is`("EVENTS"))
                jsonPath(
                    "$.localizedNames",
                    containsInAnyOrder(
                        mapOf("language" to "DE", "value" to "Musik"),
                    )
                )
                jsonPath("$.sourceCategories.size()", `is`(0))
            }
    }


    @Test
    fun `should update category`() {
        val existingSourceCategory = newsCategory.sourceCategories.first()

        val newSourceCategory = sourceCategoryRepository.save(SourceCategory("Technik", CategoryType.NEWS))

        val request = CategoryUpdateDto(
            listOf(
                LocalizationDto("Technology", Language.EN),
                LocalizationDto("Technologie", Language.DE)
            ),
            null,
            listOf(existingSourceCategory.name, newSourceCategory.name),
            null,
            CategoryColorsDto("#FF0000", "#00FF00"),
        )

        mockMvc.put("/v1/categories/${newsCategory.id}") {
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(request)
            accept = MediaType.APPLICATION_JSON
        }
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.id", `is`(newsCategory.id.toString()))
                jsonPath("$.localizedNames.size()", `is`(2))
                jsonPath(
                    "$.localizedNames",
                    containsInAnyOrder(
                        mapOf("language" to "DE", "value" to "Technologie"),
                        mapOf("language" to "EN", "value" to "Technology")
                    )
                )
                jsonPath("$.sourceCategories.size()", `is`(2))
                jsonPath("$.sourceCategories", containsInAnyOrder(newSourceCategory.name, existingSourceCategory.name))
                jsonPath("$.colors.lightThemeHexColor", `is`("#FF0000"))
                jsonPath("$.colors.darkThemeHexColor", `is`("#00FF00"))
            }
    }

    @Test
    fun `get all news source categories`() {
        mockMvc.get("/v1/source-categories") {
            param("type", "NEWS")
        }
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.size()", `is`(1))
                jsonPath("$", containsInAnyOrder(newsCategory.sourceCategories.first().name))
            }
    }

    @Test
    fun `get all event source categories`() {
        mockMvc.get("/v1/source-categories") {
            param("type", "EVENTS")
        }
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.size()", `is`(1))
                jsonPath("$", containsInAnyOrder(eventCategory.sourceCategories.first().name))
            }
    }

    @Test
    fun `filter mapped source categories`() {
        sourceCategoryRepository.save(SourceCategory("Unmapped", CategoryType.NEWS))

        mockMvc.get("/v1/source-categories") {
            param("type", "NEWS")
            param("mapped", "true")
        }
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.size()", `is`(1))
                jsonPath("$[0]", `is`(newsCategory.sourceCategories.first().name))
            }
    }

    @Test
    fun `filter unmapped source categories`() {
        val unmappedSourceCategory = sourceCategoryRepository.save(SourceCategory("Unmapped", CategoryType.NEWS))

        mockMvc.get("/v1/source-categories") {
            param("type", "NEWS")
            param("mapped", "false")
        }
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.size()", `is`(1))
                jsonPath("$[0]", `is`(unmappedSourceCategory.name))
            }
    }

    @ParameterizedTest(name = "should reject updating category: {1}")
    @MethodSource("provideInvalidCategoryUpdateDto")
    fun `should reject updating category`(categoryUpdateDto: CategoryUpdateDto, description: String) {
        val category = categoryRepository.save(createNewsCategory())

        mockMvc.put("/v1/categories/${category.id}") {
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(categoryUpdateDto)
        }.andExpect { status { isBadRequest() } }
    }

    @ParameterizedTest(name = "should reject creating category: {1}")
    @MethodSource("provideInvalidCategoryCreateDto")
    fun `should reject creating category`(categoryCreateDto: CategoryCreateDto, description: String) {
        mockMvc.post("/v1/categories") {
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(categoryCreateDto)
            accept = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isBadRequest() }
        }
    }

    @Test
    fun `should update category order`() {
        val categories = listOf(
            CategoryUpdateOrderDto(id = eventCategory.id, order = 1),
            CategoryUpdateOrderDto(id = newsCategory.id, order = 0)
        )

        mockMvc.patch("/v1/categories/order") {
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(categories)
            accept = MediaType.APPLICATION_JSON
        }
            .andExpect { status { isNoContent() } }

    }


    companion object {

        @JvmStatic
        fun provideInvalidCategoryCreateDto() = listOf(
            Arguments.of(
                buildInvalidCategoryCreateDto(
                    localization = LocalizationDto("x".repeat(256), Language.EN)
                ),
                "Localization value too long"
            ),
            Arguments.of(
                buildInvalidCategoryCreateDto(
                    localization = LocalizationDto("", Language.EN)
                ),
                "Empty localization value"
            ),
            Arguments.of(
                buildInvalidCategoryCreateDto(
                    localization = LocalizationDto("   ", Language.EN)
                ),
                "Localization value is only whitespace"
            ),
            Arguments.of(
                buildInvalidCategoryCreateDto(
                    hexColor = "#12345"
                ),
                "Invalid hex color format (too short)"
            ),
            Arguments.of(
                buildInvalidCategoryCreateDto(
                    hexColor = "123456"
                ),
                "Invalid hex color format (missing #)"
            ),
            Arguments.of(
                buildInvalidCategoryCreateDto(
                    hexColor = "#ZZZZZZ"
                ),
                "Invalid hex color format (invalid characters)"
            ),
        )

        @JvmStatic
        fun provideInvalidCategoryUpdateDto() = listOf(
            Arguments.of(
                buildInvalidCategoryUpdateDto(sourceCategories = List(1001) { "source category" }),
                "Too many source categories"
            ),
            Arguments.of(
                buildInvalidCategoryUpdateDto(sourceCategories = List(5) { "too long source category text".repeat(100) }),
                "Too long source category text"
            ),
            Arguments.of(
                buildInvalidCategoryUpdateDto(
                    localizedNames = listOf(LocalizationDto("too long localization value".repeat(255), Language.EN))
                ),
                "Too long localization value"
            ),
            Arguments.of(
                buildInvalidCategoryUpdateDto(sourceCategories = List(5) { "   " }),
                "Blank source category values"
            ),
            Arguments.of(
                buildInvalidCategoryUpdateDto(
                    hexColor = "#12345"
                ),
                "Invalid hex color format (too short)"
            ),
            Arguments.of(
                buildInvalidCategoryUpdateDto(
                    hexColor = "123456"
                ),
                "Invalid hex color format (missing #)"
            ),
            Arguments.of(
                buildInvalidCategoryUpdateDto(
                    hexColor = "#ZZZZZZ"
                ),
                "Invalid hex color format (invalid characters)"
            ),
        )
    }
}

fun createCategoryUpdateDto(): CategoryUpdateDto = CategoryUpdateDto(
    localizedNames = listOf(LocalizationDto("Valid", Language.EN)),
    null,
    sourceCategories = List(1) { "Source $it" }
)

fun buildInvalidCategoryCreateDto(
    localization: LocalizationDto = LocalizationDto("Valid", Language.EN),
    type: CategoryType = CategoryType.NEWS,
    hexColor: String? = null,
): CategoryCreateDto {
    return CategoryCreateDto(
        localizedNames = listOf(localization),
        type = type,
        colors = hexColor?.let { CategoryColorsDto(it, it) })
}

fun buildInvalidCategoryUpdateDto(
    sourceCategories: List<String>? = null,
    localizedNames: List<LocalizationDto>? = null,
    hexColor: String? = null,
): CategoryUpdateDto {
    return createCategoryUpdateDto().copy(
        sourceCategories = sourceCategories ?: createCategoryUpdateDto().sourceCategories,
        localizedNames = localizedNames ?: createCategoryUpdateDto().localizedNames,
        colors = hexColor?.let { CategoryColorsDto(it, it) }
    )
}
