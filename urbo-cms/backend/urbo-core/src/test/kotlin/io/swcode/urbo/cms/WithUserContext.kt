package io.swcode.urbo.cms

import io.swcode.urbo.user.management.UserContextMarker
import io.swcode.urbo.cms.application.CorePermission
import io.swcode.urbo.user.management.UserContextSetupExtension
import org.junit.jupiter.api.extension.ExtendWith

@UserContextMarker
@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
@ExtendWith(UserContextSetupExtension::class)
annotation class WithUserContext(
    val permissions: Array<CorePermission> = []
)