package io.swcode.urbo.cms.application.parking

import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.swcode.urbo.cms.BaseTest
import io.swcode.urbo.cms.domain.model.parking.ParkingAreaRepository
import io.swcode.urbo.cms.domain.model.parking.ParkingService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.transaction.annotation.Transactional
import testUtil.mockParkingArea
import java.util.*

class ParkingSchedulerTest : BaseTest() {

    private final val mockParkingAdapter = mockk<ParkingAdapter>()

    @Autowired
    private lateinit var parkingService: ParkingService

    @Autowired
    private lateinit var parkingAreaRepository: ParkingAreaRepository

    @BeforeEach
    fun setUp() {
        parkingAreaRepository.deleteAll()
    }

    @Test
    @Transactional
    fun getAndCreateParkingAreas() {
        val parkingScheduler = ParkingScheduler(mockParkingAdapter, parkingService)

        val mockParkingAreas = listOf(
            mockParkingArea().copy(
                id = UUID.randomUUID(),
                externalId = "1",
            ),
            mockParkingArea().copy(
                id = UUID.randomUUID(),
                externalId = "2",
            )
        )
        every { mockParkingAdapter.getParkingAreas() } returns mockParkingAreas

        parkingScheduler.getAndCreateParkingAreas()

        parkingService.getAllParkingAreas(Pageable.unpaged()).should {
            it.content.size shouldBe 2
        }
    }
}
