package io.swcode.urbo.cms.application.parking.assembler

import io.kotest.matchers.shouldBe
import io.swcode.urbo.cms.application.parking.response.ParkingAreaResponseDto
import io.swcode.urbo.cms.application.shared.image.localization.LocalizationDto
import io.swcode.urbo.cms.domain.model.parking.*
import io.swcode.urbo.cms.domain.model.shared.Address
import io.swcode.urbo.cms.domain.model.shared.GeoCoordinates
import io.swcode.urbo.cms.domain.model.shared.Language
import io.swcode.urbo.cms.domain.model.shared.Localization
import org.junit.jupiter.api.Test
import java.time.DayOfWeek
import java.time.Instant
import java.time.LocalTime
import java.util.*

class ParkingAreaAssemblerTest {

    @Test
    fun `should correctly assemble ParkingAreaResponseDto from ParkingArea`() {
        val id = UUID.randomUUID()
        val parkingArea = ParkingArea(
            id = id,
            externalId = "ext123",
            name = "Test Parking",
            location = ParkingLocation(
                address = Address("123 Test St", "12345", "Test City"),
                coordinates = GeoCoordinates(12.345, 67.890)
            ),
            capacity = ParkingCapacity(100, 75, Instant.now()),
            openingTimes = mapOf(
                DayOfWeek.MONDAY to ParkingOpeningHours(LocalTime.of(8, 0), LocalTime.of(20, 0)),
                DayOfWeek.TUESDAY to ParkingOpeningHours(LocalTime.of(8, 0), LocalTime.of(20, 0)),
                DayOfWeek.WEDNESDAY to ParkingOpeningHours(LocalTime.of(8, 0), LocalTime.of(20, 0)),
                DayOfWeek.THURSDAY to ParkingOpeningHours(LocalTime.of(8, 0), LocalTime.of(20, 0)),
                DayOfWeek.FRIDAY to ParkingOpeningHours(LocalTime.of(8, 0), LocalTime.of(20, 0)),
                DayOfWeek.SATURDAY to ParkingOpeningHours(LocalTime.of(8, 0), LocalTime.of(20, 0)),
                DayOfWeek.SUNDAY to ParkingOpeningHours(LocalTime.of(8, 0), LocalTime.of(20, 0))
            ),
            status = ParkingStatus.OPEN,
            maximumAllowedHeight = "2.5m",
            priceDescription =
            mutableListOf(
                Localization("Preis: 1€/h", Language.DE),
                Localization("Price: 1€/h", Language.EN)
            ),
            displayName = "Test Parking",
            parkingType = ParkingType.PARKING_HOUSE,
            description = mutableListOf(
                Localization("Beschreibung", Language.DE),
                Localization("Description", Language.EN)
            ),
        )

        val dto = ParkingAreaResponseDto.fromDomain(parkingArea)

        with(dto) {
            id shouldBe parkingArea.id
            name shouldBe parkingArea.name
            location shouldBe parkingArea.location
            capacity shouldBe parkingArea.capacity
            openingTimes shouldBe parkingArea.openingTimes
            status shouldBe parkingArea.status
            displayName shouldBe parkingArea.displayName
            parkingType shouldBe parkingArea.parkingType
            description shouldBe parkingArea.description.map {
                LocalizationDto(
                    language = it.language,
                    value = it.value
                )
            }.toSet()
            maximumAllowedHeight shouldBe parkingArea.maximumAllowedHeight
            priceDescription shouldBe parkingArea.priceDescription.map {
                LocalizationDto(
                    language = it.language,
                    value = it.value
                )
            }.toSet()
        }
    }
}
