package io.swcode.urbo.cms.application.citizenService

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.collections.shouldNotContain
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import io.swcode.urbo.cms.WithUserContext
import io.swcode.urbo.cms.BaseControllerTest
import io.swcode.urbo.cms.application.CorePermission
import io.swcode.urbo.cms.application.citizenService.dto.CreateCitizenServiceDto
import io.swcode.urbo.cms.application.citizenService.dto.CreateTagDto
import io.swcode.urbo.cms.application.citizenService.dto.UpdateCitizenServiceDto
import io.swcode.urbo.cms.application.shared.image.localization.LocalizationDto
import io.swcode.urbo.cms.domain.model.citizenService.CitizenService
import io.swcode.urbo.cms.domain.model.citizenService.CitizenServiceRepository
import io.swcode.urbo.cms.domain.model.citizenService.CitizenServiceService
import io.swcode.urbo.cms.domain.model.citizenService.Tag
import io.swcode.urbo.cms.domain.model.citizenService.tag.TagService
import io.swcode.urbo.cms.domain.model.shared.Language
import io.swcode.urbo.cms.domain.model.shared.Localization
import io.swcode.urbo.common.error.UrboException
import org.hamcrest.Matchers.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.*
import testUtil.createCitizenService
import java.util.*

@WithUserContext(permissions = [CorePermission.CITIZEN_SERVICE_WRITE, CorePermission.CITIZEN_SERVICE_DELETE])
class CitizenControllerTest : BaseControllerTest() {

    @Autowired
    private lateinit var citizenServiceService: CitizenServiceService

    @Autowired
    private lateinit var tagService: TagService

    @Autowired
    private lateinit var citizenServiceRepository: CitizenServiceRepository

    @BeforeEach
    fun setup() {
        citizenServiceRepository.deleteAll()
    }

    @Test
    fun `should get citizen services`() {
        val citizenService = citizenServiceService.save(createCitizenService())

        mockMvc
            .get("/v1/citizenServices")
            .andExpect { status { HttpStatus.OK } }
            .andExpect { jsonPath("$.content.size()", `is`(1)) }
            .andExpect { jsonPath("$.content.[0].id", `is`(citizenService.id.toString())) }
            .andExpect { jsonPath("$.content.[0].title", `is`("title")) }
            .andExpect { jsonPath("$.content.[0].description", `is`("description")) }
    }

    @Test
    fun `should create a new citizen service`() {
        val createCitizenServiceDto = CreateCitizenServiceDto(
            title = "New Citizen Service",
            description = "Description of the new citizen service",
            url = "https://example.com",
            visible = true,
            favorite = false
        )

        mockMvc
            .post("/v1/citizenServices") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(createCitizenServiceDto)
            }
            .andExpect {
                status { isOk() }
                content {
                    contentType(MediaType.APPLICATION_JSON)
                    jsonPath("$.id", notNullValue())
                    jsonPath("$.title", `is`("New Citizen Service"))
                    jsonPath("$.description", `is`("Description of the new citizen service"))
                    jsonPath("$.url", `is`("https://example.com"))
                    jsonPath("$.visible", `is`(true))
                    jsonPath("$.favorite", `is`(false))
                }
            }

        val createdCitizenService = citizenServiceRepository.findAll().firstOrNull {
            it.title == "New Citizen Service"
                    && it.description == "Description of the new citizen service"
                    && it.url == "https://example.com"
                    && it.visible
                    && !it.favorite
        }

        createdCitizenService shouldNotBe null
    }

    @Test
    fun `should get citizen service by id`() {

        val citizenService = citizenServiceService.save(createCitizenService())

        mockMvc
            .get("/v1/citizenServices/{id}", citizenService.id)
            .andExpect {
                status { isOk() }
            }
    }

    @Test
    fun `should update an existing citizen service`() {
        val citizenService = citizenServiceService.save(createCitizenService())
        val updateCitizenServiceDto = UpdateCitizenServiceDto(
            title = "Updated Title",
            description = "Updated Description",
            url = "https://updated-example.com",
            visible = false,
            favorite = true
        )

        mockMvc
            .put("/v1/citizenServices/{id}", citizenService.id) {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(updateCitizenServiceDto)
            }
            .andExpect {
                status { isOk() }
                content {
                    contentType(MediaType.APPLICATION_JSON)
                    jsonPath("$.id", `is`(citizenService.id.toString()))
                    jsonPath("$.title", `is`("Updated Title"))
                    jsonPath("$.description", `is`("Updated Description"))
                    jsonPath("$.url", `is`("https://updated-example.com"))
                    jsonPath("$.visible", `is`(false))
                    jsonPath("$.favorite", `is`(true))
                }
            }

        val updatedCitizenService = citizenServiceRepository.findByIdOrNull(citizenService.id)
        updatedCitizenService shouldNotBe null
        updatedCitizenService!!.title shouldBe "Updated Title"
        updatedCitizenService.description shouldBe "Updated Description"
        updatedCitizenService.url shouldBe "https://updated-example.com"
        updatedCitizenService.visible shouldBe false
        updatedCitizenService.favorite shouldBe true
    }

    @Test
    fun `should delete a citizen service`() {
        val citizenService = citizenServiceService.save(createCitizenService())

        mockMvc
            .delete("/v1/citizenServices/{id}", citizenService.id)
            .andExpect {
                status { isNoContent() }
            }

        val deletedCitizenService = citizenServiceRepository.findByIdOrNull(citizenService.id)
        deletedCitizenService shouldBe null
    }

    @Test
    fun `should get all tags`() {
        repeat(3) {
            val citizenService = createCitizenService().copy(
                externalId = "$it",
                id = UUID.randomUUID(),
                tags = mutableListOf(
                    Tag(
                        id = UUID.randomUUID(), localizedNames =
                            listOf(
                                Localization(
                                    value = "Tag $it",
                                    language = Language.DE,
                                )
                            )
                    )
                )
            )
            citizenServiceService.save(citizenService)
        }

        mockMvc
            .get("/v1/citizenServices/tags")
            .andExpect { status { isOk() } }
            .andExpect { jsonPath("$.size()", `is`(3)) }
            .andExpect { jsonPath("$[0].id", notNullValue()) }
            .andExpect { jsonPath("$[0].localizedNames[0].value", startsWith("Tag")) }
    }

    @Test
    fun `should get citizen services by favorite filter`() {
        val favoriteService = createCitizenService().copy(
            externalId = "favorite-service",
            favorite = true,
            title = "Favorite Service"
        )
        val nonFavoriteService = createCitizenService().copy(
            externalId = "non-favorite-service",
            favorite = false,
            title = "Non-Favorite Service"
        )

        citizenServiceService.save(favoriteService)
        citizenServiceService.save(nonFavoriteService)

        mockMvc
            .get("/v1/citizenServices?favorite=true")
            .andExpect { status { isOk() } }
            .andExpect { jsonPath("$.content.size()", `is`(1)) }
            .andExpect { jsonPath("$.content.[0].title", `is`("Favorite Service")) }

        mockMvc
            .get("/v1/citizenServices?favorite=false")
            .andExpect { status { isOk() } }
            .andExpect { jsonPath("$.content.size()", `is`(1)) }
            .andExpect { jsonPath("$.content.[0].title", `is`("Non-Favorite Service")) }
    }

    @Test
    fun `should get tag`() {
        val citizenService = createCitizenService()

        citizenServiceService.save(citizenService)

        mockMvc
            .get("/v1/citizenServices/tags/{id}", citizenService.tags.first().id)
            .andExpect { status { isOk() } }
            .andDo { print() }
            .andExpect { jsonPath("$.id", notNullValue()) }
            .andExpect { jsonPath("$.localizedNames[0].value", startsWith("tag")) }
    }

    @Test
    fun `should get citizen services by tag`() {
        val tagId = UUID.randomUUID()

        repeat(5) { i ->
            val citizenService = createCitizenService().apply {
                externalId = "$i"
                tags = mutableListOf(
                    Tag(
                        id = tagId,
                        localizedNames = listOf(
                            Localization(
                                value = "Tag $i",
                                language = Language.DE,
                            )
                        )
                    )
                )
                title = "Title $i"
            }
            citizenServiceService.save(citizenService)
        }

        mockMvc
            .get("/v1/citizenServices?tagId=${tagId}&page=0&size=3")
            .andExpect { status { isOk() } }
            .andExpect { jsonPath("$.content.size()", `is`(3)) }
            .andExpect { jsonPath("$.content[0].title", startsWith("Title")) }
            .andExpect { jsonPath("$.page.totalElements", `is`(5)) }
    }

    @Test
    fun `should return correct number of CitizenServices for given page size`() {
        val totalCitizens = 10
        repeat(totalCitizens) { i ->
            val entity =
                createCitizenService().copy(externalId = "$i", id = UUID.randomUUID(), title = "Title ${i + 1}")
            citizenServiceService.save(entity)
        }

        mockMvc
            .get("/v1/citizenServices?page=0&size=5")
            .andExpect {
                status { isOk() }
            }
            .andExpect {
                jsonPath("$.content.size()", `is`(5))
            }
    }

    @Test
    fun `should return correct CitizenServices for given page number`() {
        val totalCitizens = 10
        repeat(totalCitizens) { i ->
            val entity =
                createCitizenService().copy(externalId = "$i", id = UUID.randomUUID(), title = "Title ${i + 1}")
            citizenServiceService.save(entity)
        }

        mockMvc
            .get("/v1/citizenServices?page=1&size=5")
            .andExpect {
                status { isOk() }
            }
            .andExpect {
                jsonPath("$.content[0].title", `is`("Title 6"))
            }
    }

    @Test
    @WithUserContext
    fun `should not update citizen service visibility`() {

        val citizenService = createCitizenService().copy(
            visible = false
        )
        citizenServiceService.save(citizenService)

        mockMvc
            .patch("/v1/citizenServices/{id}/visibility?visible=true", citizenService.id)
            .andExpect {
                status { isForbidden() }
            }
    }

    @Test
    fun `should update citizen service visibility`() {
        val citizenService = createCitizenService().copy(
            visible = false
        )
        citizenServiceService.save(citizenService)

        mockMvc
            .patch("/v1/citizenServices/{id}/visibility?visible=true", citizenService.id)
            .andExpect {
                status { isNoContent() }
            }

        val updatedCitizenService = citizenServiceRepository.findByIdOrNull(citizenService.id)
        assert(updatedCitizenService!!.visible)
    }

    @Test
    fun `should update citizen service favorite`() {
        val citizenService = createCitizenService().copy(
            favorite = false
        )
        citizenServiceService.save(citizenService)

        mockMvc
            .patch("/v1/citizenServices/{id}/favorite?favorite=true", citizenService.id)
            .andExpect {
                status { isNoContent() }
            }

        val updatedCitizenService = citizenServiceRepository.findByIdOrNull(citizenService.id)
        assert(updatedCitizenService!!.favorite)
    }

    @Test
    fun `should create a new tag`() {
        val createTagDto = CreateTagDto(
            id = null,
            localizedNames = listOf(
                LocalizationDto(value = "Test Tag DE", language = Language.DE),
                LocalizationDto(value = "Test Tag EN", language = Language.EN)
            )
        )

        mockMvc
            .put("/v1/citizen-service-tags") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(createTagDto)
            }
            .andExpect {
                status { isOk() }
                jsonPath("$.id", notNullValue())
                jsonPath("$.localizedNames[0].value", `is`("Test Tag DE"))
                jsonPath("$.localizedNames[1].value", `is`("Test Tag EN"))
            }
    }

    @Test
    fun `should update an existing tag`() {
        val tag = Tag(
            id = UUID.randomUUID(),
            localizedNames = listOf(
                Localization(value = "Old Tag DE", language = Language.DE),
                Localization(value = "Old Tag EN", language = Language.EN)
            )
        )
        tagService.createOrUpdateTag(tag)

        val updateTagDto = CreateTagDto(
            id = tag.id,
            localizedNames = listOf(
                LocalizationDto(value = "Updated Tag DE", language = Language.DE),
                LocalizationDto(value = "Updated Tag EN", language = Language.EN)
            )
        )

        mockMvc
            .put("/v1/citizen-service-tags") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(updateTagDto)
            }
            .andExpect {
                status { isOk() }
                jsonPath("$.id", `is`(tag.id.toString()))
                jsonPath("$.localizedNames[0].value", `is`("Updated Tag DE"))
                jsonPath("$.localizedNames[1].value", `is`("Updated Tag EN"))
            }
    }

    @Test
    fun `should delete a tag`() {
        val tag = Tag(
            id = UUID.randomUUID(),
            localizedNames = listOf(
                Localization(value = "Tag to Delete", language = Language.DE)
            )
        )
        tagService.createOrUpdateTag(tag)

        mockMvc
            .delete("/v1/citizen-service-tags/{id}", tag.id)
            .andExpect {
                status { isNoContent() }
            }
    }

    @Test
    fun `should delete a tag associated with citizen services`() {
        val tag = createAndSaveTag("Tag to Delete")
        val citizenServices = createAndSaveCitizenServicesWithTag(tag, count = 3)

        citizenServices.forEach { citizenService: CitizenService ->
            val retrievedService = citizenServiceRepository.findByIdOrNull(citizenService.id)
            retrievedService shouldNotBe null
            retrievedService!!.tags shouldContain tag
        }

        mockMvc
            .delete("/v1/citizen-service-tags/{id}", tag.id)
            .andExpect {
                status { isNoContent() }
            }

        shouldThrow<UrboException> {
            tagService.getTagById(tag.id)
        }

        citizenServices.forEach { citizenService ->
            val updatedService = citizenServiceRepository.findByIdOrNull(citizenService.id)
            updatedService shouldNotBe null
            updatedService!!.tags shouldNotContain tag
        }
    }

    @Test
    fun `should assign multiple citizen services to a tag`() {
        val tagId = UUID.randomUUID()
        val tag = Tag(
            id = tagId,
            localizedNames = listOf(
                Localization(
                    value = "Test Tag",
                    language = Language.EN
                )
            )
        )
        val savedTag = tagService.createTag(tag)

        val citizenServices = (1..3).map {
            createCitizenService().apply {
                externalId = "$it"
                title = "Title $it"
                tags = mutableListOf()
            }
        }.map(citizenServiceService::save)

        val citizenServiceIds = citizenServices.map { it.id }

        mockMvc
            .patch("/v1/tags/$tagId/citizen-services") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(citizenServiceIds)
            }
            .andExpect {
                status { isNoContent() }
            }

        citizenServices.forEach {
            val updatedService = citizenServiceRepository.findByIdOrNull(it.id)
            assert(updatedService!!.tags.contains(savedTag))
        }
    }

    private fun createAndSaveTag(name: String): Tag {
        val tag = Tag(
            id = UUID.randomUUID(),
            localizedNames = listOf(
                Localization(value = name, language = Language.DE)
            )
        )
        return tagService.createTag(tag)
    }

    private fun createAndSaveCitizenServicesWithTag(tag: Tag, count: Int): List<CitizenService> {
        return (1..count).map { index ->
            createCitizenService().apply {
                externalId = index.toString()
                title = "Title $index"
                tags = mutableListOf(tag)
            }
        }.map(citizenServiceService::save)
    }
}
