package io.swcode.urbo.cms.application.accessibilityFeedback.assembler

import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeTypeOf
import io.swcode.urbo.cms.application.accessibilityFeedback.dto.AccessibilityRequestDto
import io.swcode.urbo.cms.application.feedback.dto.EnvironmentDto
import org.junit.jupiter.api.Test
import java.util.*

class AccessibilityFeedbackAssemblerTest {

    @Test
    fun `should map AccessibilityRequestDto to AccessibilityFeedback`() {
        val accessibilityRequestDto = AccessibilityRequestDto(
            answer = "Test answer",
            environment = EnvironmentDto(
                deviceModelName = "Test device",
                operatingSystemName = "Test OS",
                operatingSystemVersion = "1.0",
                appSettings = "Test settings",
                appVersion = "1.0",
                browserName = "Test browser",
                usedAs = "Test usage"
            ),
            imageFilenames = listOf()
        )

        val accessibilityFeedback = AccessibilityFeedbackAssembler.toEntity(accessibilityRequestDto)

        accessibilityFeedback.answer shouldBe accessibilityRequestDto.answer
        accessibilityFeedback.environment.deviceModelName shouldBe accessibilityRequestDto.environment.deviceModelName
        accessibilityFeedback.environment.operatingSystem.name shouldBe accessibilityRequestDto.environment.operatingSystemName
        accessibilityFeedback.environment.operatingSystem.version shouldBe accessibilityRequestDto.environment.operatingSystemVersion
        accessibilityFeedback.environment.appSettings shouldBe accessibilityRequestDto.environment.appSettings
        accessibilityFeedback.environment.app.version shouldBe accessibilityRequestDto.environment.appVersion
        accessibilityFeedback.environment.app.browserName shouldBe accessibilityRequestDto.environment.browserName
        accessibilityFeedback.environment.app.usedAs shouldBe accessibilityRequestDto.environment.usedAs
        accessibilityFeedback.imageFilenames.size shouldBe 0
        accessibilityFeedback.id.shouldBeTypeOf<UUID>()
    }
}