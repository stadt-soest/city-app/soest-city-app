package io.swcode.urbo.cms.testutil

import io.swcode.urbo.cms.domain.model.event.Event
import io.swcode.urbo.cms.domain.model.event.EventOccurrence
import io.swcode.urbo.cms.domain.model.event.EventRepository
import io.swcode.urbo.cms.domain.model.event.EventStatus
import io.swcode.urbo.cms.domain.model.shared.Image
import io.swcode.urbo.cms.domain.model.shared.ImageFile
import testUtil.createEventEntity
import java.time.Duration
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.UUID

class TestDataProvider(
    val eventRepository: EventRepository
) {
    fun createEvent(
        occurrenceCount: Int,
        startAt: Instant = Instant.parse("2024-04-01T10:00:00.00Z"),
        endAt: Instant = Instant.parse("2024-04-01T15:00:00.00Z"),
        dateCreated: Instant = Instant.parse("2024-03-01T10:00:00.00Z"),
        sourceName: String = "Soest",
    ): Event {
        val event = createEventEntity().also {
            it.title = "MyTitle"
            it.image = Image("mySource", ImageFile("thumbnail.jpg", 512, 256), ImageFile("highres.jpg", 2048, 2048))
        }

        event.occurrences = 0.rangeUntil(occurrenceCount).map {
            EventOccurrence(
                UUID.randomUUID(),
                true,
                event,
                UUID.randomUUID().toString(),
                dateCreated.plus(Duration.ofDays(it.toLong())),
                EventStatus.SCHEDULED,
                startAt.plus(it.toLong(), ChronoUnit.DAYS),
                endAt.plus(it.toLong(), ChronoUnit.DAYS),
                sourceName
            )
        }.toMutableList()

        return eventRepository.saveAndFlush(event)
    }

    fun createRunningEvent(now: Instant): Event {
        val pastStartTime = now.minus(1, ChronoUnit.HOURS)
        val futureEndTime = now.plus(1, ChronoUnit.HOURS)
        return createEvent(1, startAt = pastStartTime, endAt = futureEndTime)
    }

    fun createUpcomingEvent(now: Instant): Event {
        val futureStartTime = now.plus(1, ChronoUnit.HOURS)
        val futureEndTime = futureStartTime.plus(1, ChronoUnit.HOURS)
        return createEvent(1, startAt = futureStartTime, endAt = futureEndTime)
    }

    fun createPastEvent(now: Instant) {
        val pastStartTime = now.minus(3, ChronoUnit.HOURS)
        val pastEndTime = pastStartTime.plus(1, ChronoUnit.HOURS)
        createEvent(1, startAt = pastStartTime, endAt = pastEndTime)
    }
}