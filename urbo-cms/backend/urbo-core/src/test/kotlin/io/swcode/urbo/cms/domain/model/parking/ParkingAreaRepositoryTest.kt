package io.swcode.urbo.cms.domain.model.parking

import io.kotest.matchers.equals.shouldBeEqual
import io.swcode.urbo.cms.BaseTest
import io.swcode.urbo.cms.domain.model.shared.Address
import io.swcode.urbo.cms.domain.model.shared.GeoCoordinates
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.transaction.annotation.Transactional
import java.time.DayOfWeek
import java.time.Instant
import java.time.LocalTime
import java.util.*

class ParkingAreaRepositoryTest : BaseTest() {
    @Autowired
    private lateinit var parkingAreaRepository: ParkingAreaRepository

    @BeforeEach
    fun setup() {
        parkingAreaRepository.deleteAll()
    }

    @Test
    @Transactional
    fun `should create ParkingArea`() {
        val parkingArea = ParkingArea(
            id = UUID.randomUUID(),
            externalId = "yey",
            name = "Das Parkhaus",
            location = ParkingLocation(
                address = Address(
                    street = "Musterstrasse 1",
                    postalCode = "59494",
                    city = "Soest"
                ),
                coordinates = GeoCoordinates(
                    latitude = 54.123254356,
                    longitude = 8.12324234
                )
            ),
            capacity = ParkingCapacity(
                total = 5,
                remaining = 2,
                modifiedAt = Instant.now()
            ),
            openingTimes = mapOf(DayOfWeek.MONDAY to ParkingOpeningHours(LocalTime.now(), LocalTime.now().plusHours(1))),
            status = ParkingStatus.OPEN,
            maximumAllowedHeight = "2.5m",
            priceDescription = mutableListOf(),
            description = mutableListOf(),
            displayName = "Das Parkhaus",
            parkingType = ParkingType.PARKING_HOUSE
        )

        parkingAreaRepository.save(parkingArea)

        parkingAreaRepository.findByIdOrNull(parkingArea.id)!!.shouldBeEqual(parkingArea)
    }
}
