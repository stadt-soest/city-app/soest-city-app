package io.swcode.urbo.cms.domain.command.handler

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.swcode.urbo.cms.domain.command.DeleteImageCommand
import io.swcode.urbo.cms.domain.model.shared.Image
import io.swcode.urbo.cms.domain.model.shared.ImageFile
import io.swcode.urbo.cms.s3.api.S3Adapter
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class DeleteImageCommandHandlerTest {
    private lateinit var handler: DeleteImageCommandHandler
    private lateinit var s3Adapter: S3Adapter

    @BeforeEach
    fun setup() {
        s3Adapter = mockk(relaxed = true)

        handler = DeleteImageCommandHandler(s3Adapter)
    }

    @Test
    fun `should handle delete image command and remove the image from S3`() {
        val fileName = "testFile.jpg"
        val image = Image("testSource", ImageFile(fileName))
        val command = DeleteImageCommand(image)

        every { s3Adapter.deleteS3Object(match { it.key == fileName }) } returns Unit

        handler.handle(command)

        verify { s3Adapter.deleteS3Object(match { it.key == fileName }) }
    }

    @Test
    fun `should handle delete image command with null thumbnail and highRes`() {
        val image = Image("testSource")
        val command = DeleteImageCommand(image)

        handler.handle(command)

        verify(exactly = 0) { s3Adapter.deleteS3Object(any()) }
    }

    @Test
    fun `should handle delete image command with only thumbnail`() {
        val thumbnailFileName = "thumbnail.jpg"
        val image = Image("testSource", ImageFile(thumbnailFileName))
        val command = DeleteImageCommand(image)

        every { s3Adapter.deleteS3Object(match { it.key == thumbnailFileName }) } returns Unit

        handler.handle(command)

        verify { s3Adapter.deleteS3Object(match { it.key == thumbnailFileName }) }
    }

    @Test
    fun `should handle delete image command with only highRes`() {
        val highResFileName = "highRes.jpg"
        val image = Image("testSource", highRes = ImageFile(highResFileName))
        val command = DeleteImageCommand(image)

        every { s3Adapter.deleteS3Object(match { it.key == highResFileName }) } returns Unit

        handler.handle(command)

        verify { s3Adapter.deleteS3Object(match { it.key == highResFileName }) }
    }

    @Test
    fun `should handle delete image command with both thumbnail and highRes`() {
        val thumbnailFileName = "thumbnail.jpg"
        val highResFileName = "highRes.jpg"
        val image = Image("testSource", ImageFile(thumbnailFileName), ImageFile(highResFileName))
        val command = DeleteImageCommand(image)

        every { s3Adapter.deleteS3Object(match { it.key == thumbnailFileName || it.key == highResFileName }) } returns Unit

        handler.handle(command)

        verify(exactly = 1) { s3Adapter.deleteS3Object(match { it.key == thumbnailFileName }) }
        verify(exactly = 1) { s3Adapter.deleteS3Object(match { it.key == highResFileName }) }
    }
}
