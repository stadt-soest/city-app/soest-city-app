package io.swcode.urbo.cms.application.accessibilityFeedback

import io.swcode.urbo.cms.WithUserContext
import io.swcode.urbo.cms.BaseControllerTest
import io.swcode.urbo.cms.application.CorePermission
import io.swcode.urbo.cms.application.accessibilityFeedback.dto.AccessibilityRequestDto
import io.swcode.urbo.cms.application.feedback.dto.EnvironmentDto
import io.swcode.urbo.cms.domain.model.accessibilityFeedback.AccessibilityFeedback
import io.swcode.urbo.cms.domain.model.accessibilityFeedback.AccessibilityFeedbackService
import io.swcode.urbo.cms.domain.model.feedback.response.AppDetails
import io.swcode.urbo.cms.domain.model.feedback.response.Environment
import io.swcode.urbo.cms.domain.model.feedback.response.OperatingSystem
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import java.time.Instant
import java.util.*

@WithUserContext(permissions = [CorePermission.ACCESSIBILITY_SUBMISSION_READ])
class AccessibilityFeedbackControllerTest : BaseControllerTest() {

    @Autowired
    private lateinit var accessibilityFeedbackService: AccessibilityFeedbackService

    @Test
    fun `should submit accessibility feedback`() {

        val dto = AccessibilityRequestDto(
            answer = "Button too small",
            environment = EnvironmentDto(
                deviceModelName = "Test Device",
                operatingSystemName = "Android",
                operatingSystemVersion = "10",
                appVersion = "1.0",
                browserName = "Chrome",
                usedAs = "app",
                appSettings = """{ "userPreferences":""} """.trimIndent()
            ),
            imageFilenames = listOf()
        )

        mockMvc.post("/v1/accessibility-feedback-submissions") {
            content = objectMapper.writeValueAsString(dto)
            contentType = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isOk() }
        }
    }

    @Test
    fun `find all feedbacks`() {
        accessibilityFeedbackService.submitAccessibilityFeedback(
            AccessibilityFeedback(
                UUID.randomUUID(),
                "test",
                Instant.now(),
                mutableListOf(),
                Environment(
                    "test",
                    OperatingSystem("test", "1.0.0"),
                    AppDetails("1.0.0", "firefox", ""),
                    """{ "userPreferences":""} """.trimIndent()
                ),
            ),
        )

        mockMvc.get("/v1/accessibility-feedback-submissions")
            .andExpect { status { isOk() } }
    }

    @Test
    fun `get feedback`() {
        val feedback = accessibilityFeedbackService.submitAccessibilityFeedback(
            AccessibilityFeedback(
                UUID.randomUUID(),
                "test",
                Instant.now(),
                mutableListOf(),
                Environment(
                    "test",
                    OperatingSystem("test", "1.0.0"),
                    AppDetails("1.0.0", "firefox", ""),
                    """{ "userPreferences":""} """.trimIndent()
                )
            ),
        )

        mockMvc.get("/v1/accessibility-feedback-submissions/${feedback.id}")
            .andExpect { status { isOk() } }
    }
}
