package io.swcode.urbo.cms.application.feedback

import io.swcode.urbo.cms.WithUserContext
import io.swcode.urbo.cms.BaseControllerTest
import io.swcode.urbo.cms.application.feedback.dto.EnvironmentDto
import io.swcode.urbo.cms.application.feedback.dto.FeedbackAnswerRequestDto
import io.swcode.urbo.cms.application.feedback.dto.FeedbackFormCreateDto
import io.swcode.urbo.cms.application.feedback.dto.FeedbackSubmissionRequestDto
import io.swcode.urbo.cms.domain.model.feedback.FeedbackForm
import io.swcode.urbo.cms.domain.model.feedback.FeedbackFormRepository
import io.swcode.urbo.cms.domain.model.feedback.FeedbackService
import io.swcode.urbo.cms.domain.model.feedback.question.FeedbackQuestion
import io.swcode.urbo.cms.domain.model.feedback.question.QuestionType
import io.swcode.urbo.cms.domain.model.feedback.response.FeedbackOption
import io.swcode.urbo.cms.domain.model.shared.Language
import io.swcode.urbo.cms.domain.model.shared.Localization
import io.swcode.urbo.cms.application.CorePermission
import org.hamcrest.CoreMatchers.`is`
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import java.util.*

@WithUserContext(permissions = [CorePermission.FEEDBACK_SUBMISSION_READ, CorePermission.FEEDBACK_SUBMISSION_WRITE])
class FeedbackControllerTest : BaseControllerTest() {

    @Autowired
    private lateinit var feedbackFormRepository: FeedbackFormRepository

    @Autowired
    private lateinit var feedbackService: FeedbackService

    @Test
    fun `should create new feedback form`() {
        val feedbackFormDto = createFeedbackCreateDto()

        mockMvc.post("/v1/feedback-form") {
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(feedbackFormDto)
        }
            .andExpect { status { isOk() } }
            .andExpect { jsonPath("$.version", `is`(1)) }
    }

    @Test
    fun `should get questions by version`() {
        val feedbackForm = createAndSaveFeedbackForm()

        mockMvc.get("/v1/feedback-form?version=${feedbackForm.version}")
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.questions.size()", `is`(feedbackForm.questions.size))
                jsonPath("$.id", `is`(feedbackForm.id.toString()))
                jsonPath("$.questions[0].questionName[0].value", `is`("What do you like most?"))
                jsonPath("$.questions[0].questionName[0].language", `is`("EN"))
            }
    }

    @Test
    fun `should submit answers`() {
        val feedbackForm = createAndSaveFeedbackForm()

        val optionIdForFirstQuestion = feedbackForm.questions[0].possibleOptions.first().id
        val feedbackAnswerDtos = listOf(
            FeedbackAnswerRequestDto(
                questionId = feedbackForm.questions[0].id,
                selectedOptionId = optionIdForFirstQuestion,
                freeText = null
            ),
            FeedbackAnswerRequestDto(
                questionId = feedbackForm.questions[1].id,
                selectedOptionId = null,
                freeText = "Some text"
            )
        )

        val feedbackSubmissionRequestDto = FeedbackSubmissionRequestDto(
            version = 1,
            deduplicationKey = "unique-key-123",
            environment = EnvironmentDto(
                deviceModelName = "Test Device",
                operatingSystemName = "Android",
                operatingSystemVersion = "10",
                appVersion = "1.0",
                browserName = "Chrome",
                usedAs = "app",
                appSettings = """{ "userPreferences":""} """.trimIndent()
            ),
            answers = feedbackAnswerDtos
        )

        mockMvc.post("/v1/feedback-submissions") {
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(feedbackSubmissionRequestDto)
        }
            .andExpect { status { isOk() } }
    }

    @Test
    fun `should get feedback by id`() {
        val feedbackForm = createAndSaveFeedbackForm()
        val feedback = FeedbackSubmissionRequestDto(
            version = feedbackForm.version,
            deduplicationKey = "unique-key-123",
            environment = EnvironmentDto(
                deviceModelName = "Test Device",
                operatingSystemName = "Android",
                operatingSystemVersion = "10",
                appVersion = "1.0",
                browserName = "Chrome",
                usedAs = "app",
                appSettings = """{ "userPreferences":""} """.trimIndent()
            ),
            answers = listOf(
                FeedbackAnswerRequestDto(
                    feedbackForm.questions.first().id,
                    feedbackForm.questions.first().possibleOptions.first().id,
                    null
                ),
                FeedbackAnswerRequestDto(
                    feedbackForm.questions.last().id,
                    null,
                    "Great stuff"
                )
            )
        ).let {
            feedbackService.submitFeedback(it)
        }

        mockMvc.get("/v1/feedback-submissions/${feedback.id}")
            .andExpect { status { isOk() } }
    }

    @Test
    fun `should find all feedback`() {
        val feedbackForm = createAndSaveFeedbackForm()
        FeedbackSubmissionRequestDto(
            version = feedbackForm.version,
            deduplicationKey = "unique-key-123",
            environment = EnvironmentDto(
                deviceModelName = "Test Device",
                operatingSystemName = "Android",
                operatingSystemVersion = "10",
                appVersion = "1.0",
                browserName = "Chrome",
                usedAs = "app",
                appSettings = """{ "userPreferences":""} """.trimIndent()
            ),
            answers = listOf()
        ).let {
            feedbackService.submitFeedback(it)
        }

        mockMvc.get("/v1/feedback-submissions")
            .andExpect { status { isOk() } }
    }

    private fun createAndSaveFeedbackForm(): FeedbackForm {
        val feedbackForm = createFeedbackForm()

        val question1 = createFeedbackQuestion(feedbackForm, "What do you like most?", listOf("Option 1", "Option 2"))
        val question2 = createFeedbackQuestion(feedbackForm, "Any suggestions?", null)

        feedbackForm.questions.addAll(listOf(question1, question2))

        return feedbackFormRepository.save(feedbackForm)
    }

    private fun createFeedbackForm(): FeedbackForm {
        return FeedbackForm(
            id = UUID.randomUUID(),
            version = 1,
            questions = mutableListOf()
        )
    }

    private fun createFeedbackCreateDto(): FeedbackFormCreateDto {
        return FeedbackFormCreateDto(
            questions = listOf()
        )
    }

    private fun createFeedbackQuestion(
        feedbackForm: FeedbackForm,
        questionText: String,
        options: List<String>?
    ): FeedbackQuestion {
        val question = FeedbackQuestion(
            id = UUID.randomUUID(),
            questionType = QuestionType.SINGLE_CHOICE,
            questionName = listOf(Localization(questionText, Language.EN)),
            feedbackForm = feedbackForm
        )

        options?.forEach { optionText ->
            val feedbackOption = FeedbackOption(
                id = UUID.randomUUID(),
                question = question,
                localizedTexts = listOf(Localization(optionText, Language.EN))
            )
            question.possibleOptions.add(feedbackOption)
        }

        return question
    }
}
