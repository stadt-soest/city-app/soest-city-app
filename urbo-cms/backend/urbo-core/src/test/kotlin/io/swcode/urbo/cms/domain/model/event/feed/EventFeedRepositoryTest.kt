package io.swcode.urbo.cms.domain.model.event.feed

import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.swcode.urbo.cms.BaseTest
import io.swcode.urbo.cms.domain.model.event.Event
import io.swcode.urbo.cms.domain.model.event.EventRepository
import io.swcode.urbo.cms.domain.model.category.CategoryRepository
import io.swcode.urbo.cms.domain.model.category.CategoryType
import io.swcode.urbo.cms.domain.model.category.SourceCategory
import io.swcode.urbo.cms.domain.model.category.SourceCategoryRepository
import io.swcode.urbo.cms.testutil.TestDataProvider
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import testUtil.createEventCategory
import java.time.Duration
import java.time.Instant
import java.time.temporal.ChronoUnit

class EventFeedRepositoryTest : BaseTest() {

    @Autowired
    private lateinit var testDataProvider: TestDataProvider

    @Autowired
    private lateinit var eventFeedRepository: EventFeedRepository

    @Autowired
    private lateinit var eventRepository: EventRepository

    @Autowired
    private lateinit var categoryRepository: CategoryRepository

    @Autowired
    private lateinit var sourceCategoryRepository: SourceCategoryRepository

    private val defaultCreatedAt = Instant.parse("2024-03-01T10:00:00.00Z")
    private val defaultStartAt = Instant.parse("2024-04-01T10:00:00.00Z")
    private val defaultEndAt = Instant.parse("2024-04-01T15:00:00.00Z")
    private val defaultPageRequest = PageRequest.of(0, 10, Sort.Direction.ASC, "startAt")

    @Test
    fun `should find all grouped feeds and map`() {
        val event = createEvent(1)
        val criteria = EventGroupByStartAtCriteria(
            minStartAt = defaultStartAt.minus(365, ChronoUnit.DAYS)
        )

        eventFeedRepository.findAllGroupByStartAt(defaultPageRequest, criteria)
            .apply {
                shouldHaveSize(1)
                content[0].id.shouldBe(event.id)
                content[0].startAt.shouldBe(defaultStartAt)
                content[0].dateCreated.shouldBe(defaultCreatedAt)
                content[0].title.shouldBe(event.title)
                content[0].image.shouldBe(event.image)
            }
    }

    @Test
    fun `should find all grouped feeds ordered by startAt`() {
        createEvent(5)
        createEvent(1, Duration.ofDays(10))
        val criteria = EventGroupByStartAtCriteria(
            minStartAt = defaultStartAt.minus(365, ChronoUnit.DAYS)
        )

        eventFeedRepository.findAllGroupByStartAt(defaultPageRequest, criteria)
            .apply {
                shouldHaveSize(2)
                content[0].startAt.shouldBe(defaultStartAt)
                content[1].startAt.shouldBe(defaultStartAt.plus(10, ChronoUnit.DAYS))
            }
    }

    @Test
    fun `should find all grouped feeds by search term should be a wildcard match`() {
        createEvent(1).let {
            it.title = "Super Event"
            it.subTitle = ""
            it.description = ""
            eventRepository.saveAndFlush(it)
        }
        val criteria = EventGroupByStartAtCriteria(searchTerm = "Upe")

        eventFeedRepository.findAllGroupByStartAt(defaultPageRequest, criteria)
            .apply {
                shouldHaveSize(1)
            }
    }

    @Test
    fun `should find all grouped feeds by search term`() {
        val event1 = createEvent(1).let {
            it.title = "Kirmes"
            it.subTitle = "some subtitle"
            it.description = "some description"
            eventRepository.saveAndFlush(it)
        }
        val event2 = createEvent(1, Duration.ofDays(1)).let {
            it.title = "some Title"
            it.subTitle = "Kirmes subtitle"
            it.description = "some description"
            eventRepository.saveAndFlush(it)
        }
        val event3 = createEvent(1, Duration.ofDays(2)).let {
            it.title = "some Title"
            it.subTitle = "some subtitle"
            it.description = "Kirmes description"
            eventRepository.saveAndFlush(it)
        }
        createEvent(1).let {
            it.title = "other Title"
            it.subTitle = "other subtitle"
            it.description = "other description"
            eventRepository.saveAndFlush(it)
        }
        val criteria = EventGroupByStartAtCriteria(searchTerm = "kirmes")

        eventFeedRepository.findAllGroupByStartAt(defaultPageRequest, criteria)
            .apply {
                shouldHaveSize(3)
                content[0].id.shouldBe(event1.id)
                content[1].id.shouldBe(event2.id)
                content[2].id.shouldBe(event3.id)
            }
    }

    @Test
    fun `should find all grouped feeds ordered by startAt and filter past events`() {
        createEvent(5)
        createEvent(1, Duration.ofDays(10))
        val criteria = EventGroupByStartAtCriteria(
            minStartAt = defaultStartAt.plus(9, ChronoUnit.DAYS)
        )

        eventFeedRepository.findAllGroupByStartAt(defaultPageRequest, criteria)
            .apply {
                shouldHaveSize(1)
                content[0].startAt.shouldBe(defaultStartAt.plus(10, ChronoUnit.DAYS))
            }
    }

    @Test
    fun `should find all grouped and filter by single category`() {
        val event = createEvent(1)
        createEvent(1)

        val musicCategory = categoryRepository.saveAndFlush(createEventCategory("Music"))
        val musicSourceCategory =
            sourceCategoryRepository.saveAndFlush(SourceCategory("Musik", CategoryType.EVENTS, musicCategory))
        event.sourceCategories = mutableListOf(musicSourceCategory)
        eventRepository.saveAndFlush(event)

        val criteria = EventGroupByStartAtCriteria(
            minStartAt = defaultStartAt.minus(365, ChronoUnit.DAYS),
            categoryIds = listOf(musicCategory.id)
        )

        eventFeedRepository.findAllGroupByStartAt(defaultPageRequest, criteria).apply {
            this.shouldHaveSize(1)
            this.content[0].id.shouldBe(event.id)
        }
    }

    @Test
    fun `should find all grouped and filter by multiple categories`() {
        val musicEvent = createEvent(1)
        val artEvent = createEvent(1, Duration.ofDays(1))
        createEvent(1)

        val musicCategory = categoryRepository.saveAndFlush(createEventCategory("Music"))
        val musicSourceCategory =
            sourceCategoryRepository.saveAndFlush(SourceCategory("Musik", CategoryType.EVENTS, musicCategory))
        musicEvent.sourceCategories = mutableListOf(musicSourceCategory)
        eventRepository.saveAndFlush(musicEvent)

        val artCategory = categoryRepository.saveAndFlush(createEventCategory("Art"))
        val artSourceCategory =
            sourceCategoryRepository.saveAndFlush(SourceCategory("Kunst", CategoryType.EVENTS, artCategory))
        artEvent.sourceCategories = mutableListOf(artSourceCategory)
        eventRepository.saveAndFlush(artEvent)

        val criteria = EventGroupByStartAtCriteria(
            minStartAt = defaultStartAt.minus(365, ChronoUnit.DAYS),
            categoryIds = listOf(musicCategory.id, artCategory.id)
        )

        eventFeedRepository.findAllGroupByStartAt(defaultPageRequest, criteria).apply {
            this.shouldHaveSize(2)
            this.content[0].id.shouldBe(musicEvent.id)
            this.content[1].id.shouldBe(artEvent.id)
        }
    }

    @Test
    fun `should find all grouped and filter by single source`() {
        val event = createEvent(1, sourceName = "Soest")
        createEvent(1, sourceName = "Anzeiger")

        val criteria = EventGroupByStartAtCriteria(
            minStartAt = defaultStartAt.minus(365, ChronoUnit.DAYS),
            sourceNames = listOf("Soest")
        )

        eventFeedRepository.findAllGroupByStartAt(defaultPageRequest, criteria).apply {
            this.shouldHaveSize(1)
            this.content[0].id.shouldBe(event.id)
        }
    }

    @Test
    fun `should find all grouped and filter by multiple sources`() {
        val event = createEvent(1, sourceName = "Soest")
        val event2 = createEvent(1, delta = Duration.ofDays(1), sourceName = "Anzeiger")
        createEvent(1, sourceName = "Other")

        val criteria = EventGroupByStartAtCriteria(
            minStartAt = defaultStartAt.minus(365, ChronoUnit.DAYS),
            sourceNames = listOf("Soest", "Anzeiger")
        )

        eventFeedRepository.findAllGroupByStartAt(defaultPageRequest, criteria).apply {
            this.shouldHaveSize(2)
            this.content[0].id.shouldBe(event.id)
            this.content[1].id.shouldBe(event2.id)
        }
    }

    @Test
    fun `should find all grouped feeds paginated`() {
        0.rangeTo(10).forEach {
            createEvent(1, Duration.ofDays(it.toLong()))
        }

        val criteria = EventGroupByStartAtCriteria(
            minStartAt = defaultStartAt.minus(365, ChronoUnit.DAYS)
        )

        eventFeedRepository.findAllGroupByStartAt(defaultPageRequest, criteria).apply {
            this.shouldHaveSize(10)
            this.content[0].startAt.shouldBe(defaultStartAt)
            this.content[9].startAt.shouldBe(defaultStartAt.plus(9, ChronoUnit.DAYS))
            this.pageable.pageNumber.shouldBe(0)
            this.pageable.pageSize.shouldBe(10)
            this.totalPages.shouldBe(2)
            this.totalElements.shouldBe(11)
        }

        eventFeedRepository.findAllGroupByStartAt(
            defaultPageRequest.withPage(1),
            criteria
        ).apply {
            this.shouldHaveSize(1)
            this.content[0].startAt.shouldBe(defaultStartAt.plus(10, ChronoUnit.DAYS))
        }
    }

    @Test
    fun `should find all grouped feeds order by startAt ascending`() {
        0.rangeTo(4).forEach {
            createEvent(1, Duration.ofDays(it.toLong()))
        }

        val criteria = EventGroupByStartAtCriteria(
            minStartAt = defaultStartAt.minus(365, ChronoUnit.DAYS)
        )

        eventFeedRepository.findAllGroupByStartAt(
            defaultPageRequest.withSort(Sort.by(Sort.Direction.DESC, "startAt")),
            criteria
        ).apply {
            this.shouldHaveSize(5)
            this.content[0].startAt.shouldBe(defaultStartAt.plus(4, ChronoUnit.DAYS))
        }
    }

    @Test
    fun `should find all grouped feeds without minStartFilter`() {
        createEvent(5)
        createEvent(1, Duration.ofDays(10))

        val criteria = EventGroupByStartAtCriteria()

        eventFeedRepository.findAllGroupByStartAt(defaultPageRequest, criteria).apply {
            shouldHaveSize(2)
            content[0].startAt.shouldBe(defaultStartAt)
            content[1].startAt.shouldBe(defaultStartAt.plus(10, ChronoUnit.DAYS))
        }
    }

    @Test
    fun `should find all grouped and filter excluding event`() {
        val event = createEvent(1, sourceName = "Soest")
        val event2 = createEvent(1, delta = Duration.ofDays(1), sourceName = "Anzeiger")
        createEvent(1, sourceName = "Other")

        val criteria = EventGroupByStartAtCriteria(
            minStartAt = defaultStartAt.minus(365, ChronoUnit.DAYS),
            sourceNames = listOf("Soest", "Anzeiger"),
            excludedEventIds = listOf(event2.id)
        )

        eventFeedRepository.findAllGroupByStartAt(defaultPageRequest, criteria).apply {
            this.shouldHaveSize(1)
            this.content[0].id.shouldBe(event.id)
        }
    }

    @Test
    fun `should find all feeds`() {
        createEvent(5)
        createEvent(1, Duration.ofDays(10))

        val criteria = EventFilterCriteria()

        eventFeedRepository.findAll(defaultPageRequest, criteria).apply {
            shouldHaveSize(6)
            content[0].startAt.shouldBe(defaultStartAt)
            content[5].startAt.shouldBe(defaultStartAt.plus(10, ChronoUnit.DAYS))
        }
    }

    @Test
    fun `should find all and filter by sources`() {
        val event = createEvent(1, sourceName = "Soest")
        val event2 = createEvent(1, delta = Duration.ofDays(1), sourceName = "Anzeiger")
        createEvent(1, sourceName = "Other")

        val criteria = EventFilterCriteria(
            sourceNames = listOf("Soest", "Anzeiger")
        )

        eventFeedRepository.findAll(defaultPageRequest, criteria).apply {
            this.shouldHaveSize(2)
            this.content[0].id.shouldBe(event.id)
            this.content[1].id.shouldBe(event2.id)
        }
    }

    @Test
    fun `should find all feeds and filter by minStartAt`() {
        createEvent(5)
        createEvent(1, Duration.ofDays(10))

        val criteria = EventFilterCriteria(
            minStartAt = defaultStartAt.plus(9, ChronoUnit.DAYS)
        )

        eventFeedRepository.findAll(defaultPageRequest, criteria)
            .apply {
                shouldHaveSize(1)
                content[0].startAt.shouldBe(defaultStartAt.plus(10, ChronoUnit.DAYS))
            }
    }

    @Test
    fun `should find all feeds and filter by maxEndAt`() {
        val event = createEvent(5)
        createEvent(1, Duration.ofDays(10))

        val criteria = EventFilterCriteria(
            maxEndAt = defaultStartAt.plus(5, ChronoUnit.DAYS)
        )

        eventFeedRepository.findAll(defaultPageRequest, criteria)
            .apply {
                shouldHaveSize(5)
                content.forEach {
                    it.id.shouldBe(event.id)
                }
            }
    }

    @Test
    fun `should find all and filter by categories`() {
        val musicEvent = createEvent(1)
        val artEvent = createEvent(1, Duration.ofDays(1))
        createEvent(1)

        val musicCategory = categoryRepository.saveAndFlush(createEventCategory("Music"))
        val musicSourceCategory =
            sourceCategoryRepository.saveAndFlush(SourceCategory("Musik", CategoryType.EVENTS, musicCategory))
        musicEvent.sourceCategories = mutableListOf(musicSourceCategory)
        eventRepository.saveAndFlush(musicEvent)

        val artCategory = categoryRepository.saveAndFlush(createEventCategory("Art"))
        val artSourceCategory =
            sourceCategoryRepository.saveAndFlush(SourceCategory("Kunst", CategoryType.EVENTS, artCategory))
        artEvent.sourceCategories = mutableListOf(artSourceCategory)
        eventRepository.saveAndFlush(artEvent)

        val criteria = EventFilterCriteria(
            categoryIds = listOf(musicCategory.id, artCategory.id)
        )

        eventFeedRepository.findAll(defaultPageRequest, criteria).apply {
            this.shouldHaveSize(2)
            this.content[0].id.shouldBe(musicEvent.id)
            this.content[1].id.shouldBe(artEvent.id)
        }
    }

    @Test
    fun `should find all feeds and filter excluding event`() {
        val event = createEvent(1)
        val event2 = createEvent(1)

        val criteria = EventFilterCriteria(
            excludedEventIds = listOf(event2.id)
        )

        eventFeedRepository.findAll(defaultPageRequest, criteria)
            .apply {
                shouldHaveSize(1)
                content[0].id.shouldBe(event.id)
            }
    }

    private fun createEvent(
        occurrenceCount: Int,
        delta: Duration = Duration.ZERO,
        sourceName: String = "Soest"
    ): Event {
        return testDataProvider.createEvent(
            occurrenceCount,
            defaultStartAt.plus(delta),
            defaultEndAt.plus(delta),
            defaultCreatedAt.plus(delta),
            sourceName
        )
    }
}
