package io.swcode.urbo.cms.application.news.assembler

import io.kotest.inspectors.forAtLeastOne
import io.kotest.matchers.shouldBe
import io.swcode.urbo.cms.application.news.dto.NewsResponseDto
import io.swcode.urbo.cms.application.shared.image.ImageDto
import io.swcode.urbo.cms.application.shared.image.ImageFileDto
import io.swcode.urbo.cms.domain.model.category.CategoryType
import io.swcode.urbo.cms.domain.model.category.SourceCategory
import io.swcode.urbo.cms.domain.model.shared.Image
import io.swcode.urbo.cms.domain.model.shared.ImageFile
import io.swcode.urbo.common.date.toBerlinInstant
import org.junit.jupiter.api.Test
import testUtil.createNewsCategory
import testUtil.createNewsEntity
import java.time.LocalDateTime

class NewsAssemblerTest {

    @Test
    fun `should map from News entity to NewsResponseDto`() {
        val newsCategory = createNewsCategory()
        val newsResponseDto = NewsResponseDto.fromEntity(createNewsEntity {
            sourceCategories = mutableListOf(SourceCategory("Politik", CategoryType.NEWS, newsCategory))
            image = Image(
                source = "source",
                thumbnail = ImageFile("thumbnail", 1, 1),
                highRes = ImageFile("highRes", 1, 1)
            )
        })

        newsResponseDto.also {
            it.id shouldBe newsResponseDto.id
            it.content shouldBe "content"
            it.link shouldBe "link"
            it.source shouldBe "soester-anzeiger"
            it.date shouldBe LocalDateTime.of(2000, 12, 8, 10, 10, 10).toBerlinInstant()
            it.modified shouldBe LocalDateTime.of(2000, 12, 8, 10, 10, 10).toBerlinInstant()
            it.authors.forAtLeastOne { author ->
                author shouldBe "author"
            }
            it.categories.forAtLeastOne { category ->
                category.sourceCategory shouldBe "Politik"
            }
            it.image.shouldBe(ImageDto("source", ImageFileDto("thumbnail", 1, 1), ImageFileDto("highRes", 1, 1)))
        }
    }

    @Test
    fun `should handle null image and empty categories`() {
        val newsWithNullImageAndEmptyCategories = createNewsEntity().apply {
            image = null
            sourceCategories.clear()
        }

        val newsResponseDto = NewsResponseDto.fromEntity(newsWithNullImageAndEmptyCategories)

        newsResponseDto.id shouldBe newsWithNullImageAndEmptyCategories.id
        newsResponseDto.image shouldBe null
        newsResponseDto.categories shouldBe emptyList()
    }

    @Test
    fun `should handle null thumbnail and highRes in image`() {
        val newsWithNullImageFields = createNewsEntity().apply {
            image = Image(source = "source").apply {
                thumbnail = null
                highRes = null
            }
        }

        val newsResponseDto = NewsResponseDto.fromEntity(newsWithNullImageFields)

        newsResponseDto.id shouldBe newsWithNullImageFields.id
        newsResponseDto.image?.thumbnail shouldBe null
        newsResponseDto.image?.highRes shouldBe null
    }
}
