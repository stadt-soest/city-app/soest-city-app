package io.swcode.urbo.cms

import io.swcode.urbo.user.management.TestUserContextService
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import com.ninjasquad.springmockk.MockkBean
import io.mockk.mockk
import io.swcode.urbo.cms.application.citizenService.CitizenAdapter
import io.swcode.urbo.cms.application.event.EventAdapter
import io.swcode.urbo.cms.application.news.NewsAdapter
import io.swcode.urbo.cms.application.parking.ParkingAdapter
import io.swcode.urbo.cms.application.poi.PoiAdapter
import io.swcode.urbo.cms.application.status.NodeRedAdapter
import io.swcode.urbo.cms.application.waste.WasteAdapter
import io.swcode.urbo.cms.s3.api.S3Adapter
import io.swcode.urbo.cms.search.meili.api.SearchIndexerAdapter
import io.swcode.urbo.cms.testutil.TestDataProvider
import io.swcode.urbo.command.api.CommandBus
import io.swcode.urbo.command.api.CommandRegistry
import io.swcode.urbo.core.api.auth.AuthUserAdapter
import io.swcode.urbo.core.api.image.ImageProcessingAdapter
import io.swcode.urbo.user.management.domain.user.userContext.UserContextService
import io.swcode.urbo.web.UrboWebConfig
import org.springframework.boot.SpringBootConfiguration
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.Primary
import stub.*

@SpringBootConfiguration
@EnableAutoConfiguration
@Import(CoreConfiguration::class, TestDataProvider::class, UrboWebConfig::class)
class TestConfig {

    @Bean
    @Primary
    fun testUserContextService(): UserContextService {
        return TestUserContextService()
    }

    @Bean
    fun commandRegistry(): CommandRegistry {
        return mockk(relaxed = true)
    }

    @Bean
    fun commandBus(): CommandBus {
        return CommandBusStub()
    }

    @MockkBean
    private lateinit var authUserAdapter: AuthUserAdapter

    @Bean
    fun nodeRedAdapter(): NodeRedAdapter = mockk()

    @Bean
    fun citizenAdapter(): CitizenAdapter = mockk()

    @Bean
    fun eventAdapter(): EventAdapter = mockk()

    @Bean
    fun newsAdapter(): NewsAdapter = mockk()

    @Bean
    fun wasteAdapter(): WasteAdapter = mockk()

    @Bean
    fun parkingAdapter(): ParkingAdapter = mockk()

    @Bean
    fun poiAdapter(): PoiAdapter = mockk()

    @Bean
    fun searchIndexerAdapter(): SearchIndexerAdapter = mockk()

    @Bean
    fun imageProcessingAdapter(): ImageProcessingAdapter = mockk()

    @Bean
    fun s3StorageAdapter(): S3Adapter {
        return S3StorageAdapterStub()
    }

    @Bean(initMethod = "start", destroyMethod = "stop")
    fun wireMockServer(): WireMockServer {
        return WireMockServer(WireMockConfiguration.wireMockConfig().dynamicPort())
    }
}


