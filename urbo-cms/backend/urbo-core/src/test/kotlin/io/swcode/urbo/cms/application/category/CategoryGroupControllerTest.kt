package io.swcode.urbo.cms.application.category

import io.swcode.urbo.cms.WithUserContext
import io.swcode.urbo.cms.BaseControllerTest
import io.swcode.urbo.cms.application.CorePermission
import io.swcode.urbo.cms.application.shared.image.localization.LocalizationDto
import io.swcode.urbo.cms.domain.model.category.CategoryType
import io.swcode.urbo.cms.domain.model.category.group.CategoryGroupService
import io.swcode.urbo.cms.domain.model.shared.Language
import org.hamcrest.Matchers.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.BeforeEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.*

@WithUserContext(permissions = [CorePermission.CATEGORY_DELETE, CorePermission.CATEGORY_WRITE, CorePermission.CATEGORY_READ])
class CategoryGroupControllerTest : BaseControllerTest() {

    @Autowired
    lateinit var categoryGroupService: CategoryGroupService

    lateinit var categoryGroupCreateDto: CategoryGroupCreateDto

    @BeforeEach
    fun setup() {
        categoryGroupCreateDto = CategoryGroupCreateDto(
            CategoryType.POIS,
            listOf(
                LocalizationDto(
                    "English", Language.EN
                )
            )
        )
    }

    @Test
    fun getCategoryGroups() {
        categoryGroupService.createCategoryGroup(categoryGroupCreateDto)

        mockMvc
            .get("/v1/category-groups") {
                accept = MediaType.APPLICATION_JSON
                param("type", "POIS")
            }
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.size()", `is`(1))
                jsonPath("$[0].id", notNullValue())
                jsonPath("$[0].localizedNames.size()", `is`(1))
                jsonPath(
                    "$[0].localizedNames", containsInAnyOrder(
                        mapOf("language" to "EN", "value" to "English"),
                    )
                )
                jsonPath("$[0].type", `is`(CategoryType.POIS.name))
            }

    }

    @Test
    @WithMockUser
    fun createCategoryGroup() {
        mockMvc
            .post("/v1/category-groups") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(categoryGroupCreateDto)
                accept = MediaType.APPLICATION_JSON
            }
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.id", notNullValue())
                jsonPath("$.localizedNames.size()", `is`(1))
                jsonPath(
                    "$.localizedNames", containsInAnyOrder(
                        mapOf("language" to "EN", "value" to "English"),
                    )
                )
                jsonPath("$.type", `is`(CategoryType.POIS.name))
            }
    }

    @Test
    fun deleteCategoryGroup() {
        val categoryGroup = categoryGroupService.createCategoryGroup(categoryGroupCreateDto)

        mockMvc
            .delete("/v1/category-groups/${categoryGroup.id}") {
                accept = MediaType.APPLICATION_JSON
            }
            .andExpect { status { isNoContent() } }
    }

    @Test
    fun updateCategoryGroup() {
        val category = categoryGroupService.createCategoryGroup(categoryGroupCreateDto)

        val updateCreateDto = CategoryGroupCreateDto(
            CategoryType.POIS,
            listOf(
                LocalizationDto(
                    "German", Language.DE
                )
            )
        )

        mockMvc
            .put("/v1/category-groups/${category.id}") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(updateCreateDto)
                accept = MediaType.APPLICATION_JSON
            }
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.id", notNullValue())
                jsonPath("$.localizedNames.size()", `is`(1))
                jsonPath(
                    "$.localizedNames", containsInAnyOrder(
                        mapOf("language" to "DE", "value" to "German"),
                    )
                )
                jsonPath("$.type", `is`(CategoryType.POIS.name))
            }
    }

    @Test
    fun `should update category group order`() {
        val categoryGroup1 = categoryGroupService.createCategoryGroup(categoryGroupCreateDto)
        val categoryGroup2 = categoryGroupService.createCategoryGroup(
            CategoryGroupCreateDto(
                CategoryType.POIS,
                listOf(LocalizationDto("German", Language.DE))
            )
        )

        val categoryGroups = listOf(
            CategoryGroupUpdateOrderDto(id = categoryGroup1.id, order = 1),
            CategoryGroupUpdateOrderDto(id = categoryGroup2.id, order = 0)
        )

        mockMvc
            .patch("/v1/category-groups/order") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(categoryGroups)
                accept = MediaType.APPLICATION_JSON
            }
            .andExpect { status { isNoContent() } }
    }
}