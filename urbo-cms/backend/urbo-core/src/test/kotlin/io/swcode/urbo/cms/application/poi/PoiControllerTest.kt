package io.swcode.urbo.cms.application.poi

import io.swcode.urbo.cms.BaseControllerTest
import io.swcode.urbo.cms.domain.model.category.Category
import io.swcode.urbo.cms.domain.model.category.CategoryType
import io.swcode.urbo.cms.domain.model.category.SourceCategory
import io.swcode.urbo.cms.domain.model.poi.PoiService
import io.swcode.urbo.cms.domain.model.shared.Language
import io.swcode.urbo.cms.domain.model.shared.Localization
import org.hamcrest.CoreMatchers
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import testUtil.createNewPoi
import java.util.*

class PoiControllerTest : BaseControllerTest() {
    @Autowired
    private lateinit var poiService: PoiService

    @Test
    fun `should get poi by id`() {
        val poi = poiService.save(createNewPoi())

        mockMvc.perform(get("/v1/pois/{id}", poi.id))
            .andExpect(status().isOk)
    }

    @Test
    fun `should find pois`() {
        poiService.save(createNewPoi())

        mockMvc.perform(get("/v1/pois"))
            .andExpect(status().isOk)
            .andExpect { jsonPath("$.content.size()", CoreMatchers.`is`(1)) }
    }

    @Test
    fun `should find pois filtered by categoryIds`() {
        val category1 = UUID.randomUUID()
        val category2 = UUID.randomUUID()
        val poi1 = createNewPoi {
            sourceCategories = listOf(
                SourceCategory(
                    "Category1", CategoryType.POIS, Category(
                        category1, listOf(
                            Localization("Category 1", Language.EN)
                        ), listOf(), CategoryType.NEWS, null, null
                    )
                )
            )
        }
        val poi2 = createNewPoi {
            sourceCategories = listOf(
                SourceCategory(
                    "Category2",
                    CategoryType.POIS,
                    Category(
                        category2,
                        listOf(Localization("Category 2", Language.EN)),
                        listOf(),
                        CategoryType.POIS,
                        null,
                        null,
                    )
                )
            )
        }

        poiService.save(poi1)
        poiService.save(poi2)

        mockMvc.perform(
            get("/v1/pois")
                .param("categoryIds", category1.toString())
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.content.size()", CoreMatchers.`is`(1)))
            .andExpect(jsonPath("$.content[0].id", CoreMatchers.`is`(poi1.id.toString())))
    }
}