package io.swcode.urbo.cms.domain.model.parking

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.swcode.urbo.cms.BaseTest
import io.swcode.urbo.cms.application.parking.request.ParkingSpecialOpeningHoursDto
import io.swcode.urbo.cms.application.parking.response.ParkingSpecialOpeningHoursResponseDto
import io.swcode.urbo.cms.domain.model.shared.Language
import io.swcode.urbo.cms.domain.model.shared.Localization
import io.swcode.urbo.common.error.ErrorType
import io.swcode.urbo.common.error.UrboException
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.transaction.annotation.Transactional
import testUtil.mockParkingArea
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalTime
import java.util.*

class ParkingServiceTest : BaseTest() {

    @Autowired
    private lateinit var parkingService: ParkingService

    @Autowired
    private lateinit var parkingAreaRepository: ParkingAreaRepository

    @BeforeEach
    fun setup() {
        parkingAreaRepository.deleteAll()
    }

    @Test
    @Transactional
    fun save() {
        val initialParkingArea = parkingAreaRepository.save(mockParkingArea())

        parkingAreaRepository.findByExternalId(initialParkingArea.externalId) should {
            it!!.id shouldBe initialParkingArea.id
            it.externalId shouldBe initialParkingArea.externalId
            it.name shouldBe initialParkingArea.name
            it.location shouldBe initialParkingArea.location
            it.capacity shouldBe initialParkingArea.capacity
            it.openingTimes shouldBe initialParkingArea.openingTimes
            it.description shouldBe initialParkingArea.description
            it.priceDescription shouldBe initialParkingArea.priceDescription
        }

        val updateParkingArea = mockParkingArea().copy(
            name = "new name", priceDescription = mutableListOf(
                Localization(
                    value = "test2",
                    language = Language.DE
                )
            ), description = mutableListOf(
                Localization(
                    value = "test2",
                    language = Language.DE
                )
            )
        )

        parkingService.save(updateParkingArea)

        parkingAreaRepository.findByExternalId(initialParkingArea.externalId) should {
            it!!.name shouldBe "new name"
        }
    }


    @Test
    fun `getAllParkingAreas should return paginated parking areas`() {
        repeat(3) {
            parkingService.save(mockParkingArea().copy(UUID.randomUUID(), UUID.randomUUID().toString()))
        }

        val pageable = PageRequest.of(0, 2)

        val result = parkingService.getAllParkingAreas(pageable)

        result.content.size shouldBe 2
        result.totalElements shouldBe 3
        result.number shouldBe 0
        result.size shouldBe 2
    }

    @Test
    @Transactional
    fun `getParkingArea should return a parking area for valid ID`() {
        val parkingArea = mockParkingArea()
        parkingService.save(parkingArea)

        val result = parkingService.getParkingArea(parkingArea.id)

        result.id shouldBe parkingArea.id
        result.name shouldBe parkingArea.name
        result.location shouldBe parkingArea.location
        result.capacity shouldBe parkingArea.capacity
        result.openingTimes shouldBe parkingArea.openingTimes
    }

    @Test
    fun `getParkingArea should throw EntityNotFoundException for invalid ID`() {
        val invalidId = UUID.randomUUID()

        val exception = shouldThrow<UrboException> {
            parkingService.getParkingArea(invalidId)
        }

        exception.message shouldBe "ParkingArea not found with id: $invalidId"
    }

    @Test
    fun `updateOpeningTimes should update opening times for valid ID`() {
        val parkingArea = mockParkingArea()
        parkingService.save(parkingArea)

        val updatedOpeningTimes = mapOf(
            DayOfWeek.MONDAY to ParkingOpeningHours(LocalTime.of(8, 0), LocalTime.of(18, 0)),
            DayOfWeek.TUESDAY to ParkingOpeningHours(LocalTime.of(8, 0), LocalTime.of(18, 0))
        )

        val result = parkingService.updateOpeningTimes(parkingArea.id, updatedOpeningTimes)

        result.id shouldBe parkingArea.id
        result.openingTimes shouldBe updatedOpeningTimes
    }

    @Test
    fun `updateOpeningTimes should throw UrboException for invalid ID`() {
        val invalidId = UUID.randomUUID()
        val updatedOpeningTimes = mapOf(
            DayOfWeek.MONDAY to ParkingOpeningHours(LocalTime.of(8, 0), LocalTime.of(18, 0))
        )

        val exception = shouldThrow<UrboException> {
            parkingService.updateOpeningTimes(invalidId, updatedOpeningTimes)
        }

        exception.message shouldBe "ParkingArea not found with id: $invalidId"
        exception.errorType shouldBe ErrorType.RESOURCE_NOT_FOUND
    }

    @Test
    fun `updateSpecialOpeningTimes should update special opening times for valid ID`() {
        val parkingArea = mockParkingArea()
        parkingService.save(parkingArea)

        val updatedSpecialOpeningTimes = listOf(
            ParkingSpecialOpeningHoursDto(
                from = LocalTime.of(10, 0),
                to = LocalTime.of(10, 0),
                date = LocalDate.of(10, 1, 1),
                descriptions = listOf()
            )
        )

        val result = parkingService.updateSpecialOpeningTimes(parkingArea.id, updatedSpecialOpeningTimes)

        result.id shouldBe parkingArea.id
        result.specialOpeningTimes.size shouldBe updatedSpecialOpeningTimes.size
        result.specialOpeningTimes[0].should {
            it.from shouldBe updatedSpecialOpeningTimes[0].from
            it.to shouldBe updatedSpecialOpeningTimes[0].to
            it.date shouldBe updatedSpecialOpeningTimes[0].date
        }
    }

    @Test
    fun `updateSpecialOpeningTimes should throw UrboException for invalid ID`() {
        val invalidId = UUID.randomUUID()
        val updatedSpecialOpeningTimes = listOf(
            ParkingSpecialOpeningHoursDto(
                from = LocalTime.of(10, 0),
                to = LocalTime.of(10, 0),
                date = LocalDate.of(10, 1, 1),
                descriptions = listOf()
            )
        )

        val exception = shouldThrow<UrboException> {
            parkingService.updateSpecialOpeningTimes(invalidId, updatedSpecialOpeningTimes)
        }

        exception.message shouldBe "ParkingArea not found with id: $invalidId"
        exception.errorType shouldBe ErrorType.RESOURCE_NOT_FOUND
    }
}
