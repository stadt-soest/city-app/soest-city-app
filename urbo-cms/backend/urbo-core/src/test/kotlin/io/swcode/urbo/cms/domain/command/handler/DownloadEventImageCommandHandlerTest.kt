package io.swcode.urbo.cms.domain.command.handler

import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.swcode.urbo.cms.domain.command.DownloadEventImageCommand
import io.swcode.urbo.cms.domain.model.event.Event
import io.swcode.urbo.cms.domain.model.event.EventRepository
import io.swcode.urbo.cms.domain.model.shared.Image
import io.swcode.urbo.cms.domain.model.shared.ImageFile
import io.swcode.urbo.cms.search.index.SearchIndexService
import io.swcode.urbo.core.api.image.ImageProcessingAdapter
import io.swcode.urbo.core.api.image.ImageProcessingFileDto
import io.swcode.urbo.core.api.image.ImageProcessingResult
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.data.repository.findByIdOrNull
import testUtil.createEventEntity

class DownloadEventImageCommandHandlerTest {
    private lateinit var handler: DownloadEventImageCommandHandler
    private lateinit var eventRepository: EventRepository
    private lateinit var searchIndexService: SearchIndexService
    private lateinit var imageProcessingAdapter: ImageProcessingAdapter

    @BeforeEach
    fun setup() {
        eventRepository = mockk(relaxed = true)
        searchIndexService = mockk(relaxed = true)
        imageProcessingAdapter = mockk(relaxed = true)

        handler = DownloadEventImageCommandHandler(
            eventRepository,
            searchIndexService,
            imageProcessingAdapter,
        )
    }

    @Test
    fun `should handle download event image command and update the event entity`() {
        val event = createEventEntity()
        val command = DownloadEventImageCommand(event.id, Image("testSource"))

        val image = Image("testSource")
        val downloadedImage = ImageProcessingResult(
            "downloaded", ImageProcessingFileDto("file1", 100, 100),
            ImageProcessingFileDto("file2", 200, 200)
        )
        val expectedImage = Image("testSource", ImageFile("file1", 100, 100), ImageFile("file2", 200, 200))

        every { eventRepository.findByIdOrNull(event.id) } returns event
        every { imageProcessingAdapter.downloadAndProcessImage(image.source) } returns downloadedImage
        every { eventRepository.save(any<Event>()) } returnsArgument 0

        handler.handle(command)

        verify { eventRepository.save(event) }

        event.image shouldBe expectedImage
    }
}
