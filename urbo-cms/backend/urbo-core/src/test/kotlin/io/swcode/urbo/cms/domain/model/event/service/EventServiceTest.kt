package io.swcode.urbo.cms.domain.model.event.service

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.equals.shouldBeEqual
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import io.swcode.urbo.cms.BaseTest
import io.swcode.urbo.cms.domain.command.DeleteImageCommand
import io.swcode.urbo.cms.domain.command.DownloadEventImageCommand
import io.swcode.urbo.cms.domain.model.event.EventOccurrence
import io.swcode.urbo.cms.domain.model.event.EventRepository
import io.swcode.urbo.cms.domain.model.event.EventService
import io.swcode.urbo.cms.domain.model.event.EventStatus
import io.swcode.urbo.cms.domain.model.shared.Image
import io.swcode.urbo.cms.domain.model.shared.ImageFile
import io.swcode.urbo.common.error.UrboException
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import stub.CommandBusStub
import testUtil.createEventEntity
import java.time.Instant
import java.util.*

class EventServiceTest : BaseTest() {

    @Autowired
    private lateinit var commandBus: CommandBusStub

    @Autowired
    private lateinit var eventService: EventService

    @Autowired
    private lateinit var eventRepository: EventRepository

    @BeforeEach
    fun setup() {
        eventRepository.deleteAll()
        commandBus.clearCommands()
    }

    @Test
    fun `should throw UrboException when trying to get nonexistent event`() {
        val nonExistentId = UUID.randomUUID()

        shouldThrow<UrboException> {
            eventService.getEvent(nonExistentId)
        }.also {
            it.message shouldBe "Event not found with id: $nonExistentId"
        }
    }

    @Test
    fun `should update Event`() {

        val initialEvent = eventService.save(createEventEntity())

        val updatedOccurence = initialEvent.occurrences.elementAt(0).apply {
            this.startAt = Instant.now()
            this.endAt = Instant.now()
        }

        eventService.save(createEventEntity().copy(occurrences = mutableListOf(updatedOccurence)))

        eventRepository.findByIdOrNull(initialEvent.id)!!.should {
            it.occurrences.size shouldBe 1
            it.occurrences.elementAt(0).externalId shouldBe initialEvent.occurrences.elementAt(0).externalId
            it.occurrences.elementAt(0).startAt shouldBe updatedOccurence.startAt
            it.occurrences.elementAt(0).endAt shouldBe updatedOccurence.endAt
        }
    }

    @Test
    fun `should delete old image and download new image when image URL is updated`() {
        val initialEvent = eventService.save(
            createEventEntity().apply {
                this.image = Image("http://localstack:4566/old.jpg", ImageFile("old.jpg"))
            }
        )

        val updateEvent = createEventEntity().copy(
            occurrences = initialEvent.occurrences,
        ).apply {
            this.image = Image("http://localhost:4566/new.jpg")
        }

        val updatedEvent = eventService.save(updateEvent)

        eventRepository.findByIdOrNull(initialEvent.id)?.shouldBeEqual(updatedEvent)

        commandBus.getCommands().shouldContain(DeleteImageCommand(initialEvent.image!!))
        commandBus.getCommands()
            .shouldContain(DownloadEventImageCommand(eventId = initialEvent.id, image = updateEvent.image!!))
    }

    @Test
    fun `should submit DownloadEntityImageCommand when event is saved`() {
        val event = createEventEntity().copy(image = Image("http://localhost:4566/image.jpg"))

        eventService.save(event)

        val commands = commandBus.getCommands()

        val downloadCommand = commands.filterIsInstance<DownloadEventImageCommand>().firstOrNull()

        downloadCommand shouldNotBe null
        downloadCommand?.image?.source shouldBe event.image?.source
    }

    @Test
    fun `should delete event if all occurrences are deleted`() {
        val event1 = createEventEntity()

        eventService.save(event1)

        eventService.deleteOccurrence(event1.occurrences[0].externalId)

        eventRepository.findByIdOrNull(event1.id) shouldBe null
    }

    @Test
    fun `should delete only the occurrence by occurrence ID`() {
        val event = createEventEntity().apply {
            occurrences.add(
                EventOccurrence(
                    id = UUID.randomUUID(),
                    event = this,
                    externalId = "occurrence2",
                    startAt = Instant.now(),
                    endAt = Instant.now(),
                    dateCreated = Instant.now(),
                    source = "source",
                    status = EventStatus.SCHEDULED,
                )
            )
        }

        eventService.save(event)

        eventService.deleteOccurrence("occurrence2")

        val updatedEvent = eventRepository.findByIdOrNull(event.id)
        updatedEvent!!.occurrences.size shouldBe 1
    }
}
