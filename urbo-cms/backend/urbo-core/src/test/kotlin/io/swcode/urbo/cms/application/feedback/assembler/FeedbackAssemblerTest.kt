package io.swcode.urbo.cms.application.feedback.assembler

import io.kotest.matchers.shouldBe
import io.swcode.urbo.cms.application.feedback.dto.EnvironmentDto
import io.swcode.urbo.cms.application.feedback.dto.FeedbackSubmissionRequestDto
import io.swcode.urbo.cms.domain.model.feedback.FeedbackForm
import io.swcode.urbo.cms.domain.model.feedback.question.FeedbackQuestion
import io.swcode.urbo.cms.domain.model.feedback.question.QuestionType
import io.swcode.urbo.cms.domain.model.shared.Language
import io.swcode.urbo.cms.domain.model.shared.Localization
import org.junit.jupiter.api.Test
import java.util.*

class FeedbackAssemblerTest {

    @Test
    fun `should correctly map FeedbackForm to FeedbackFormDto`() {

        val feedbackFormId = UUID.randomUUID()
        val questionId = UUID.randomUUID()
        val feedbackForm = FeedbackForm(
            id = feedbackFormId,
            version = 1,
            questions = mutableListOf(
                FeedbackQuestion(
                    id = questionId,
                    questionType = QuestionType.SINGLE_CHOICE,
                    questionName = listOf(Localization("What do you like most?", Language.EN)),
                    feedbackForm = FeedbackForm(feedbackFormId, 1, mutableListOf())
                )
            )
        )

        val dto = FeedbackAssembler.mapToDto(feedbackForm)

        dto.id shouldBe feedbackFormId
        dto.version shouldBe 1
        dto.questions.size shouldBe 1
        val questionDto = dto.questions.first()
        questionDto.id shouldBe questionId
        questionDto.questionType shouldBe "SINGLE_CHOICE"
        questionDto.questionName.size shouldBe 1
        questionDto.questionName.first().value shouldBe "What do you like most?"
        questionDto.questionName.first().language shouldBe Language.EN
    }

    @Test
    fun `should correctly map FeedbackSubmissionDto to FeedbackResponse`() {
        val feedbackForm = FeedbackForm(
            id = UUID.randomUUID(),
            version = 1,
            questions = mutableListOf()
        )
        val submissionDto = FeedbackSubmissionRequestDto(
            version = 1,
            deduplicationKey = "unique-key",
            environment = EnvironmentDto(
                deviceModelName = "Test Device",
                operatingSystemName = "Android",
                operatingSystemVersion = "10",
                appVersion = "1.0",
                browserName = "Chrome",
                usedAs = "app",
                appSettings = """{ "userPreferences":""} """.trimIndent()
            ),
            answers = listOf()
        )

        val response = FeedbackAssembler.mapToEntity(submissionDto, feedbackForm)

        response.deduplicationKey shouldBe "unique-key"
        response.environment.deviceModelName shouldBe "Test Device"
        response.environment.operatingSystem.name shouldBe "Android"
        response.environment.operatingSystem.version shouldBe "10"
        response.environment.app.version shouldBe "1.0"
        response.environment.app.browserName shouldBe "Chrome"
        response.environment.app.usedAs shouldBe "app"
    }
}
