package io.swcode.urbo.cms.application.file

import io.kotest.matchers.shouldBe
import io.mockk.*
import io.swcode.urbo.cms.BaseControllerTest
import io.swcode.urbo.cms.s3.api.S3Adapter
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatusCode
import org.springframework.http.ResponseEntity
import org.springframework.mock.web.MockMultipartFile
import java.io.IOException
import java.net.URI
import java.time.Duration

class FileControllerTest: BaseControllerTest() {

    private lateinit var s3Adapter: S3Adapter
    private lateinit var fileController: FileController

    @BeforeEach
    fun setup() {
        s3Adapter = mockk(relaxed = true)
        fileController = FileController(s3Adapter)
    }

    @Test
    fun `should redirect to signed URL`() {
        val fileName = "test.jpg"
        val signedUrl = URI.create("https://example.com/test.jpg")

        every { s3Adapter.requestSignedDownloadUrl(fileName, Duration.ofDays(1)) } returns signedUrl.toURL()

        val response: ResponseEntity<Unit> = fileController.downloadVisualFile(fileName)

        response.statusCode shouldBe HttpStatusCode.valueOf(302)
        response.headers.location shouldBe signedUrl

        verify { s3Adapter.requestSignedDownloadUrl(fileName, Duration.ofDays(1)) }
    }

    @Test
    fun `should upload file successfully`() {
        val mockFile = MockMultipartFile(
            "image",
            "testfile.txt",
            "text/plain",
            "test content".toByteArray()
        )

        every { s3Adapter.putS3Object(any(), any()) } just Runs

        val response: ResponseEntity<String> = fileController.uploadFile(mockFile)

        response.statusCode shouldBe HttpStatusCode.valueOf(200)

        verify(exactly = 1) { s3Adapter.putS3Object(any(), any()) }
    }

    @Test
    fun `should return bad request on upload failure`() {
        val mockFile = MockMultipartFile(
            "image",
            "testfile.txt",
            "text/plain",
            "test content".toByteArray()
        )

        every { s3Adapter.putS3Object(any(), any()) } throws IOException("Upload failed")

        val response: ResponseEntity<String> = fileController.uploadFile(mockFile)

        response.statusCode shouldBe HttpStatusCode.valueOf(400)

        verify(exactly = 1) { s3Adapter.putS3Object(any(), any()) }
    }
}