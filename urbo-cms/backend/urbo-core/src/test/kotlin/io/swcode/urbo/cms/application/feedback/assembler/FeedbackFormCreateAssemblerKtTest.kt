package io.swcode.urbo.cms.application.feedback.assembler

import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.swcode.urbo.cms.application.feedback.dto.FeedbackFormCreateDto
import io.swcode.urbo.cms.application.feedback.dto.FeedbackOptionDto
import io.swcode.urbo.cms.application.feedback.dto.FeedbackQuestionCreateDto
import io.swcode.urbo.cms.application.shared.image.localization.LocalizationDto
import io.swcode.urbo.cms.domain.model.feedback.FeedbackForm
import io.swcode.urbo.cms.domain.model.feedback.question.QuestionType
import io.swcode.urbo.cms.domain.model.shared.Language
import org.junit.jupiter.api.Test

class FeedbackFormCreateAssemblerKtTest {

    @Test
    fun `should map feedback create dto to entity`() {
        val feedbackFormCreateDto = prepareFeedbackFormCreateDto()

        val result = feedbackFormCreateDto.assembleFeedbackForm(1)

        result.shouldHaveCorrectBasics(1, 1)
        result.shouldHaveFirstQuestionDetails(QuestionType.SINGLE_CHOICE, "What is your favorite color?", Language.EN)
        result.shouldHaveFirstOptionDetails("Option 1", Language.EN)
    }


    private fun prepareFeedbackFormCreateDto(): FeedbackFormCreateDto {
        val optionDto = FeedbackOptionDto(
            localizedTexts = listOf(LocalizationDto("Option 1", Language.EN))
        )

        val questionDto = FeedbackQuestionCreateDto(
            questionType = QuestionType.SINGLE_CHOICE,
            questionName = listOf(LocalizationDto("What is your favorite color?", Language.EN)),
            possibleOptions = listOf(optionDto)
        )

        return FeedbackFormCreateDto(
            questions = listOf(questionDto)
        )
    }

    private fun FeedbackForm.shouldHaveCorrectBasics(version: Int, questionsCount: Int) {
        this.version shouldBe version
        this.id.shouldNotBeNull()
        this.questions shouldHaveSize questionsCount
    }

    private fun FeedbackForm.shouldHaveFirstQuestionDetails(
        expectedType: QuestionType,
        expectedValue: String,
        expectedLanguage: Language
    ) {
        val question = this.questions.first()
        question.questionType shouldBe expectedType
        question.questionName.first().value shouldBe expectedValue
        question.questionName.first().language shouldBe expectedLanguage
        question.possibleOptions shouldHaveSize 1
    }

    private fun FeedbackForm.shouldHaveFirstOptionDetails(expectedValue: String, expectedLanguage: Language) {
        val option = this.questions.first().possibleOptions.first()
        option.localizedTexts.first().value shouldBe expectedValue
        option.localizedTexts.first().language shouldBe expectedLanguage
    }
}