package io.swcode.urbo.cms.domain.event.news

import io.kotest.matchers.shouldBe
import io.mockk.confirmVerified
import io.mockk.mockk
import io.mockk.verify
import io.swcode.urbo.cms.domain.command.DeleteImageCommand
import io.swcode.urbo.cms.domain.command.DownloadEventImageCommand
import io.swcode.urbo.cms.domain.event.image.EntityType
import io.swcode.urbo.cms.domain.event.image.ImageSourceChangedEventHandler
import io.swcode.urbo.cms.domain.event.image.ImageSourceChangedEvent
import io.swcode.urbo.cms.domain.model.shared.Image
import io.swcode.urbo.command.api.CommandBus
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.*


class EventChangedNewsHandlerTest {

    private lateinit var commandBus: CommandBus

    private lateinit var handler: ImageSourceChangedEventHandler

    @BeforeEach
    fun setup() {
        commandBus = mockk(relaxUnitFun = true)
        handler = ImageSourceChangedEventHandler(commandBus)
    }

    @Test
    fun `should no trigger actions for null images`() {
        val event = ImageSourceChangedEvent(
            id = UUID.randomUUID(),
            EntityType.EVENT,
            image = null,
            imageRequest = null,
        )

        handler.handleImageChanged(event)
        confirmVerified(commandBus)
    }

    @Test
    fun `should trigger image delete and download`() {

        val expectedUUID = UUID.randomUUID()

        val event = ImageSourceChangedEvent(
            id = expectedUUID,
            EntityType.EVENT,
            image = Image("ExpectedImageSource"),
            imageRequest = Image("UpdatedImageSource"),
        )


        handler.handleImageChanged(event)

        verify {
            commandBus.submitAsyncCommand(match<DeleteImageCommand> {
                it.image.source shouldBe "ExpectedImageSource"
                true
            })
        }

        verify {
            commandBus.submitAsyncCommand(match<DownloadEventImageCommand> {
                it.eventId shouldBe expectedUUID
                it.image.source shouldBe "UpdatedImageSource"
                true
            })
        }
        confirmVerified(commandBus)
    }
}
