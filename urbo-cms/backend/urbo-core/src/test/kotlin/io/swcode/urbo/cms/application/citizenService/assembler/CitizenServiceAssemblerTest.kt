package io.swcode.urbo.cms.application.citizenService.assembler

import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.swcode.urbo.cms.application.citizenService.dto.CitizenServiceResponseDto
import io.swcode.urbo.cms.domain.model.citizenService.CitizenService
import io.swcode.urbo.cms.domain.model.citizenService.Tag
import io.swcode.urbo.cms.domain.model.shared.Language
import io.swcode.urbo.cms.domain.model.shared.Localization
import org.junit.jupiter.api.Test
import java.util.*

class CitizenServiceAssemblerTest {

    @Test
    fun `should map from CitizenService to CitizenServiceResponseDto`() {
        val citizenService = CitizenService(
            id = UUID.randomUUID(),
            externalId = "citizenId",
            title = "testTitle",
            description = "testDescription",
            url = "testUrl",
            tags = mutableListOf(
                Tag(
                    id = UUID.randomUUID(),
                    localizedNames = listOf(
                        Localization(
                            language = Language.DE,
                            value = "testTag"
                        )
                    )
                )
            )
        )

        val citizenServiceResponseDto = CitizenServiceResponseDto.fromDomain(citizenService)

        citizenServiceResponseDto.should {
            it.id shouldBe citizenService.id
            it.title shouldBe citizenService.title
            it.description shouldBe citizenService.description
            it.url shouldBe citizenService.url
            it.tags.size shouldBe citizenService.tags.size
            it.tags.first().id shouldBe citizenService.tags.first().id
            it.tags.first().localizedNames.size shouldBe citizenService.tags.first().localizedNames.size
            it.tags.first().localizedNames.first().language shouldBe citizenService.tags.first().localizedNames.first().language
            it.tags.first().localizedNames.first().value shouldBe citizenService.tags.first().localizedNames.first().value
        }
    }
}
