package io.swcode.urbo.cms.domain.model.feedback

import io.kotest.matchers.shouldBe
import io.swcode.urbo.cms.BaseTest
import io.swcode.urbo.cms.application.feedback.dto.EnvironmentDto
import io.swcode.urbo.cms.application.feedback.dto.FeedbackAnswerRequestDto
import io.swcode.urbo.cms.application.feedback.dto.FeedbackSubmissionRequestDto
import io.swcode.urbo.cms.domain.model.feedback.question.FeedbackQuestion
import io.swcode.urbo.cms.domain.model.feedback.question.QuestionType
import io.swcode.urbo.cms.domain.model.feedback.response.FeedbackResponseRepository
import io.swcode.urbo.cms.domain.model.shared.Language
import io.swcode.urbo.cms.domain.model.shared.Localization
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.util.*

class FeedbackServiceTest() : BaseTest() {

    @Autowired
    private lateinit var feedbackFormRepository: FeedbackFormRepository

    @Autowired
    private lateinit var feedbackResponseRepository: FeedbackResponseRepository

    @Autowired
    private lateinit var feedbackService: FeedbackService

    @Test
    fun `should return correct form`() {
        val feedbackForm = createAndSaveFeedbackForm()

        val retrievedForm = feedbackService.getFeedbackForm(feedbackForm.version)

        retrievedForm.id shouldBe feedbackForm.id
        retrievedForm.version shouldBe feedbackForm.version
        retrievedForm.questions.size shouldBe 1
        retrievedForm.questions[0].id shouldBe feedbackForm.questions[0].id
        retrievedForm.questions[0].questionType shouldBe feedbackForm.questions[0].questionType
        retrievedForm.questions[0].questionName shouldBe feedbackForm.questions[0].questionName
    }

    @Test
    fun `should save feedback response correctly`() {
        val feedbackForm = createAndSaveFeedbackForm()
        val feedbackSubmissionDto = createFeedbackSubmissionDto(feedbackForm)

        feedbackService.submitFeedback(feedbackSubmissionDto)

        val savedResponses = feedbackResponseRepository.findAll()
        savedResponses.size shouldBe 1
        savedResponses[0].feedbackForm.id shouldBe feedbackForm.id
        savedResponses[0].answers.size shouldBe 2
    }

    private fun createAndSaveFeedbackForm(): FeedbackForm {
        val feedbackForm = FeedbackForm(
            id = UUID.randomUUID(),
            version = 1,
            questions = mutableListOf()
        )

        val question = FeedbackQuestion(
            id = UUID.randomUUID(),
            questionType = QuestionType.SINGLE_CHOICE,
            questionName = listOf(Localization("What do you like most?", Language.EN)),
            feedbackForm = feedbackForm
        )

        feedbackForm.questions.add(question)

        return feedbackFormRepository.save(feedbackForm)
    }

    private fun createFeedbackSubmissionDto(feedbackForm: FeedbackForm): FeedbackSubmissionRequestDto {
        val questionId = feedbackForm.questions.first().id
        return FeedbackSubmissionRequestDto(
            version = feedbackForm.version,
            deduplicationKey = "unique-key",
            environment = EnvironmentDto(
                deviceModelName = "Test Device",
                operatingSystemName = "Android",
                operatingSystemVersion = "10",
                appVersion = "1.0",
                browserName = "Chrome",
                usedAs = "app",
                appSettings = """{ "userPreferences":""} """.trimIndent()
            ),
            answers = listOf(
                FeedbackAnswerRequestDto(questionId, selectedOptionId = null),
                FeedbackAnswerRequestDto(questionId, selectedOptionId = null, freeText = "Some feedback text")
            )
        )
    }
}

