package io.swcode.urbo.cms.domain.model.citizenService.service

import io.kotest.matchers.equals.shouldBeEqual
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.swcode.urbo.cms.BaseTest
import io.swcode.urbo.cms.application.citizenService.dto.CitizenServiceResponseDto
import io.swcode.urbo.cms.domain.model.citizenService.CitizenServiceRepository
import io.swcode.urbo.cms.domain.model.citizenService.CitizenServiceService
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import testUtil.createCitizenService

class CitizenServiceServiceTest : BaseTest() {

    @Autowired
    private lateinit var citizenServiceService: CitizenServiceService

    @Autowired
    private lateinit var citizenServiceRepository: CitizenServiceRepository

    @Test
    fun `should get CitizenServices`() {

        val citizenService = citizenServiceService.save(createCitizenService())

        val citizenResponseDtos =
            citizenServiceService.getCitizenServices(Pageable.unpaged(), null, visible = true, favorite = false).content

        citizenResponseDtos.shouldNotBeNull()

        citizenResponseDtos[0].shouldBe(CitizenServiceResponseDto.fromDomain(citizenService))
    }

    @Test
    fun `should get Citizen services by tag id`() {

        val citizenService = citizenServiceService.save(createCitizenService())

        val citizenServices =
            citizenServiceService.getCitizenServicesByTag(citizenService.tags.first().id, Pageable.unpaged()).content

        citizenServices.shouldNotBeNull()

        citizenServices[0].shouldBe(CitizenServiceResponseDto.fromDomain(citizenService))
    }

    @Test
    fun `should update CitizenService`() {
        val initialCitizenService = citizenServiceService.save(createCitizenService())

        val updatedCitizenService = citizenServiceService.update(createCitizenService().copy(title = "new title"))

        citizenServiceRepository.findByIdOrNull(initialCitizenService.id)?.shouldBeEqual(updatedCitizenService)
        citizenServiceRepository.findByIdOrNull(initialCitizenService.id)?.title shouldBe "new title"
    }

    @Test
    fun `should search CitizenService`() {
        citizenServiceService.save(
            createCitizenService().copy(
                externalId = "1",
                title = "Kotlin",
                description = "Kotlin is a programming language"
            )
        )
        citizenServiceService.save(
            createCitizenService().copy(
                externalId = "2",
                title = "Java",
                description = "Java is a programming language"
            )
        )


        citizenServiceService.searchCitizenServices("Kotlin", Pageable.ofSize(100)) should {
            it.content.size shouldBe 1
            it.content[0].title shouldBe "Kotlin"
        }
    }

    @Test
    fun `should get CitizenService by id`() {

        val citizenService = citizenServiceService.save(createCitizenService())

        citizenServiceService.getCitizenService(citizenService.id)
            .shouldBeEqual(CitizenServiceResponseDto.fromDomain(citizenService))
    }
}
