package io.swcode.urbo.cms.application.feed

import io.swcode.urbo.cms.BaseControllerTest
import io.swcode.urbo.cms.testutil.TestDataProvider
import org.hamcrest.Matchers
import org.hamcrest.Matchers.containsInAnyOrder
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.get
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*

class FeedControllerTest : BaseControllerTest() {

    @Autowired
    private lateinit var testDataProvider: TestDataProvider

    @Test
    fun `get event feed without filter`() {
        val event = testDataProvider.createEvent(1)

        mockMvc.get("/v1/feed-events")
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.page.totalElements", Matchers.`is`(1))
                jsonPath("$.content[0].id", Matchers.`is`(event.id.toString()))
            }
    }

    @Test
    fun `get event feed grouped`() {
        val event = testDataProvider.createEvent(2)

        mockMvc.get("/v1/grouped-feed-events")
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.page.totalElements", Matchers.`is`(1))
                jsonPath("$.content[0].id", Matchers.`is`(event.id.toString()))
            }
    }

    @Test
    fun `get whole event feed`() {
        val event = testDataProvider.createEvent(2)

        mockMvc.get("/v1/feed-events") {
        }.andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.page.totalElements", Matchers.`is`(2))
                jsonPath("$.content[0].id", Matchers.`is`(event.id.toString()))
                jsonPath("$.content[1].id", Matchers.`is`(event.id.toString()))
            }
    }

    @Test
    fun `get event feed with filter`() {
        mockMvc.get("/v1/feed-events") {
            param("minStartAt", Instant.now().plus(1, ChronoUnit.DAYS).toString())
            param("categoryIds", "${UUID.randomUUID()}")
            param("sourceNames", "Soest,Anzeiger")
        }.andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.page.totalElements", Matchers.`is`(0))
            }
    }

    @Test
    fun `should reject invalid UUID in categoryIds`() {
        mockMvc.get("/v1/feed-events") {
            param("categoryIds", "invalid-uuid")
        }.andExpect {
            status { isBadRequest() }
            jsonPath("$.detail", Matchers.`is`("Failed to convert 'categoryIds' with value: 'invalid-uuid'"))
        }
    }

    @Test
    fun `should reject overly long searchTerm in grouped feed`() {
        val longSearchTerm = "a".repeat(501)
        mockMvc.get("/v1/grouped-feed-events") {
            param("searchTerm", longSearchTerm)
        }.andExpect {
            status { isBadRequest() }
            jsonPath("$.detail", Matchers.`is`("Request validation failed"))
        }
    }

    @Test
    fun `should reject invalid UUID in categoryIds for grouped feed`() {
        mockMvc.get("/v1/grouped-feed-events") {
            param("categoryIds", "invalid-uuid")
        }.andExpect {
            status { isBadRequest() }
            jsonPath("$.detail", Matchers.`is`("Failed to convert 'categoryIds' with value: 'invalid-uuid'"))
        }
    }

    @Test
    fun `should reject invalid UUID in excludingEventIds for grouped feed`() {
        mockMvc.get("/v1/grouped-feed-events") {
            param("excludingEventIds", "invalid-uuid")
        }.andExpect {
            status { isBadRequest() }
            jsonPath("$.detail", Matchers.`is`("Failed to convert 'excludingEventIds' with value: 'invalid-uuid'"))
        }
    }

    @Test
    fun `should accept future date for minStartAt`() {
        val presentDate = Instant.now().plus(1, ChronoUnit.HOURS)
        mockMvc.get("/v1/feed-events") {
            param("minStartAt", presentDate.toString())
        }.andExpect {
            status { isOk() }
        }
    }

    @Test
    fun `should accept future date for maxEndAt`() {
        val futureDate = Instant.now().plusSeconds(86400)
        mockMvc.get("/v1/feed-events") {
            param("maxEndAt", futureDate.toString())
        }.andExpect {
            status { isOk() }
        }
    }

    @Test
    fun `get grouped event feed with eventIds filter`() {
        val event1 = testDataProvider.createEvent(1)
        val event2 = testDataProvider.createEvent(2)

        mockMvc.get("/v1/grouped-feed-events") {
            param("eventIds", event1.id.toString(), event2.id.toString())
        }
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.page.totalElements", Matchers.`is`(2))
                jsonPath("$.content[*].id", containsInAnyOrder(event1.id.toString(), event2.id.toString()))
            }
    }

    @Test
    fun `get grouped event feed with eventIds filter with a not existing uuid`() {
        val event1 = testDataProvider.createEvent(1)
        testDataProvider.createEvent(2)

        mockMvc.get("/v1/grouped-feed-events") {
            param("eventIds", event1.id.toString(), UUID.randomUUID().toString())
        }.andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.page.totalElements", Matchers.`is`(1))
                jsonPath("$.content[0].id", Matchers.`is`(event1.id.toString()))
            }
    }

    @Test
    fun `get grouped event feed with excludedOccurrenceIds filter`() {
        val event1 = testDataProvider.createEvent(1)
        val event2 = testDataProvider.createEvent(2)
        val occurrenceToExclude = event1.occurrences[0]

        mockMvc.get("/v1/grouped-feed-events") {
            param("excludingOccurrenceIds", occurrenceToExclude.id.toString())
        }
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.page.totalElements", Matchers.`is`(1))
                jsonPath("$.content[0].id", Matchers.`is`(event2.id.toString()))
            }
    }

    @Test
    fun `get event feed with excludedOccurrenceIds filter`() {
        val event1 = testDataProvider.createEvent(1)
        val event2 = testDataProvider.createEvent(1)
        val occurrenceToExclude = event1.occurrences[0]

        mockMvc.get("/v1/feed-events") {
            param("excludingOccurrenceIds", occurrenceToExclude.id.toString())
        }
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.page.totalElements", Matchers.`is`(1))
                jsonPath("$.content[0].id", Matchers.`is`(event2.id.toString()))
            }
    }

    @Test
    fun `get event feed including currently running events`() {
        val now = Instant.now()

        val runningEvent = testDataProvider.createRunningEvent(now)
        val upcomingEvent = testDataProvider.createUpcomingEvent(now)
        testDataProvider.createPastEvent(now)

        mockMvc.get("/v1/feed-events") {
            param("minStartAt", now.toString())
        }.andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.page.totalElements", Matchers.`is`(2))
                jsonPath("$.content[*].id", containsInAnyOrder(runningEvent.id.toString(), upcomingEvent.id.toString()))
            }
    }
}
