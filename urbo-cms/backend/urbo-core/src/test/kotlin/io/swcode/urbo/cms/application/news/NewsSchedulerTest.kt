package io.swcode.urbo.cms.application.news

import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.swcode.urbo.cms.BaseTest
import io.swcode.urbo.cms.domain.model.news.NewsRepository
import io.swcode.urbo.cms.domain.model.news.NewsService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import testUtil.createNewsEntity

class NewsSchedulerTest : BaseTest() {

    private final val mockNewsAdapter = mockk<NewsAdapter>()

    @Autowired
    private lateinit var newsService: NewsService

    @Autowired
    private lateinit var newsRepository: NewsRepository


    @BeforeEach
    fun setup() {
        newsRepository.deleteAll()
    }

    @Test
    fun `getAndCreateNews should process every News from the adapter`() {
        val newsScheduler = NewsScheduler(mockNewsAdapter, newsService)

        val news = listOf(createNewsEntity(), createNewsEntity())
        every { mockNewsAdapter.getNews() } returns news

        newsScheduler.getAndCreateNews()

        newsService.getAllNews(Pageable.unpaged()).should {
            it.content.size shouldBe 2
        }
    }
}
