package io.swcode.urbo.cms.application.waste

import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.swcode.urbo.cms.domain.model.waste.WasteCollectionLocation
import io.swcode.urbo.cms.domain.model.waste.repository.WasteCollectionLocationRepository
import io.swcode.urbo.cms.domain.model.waste.service.WasteService
import io.swcode.urbo.cms.BaseTest
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import java.util.*

class WasteCollectionLocationSchedulerTest : BaseTest() {

    private final val mockWasteAdapter = mockk<WasteAdapter>()

    @Autowired
    private lateinit var wasteService: WasteService

    @Autowired
    private lateinit var wasteCollectionLocationRepository: WasteCollectionLocationRepository


    @BeforeEach
    fun setup() {
        wasteCollectionLocationRepository.deleteAll()
    }

    @Test
    fun `getAndCreateWasteCollections should process every WasteCollection from the adapter`() {

        val wasteScheduler = WasteScheduler(mockWasteAdapter, wasteService)

        val mockWasteCollectionLocations = listOf(

            WasteCollectionLocation(
                id = UUID.randomUUID(),
                wasteId = "id",
                city = "city",
                street = "street",
                wasteCollectionDates = mutableListOf()
            ),

            WasteCollectionLocation(
                id = UUID.randomUUID(),
                wasteId = "id2",
                city = "city",
                street = "street",
                wasteCollectionDates = mutableListOf()
            ),

            )
        every { mockWasteAdapter.getWasteCollections() } returns mockWasteCollectionLocations

        wasteScheduler.getAndCreateWasteCollection()

        wasteService.getWasteCollections(pageable = Pageable.unpaged()).should {
            it.size shouldBe 2
        }
    }
}
