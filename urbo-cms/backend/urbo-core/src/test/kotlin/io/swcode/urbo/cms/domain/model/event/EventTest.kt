package io.swcode.urbo.cms.domain.model.event

import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.swcode.urbo.cms.domain.model.category.SourceCategory
import io.swcode.urbo.cms.domain.model.category.CategoryType
import org.junit.jupiter.api.Test
import testUtil.createEventEntity
import java.util.*

class EventTest {

    @Test
    fun `should update Event`() {
        val initialEvent = createEventEntity()

        val updateEvent = createEventEntity().copy(
            id = UUID.randomUUID(),
            title = "Event2",
            occurrences = initialEvent.occurrences,
            sourceCategories = mutableListOf(SourceCategory("Category2", CategoryType.EVENTS))
        )

        val updatedEvent = initialEvent.updateFrom(updateEvent)

        updatedEvent.title shouldBe "Event2"
        updatedEvent.sourceCategories shouldHaveSize 1
        updatedEvent.sourceCategories[0].name shouldBe "Category2"
    }

    @Test
    fun `equals should correctly identify matching events based on eventId`() {
        val event1 = createEventEntity()

        val event2 = event1.copy()
        val event3 = event1.copy(id = UUID.randomUUID())

        (event1 == event2) shouldBe true
        (event1 == event3) shouldBe false
    }
}
