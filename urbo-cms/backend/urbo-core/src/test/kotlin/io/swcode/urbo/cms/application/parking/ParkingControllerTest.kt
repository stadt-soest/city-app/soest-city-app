package io.swcode.urbo.cms.application.parking

import io.swcode.urbo.cms.BaseControllerTest
import io.swcode.urbo.cms.WithUserContext
import io.swcode.urbo.cms.application.CorePermission
import io.swcode.urbo.cms.domain.model.parking.ParkingAreaRepository
import io.swcode.urbo.cms.domain.model.parking.ParkingService
import io.swcode.urbo.cms.domain.model.parking.ParkingSpecialOpeningHours
import org.hamcrest.CoreMatchers.`is`
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.put
import testUtil.mockParkingArea
import java.time.LocalDate
import java.time.LocalTime

class ParkingControllerTest : BaseControllerTest() {

    @Autowired
    private lateinit var parkingService: ParkingService

    @Autowired
    private lateinit var parkingRepository: ParkingAreaRepository

    @BeforeEach
    fun setup() {
        parkingRepository.deleteAll()
    }

    @Test
    fun `should get all parking areas`() {
        parkingService.save(mockParkingArea())

        mockMvc
            .get("/v1/parkingAreas")
            .andExpect { status { isOk() } }
            .andExpect { jsonPath("$.content.size()", `is`(1)) }
    }

    @Test
    fun `should get parking area`() {
        val parkingArea = mockParkingArea()
        parkingService.save(parkingArea)

        mockMvc
            .get("/v1/parkingAreas/${parkingArea.id}")
            .andExpect { status { isOk() } }
            .andExpect { jsonPath("$.id", `is`(parkingArea.id.toString())) }
            .andExpect { jsonPath("$.name", `is`(parkingArea.name)) }
            .andExpect { jsonPath("$.location.address.street", `is`(parkingArea.location.address.street)) }
            .andExpect { jsonPath("$.location.address.postalCode", `is`(parkingArea.location.address.postalCode)) }
            .andExpect { jsonPath("$.location.address.city", `is`(parkingArea.location.address.city)) }
            .andExpect { jsonPath("$.location.coordinates.latitude", `is`(parkingArea.location.coordinates.latitude)) }
            .andExpect {
                jsonPath(
                    "$.location.coordinates.longitude",
                    `is`(parkingArea.location.coordinates.longitude)
                )
            }
            .andExpect { jsonPath("$.capacity.total", `is`(parkingArea.capacity.total)) }
            .andExpect { jsonPath("$.capacity.remaining", `is`(parkingArea.capacity.remaining)) }
            .andExpect { jsonPath("$.openingTimes", `is`(parkingArea.openingTimes)) }
    }

    @Test
    @WithUserContext(permissions = [CorePermission.PARKING_AREA_WRITE])
    fun `should update parking area opening times`() {
        val parkingArea = mockParkingArea()
        parkingService.save(parkingArea)

        val requestBody = """
            {
                "openingTimes": {
                    "MONDAY": { "from": "08:00", "to": "18:00" },
                    "TUESDAY": { "from": "08:00", "to": "18:00" }
                }
            }
        """.trimIndent()

        mockMvc
            .put("/v1/parkingAreas/${parkingArea.id}/opening-times") {
                contentType = MediaType.APPLICATION_JSON
                content = requestBody
            }
            .andExpect { status { isOk() } }
            .andExpect { jsonPath("$.id", `is`(parkingArea.id.toString())) }
            .andExpect { jsonPath("$.openingTimes.MONDAY.from", `is`("08:00:00")) }
            .andExpect { jsonPath("$.openingTimes.MONDAY.to", `is`("18:00:00")) }
            .andExpect { jsonPath("$.openingTimes.TUESDAY.from", `is`("08:00:00")) }
            .andExpect { jsonPath("$.openingTimes.TUESDAY.to", `is`("18:00:00")) }
    }

    @Test
    @WithUserContext
    fun `should not update parking area opening times`() {
        val parkingArea = mockParkingArea()
        parkingService.save(parkingArea)

        val requestBody = """
            {
                "openingTimes": {
                    "MONDAY": { "from": "08:00", "to": "18:00" },
                    "TUESDAY": { "from": "08:00", "to": "18:00" }
                }
            }
        """.trimIndent()

        mockMvc
            .put("/v1/parkingAreas/${parkingArea.id}/opening-times") {
                contentType = MediaType.APPLICATION_JSON
                content = requestBody
            }
            .andExpect { status { isForbidden() } }
    }

    @Test
    @WithUserContext(permissions = [CorePermission.PARKING_AREA_WRITE])
    fun `should update parking area special opening times`() {
        val parkingArea = mockParkingArea()
        parkingService.save(parkingArea)

        val requestBody = """
        {
            "specialOpeningTimes": [
                { 
                    "date": "2023-12-25", 
                    "from": "09:00", 
                    "to": "17:00",
                    "descriptions": []
                }
            ]
        }
    """.trimIndent()

        mockMvc
            .put("/v1/parkingAreas/${parkingArea.id}/special-opening-times") {
                contentType = MediaType.APPLICATION_JSON
                content = requestBody
            }
            .andExpect { status { isOk() } }
            .andExpect { jsonPath("$.id", `is`(parkingArea.id.toString())) }
            .andExpect { jsonPath("$.specialOpeningTimes[0].from", `is`("09:00:00")) }
            .andExpect { jsonPath("$.specialOpeningTimes[0].to", `is`("17:00:00")) }
    }

    @Test
    @WithUserContext(permissions = [CorePermission.PARKING_AREA_WRITE])
    fun `should clear all special opening times when passing empty list`() {
        val parkingArea = mockParkingArea()
        parkingArea.specialOpeningTimes[LocalDate.of(2023, 12, 25)] = ParkingSpecialOpeningHours(
            date = LocalDate.of(2023, 12, 25),
            from = LocalTime.of(9, 0),
            to = LocalTime.of(17, 0)
        )
        parkingService.save(parkingArea)

        val requestBody = """
        {
            "specialOpeningTimes": []
        }
    """.trimIndent()

        mockMvc
            .put("/v1/parkingAreas/${parkingArea.id}/special-opening-times") {
                contentType = MediaType.APPLICATION_JSON
                content = requestBody
            }
            .andExpect { status { isOk() } }
            .andExpect { jsonPath("$.id", `is`(parkingArea.id.toString())) }
            .andExpect { jsonPath("$.specialOpeningTimes", `is`(emptyList<Any>())) }
    }

    @Test
    @WithUserContext(permissions = [CorePermission.PARKING_AREA_WRITE])
    fun `should overwrite existing special opening times with new entries`() {
        val parkingArea = mockParkingArea()
        parkingArea.specialOpeningTimes[LocalDate.of(2023, 12, 25)] = ParkingSpecialOpeningHours(
            date = LocalDate.of(2023, 12, 25),
            from = LocalTime.of(9, 0),
            to = LocalTime.of(17, 0)
        )
        parkingService.save(parkingArea)

        val requestBody = """
        {
            "specialOpeningTimes": [
                { 
                    "date": "2024-01-01", 
                    "from": "10:00", 
                    "to": "20:00",
                    "descriptions": []
                },
                { 
                    "date": "2024-01-02", 
                    "from": "11:00", 
                    "to": "21:00",
                    "descriptions": []
                }
            ]
        }
    """.trimIndent()

        mockMvc
            .put("/v1/parkingAreas/${parkingArea.id}/special-opening-times") {
                contentType = MediaType.APPLICATION_JSON
                content = requestBody
            }
            .andExpect { status { isOk() } }
            .andExpect { jsonPath("$.id", `is`(parkingArea.id.toString())) }
            .andExpect { jsonPath("$.specialOpeningTimes.length()", `is`(2)) }
    }

    @Test
    @WithUserContext
    fun `should not update parking area special opening times without permission`() {
        val parkingArea = mockParkingArea()
        parkingService.save(parkingArea)

        val requestBody = """
        {
            "specialOpeningTimes": [
                { 
                    "date": "2023-12-25", 
                    "from": "09:00", 
                    "to": "17:00",
                    "descriptions": []
                }
            ]
        }
    """.trimIndent()

        mockMvc
            .put("/v1/parkingAreas/${parkingArea.id}/special-opening-times") {
                contentType = MediaType.APPLICATION_JSON
                content = requestBody
            }
            .andExpect { status { isForbidden() } }
    }
}
