package io.swcode.urbo.cms.domain.model.waste.service

import io.kotest.matchers.equals.shouldBeEqual
import io.swcode.urbo.cms.BaseTest
import io.swcode.urbo.cms.domain.model.waste.WasteCollectionLocation
import io.swcode.urbo.cms.domain.model.waste.WasteCollectionDate
import io.swcode.urbo.cms.domain.model.waste.WasteType
import io.swcode.urbo.cms.domain.model.waste.repository.WasteCollectionLocationRepository
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.util.*

class WasteCollectionLocationServiceTest : BaseTest() {

    @Autowired
    private lateinit var wasteService: WasteService

    @Autowired
    private lateinit var wasteCollectionLocationRepository: WasteCollectionLocationRepository

    @BeforeEach
    fun setup() {
        wasteCollectionLocationRepository.deleteAll()
    }

    private final val initialEntity = WasteCollectionLocation(
        id = UUID.randomUUID(),
        wasteId = "wasteId",
        city = "city",
        street = "street",
        wasteCollectionDates = mutableListOf()
    )

    val updateWasteCollectionLocation = WasteCollectionLocation(
        id = UUID.randomUUID(),
        wasteId = "wasteId",
        city = "Updated city",
        street = "Updated street",
        wasteCollectionDates = mutableListOf(
            WasteCollectionDate(
                UUID.randomUUID(),
                Instant.now(),
                WasteType.RESIDUAL_WASTE_14DAILY,
                initialEntity
            )
        )
    )

    @Test
    @Transactional
    fun `should create Waste`() {
        val waste = initialEntity

        wasteService.save(waste)

        wasteCollectionLocationRepository.findByIdOrNull(waste.id)?.shouldBeEqual(waste)
    }

    @Test
    @Transactional
    fun `should update Waste`() {
        val initialWaste = wasteService.save(initialEntity)

        val updatedWaste = wasteService.update(updateWasteCollectionLocation)

        wasteCollectionLocationRepository.findByIdOrNull(initialWaste.id)?.shouldBeEqual(updatedWaste)
    }
}
