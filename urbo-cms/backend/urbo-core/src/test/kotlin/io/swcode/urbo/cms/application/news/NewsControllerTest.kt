package io.swcode.urbo.cms.application.news

import io.kotest.matchers.shouldBe
import io.swcode.urbo.cms.WithUserContext
import io.swcode.urbo.cms.BaseControllerTest
import io.swcode.urbo.cms.application.news.dto.CreateNewsDto
import io.swcode.urbo.cms.application.news.dto.NewsHighlightRequestDto
import io.swcode.urbo.cms.application.news.dto.UpdateNewsDto
import io.swcode.urbo.cms.domain.model.category.Category
import io.swcode.urbo.cms.domain.model.category.CategoryRepository
import io.swcode.urbo.cms.domain.model.category.CategoryType
import io.swcode.urbo.cms.domain.model.category.SourceCategory
import io.swcode.urbo.cms.domain.model.news.NewsRepository
import io.swcode.urbo.cms.domain.model.news.NewsService
import io.swcode.urbo.cms.domain.model.shared.Language
import io.swcode.urbo.cms.domain.model.shared.Localization
import io.swcode.urbo.cms.application.CorePermission
import org.hamcrest.Matchers.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put
import testUtil.createNewsEntity
import java.util.*

@WithUserContext(permissions = [CorePermission.NEWS_WRITE, CorePermission.NEWS_DELETE])
class NewsControllerTest : BaseControllerTest() {

    @Autowired
    private lateinit var newsService: NewsService

    @Autowired
    private lateinit var categoryRepository: CategoryRepository

    @Autowired
    private lateinit var newsRepository: NewsRepository

    @BeforeEach
    fun setup() {
        newsRepository.deleteAll()
        categoryRepository.deleteAll()
    }

    @Test
    fun `should create news`() {
        val createNewsDto = CreateNewsDto(
            title = "New Kotlin Release",
            content = "Kotlin 1.5 has been released.",
            source = "Kotlin Blog",
            subtitle = "Exciting news for Kotlin developers!",
            link = "http://example.com",
            imageUrl = "http://example.com/image.png",
            categoryIds = listOf(UUID.randomUUID()),
            authors = listOf("John Doe")
        )

        val jsonContent = objectMapper.writeValueAsString(createNewsDto)

        mockMvc.post("/v1/news") {
            contentType = MediaType.APPLICATION_JSON
            content = jsonContent
        }
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.id", notNullValue())
                jsonPath("$.title", `is`("New Kotlin Release"))
                jsonPath("$.content", `is`("Kotlin 1.5 has been released."))
                jsonPath("$.source", `is`("Kotlin Blog"))
                jsonPath("$.subtitle", `is`("Exciting news for Kotlin developers!"))
                jsonPath("$.link", `is`("http://example.com"))
                jsonPath("$.image", notNullValue())
                jsonPath("$.categories.size()", `is`(0))
                jsonPath("$.authors.size()", `is`(1))
            }
    }


    @Test
    fun `should update news`() {
        val news = newsService.save(createNewsEntity())

        val updateNewsDto = UpdateNewsDto(
            title = "Updated Kotlin Release",
            content = "Kotlin 1.6 has been released.",
            source = "Kotlin Blog",
            subtitle = "More exciting news for Kotlin developers!",
            link = "http://example.com/new-link",
            imageUrl = "http://example.com/new-image.png",
            categoryIds = listOf(),
            authors = listOf("Jane Doe")
        )

        val jsonContent = objectMapper.writeValueAsString(updateNewsDto)

        mockMvc.put("/v1/news/${news.id}") {
            contentType = MediaType.APPLICATION_JSON
            content = jsonContent
        }
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.id", `is`(news.id.toString()))
                jsonPath("$.title", `is`("Updated Kotlin Release"))
                jsonPath("$.content", `is`("Kotlin 1.6 has been released."))
                jsonPath("$.source", `is`("Kotlin Blog"))
                jsonPath("$.subtitle", `is`("More exciting news for Kotlin developers!"))
                jsonPath("$.link", `is`("http://example.com/new-link"))
                jsonPath("$.image", notNullValue())
                jsonPath("$.categories.size()", `is`(0))
                jsonPath("$.authors.size()", `is`(1))
            }
    }

    @Test
    fun `should delete news`() {
        val news = newsService.save(createNewsEntity())

        mockMvc.delete("/v1/news/${news.id}")
            .andExpect { status { isNoContent() } }

        newsRepository.findByIdOrNull(news.id) shouldBe null
    }

    @Test
    fun `should get news`() {
        val news = newsService.save(createNewsEntity())

        mockMvc
            .get("/v1/news")
            .andExpect { status { HttpStatus.OK } }
            .andExpect {
                jsonPath("$.content.size()", `is`(1))
                jsonPath("$.content.[0].id", `is`(news.id.toString()))
                jsonPath("$.content.[0].title", `is`("title"))
                jsonPath("$.content.[0].content", `is`("content"))

            }
    }

    @Test
    fun `should get news by id`() {

        val news = newsService.save(createNewsEntity())

        mockMvc.get("/v1/news/{id}", news.id)
            .andExpect { status { HttpStatus.OK } }
            .andExpect {
                jsonPath("$.id", `is`(news.id.toString()))
                jsonPath("$.title", `is`("title"))
                jsonPath("$.content", `is`("content"))
            }
    }

    @Test
    fun `should filter news by category IDs`() {

        val news = newsService.save(
            createNewsEntity().apply {
                this.sourceCategories = mutableListOf(
                    SourceCategory(
                        "Technology",
                        CategoryType.NEWS,
                        Category(
                            UUID.randomUUID(),
                            listOf(Localization("Technology", Language.EN)),
                            listOf(),
                            CategoryType.NEWS,
                            null,
                            null,
                        )
                    )
                )
            })

        newsRepository.saveAll(listOf(createNewsEntity(), createNewsEntity()))


        mockMvc.get("/v1/news?categoryIds=${news.sourceCategories[0].category!!.id}")
            .andExpect { status { HttpStatus.OK } }
            .andExpect {
                jsonPath("$.content", hasSize<Any>(1))
                jsonPath("$.content[0].id", `is`(news.id.toString()))
            }
    }

    @Test
    fun `should filter news by sources`() {
        val sourceName = "TechBlog"
        val newsItem = createNewsEntity().apply {
            this.source = sourceName
        }
        newsService.save(newsItem)

        mockMvc.get("/v1/news?sources=$sourceName")
            .andExpect { status { HttpStatus.OK } }
            .andExpect {
                jsonPath("$.content", hasSize<Any>(1))
                jsonPath("$.content[0].id", `is`(newsItem.id.toString()))
            }
    }

    @Test
    fun `should exclude news by id`() {
        val newsItem = newsService.save(createNewsEntity())
        val newsItem2 =
            newsService.save(newsItem.copy(externalId = UUID.randomUUID().toString(), id = UUID.randomUUID()))

        mockMvc.get("/v1/news?exclude=${newsItem.id}")
            .andExpect { status { HttpStatus.OK } }
            .andExpect {
                jsonPath("$.content", hasSize<Any>(1))
                jsonPath("$.content[0].id", not(`is`(newsItem2.id)))
            }
    }

    @Test
    fun `should filter news by highlight`() {
        newsService.save(createNewsEntity())
        val newsItem2 = newsService.save(createNewsEntity {
            highlight = true
        })

        mockMvc.get("/v1/news?highlight=true")
            .andExpect { status { HttpStatus.OK } }
            .andExpect {
                jsonPath("$.content", hasSize<Any>(1))
                jsonPath("$.content[0].id", `is`(newsItem2.id.toString()))
            }
    }

    @Test
    fun `should search news by keyword`() {
        val news1 = newsService.save(createNewsEntity().copy(title = "Kotlin News", content = "Kotlin is awesome"))
        val news2 = newsService.save(createNewsEntity().copy(title = "Java News", content = "Kotlin and Spring"))
        newsService.save(createNewsEntity().copy(title = "Tech Update", content = "Latest in technology"))

        mockMvc.get("/v1/news/search") { param("keyword", "Kotlin") }
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.content.size()", `is`(2))
                jsonPath("$.content[0].id", `is`(news1.id.toString()))
                jsonPath("$.content[0].title", `is`("Kotlin News"))
                jsonPath("$.content[0].content", `is`("Kotlin is awesome"))
                jsonPath("$.content[1].id", `is`(news2.id.toString()))
                jsonPath("$.content[1].title", `is`("Java News"))
                jsonPath("$.content[1].content", `is`("Kotlin and Spring"))
            }
    }

    @Test
    fun `should return correct number of News for given page size`() {
        repeat(10) {
            newsService.save(createNewsEntity())
        }

        mockMvc
            .get("/v1/news?page=0&size=5") {
                accept = MediaType.APPLICATION_JSON
            }
            .andExpect { status { isOk() } }
            .andExpect { jsonPath("$.content.size()", `is`(5)) }
    }

    @Test
    fun `should return correct News for given page number`() {
        repeat(10) { i ->
            newsRepository.save(createNewsEntity { title = "Title ${i + 1}" })
        }

        mockMvc
            .get("/v1/news?page=1&size=5")
            .andExpect { status { isOk() } }
            .andExpect { jsonPath("$.content[0].title", `is`("Title 6")) }
    }

    @Test
    fun `should return correct News for given page number with sources filter`() {
        repeat(30) { i ->
            newsRepository.save(createNewsEntity { title = "Title ${i + 1}" })
        }

        mockMvc
            .get("/v1/news?page=1&size=20", "sources" to arrayOf("soester-anzeiger"))
            .andExpect { status { isOk() } }
            .andExpect { jsonPath("$.page.totalElements", `is`(30)) }
    }

    @Test
    fun `should validate sources are within required size`() {
        val validSource = "ValidSource"
        val invalidSource = "x".repeat(51)

        mockMvc.get("/v1/news?sources=$validSource")
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.content", hasSize<Any>(0))
            }

        mockMvc.get("/v1/news?sources=$invalidSource")
            .andExpect { status { isBadRequest() } }
    }

    @Test
    fun `should ensure sources are not blank`() {
        val blankSource = " "

        mockMvc.get("/v1/news?sources=$blankSource")
            .andExpect { status { isBadRequest() } }
    }

    @Test
    fun `should validate keyword length does not exceed maximum`() {
        val validKeyword = "Spring"
        val invalidKeyword = "x".repeat(101)

        mockMvc.get("/v1/news/search?keyword=$validKeyword")
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.content", hasSize<Any>(0))
            }

        mockMvc.get("/v1/news/search?keyword=$invalidKeyword")
            .andExpect { status { isBadRequest() } }
    }

    @Test
    fun `should return all unique news sources`() {
        val sourceNames = listOf("Source1", "Source2", "Source1", "Source3", "Source2")
        sourceNames.forEach { sourceName ->
            newsService.save(createNewsEntity().apply { source = sourceName })
        }

        val expectedSources = sourceNames.distinct()

        mockMvc.get("/v1/news-sources")
            .andExpect { status { isOk() } }
            .andExpect {
                jsonPath("$.size()", `is`(expectedSources.size))
                jsonPath("$", containsInAnyOrder(*expectedSources.toTypedArray()))
            }
    }

    @Test
    @WithUserContext(permissions = [CorePermission.NEWS_HIGHLIGHT])
    fun `should highlight news`() {
        val news = newsService.save(createNewsEntity())
        val request = NewsHighlightRequestDto(true)

        mockMvc.put("/v1/news/${news.id}/highlight") {
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(request)
        }
            .andExpect { status { HttpStatus.OK } }
            .andExpect {
                jsonPath("$.id", `is`(news.id.toString()))
                jsonPath("$.highlight", `is`(true))
            }
    }
}
