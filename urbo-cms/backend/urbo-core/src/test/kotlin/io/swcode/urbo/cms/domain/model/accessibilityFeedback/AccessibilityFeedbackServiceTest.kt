package io.swcode.urbo.cms.domain.model.accessibilityFeedback

import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.swcode.urbo.cms.BaseTest
import io.swcode.urbo.cms.domain.model.feedback.response.AppDetails
import io.swcode.urbo.cms.domain.model.feedback.response.Environment
import io.swcode.urbo.cms.domain.model.feedback.response.OperatingSystem
import io.swcode.urbo.cms.s3.api.S3Adapter
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.multipart.MultipartFile
import java.time.Instant
import java.util.*

class AccessibilityFeedbackServiceTest : BaseTest() {

    private val s3Adapter: S3Adapter = mockk()

    @Autowired
    private lateinit var accessibilityFeedbackService: AccessibilityFeedbackService

    @Autowired
    private lateinit var accessibilityFeedbackRepository: AccessibilityFeedbackRepository

    @Test
    fun `should submit accessibility feedback`() {
        val accessibilityFeedback = AccessibilityFeedback(
            UUID.randomUUID(),
            "Button too big",
            createdAt = Instant.now(),
            mutableListOf(),
            environment = Environment(
                app = AppDetails(
                    "0.0.1",
                    "Chrome",
                    "PWA",
                ),
                deviceModelName = "Samsung",
                operatingSystem = OperatingSystem(
                    name = "Android",
                    version = "0.0.1"
                ),
                appSettings = """{ "userPreferences":""} """.trimIndent()
            )
        )

        val mockImage: MultipartFile = mockk()
        every { mockImage.inputStream } returns "".byteInputStream()
        every { s3Adapter.putS3Object(any(), any()) } returns Unit


        accessibilityFeedbackService.submitAccessibilityFeedback(accessibilityFeedback)

        accessibilityFeedbackRepository.getReferenceById(accessibilityFeedback.id).should {
            it.id shouldBe accessibilityFeedback.id
            it.answer shouldBe "Button too big"
            it.createdAt shouldBe accessibilityFeedback.createdAt
            it.environment.app.version shouldBe "0.0.1"
            it.environment.app.browserName shouldBe "Chrome"
            it.environment.app.usedAs shouldBe "PWA"
            it.environment.deviceModelName shouldBe "Samsung"
            it.environment.operatingSystem.name shouldBe "Android"
            it.environment.operatingSystem.version shouldBe "0.0.1"
        }
    }
}
