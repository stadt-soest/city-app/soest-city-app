package io.swcode.urbo.cms.application.citizenService

import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.swcode.urbo.cms.domain.model.citizenService.CitizenServiceRepository
import io.swcode.urbo.cms.domain.model.citizenService.CitizenServiceService
import io.swcode.urbo.cms.BaseTest
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.transaction.annotation.Transactional
import java.util.*

class CitizenSchedulerTest : BaseTest() {

    private final val mockCitizenAdapter = mockk<CitizenAdapter>()

    @Autowired
    private lateinit var citizenServiceService: CitizenServiceService

    @Autowired
    private lateinit var citizenServiceRepository: CitizenServiceRepository


    @BeforeEach
    fun setup() {
        citizenServiceRepository.deleteAll()
    }


    @Test
    @Transactional
    fun `getAndCreateCitizenServices should process every citizen service from the adapter`() {

        val citizenScheduler = CitizenScheduler(mockCitizenAdapter, citizenServiceService)

        val mockServices = listOf(
            io.swcode.urbo.cms.domain.model.citizenService.CitizenService(
                id = UUID.randomUUID(),
                externalId = "UUID1",
                title = "title",
                description = "description",
                url = "https://url.de",
            ),
            io.swcode.urbo.cms.domain.model.citizenService.CitizenService(
                id = UUID.randomUUID(),
                externalId = "UUID2",
                title = "title 2",
                description = "description 2",
                url = "https://url2.de",
            ),
        )
        every { mockCitizenAdapter.getCitizenServices() } returns mockServices

        citizenScheduler.getAndCreateCitizenServices()

        citizenServiceService.getCitizenServices(Pageable.unpaged(), null, visible = true, favorite = false).shouldNotBeNull()

        citizenServiceService.getCitizenServices(Pageable.unpaged(), null, visible = true, favorite = false).should {
            it.size shouldBe 2
        }


    }
}
