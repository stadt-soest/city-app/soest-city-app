package io.swcode.urbo.cms.domain.command.handler

import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.swcode.urbo.cms.domain.command.DownloadNewsImageCommand
import io.swcode.urbo.cms.domain.model.news.News
import io.swcode.urbo.cms.domain.model.news.NewsRepository
import io.swcode.urbo.cms.domain.model.shared.Image
import io.swcode.urbo.cms.domain.model.shared.ImageFile
import io.swcode.urbo.core.api.image.ImageProcessingAdapter
import io.swcode.urbo.core.api.image.ImageProcessingFileDto
import io.swcode.urbo.core.api.image.ImageProcessingResult
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.data.repository.findByIdOrNull
import testUtil.createNewsEntity

class DownloadNewsImageCommandHandlerTest {

    private lateinit var handler: DownloadNewsImageCommandHandler
    private lateinit var newsRepository: NewsRepository
    private lateinit var imageProcessingAdapter: ImageProcessingAdapter

    @BeforeEach
    fun setup() {
        newsRepository = mockk(relaxed = true)
        imageProcessingAdapter = mockk(relaxed = true)

        handler = DownloadNewsImageCommandHandler(newsRepository, imageProcessingAdapter)
    }

    @Test
    fun `should handle download news image command and update the news entity`() {
        val news = createNewsEntity()
        val command = DownloadNewsImageCommand(news.id, Image("testSource"))

        val image = Image("testSource")
        val downloadedImage = ImageProcessingResult(
            "downloaded", ImageProcessingFileDto("file1", 100, 100),
            ImageProcessingFileDto("file2", 200, 200)
        )
        val expected = Image("testSource", ImageFile("file1", 100, 100), ImageFile("file2", 200, 200))

        every { newsRepository.findByIdOrNull(news.id) } returns news
        every { imageProcessingAdapter.downloadAndProcessImage(image.source) } returns downloadedImage
        every { newsRepository.save(any<News>()) } returnsArgument 0

        handler.handle(command)

        verify { newsRepository.save(news) }

        news.image shouldBe expected
    }
}
