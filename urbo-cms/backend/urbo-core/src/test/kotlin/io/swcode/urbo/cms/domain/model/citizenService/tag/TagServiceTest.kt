package io.swcode.urbo.cms.domain.model.citizenService.tag

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.swcode.urbo.cms.BaseTest
import io.swcode.urbo.cms.domain.model.citizenService.CitizenServiceService
import io.swcode.urbo.common.error.UrboException
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import testUtil.createCitizenService
import java.util.*

class TagServiceTest : BaseTest() {

    @Autowired
    private lateinit var citizenServiceService: CitizenServiceService

    @Autowired
    private lateinit var tagService: TagService

    @Test
    fun `should get Tags`() {

        val citizenService = citizenServiceService.save(createCitizenService())

        val allTags = tagService.getAllTags()

        allTags.shouldNotBeNull()

        allTags[0].should {
            it.id shouldBe citizenService.tags.first().id
            it.localizedNames.size shouldBe citizenService.tags.first().localizedNames.size
            it.localizedNames[0].language shouldBe citizenService.tags.first().localizedNames[0].language
            it.localizedNames[0].value shouldBe citizenService.tags.first().localizedNames[0].value
        }
    }

    @Test
    fun `should get Tag`() {

        val citizenService = citizenServiceService.save(createCitizenService())

        val tag = tagService.getTagById(citizenService.tags.first().id)

        tag.should {
            it.id shouldBe citizenService.tags.first().id
            it.localizedNames.size shouldBe citizenService.tags.first().localizedNames.size
            it.localizedNames[0].language shouldBe citizenService.tags.first().localizedNames[0].language
            it.localizedNames[0].value shouldBe citizenService.tags.first().localizedNames[0].value
        }
    }

    @Test
    fun `should throw Tag not found error`() {

        citizenServiceService.save(createCitizenService())

        shouldThrow<UrboException> {
            tagService.getTagById(UUID.randomUUID())
        }
    }
}
