package io.swcode.urbo.cms.domain.model.news

import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import org.junit.jupiter.api.Test
import testUtil.createNewsEntity
import java.util.*

class NewsTest {

    private val initialNews = createNewsEntity()

    @Test
    fun `should be equal if ids are the same`() {
        val otherNews = createNewsEntity()
        otherNews.id = initialNews.id

        initialNews shouldBe otherNews
    }

    @Test
    fun `should not be equal if ids are different`() {
        val otherNews = createNewsEntity()
        otherNews.id = UUID.randomUUID()

        initialNews shouldNotBe otherNews
    }

    @Test
    fun `should have same hashcode if ids are the same`() {
        val otherNews = createNewsEntity()
        otherNews.id = initialNews.id

        initialNews.hashCode() shouldBe otherNews.hashCode()
    }

    @Test
    fun `should not have same hashcode if ids are different`() {
        val otherNews = createNewsEntity()
        otherNews.id = UUID.randomUUID()

        initialNews.hashCode() shouldNotBe otherNews.hashCode()
    }
}
