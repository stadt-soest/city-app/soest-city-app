package io.swcode.urbo.cms.domain.model.news.service

import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.collections.shouldContainExactlyInAnyOrder
import io.kotest.matchers.equals.shouldBeEqual
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import io.swcode.urbo.cms.BaseTest
import io.swcode.urbo.cms.domain.command.DeleteImageCommand
import io.swcode.urbo.cms.domain.command.DownloadNewsImageCommand
import io.swcode.urbo.cms.domain.model.news.NewsRepository
import io.swcode.urbo.cms.domain.model.news.NewsService
import io.swcode.urbo.common.date.toBerlinInstant
import io.swcode.urbo.cms.domain.model.shared.Image
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import stub.CommandBusStub
import testUtil.createNewsEntity

class NewsServiceTest : BaseTest() {

    @Autowired
    private lateinit var commandBus: CommandBusStub

    @Autowired
    private lateinit var newsService: NewsService

    @Autowired
    private lateinit var newsRepository: NewsRepository

    @BeforeEach
    fun setup() {
        commandBus.clearCommands()
    }

    @Test
    fun `should get News`() {
        newsService.save(createNewsEntity())

        val newsResponseDtos = newsService.getAllNews(Pageable.unpaged())

        newsResponseDtos.content.shouldNotBeNull()

    }

    @Test
    fun `should get News by id`() {
        val initialNews = newsService.save(createNewsEntity())

        val news = newsService.getNews(initialNews.id)

        news should {
            it.id shouldBe initialNews.id
            it.title shouldBe initialNews.title
            it.content shouldBe initialNews.content
            it.link shouldBe initialNews.link
            it.date shouldBe initialNews.date.toBerlinInstant()
            it.modified shouldBe initialNews.modified?.toBerlinInstant()
        }
    }

    @Test
    fun `should not create news again if it already exists`() {
        val news = createNewsEntity()

        newsService.save(news)

        newsRepository.findAll().size shouldBe 1
        newsRepository.findNewsByExternalId(news.externalId!!)
    }

    @Test
    fun `should update News`() {
        val initialNews = newsService.save(createNewsEntity().copy(source = "source"))

        val updatedNews = newsService.save(
            createNewsEntity().copy(
                id = initialNews.id,
                title = "updated title",
                source = "source"
            )
        )

        newsRepository.findByIdOrNull(initialNews.id)?.shouldBeEqual(updatedNews)
    }

    @Test
    fun `should delete old image and download new image when image URL is updated`() {
        val initialNews = newsService.save(
            createNewsEntity {
                image = Image("http://localhost:4566/old.jpg")
            }
        )

        val updateNews =
            createNewsEntity().copy(externalId = initialNews.externalId, image = Image("http://localhost:4566/new.jpg"))

        val updatedNews = newsService.save(updateNews)

        newsRepository.findByIdOrNull(initialNews.id)?.shouldBeEqual(updatedNews)

        commandBus.getCommands().shouldContain(DeleteImageCommand(initialNews.image!!))
        commandBus.getCommands().shouldContain(DownloadNewsImageCommand(newsId = initialNews.id, image = updatedNews.image!!))
    }

    @Test
    fun `should submit DownloadNewsImageCommand when news is saved`() {

        val news = newsService.save(
            createNewsEntity().copy(
                image = Image("http://localhost:4566/old.jpg")
            )
        )

        val commands = commandBus.getCommands()

        val downloadCommand = commands.filterIsInstance<DownloadNewsImageCommand>().first()

        downloadCommand shouldNotBe null
        downloadCommand.image.source shouldBe news.image?.source
    }

    @Test
    fun `should search News by title or content`() {
        val news1 = createNewsEntity().copy(title = "Kotlin is awesome", content = "Programming in Kotlin...")
        val news2 = createNewsEntity().copy(title = "Java News", content = "Kotlin interoperability with Java...")
        val news3 = createNewsEntity().copy(title = "Tech Update", content = "Latest tech news...")

        newsService.save(news1)
        newsService.save(news2)
        newsService.save(news3)

        val pageable = PageRequest.of(0, 10)

        val searchResult = newsService.searchNews("Kotlin", pageable)

        searchResult.content.map { it.title } shouldContainExactlyInAnyOrder listOf(news1.title, news2.title)
    }
}
