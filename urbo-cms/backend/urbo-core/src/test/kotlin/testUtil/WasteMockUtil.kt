package testUtil

import io.swcode.urbo.cms.domain.model.waste.WasteCollectionLocation
import io.swcode.urbo.cms.domain.model.waste.WasteCollectionDate
import io.swcode.urbo.cms.domain.model.waste.WasteType
import java.time.Instant
import java.util.*

fun createWaste(): WasteCollectionLocation = WasteCollectionLocation(
    id = UUID.randomUUID(),
    city = "city",
    street = "street",
    wasteId = UUID.randomUUID().toString(),
    wasteCollectionDates = mutableListOf()
)

fun createWasteDate(): WasteCollectionDate = WasteCollectionDate(
    id = UUID.randomUUID(),
    date = Instant.ofEpochSecond(1000),
    wasteType = WasteType.DIAPER_WASTE,
    wasteCollectionLocation = createWaste()
)
