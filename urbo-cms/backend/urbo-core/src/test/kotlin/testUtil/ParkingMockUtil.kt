package testUtil

import io.swcode.urbo.cms.domain.model.parking.*
import io.swcode.urbo.cms.domain.model.shared.Address
import io.swcode.urbo.cms.domain.model.shared.GeoCoordinates
import io.swcode.urbo.cms.domain.model.shared.Language
import io.swcode.urbo.cms.domain.model.shared.Localization
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.*

fun mockParkingArea() = ParkingArea(
    id = UUID.randomUUID(),
    externalId = "externalId",
    name = "name",
    location = ParkingLocation(
        address = Address(
            street = "street",
            city = "city",
            postalCode = "zipCode",
        ),
        coordinates = GeoCoordinates(0.0, 0.0)
    ),
    capacity = ParkingCapacity(
        total = 100,
        remaining = 50,
        modifiedAt = LocalDateTime.of(2000, 12, 8, 10, 10, 10).toInstant(ZoneOffset.UTC)
    ),
    openingTimes = mapOf(),
    status = ParkingStatus.OPEN,
    maximumAllowedHeight = "2.5m",
    priceDescription = mutableListOf(
        Localization(
            value = "test",
            language = Language.DE
        )
    ),
    description = mutableListOf(
        Localization(
            value = "test",
            language = Language.DE
        )
    ),
    displayName = "displayName",
    parkingType = ParkingType.PARKING_HOUSE
)
