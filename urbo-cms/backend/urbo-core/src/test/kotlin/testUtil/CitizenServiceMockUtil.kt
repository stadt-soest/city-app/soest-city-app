package testUtil

import io.swcode.urbo.cms.domain.model.citizenService.CitizenService
import io.swcode.urbo.cms.domain.model.citizenService.Tag
import io.swcode.urbo.cms.domain.model.shared.Language
import io.swcode.urbo.cms.domain.model.shared.Localization
import java.util.*

fun createCitizenService() = CitizenService(
    id = UUID.randomUUID(),
    externalId = "citizenId",
    title = "title",
    description = "description",
    url = "url",
    tags = mutableListOf(
        Tag(
            id = UUID.randomUUID(),
            localizedNames = listOf(Localization(
                language = Language.DE,
                value = "tag"
            ))
        )
    )
)
