package testUtil

import io.swcode.urbo.cms.domain.model.event.*
import io.swcode.urbo.cms.domain.model.category.SourceCategory
import io.swcode.urbo.cms.domain.model.category.CategoryType
import io.swcode.urbo.cms.domain.model.shared.Image
import java.time.*
import java.util.*

fun createEventEntity(
    sourceCategory: SourceCategory = SourceCategory(
        UUID.randomUUID().toString(),
        CategoryType.EVENTS
    )
): Event {

    val event = Event(
        id = UUID.randomUUID(),
        contactPoint = EventContactPoint(telephone = "telephone", email = "email"),
        sourceCategories = mutableListOf(sourceCategory),
        title = "title",
        subTitle = "subTitle",
        description = "description",
        image = Image("source"),
        offer = EventOffer(price = "price", availability = "availability", url = "url", description = "description"),
        location = EventLocation(
            locationName = "name",
            streetAddress = "address",
            city = "city",
            postalCode = "postalCode"
        ),
        occurrences = mutableListOf(),
    )

    val occurrence = EventOccurrence(
        id = UUID.randomUUID(),
        event = event,
        externalId = UUID.randomUUID().toString(),
        dateCreated = Instant.now(),
        status = EventStatus.SCHEDULED,
        startAt = ZonedDateTime.of(LocalDate.now().plusDays(2), LocalTime.of(1, 0, 0), ZoneId.systemDefault())
            .toInstant(),
        endAt = ZonedDateTime.of(LocalDate.now().plusDays(3), LocalTime.of(2, 0, 0), ZoneId.systemDefault())
            .toInstant(),
        source = "source"
    )

    event.occurrences.add(occurrence)

    return event
}


