package testUtil

import io.swcode.urbo.cms.domain.model.category.Category
import io.swcode.urbo.cms.domain.model.category.CategoryType
import io.swcode.urbo.cms.domain.model.news.News
import io.swcode.urbo.cms.domain.model.poi.Poi
import io.swcode.urbo.cms.domain.model.shared.*
import io.swcode.urbo.common.date.toBerlinInstant
import java.time.Instant
import java.time.LocalDateTime
import java.util.*

fun createNewsEntity(externalId: String = UUID.randomUUID().toString(), customizer: News.() -> Unit = {}) =
    News(
        id = UUID.randomUUID(),
        externalId = externalId,
        title = "title",
        subtitle = "subTitle",
        date = LocalDateTime.of(2000, 12, 8, 10, 10, 10).toBerlinInstant(),
        modified = LocalDateTime.of(2000, 12, 8, 10, 10, 10).toBerlinInstant(),
        content = "content",
        link = "link",
        authors = mutableListOf("author"),
        image = Image("source"),
        source = "soester-anzeiger",
        sourceCategories = mutableListOf(),
        highlight = false
    ).apply(customizer)

fun createNewsCategory(
    name: String = "Technology",
    language: Language = Language.EN,
    customizer: Category.() -> Unit = {}
) =
    Category.of(listOf(Localization(name, language)), CategoryType.NEWS, null).apply(customizer)

fun createEventCategory(
    name: String = "Music",
    language: Language = Language.EN,
    customizer: Category.() -> Unit = {}
) =
    Category.of(listOf(Localization(name, language)), CategoryType.EVENTS, null).apply(customizer)


fun createNewPoi(customizer: Poi.() -> Unit = {}): Poi {
    return Poi(
        UUID.randomUUID(),
        true,
        UUID.randomUUID().toString(),
        "Amtsgericht",
        ContactPoint("123456", "email", "Herr Müller"),
        Address("Nöttenstraße", "59494", "Soest"),
        listOf(),
        GeoCoordinates(50.0, 8.0),
        Instant.parse("2017-07-21T17:32:28Z"),
        "https:/gericht.com"
    ).apply(customizer)
}