plugins {
    id("io.swcode.urbo.library-conventions")
    alias(libs.plugins.kotlin.jpa)
    kotlin("kapt")
    jacoco
}

dependencies {
    runtimeOnly(libs.postgresql)

    implementation(project(":urbo-search-index-starter"))
    implementation(project(":urbo-meilisearch-api"))
    implementation(project(":urbo-core-api"))
    implementation(project(":urbo-permission-adapter"))
    implementation(project(":urbo-auth-adapter"))
    implementation(project(":urbo-common"))
    implementation(project(":urbo-user-management"))
    implementation(project(":urbo-s3-api"))
    implementation(project(":urbo-command-api"))

    implementation(libs.blaze.core.api)
    implementation(libs.blaze.core.impl)
    implementation(libs.blaze.hibernate.integration)
    implementation(libs.spring.boot.starter.data.jpa)
    implementation(libs.spring.boot.starter.web)
    implementation(libs.flyway.database.postgresql)
    implementation(libs.jackson.databind)
    implementation(libs.jackson.core)
    implementation(libs.jackson.datatype.js310)
    implementation(libs.jackson.module.kotlin)
    implementation(libs.jakarta.persistence.api)
    implementation(libs.hypersistence.utils.hibernate.get63())
    implementation(libs.spring.boot.starter.validation)

    //Push Notification (Firebase(Android) & Pushy(iOS))
    implementation(libs.firebase.admin)
    implementation(libs.pushy)

    // API Docuemntation
    implementation(libs.springdoc.openapi.webmvc)

    // Spring Security
    implementation(libs.spring.security.config)
    implementation(libs.spring.security.oauth2.jose)
    implementation(libs.spring.security.oauth2.resource.server)
    implementation(libs.kotlin.stdlib.jdk8)

    // deployment
    implementation(libs.spring.boot.starter.actuator)

    // ShedLock
    implementation(libs.shedlock.spring)
    implementation(libs.shedlock.jdbc)

    // tests
    testImplementation(project(":urbo-web"))
    testImplementation(testFixtures(project(":urbo-user-management")))
    testImplementation(libs.swagger.request.validator.mockmvc)
    testImplementation(libs.spring.boot.starter.test)
    testImplementation(libs.spring.security.test)
    testImplementation(libs.testcontainers.postgresql)
    testImplementation(libs.mockk)
    testImplementation(libs.spring.cloud.wiremock)
    testImplementation(libs.springmockk)

    testFixturesImplementation(libs.swagger.request.validator.mockmvc)
    testFixturesImplementation(libs.spring.boot.starter.test)
    testFixturesImplementation(libs.spring.security.test)
    testFixturesImplementation(libs.testcontainers.postgresql)
    testFixturesImplementation(libs.jakarta.transaction.api)
    testFixturesImplementation(libs.jakarta.servlet.api)
    testFixturesImplementation(libs.jackson.datatype.js310)
    testFixturesImplementation(libs.jackson.module.kotlin)
}

kotlin {
    compilerOptions.freeCompilerArgs.add("-Xemit-jvm-type-annotations")
}