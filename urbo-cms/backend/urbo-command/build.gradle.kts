plugins {
    id("org.springframework.boot")
    kotlin("jvm")
    kotlin("plugin.spring")
    alias(libs.plugins.kotlin.jpa)
    id("io.swcode.urbo.library-conventions")
}

dependencies {
    implementation(project(":urbo-common"))
    implementation(project(":urbo-command-api"))

    implementation(libs.spring.boot.starter.data.jpa)
    implementation(libs.kotlin.stdlib.jdk8)
    implementation(libs.kotlin.reflect)

    testImplementation(libs.spring.boot.starter.test)
}