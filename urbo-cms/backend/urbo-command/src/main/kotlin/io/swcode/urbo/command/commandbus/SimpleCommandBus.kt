package io.swcode.urbo.command.commandbus

import io.swcode.urbo.command.api.AsyncCommandSubmitter
import io.swcode.urbo.command.api.Command
import io.swcode.urbo.command.api.CommandBus
import io.swcode.urbo.command.api.WrappedAsyncCommand
import io.swcode.urbo.command.registry.CommandHandlerRegistry
import io.swcode.urbo.common.logging.createLogger
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.time.Duration
import java.util.*

@Component
class SimpleCommandBus(
    private val asyncCommandSubmitter: AsyncCommandSubmitter,
    private val commandHandlerRegistry: CommandHandlerRegistry
) : CommandBus {

    @Transactional
    override fun execute(commandId: UUID, command: Command) {
        try {
            commandHandlerRegistry.getHandler(command).handle(command)
        } catch (e: Exception) {
            log.error("Failed execute command=$command", e)
            throw e
        }
    }

    override fun submitAsyncCommand(command: Command) {
        asyncCommandSubmitter.submit(WrappedAsyncCommand(command))
    }

    override fun submitCommandWithDelay(command: Command, delay: Duration) {
        asyncCommandSubmitter.submit(WrappedAsyncCommand(command, delay))
    }

    companion object {
        private val log = createLogger()
    }
}