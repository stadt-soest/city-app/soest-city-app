package io.swcode.urbo.command.registry

import io.swcode.urbo.command.api.Command
import io.swcode.urbo.command.api.CommandHandler
import io.swcode.urbo.command.api.CommandRegistry
import io.swcode.urbo.common.error.UrboException
import io.swcode.urbo.common.logging.createLogger
import org.springframework.stereotype.Service
import kotlin.reflect.KClass
import kotlin.reflect.full.starProjectedType

@Service
class CommandHandlerRegistry(
    commandRegistry: CommandRegistry,
    private val commandHandlers: List<CommandHandler<out Command>>
) {
    private final val commandHandlerByCommand: Map<KClass<out Command>, CommandHandler<Command>>

    init {
        log.info("Handler classes: ${commandHandlers.map { it::class }}")
        commandHandlerByCommand = commandRegistry.getAllCommandClasses().associateWith { klass ->
            @Suppress("UNCHECKED_CAST")
            commandHandlers.firstOrNull {
                log.info("Checking handler: ${it::class}, Command: ${klass.simpleName}")
                it.canHandle(klass)
            } as? CommandHandler<Command>
                ?: throw UrboException("No handler registered for command class ${klass.simpleName}")
        }
    }

    fun getHandler(command: Command): CommandHandler<Command> {
        return commandHandlerByCommand[command::class]
            ?: throw UrboException("No handler found for ${command::class.simpleName}")
    }

    private fun CommandHandler<*>.canHandle(commandClass: KClass<*>): Boolean {
        return this::class.supertypes.flatMap { it.arguments }.any { it.type == commandClass.starProjectedType }
    }

    companion object {
        private val log = createLogger()
    }
}

