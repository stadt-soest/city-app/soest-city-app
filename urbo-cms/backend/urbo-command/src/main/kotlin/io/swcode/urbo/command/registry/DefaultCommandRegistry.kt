package io.swcode.urbo.command.registry

import io.swcode.urbo.command.api.Command
import io.swcode.urbo.command.api.CommandRegistry
import io.swcode.urbo.common.logging.createLogger
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider
import org.springframework.core.type.filter.AssignableTypeFilter
import org.springframework.stereotype.Component
import java.util.concurrent.ConcurrentHashMap
import java.util.function.Consumer
import kotlin.reflect.KClass

@Component
class DefaultCommandRegistry(
    private val context: ApplicationContext,
    @Value("\${swcode.queue.commandBasePackage}") private val commandBasePackage: String
) : CommandRegistry, InitializingBean {

    /**
     * Contains all known [Command(s)][Command].
     */
    private val commandClasses = ConcurrentHashMap<String, KClass<out Command>>()

    override fun afterPropertiesSet() {
        loadCommandClasses()
    }

    override fun registerCommand(commandClass: KClass<out Command>) {
        commandClasses[commandClass.simpleName!!] = commandClass
    }

    /**
     * Returns the [Command] class for the given command name.
     * @param commandName The wanted command name
     * @return class of a [Command]
     * @throws IllegalArgumentException if no [Command] is known for the given command name
     */
    override fun getCommandClass(commandName: String): KClass<out Command> {
        return commandClasses[commandName] ?: run {
            logCommandRegistry(commandName)
            throw IllegalArgumentException("Unknown command: $commandName")
        }
    }

    override fun getAllCommandClasses(): Collection<KClass<out Command>> {
        return commandClasses.values.toList()
    }

    private fun loadCommandClasses() {
        log.info("Load commands from package path $commandBasePackage")
        findCommandClasses().forEach { commandClass: KClass<out Command> ->
            log.info("Registering command: ${commandClass.simpleName}")
            commandClasses[commandClass.simpleName!!] = commandClass
        }
        log.info("${commandClasses.size} commands loaded")
    }

    private fun findCommandClasses(): List<KClass<out Command>> {
        val provider = ClassPathScanningCandidateComponentProvider(false)
        provider.addIncludeFilter(AssignableTypeFilter(Command::class.java))
        return provider
            .findCandidateComponents(commandBasePackage)
            .map { it.beanClassName }
            .map { createClass(it) }
    }

    private fun createClass(name: String?): KClass<out Command> {
        return try {
            val clazz = context.classLoader!!.loadClass(name)

            check(Command::class.java.isAssignableFrom(clazz)) { "Class not subtype of Command: $name" }

            @Suppress("UNCHECKED_CAST")
            clazz.kotlin as KClass<out Command>
        } catch (ex: ClassNotFoundException) {
            throw IllegalStateException(ex)
        }
    }

    private fun logCommandRegistry(lookupCommand: String) {
        val stringBuilder = StringBuilder()
        commandClasses.keys.forEach(Consumer { commandName: String? ->
            stringBuilder.append(" ")
            stringBuilder.append(commandName)
            stringBuilder.append(" ")
        })
        log.error("Looked for command {} in registered commands: [{}]", lookupCommand, stringBuilder.toString())
    }

    companion object {
        private val log = createLogger()
    }
}