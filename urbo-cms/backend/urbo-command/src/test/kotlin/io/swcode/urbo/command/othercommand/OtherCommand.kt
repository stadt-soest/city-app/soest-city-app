package io.swcode.urbo.command.othercommand

import io.swcode.urbo.command.api.Command


data class OtherCommand(val value: String) : Command