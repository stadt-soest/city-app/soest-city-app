package io.swcode.urbo.command.registry

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.swcode.urbo.command.othercommand.OtherCommand
import io.swcode.urbo.command.testcommand.CommandA
import io.swcode.urbo.command.testcommand.CommandB
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.NONE,
    properties = ["swcode.queue.commandBasePackage=io.swcode.urbo.command.testcommand"],
    classes = [DefaultCommandRegistry::class]
)
class DefaultCommandRegistryTest {

    @Autowired
    lateinit var commandRegistry: DefaultCommandRegistry

    @Test
    fun `should load commands`() {
        val actual = commandRegistry.getAllCommandClasses().shouldHaveSize(2)
        actual.shouldContain(CommandA::class)
        actual.shouldContain(CommandB::class)

        commandRegistry.getCommandClass(CommandA::class.simpleName!!).shouldBe(CommandA::class)
        commandRegistry.getCommandClass(CommandB::class.simpleName!!).shouldBe(CommandB::class)


        shouldThrow<IllegalArgumentException> {
            commandRegistry.getCommandClass(OtherCommand::class.simpleName!!)
        }

    }
}