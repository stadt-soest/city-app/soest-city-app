package io.swcode.urbo.command.registry

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.collections.shouldHaveSize
import io.swcode.urbo.command.api.CommandHandler
import io.swcode.urbo.command.testcommand.CommandA
import io.swcode.urbo.command.testcommand.CommandB
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.runner.ApplicationContextRunner
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.stereotype.Component
import org.springframework.test.context.TestPropertySource

class CommandHandlerRegistryTest {

    @Test
    fun `should fail fast on missing command handler`() {
        shouldThrow<IllegalStateException> {
            ApplicationContextRunner()
                .withUserConfiguration(TestConfigMissingHandler::class.java)
                .withPropertyValues("swcode.queue.commandBasePackage=io.swcode.urbo.command.testcommand")
                .run { context -> context.getBean(CommandHandlerRegistry::class.java) }
        }
    }

    @Test
    fun `should load command handlers`() {
        ApplicationContextRunner()
            .withUserConfiguration(TestConfig::class.java)
            .withPropertyValues("swcode.queue.commandBasePackage=io.swcode.urbo.command.testcommand")
            .run { context ->
                val registry = context.getBean(CommandHandlerRegistry::class.java)
                val commandA = CommandA("test")
                val handler = registry.getHandler(commandA)
                handler.handle(commandA)
                (handler as CommandAHandler).apply {
                    this.handledCommands.shouldHaveSize(1)
                    this.handledCommands.shouldContain(commandA)
                }
            }
    }
}

@Configuration
@TestPropertySource(properties = ["swcode.queue.commandBasePackage=io.swcode.urbo.command.testcommand"])
@Import(DefaultCommandRegistry::class, CommandHandlerRegistry::class)
class TestConfigMissingHandler

@Configuration
@TestPropertySource(properties = ["swcode.queue.commandBasePackage=io.swcode.urbo.command.testcommand"])
@Import(DefaultCommandRegistry::class, CommandHandlerRegistry::class, CommandAHandler::class, CommandBHandler::class)
class TestConfig {

}

@Component
class CommandAHandler : CommandHandler<CommandA> {
    val handledCommands: MutableList<CommandA> = mutableListOf()

    override fun handle(command: CommandA) {
        handledCommands.add(command)
    }
}

@Component
class CommandBHandler : CommandHandler<CommandB> {
    override fun handle(command: CommandB) {
    }
}