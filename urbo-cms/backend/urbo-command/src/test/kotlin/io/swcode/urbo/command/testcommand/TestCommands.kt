package io.swcode.urbo.command.testcommand

import io.swcode.urbo.command.api.Command

data class CommandA(val value: String): Command

data class CommandB(val value: Int): Command