name: URBO-CMS Frontend

on:
  push:
    branches:
      - '**'
    tags-ignore:
      - '**'
    paths:
      - 'urbo-cms/frontend/**'
      - '.github/workflows/cms-frontend.yml'
  pull_request:
    branches:
      - '**'
    paths:
      - 'urbo-cms/frontend/**'
      - '.github/workflows/cms-frontend.yml'

permissions:
  contents: write
  packages: write
  pull-requests: read
  id-token: write

env:
  APP_NAME: urbo-cms-frontend
  RELEASE_NAME: urbo-cms-frontend
  RELEASE_NAMESPACE: urbo
  WORKING_DIRECTORY: urbo-cms/frontend

jobs:
  prestart:
    runs-on:
      - runs-on=${{ github.run_id }}
      - runner=2cpu-linux-x64
    outputs:
      pull_request_state: ${{ steps.find_pull_request.outputs.state }}
      pull_request_number: ${{ steps.find_pull_request.outputs.number }}
      pull_request_base_ref: ${{ steps.find_pull_request.outputs.base-ref }}
      branch_name: ${{ steps.extract_branch.outputs.branch }}
      commit_sha: ${{ steps.extract_sha.outputs.sha_short }}

    steps:
      - uses: actions/checkout@v4

      - name: Extract branch name
        id: extract_branch
        shell: bash
        run: echo "branch=${GITHUB_HEAD_REF:-${GITHUB_REF#refs/heads/}}" >> "$GITHUB_OUTPUT"

      - name: Extract commit SHA
        id: extract_sha
        run: echo "sha_short=$(git rev-parse --short ${{ github.sha }})" >> "$GITHUB_OUTPUT"

      - name: Find Pull Request
        uses: juliangruber/find-pull-request-action@v1
        id: find_pull_request
        with:
          branch: ${{ steps.extract_branch.outputs.branch }}
          state: open

  lint-and-format:
    needs: prestart
    concurrency:
      group: ${{ github.workflow }}-lint-${{ github.event.pull_request.number || github.ref }}
      cancel-in-progress: true
    runs-on:
      - runs-on=${{ github.run_id }}
      - runner=2cpu-linux-x64
    defaults:
      run:
        working-directory: ${{ env.WORKING_DIRECTORY }}
    steps:
      - uses: actions/checkout@v4
        with:
          fetch-depth: 0

      - uses: pnpm/action-setup@v4
        name: Install pnpm
        with:
          version: 9
          run_install: false

      - name: Install dependencies
        run: pnpm install --frozen-lockfile

      - name: Check format
        if: github.ref_name != 'main'
        run: pnpm nx format:check

      - name: Check lint
        if: github.ref_name != 'main'
        run: pnpm nx affected --target=lint --base=origin/${{ needs.prestart.outputs.pull_request_base_ref }} --head=origin/${{ needs.prestart.outputs.branch_name }}

  build:
    needs: [ prestart, lint-and-format ]
    concurrency:
      group: ${{ github.workflow }}-${{ github.ref }}-build
      cancel-in-progress: true
    runs-on:
      - runs-on=${{ github.run_id }}
      - runner=2cpu-linux-x64
    defaults:
      run:
        working-directory: ${{ env.WORKING_DIRECTORY }}
    outputs:
      releaseVersion: ${{ steps.gitversion.outputs.InformationalVersion }}
    steps:
      - uses: actions/checkout@v4
        with:
          fetch-depth: 0

      - uses: pnpm/action-setup@v4
        name: Install pnpm
        with:
          version: 9
          run_install: false

      - uses: actions/setup-node@v4
        with:
          node-version: 20.17.0
          cache: 'pnpm'
          cache-dependency-path: ${{ env.WORKING_DIRECTORY }}/pnpm-lock.yaml

      - run: pnpm install --frozen-lockfile
      - run: pnpm run build:all
      - run: pnpm run test:coverage

      - uses: actions/setup-java@v4
        with:
          distribution: 'temurin'
          java-version: '17'

      - name: Setup SonarQube
        uses: warchant/setup-sonar-scanner@v7

      - name: SonarQube Scan
        run: sonar-scanner -Dsonar.branch.name=${{ needs.prestart.outputs.branch_name }} -Dsonar.token=${{ secrets.SONAR_TOKEN }}

      - name: Install GitVersion
        uses: gittools/actions/gitversion/setup@v3.1.1
        with:
          versionSpec: '6.0.0'

      - name: GitVersion
        uses: GitTools/actions/gitversion/execute@v0.9.12
        id: gitversion
        with:
          useConfigFile: true

      - name: Setup Node.js with Authentication
        uses: actions/setup-node@v4
        with:
          node-version: 20.17.0
          registry-url: 'https://npm.pkg.github.com'
          scope: '@sw-code'
          cache: 'pnpm'
          cache-dependency-path: ${{ env.WORKING_DIRECTORY }}/pnpm-lock.yaml

      - name: Update and Publish Packages
        if: github.ref == 'refs/heads/main'
        run: pnpm nx release --specifier=${{ steps.gitversion.outputs.InformationalVersion }} --yes
        env:
          NODE_AUTH_TOKEN: ${{ secrets.GITHUB_TOKEN }}

      - name: Login to GitHub Container Registry
        if: contains('refs/heads/main', github.ref)
        uses: docker/login-action@v3
        with:
          registry: ghcr.io
          username: ${{ github.actor }}
          password: ${{ secrets.GITHUB_TOKEN }}

      - name: Docker Build and push
        if: contains('refs/heads/main', github.ref)
        uses: docker/build-push-action@v6
        with:
          context: ${{ env.WORKING_DIRECTORY }}
          push: true
          tags: |
            ghcr.io/sw-code/urbo/${{ env.APP_NAME }}:latest
            ghcr.io/sw-code/urbo/${{ env.APP_NAME }}:${{ steps.gitversion.outputs.InformationalVersion }}

  deploy:
    if: contains('refs/heads/main', github.ref)
    needs: build
    runs-on:
      - runs-on=${{ github.run_id }}
      - runner=2cpu-linux-x64
    steps:
      - uses: actions/checkout@v4

      - name: 'Helm Deploy'
        uses: WyriHaximus/github-action-helm3@v3
        with:
          exec: >-
            helm repo add bitnami https://charts.bitnami.com/bitnami &&
            helm repo update &&
            helm upgrade ${{ env.RELEASE_NAME }} bitnami/nginx \
              --install \
              --wait \
              --atomic \
              --namespace=${{ env.RELEASE_NAMESPACE }} \
              --values=${{ env.WORKING_DIRECTORY }}/kubernetes/values-prod.yml \
              --set image.tag=${{ needs.build.outputs.releaseVersion }} \
              --version 17.0.1
          kubeconfig: '${{ secrets.OTC_KUBE_CONFIG }}'
          overrule_existing_kubeconfig: "true"
