# Angular & Ionic Modular Application Integration

This open-source Angular/Ionic workspace provides developers a way to easily integrate new feature-modules, which are
automatically implemented into the App after following this documentation.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Starting the Project](#starting-the-project)
  - [Option 1: Local Backend](#option-1-local-backend)
  - [Option 2: Production Backend with API Proxy](#option-2-production-backend-with-api-proxy)
- [Generating Library & Ionic Page](#generating-library--ionic-page)
- [Setting up the Feature Module](#setting-up-the-feature-module)
- [Feature Module Internationalization Guide](#feature-module-internationalization-guide)
- [Integration into Main App](#integration-into-main-app)

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Node.js (version 20.17.0 or higher) installed
- pnpm (version 9.12.3 or higher) installed
- Nx CLI `nx` installed (`pnpm install -g nx`)
- Ionic CLI `ionic` installed (`pnpm install -g @ionic/cli`)

## Installation

Make sure you are in the app directory, for example by running the following command in your project directory.

```bash
cd app
```

To install the necessary dependencies, run:

```bash
pnpm install
```

## Starting the Project

You have two options to start the project, either use your local backend or production online backend.
Each option starts a server that recompiles the project if you change any file.

### Option 1: Local Backend

Use this option if you want to run the backend locally. This is useful for development and testing purposes. For more information on setting up and running the backend, refer to the [backend README](../urbo-cms/backend/README.md).

```bash
pnpm run start
```

### Option 2: Production Backend with API Proxy

This option allows you to use the production backend with real data. It is useful for simulating a production
environment.

```bash
pnpm run start:api-prod-proxy
```

## Running the tests

The easiest option is to run this script:

```bash
pnpm run test
```

To run tests for a specific project or library, use the following Nx command, replacing `feature-news` with the name of the project you want to test:

```bash
pnpm run test:feature-news
```

## Generating Library & Ionic Page

1. Generate a library using Nx:

   ```bash
   nx generate @nrwl/angular:library your-library-name
   ```

   1.1 Create the following file named 'jest.config.ts' in the root folder your new feature library.

   ```typescript
   import { Config } from 'jest';
   import baseConfig from './../../jest.config';

   const config: Config = {
     ...baseConfig,
     rootDir: '../../',
     roots: ['<rootDir>/projects/YOUR_FEATURE_MODULE_NAME_HERE/src'],
     displayName: 'Feature: YOUR_MODULE_DISPLAY_NAME_HERE',
   };

   export default config;
   ```

2. Generate an Angular component:

   ```bash
   nx generate component YOUR_COMPONENT_NAME --project=YOUR_LIBRARY_NAME
   ```

## Setting up the Feature Module

Within the generated library, the module should be set up to implement required interfaces and register the feature with
the App Module.

```typescript
import { ModuleWithProviders, NgModule, APP_INITIALIZER } from '@angular/core';
// ... (other necessary imports) ...

const MODULES = [UIModule];

@NgModule({
  imports: [...MODULES],
  exports: [...MODULES],
  providers: [YOUR_SERVICE_NAME],
})
export class YourFeatureModule {
  readonly feature: FeatureModule<FeedItem>;

  constructor(private yourService: YOUR_SERVICE_NAME) {
    this.feature = {
      feedables: this.yourService.getFeed(),
      info: new ModuleInfo('YOUR_MODULE_DISPLAY_NAME_HERE', ModuleCategory.YOUR_CATEGORY, 'your-icon', '/your-route'),
      routes: [
        {
          path: 'your-route',
          loadChildren: () => import('../pages/your-page/your-page.module').then((m) => m.YourPageModule),
        },
      ],
    };
  }

  static forRoot(): ModuleWithProviders<YourFeatureModule> {
    return {
      ngModule: YourFeatureModule,
      providers: [
        {
          provide: APP_INITIALIZER,
          useFactory: (registry: ModuleRegistryService, module: YourFeatureModule) => () => {
            registry.registerFeature(module.feature);
          },
          deps: [ModuleRegistryService, YourFeatureModule],
          multi: true,
        },
      ],
    };
  }
}
```

#### FeatureModule

- info: ModuleInfo: Contains meta-information about the feature module like its name, category, icon, and base routing
  path. This can be useful for rendering modules in UI sections based on their categories, showing icons, and providing
  navigation.
- routes: Routes: This is an array of Angular routing configurations specific to this module. By defining routes here,
  the application can dynamically add these to the main routing configuration ensuring the navigational paths are set
  correctly.
- feedables: Observable<FeedItem[]>: An observable stream of feed items that the module can provide. This could be
  entries, updates, notifications, etc., relevant to the module. The main app can listen to this stream to aggregate
  feeds from different modules.

#### ModuleInfo

Represents metadata about the feature module. For instance, if there was a dashboard or a module selection screen,
ModuleInfo would provide necessary details like the display name, icon, and so on.

#### ModuleCategory

An enumeration defining potential categories for feature modules. By categorizing modules, the main application can take
decisions like grouping similar modules together.

### Service and Interfaces

Your service should be implemented with the [Feedable](./projects/core/src/lib/intefaces/feedable.interface.ts)
interface and return an observable of [FeedItem](./projects/core/src/lib/models/feed-item.model.ts)[]. By using the
Feedable and FeedItem interface together, services can provide dynamic content updates to the main application in a
structured and consistent manner.

```typescript
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

// ... (other necessary imports) ...
@Injectable()
export class YourService implements Feedable {
  getFeed(): Observable<FeedItem[]> {
    // You might want to make a call to the backend/API, using the core-api module.
    return of([
      {
        moduleName: 'SampleModule',
        icon: 'sample-icon' as IconName, // Ensure you cast or provide correct iconName
        title: 'Sample Title',
        text: 'Sample Text',
        image: 'path-to-image.png',
        date: new Date(),
      },
      // ... (more feed items) ...
    ]);
  }

  getById(id: string): Observable<Model> {
    // TODO: Fetch a model by its ID. You might want to make a call to the backend/API.
  }
}
```

#### Feedable Interface

The Feedable interface acts as a contract that any service or component must adhere to if it intends to provide feed
items. Implementing this interface ensures that two methods are available. The first method, getFeed(), returns an
observable stream of feed items. The second method, getById(), is intended to fetch a specific feed item based on its
ID. By having these consistent method signatures across different services, the main application can easily fetch,
aggregate, and retrieve specific feeds from various modules without having to know the specifics of each module's
service.

```typescript
export interface Feedable {
  getFeed(): Observable<FeedItem[]>;

  getById(id: string): Observable<any>;
}
```

#### FeedItem Interface

The FeedItem model represents an individual entries or notification item that a module might want to communicate to the
main application. This standardized model ensures a consistent structure for feed items, enabling the main application
to display them in a unified manner. The properties of a FeedItem include:

```typescript
export interface FeedItem {
  id: string;
  moduleName: string;
  baseRoutePath: string;
  icon: string;
  title: string;
  text: string;
  date: Date;
  cardComponent: Type<AbstractFeedCardComponent<any>>;
}
```

- id: A unique identifier for each feed item.

- moduleName: Identifies the source or module of the feed item.

- baseRoutePath: The base route or path associated with the feed item.

- icon: Provides an icon name associated with the feed item.

- title: The main title or headline of the feed item.

- text: A brief description or content of the feed item.

- image: A path to an image associated with the feed item.

- date: The timestamp when the feed item was created or published.

- cardComponent: A component type that represents the card view of the feed item.

  > **Hint**: When implementing the ModuleFeedCardComponent, it is crucial to extend the AbstractFeedCardComponent. This
  > base component provides core functionalities that the feed card components leverage.

```typescript
export class ModuleFeedCardComponent extends AbstractFeedCardComponent<FeedItem> {}
```

## Feature Module Internationalization Guide

Follow these steps to implement translations for a feature module:

### 1. Create Translation Assets

#### 1.1 Directory Structure

Ensure that the folder structure is as follows:

    ├── feature-news/
       ├── src/
            ├── assets/
                ├── i18n/
                    ├── de.json
                    ├── en.json

- If the `assets` and `i18n` folders don't exist yet, please create them.

#### 1.2 Add Translations

Insert your translations into the respective `json` files:

#### `de.json`

```json
{
  "TITLE": "FEATURE NEWS MODULE TITLE DE"
}
```

#### `en.json`

```json
{
  "TITLE": "FEATURE NEWS MODULE TITLE EN"
}
```

### 2. Update Angular Assets

Add the path of the feature's assets folder to the angular.json file:

```json
{
  "glob": "**/*",
  "input": "projects/feature-news/src/assets/i18n",
  "output": "/assets/i18n/feature-news"
}
```

### 3. Update Feature Module

Update the `feature-news.module.ts` by adding SharedTranslationsModule to both the imports and exports sections of your
module; see the example below for detailed implementation.

```typescript
@NgModule({
  imports: [...MODULES],
  exports: [...MODULES],
  providers: [
    YOUR_SERVICE_NAME,
    provideTranslocoScope({
      scope: 'feature-news',
      alias: 'feature_news',
    })],
})
```

### 4. Import the Feature Module

When you want to use the translations in a page or component, import the Feature Module. For example:

```typescript
@NgModule({
  imports: [FeatureNewsModule, NewsPageRoutingModule],
  declarations: [NewsPage],
})
export class NewsPageModule {}
```

### 5. Use Translations in HTML

Lastly, to display the translations in your templates, use the `translate` pipe. For instance:

```html
<ion-title>{{ 'TITLE' | transloco }}</ion-title>
```

And that's it! Follow the steps above to implement and use translations in your feature-module .

## Integration into Main App

Lastly, to integrate the feature module:

1. Add your `FeatureModule.forRoot()` to the `FEATURE_MODULES` array in the
   main [App Module](./projects/app/src/app/app.module.ts).

   ```typescript
   const FEATURE_MODULES = [..., YourFeatureModule.forRoot()];
   ```

By following these steps, developers can seamlessly integrate their feature modules into the main application.
