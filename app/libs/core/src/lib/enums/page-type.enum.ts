export enum PageType {
  filterList = 'filterList',
  poiList = 'poiList',
  poiDetails = 'poiDetails',
}
