export enum StorageKeyType {
  /*
   eslint @typescript-eslint/naming-convention: 0
  */
  SOURCE = 'SourceSettings',
  CATEGORY = 'CategorySettings',
  NOTIFICATION = 'NotificationSettings',
  NOTIFICATION_DATA = 'NotificationData',
  MODULE_VISIBILITY = 'ModuleVisibilitySettings',
  LAST_SEARCH = 'LastSearch',
}
