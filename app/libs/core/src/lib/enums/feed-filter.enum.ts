export enum FeedFilterType {
  all = 'all',
  today = 'today',
  tomorrow = 'tomorrow',
  thisWeekend = 'thisWeekend',
  selectDays = 'selectDays',
}
