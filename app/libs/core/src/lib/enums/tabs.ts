export enum TabType {
  forYou = 'for-you',
  search = 'search',
  map = 'map',
  categories = 'categories',
}
