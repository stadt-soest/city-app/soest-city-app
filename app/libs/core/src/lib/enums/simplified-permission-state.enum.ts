export enum SimplifiedPermissionState {
  granted = 'granted',
  denied = 'denied',
}
