export enum ErrorType {
  /*
   eslint @typescript-eslint/naming-convention: 0
  */
  MODULE_API_ERROR = 'MODULE_API_ERROR',
  DETAILS_API_ERROR = 'DETAILS_API_ERROR',
}
