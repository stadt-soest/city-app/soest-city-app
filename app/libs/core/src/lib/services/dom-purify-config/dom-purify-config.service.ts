import { Injectable } from '@angular/core';
import DOMPurify from 'dompurify';
import { DomPurifyConfigType } from '../../enums/dom-purify-config-type.enum';

/* eslint @typescript-eslint/naming-convention : 0 */

@Injectable()
export class DomPurifyConfigService {
  private readonly configs: Record<DomPurifyConfigType, DOMPurify.Config> = {
    [DomPurifyConfigType.title]: {
      ALLOWED_TAGS: [],
      ALLOWED_ATTR: [],
    },
    [DomPurifyConfigType.description]: {
      ALLOWED_TAGS: [
        'i',
        'b',
        'strong',
        'p',
        'code',
        'pre',
        'a',
        'br',
        'ul',
        'ol',
        'li',
      ],
      ALLOWED_ATTR: ['href', 'target'],
    },
    [DomPurifyConfigType.basic]: {
      ALLOWED_TAGS: ['i', 'b', 'strong', 'p', 'code', 'pre', 'a', 'br'],
      ALLOWED_ATTR: ['href', 'target'],
    },
  };

  getConfig(type: DomPurifyConfigType): DOMPurify.Config {
    const config = this.configs[type];
    this.addTelLinkHook();
    return config;
  }

  private addTelLinkHook() {
    DOMPurify.addHook('beforeSanitizeElements', (node) => {
      if (
        node instanceof HTMLAnchorElement &&
        node.getAttribute('href')?.startsWith('tel:')
      ) {
        const textNode = document.createTextNode(node.textContent ?? '');
        node.parentNode?.replaceChild(textNode, node);
      }
    });
  }
}
