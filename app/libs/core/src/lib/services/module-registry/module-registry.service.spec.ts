import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { createModuleInfo } from '../../test/shared/test.util';
import {
  FeatureModule,
  FeedResult,
} from '../../interfaces/feature-module.interface';
import { FeedItem } from '../../models/feed-item.model';
import { MenuItem, ModuleCategory, ModuleInfo } from '../../models/menu.model';
import { ModuleRegistryService } from './module-registry.service';
import { SettingsPersistenceService } from '../settings-persistence/settings-persistence-service';
import { StorageKeyType } from '../../enums/storage-key.type';
import { Component } from '@angular/core';

@Component({
  selector: 'lib-mock-component1',
  template: '<p>Mock Component1</p>',
})
export class MockOnboardingComponent {}

@Component({
  selector: 'lib-mock-component2',
  template: '<p>Mock Component2</p>',
})
export class MockOnboarding2Component {}

describe('ModuleRegistryService', () => {
  let service: ModuleRegistryService;
  let mockSettingsPersistenceService: jest.Mocked<SettingsPersistenceService>;
  const feedItemMock: FeedItem[] = [{ creationDate: new Date() } as FeedItem];
  const routeMock = { path: 'test' };
  const mockModuleInfo: ModuleInfo = createModuleInfo({
    name: 'testModule',
    category: ModuleCategory.information,
    icon: 'icon',
    baseRoutingPath: '/baseRoutingPath',
    defaultVisibility: true,
    settingsPageAvailable: true,
  });
  const mockMenuItem: MenuItem = {
    title: 'menuItemTitle',
    icon: 'menuItemIcon',
    baseRoutingPath: '/menuItemBaseRoutingPath',
  };

  const feedResultMock: FeedResult<any> = {
    items: feedItemMock,
    totalRecords: feedItemMock.length,
  };

  const featureModuleMock1: FeatureModule<any> = {
    info: mockModuleInfo,
    feedables: () => of(feedResultMock),
    feedablesUpcoming: () => of(feedResultMock),
    feedablesUnfiltered: () => of(feedResultMock),
    feedablesActivity: () => of(feedResultMock),
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    initializeDefaultSettings: () => {},
    routes: [routeMock],
    onboardingComponent: MockOnboardingComponent,
  };

  const featureModuleMock2: FeatureModule<any> = {
    info: mockModuleInfo,
    feedables: () => of(feedResultMock),
    feedablesUpcoming: () => of(feedResultMock),
    feedablesUnfiltered: () => of(feedResultMock),
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    initializeDefaultSettings: () => {},
    routes: [routeMock],
    onboardingComponent: MockOnboarding2Component,
  };

  beforeEach(() => {
    mockSettingsPersistenceService = {
      getSettingsValues: jest.fn().mockResolvedValue([false]),
      saveSettings: jest.fn(),
    } as any;

    TestBed.configureTestingModule({
      providers: [
        ModuleRegistryService,
        {
          provide: SettingsPersistenceService,
          useValue: mockSettingsPersistenceService,
        },
      ],
    });

    service = TestBed.inject(ModuleRegistryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('registerFeature', () => {
    it('should add a feature to features array', async () => {
      await service.registerFeature(featureModuleMock1);
      expect(service.features.length).toBe(1);
    });
  });

  describe('getModuleInfos', () => {
    it('should return array of module infos', async () => {
      await service.registerFeature(featureModuleMock1);
      const moduleInfos = service.getModuleInfos();
      expect(moduleInfos[0].name).toBe('testModule');
    });
  });

  describe('getFeedables', () => {
    it('should return concatenated feed items', (done) => {
      service.registerFeature(featureModuleMock1).then(() => {
        service.getFeedables(0).subscribe((feedItems) => {
          expect(feedItems).toEqual(feedItemMock);
          done();
        });
      });
    });
  });

  describe('getFeedablesUpcoming', () => {
    it('should return concatenated upcoming feed items', (done) => {
      service.registerFeature(featureModuleMock1).then(() => {
        service.getFeedablesUpcoming(0).subscribe({
          next: (feedItems) => {
            expect(feedItems).toEqual(feedItemMock);
            done();
          },
          error: done,
        });
      });
    });
  });

  describe('getFeedablesUnfiltered', () => {
    it('should return concatenated unfiltered feed items', (done) => {
      service
        .registerFeature(featureModuleMock1)
        .then(() => {
          service.getFeedablesUnfiltered(0).subscribe({
            next: (feedItems) => {
              expect(feedItems).toEqual(feedItemMock);
              done();
            },
            error: (err) => {
              done(err);
            },
          });
        })
        .catch(done);
    });
  });

  describe('getFeatureRoutes', () => {
    it('should return concatenated feature routes', async () => {
      await service.registerFeature(featureModuleMock1);

      const featureRoutes = service.getFeatureRoutes();
      expect(featureRoutes[0].path).toBe('test');
    });
  });

  describe('getOnboardingComponents', () => {
    it('should return an array of onboarding component types along with module info', async () => {
      await service.registerFeature(featureModuleMock1);
      await service.registerFeature(featureModuleMock2);
      const onboardingComponents = service.getOnboardingComponents();
      expect(onboardingComponents).toEqual([
        {
          component: MockOnboardingComponent,
          moduleInfo: mockModuleInfo,
          isLaterSelected: false,
          isActivateSelected: true,
        },
        {
          component: MockOnboarding2Component,
          moduleInfo: mockModuleInfo,
          isLaterSelected: false,
          isActivateSelected: true,
        },
      ]);
    });
  });

  describe('getMatchingModuleInfo', () => {
    it('should return matching module info', async () => {
      await service.registerFeature(featureModuleMock1);
      await service.registerFeature(featureModuleMock2);

      const moduleInfo = service.getMatchingModuleInfo(
        service.getModuleInfos(),
        '/baseRoutingPath',
      );
      expect(moduleInfo).toEqual(mockModuleInfo);
    });

    it('should return undefined if no matching module info is found', async () => {
      await service.registerFeature(featureModuleMock1);
      await service.registerFeature(featureModuleMock2);

      const moduleInfo = service.getMatchingModuleInfo(
        service.getModuleInfos(),
        '/notMatchingPath',
      );
      expect(moduleInfo).toEqual(undefined);
    });
  });

  describe('getModuleVisibility', () => {
    it('should return true when module is visible', async () => {
      service.getMatchingModuleInfo = jest.fn().mockReturnValue(mockModuleInfo);
      mockSettingsPersistenceService.getSettingsValues = jest
        .fn()
        .mockResolvedValue([true]);

      const result = await service.getModuleVisibility([], mockMenuItem);
      expect(result).toBe(true);
    });

    it('should return false when module is not visible', async () => {
      service.getMatchingModuleInfo = jest.fn().mockReturnValue(mockModuleInfo);
      mockSettingsPersistenceService.getSettingsValues = jest
        .fn()
        .mockResolvedValue([false]);

      const result = await service.getModuleVisibility([], mockMenuItem);
      expect(result).toBe(false);
    });

    it('should return false when no matching module is found', async () => {
      service.getMatchingModuleInfo = jest.fn().mockReturnValue(undefined);

      const result = await service.getModuleVisibility([], mockMenuItem);
      expect(result).toBe(false);
    });
  });

  describe('saveModuleSettings', () => {
    it('should call saveSettings of settingsPersistenceService', async () => {
      await service.saveModuleVisibilitySettings(mockModuleInfo, true);
      expect(mockSettingsPersistenceService.saveSettings).toHaveBeenCalledWith(
        mockModuleInfo,
        StorageKeyType.MODULE_VISIBILITY,
        {
          [mockModuleInfo.baseRoutingPath]: true,
        },
      );
    });
  });

  describe('getFeedablesActivity', () => {
    it('should return feed items', (done) => {
      service
        .registerFeature(featureModuleMock1)
        .then(() => {
          service.getFeedablesUnfiltered(0).subscribe({
            next: (feedItems) => {
              expect(feedItems).toEqual(feedItemMock);
              done();
            },
            error: (err) => {
              done(err);
            },
          });
        })
        .catch(done);
    });
  });
});
