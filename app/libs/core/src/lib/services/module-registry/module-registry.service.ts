import { inject, Injectable, Type } from '@angular/core';
import { Routes } from '@angular/router';
import {
  BehaviorSubject,
  concatMap,
  forkJoin,
  from,
  map,
  Observable,
  of,
  reduce,
  toArray,
} from 'rxjs';
import { filter } from 'rxjs/operators';
import {
  FeatureModule,
  FeedResult,
  FeedUserSettings,
  ModuleFullSettings,
} from '../../interfaces/feature-module.interface';
import { SettingsPersistenceService } from '../settings-persistence/settings-persistence-service';
import { MenuItem, ModuleInfo } from '../../models/menu.model';
import { OnboardingComponentInfo } from '../../interfaces/onboarding-component-info.interface';
import { FeedItem } from '../../models/feed-item.model';
import { StorageKeyType } from '../../enums/storage-key.type';

@Injectable()
export class ModuleRegistryService {
  features: FeatureModule<any>[] = [];
  private readonly settingsPersistenceService = inject(
    SettingsPersistenceService,
  );
  private visibilitySettingsSubjects: {
    [moduleId: string]: BehaviorSubject<boolean>;
  } = {};

  async registerFeature(featureModule: FeatureModule<any>) {
    const moduleVisibility = await this.getValuesModuleVisibilitySettings(
      featureModule.info,
    );
    if (moduleVisibility.length === 0) {
      await this.saveModuleVisibilitySettings(
        featureModule.info,
        featureModule.info.defaultVisibility,
      );
    }
    this.features.push(featureModule);
  }

  getFeatureModules(): FeatureModule<any>[] {
    return this.features;
  }

  getById(id: string): FeatureModule<any> | undefined {
    return this.features.find((feature) => feature.info.id === id);
  }

  getModulesByCustomMapButton(): (FeatureModule<any> & {
    getCustomMapButton: () => Observable<{ component: Type<any> }>;
  })[] {
    return this.features.filter(
      (
        feature,
      ): feature is FeatureModule<any> & {
        getCustomMapButton: () => Observable<{ component: Type<any> }>;
      } => feature.getCustomMapButton !== undefined,
    );
  }

  getModuleInfos(filterFn?: (moduleInfo: ModuleInfo) => boolean): ModuleInfo[] {
    let moduleInfos = this.features.map((feature) => feature.info);

    if (filterFn) {
      moduleInfos = moduleInfos.filter(filterFn);
    }

    return moduleInfos;
  }

  getAllUserSettings(): Observable<{ [moduleName: string]: FeedUserSettings }> {
    return from(this.features).pipe(
      concatMap((feature) => this.getUserSettingsForFeature(feature)),
      reduce(
        (acc, settings) => {
          const moduleName = Object.keys(settings)[0];
          acc[moduleName] = settings[moduleName];
          return acc;
        },
        {} as { [moduleName: string]: FeedUserSettings },
      ),
    );
  }

  getAllModuleFullSettings(): Observable<ModuleFullSettings[]> {
    return from(this.features).pipe(
      concatMap((feature) => {
        const userSettings$ = this.getUserSettingsObservable(feature);

        const visibility$ = from(
          this.getValuesModuleVisibilitySettings(feature.info),
        ).pipe(
          map((visibilityValues) =>
            visibilityValues.length > 0
              ? visibilityValues[0]
              : feature.info.defaultVisibility,
          ),
        );

        return forkJoin([userSettings$, visibility$]).pipe(
          map(([userSetting, isVisible]) => ({
            ...userSetting,
            isVisible,
          })),
        );
      }),
      toArray(),
      map((modules) =>
        modules.filter(
          (m): m is ModuleFullSettings =>
            m.isVisible && m.feedItemType.trim() !== '',
        ),
      ),
    );
  }

  getMatchingModuleInfo(
    moduleInfos: ModuleInfo[],
    baseRoutingPath: string,
  ): ModuleInfo | undefined {
    return moduleInfos.find(
      (moduleInfo) => moduleInfo.baseRoutingPath === baseRoutingPath,
    );
  }

  async initializeDefaultModuleSettings() {
    for (const feature of this.features) {
      feature.initializeDefaultSettings();

      const moduleInfo = feature.info;
      const currentVisibility =
        await this.getValuesModuleVisibilitySettings(moduleInfo);

      if (currentVisibility.length === 0) {
        await this.saveModuleVisibilitySettings(
          moduleInfo,
          moduleInfo.defaultVisibility,
        );
      }
    }
  }

  getOnboardingComponents(): OnboardingComponentInfo[] {
    return this.features
      .filter((feature) => feature.onboardingComponent != null)
      .map((feature) => ({
        component: feature.onboardingComponent,
        moduleInfo: feature.info,
        isLaterSelected: !feature.info.defaultVisibility,
        isActivateSelected: feature.info.defaultVisibility,
      }));
  }

  getFeedables(page: number): Observable<FeedItem[]> {
    return from(this.features).pipe(
      concatMap((feature: FeatureModule<any>) =>
        feature.feedables
          ? feature.feedables(page)
          : from([
              {
                items: [],
                totalRecords: 0,
              },
            ]),
      ),
      reduce(
        (acc: FeedItem[], feedResult: FeedResult<any>) => [
          ...acc,
          ...feedResult.items,
        ],
        [],
      ),
    );
  }

  getFeedablesUpcoming(page: number): Observable<FeedItem[]> {
    return from(this.features).pipe(
      filter(
        (feature: FeatureModule<any>) =>
          feature.feedablesUpcoming !== undefined,
      ),
      concatMap((feature: FeatureModule<any>) =>
        feature.feedablesUpcoming
          ? feature.feedablesUpcoming(page)
          : from([
              {
                items: [],
                totalRecords: 0,
              },
            ]),
      ),
      reduce(
        (acc: FeedItem[], feedResult: FeedResult<any>) => [
          ...acc,
          ...feedResult.items,
        ],
        [],
      ),
    );
  }

  getFeedablesUnfiltered(page: number): Observable<FeedItem[]> {
    return from(this.features).pipe(
      concatMap((feature: FeatureModule<any>) =>
        feature.feedablesUnfiltered
          ? feature.feedablesUnfiltered(page)
          : from([
              {
                items: [],
                totalRecords: 0,
              },
            ]),
      ),
      reduce(
        (acc: FeedItem[], feedResult: FeedResult<any>) => [
          ...acc,
          ...feedResult.items,
        ],
        [],
      ),
    );
  }

  getFeedablesActivity(page: number): Observable<FeedItem[]> {
    return from(this.features).pipe(
      concatMap((feature: FeatureModule<any>) =>
        feature.feedablesActivity
          ? feature.feedablesActivity(page)
          : from([
              {
                items: [],
                totalRecords: 0,
              },
            ]),
      ),
      reduce(
        (acc: FeedItem[], feedResult: FeedResult<any>) => [
          ...acc,
          ...feedResult.items,
        ],
        [],
      ),
    );
  }

  getPriorityFeedables(): Observable<FeedItem[]> {
    return from(this.features).pipe(
      concatMap((feature: FeatureModule<any>) =>
        feature.getPriorityFeedables
          ? feature.getPriorityFeedables()
          : from([
              {
                items: [],
                totalRecords: 0,
              },
            ]),
      ),
      reduce(
        (acc: FeedItem[], feedResult: FeedResult<any>) => [
          ...acc,
          ...feedResult.items,
        ],
        [],
      ),
    );
  }

  getHighlightedFeedables(page: number): Observable<FeedItem[]> {
    return from(this.features).pipe(
      concatMap((feature: FeatureModule<any>) =>
        feature.feedablesHighlighted
          ? feature.feedablesHighlighted(page)
          : from([
              {
                items: [],
                totalRecords: 0,
              },
            ]),
      ),
      reduce(
        (acc: FeedItem[], feedResult: FeedResult<any>) => [
          ...acc,
          ...feedResult.items,
        ],
        [],
      ),
    );
  }

  getFeatureRoutes(): Routes {
    return this.features.reduce(
      (acc: Routes, feature: FeatureModule<any>) => acc.concat(feature.routes),
      [],
    );
  }

  async getModuleVisibility(
    moduleInfos: ModuleInfo[],
    menuItem: MenuItem,
  ): Promise<boolean> {
    const matchingModuleInfo = this.getMatchingModuleInfo(
      moduleInfos,
      menuItem.baseRoutingPath,
    );
    if (matchingModuleInfo) {
      const settingsValues =
        await this.settingsPersistenceService.getSettingsValues(
          matchingModuleInfo,
          StorageKeyType.MODULE_VISIBILITY,
        );

      const booleanSettingsValues = settingsValues.filter(
        (value): value is boolean => typeof value === 'boolean',
      );

      if (booleanSettingsValues.length > 0) {
        return booleanSettingsValues[0];
      } else {
        return matchingModuleInfo.defaultVisibility;
      }
    } else {
      console.error(`Module ${menuItem.baseRoutingPath} cannot be found`);
      return false;
    }
  }

  getModuleVisibilityWithUpdates(moduleInfo: ModuleInfo): Observable<boolean> {
    if (!this.visibilitySettingsSubjects[moduleInfo.id]) {
      this.visibilitySettingsSubjects[moduleInfo.id] =
        new BehaviorSubject<boolean>(moduleInfo.defaultVisibility);

      this.getValuesModuleVisibilitySettings(moduleInfo).then(
        (settingsValues) => {
          const isVisible =
            settingsValues.length > 0
              ? settingsValues[0]
              : moduleInfo.defaultVisibility;
          this.visibilitySettingsSubjects[moduleInfo.id].next(isVisible);
        },
      );
    }

    return this.visibilitySettingsSubjects[moduleInfo.id].asObservable();
  }

  async getValuesModuleVisibilitySettings(
    moduleInfo: ModuleInfo,
  ): Promise<boolean[]> {
    const settingsValues =
      await this.settingsPersistenceService.getSettingsValues(
        moduleInfo,
        StorageKeyType.MODULE_VISIBILITY,
      );

    return settingsValues.filter(
      (value): value is boolean => typeof value === 'boolean',
    );
  }

  async getAllModuleVisibilitySettings(moduleInfo: ModuleInfo): Promise<any> {
    return await this.settingsPersistenceService.getSettings(
      moduleInfo,
      StorageKeyType.MODULE_VISIBILITY,
    );
  }

  async saveModuleVisibilitySettings(
    moduleInfo: ModuleInfo,
    isVisible: boolean,
  ) {
    await this.settingsPersistenceService.saveSettings(
      moduleInfo,
      StorageKeyType.MODULE_VISIBILITY,
      { [moduleInfo.baseRoutingPath]: isVisible },
    );

    if (this.visibilitySettingsSubjects[moduleInfo.id]) {
      this.visibilitySettingsSubjects[moduleInfo.id].next(isVisible);
    }
  }

  getByBaseRoutingPath(baseRoutingPath: string): ModuleInfo | undefined {
    return this.features?.find(
      (feature) => feature.info.baseRoutingPath === baseRoutingPath,
    )?.info;
  }

  private getUserSettingsForFeature(
    feature: FeatureModule<any>,
  ): Observable<{ [id: string]: FeedUserSettings }> {
    return feature.getUserSettings
      ? from(feature.getUserSettings()).pipe(
          map((settings) => ({ [feature.info.id]: settings })),
        )
      : of({ [feature.info.id]: { sources: [], categoryIds: [] } });
  }

  private getUserSettingsObservable(
    feature: FeatureModule<any>,
  ): Observable<ModuleFullSettings> {
    if (feature.getUserSettings) {
      return from(feature.getUserSettings()).pipe(
        map((settings: FeedUserSettings) => ({
          moduleName: feature.info.id,
          settings,
          isVisible: false,
          feedItemType: feature.feedItemType ?? '',
        })),
      );
    } else {
      return of({
        moduleName: feature.info.id,
        settings: { sources: [], categoryIds: [] },
        isVisible: false,
        feedItemType: feature.feedItemType ?? '',
      });
    }
  }
}
