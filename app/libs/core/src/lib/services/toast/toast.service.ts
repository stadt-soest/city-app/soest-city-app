import { inject, Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular/standalone';

@Injectable()
export class ToastService {
  private readonly toastController = inject(ToastController);

  async presentToast(
    message: string,
    duration = 3000,
    position: ToastPosition = 'top',
    color = 'danger',
  ) {
    const toast = await this.toastController.create({
      mode: 'ios',
      message,
      duration,
      position,
      color,
    });
    await toast.present();
  }
}

type ToastPosition = 'top' | 'bottom' | 'middle';
