import { inject, Injectable } from '@angular/core';
import { MatomoTracker } from 'ngx-matomo-client';

@Injectable()
export class PrivacyService {
  private readonly matomoTracker = inject(MatomoTracker);

  activateTracking(): void {
    this.matomoTracker.forgetUserOptOut();
  }

  deactivateTracking(): void {
    this.matomoTracker.optUserOut();
  }

  async isTrackingActive(): Promise<boolean> {
    return !(await this.matomoTracker.isUserOptedOut());
  }
}
