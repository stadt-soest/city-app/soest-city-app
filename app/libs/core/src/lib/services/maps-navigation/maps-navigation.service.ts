import { inject, Injectable } from '@angular/core';
import { ActionSheetController } from '@ionic/angular/standalone';
import { TranslocoService } from '@jsverse/transloco';
import { BrowserService } from '../browser/browser.service';
import { DeviceService } from '../device/device.service';
import { MapsNavigationApp } from '../../models/maps-navigation.model';

@Injectable()
export class MapsNavigationService {
  private readonly deviceService = inject(DeviceService);
  private readonly browserService = inject(BrowserService);
  private readonly translateService = inject(TranslocoService);
  private readonly actionSheetController = inject(ActionSheetController);

  async navigateToLocation(latitude: number, longitude: number): Promise<void> {
    if (this.deviceService.isWebPlatform()) {
      this.navigateToLocationWeb(latitude, longitude);
    } else {
      await this.openNavigationAppSelection(latitude, longitude);
    }
  }

  async getInstalledNativeNavigationApps(): Promise<MapsNavigationApp[]> {
    const installedApps: MapsNavigationApp[] = [];

    for (const navigationApp of Object.values(MapsNavigationApp)) {
      const urlForApp: string = this.buildNavigationUrlForApp(
        navigationApp,
        51.570049,
        8.10829,
      );
      if (await this.browserService.canOpenApp(urlForApp)) {
        installedApps.push(navigationApp);
      }
    }

    return installedApps;
  }

  private async openNavigation(
    latitude: number,
    longitude: number,
    chosenApp?: MapsNavigationApp,
  ): Promise<void> {
    if (this.deviceService.isAndroid()) {
      const androidGeoUrl = `geo:0,0?q=${latitude},${longitude}`;
      await this.browserService.openInExternalBrowser(androidGeoUrl);
    } else if (this.deviceService.isWebPlatform() || !chosenApp) {
      const googleMapsUrl = `https://www.google.com/maps/search/?api=1&query=${latitude},${longitude}`;
      window.open(googleMapsUrl, '_blank');
    } else {
      const navigationUrl: string = this.buildNavigationUrlForApp(
        chosenApp,
        latitude,
        longitude,
      );
      this.browserService.openInExternalBrowser(navigationUrl).catch((err) => {
        console.error('Could not open the navigation app:', err);
      });
    }
  }

  private navigateToLocationWeb(latitude: number, longitude: number) {
    this.openNavigation(latitude, longitude);
  }

  private async openNavigationAppSelection(
    latitude: number,
    longitude: number,
  ): Promise<void> {
    const installedNavigationApps: MapsNavigationApp[] =
      await this.getInstalledNativeNavigationApps();

    if (installedNavigationApps.length === 0) {
      this.navigateToLocationWeb(latitude, longitude);
      return;
    }

    if (installedNavigationApps.length === 1) {
      this.openNavigation(latitude, longitude, installedNavigationApps[0]);
      return;
    }

    const actionSheet: HTMLIonActionSheetElement =
      await this.actionSheetController.create({
        header: this.translateService.translate(
          'ui.maps_navigation.choose_app_title',
        ),
        mode: 'ios',
        buttons: [
          ...installedNavigationApps.map(
            (navigationApp: MapsNavigationApp) => ({
              text: this.translateService.translate(
                `ui.maps_navigation.apps.${navigationApp}`,
              ),
              handler: () => {
                this.openNavigation(latitude, longitude, navigationApp);
              },
            }),
          ),
          {
            text: this.translateService.translate(
              'ui.maps_navigation.choose_app_cancel',
            ),
            role: 'cancel',
            data: {
              action: 'cancel',
            },
          },
        ],
      });
    await actionSheet.present();
  }

  private buildNavigationUrlForApp(
    chosenApp: MapsNavigationApp,
    latitude: number,
    longitude: number,
  ): string {
    switch (chosenApp) {
      case MapsNavigationApp.APPLE_MAPS:
        return `https://maps.apple.com/?daddr=${latitude},${longitude}`;
      case MapsNavigationApp.GOOGLE_MAPS:
        return `comgooglemaps://?daddr=${latitude},${longitude}&directionsmode=driving`;
      case MapsNavigationApp.WAZE:
        return `waze://?ll=${latitude},${longitude}&navigate=yes`;
    }
  }
}
