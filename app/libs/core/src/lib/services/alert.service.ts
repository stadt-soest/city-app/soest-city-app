import { inject, Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular/standalone';
import { TranslocoService } from '@jsverse/transloco';
import { firstValueFrom } from 'rxjs';
import { DeviceService } from './device/device.service';

@Injectable()
export class AlertService {
  private readonly alertController = inject(AlertController);
  private readonly translocoService = inject(TranslocoService);
  private readonly deviceService = inject(DeviceService);

  async presentAlert(
    headerKey: string,
    messageKey?: string,
    cancelKey?: string,
    confirmKey?: string,
    onConfirm?: () => void,
  ): Promise<void> {
    const header = await this.getTranslation(headerKey);
    const message = await this.getTranslation(messageKey);
    const cancelText = await this.getTranslation(cancelKey);
    const confirmText = await this.getTranslation(confirmKey);

    const buttons = [];

    if (cancelText) {
      buttons.push({
        text: cancelText,
        role: 'cancel',
      });
    }

    if (confirmText) {
      buttons.push({
        text: confirmText,
        handler: onConfirm,
      });
    }

    const alert = await this.alertController.create({
      mode: 'ios',
      header,
      message,
      buttons,
    });
    await alert.present();
  }

  async showLocationPermissionAlert(): Promise<void> {
    await this.presentAlert(
      'ui.alert.location.header',
      'ui.alert.location.message',
      'ui.alert.cancel',
      'ui.alert.open_settings',
      () => this.deviceService.openNativeAppSettings(),
    );
  }

  async showNotificationPermissionAlert(): Promise<void> {
    await this.presentAlert(
      'ui.alert.notification.header',
      'ui.alert.notification.message',
      'ui.alert.cancel',
      'ui.alert.open_settings',
      () => this.deviceService.openNativeAppSettings(),
    );
  }

  async showCalendarPermissionAlert(): Promise<void> {
    await this.presentAlert(
      'ui.alert.calendar.header',
      'ui.alert.calendar.message',
      'ui.alert.cancel',
      'ui.alert.open_settings',
      () => this.deviceService.openNativeAppSettings(),
    );
  }

  private async getTranslation(key?: string): Promise<string | undefined> {
    return key
      ? await firstValueFrom(this.translocoService.selectTranslate(key))
      : undefined;
  }
}
