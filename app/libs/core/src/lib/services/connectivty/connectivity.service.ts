import { Injectable } from '@angular/core';
import { ConnectionStatus, Network } from '@capacitor/network';
import { Observable, ReplaySubject } from 'rxjs';

@Injectable()
export class ConnectivityService {
  private readonly networkStatus: ReplaySubject<ConnectionStatus> =
    new ReplaySubject<ConnectionStatus>(1);

  public getNetworkStatus(): Observable<ConnectionStatus> {
    return this.networkStatus.asObservable();
  }

  public async checkOnlineStatus(): Promise<boolean> {
    const status = await Network.getStatus();
    return status.connected;
  }

  async initializeNetworkStatus(): Promise<void> {
    const status = await Network.getStatus();
    this.networkStatus.next(status);

    await Network.addListener(
      'networkStatusChange',
      (connectionStatus: ConnectionStatus) => {
        this.networkStatus.next(connectionStatus);
      },
    );
  }
}
