import { inject, Injectable } from '@angular/core';
import { CORE_MODULE_CONFIG } from '../core-module.config';

@Injectable({ providedIn: 'root' })
export class TranslocoConfigService {
  private readonly config = inject(CORE_MODULE_CONFIG);

  getTranslocoConfig() {
    return {
      availableLangs: this.config.translocoLanguages ?? ['de', 'en'],
      defaultLang: this.config.translocoDefaultLanguage ?? 'de',
      reRenderOnLangChange: true,
      prodMode: this.config.production,
    };
  }
}
