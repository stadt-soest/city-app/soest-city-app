import { StorageMigrationService } from './storage-migration.service';
import { TestBed } from '@angular/core/testing';
import { StorageService } from '../storage/storage.service';

const mockStorageData: { [key: string]: any } = { dataVersion: '0' };
const mockStorageService = {
  get: jest.fn((key: string) => Promise.resolve(mockStorageData[key])),
  set: jest.fn((key: string, value: any) => {
    mockStorageData[key] = value;
    return Promise.resolve();
  }),
  remove: jest.fn((key: string) => {
    delete mockStorageData[key];
    return Promise.resolve();
  }),
};

describe('StorageMigrationService', () => {
  let service: StorageMigrationService;

  beforeEach(() => {
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      providers: [
        StorageMigrationService,
        { provide: StorageService, useValue: mockStorageService },
      ],
    });

    service = TestBed.inject(StorageMigrationService);
  });

  it('should migrate data to version 1 if outdated', async () => {
    mockStorageService.get.mockImplementation((key: string) => {
      if (key === 'dataVersion') {
        return Promise.resolve(mockStorageData['dataVersion']);
      } else if (key === 'storageKey_CategorySettings_Neuigkeiten') {
        return Promise.resolve(JSON.stringify({ someData: 'oldData' }));
      }
      return Promise.resolve(null);
    });

    await service.migrateData();

    expect(mockStorageService.set).toHaveBeenCalledWith(
      'storageKey_CategorySettings_news',
      expect.any(String),
    );
    expect(mockStorageService.remove).toHaveBeenCalledWith(
      'storageKey_CategorySettings_Neuigkeiten',
    );
    expect(mockStorageService.set).toHaveBeenCalledWith('dataVersion', '1');
  });

  it('should not migrate if current version is up-to-date', async () => {
    mockStorageService.get.mockResolvedValueOnce('3');

    await service.migrateData();

    expect(mockStorageService.set).not.toHaveBeenCalled();
    expect(mockStorageService.remove).not.toHaveBeenCalled();
  });

  it('should not perform migration if old data is not present', async () => {
    mockStorageService.get.mockImplementation((key: string) => {
      if (key === 'dataVersion') {
        const version = mockStorageData[key];
        mockStorageData[key] = '1';
        return Promise.resolve(version);
      }
      return Promise.resolve(null);
    });

    await service.migrateData();

    expect(mockStorageService.set).not.toHaveBeenCalledWith(
      expect.not.stringContaining('dataVersion'),
      expect.anything(),
    );
    expect(mockStorageService.remove).not.toHaveBeenCalledWith(
      expect.anything(),
    );
    expect(mockStorageData['dataVersion']).toBe('1');
  });
});
