import { inject, Injectable } from '@angular/core';
import { StorageService } from '../storage/storage.service';

/*  eslint @typescript-eslint/naming-convention : 0 */

type KeyMapping = {
  [key: string]: string;
};

@Injectable()
export class StorageMigrationService {
  private readonly storageService = inject(StorageService);
  private readonly currentDataVersion = '3';

  private readonly migrations: { [key: string]: () => Promise<void> } = {
    0: this.migrateFrom0To1.bind(this),
    1: this.migrateFrom1To3.bind(this),
    2: this.migrateFrom2To3.bind(this),
  };

  async migrateData() {
    let currentVersion = await this.getCurrentDataVersion();

    while (currentVersion < this.currentDataVersion) {
      const migrationFunc = this.migrations[currentVersion];
      if (migrationFunc) {
        await migrationFunc();
        currentVersion = await this.getCurrentDataVersion();
      } else {
        break;
      }
    }
  }

  async getCurrentDataVersion(): Promise<string> {
    const version = await this.storageService.get('dataVersion');
    return version !== null ? version : '0';
  }

  private async updateDataVersion(newVersion: string) {
    await this.storageService.set('dataVersion', newVersion);
  }

  private async migrateFrom0To1() {
    const keyMapping: KeyMapping = {
      storageKey_ModuleVisibilitySettings_Neuigkeiten:
        'storageKey_ModuleVisibilitySettings_news',
      storageKey_CategorySettings_Neuigkeiten:
        'storageKey_CategorySettings_news',
      storageKey_ModuleVisibilitySettings_Veranstaltungen:
        'storageKey_ModuleVisibilitySettings_events',
      storageKey_SourceSettings_Neuigkeiten: 'storageKey_SourceSettings_events',
      storageKey_CategorySettings_Veranstaltungen:
        'storageKey_CategorySettings_events',
      'storageKey_ModuleVisibilitySettings_Bürgerbüro Online':
        'storageKey_ModuleVisibilitySettings_citizen-services',
    };

    for (const oldKey in keyMapping) {
      if (Object.prototype.hasOwnProperty.call(keyMapping, oldKey)) {
        const oldData = await this.storageService.get(oldKey);
        if (oldData) {
          const newKey = keyMapping[oldKey];
          await this.storageService.set(newKey, oldData);
          await this.storageService.remove(oldKey);
        }
      }
    }
    await this.updateDataVersion('1');
  }

  private async migrateFrom1To3() {
    await this.storageService.remove('storageKey_CategorySettings_events');
    await this.updateDataVersion('3');
  }

  private async migrateFrom2To3() {
    await this.updateDataVersion('3');
  }
}
