import { TestBed } from '@angular/core/testing';
import { CORE_MODULE_CONFIG, CoreModuleConfig } from '../../core-module.config';
import { Image } from '../../models/feed-item.model';
import { ImageService } from './image.service';

describe('ImageService', () => {
  let service: ImageService;

  const mockAppConfig: CoreModuleConfig = {
    apiBaseUrl: 'http://test-api.com',
  } as any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ImageService,
        { provide: CORE_MODULE_CONFIG, useValue: mockAppConfig },
      ],
    });

    service = TestBed.inject(ImageService);
  });

  describe('getImageUrl', () => {
    it('should return null when image is null', () => {
      expect(service.getImageUrl(null)).toBeNull();
    });

    it('should return null when image is undefined', () => {
      expect(service.getImageUrl(undefined)).toBeNull();
    });

    it('should return null when image.fileName is falsy', () => {
      const image: Image = { thumbnail: { filename: '' } } as Image;
      expect(service.getImageUrl(image)).toBeNull();
    });

    it('should return the correct URL when image.fileName is truthy', () => {
      const fileName = 'test.jpg';
      const image: Image = {
        thumbnail: { filename: fileName },
        highRes: { filename: fileName },
      } as Image;
      const expectedUrl = `${mockAppConfig.apiBaseUrl}/files/${fileName}`;

      expect(service.getImageUrl(image)).toBe(expectedUrl);
      expect(service.getHighResImageUrl(image)).toBe(expectedUrl);
    });
  });
});
