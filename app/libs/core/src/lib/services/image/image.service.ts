import { inject, Injectable } from '@angular/core';
import { CORE_MODULE_CONFIG } from '../../core-module.config';
import { Image } from '../../models/feed-item.model';

@Injectable()
export class ImageService {
  private readonly config = inject(CORE_MODULE_CONFIG);

  getImageUrl(image: Image | null | undefined): string | null {
    return image?.thumbnail?.filename
      ? this.config.apiBaseUrl + '/files/' + image.thumbnail.filename
      : null;
  }

  getHighResImageUrl(image: Image | null | undefined): string | null {
    return image?.highRes?.filename
      ? this.config.apiBaseUrl + '/files/' + image.highRes.filename
      : null;
  }
}
