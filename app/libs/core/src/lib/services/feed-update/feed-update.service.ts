import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class FeedUpdateService {
  private readonly feedUpdated = new Subject<void>();

  getFeedUpdatedListener(): Observable<void> {
    return this.feedUpdated.asObservable();
  }

  notifyFeedUpdate(): void {
    this.feedUpdated.next();
  }
}
