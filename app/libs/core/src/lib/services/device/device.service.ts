import { Device } from '@capacitor/device';
import { inject, Injectable } from '@angular/core';
import { App, AppInfo } from '@capacitor/app';
import { Capacitor } from '@capacitor/core';
import {
  AndroidSettings,
  IOSSettings,
  NativeSettings,
} from 'capacitor-native-settings';
import { DarkMode, DarkModeListenerData } from '@aparajita/capacitor-dark-mode';
import { SafeArea } from '@capacitor-community/safe-area';
import { SettingsPersistenceService } from '../settings-persistence/settings-persistence-service';
import { DeviceEnvironment } from '../../models/device-enviroment.model';

@Injectable()
export class DeviceService {
  private readonly settingsPersistenceService = inject(
    SettingsPersistenceService,
  );
  private platform = '';
  private readonly deviceInfo: DeviceEnvironment = new DeviceEnvironment();

  getOsName(): string {
    return this.deviceInfo.operatingSystemName;
  }

  isWebPlatform(): boolean {
    return this.platform === 'web';
  }

  isNativePlatform(): boolean {
    return Capacitor.isNativePlatform();
  }

  isIOS(): boolean {
    return Capacitor.getPlatform() === 'ios';
  }

  isAndroid() {
    return Capacitor.getPlatform() === 'android';
  }

  isMobileBrowser(): boolean {
    return /iPhone|iPad|Android/i.test(navigator.userAgent);
  }

  isPWA(): boolean {
    return (
      window.matchMedia('(display-mode: standalone)').matches ||
      // @ts-expect-error standalone is not typed in TypeScript but exists on some browsers
      !!window.navigator.standalone
    );
  }

  async getDeviceId(): Promise<string> {
    const deviceIdInfo = await Device.getId();
    return deviceIdInfo.identifier;
  }

  getDeviceInfo(): DeviceEnvironment {
    return this.deviceInfo;
  }

  getBrowserName(): string {
    if (navigator.userAgent.indexOf('Chrome') !== -1) {
      return 'Chrome';
    } else if (navigator.userAgent.indexOf('Opera') !== -1) {
      return 'Opera';
    } else if (navigator.userAgent.indexOf('Firefox') !== -1) {
      return 'Firefox';
    } else if (navigator.userAgent.indexOf('Safari') !== -1) {
      return 'Safari';
    } else {
      return 'Unknown';
    }
  }

  async openNativeAppSettings() {
    await NativeSettings.open({
      optionAndroid: AndroidSettings.ApplicationDetails,
      optionIOS: IOSSettings.App,
    });
  }

  async listenToNativeLightOrDarkModeChanges() {
    if (!this.isNativePlatform()) {
      return;
    }

    try {
      await DarkMode.setNativeDarkModeListener(
        {},
        async (result: DarkModeListenerData) => {
          await this.changeAndroidSystemBarColors(result.dark);
        },
      );
    } catch (err: any) {
      console.error('Could not listen to native dark mode changes', err);
    }
  }

  async init() {
    const info = await Device.getInfo();
    let appInfo: AppInfo | null = null;

    if (this.isNativePlatform()) {
      appInfo = await App.getInfo();
    }

    this.platform = info.platform;
    this.deviceInfo.deviceModelName = info.model ?? 'Unknown Model';
    this.deviceInfo.operatingSystemName =
      this.formatOsName(info.operatingSystem) ?? 'Unknown OS';
    this.deviceInfo.operatingSystemVersion =
      info.osVersion ?? 'Unknown Version';
    this.deviceInfo.appVersion = appInfo
      ? appInfo.version
      : 'Unknown App Version';
    this.deviceInfo.appSettings =
      await this.settingsPersistenceService.getStorage();

    if (this.platform === 'web') {
      this.deviceInfo.browserName = this.getBrowserName();
      this.deviceInfo.usedAs = 'web';
    } else {
      this.deviceInfo.usedAs = 'app';
    }
  }

  private async changeAndroidSystemBarColors(darkMode: boolean): Promise<void> {
    if (this.isAndroid()) {
      await SafeArea.enable({
        config: {
          customColorsForSystemBars: true,
          statusBarColor: darkMode ? '#343C38' : '#EEE7DC',
          statusBarContent: darkMode ? 'light' : 'dark',
          navigationBarColor: darkMode ? '#445551' : '#D8D0C5',
          navigationBarContent: darkMode ? 'light' : 'dark',
        },
      });
    }
  }

  private formatOsName(osName: string | undefined): string | undefined {
    if (osName === 'android') {
      return 'Android';
    } else if (osName === 'ios') {
      return 'iOS';
    } else {
      return osName;
    }
  }
}
