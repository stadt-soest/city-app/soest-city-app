import { TestBed } from '@angular/core/testing';
import { BrowserService } from './browser.service';
import { Browser } from '@capacitor/browser';
import { AppLauncher } from '@capacitor/app-launcher';
import { CoreTestModule } from '../../test/core-test.module';
import { DeviceService } from '../device/device.service';

jest.mock('@capacitor/browser', () => ({
  Browser: {
    open: jest.fn(),
    close: jest.fn(),
    addListener: jest.fn(),
    removeAllListeners: jest.fn(),
  },
}));

jest.mock('@capacitor/app-launcher', () => ({
  AppLauncher: {
    openUrl: jest.fn(),
  },
}));

describe('BrowserService', () => {
  let service: BrowserService;

  beforeEach(() => {
    const deviceServiceMock = {
      isWebPlatform: jest.fn(),
    } as unknown as jest.Mocked<DeviceService>;

    TestBed.configureTestingModule({
      imports: [CoreTestModule.forRoot()],
      providers: [
        BrowserService,
        { provide: DeviceService, useValue: deviceServiceMock },
      ],
    });

    service = TestBed.inject(BrowserService);

    jest.clearAllMocks();
    (AppLauncher.openUrl as jest.Mock).mockResolvedValue(undefined);
    (Browser.open as jest.Mock).mockResolvedValue(undefined);
    (Browser.close as jest.Mock).mockResolvedValue(undefined);
    (Browser.addListener as jest.Mock).mockResolvedValue({
      remove: jest.fn().mockResolvedValue(undefined),
    });
    (Browser.removeAllListeners as jest.Mock).mockResolvedValue(undefined);
  });

  it('should open a URL in the system browser', async () => {
    const testUrl = 'https://example.com';

    await service.openInExternalBrowser(testUrl);

    expect(AppLauncher.openUrl).toHaveBeenCalledWith({ url: testUrl });
  });

  it('should handle errors when opening a URL in the system browser', async () => {
    const testUrl = 'https://example.com';
    const testError = new Error('Test error');
    (AppLauncher.openUrl as jest.Mock).mockRejectedValueOnce(testError);

    await expect(service.openInExternalBrowser(testUrl)).rejects.toThrow(
      'Test error',
    );
    expect(AppLauncher.openUrl).toHaveBeenCalledWith({ url: testUrl });
  });

  it('should open a URL', async () => {
    const testUrl = 'https://example.com';

    await service.openInAppBrowser(testUrl);

    expect(Browser.open).toHaveBeenCalledWith({
      url: testUrl,
      windowName: '_system',
    });
  });

  it('should close the browser', async () => {
    await service.closeBrowser();
    expect(Browser.close).toHaveBeenCalled();
  });

  it('should add listener for browserFinished', async () => {
    const mockListener = {
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      remove: async () => {},
    };
    (Browser.addListener as jest.Mock).mockResolvedValueOnce(mockListener);
    const listener = await service.addListenerForBrowserFinished();
    expect(Browser.addListener).toHaveBeenCalledWith(
      'browserFinished',
      expect.any(Function),
    );
    expect(listener).toEqual(mockListener);
  });

  it('should add listener for browserPageLoaded', async () => {
    const mockListener = {
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      remove: async () => {},
    };
    (Browser.addListener as jest.Mock).mockResolvedValueOnce(mockListener);
    const listener = await service.addListenerForPageLoaded();
    expect(Browser.addListener).toHaveBeenCalledWith(
      'browserPageLoaded',
      expect.any(Function),
    );
    expect(listener).toEqual(mockListener);
  });

  it('should remove all listeners', async () => {
    await service.removeAllListeners();
    expect(Browser.removeAllListeners).toHaveBeenCalled();
  });

  it('should handle errors when opening a URL', async () => {
    const testUrl = 'https://example.com';
    const testError = new Error('Test error');
    (Browser.open as jest.Mock).mockRejectedValueOnce(testError);

    await expect(service.openInAppBrowser(testUrl)).rejects.toThrow(
      'Test error',
    );
    expect(Browser.open).toHaveBeenCalledWith({
      url: testUrl,
      windowName: '_system',
    });
  });

  it('should handle errors when closing the browser', async () => {
    const testError = new Error('Test error');
    (Browser.close as jest.Mock).mockRejectedValueOnce(testError);

    await expect(service.closeBrowser()).rejects.toThrow('Test error');
    expect(Browser.close).toHaveBeenCalled();
  });

  it('should handle errors when adding listener for browserFinished', async () => {
    const testError = new Error('Test error');
    (Browser.addListener as jest.Mock).mockRejectedValueOnce(testError);

    await expect(service.addListenerForBrowserFinished()).rejects.toThrow(
      'Test error',
    );
    expect(Browser.addListener).toHaveBeenCalledWith(
      'browserFinished',
      expect.any(Function),
    );
  });

  it('should handle errors when adding listener for browserPageLoaded', async () => {
    const testError = new Error('Test error');
    (Browser.addListener as jest.Mock).mockRejectedValueOnce(testError);

    await expect(service.addListenerForPageLoaded()).rejects.toThrow(
      'Test error',
    );
    expect(Browser.addListener).toHaveBeenCalledWith(
      'browserPageLoaded',
      expect.any(Function),
    );
  });

  it('should handle errors when removing all listeners', async () => {
    const testError = new Error('Test error');
    (Browser.removeAllListeners as jest.Mock).mockRejectedValueOnce(testError);

    await expect(service.removeAllListeners()).rejects.toThrow('Test error');
    expect(Browser.removeAllListeners).toHaveBeenCalled();
  });
});
