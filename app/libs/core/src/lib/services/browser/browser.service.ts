import { inject, Injectable } from '@angular/core';
import { Browser } from '@capacitor/browser';
import { PluginListenerHandle } from '@capacitor/core';
import { AppLauncher, CanOpenURLResult } from '@capacitor/app-launcher';
import { DeviceService } from '../device/device.service';

@Injectable()
export class BrowserService {
  private readonly deviceService = inject(DeviceService);

  async openInExternalBrowser(url: string): Promise<void> {
    try {
      await AppLauncher.openUrl({ url });
    } catch (err) {
      console.error('Error opening browser:', err);
      return Promise.reject(
        err instanceof Error ? err : new Error(String(err)),
      );
    }
  }

  async openInAppBrowser(url: string): Promise<void> {
    const openBrowser = this.isWebPlatform()
      ? this.openInExternalBrowser
      : this.openInSystemBrowser;
    await openBrowser.call(this, url);
  }

  async closeBrowser(): Promise<void> {
    try {
      await Browser.close();
    } catch (err) {
      console.error('Error closing browser:', err);
      return Promise.reject(
        err instanceof Error ? err : new Error(String(err)),
      );
    }
  }

  async addListenerForBrowserFinished(): Promise<PluginListenerHandle> {
    try {
      return await Browser.addListener('browserFinished', () => {
        console.log('Browser finished event');
      });
    } catch (err) {
      console.error('Error adding listener for browserFinished:', err);
      return Promise.reject(
        err instanceof Error ? err : new Error(String(err)),
      );
    }
  }

  async addListenerForPageLoaded(): Promise<PluginListenerHandle> {
    try {
      return await Browser.addListener('browserPageLoaded', () => {
        console.log('Page loaded event');
      });
    } catch (err) {
      console.error('Error adding listener for browserPageLoaded:', err);
      return Promise.reject(
        err instanceof Error ? err : new Error(String(err)),
      );
    }
  }

  async removeAllListeners(): Promise<void> {
    try {
      await Browser.removeAllListeners();
    } catch (err) {
      console.error('Error removing all listeners:', err);
      return Promise.reject(
        err instanceof Error ? err : new Error(String(err)),
      );
    }
  }

  async canOpenApp(url: string): Promise<boolean> {
    try {
      const canOpenResult: CanOpenURLResult = await AppLauncher.canOpenUrl({
        url,
      });
      return canOpenResult.value;
    } catch (err) {
      console.error('Error can open app:', err);
      return false;
    }
  }

  private isWebPlatform(): boolean {
    return this.deviceService.isWebPlatform();
  }

  private async openInSystemBrowser(url: string): Promise<void> {
    try {
      await Browser.open({
        url,
        windowName: '_system',
      });
    } catch (err) {
      console.error('Error opening URL:', err);
      return Promise.reject(
        err instanceof Error ? err : new Error(String(err)),
      );
    }
  }
}
