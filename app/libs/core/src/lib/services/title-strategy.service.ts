import { inject, Injectable } from '@angular/core';
import { RouterStateSnapshot, TitleStrategy } from '@angular/router';
import { TranslocoService } from '@jsverse/transloco';
import { take } from 'rxjs';
import { TitleService } from './title.service';

@Injectable()
export class TitleStrategyService extends TitleStrategy {
  private readonly translocoService = inject(TranslocoService);
  private readonly titleService = inject(TitleService);

  updateTitle(snapshot: RouterStateSnapshot): void {
    const title = this.buildTitle(snapshot);

    if (title) {
      this.setTranslatedTitle(title);
    }
  }

  private setTranslatedTitle(title: string): void {
    const scope = this.detectScopeFromTitle(title);

    this.translocoService
      .selectTranslate(
        scope ? this.removeScopeFromTitle(title) : title,
        {},
        scope ?? undefined,
      )
      .pipe(take(1))
      .subscribe((translatedTitle) => {
        this.titleService.setTitle(translatedTitle);
      });
  }

  private detectScopeFromTitle(title: string): string | null {
    const parts = title.split('.');
    const possibleScope = parts[0].replace(/_/g, '-');
    if (possibleScope.startsWith('feature-')) {
      return possibleScope;
    }

    return null;
  }

  private removeScopeFromTitle(title: string): string {
    return title.split('.').slice(1).join('.');
  }
}
