import { inject, Injectable } from '@angular/core';
import { NavController } from '@ionic/angular/standalone';
import { ROUTES } from '../../constants/routes.constant';
import { FeedItem } from '../../models/feed-item.model';

@Injectable()
export class NavigationService {
  private readonly navCtrl = inject(NavController);

  navigateToNotificationSettings(): Promise<boolean> {
    return this.navCtrl.navigateForward(ROUTES.notifications);
  }

  navigateToFeedItemSettings(menuItemBaseRoutePath: string): Promise<boolean> {
    return this.navCtrl.navigateForward(
      menuItemBaseRoutePath + ROUTES.settings,
    );
  }

  navigateToFeedItemDetails(feedItem: FeedItem): Promise<boolean> {
    return this.navCtrl.navigateForward(
      `${feedItem.baseRoutePath}${ROUTES.details}/${feedItem.id}/0`,
    );
  }

  navigateToForYou(replaceUrl?: boolean): Promise<boolean> {
    if (replaceUrl) {
      return this.navCtrl.navigateRoot(ROUTES.forYou);
    } else {
      return this.navCtrl.navigateForward(ROUTES.forYou);
    }
  }

  navigateToSearchTab(replaceUrl?: boolean): Promise<boolean> {
    if (replaceUrl) {
      return this.navCtrl.navigateRoot(ROUTES.search);
    } else {
      return this.navCtrl.navigateForward(ROUTES.search);
    }
  }

  navigateToSettingsMenu(): Promise<boolean> {
    return this.navCtrl.navigateForward(ROUTES.settingsMenu);
  }

  navigateToForyouSettings(): Promise<boolean> {
    return this.navCtrl.navigateForward(ROUTES.forYouSettings);
  }

  navigateToPermissionSettings(): Promise<boolean> {
    return this.navCtrl.navigateForward(ROUTES.permissions);
  }
}
