import { NavigationService } from './navigation.service';
import { TestBed } from '@angular/core/testing';
import { NavController } from '@ionic/angular/standalone';
import { FeedItem } from '../../models/feed-item.model';
import { ROUTES } from '../../constants/routes.constant';

describe('NavigationService', () => {
  let service: NavigationService;
  let mockController: jest.Mocked<NavController>;

  beforeEach(() => {
    mockController = {
      navigateForward: jest.fn(),
      navigateRoot: jest.fn(),
    } as any;

    TestBed.configureTestingModule({
      providers: [
        NavigationService,
        { provide: NavController, useValue: mockController },
      ],
    });

    service = TestBed.inject(NavigationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('navigateToNotificationSettings', () => {
    it('should navigate to notification settings', () => {
      const spy = jest.spyOn(mockController, 'navigateForward');
      service.navigateToNotificationSettings();
      expect(spy).toHaveBeenCalledWith(ROUTES.notifications);
    });
  });

  describe('navigateToFeedItemSettings', () => {
    it('should navigate to feed item settings', () => {
      const spy = jest.spyOn(mockController, 'navigateForward');
      const baseRoutePath = 'path/to/feed/item';
      service.navigateToFeedItemSettings(baseRoutePath);
      expect(spy).toHaveBeenCalledWith(baseRoutePath + ROUTES.settings);
    });
  });

  describe('navigateToFeedItemDetails', () => {
    it('should navigate to feed item details', () => {
      const spy = jest.spyOn(mockController, 'navigateForward');
      const feedItem: FeedItem = {
        id: '123',
        baseRoutePath: 'path/to/feed/item',
      } as FeedItem;
      service.navigateToFeedItemDetails(feedItem);
      expect(spy).toHaveBeenCalledWith(
        `${feedItem.baseRoutePath}${ROUTES.details}/${feedItem.id}/0`,
      );
    });
  });

  describe('navigateToForyouSettings', () => {
    it('should navigate to "for you" settings', () => {
      const spy = jest.spyOn(mockController, 'navigateForward');
      service.navigateToForyouSettings();
      expect(spy).toHaveBeenCalledWith(ROUTES.forYouSettings);
    });
  });

  describe('navigateToPermissionSettings', () => {
    it('should navigate to permission settings', () => {
      const spy = jest.spyOn(mockController, 'navigateForward');
      service.navigateToPermissionSettings();
      expect(spy).toHaveBeenCalledWith(ROUTES.permissions);
    });
  });

  describe('navigateToForYou', () => {
    it('should navigate to ForYou route with navigateForward when replaceUrl is false', () => {
      const spyNavigateForward = jest.spyOn(mockController, 'navigateForward');
      service.navigateToForYou(false);
      expect(spyNavigateForward).toHaveBeenCalledWith(ROUTES.forYou);
    });

    it('should navigate to ForYou route with navigateRoot when replaceUrl is true', () => {
      const spyNavigateRoot = jest.spyOn(mockController, 'navigateRoot');
      service.navigateToForYou(true);
      expect(spyNavigateRoot).toHaveBeenCalledWith(ROUTES.forYou);
    });
  });

  describe('navigateToSearchTab', () => {
    it('should navigate to SearchTab route with navigateForward when replaceUrl is false', () => {
      const spyNavigateForward = jest.spyOn(mockController, 'navigateForward');
      service.navigateToSearchTab(false);
      expect(spyNavigateForward).toHaveBeenCalledWith(ROUTES.search);
    });

    it('should navigate to SearchTab route with navigateRoot when replaceUrl is true', () => {
      const spyNavigateRoot = jest.spyOn(mockController, 'navigateRoot');
      service.navigateToSearchTab(true);
      expect(spyNavigateRoot).toHaveBeenCalledWith(ROUTES.search);
    });
  });

  describe('navigateToSettingsMenu', () => {
    it('should navigate to settings menu', () => {
      const spy = jest.spyOn(mockController, 'navigateForward');
      service.navigateToSettingsMenu();
      expect(spy).toHaveBeenCalledWith(ROUTES.settingsMenu);
    });
  });
});
