import { inject, Injectable } from '@angular/core';
import {
  MenuItem,
  MenuItemGroup,
  ModuleCategory,
  ModuleInfo,
} from '../../models/menu.model';
import { ModuleRegistryService } from '../module-registry/module-registry.service';

@Injectable()
export class MenuService {
  private readonly moduleRegistry = inject(ModuleRegistryService);

  getMenuItems(
    filterFn?: (moduleInfo: ModuleInfo) => boolean,
  ): MenuItemGroup[] {
    const moduleInfos = this.getFilteredModuleInfos(filterFn);
    const menuItemGroups = this.buildMenuItemGroups(moduleInfos);
    return this.sortMenuItemGroupsByCategory(menuItemGroups);
  }

  private getFilteredModuleInfos(
    filterFn?: (moduleInfo: ModuleInfo) => boolean,
  ): ModuleInfo[] {
    let moduleInfos = this.moduleRegistry.getModuleInfos();
    if (filterFn) {
      moduleInfos = moduleInfos.filter(filterFn);
    }
    return moduleInfos;
  }

  private buildMenuItemGroups(moduleInfos: ModuleInfo[]): MenuItemGroup[] {
    const menuItemGroupMap = new Map<ModuleCategory, MenuItemGroup>();

    moduleInfos.forEach((moduleInfo) => {
      if (!menuItemGroupMap.has(moduleInfo.category)) {
        menuItemGroupMap.set(
          moduleInfo.category,
          new MenuItemGroup(moduleInfo.category, []),
        );
      }
      const menuItemGroup = menuItemGroupMap.get(moduleInfo.category);
      menuItemGroup?.menuItems.push(
        new MenuItem(
          moduleInfo.name,
          moduleInfo.baseRoutingPath,
          moduleInfo.icon,
        ),
      );
    });

    const menuItemGroups = Array.from(menuItemGroupMap.values());

    menuItemGroups.forEach((group) => {
      this.sortMenuItemsInGroup(group);
    });

    return menuItemGroups;
  }

  private sortMenuItemsInGroup(group: MenuItemGroup) {
    group.menuItems.sort((a, b) => {
      const moduleInfoA = this.moduleRegistry.getByBaseRoutingPath(
        a.baseRoutingPath,
      );
      const moduleInfoB = this.moduleRegistry.getByBaseRoutingPath(
        b.baseRoutingPath,
      );

      if (!moduleInfoA || !moduleInfoB) {
        return 0;
      }

      return (
        (moduleInfoA?.orderInCategory ?? Number.MAX_SAFE_INTEGER) -
        (moduleInfoB?.orderInCategory ?? Number.MAX_SAFE_INTEGER)
      );
    });
  }

  private sortMenuItemGroupsByCategory(
    menuItemGroups: MenuItemGroup[],
  ): MenuItemGroup[] {
    const categoryOrder = Object.values(ModuleCategory);
    return menuItemGroups.sort(
      (a, b) =>
        categoryOrder.indexOf(a.category) - categoryOrder.indexOf(b.category),
    );
  }
}
