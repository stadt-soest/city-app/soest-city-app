import { MenuService } from './menu.service';
import { TestBed } from '@angular/core/testing';
import { createModuleInfo } from '../../test/shared/test.util';
import {
  MenuItem,
  MenuItemGroup,
  ModuleCategory,
  ModuleInfo,
} from '../../models/menu.model';
import { ModuleRegistryService } from '../module-registry/module-registry.service';

describe('MenuService', () => {
  let service: MenuService;
  let mockModuleRegistryService: jest.Mocked<ModuleRegistryService>;

  beforeEach(() => {
    mockModuleRegistryService = {
      registerFeature: jest.fn(),
      getModuleInfos: jest.fn(),
      getFeedables: jest.fn(),
      getFeatureRoutes: jest.fn(),
      getByBaseRoutingPath: jest.fn(),
    } as unknown as jest.Mocked<ModuleRegistryService>;

    TestBed.configureTestingModule({
      providers: [
        MenuService,
        { provide: ModuleRegistryService, useValue: mockModuleRegistryService },
      ],
    });

    service = TestBed.inject(MenuService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getMenuItems', () => {
    it('should return correct menu item groups', () => {
      const mockModuleInfos: ModuleInfo[] = [
        createModuleInfo({
          category: ModuleCategory.information,
          name: 'Name1',
          baseRoutingPath: 'Path1',
          icon: 'Icon1',
          orderInCategory: 1,
        }),
        createModuleInfo({
          category: ModuleCategory.yourCity,
          name: 'Name2',
          baseRoutingPath: 'Path2',
          icon: 'Icon2',
          orderInCategory: 2,
        }),
        createModuleInfo({
          category: ModuleCategory.information,
          name: 'Name3',
          baseRoutingPath: 'Path3',
          icon: 'Icon3',
          orderInCategory: 3,
        }),
      ];

      mockModuleRegistryService.getModuleInfos.mockReturnValue(mockModuleInfos);

      mockModuleRegistryService.getByBaseRoutingPath.mockImplementation(
        (path: string) =>
          mockModuleInfos.find(
            (moduleInfo) => moduleInfo.baseRoutingPath === path,
          ),
      );

      const expectedMenuItemGroups: MenuItemGroup[] = [
        new MenuItemGroup(ModuleCategory.information, [
          new MenuItem('Name1', 'Path1', 'Icon1'),
          new MenuItem('Name3', 'Path3', 'Icon3'),
        ]),
        new MenuItemGroup(ModuleCategory.yourCity, [
          new MenuItem('Name2', 'Path2', 'Icon2'),
        ]),
      ];

      mockModuleRegistryService.getModuleInfos.mockReturnValue(mockModuleInfos);
      const result = service.getMenuItems();
      expect(result).toEqual(expectedMenuItemGroups);
    });
  });
});
