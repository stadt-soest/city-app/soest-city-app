import { inject, Injectable } from '@angular/core';
import {
  forkJoin,
  from,
  map,
  mergeMap,
  Observable,
  switchMap,
  toArray,
} from 'rxjs';
import { filter } from 'rxjs/operators';
import { getDistance } from 'geolib';
import { Category } from '../../models/category.model';
import { FeedItem } from '../../models/feed-item.model';
import { ModuleRegistryService } from '../module-registry/module-registry.service';
import { CoreAdapter } from '../../core.adapter';
import { Poi, PoiWithDistance } from '../../models/poi.model';

@Injectable()
export class PoiService {
  private readonly coreAdapter = inject(CoreAdapter);
  private readonly moduleRegistryService = inject(ModuleRegistryService);

  searchPois(
    searchTerm: string,
    page?: number,
    size?: number,
  ): Observable<FeedItem[]> {
    return this.coreAdapter.searchPois(searchTerm, page, size);
  }

  getPoiById(id: string): Observable<Poi> {
    return this.coreAdapter.getPoiById(id);
  }

  getAllCategories(): Observable<Category[]> {
    return forkJoin({
      apiCategories: this.getCategories(),
      moduleCategories: this.getCategoriesFromModules(),
    }).pipe(
      map(({ apiCategories, moduleCategories }) =>
        [...apiCategories, ...moduleCategories].filter(
          (category) => category.groupCategory !== undefined,
        ),
      ),
    );
  }

  getAllPois(): Observable<Poi[]> {
    return this.getAllCategories().pipe(
      switchMap((categories) => this.fetchPoisForCategories(categories)),
    );
  }

  getPois(categoryIds: string[]): Observable<Poi[]> {
    return this.coreAdapter.getPois(categoryIds);
  }

  getPoisWithDistance(
    categoryIds: string[],
    currentLocation?: {
      lat: number;
      long: number;
    },
  ): Observable<PoiWithDistance[]> {
    return this.getPois(categoryIds).pipe(
      mergeMap((pois) => from(pois)),
      map((poi) => this.calculatePoiDistance(poi, currentLocation)),
      toArray(),
      map((poisWithDistance) => poisWithDistance.filter(Boolean)),
      map((poisWithDistance) =>
        [...poisWithDistance].sort(
          (a, b) => (a.distance ?? Infinity) - (b.distance ?? Infinity),
        ),
      ),
    );
  }

  getCategories(): Observable<Category[]> {
    return this.coreAdapter.getPoiCategories();
  }

  getPoisFromModules(): Observable<Poi[]> {
    return from(this.moduleRegistryService.getFeatureModules()).pipe(
      filter((module) => module.registerMap !== undefined),
      mergeMap((module) => {
        if (module.registerMap) {
          return module.registerMap();
        }
        return [];
      }),
      map((mapIntegration) => mapIntegration.poi),
    );
  }

  private calculatePoiDistance(
    poi: Poi,
    currentLocation?: { lat: number; long: number },
  ): PoiWithDistance {
    const distance = currentLocation
      ? getDistance(
          { latitude: currentLocation.lat, longitude: currentLocation.long },
          {
            latitude: poi.location.coordinates.latitude,
            longitude: poi.location.coordinates.longitude,
          },
        )
      : undefined;

    return new PoiWithDistance(poi, distance);
  }

  private fetchPoisForCategories(categories: Category[]): Observable<Poi[]> {
    const categoryIds = categories
      .filter((cat) => cat.source === 'API')
      .map((cat) => cat.id);
    return forkJoin({
      apiPois: this.getPois(categoryIds),
      modulePois: this.getPoisFromModules(),
    }).pipe(map(({ apiPois, modulePois }) => [...apiPois, ...modulePois]));
  }

  private getCategoriesFromModules(): Observable<Category[]> {
    return from(this.moduleRegistryService.getFeatureModules()).pipe(
      filter((module) => module.registerMap !== undefined),
      mergeMap((module) => {
        if (module.registerMap) {
          return module.registerMap();
        }
        return [];
      }),
      map((mapIntegration) => mapIntegration.category),
      toArray(),
    );
  }
}
