import { Injectable } from '@angular/core';

@Injectable()
export class JsonParsingService {
  parseJson<T>(jsonString: string): T | null {
    try {
      return JSON.parse(jsonString);
    } catch (error) {
      console.error('Error parsing JSON:', error);
      return null;
    }
  }

  stringifyJson<T>(json: T): string | null {
    try {
      return JSON.stringify(json);
    } catch (error) {
      console.error('Error stringifying JSON:', error);
      return null;
    }
  }
}
