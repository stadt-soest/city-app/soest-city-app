import { JsonParsingService } from './json-parsing.service';

describe('JsonParsingService', () => {
  let service: JsonParsingService;

  beforeEach(() => {
    service = new JsonParsingService();
  });
  describe('parseJson', () => {
    it('should parse valid JSON string', () => {
      const jsonString = '{"name":"John", "age":30, "city":"New York"}';
      const expectedResult = { name: 'John', age: 30, city: 'New York' };
      expect(service.parseJson(jsonString)).toEqual(expectedResult);
    });

    it('should return null for invalid JSON string', () => {
      const invalidJsonString = '{"name": "John", "age": 30,';
      expect(service.parseJson(invalidJsonString)).toBeNull();
    });

    it('should log an error for invalid JSON string', () => {
      const invalidJsonString = '{"name": "John", "age": 30,';
      const consoleSpy = jest.spyOn(console, 'error');
      service.parseJson(invalidJsonString);
      expect(consoleSpy).toHaveBeenCalledWith(
        'Error parsing JSON:',
        expect.any(Error),
      );
      consoleSpy.mockRestore();
    });
  });
  describe('stringifyJson', () => {
    it('should stringify valid JSON object', () => {
      const json = { name: 'John', age: 30, city: 'New York' };
      const expectedResult = '{"name":"John","age":30,"city":"New York"}';
      expect(service.stringifyJson(json)).toEqual(expectedResult);
    });

    it('should return null for invalid JSON object', () => {
      const circularObject: { [key: string]: any } = { name: 'John', age: 30 };
      circularObject['self'] = circularObject;
      expect(service.stringifyJson(circularObject)).toBeNull();
    });

    it('should log an error for invalid JSON object', () => {
      const circularObject: { [key: string]: any } = { name: 'John', age: 30 };
      circularObject['self'] = circularObject;
      const consoleSpy = jest.spyOn(console, 'error');
      service.stringifyJson(circularObject);
      expect(consoleSpy).toHaveBeenCalledWith(
        'Error stringifying JSON:',
        expect.any(Error),
      );
      consoleSpy.mockRestore();
    });
  });
});
