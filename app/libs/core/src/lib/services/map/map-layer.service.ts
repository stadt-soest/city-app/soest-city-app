import { inject, Injectable } from '@angular/core';
import { MapConfigService } from './map-config.service';
import * as maplibregl from 'maplibre-gl';
import { LayerSpecification, SourceSpecification } from 'maplibre-gl';
import { CORE_MODULE_CONFIG } from '../../core-module.config';

/* eslint @typescript-eslint/naming-convention: 0 */ // --> OFF
@Injectable({ providedIn: 'root' })
export class MapLayerService {
  private readonly configService = inject(MapConfigService);
  private readonly config = inject(CORE_MODULE_CONFIG);
  private poiClickHandler?: (event: any) => void;

  loadInitialFeatures(): void {
    const map = this.configService.getMap();
    if (!map) {
      return;
    }
    if (!map.isStyleLoaded()) {
      map.on('load', () => this.setupInitialLayers());
    } else {
      this.setupInitialLayers();
    }
  }

  setupInitialLayers(): void {
    const map = this.configService.getMap();
    if (!map) {
      return;
    }

    this.addSourceIfNotExists(map, 'pois', {
      type: 'geojson',
      generateId: true,
      data: {
        type: 'FeatureCollection',
        features: [],
      },
    });

    this.addLayerIfNotExists(map, 'poi-markers', {
      id: 'poi-markers',
      type: 'symbol',
      source: 'pois',
      layout: {
        'icon-image': ['get', 'iconKey'],
        'icon-size': 1 / this.config.map.pixelRatio,
        'icon-allow-overlap': true,
        'icon-ignore-placement': true,
        'icon-padding': 10,
        'text-padding': 10,
        'text-optional': true,
        'text-offset': [0, -1.3],
        'text-anchor': 'bottom',
        'text-font': ['Noto Sans Bold'],
        'text-field': ['get', 'title'],
      },
      paint: {
        'text-color': [
          'case',
          ['boolean', ['feature-state', 'hover'], false],
          '#FFFFFF',
          ['coalesce', ['get', 'backgroundColor'], '#A50000'],
        ],
        'text-halo-color': [
          'case',
          ['boolean', ['feature-state', 'hover'], false],
          ['coalesce', ['get', 'backgroundColor'], '#A50000'],
          '#FFFFFF',
        ],
        'text-halo-width': 2,
        'text-opacity': ['step', ['zoom'], 0, 10, 0, 11, 1],
      },
    });
  }

  setFeatures(
    features: GeoJSON.Feature[],
    clickCallback: (poiId: string, modulePath?: string) => void,
  ): void {
    const map = this.configService.getMap();
    if (!map) {
      return;
    }

    const source = map.getSource('pois') as maplibregl.GeoJSONSource;
    if (source) {
      source.setData({
        type: 'FeatureCollection',
        features,
      });

      if (this.poiClickHandler) {
        map.off('click', 'poi-markers', this.poiClickHandler);
      }

      this.poiClickHandler = (event) => {
        if (event.features && event.features.length > 0) {
          const properties = event.features[0].properties;
          const poiId = properties.poiId;
          const modulePath = properties.modulePath;
          clickCallback(poiId, modulePath);
        }
      };

      map.on('click', 'poi-markers', this.poiClickHandler);
    } else {
      console.error('Failed to find or set the POIs source');
    }
  }

  private addSourceIfNotExists(
    map: maplibregl.Map,
    sourceId: string,
    sourceConfig: SourceSpecification,
  ): void {
    if (!map.getSource(sourceId)) {
      map.addSource(sourceId, sourceConfig);
    }
  }

  private addLayerIfNotExists(
    map: maplibregl.Map,
    layerId: string,
    layerConfig: LayerSpecification,
  ): void {
    if (!map.getLayer(layerId)) {
      map.addLayer(layerConfig);
    }
  }
}
