import { DestroyRef, EventEmitter, inject, Injectable } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MapConfigService } from './map-config.service';
import { MapLayerService } from './map-layer.service';
import { MapInteractionService } from './map-interaction.service';
import { MapIconService } from './map-icon.service';
import { CORE_MODULE_CONFIG } from '../../core-module.config';

@Injectable({ providedIn: 'root' })
export class MapService {
  onMapReady = new EventEmitter<void>();
  private readonly destroyRef = inject(DestroyRef);
  private readonly configService = inject(MapConfigService);
  private readonly layerService = inject(MapLayerService);
  private readonly interactionService = inject(MapInteractionService);
  private readonly iconService = inject(MapIconService);
  private readonly config = inject(CORE_MODULE_CONFIG);
  private mapReady = false;

  constructor() {
    this.addMissingIcons();
    this.onMapReady.pipe(takeUntilDestroyed(this.destroyRef)).subscribe(() => {
      this.mapReady = true;
    });
    this.emitMapReady();
  }

  isMapReady(): boolean {
    return this.mapReady;
  }

  initializeMap(containerId: string): void {
    this.configService.initializeMap(containerId);
    this.layerService.loadInitialFeatures();
    this.interactionService.setupInteractions();
  }

  addFeatures(
    features: GeoJSON.Feature[],
    clickCallback: (poiId: string) => void,
  ): void {
    this.layerService.setFeatures(features, (poiId, modulePath) => {
      clickCallback(poiId);
    });
  }

  getCurrentCenter(): { lat: number; long: number } | null {
    const map = this.configService.getMap();

    if (map) {
      const center = map.getCenter();
      return { lat: center.lat, long: center.lng };
    }

    return null;
  }

  resetMap(): void {
    const map = this.configService.getMap();
    if (map) {
      map.easeTo({
        center: this.config.map.center,
        zoom: this.config.map.zoom,
      });
    }
  }

  panToLocation(lat: number, lng: number, offsetY = 0, zoomLevel = 18): void {
    const map = this.configService.getMap();
    if (map) {
      map.flyTo({
        center: [lng, lat],
        zoom: zoomLevel,
        speed: 1.2,
        essential: true,
        offset: [0, offsetY],
      });
    }
  }

  updateControlPositions(position: 'top-right' | 'bottom-right'): void {
    this.configService.updateControlPositions(position);
  }

  private addMissingIcons() {
    this.configService.onMapReady
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((map) => {
        map.on('styleimagemissing', async (e) =>
          this.handleMissingIcon(e, map),
        );
      });
  }

  private async handleMissingIcon(e: any, map: any): Promise<void> {
    if (e.id === 'atm_11') {
      return;
    }

    const missingIconId = e.id;
    const iconBase = this.getIconBase(missingIconId);
    const backgroundColor = this.extractBackgroundColor(missingIconId);

    if (backgroundColor) {
      await this.iconService.loadIcon(missingIconId, backgroundColor);
    } else {
      await this.loadDefaultIcon(iconBase, map);
    }
  }

  private getIconBase(iconId: string): string {
    return iconId.split('-')[0];
  }

  private extractBackgroundColor(iconId: string): string | undefined {
    const rgbaRegex = /rgba\(([^)]+)\)/;
    const match = rgbaRegex.exec(iconId);
    return match ? `rgba(${match[1]})` : undefined;
  }

  private async loadDefaultIcon(iconBase: string, map: any): Promise<void> {
    const features = map.queryRenderedFeatures(
      { layers: ['poi-markers'] },
      {
        filter: ['==', 'iconBase', iconBase],
      },
    );

    if (features.length && features[0].properties.backgroundColor) {
      await this.iconService.loadIcon(
        `${iconBase}-default`,
        features[0].properties.backgroundColor,
      );
    } else {
      await this.iconService.loadIcon(`${iconBase}-default`);
    }
  }

  private emitMapReady() {
    this.configService.onMapReady
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(() => {
        this.onMapReady.emit();
      });
  }
}
