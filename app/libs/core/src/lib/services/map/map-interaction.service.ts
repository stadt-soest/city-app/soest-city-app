import { EventEmitter, inject, Injectable } from '@angular/core';
import { MapConfigService } from './map-config.service';
import * as maplibregl from 'maplibre-gl';

@Injectable({ providedIn: 'root' })
export class MapInteractionService {
  public mapInteracted = new EventEmitter<void>();

  private readonly configService = inject(MapConfigService);
  private hoveredStateId?: number | string | null = null;

  setupInteractions(): void {
    const map = this.configService.getMap();
    if (!map) {
      return;
    }

    map.on('mousemove', 'poi-markers', (e: MapMouseEventWithFeatures) =>
      this.handleMouseMove(e, map),
    );
    map.on('mouseleave', 'poi-markers', () => this.handleMouseLeave(map));
    map.on('click', (e: maplibregl.MapMouseEvent) => this.onMapClick(e, map));
    map.on('dragstart', () => this.emitMapInteracted());
  }

  private handleMouseMove(
    e: MapMouseEventWithFeatures,
    map: maplibregl.Map,
  ): void {
    if (this.featureExists(e)) {
      this.resetHoverState(map);
      this.setHoverState(e.features?.[0].id, map);
    }
  }

  private handleMouseLeave(map: maplibregl.Map): void {
    this.resetHoverState(map);
    this.hoveredStateId = null;
  }

  private onMapClick(e: maplibregl.MapMouseEvent, map: maplibregl.Map) {
    const features = map.queryRenderedFeatures(e.point);
    const isOnMarker = features.some(
      (feature) => feature.layer && feature.layer.id === 'poi-markers',
    );

    if (!isOnMarker) {
      this.emitMapInteracted();
    }
  }

  private emitMapInteracted() {
    this.mapInteracted.emit();
  }

  private featureExists(e: MapMouseEventWithFeatures): boolean {
    return !!(e.features && e.features.length > 0);
  }

  private resetHoverState(map: maplibregl.Map): void {
    if (this.hoveredStateId !== null) {
      map.getCanvas().style.cursor = '';
      map.setFeatureState(
        { source: 'pois', id: this.hoveredStateId },
        { hover: false },
      );
    }
  }

  private setHoverState(
    newHoveredStateId: number | string | undefined,
    map: maplibregl.Map,
  ): void {
    this.hoveredStateId = newHoveredStateId;
    map.getCanvas().style.cursor = 'pointer';
    map.setFeatureState(
      { source: 'pois', id: this.hoveredStateId },
      { hover: true },
    );
  }
}

type MapMouseEventWithFeatures = maplibregl.MapMouseEvent & {
  features?: maplibregl.MapGeoJSONFeature[];
};
