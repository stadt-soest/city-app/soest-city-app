import { inject, Injectable } from '@angular/core';
import { CORE_MODULE_CONFIG } from '../../core-module.config';
import { MapConfigService } from './map-config.service';

@Injectable({ providedIn: 'root' })
export class MapIconService {
  private readonly mapConfigService = inject(MapConfigService);
  private readonly config = inject(CORE_MODULE_CONFIG);
  private readonly loadingIcons: Set<string> = new Set();
  private readonly defaultIconUrl = '/assets/map/icons/default-icon.svg';

  async loadIcon(iconKey: string, backgroundColor?: string): Promise<void> {
    const map = this.mapConfigService.getMap();
    if (!map.hasImage(iconKey) && !this.loadingIcons.has(iconKey)) {
      this.loadingIcons.add(iconKey);
      try {
        const image = await this.fetchAndPrepareIcon(iconKey, backgroundColor);
        await this.registerIconWithMap(iconKey, image, map);
      } catch (error) {
        console.error(`Error processing icon ${iconKey}:`, error);
      } finally {
        this.loadingIcons.delete(iconKey);
      }
    }
  }

  private async fetchAndPrepareIcon(
    iconKey: string,
    backgroundColor?: string,
  ): Promise<HTMLImageElement> {
    try {
      const svgUrl = this.getMaterialSymbolSvgUrl(iconKey);
      const svgText = await this.fetchSvg(svgUrl);
      const modifiedSvg = this.modifySvg(svgText, backgroundColor);
      return this.svgToImage(modifiedSvg);
    } catch (error) {
      console.warn(
        `Icon not found for ${iconKey}, falling back to default icon.`,
      );
      return this.loadDefaultIcon();
    }
  }

  private async registerIconWithMap(
    iconKey: string,
    image: HTMLImageElement,
    map: maplibregl.Map,
  ): Promise<void> {
    try {
      map.addImage(iconKey, image);
    } catch (error) {
      console.error(`Error adding icon ${iconKey} to the map:`, error);
      throw error;
    }
  }

  private getMaterialSymbolSvgUrl(iconKey: string): string {
    const baseIconKey = iconKey.split('-')[0];
    return `/assets/map/icons/${baseIconKey}.svg`;
  }

  private async fetchSvg(url: string): Promise<string> {
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error(`Failed to fetch SVG from ${url}`);
    }
    return response.text();
  }

  private modifySvg(svgText: string, backgroundColor?: string): string {
    const parser = new DOMParser();
    const svgDoc = parser.parseFromString(svgText, 'image/svg+xml');
    const svgElement = svgDoc.documentElement;

    const intToString = (x: number) => Math.floor(x).toString();
    svgElement.setAttribute(
      'width',
      intToString(40 * this.config.map.pixelRatio),
    );
    svgElement.setAttribute(
      'height',
      intToString(40 * this.config.map.pixelRatio),
    );
    svgElement.setAttribute('viewBox', '-2 -2 42 42'); // slightly enlarge viewBox to avoid stoke being cutoff

    const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    rect.setAttribute('width', '40');
    rect.setAttribute('height', '40');
    rect.setAttribute('rx', '20');
    rect.setAttribute('fill', backgroundColor ?? '#A50000');
    rect.setAttribute('stroke', 'white');
    rect.setAttribute('stroke-width', '2');
    svgElement.insertBefore(rect, svgElement.firstChild);

    const paths = svgElement.querySelectorAll('path');
    paths.forEach((path) => {
      path.setAttribute('fill', 'white');
      const scale = 0.029;
      const dx = (40 - 960 * scale) / 2;
      const dy = (40 + 960 * scale) / 2;
      path.setAttribute('transform', `translate(${dx}, ${dy}) scale(${scale})`);
    });

    const serializer = new XMLSerializer();
    return serializer.serializeToString(svgElement);
  }

  private async svgToImage(svgData: string): Promise<HTMLImageElement> {
    return new Promise((resolve, reject) => {
      const image = new Image();
      const blob = new Blob([svgData], { type: 'image/svg+xml' });
      const url = URL.createObjectURL(blob);

      image.onload = () => {
        resolve(image);
        URL.revokeObjectURL(url);
      };
      image.onerror = () => {
        reject(new Error('Failed to load SVG image'));
        URL.revokeObjectURL(url);
      };

      image.src = url;
    });
  }

  private async loadDefaultIcon(): Promise<HTMLImageElement> {
    try {
      const defaultSvg = await this.fetchSvg(this.defaultIconUrl);
      const modifiedSvg = this.modifySvg(defaultSvg);
      return this.svgToImage(modifiedSvg);
    } catch (error) {
      console.error('Critical: Failed to load default icon:', error);
      const img = new Image(40, 40);
      img.src =
        'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
      return img;
    }
  }
}
