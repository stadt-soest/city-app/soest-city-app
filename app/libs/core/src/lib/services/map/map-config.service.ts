import { EventEmitter, inject, Injectable } from '@angular/core';
import * as maplibregl from 'maplibre-gl';
import { TranslocoService } from '@jsverse/transloco';
import { MAP_STYLE_URL } from '../../core.module';
import { CORE_MODULE_CONFIG } from '../../core-module.config';

@Injectable({ providedIn: 'root' })
export class MapConfigService {
  public onMapReady = new EventEmitter<maplibregl.Map>();
  private map!: maplibregl.Map;
  private geolocateControl!: maplibregl.GeolocateControl;
  private navigationControl!: maplibregl.NavigationControl;
  private readonly mapStyleUrl = inject(MAP_STYLE_URL);
  private readonly config = inject(CORE_MODULE_CONFIG);
  private readonly translateService = inject(TranslocoService);

  getMap(): maplibregl.Map {
    return this.map;
  }

  initializeMap(containerId: string): maplibregl.Map {
    this.map = new maplibregl.Map({
      container: containerId,
      style: this.mapStyleUrl,
      canvasContextAttributes: {
        antialias: this.config.map.antialias,
      },
      pixelRatio: this.config.map.pixelRatio,
      center: this.config.map.center,
      zoom: this.config.map.zoom,
      attributionControl: false,
    });

    this.map.on('load', () => {
      this.addControls();
      this.onMapReady.emit(this.map);

      this.map.once('idle', () => {
        this.setControlLabels();
      });
    });

    return this.map;
  }

  updateControlPositions(position: 'top-right' | 'bottom-right'): void {
    try {
      if (this.geolocateControl && this.map.hasControl(this.geolocateControl)) {
        this.map.removeControl(this.geolocateControl);
      }

      if (
        this.navigationControl &&
        this.map.hasControl(this.navigationControl)
      ) {
        this.map.removeControl(this.navigationControl);
      }

      if (this.geolocateControl) {
        this.map.addControl(this.geolocateControl, position);
      }

      if (this.navigationControl) {
        this.map.addControl(this.navigationControl, position);
      }
    } catch (error) {
      console.error('Failed to update control positions:', error);
    }
  }

  private addControls(): void {
    this.geolocateControl = new maplibregl.GeolocateControl({
      positionOptions: { enableHighAccuracy: true },
      trackUserLocation: true,
      showUserLocation: true,
    });

    this.navigationControl = new maplibregl.NavigationControl({
      showCompass: true,
    });

    const attributionControl = new maplibregl.AttributionControl({
      compact: true,
    });

    this.map.once('load', () => {
      const attributionDetails = this.map
        .getContainer()
        .querySelector('.maplibregl-ctrl-attrib');

      if (!attributionDetails) {
        console.warn('Attribution control not found');
        return;
      }

      if (
        attributionDetails instanceof HTMLElement &&
        attributionDetails.tagName === 'DETAILS'
      ) {
        attributionDetails.removeAttribute('open');
        attributionDetails.classList.remove('maplibregl-compact-show');
      }
    });

    this.map.addControl(attributionControl, 'top-left');
    this.map.addControl(this.geolocateControl, 'bottom-right');
    this.map.addControl(this.navigationControl, 'bottom-right');
  }

  private setControlLabels(): void {
    const controlsMap = [
      {
        selector: '.maplibregl-ctrl-zoom-in',
        translationKey: 'ui.maps_navigation.zoom_in',
      },
      {
        selector: '.maplibregl-ctrl-zoom-out',
        translationKey: 'ui.maps_navigation.zoom_out',
      },
      {
        selector: '.maplibregl-ctrl-compass',
        translationKey: 'ui.maps_navigation.compass',
      },
      {
        selector: '.maplibregl-ctrl-geolocate',
        translationKey: 'ui.maps_navigation.find_my_location',
      },
    ];

    controlsMap.forEach((control) => {
      this.setControlLabel(control.selector, control.translationKey);
    });
  }

  private setControlLabel(selector: string, translationKey: string): void {
    const element = document.querySelector(selector);
    if (element) {
      this.translateService
        .selectTranslate(translationKey)
        .subscribe((translation) => {
          element.setAttribute('aria-label', translation);
          element.setAttribute('title', translation);
        });
    }
  }
}
