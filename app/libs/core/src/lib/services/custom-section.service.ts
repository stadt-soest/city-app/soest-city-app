import { inject, Injectable } from '@angular/core';
import { combineLatest, Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { ModuleRegistryService } from './module-registry/module-registry.service';
import {
  FeatureModule,
  FeatureSection,
} from '../interfaces/feature-module.interface';

@Injectable({
  providedIn: 'root',
})
export class CustomSectionService {
  private readonly moduleRegistryService = inject(ModuleRegistryService);

  loadCustomSections(): Observable<FeatureSection[]> {
    const registeredModules = this.moduleRegistryService.getFeatureModules();

    const sectionObservables = registeredModules.map((module) =>
      this.getSectionObservableForModule(module),
    );

    return combineLatest(sectionObservables).pipe(
      map((sections) => this.filterVisibleSections(sections)),
    );
  }

  private getSectionObservableForModule(
    module: FeatureModule<any>,
  ): Observable<FeatureSection | null> {
    return this.moduleRegistryService
      .getModuleVisibilityWithUpdates(module.info)
      .pipe(switchMap((isVisible) => this.getSectionOrNull(module, isVisible)));
  }

  private getSectionOrNull(
    module: FeatureModule<any>,
    isVisible: boolean,
  ): Observable<FeatureSection | null> {
    if (isVisible && module.getCustomForYouSection) {
      return module.getCustomForYouSection();
    }
    return of(null);
  }

  private filterVisibleSections(
    sections: (FeatureSection | null)[],
  ): FeatureSection[] {
    return sections.filter(
      (section): section is FeatureSection => section !== null,
    );
  }
}
