import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { TabType } from '../../enums/tabs';

@Injectable({
  providedIn: 'root',
})
export class TabRefreshService {
  private readonly refreshSubject = new Subject<TabType>();

  refresh$ = this.refreshSubject.asObservable();

  triggerRefresh(tabType: TabType) {
    this.refreshSubject.next(tabType);
  }
}
