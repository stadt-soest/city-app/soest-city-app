import { Injectable } from '@angular/core';
import { IonNav } from '@ionic/angular';

@Injectable()
export class NavService {
  private ionNav: IonNav | null = null;

  setNav(nav: IonNav | null) {
    this.ionNav = nav;
  }

  async canGoBack(): Promise<boolean> {
    return this.ionNav ? this.ionNav.canGoBack() : false;
  }

  async popPage(): Promise<void> {
    if (this.ionNav) {
      await this.ionNav.pop();
    }
  }

  async pushPage(page: any, params?: any, extra?: any) {
    if (this.ionNav) {
      await this.ionNav.push(page, params, extra);
    }
  }
}
