import { inject, Injectable, OnDestroy } from '@angular/core';
import { Keyboard } from '@capacitor/keyboard';
import { DeviceService } from '../device/device.service';

@Injectable()
export class KeyboardService implements OnDestroy {
  private readonly deviceService = inject(DeviceService);
  private keyboardOpen = false;

  get isKeyboardOpen(): boolean {
    return this.keyboardOpen;
  }

  async initialize() {
    await this.initKeyboardListener();
  }

  async ngOnDestroy() {
    if (this.deviceService.isNativePlatform()) {
      await Keyboard.removeAllListeners();
    }
  }

  async hideKeyboard() {
    if (this.deviceService.isNativePlatform()) {
      await Keyboard.hide();
    }
  }

  private async initKeyboardListener() {
    if (this.deviceService.isNativePlatform()) {
      await Keyboard.addListener('keyboardWillShow', () => {
        this.keyboardOpen = true;
      });

      await Keyboard.addListener('keyboardWillHide', () => {
        this.keyboardOpen = false;
      });
    }
  }
}
