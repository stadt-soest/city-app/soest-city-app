import { Injectable, inject } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';
import { TranslatableString } from '../../models/translatable-string.model';

@Injectable()
export class TranslationService {
  private readonly translocoService = inject(TranslocoService);

  getCurrentLocale(): string {
    return this.translocoService.getActiveLang().toUpperCase();
  }

  translate(translatable: TranslatableString): string {
    return this.translocoService.translate(
      translatable.key,
      translatable.params,
    );
  }
}
