import { TestBed } from '@angular/core/testing';
import { createModuleInfo } from '../../test/shared/test.util';
import { ModuleInfo } from '../../models/menu.model';
import { ModuleRegistryService } from '../module-registry/module-registry.service';
import { OnboardingService } from './onboarding.service';
import { StorageService } from '../storage/storage.service';
import { Component } from '@angular/core';

@Component({
  selector: 'lib-mock-component',
  template: '<p>Mock Component</p>',
})
export class MockComponent {}

describe('OnboardingService', () => {
  let service: OnboardingService;
  let mockStorageService: jest.Mocked<StorageService>;
  let mockModuleRegistryService: jest.Mocked<ModuleRegistryService>;

  const mockSliderComponent = MockComponent;
  const mockModuleInfo: ModuleInfo = createModuleInfo();
  const mockSliders = [
    {
      component: mockSliderComponent,
      moduleInfo: mockModuleInfo,
      isActivateSelected: false,
      isLaterSelected: true,
    },
  ];

  beforeEach(() => {
    mockStorageService = {
      get: jest.fn(),
      set: jest.fn(),
    } as any;

    mockModuleRegistryService = {
      getValuesModuleVisibilitySettings: jest.fn(),
      getAllModuleVisibilitySettings: jest.fn(),
    } as any;

    TestBed.configureTestingModule({
      providers: [
        OnboardingService,
        {
          provide: StorageService,
          useValue: mockStorageService,
        },
        { provide: ModuleRegistryService, useValue: mockModuleRegistryService },
      ],
    });

    service = TestBed.inject(OnboardingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('hasFinishedOnboarding', () => {
    it('should return true when storage returns "true"', async () => {
      mockStorageService.get.mockResolvedValue('true');

      const result = await service.hasFinishedOnboarding();

      expect(result).toBe(true);
      expect(mockStorageService.get).toHaveBeenCalledWith('finishedOnboarding');
    });

    it('should return false when storage returns not "true"', async () => {
      mockStorageService.get.mockResolvedValue('false');

      const result = await service.hasFinishedOnboarding();

      expect(result).toBe(false);
      expect(mockStorageService.get).toHaveBeenCalledWith('finishedOnboarding');
    });
  });

  describe('setFinishedOnboarding', () => {
    it('should set finishedOnboarding in storage to "true"', async () => {
      await service.setFinishedOnboarding();

      expect(mockStorageService.set).toHaveBeenCalledWith(
        'finishedOnboarding',
        'true',
      );
    });
  });

  describe('setRadioButtonSelection', () => {
    it('should set isActivateSelected to true when settings value is true', async () => {
      mockModuleRegistryService.getValuesModuleVisibilitySettings.mockResolvedValue(
        [true],
      );
      const result = await service.setRadioButtonSelection(mockSliders);

      expect(result[0].isActivateSelected).toBe(true);
      expect(result[0].isLaterSelected).toBe(false);
    });

    it('should set isLaterSelected to true when settings value is false', async () => {
      mockModuleRegistryService.getValuesModuleVisibilitySettings.mockResolvedValue(
        [false],
      );
      const result = await service.setRadioButtonSelection(mockSliders);

      expect(result[0].isActivateSelected).toBe(false);
      expect(result[0].isLaterSelected).toBe(true);
    });
  });

  describe('doSettingsExist', () => {
    it('should return true when settings exist', async () => {
      mockModuleRegistryService.getAllModuleVisibilitySettings.mockResolvedValue(
        {
          item1: true,
          item2: false,
        },
      );
      const result = await service.doSettingsExist(mockSliders);

      expect(result).toBe(true);
    });

    it('should return false when settings do not exist', async () => {
      mockModuleRegistryService.getAllModuleVisibilitySettings.mockResolvedValue(
        null,
      );
      const result = await service.doSettingsExist(mockSliders);

      expect(result).toBe(false);
    });
  });
});
