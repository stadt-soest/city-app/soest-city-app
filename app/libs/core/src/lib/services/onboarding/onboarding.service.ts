import { EventEmitter, inject, Injectable } from '@angular/core';
import { StorageService } from '../storage/storage.service';
import { ModuleInfo } from '../../models/menu.model';
import { ModuleRegistryService } from '../module-registry/module-registry.service';
import { OnboardingComponentInfo } from '../../interfaces/onboarding-component-info.interface';

const FINISHED_ONBOARDING_KEY = 'finishedOnboarding';

@Injectable()
export class OnboardingService {
  navigateNext = new EventEmitter<void>();
  finishOnboardingEmitter = new EventEmitter<void>();
  private readonly storageService = inject(StorageService);
  private readonly moduleRegistryService = inject(ModuleRegistryService);

  triggerNextSlide() {
    this.navigateNext.emit();
  }

  finishOnboarding() {
    this.finishOnboardingEmitter.emit();
  }

  async navigateToNextSlide(moduleInfo?: ModuleInfo, type?: string) {
    if (moduleInfo && type) {
      await this.moduleRegistryService.saveModuleVisibilitySettings(
        moduleInfo,
        type === 'activate',
      );
    }
    this.triggerNextSlide();
  }

  async hasFinishedOnboarding(): Promise<boolean> {
    const finishedOnboarding = await this.storageService.get(
      FINISHED_ONBOARDING_KEY,
    );
    return finishedOnboarding === 'true';
  }

  async setFinishedOnboarding(): Promise<void> {
    await this.storageService.set(FINISHED_ONBOARDING_KEY, 'true');
  }

  async setRadioButtonSelection(
    sliders: OnboardingComponentInfo[],
  ): Promise<OnboardingComponentInfo[]> {
    for (const slider of sliders) {
      const settingsValues =
        await this.moduleRegistryService.getValuesModuleVisibilitySettings(
          slider.moduleInfo,
        );

      if (settingsValues[0]) {
        slider.isActivateSelected = true;
        slider.isLaterSelected = false;
      } else if (!settingsValues[0]) {
        slider.isActivateSelected = false;
        slider.isLaterSelected = true;
      }
    }
    return sliders;
  }

  async doSettingsExist(sliders: OnboardingComponentInfo[]): Promise<boolean> {
    for (const slider of sliders) {
      const setting =
        await this.moduleRegistryService.getAllModuleVisibilitySettings(
          slider.moduleInfo,
        );
      if (setting !== null) {
        return true;
      }
    }
    return false;
  }
}
