import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { PageType } from '../../enums/page-type.enum';

@Injectable({ providedIn: 'root' })
export class MapNavigationService {
  private readonly isOnCategoryListPageSubject = new BehaviorSubject<boolean>(
    false,
  );
  isOnCategoryListPage$ = this.isOnCategoryListPageSubject.asObservable();

  private readonly isOnCategoryDetailsPageSubject =
    new BehaviorSubject<boolean>(false);
  isOnCategoryDetailsPage$ = this.isOnCategoryDetailsPageSubject.asObservable();

  private readonly updateModuleCategoryNameSubject = new BehaviorSubject<
    string | undefined
  >(undefined);
  updateModuleCategoryName$ =
    this.updateModuleCategoryNameSubject.asObservable();

  private readonly updateMapSubject = new Subject<{
    categoryId: string;
    categoryDetailsId: string;
    selectedCategories: PageType[];
    pageType: PageType;
    categoryName: string;
  }>();
  mapEvents$ = this.updateMapSubject.asObservable();

  private readonly popPageListSubject = new Subject<void>();
  popPageList$ = this.popPageListSubject.asObservable();

  private readonly popPageDetailsSubject = new Subject<void>();
  popPageDetails$ = this.popPageDetailsSubject.asObservable();

  setIsOnCategoryListPage(value: boolean) {
    this.isOnCategoryListPageSubject.next(value);
  }

  setIsOnCategoryDetailsPage(value: boolean) {
    this.isOnCategoryDetailsPageSubject.next(value);
  }

  setModuleUpdateCategoryName(categoryName: string | undefined) {
    this.updateModuleCategoryNameSubject.next(categoryName);
  }

  getCurrentModuleCategoryName(): string | undefined {
    return this.updateModuleCategoryNameSubject.getValue();
  }

  setUpdateMap(
    categoryId: string,
    categoryDetailsId: string,
    selectedCategories: PageType[],
    pageType: PageType,
    categoryName: string,
  ) {
    this.updateMapSubject.next({
      categoryId,
      categoryDetailsId,
      selectedCategories,
      pageType,
      categoryName,
    });
  }
}
