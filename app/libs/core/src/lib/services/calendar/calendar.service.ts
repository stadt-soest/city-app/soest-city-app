import { inject, Injectable } from '@angular/core';
import {
  ICalCalendar,
  ICalCalendarMethod,
  ICalEventRepeatingFreq,
} from 'ical-generator';
import { Calendar } from '@awesome-cordova-plugins/calendar/ngx';
import { convert } from 'html-to-text';
import { DeviceService } from '../device/device.service';
import { AlertService } from '../alert.service';

@Injectable()
export class CalendarService {
  private readonly deviceService = inject(DeviceService);
  private readonly alertService = inject(AlertService);
  private readonly calendar = inject(Calendar);

  async createEvent(
    id: string,
    title: string,
    location: string,
    notes: string,
    startDate: Date,
    endDate: Date,
  ) {
    if (this.deviceService.isNativePlatform()) {
      if (this.deviceService.isIOS()) {
        await this.createEventForIOSDevicesAndOpen(
          title,
          location,
          notes,
          startDate,
          endDate,
        );
      } else {
        await this.createEventForNativeDevicesAndOpen(
          title,
          location,
          notes,
          startDate,
          endDate,
        );
      }
    } else {
      const ics: string = this.generateIcsString(
        id,
        title,
        location,
        notes,
        startDate,
        endDate,
      );
      this.createIcsForWebAndDownload(ics, title);
    }
  }

  generateIcsString(
    id: string,
    title: string,
    location: string,
    notes: string,
    startDate: Date,
    endDate: Date,
  ) {
    const cal = new ICalCalendar();
    cal.method(ICalCalendarMethod.REQUEST);

    if (this.isMultidayEvent(startDate, endDate)) {
      cal.createEvent({
        id,
        summary: title,
        description: convert(notes),
        location,
        start: startDate,
        end: this.calculateMultidayEndDate(startDate, endDate),
        timezone: 'Europe/Berlin',
        repeating: {
          freq: ICalEventRepeatingFreq.DAILY,
          until: endDate,
        },
      });
    } else {
      cal.createEvent({
        id,
        summary: title,
        description: notes,
        location,
        start: startDate,
        end: endDate,
        timezone: 'Europe/Berlin',
      });
    }
    return cal.toString();
  }

  private async createEventForIOSDevicesAndOpen(
    title: string,
    location: string,
    notes: string,
    startDate: Date,
    endDate: Date,
  ) {
    const readWritePermissions: boolean =
      await this.calendar.hasReadWritePermission();
    if (!readWritePermissions) {
      let requestResult: any;
      try {
        requestResult = await this.calendar.requestReadWritePermission();
      } catch (err: any) {
        console.log('Permissions already denied');
        requestResult = false;
      }

      if (!requestResult) {
        await this.alertService.showCalendarPermissionAlert();
      }
    }

    if (!(await this.calendar.hasReadWritePermission())) {
      return;
    }

    await this.createEventForNativeDevicesAndOpen(
      title,
      location,
      notes,
      startDate,
      endDate,
    );
  }

  private async createEventForNativeDevicesAndOpen(
    title: string,
    location: string,
    notes: string,
    startDate: Date,
    endDate: Date,
  ) {
    if (this.isMultidayEvent(startDate, endDate)) {
      await this.calendar.createEventInteractivelyWithOptions(
        title,
        location,
        notes,
        startDate,
        endDate,
        {
          recurrence: 'daily',
          recurrenceInterval: 1,
          recurrenceEndDate: this.calculateMultidayEndDate(startDate, endDate),
        },
      );
    } else {
      await this.calendar.createEventInteractively(
        title,
        location,
        notes,
        startDate,
        endDate,
      );
    }
  }

  private createIcsForWebAndDownload(ics: string, title: string): void {
    const element = document.createElement('a');
    element.setAttribute(
      'href',
      'data:text/calendar;charset=utf-8,' + encodeURIComponent(ics),
    );
    element.setAttribute('download', `${title}.ics`);
    element.setAttribute('target', '_blank');
    element.style.display = 'none';
    element.click();
    element.remove();
  }

  private isMultidayEvent(startDate: Date, endDate: Date): boolean {
    return (
      startDate.getDate() === endDate.getDate() ||
      startDate.getHours() > endDate.getHours()
    );
  }

  private calculateMultidayEndDate(startDate: Date, endDate: Date): Date {
    const calculatedEndTime: Date = new Date(endDate.getTime());
    calculatedEndTime.setDate(startDate.getDate());
    return calculatedEndTime;
  }
}
