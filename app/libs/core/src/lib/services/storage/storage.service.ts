import { inject, Injectable } from '@angular/core';
import { Preferences } from '@capacitor/preferences';
import { JsonParsingService } from '../json/json-parsing.service';
import { ModuleInfo } from '../../models/menu.model';
import { StorageKeyType } from '../../enums/storage-key.type';

@Injectable()
export class StorageService {
  private readonly jsonParsingService = inject(JsonParsingService);

  async get(key: string): Promise<any> {
    const { value } = await Preferences.get({ key });
    return value;
  }

  async set(key: string, value: any): Promise<void> {
    await Preferences.set({ key, value: value.toString() });
  }

  async clear(): Promise<void> {
    await Preferences.clear();
  }

  public generateStorageKey(
    module: ModuleInfo,
    keyType: StorageKeyType,
  ): string {
    return `storageKey_${keyType}_${module.id}`;
  }

  async remove(key: string): Promise<void> {
    await Preferences.remove({ key });
  }

  async getKeys(): Promise<string[]> {
    const keysResult = await Preferences.keys();
    return keysResult.keys;
  }

  async getObject<T>(key: string): Promise<T | null> {
    const value = await this.get(key);
    return this.jsonParsingService.parseJson<T>(value);
  }
}
