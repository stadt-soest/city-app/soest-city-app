import { TestBed } from '@angular/core/testing';
import { StorageService } from './storage.service';

jest.mock('@capacitor/preferences');

describe('StorageService', () => {
  let service: StorageService;
  let mockStorage: { get: jest.Mock; set: jest.Mock; remove: jest.Mock };

  beforeEach(() => {
    mockStorage = {
      get: jest.fn(),
      set: jest.fn(),
      remove: jest.fn(),
    };

    TestBed.configureTestingModule({
      providers: [
        StorageService,
        { provide: StorageService, useValue: mockStorage },
      ],
    });

    service = TestBed.inject(StorageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('get', () => {
    it('should retrieve a value by key', async () => {
      const key = 'testKey';
      const value = 'testValue';
      mockStorage.get.mockResolvedValue({ value });
      const result = await service.get(key);
      expect(result).toEqual({ value });
      expect(mockStorage.get).toHaveBeenCalledWith(key);
    });
  });

  describe('set', () => {
    it('should set a value for key', async () => {
      const key = 'testKey';
      const value = 'testValue';
      await service.set(key, value);
      expect(mockStorage.set).toHaveBeenCalledWith(key, value);
    });
  });

  describe('remove', () => {
    it('should remove a value by key', async () => {
      const key = 'testKey';
      await service.remove(key);
      expect(mockStorage.remove).toHaveBeenCalledWith(key);
    });
  });
});
