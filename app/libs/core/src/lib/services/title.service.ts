import { inject, Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Injectable()
export class TitleService {
  private readonly title = inject(Title);

  setTitle(title: string) {
    this.title.setTitle(title + ' — SoestApp');
  }
}
