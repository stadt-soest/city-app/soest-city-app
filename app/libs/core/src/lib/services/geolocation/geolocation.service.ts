import { inject, Injectable } from '@angular/core';
import { CallbackID, Geolocation } from '@capacitor/geolocation';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { PermissionState } from '@capacitor/core';
import { getDistance } from 'geolib';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { SimplifiedPermissionState } from '../../enums/simplified-permission-state.enum';
import { DeviceService } from '../device/device.service';
import { AlertService } from '../alert.service';

@Injectable({ providedIn: 'root' })
export class GeolocationService {
  isNative: boolean;
  permissionState$: Observable<SimplifiedPermissionState>;
  currentLocation$: Observable<{ lat: number; long: number } | null>;
  private readonly permissionStateSubject =
    new BehaviorSubject<SimplifiedPermissionState>(
      SimplifiedPermissionState.denied,
    );
  private readonly currentLocationSubject = new ReplaySubject<{
    lat: number;
    long: number;
  } | null>(1);
  private readonly deviceService = inject(DeviceService);
  private readonly alertService = inject(AlertService);
  private watchId: CallbackID | null = null;

  constructor() {
    this.isNative = this.deviceService.isNativePlatform();
    this.permissionState$ = this.permissionStateSubject.asObservable();
    this.currentLocation$ = this.currentLocationSubject.asObservable();
    this.permissionState$
      .pipe(takeUntilDestroyed())
      .subscribe(async (permission) => {
        if (permission === SimplifiedPermissionState.granted) {
          await this.fetchCurrentPositionOnce();
        } else if (permission === SimplifiedPermissionState.denied) {
          await this.stopWatchingLocation();
          this.currentLocationSubject.next(null);
        }
      });
  }

  async initializePermissionStatus(): Promise<void> {
    if (this.isNative) {
      const permissions = await Geolocation.checkPermissions();
      this.permissionStateSubject.next(
        this.translatePermissionState(permissions.location),
      );
      if (permissions.location === 'granted') {
        await this.startWatchingLocation();
      }
    } else {
      this.permissionStateSubject.next(SimplifiedPermissionState.denied);
    }
  }

  public async requestPermissions(): Promise<void> {
    if (!this.isNative) {
      this.permissionStateSubject.next(SimplifiedPermissionState.denied);
      return;
    }

    try {
      const permissions = await Geolocation.checkPermissions();

      if (permissions.location === 'granted') {
        this.permissionStateSubject.next(SimplifiedPermissionState.granted);
      } else if (permissions.location === 'denied') {
        this.permissionStateSubject.next(SimplifiedPermissionState.denied);
        await this.alertService.showLocationPermissionAlert();
      } else {
        const permissionsRequest = await Geolocation.requestPermissions();
        this.permissionStateSubject.next(
          this.translatePermissionState(permissionsRequest.location),
        );
      }
    } catch (error) {
      console.error('Error requesting permissions', error);
      this.permissionStateSubject.next(SimplifiedPermissionState.denied);
      await this.alertService.presentAlert(
        'permissions.location.header',
        'permissions.location.message',
        undefined,
        'permissions.location.confirm',
      );
    }
  }

  public async getCurrentLocation(): Promise<{
    lat: number;
    long: number;
  } | null> {
    if (
      !this.isNative ||
      this.permissionStateSubject.getValue() !==
        SimplifiedPermissionState.granted
    ) {
      return null;
    }

    try {
      const position = await this.getPositionWithTimeout(3000);
      const mappedPosition = {
        lat: position.coords.latitude,
        long: position.coords.longitude,
      };
      this.currentLocationSubject.next(mappedPosition);
      return mappedPosition;
    } catch (error: any) {
      console.error('Error getting location or GPS is off', error);

      if (error.code === 1) {
        this.permissionStateSubject.next(SimplifiedPermissionState.denied);
      }
      return null;
    }
  }

  public async calculateDistance(coordinates: {
    lat: number;
    long: number;
  }): Promise<number> {
    const currentPosition = await this.getCurrentLocation();
    if (!currentPosition) {
      throw new Error('Current location is not available.');
    }
    return getDistance(
      { latitude: currentPosition.lat, longitude: currentPosition.long },
      { latitude: coordinates.lat, longitude: coordinates.long },
    );
  }

  async stopWatchingLocation() {
    if (this.watchId) {
      await Geolocation.clearWatch({ id: this.watchId });
      this.watchId = null;
    }
  }

  async startWatchingLocation() {
    if (
      !this.isNative ||
      this.watchId ||
      this.permissionStateSubject.getValue() !==
        SimplifiedPermissionState.granted
    ) {
      return;
    }

    this.watchId = await Geolocation.watchPosition({}, (position, err) => {
      if (err) {
        console.error('Error watching location', err);
        return;
      }

      if (position) {
        this.currentLocationSubject.next({
          lat: position.coords.latitude,
          long: position.coords.longitude,
        });
      }
    });
  }

  private translatePermissionState(
    capacitorState: PermissionState,
  ): SimplifiedPermissionState {
    if (capacitorState === 'granted') {
      return SimplifiedPermissionState.granted;
    } else {
      return SimplifiedPermissionState.denied;
    }
  }

  private async fetchCurrentPositionOnce(): Promise<void> {
    if (
      !this.isNative ||
      this.permissionStateSubject.getValue() ===
        SimplifiedPermissionState.denied
    ) {
      return;
    }

    try {
      const position = await Geolocation.getCurrentPosition();
      const mappedPosition = {
        lat: position.coords.latitude,
        long: position.coords.longitude,
      };
      this.currentLocationSubject.next(mappedPosition);
    } catch (error: any) {
      console.error('Error fetching current position', error);

      if (error.code === 1) {
        this.permissionStateSubject.next(SimplifiedPermissionState.denied);
      }
      this.currentLocationSubject.next(null);
    }
  }

  private async getPositionWithTimeout(
    timeoutMs: number,
  ): Promise<GeolocationPosition> {
    return new Promise((resolve, reject) => {
      const timeoutId = setTimeout(
        () => reject(new Error('Timeout')),
        timeoutMs,
      );

      Geolocation.getCurrentPosition()
        .then((position) => {
          clearTimeout(timeoutId);
          resolve(position as GeolocationPosition);
        })
        .catch((error) => {
          clearTimeout(timeoutId);
          reject(
            new Error(error instanceof Error ? error.message : String(error)),
          );
        });
    });
  }
}
