import { ToastController } from '@ionic/angular/standalone';
import { TestBed } from '@angular/core/testing';
import { Clipboard } from '@capacitor/clipboard';
import { TranslocoService } from '@jsverse/transloco';
import { BaseShareService } from './base-share.service';
import { DeviceService } from '../device/device.service';

jest.mock('@capacitor/clipboard', () => ({
  Clipboard: {
    write: jest.fn(),
  },
}));

describe('ShareService', () => {
  let service: BaseShareService;
  let toastController: ToastController;
  let mockDeviceService: any;
  let toastSpy: jest.SpyInstance;

  beforeEach(() => {
    const mockTranslocoService = {
      translate: jest.fn().mockReturnValue('Link copied to clipboard'),
    };

    mockDeviceService = {
      isNativePlatform: jest.fn(),
    };

    Clipboard.write = jest.fn().mockImplementation(() => Promise.resolve());

    TestBed.configureTestingModule({
      providers: [
        BaseShareService,
        ToastController,
        { provide: TranslocoService, useValue: mockTranslocoService },
        { provide: DeviceService, useValue: mockDeviceService },
      ],
    });

    service = TestBed.inject(BaseShareService);
    toastController = TestBed.inject(ToastController);

    toastSpy = jest.spyOn(toastController, 'create').mockImplementation(() =>
      Promise.resolve({
        present: jest.fn(),
      } as any),
    );

    mockDeviceService.isNativePlatform.mockReturnValue(true);
  });

  it('should successfully share when share is supported', async () => {
    navigator.share = jest.fn().mockResolvedValueOnce(undefined);

    await expect(
      service.share('title', 'text', 'url'),
    ).resolves.toBeUndefined();
    expect(navigator.share).toHaveBeenCalledWith({
      title: 'title',
      text: 'text',
    });
  });

  it('should handle errors when sharing fails', async () => {
    const consoleErrorMock = jest
      .spyOn(console, 'error')
      .mockImplementation(jest.fn());
    navigator.share = jest
      .fn()
      .mockRejectedValueOnce(new Error('Share failed'));

    await service.share('title', 'text', 'url');
    expect(consoleErrorMock).toHaveBeenCalledWith(
      'Error sharing:',
      new Error('Share failed'),
    );

    consoleErrorMock.mockRestore();
  });

  it('should call copy to clipboard when web platform', async () => {
    navigator.share = jest.fn().mockResolvedValueOnce(undefined);
    mockDeviceService.isNativePlatform.mockReturnValueOnce(false);

    await service.share('title', 'text', 'url');
    expect(Clipboard.write).toHaveBeenCalled();
  });

  describe('copyToClipboard', () => {
    it('should write the URL to the clipboard and show a toast', async () => {
      await service.copyToClipboard('test-url');

      expect(Clipboard.write).toHaveBeenCalledWith({ string: 'test-url' });
      expect(toastSpy).toHaveBeenCalled();
    });
  });
});
