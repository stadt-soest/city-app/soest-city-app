import { inject, Injectable } from '@angular/core';
import { Share } from '@capacitor/share';
import { Clipboard } from '@capacitor/clipboard';
import { ToastController } from '@ionic/angular/standalone';
import { TranslocoService } from '@jsverse/transloco';
import { DeviceService } from '../device/device.service';

@Injectable()
export class BaseShareService {
  private readonly toastController = inject(ToastController);
  private readonly translocoService = inject(TranslocoService);
  private readonly deviceService = inject(DeviceService);

  async share(title: string, text: string, url: string): Promise<void> {
    const cleanedUrl: string = this.cleanUrlScheme(url);
    const cleanedTextWithUrl: string = this.cleanUrlScheme(text);

    if (this.deviceService.isNativePlatform() && (await Share.canShare())) {
      try {
        await Share.share({
          title,
          dialogTitle: title,
          text: cleanedTextWithUrl,
        });
      } catch (error) {
        console.error('Error sharing:', error);
      }
    } else {
      await this.copyToClipboard(cleanedUrl);
    }
  }

  async copyToClipboard(url: string): Promise<void> {
    await Clipboard.write({ string: url });
    const toast = await this.toastController.create({
      message: this.translocoService.translate('ui.copy_clipboard'),
      duration: 2000,
    });
    await toast.present();
  }

  cleanUrlScheme(text: string) {
    return text.replace(/capacitor:\/\//, 'https://');
  }
}
