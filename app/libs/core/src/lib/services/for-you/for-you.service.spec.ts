import { TestBed } from '@angular/core/testing';
import { ForYouService } from './for-you.service';
import { CoreAdapter } from '../../core.adapter';
import { createModuleInfo } from '../../test/shared/test.util';
import { FeedItem } from '../../models/feed-item.model';
import { ModuleRegistryService } from '../module-registry/module-registry.service';
import { SettingsPersistenceService } from '../settings-persistence/settings-persistence-service';
import { StorageService } from '../storage/storage.service';
import { of } from 'rxjs';

describe('ForYouService', () => {
  let service: ForYouService;
  let mockCoreAdapter: jest.Mocked<CoreAdapter>;
  let mockModuleRegistryService: jest.Mocked<ModuleRegistryService>;
  let mockStorageService: jest.Mocked<StorageService>;
  let mockSettingsPersistenceService: jest.Mocked<SettingsPersistenceService>;
  const mockedFeedItems = of([
    { creationDate: new Date('2023-10-20T08:00:00Z') } as FeedItem,
    { creationDate: new Date('2023-09-20T08:00:00Z') } as FeedItem,
    { creationDate: new Date('2023-08-20T08:00:00Z') } as FeedItem,
  ]);

  beforeEach(() => {
    mockCoreAdapter = {
      getFeed: jest.fn().mockReturnValue(of([])),
      getPoiById: jest.fn(),
      searchPois: jest.fn(),
      getPois: jest.fn(),
      getPoiCategories: jest.fn(),
      uploadSingleImage: jest.fn(),
    } as unknown as jest.Mocked<CoreAdapter>;

    mockStorageService = {
      get: jest.fn(),
    } as any;

    mockModuleRegistryService = {
      getFeedables: jest.fn().mockReturnValue(mockedFeedItems),
      getModuleInfos: jest.fn().mockReturnValue([]),
      getAllUserSettings: jest.fn().mockReturnValue(
        of({
          newsModule: {
            sources: ['source1', 'source2'],
            categoryIds: ['1', '2'],
          },
        }),
      ),
      getMatchingModuleInfo: jest.fn().mockReturnValue(createModuleInfo()),
      getValuesModuleVisibilitySettings: jest.fn().mockReturnValue(of([true])),
      getFeedablesUpcoming: jest.fn().mockReturnValue(mockedFeedItems),
      getFeedablesUnfiltered: jest.fn().mockReturnValue(mockedFeedItems),
      getFeedablesActivity: jest.fn().mockReturnValue(mockedFeedItems),
      getHighlightedFeedables: jest.fn().mockReturnValue(mockedFeedItems),
      getAllModuleFullSettings: jest.fn().mockReturnValue(of([])),
    } as unknown as jest.Mocked<ModuleRegistryService>;

    mockSettingsPersistenceService = {
      getSettingsValues: jest.fn().mockReturnValue(of([true])),
    } as unknown as jest.Mocked<SettingsPersistenceService>;

    TestBed.configureTestingModule({
      providers: [
        { provide: CoreAdapter, useValue: mockCoreAdapter },
        ForYouService,
        { provide: ModuleRegistryService, useValue: mockModuleRegistryService },
        { provide: StorageService, useValue: mockStorageService },
        {
          provide: SettingsPersistenceService,
          useValue: mockSettingsPersistenceService,
        },
      ],
    });

    mockStorageService.get.mockReturnValue(Promise.resolve());
    service = TestBed.inject(ForYouService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getFeedItems', () => {
    it('should return an empty array when there is no content', (done) => {
      mockModuleRegistryService.getFeedables.mockReturnValue(of([]));

      service.getFilteredFeedItems(0).subscribe((feedItems) => {
        expect(feedItems).toEqual([]);
        done();
      });
    });
  });

  describe('getFeedItemsUnfiltered', () => {
    it('should return an empty array when there is no content', (done) => {
      mockModuleRegistryService.getFeedablesUnfiltered.mockReturnValue(of([]));

      service.getUnfilteredFeedItems(0).subscribe((feedItems) => {
        expect(feedItems).toEqual([]);
        done();
      });
    });
  });

  describe('getFeedItemsUpcoming', () => {
    it('should return an empty array when there is no content', (done) => {
      mockModuleRegistryService.getFeedablesUpcoming.mockReturnValue(of([]));
      service.getFilteredFeedItemsUpcoming(0).subscribe((feedItems) => {
        expect(feedItems).toEqual([]);
        done();
      });
    });

    it('should return upcoming feed items in the correct order', (done) => {
      service.getFilteredFeedItemsUpcoming(0).subscribe((feedItems) => {
        expect(feedItems[0].creationDate).toEqual(
          new Date('2023-10-20T08:00:00Z'),
        );
        expect(feedItems[1].creationDate).toEqual(
          new Date('2023-09-20T08:00:00Z'),
        );
        expect(feedItems[2].creationDate).toEqual(
          new Date('2023-08-20T08:00:00Z'),
        );
        done();
      });
    });
  });

  describe('getActivityFeedItems', () => {
    it('should return activity Feed Items', () => {
      service.getActivityFeedItems(0).subscribe((feedItems) => {
        expect(feedItems[0].creationDate).toEqual(
          new Date('2023-10-20T08:00:00Z'),
        );
        expect(feedItems[1].creationDate).toEqual(
          new Date('2023-09-20T08:00:00Z'),
        );
        expect(feedItems[2].creationDate).toEqual(
          new Date('2023-08-20T08:00:00Z'),
        );
      });
    });

    it('should return an empty array when there is no content', (done) => {
      mockModuleRegistryService.getFeedablesActivity.mockReturnValue(of([]));

      service.getActivityFeedItems(0).subscribe((feedItems) => {
        expect(feedItems).toEqual([]);
        done();
      });
    });
  });
});
