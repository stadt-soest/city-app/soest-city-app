import { inject, Injectable } from '@angular/core';
import { forkJoin, from, map, Observable, of, switchMap } from 'rxjs';
import { FeedItem } from '../../models/feed-item.model';
import { ModuleInfo } from '../../models/menu.model';
import { ModuleRegistryService } from '../module-registry/module-registry.service';
import { CoreAdapter } from '../../core.adapter';
import { FeedFilterCriteria } from '../../models/feed-filter-criteria.model';

@Injectable()
export class ForYouService {
  private readonly moduleRegistryService = inject(ModuleRegistryService);
  private readonly coreAdapter = inject(CoreAdapter);
  private readonly feedables: Observable<FeedItem[]>;
  private readonly feedablesUpcoming: Observable<FeedItem[]>;
  private readonly feedablesUnfiltered: Observable<FeedItem[]>;
  private readonly feedablesHighlighted: Observable<FeedItem[]>;
  private readonly feedablesLength$: Observable<number>;
  private readonly feedablesUpcomingLength$: Observable<number>;
  private readonly feedablesUnfilteredLength$: Observable<number>;

  private readonly moduleInfos: ModuleInfo[];

  constructor() {
    this.feedables = this.moduleRegistryService.getFeedables(0);
    this.feedablesUpcoming = this.moduleRegistryService.getFeedablesUpcoming(0);
    this.feedablesUnfiltered =
      this.moduleRegistryService.getFeedablesUnfiltered(0);
    this.feedablesHighlighted =
      this.moduleRegistryService.getHighlightedFeedables(0);
    this.feedablesLength$ = this.feedables.pipe(
      map((feedItems) => feedItems.length),
    );
    this.feedablesUpcomingLength$ = this.feedablesUpcoming.pipe(
      map((feedItems) => feedItems.length),
    );
    this.feedablesUnfilteredLength$ = this.feedablesUnfiltered.pipe(
      map((feedItems) => feedItems.length),
    );
    this.moduleInfos = this.moduleRegistryService.getModuleInfos();
  }

  hasContent(): Observable<boolean> {
    return this.feedablesLength$.pipe(map((length) => length > 0));
  }

  hasContentUpcoming(): Observable<boolean> {
    return this.feedablesUpcomingLength$.pipe(map((length) => length > 0));
  }

  hasContentUnfiltered(): Observable<boolean> {
    return this.feedablesUnfilteredLength$.pipe(map((length) => length > 0));
  }

  getFilteredFeedItems(page: number): Observable<FeedItem[]> {
    return this.moduleRegistryService.getAllModuleFullSettings().pipe(
      map((visibleModules) =>
        visibleModules.map((module) => ({
          type: module.feedItemType,
          sources: module.settings.sources,
          categoryIds: module.settings.categoryIds,
        })),
      ),
      switchMap((criteriaArray) => {
        const combinedCriteria = this.combineFilterCriteria(criteriaArray);
        return this.coreAdapter.getFeed(page, combinedCriteria);
      }),
    );
  }

  getHighlightedFeedItems(): Observable<FeedItem[]> {
    return this.feedablesHighlighted.pipe(
      switchMap((feedItems) => {
        if (feedItems.length === 0) {
          return of([]);
        }
        return of(feedItems);
      }),
    );
  }

  getFilteredFeedItemsUpcoming(page: number): Observable<FeedItem[]> {
    const threeMonthsFromNow = new Date();
    threeMonthsFromNow.setMonth(threeMonthsFromNow.getMonth() + 3);

    return this.moduleRegistryService.getFeedablesUpcoming(page).pipe(
      switchMap((feedItems) => {
        if (feedItems.length === 0) {
          return of([]);
        } else {
          return this.filterAndSortUpcomingFeedItems(
            feedItems,
            threeMonthsFromNow,
          );
        }
      }),
    );
  }

  getUnfilteredFeedItems(page: number): Observable<FeedItem[]> {
    return this.coreAdapter.getFeed(page);
  }

  getPriorityFeedItems(): Observable<FeedItem[]> {
    return this.moduleRegistryService.getPriorityFeedables().pipe(
      switchMap((feedItems) => {
        if (feedItems.length === 0) {
          return of([]);
        }
        return forkJoin(
          feedItems.map((item, index) =>
            this.getSingleFeedItemVisibility(item).pipe(
              map((visibleItem) =>
                visibleItem ? { item: visibleItem, index } : null,
              ),
            ),
          ),
        ).pipe(
          map((feedItemsWithIndex) =>
            feedItemsWithIndex.filter(
              (
                feedItemWithIndex,
              ): feedItemWithIndex is { item: FeedItem; index: number } =>
                feedItemWithIndex !== null,
            ),
          ),
          map((filteredItems) =>
            filteredItems.map((feedItemWithIndex) => feedItemWithIndex.item),
          ),
          map((mappedFeedItems) =>
            this.sortFeedItemsByRelevancy(mappedFeedItems),
          ),
        );
      }),
    );
  }

  getActivityFeedItems(page: number): Observable<FeedItem[]> {
    return this.moduleRegistryService.getFeedablesActivity(page).pipe(
      switchMap((feedItems) => {
        if (feedItems.length === 0) {
          return of([]);
        }
        return forkJoin(
          feedItems.map((item, index) =>
            this.getSingleFeedItemVisibility(item).pipe(
              map((visibleItem) =>
                visibleItem ? { item: visibleItem, index } : null,
              ),
            ),
          ),
        ).pipe(
          map((feedItemsWithIndex) =>
            feedItemsWithIndex.filter(
              (
                feedItemWithIndex,
              ): feedItemWithIndex is { item: FeedItem; index: number } =>
                feedItemWithIndex !== null,
            ),
          ),
          map((filteredItems) =>
            filteredItems.map((feedItemWithIndex) => feedItemWithIndex.item),
          ),
          map((mappedFeedItems) =>
            this.sortFeedItemsByRelevancy(mappedFeedItems),
          ),
        );
      }),
    );
  }

  private filterAndSortUpcomingFeedItems(
    feedItems: FeedItem[],
    cutoffDate: Date,
  ): Observable<FeedItem[]> {
    return forkJoin(
      feedItems.map((item, index) =>
        this.evaluateItemForUpcoming(item, index, cutoffDate),
      ),
    ).pipe(
      map((items) =>
        items.filter(
          (item): item is { item: FeedItem; index: number } => item !== null,
        ),
      ),
      map((items) =>
        [...items].sort((a, b) => a.index - b.index).map((item) => item.item),
      ), // Create a copy before sorting
    );
  }

  private evaluateItemForUpcoming(
    item: FeedItem,
    index: number,
    cutoffDate: Date,
  ): Observable<{
    item: FeedItem;
    index: number;
  } | null> {
    return this.getSingleFeedItemVisibility(item).pipe(
      map((visibleItem) => {
        if (visibleItem) {
          const itemDate = new Date(visibleItem.creationDate);
          if (itemDate <= cutoffDate) {
            return { item: visibleItem, index };
          }
        }
        return null;
      }),
    );
  }

  private sortFeedItemsByRelevancy(feedItems: FeedItem[]): FeedItem[] {
    return [...feedItems].sort((a, b) => b.relevancy - a.relevancy);
  }

  private getSingleFeedItemVisibility(
    item: FeedItem,
  ): Observable<FeedItem | null> {
    return from(
      this.moduleRegistryService.getValuesModuleVisibilitySettings(
        this.getMatchingModuleInfo(item),
      ),
    ).pipe(
      map((settingsValues) =>
        settingsValues.length > 0 && settingsValues[0] ? item : null,
      ),
    );
  }

  private getMatchingModuleInfo(item: FeedItem): ModuleInfo {
    return this.moduleRegistryService.getMatchingModuleInfo(
      this.moduleInfos,
      item.baseRoutePath,
    ) as ModuleInfo;
  }

  private combineFilterCriteria(
    criteriaArray: FeedFilterCriteria[],
  ): FeedFilterCriteria {
    const combined: FeedFilterCriteria = {
      type: undefined,
      sources: [],
      categoryIds: [],
    };

    for (const criteria of criteriaArray) {
      combined.type = combined.type ?? criteria.type;
      combined.sources?.push(...(criteria.sources ?? []));
      combined.categoryIds?.push(...(criteria.categoryIds ?? []));
    }

    return combined;
  }
}
