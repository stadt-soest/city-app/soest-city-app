import { inject, Injectable } from '@angular/core';
import { BehaviorSubject, forkJoin, map, Observable, of } from 'rxjs';
import { FeedItem } from '../../models/feed-item.model';
import { ModuleRegistryService } from '../module-registry/module-registry.service';
import { StorageService } from '../storage/storage.service';
import { PoiService } from '../poi/poi.service';
import { ModuleSearchResult } from '../../models/module-search-result.inteface';
import { FeatureModule } from '../../interfaces/feature-module.interface';

@Injectable()
export class SearchService<T extends FeedItem> {
  private readonly lastSearchesKey = 'last_searches';
  private readonly maxSearchHistory = 5;
  private readonly moduleRegistryService = inject(ModuleRegistryService);
  private readonly storageService = inject(StorageService);
  private readonly poiService = inject(PoiService);
  private readonly lastSearchTerms = new BehaviorSubject<string[]>([]);

  initLastSearches() {
    this.storageService.get(this.lastSearchesKey).then((lastSearchesString) => {
      const searches = lastSearchesString ? JSON.parse(lastSearchesString) : [];
      this.lastSearchTerms.next(searches);
    });
  }

  search(term: string): Observable<ModuleSearchResult<FeedItem>[]> {
    void this.updateSearchHistory(term);

    const poiSearchResults = this.searchPoisAsFeedItems(term);
    const featureModuleSearchResults = this.searchFeatureModules(term);

    return forkJoin([poiSearchResults, featureModuleSearchResults]).pipe(
      map(([poiResults, moduleResults]) => [poiResults, ...moduleResults]),
    );
  }

  searchByModule(
    term: string,
    moduleId: string,
    page = 1,
    size = 10,
  ): Observable<{
    items: T[];
    total: number;
  }> {
    const selectedModule = this.moduleRegistryService.getById(moduleId);

    if (selectedModule?.search) {
      return selectedModule.search(term, page, size);
    }

    return of({ items: [], total: 0 });
  }

  async updateSearchHistory(searchTerm: string) {
    try {
      const lastSearchesString = await this.storageService.get(
        this.lastSearchesKey,
      );
      let searches: string[] = lastSearchesString
        ? JSON.parse(lastSearchesString)
        : [];

      searches.unshift(searchTerm);

      searches = Array.from(new Set(searches)).slice(0, this.maxSearchHistory);

      await this.storageService.set(
        this.lastSearchesKey,
        JSON.stringify(searches),
      );

      this.lastSearchTerms.next(searches);
    } catch (error) {
      console.error('Error updating search history:', error);
    }
  }

  getLastSearches(): Observable<string[]> {
    return this.lastSearchTerms.asObservable();
  }

  async deleteSearchTerm(term: string) {
    try {
      const lastSearchesString = await this.storageService.get(
        this.lastSearchesKey,
      );
      let searches: string[] = lastSearchesString
        ? JSON.parse(lastSearchesString)
        : [];

      searches = searches.filter((search) => search !== term);

      await this.storageService.set(
        this.lastSearchesKey,
        JSON.stringify(searches),
      );

      this.lastSearchTerms.next(searches);
    } catch (error) {
      console.error('Error deleting search term:', error);
    }
  }

  getSortedModuleIdsByAggregateRelevancy(feedItems: FeedItem[]): string[] {
    const aggregateRelevancyByModule = new Map<string, number>();

    feedItems.forEach((item) => {
      aggregateRelevancyByModule.set(item.moduleId, item.relevancy);
    });

    return Array.from(aggregateRelevancyByModule)
      .sort((a, b) => b[1] - a[1])
      .map(([moduleId]) => moduleId);
  }

  private createSearchObservableForModule(
    module: FeatureModule<T>,
    term: string,
  ): Observable<ModuleSearchResult<T>> {
    if (module.search) {
      return module.search(term).pipe(
        map((result) => ({
          moduleId: module.info.id,
          items: result.items,
          total: result.total,
        })),
      );
    } else {
      return of({
        moduleId: module.info.id,
        items: [],
        total: 0,
      });
    }
  }

  private searchPoisAsFeedItems(
    term: string,
  ): Observable<ModuleSearchResult<FeedItem>> {
    return this.poiService
      .searchPois(term)
      .pipe(map((items) => ({ moduleId: 'map', items, total: items.length })));
  }

  private searchFeatureModules(
    term: string,
  ): Observable<ModuleSearchResult<FeedItem>[]> {
    const featureModules = this.moduleRegistryService.getFeatureModules();
    return forkJoin(
      featureModules
        .filter((module) => module.search)
        .map((module) => this.createSearchObservableForModule(module, term)),
    );
  }
}
