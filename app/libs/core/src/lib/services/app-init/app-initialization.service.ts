import { inject, Injectable } from '@angular/core';
import { IonTabs, Platform } from '@ionic/angular/standalone';
import { App } from '@capacitor/app';
import { ScreenOrientationService } from './screen-orientation.service';
import { DeviceService } from '../device/device.service';
import { DocumentLanguageService } from './document-language.service';
import { AppUrlOpeningService } from './app-url-opening.service';
import { KeyboardService } from '../keyboard/keyboard.service';
import { GeolocationService } from '../geolocation/geolocation.service';
import { ConnectivityService } from '../connectivty/connectivity.service';

@Injectable()
export class AppInitializationService {
  private readonly deviceService = inject(DeviceService);
  private readonly documentLanguageService = inject(DocumentLanguageService);
  private readonly appUrlOpeningService = inject(AppUrlOpeningService);
  private readonly platform = inject(Platform);
  private readonly screenOrientationService = inject(ScreenOrientationService);
  private readonly keyboardService = inject(KeyboardService);
  private readonly geolocationService = inject(GeolocationService);
  private readonly conectivityService = inject(ConnectivityService);

  initializeApp() {
    this.deviceService.init();
    this.deviceService.listenToNativeLightOrDarkModeChanges();
    this.conectivityService.initializeNetworkStatus();
    this.documentLanguageService.setDocumentLanguage();
    this.appUrlOpeningService.initialize();
    this.setOrientationOnPlatformReady();
    this.initializeKeyboardService();
    this.geolocationService.initializePermissionStatus();
  }

  setupBackButtonHandler(ionTabs: IonTabs) {
    this.platform.backButton.subscribeWithPriority(-1, () => {
      if (!ionTabs.outlet.canGoBack()) {
        App.exitApp();
      }
    });
  }

  private setOrientationOnPlatformReady() {
    this.platform.ready().then(() => {
      this.screenOrientationService.setOrientationBasedOnDevice();
    });
  }

  private async initializeKeyboardService() {
    await this.keyboardService.initialize();
  }
}
