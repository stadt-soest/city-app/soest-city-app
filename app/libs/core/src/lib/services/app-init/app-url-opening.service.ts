import { inject, Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { App, URLOpenListenerEvent } from '@capacitor/app';

@Injectable()
export class AppUrlOpeningService {
  private readonly router = inject(Router);
  private readonly zone = inject(NgZone);

  initialize() {
    App.addListener('appUrlOpen', (event: URLOpenListenerEvent) => {
      this.zone.run(() => {
        try {
          const url: URL = new URL(event.url);
          this.router.navigateByUrl(url.pathname + url.search + url.hash);
        } catch (err) {
          console.error('Could not open url inside app', event.url);
        }
      });
    });
  }
}
