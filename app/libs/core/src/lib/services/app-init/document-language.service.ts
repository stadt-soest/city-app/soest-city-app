import { inject, Injectable } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';

@Injectable()
export class DocumentLanguageService {
  private readonly translocoService = inject(TranslocoService);

  setDocumentLanguage() {
    document.documentElement.lang = this.translocoService.getActiveLang();
  }
}
