import { inject, Injectable } from '@angular/core';
import { Platform } from '@ionic/angular/standalone';
import { ScreenOrientation } from '@capacitor/screen-orientation';

@Injectable()
export class ScreenOrientationService {
  private readonly platform = inject(Platform);

  async setOrientationBasedOnDevice() {
    if (this.platform.is('tablet')) {
      await ScreenOrientation.unlock();
    } else {
      await this.lockScreenOrientation();
    }
  }

  private async lockScreenOrientation() {
    if (this.platform.is('hybrid')) {
      await ScreenOrientation.lock({ orientation: 'portrait' });
    }
  }
}
