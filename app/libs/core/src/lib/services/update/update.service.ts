import { inject, Injectable } from '@angular/core';
import { SwUpdate, VersionReadyEvent } from '@angular/service-worker';
import { filter } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import {
  AppUpdate,
  AppUpdateAvailability,
} from '@capawesome/capacitor-app-update';
import { Capacitor } from '@capacitor/core';

@Injectable()
export class UpdateService {
  updateAvailable$: Observable<boolean>;
  private readonly swUpdate = inject(SwUpdate);
  private readonly updateAvailableSubject = new BehaviorSubject<boolean>(false);

  constructor() {
    this.updateAvailable$ = this.updateAvailableSubject.asObservable();
    this.checkForUpdates();
  }

  public async performUpdate(): Promise<void> {
    if (Capacitor.isNativePlatform()) {
      await AppUpdate.openAppStore();
    } else {
      window.location.reload();
    }
  }

  private checkForUpdates(): void {
    if (Capacitor.isNativePlatform()) {
      void this.checkForNativeUpdate();
    } else {
      this.checkForWebUpdate();
    }
  }

  private checkForWebUpdate(): void {
    if (this.swUpdate.isEnabled) {
      this.swUpdate.versionUpdates
        .pipe(
          filter(
            (event): event is VersionReadyEvent =>
              event.type === 'VERSION_READY',
          ),
        )
        .subscribe(() => {
          this.updateAvailableSubject.next(true);
        });
    }
  }

  private async checkForNativeUpdate(): Promise<void> {
    const appUpdateInfo = await AppUpdate.getAppUpdateInfo();
    if (
      appUpdateInfo.updateAvailability ===
      AppUpdateAvailability.UPDATE_AVAILABLE
    ) {
      this.updateAvailableSubject.next(true);
    }
  }
}
