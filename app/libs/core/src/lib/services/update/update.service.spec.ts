import { TestBed } from '@angular/core/testing';
import { SwUpdate, VersionReadyEvent } from '@angular/service-worker';
import { BehaviorSubject, first, Subject } from 'rxjs';
import { DeviceService } from '../device/device.service';
import { UpdateService } from './update.service';

describe('UpdateService', () => {
  let service: UpdateService;
  let versionUpdatesSubject: Subject<VersionReadyEvent>;

  beforeEach(() => {
    versionUpdatesSubject = new BehaviorSubject<VersionReadyEvent>({
      type: 'VERSION_READY',
      currentVersion: { hash: 'old-hash', appData: undefined },
      latestVersion: { hash: 'new-hash', appData: undefined },
    });

    const mockSwUpdate = {
      isEnabled: true,
      versionUpdates: versionUpdatesSubject.asObservable(),
    };

    TestBed.configureTestingModule({
      providers: [
        DeviceService,
        UpdateService,
        { provide: SwUpdate, useValue: mockSwUpdate },
      ],
    });

    service = TestBed.inject(UpdateService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should subscribe to version updates on initialization', () => {
    expect(service.updateAvailable$).toBeTruthy();
  });

  it('should set updateAvailable$ to true on version ready', (done) => {
    service.updateAvailable$.pipe(first()).subscribe((isAvailable) => {
      expect(isAvailable).toBe(true);
      done();
    });

    versionUpdatesSubject.next({
      type: 'VERSION_READY',
      currentVersion: { hash: 'old-hash', appData: undefined },
      latestVersion: { hash: 'new-hash', appData: undefined },
    });
  });
});
