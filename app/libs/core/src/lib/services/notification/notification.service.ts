import { Injectable } from '@angular/core';
import { LocalNotifications } from '@capacitor/local-notifications';
import { NotificationDetails } from '../../interfaces/notification-details.interface';

@Injectable()
export class NotificationService {
  async scheduleNotification(notificationDetails: NotificationDetails) {
    await LocalNotifications.schedule({
      notifications: [
        {
          title: notificationDetails.title,
          body: notificationDetails.body,
          id: notificationDetails.id,
          schedule: { at: new Date(notificationDetails.scheduleTime) },
          smallIcon: 'notification_mask',
          iconColor: '#da121a',
          largeIcon: notificationDetails.drawableResourceId,
        },
      ],
    });
  }

  async cancelAllNotifications() {
    const { notifications } = await LocalNotifications.getPending();
    const idsToCancel = notifications.map((notification) => ({
      id: notification.id,
    }));

    if (idsToCancel.length > 0) {
      await LocalNotifications.cancel({ notifications: idsToCancel });
    }
  }
}
