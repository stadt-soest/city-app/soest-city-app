import { inject, Injectable } from '@angular/core';
import {
  BehaviorSubject,
  catchError,
  forkJoin,
  map,
  Observable,
  of,
} from 'rxjs';
import { CoreAdapter } from '../../core.adapter';

@Injectable()
export class ImageUploadHandler {
  private readonly coreAdapter = inject(CoreAdapter);
  private readonly selectedImages = new BehaviorSubject<SelectedImage[]>([]);

  get images$(): Observable<SelectedImage[]> {
    return this.selectedImages.asObservable();
  }

  addFiles(files: FileList): void {
    const newImages: SelectedImage[] = Array.from(files).map((file) => ({
      file,
      url: URL.createObjectURL(file),
      status: 'ready',
    }));
    this.selectedImages.next([...this.selectedImages.value, ...newImages]);
  }

  removeImage(index: number): void {
    const images = this.selectedImages.value;
    images.splice(index, 1);
    this.selectedImages.next([...images]);
  }

  hasPendingImages(): boolean {
    return this.selectedImages.value.some(
      (image) => image.status === 'ready' || image.status === 'failed',
    );
  }

  reset(): void {
    this.selectedImages.next([]);
  }

  uploadImages(): Observable<{
    uploadedFilenames: string[];
    failedFiles: File[];
  }> {
    const uploadTasks$ = this.selectedImages.value
      .filter((image) => image.status === 'ready' || image.status === 'failed')
      .map((image) => this.prepareUploadTask(image));

    return forkJoin(uploadTasks$).pipe(
      map((results) => this.handleUploadResults(results)),
    );
  }

  private prepareUploadTask(image: SelectedImage): Observable<UploadResult> {
    image.status = 'uploading';
    this.updateImage(image);

    return this.uploadSingleImage(image.file).pipe(
      map((filename) => {
        image.status = 'success';
        image.uploadFilename = filename ?? '';
        this.updateImage(image);
        return {
          status: 'success',
          file: image.file,
          filename,
        } as UploadSuccess;
      }),
      catchError((error) => {
        console.error('Error uploading file', error);
        image.status = 'failed';
        image.errorMessage = 'Upload failed';
        this.updateImage(image);
        return of({ status: 'failed', file: image.file } as UploadFailure);
      }),
    );
  }

  private updateImage(updatedImage: SelectedImage) {
    const images = this.selectedImages.value;
    const index = images.findIndex((image) => image.file === updatedImage.file);
    if (index !== -1) {
      images[index] = updatedImage;
      this.selectedImages.next([...images]);
    }
  }

  private handleUploadResults(results: UploadResult[]): {
    uploadedFilenames: string[];
    failedFiles: File[];
  } {
    const successfulUploads = results.filter(
      (result): result is UploadSuccess => result.status === 'success',
    );
    const failedUploads = results.filter(
      (result): result is UploadFailure => result.status === 'failed',
    );

    successfulUploads.forEach((upload) =>
      this.updateImageStatus(upload, 'success'),
    );
    failedUploads.forEach((upload) => this.updateImageStatus(upload, 'failed'));

    return {
      uploadedFilenames: successfulUploads.map((upload) => upload.filename),
      failedFiles: failedUploads.map((upload) => upload.file),
    };
  }

  private updateImageStatus(
    upload: UploadResult,
    status: SelectedImage['status'],
  ): void {
    const images = this.selectedImages.value;
    const imageIndex = images.findIndex((img) => img.file === upload.file);
    if (imageIndex !== -1) {
      images[imageIndex].status = status;
      if (status === 'success') {
        images[imageIndex].uploadFilename = (upload as UploadSuccess).filename;
      } else {
        images[imageIndex].errorMessage = 'Upload failed';
      }
      this.selectedImages.next([...images]);
    }
  }

  private uploadSingleImage(file: File): Observable<string | null> {
    return this.coreAdapter.uploadSingleImage(file);
  }
}

type UploadResult = UploadSuccess | UploadFailure;

export interface SelectedImage {
  file: File;
  url: string;
  status: 'ready' | 'uploading' | 'success' | 'failed';
  errorMessage?: string;
  uploadFilename?: string;
}

interface UploadSuccess {
  status: 'success';
  file: File;
  filename: string;
}

interface UploadFailure {
  status: 'failed';
  file: File;
}
