import { TestBed } from '@angular/core/testing';
import { ImageUploadHandler } from './image-upload-handler.service';
import { of } from 'rxjs';
import { CoreAdapter } from '../../core.adapter';
import { CoreTestModule } from '../../test/core-test.module';

describe('ImageUploadHandler', () => {
  let service: ImageUploadHandler;
  let coreAdapterMock: Partial<CoreAdapter>;

  beforeEach(() => {
    global.URL.createObjectURL = jest.fn(() => 'http://mocked.url/objectURL');

    coreAdapterMock = {
      uploadSingleImage: jest
        .fn()
        .mockImplementation(() => of('mockUploadedFilename')),
    };

    TestBed.configureTestingModule({
      imports: [CoreTestModule.forRoot()],
      providers: [
        ImageUploadHandler,
        { provide: CoreAdapter, useValue: coreAdapterMock },
      ],
    });

    service = TestBed.inject(ImageUploadHandler);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add files correctly', (done) => {
    const file = new File(['content'], 'test.png', { type: 'image/png' });
    const mockFileList: FileList = [file] as unknown as FileList;

    service.addFiles(mockFileList);

    service.images$.subscribe((images) => {
      if (images.length > 0) {
        expect(images.length).toBe(1);
        expect(images[0].file.name).toBe('test.png');
        done();
      }
    });
  });

  it('should remove an image correctly', (done) => {
    service.addFiles(
      new File(['content'], 'test.png', {
        type: 'image/png',
      }) as unknown as FileList,
    );
    service.removeImage(0);
    service.images$.subscribe((images) => {
      expect(images.length).toBe(0);
      done();
    });
  });
});
