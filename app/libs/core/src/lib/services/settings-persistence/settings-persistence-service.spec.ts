import { TestBed } from '@angular/core/testing';
import { createModuleInfo } from '../../test/shared/test.util';
import { JsonParsingService } from '../json/json-parsing.service';
import { ModuleCategory, ModuleInfo } from '../../models/menu.model';
import { SettingsPersistenceService } from './settings-persistence-service';
import { StorageKeyType } from '../../enums/storage-key.type';
import { StorageService } from '../storage/storage.service';

describe('SettingsPersistenceService', () => {
  let service: SettingsPersistenceService;
  let storageServiceMock: jest.Mocked<StorageService>;
  let jsonParsingServiceMock: jest.Mocked<JsonParsingService>;

  beforeEach(() => {
    storageServiceMock = {
      set: jest.fn(),
      get: jest.fn(),
      remove: jest.fn(),
      generateStorageKey: jest
        .fn()
        .mockReturnValue('sourceNeuigkeitenStorageKey'),
      getKeys: jest.fn(),
      getJson: jest.fn(),
    } as unknown as jest.Mocked<StorageService>;

    jsonParsingServiceMock = {
      parseJson: jest.fn().mockResolvedValue({ key1: true, key2: false }),
      stringifyJson: jest.fn().mockReturnValue('{"key1":true,"key2":false}'),
    } as unknown as jest.Mocked<JsonParsingService>;

    TestBed.configureTestingModule({
      providers: [
        SettingsPersistenceService,
        { provide: StorageService, useValue: storageServiceMock },
        { provide: JsonParsingService, useValue: jsonParsingServiceMock },
      ],
    });

    service = TestBed.inject(SettingsPersistenceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('setSettings', () => {
    it('should save settings using the storage service', async () => {
      const moduleInfo: ModuleInfo = createModuleInfo({
        id: 'news',
        name: 'Neuigkeiten',
        category: ModuleCategory.information,
        icon: 'campaign',
        baseRoutingPath: '/news',
        defaultVisibility: true,
        settingsPageAvailable: true,
      });
      const settings = { key1: true, key2: false };
      const expectedStorageKey = 'sourceNeuigkeitenStorageKey';

      await service.saveSettings(moduleInfo, StorageKeyType.SOURCE, settings);
      expect(storageServiceMock.set).toHaveBeenCalledWith(
        expectedStorageKey,
        JSON.stringify(settings),
      );
    });
  });

  describe('getSettings', () => {
    it('should retrieve settings using the storage service', async () => {
      const moduleInfo: ModuleInfo = createModuleInfo({
        name: 'Neuigkeiten',
        category: ModuleCategory.information,
        icon: 'campaign',
        baseRoutingPath: '/news',
      });

      const expectedStorageKey = 'sourceNeuigkeitenStorageKey';
      await service.getSettings(moduleInfo, StorageKeyType.SOURCE);
      expect(storageServiceMock.get).toHaveBeenCalledWith(expectedStorageKey);
    });
  });

  describe('getEnabledSettingsKeys', () => {
    const moduleInfo: ModuleInfo = createModuleInfo({
      name: 'Neuigkeiten',
      category: ModuleCategory.information,
      icon: 'campaign',
      baseRoutingPath: '/news',
    });

    it('should return enabled settings keys', async () => {
      const settings = { key1: true, key2: false };
      storageServiceMock.get.mockResolvedValue(JSON.stringify(settings));
      const result = await service.getEnabledSettingsKeys(
        moduleInfo,
        StorageKeyType.SOURCE,
      );
      expect(result).toEqual(['key1']);
    });

    it('should return an empty array if no settings are found', async () => {
      storageServiceMock.get.mockResolvedValue(null);
      const result = await service.getEnabledSettingsKeys(
        moduleInfo,
        StorageKeyType.SOURCE,
      );
      expect(result).toEqual([]);
    });
  });

  describe('getSettingsValues', () => {
    const moduleInfo: ModuleInfo = createModuleInfo({
      name: 'Neuigkeiten',
      category: ModuleCategory.information,
      icon: 'campaign',
      baseRoutingPath: '/news',
    });

    it('should return enabled settings keys', async () => {
      const settings = { key1: true, key2: false };
      storageServiceMock.get.mockResolvedValue(JSON.stringify(settings));
      const result = await service.getSettingsValues(
        moduleInfo,
        StorageKeyType.SOURCE,
      );
      expect(result).toEqual([true, false]);
    });

    it('should return an empty array if no settings are found', async () => {
      storageServiceMock.get.mockResolvedValue(null);
      const result = await service.getSettingsValues(
        moduleInfo,
        StorageKeyType.SOURCE,
      );
      expect(result).toEqual([]);
    });

    it('should return an empty array if settings are empty', async () => {
      storageServiceMock.get.mockResolvedValue(JSON.stringify({}));
      const result = await service.getSettingsValues(
        moduleInfo,
        StorageKeyType.SOURCE,
      );
      expect(result).toEqual([]);
    });
  });
});
