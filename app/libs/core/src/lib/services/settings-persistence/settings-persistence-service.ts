import { inject, Injectable } from '@angular/core';
import { StorageService } from '../storage/storage.service';
import { JsonParsingService } from '../json/json-parsing.service';
import { ModuleInfo } from '../../models/menu.model';
import { StorageKeyType } from '../../enums/storage-key.type';

@Injectable()
export class SettingsPersistenceService {
  private readonly storageService = inject(StorageService);
  private readonly jsonParsingService = inject(JsonParsingService);

  async saveSettings(
    moduleInfo: ModuleInfo,
    storageKeyType: StorageKeyType,
    settings: { [key: string]: boolean | string } | object,
  ): Promise<void> {
    const settingsString = this.jsonParsingService.stringifyJson(settings);
    await this.storageService.set(
      this.generateKey(moduleInfo, storageKeyType),
      settingsString,
    );
  }

  async getSettings(
    moduleInfo: ModuleInfo,
    storageKeyType: StorageKeyType,
  ): Promise<any> {
    return await this.storageService.get(
      this.generateKey(moduleInfo, storageKeyType),
    );
  }

  async getEnabledSettingsKeys(
    moduleInfo: ModuleInfo,
    storageKeyType: StorageKeyType,
  ): Promise<string[]> {
    const sourcesString = await this.storageService.get(
      this.generateKey(moduleInfo, storageKeyType),
    );
    if (sourcesString) {
      const sourcesObject = JSON.parse(sourcesString);
      return Object.keys(sourcesObject).filter((key) => sourcesObject[key]);
    }
    return [];
  }

  async getSettingsValues(
    moduleInfo: ModuleInfo,
    storageKeyType: StorageKeyType,
  ): Promise<(boolean | string)[]> {
    const settingsString = await this.storageService.get(
      this.generateKey(moduleInfo, storageKeyType),
    );
    if (settingsString) {
      const settingsObject = JSON.parse(settingsString);
      return Object.values(settingsObject);
    }
    return [];
  }

  async getStorage(): Promise<string> {
    const keys = (await this.storageService.getKeys()) || [];
    const settingsObject: { [key: string]: any } = {};

    for (const key of keys) {
      settingsObject[key] = await this.storageService.get(key);
    }
    return JSON.stringify(settingsObject);
  }

  async removeSettings(moduleInfo: ModuleInfo, storageKeyType: StorageKeyType) {
    await this.storageService.remove(
      this.generateKey(moduleInfo, storageKeyType),
    );
  }

  async updateSettings(
    moduleInfo: ModuleInfo,
    newItems: string[],
    existingSettings: { [key: string]: boolean | string },
    keyType: StorageKeyType,
  ): Promise<void> {
    const updatedSettings = { ...existingSettings };

    newItems.forEach((item) => {
      if (!(item in existingSettings)) {
        updatedSettings[item] = true;
      }
    });

    Object.keys(updatedSettings).forEach((key) => {
      if (!newItems.includes(key)) {
        delete updatedSettings[key];
      }
    });

    if (this.settingsHaveChanged(existingSettings, updatedSettings)) {
      await this.saveSettings(moduleInfo, keyType, updatedSettings);
    }
  }

  private generateKey(
    moduleInfo: ModuleInfo,
    storageKeyType: StorageKeyType,
  ): string {
    return this.storageService.generateStorageKey(moduleInfo, storageKeyType);
  }

  private settingsHaveChanged(
    originalSettings: { [key: string]: boolean | string },
    updatedSettings: { [key: string]: boolean | string },
  ): boolean {
    return (
      Object.keys(originalSettings).length !==
        Object.keys(updatedSettings).length ||
      !Object.keys(updatedSettings).every(
        (key) =>
          Object.prototype.hasOwnProperty.call(originalSettings, key) &&
          originalSettings[key] === updatedSettings[key],
      )
    );
  }
}
