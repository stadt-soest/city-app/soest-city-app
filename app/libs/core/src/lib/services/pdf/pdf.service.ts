import { inject, Injectable } from '@angular/core';
import { FileOpener } from '@capacitor-community/file-opener';
import { Directory, Filesystem } from '@capacitor/filesystem';
import { BrowserService } from '../browser/browser.service';
import { DeviceService } from '../device/device.service';

@Injectable()
export class PdfService {
  private readonly deviceService = inject(DeviceService);
  private readonly browserService = inject(BrowserService);

  async openPDF(filePath: string) {
    if (this.deviceService.isWebPlatform()) {
      this.browserService.openInAppBrowser(filePath);
    } else {
      await this.openPdfOnDevice(filePath);
    }
  }

  private async openPdfOnDevice(filePath: string) {
    try {
      const response = await fetch(filePath);
      const blob = await response.blob();
      const base64Data = await this.convertBlobToBase64(blob);

      const fileName = filePath.substring(filePath.lastIndexOf('/') + 1);

      await Filesystem.writeFile({
        path: fileName,
        data: base64Data,
        directory: Directory.Cache,
      });

      const fileUri = await Filesystem.getUri({
        directory: Directory.Cache,
        path: fileName,
      });

      const path = fileUri.uri;

      await FileOpener.open({
        filePath: path,
        contentType: 'application/pdf',
      });
    } catch (e) {
      console.error('Error opening PDF file', e);
    }
  }

  private convertBlobToBase64(blob: Blob): Promise<string> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onerror = reject;
      reader.onload = () => {
        const dataUrl = reader.result as string;
        const base64 = dataUrl.split(',')[1];
        resolve(base64);
      };
      reader.readAsDataURL(blob);
    });
  }
}
