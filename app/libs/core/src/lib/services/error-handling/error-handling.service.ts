import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { ModuleInfo } from '../../models/menu.model';
import { ErrorInfo } from '../../interfaces/ErrorInfo.interface';
import { ErrorType } from '../../enums/error-type';

@Injectable()
export class ErrorHandlingService {
  private readonly errors = new BehaviorSubject<{ [key: string]: ErrorInfo[] }>(
    {},
  );

  setErrorState(
    moduleInfo: ModuleInfo,
    hasError: boolean,
    errorType: ErrorType,
    id?: string,
  ): void {
    const errorInfo: ErrorInfo = { hasError, errorType, id };
    const currentErrors = this.errors.value[moduleInfo.name] || [];
    this.errors.next({
      ...this.errors.value,
      [moduleInfo.name]: [...currentErrors, errorInfo],
    });
  }

  getErrorState(): Observable<{ [key: string]: ErrorInfo[] }> {
    return this.errors.asObservable();
  }

  hasErrorInSpecificModule(moduleInfo: ModuleInfo): Observable<boolean> {
    return this.getErrorState().pipe(
      map((errorStates) => {
        const errorInfos = errorStates[moduleInfo.name] || [];
        return errorInfos.some(
          (errorInfo) =>
            errorInfo.hasError &&
            errorInfo.errorType === ErrorType.MODULE_API_ERROR,
        );
      }),
    );
  }

  hasErrorInAnyModule(): Observable<boolean> {
    return this.getErrorState().pipe(
      map((errorStates) =>
        Object.values(errorStates).some((errorInfos) =>
          errorInfos.some(
            (errorInfo) =>
              errorInfo.hasError &&
              errorInfo.errorType === ErrorType.MODULE_API_ERROR,
          ),
        ),
      ),
    );
  }

  hasErrorInDetails(moduleInfo: ModuleInfo, id: string): Observable<boolean> {
    return this.getErrorState().pipe(
      map((errorStates) => {
        const errorInfos = errorStates[moduleInfo.name] || [];
        return errorInfos.some(
          (errorInfo) =>
            errorInfo.hasError &&
            errorInfo.errorType === ErrorType.DETAILS_API_ERROR &&
            errorInfo.id === id,
        );
      }),
    );
  }

  hasErrorInAllModules(): Observable<boolean> {
    return this.getErrorState().pipe(
      map((errorStates) =>
        Object.keys(errorStates).every((moduleName) => {
          const errorInfos = errorStates[moduleName];
          return errorInfos.some(
            (errorInfo) =>
              errorInfo.hasError &&
              errorInfo.errorType === ErrorType.MODULE_API_ERROR,
          );
        }),
      ),
    );
  }
}
