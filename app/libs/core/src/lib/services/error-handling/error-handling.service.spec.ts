import { TestBed } from '@angular/core/testing';
import { ErrorHandlingService } from './error-handling.service';
import { ErrorType } from '../../enums/error-type';
import { ModuleCategory, ModuleInfo } from '../../models/menu.model';

describe('ErrorHandlingService', () => {
  let service: ErrorHandlingService;
  const moduleInfo1: ModuleInfo = {
    id: 'moduleId1',
    name: 'module1',
    category: ModuleCategory.information,
    icon: 'moduleIcon1',
    baseRoutingPath: '/module1',
    defaultVisibility: true,
    settingsPageAvailable: false,
    isAboutAppItself: false,
    relevancy: 0.5,
    forYouRelevant: true,
    showInCategoriesPage: true,
    orderInCategory: 1,
  };

  const moduleInfo2: ModuleInfo = {
    id: 'moduleId2',
    name: 'module2',
    category: ModuleCategory.information,
    icon: 'moduleIcon2',
    baseRoutingPath: '/module2',
    defaultVisibility: true,
    settingsPageAvailable: false,
    isAboutAppItself: false,
    relevancy: 0.5,
    forYouRelevant: true,
    showInCategoriesPage: true,
    orderInCategory: 1,
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ErrorHandlingService],
    });
    service = TestBed.inject(ErrorHandlingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('setErrorState', () => {
    it('should add error info to the specified module', (done) => {
      service.setErrorState(moduleInfo1, true, ErrorType.MODULE_API_ERROR, '1');
      service.setErrorState(moduleInfo2, true, ErrorType.MODULE_API_ERROR, '2');
      service.getErrorState().subscribe((errors) => {
        expect(errors['module1']).toHaveLength(1);

        expect(errors['module1'][0]).toEqual({
          hasError: true,
          errorType: ErrorType.MODULE_API_ERROR,
          id: '1',
        });
        expect(errors['module2'][0]).toEqual({
          hasError: true,
          errorType: ErrorType.MODULE_API_ERROR,
          id: '2',
        });
        done();
      });
    });
  });

  describe('getErrorState', () => {
    it('should return an Observable of the current error state', (done) => {
      service.getErrorState().subscribe((errors) => {
        expect(errors).toEqual({});
        done();
      });
    });
  });

  describe('singleModuleErrored', () => {
    it('should return true if the specified module has a MODULE_API_ERROR', (done) => {
      service.setErrorState(moduleInfo1, true, ErrorType.MODULE_API_ERROR, '1');
      service.setErrorState(
        moduleInfo2,
        false,
        ErrorType.MODULE_API_ERROR,
        '2',
      );
      service.hasErrorInSpecificModule(moduleInfo2).subscribe((errored) => {
        expect(errored).toBe(false);
        done();
      });
    });
  });

  it('should return false if the specified module does not have a MODULE_API_ERROR', (done) => {
    service.setErrorState(moduleInfo1, false, ErrorType.MODULE_API_ERROR);
    service.setErrorState(moduleInfo2, true, ErrorType.MODULE_API_ERROR);

    service.hasErrorInSpecificModule(moduleInfo1).subscribe((errored) => {
      expect(errored).toBe(false);
      done();
    });
  });

  describe('hasErrorInAnyModule', () => {
    it('should return true if any module has a MODULE_API_ERROR', (done) => {
      service.setErrorState(moduleInfo1, true, ErrorType.MODULE_API_ERROR);
      service.setErrorState(moduleInfo2, false, ErrorType.MODULE_API_ERROR);
      service.hasErrorInAnyModule().subscribe((errored) => {
        expect(errored).toBe(true);
        done();
      });
    });
    it('should return false if no module has a MODULE_API_ERROR', (done) => {
      service.setErrorState(moduleInfo1, false, ErrorType.MODULE_API_ERROR);
      service.setErrorState(moduleInfo2, false, ErrorType.MODULE_API_ERROR);
      service.hasErrorInAnyModule().subscribe((errored) => {
        expect(errored).toBe(false);
        done();
      });
    });
  });

  describe('hasErrorInSpecificModule', () => {
    it('should return true if the specified module has a MODULE_API_ERROR', (done) => {
      service.setErrorState(moduleInfo1, false, ErrorType.MODULE_API_ERROR);
      service.setErrorState(moduleInfo2, true, ErrorType.MODULE_API_ERROR);
      service.hasErrorInSpecificModule(moduleInfo2).subscribe((errored) => {
        expect(errored).toBe(true);
        done();
      });
    });
    it('should return false if the specified module does not have a MODULE_API_ERROR', (done) => {
      service.setErrorState(moduleInfo1, false, ErrorType.MODULE_API_ERROR);
      service.setErrorState(moduleInfo2, true, ErrorType.MODULE_API_ERROR);
      service.hasErrorInSpecificModule(moduleInfo1).subscribe((errored) => {
        expect(errored).toBe(false);
        done();
      });
    });
  });

  describe('hasErrorInDetails', () => {
    it('should return true if the specified module has a DETAILS_API_ERROR', (done) => {
      service.setErrorState(
        moduleInfo1,
        false,
        ErrorType.DETAILS_API_ERROR,
        '1',
      );
      service.setErrorState(
        moduleInfo2,
        true,
        ErrorType.DETAILS_API_ERROR,
        '2',
      );
      service.hasErrorInDetails(moduleInfo2, '2').subscribe((errored) => {
        expect(errored).toBe(true);
        done();
      });
    });
    it('should return false if the specified module does not have a DETAILS_API_ERROR', (done) => {
      service.setErrorState(
        moduleInfo1,
        false,
        ErrorType.DETAILS_API_ERROR,
        '1',
      );
      service.setErrorState(
        moduleInfo2,
        true,
        ErrorType.DETAILS_API_ERROR,
        '2',
      );
      service.hasErrorInDetails(moduleInfo1, '1').subscribe((errored) => {
        expect(errored).toBe(false);
        done();
      });
    });
  });

  describe('hasErrorInAllModules', () => {
    it('should return true if all modules have a MODULE_API_ERROR', (done) => {
      service.setErrorState(moduleInfo1, true, ErrorType.MODULE_API_ERROR);
      service.setErrorState(moduleInfo2, true, ErrorType.MODULE_API_ERROR);
      service.hasErrorInAllModules().subscribe((allErrored) => {
        expect(allErrored).toBe(true);
        done();
      });
    });

    it('should return false if not all modules have a MODULE_API_ERROR', (done) => {
      service.setErrorState(moduleInfo1, true, ErrorType.MODULE_API_ERROR);
      service.setErrorState(moduleInfo2, false, ErrorType.MODULE_API_ERROR);
      service.hasErrorInAllModules().subscribe((allErrored) => {
        expect(allErrored).toBe(false);
        done();
      });
    });
  });
});
