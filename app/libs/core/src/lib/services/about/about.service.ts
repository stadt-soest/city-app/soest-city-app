import { inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { About } from '../../models/about.model';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class AboutService {
  private readonly http = inject(HttpClient);
  private about: About = { appVersion: '0' };

  async loadAbout(): Promise<About> {
    return firstValueFrom(
      this.http.get<About>('/shared/assets/config/dynamic/about.json'),
    ).then(
      (data: About) => (this.about = data),
      (error) => {
        console.error('Failed to load about.json:', error);
        this.about = { appVersion: '0' };
        return this.about;
      },
    );
  }

  get(): About {
    return this.about;
  }
}
