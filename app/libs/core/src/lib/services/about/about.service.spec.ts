import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { AboutService } from './about.service';
import { About } from '../../models/about.model';

describe('AboutService', () => {
  let service: AboutService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AboutService],
      imports: [HttpClientTestingModule],
    });

    service = TestBed.inject(AboutService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('loadAbout', () => {
    it('should load about data and update the about property', async () => {
      const mockAbout: About = {
        appVersion: '1.0.0',
      };

      service.loadAbout().then((data) => {
        expect(data).toEqual(mockAbout);
        expect(service.get()).toEqual(mockAbout);
      });

      const req = httpTestingController.expectOne(
        '/shared/assets/config/dynamic/about.json',
      );
      expect(req.request.method).toEqual('GET');
      req.flush(mockAbout);
    });
  });

  describe('get', () => {
    it('should return the about data', () => {
      const mockAbout: About = {
        appVersion: '1.0.0',
      };
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-expect-error
      service.about = mockAbout;
      expect(service.get()).toEqual(mockAbout);
    });
  });
});
