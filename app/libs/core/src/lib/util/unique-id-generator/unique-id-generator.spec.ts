import { generateUniqueId } from './unique-id-generator';

describe('Unique Id generator', () => {
  describe('generateTimestampId', () => {
    it('should generate a valid timestamp ID', () => {
      const id = generateUniqueId();
      expect(typeof id).toBe('number');
      expect(id).toBeGreaterThan(0);
    });

    it('should generate unique IDs for consecutive calls', async () => {
      const id1 = generateUniqueId();
      const id2 = generateUniqueId();
      expect(id1).not.toBe(id2);
    });

    it('should generate IDs less than or equal to 2_147_483_647', () => {
      for (let i = 0; i < 1000; i++) {
        const id = generateUniqueId();
        expect(id).toBeLessThanOrEqual(2_147_483_647);
      }
    });
  });
});
