export const generateUniqueId = (): number => {
  const timestamp = Date.now();

  const randomComponent = Math.floor(Math.random() * 1000);

  const uniqueId = timestamp + randomComponent;

  return uniqueId <= 2_147_483_647 ? uniqueId : uniqueId % 2_147_483_647;
};
