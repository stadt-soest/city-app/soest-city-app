import { Localization } from '../../models/localization.model';

export enum DayOfWeek {
  MONDAY = 'MONDAY',
  TUESDAY = 'TUESDAY',
  WEDNESDAY = 'WEDNESDAY',
  THURSDAY = 'THURSDAY',
  FRIDAY = 'FRIDAY',
  SATURDAY = 'SATURDAY',
  SUNDAY = 'SUNDAY',
}

const DAYS_MAP = [
  DayOfWeek.SUNDAY,
  DayOfWeek.MONDAY,
  DayOfWeek.TUESDAY,
  DayOfWeek.WEDNESDAY,
  DayOfWeek.THURSDAY,
  DayOfWeek.FRIDAY,
  DayOfWeek.SATURDAY,
];

export const getCurrentDayOfWeek = (date = new Date()): DayOfWeek => {
  const dayIndex = date.getDay();
  if (dayIndex < 0 || dayIndex >= DAYS_MAP.length) {
    throw new Error(`Invalid day index: ${dayIndex}`);
  }
  return DAYS_MAP[dayIndex];
};

export type OpeningHoursMap = { [day in DayOfWeek]: DateRange };

export type DateRange = { from: string; to: string };

export interface SpecialOpeningHours {
  from: string;
  to: string;
  descriptions: Localization[];
}

export type SpecialOpeningHoursMap = {
  [date: string]: SpecialOpeningHours;
};
