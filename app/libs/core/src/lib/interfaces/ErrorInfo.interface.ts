import { ErrorType } from '../enums/error-type';

export interface ErrorInfo {
  hasError: boolean;
  errorType: ErrorType;
  id?: string;
}
