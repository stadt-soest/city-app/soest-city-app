import { Component, inject, Input } from '@angular/core';
import { FeedItem } from '../models/feed-item.model';
import { ImageService } from '../services/image/image.service';
import { NavigationService } from '../services/navigation/navigation.service';
import { TranslocoService } from '@jsverse/transloco';

@Component({
  template: '',
  standalone: false,
})
export abstract class AbstractFeedCardComponent<T extends FeedItem> {
  @Input() feedItem!: T;
  @Input() routerLink: string | null | any[] = null;
  @Input() state: any;
  protected translateService = inject(TranslocoService);
  protected navigationService = inject(NavigationService);
  private readonly imageService = inject(ImageService);

  get imageUrl(): string | null {
    return this.imageService.getImageUrl(this.feedItem?.image);
  }

  async navigateToDetails(event?: any) {
    if (event) {
      event.preventDefault();
    }
    await this.navigationService.navigateToFeedItemDetails(this.feedItem);
  }
}
