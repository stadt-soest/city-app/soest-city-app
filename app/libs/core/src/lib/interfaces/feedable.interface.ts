import { Observable } from 'rxjs';
import { FeedItem } from '../models/feed-item.model';

export interface Feedable {
  getFeed(): Observable<FeedItem[]>;

  getById(id: string): Observable<any>;
}
