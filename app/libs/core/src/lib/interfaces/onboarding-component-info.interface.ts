import { Type } from '@angular/core';
import { ModuleInfo } from '../models/menu.model';

export interface OnboardingComponentInfo {
  component?: Type<any>;
  moduleInfo: ModuleInfo;
  isLaterSelected: boolean;
  isActivateSelected: boolean;
}
