export interface NotificationDetails {
  id: number;
  title: string;
  body: string;
  scheduleTime: Date;
  drawableResourceId: string;
  iconColor: string;
  attachments: Array<{ id: string; url: string; type: string }>;
}
