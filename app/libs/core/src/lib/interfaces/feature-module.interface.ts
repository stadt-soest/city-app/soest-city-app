import { ModuleInfo } from '../models/menu.model';
import { Routes } from '@angular/router';
import { Observable } from 'rxjs';
import { FeedItem } from '../models/feed-item.model';
import { Type } from '@angular/core';
import { Category } from '../models/category.model';
import { Poi } from '../models/poi.model';

export interface FeatureModule<T extends FeedItem | null> {
  info: ModuleInfo;
  routes: Routes;
  onboardingComponent?: Type<any>;
  registerMap?: () => Observable<MapIntegration>;
  feedables: (page: number) => Observable<FeedResult<any>>;
  feedablesUpcoming?: (page: number) => Observable<FeedResult<any>>;
  feedablesUnfiltered?: (page: number) => Observable<FeedResult<any>>;
  feedablesActivity?: (page: number) => Observable<FeedResult<any>>;
  feedablesHighlighted?: (page: number) => Observable<FeedResult<any>>;
  getUserSettings?: () => Promise<FeedUserSettings>;
  getPriorityFeedables?: () => Observable<FeedResult<any>>;
  initializeDefaultSettings: () => void;
  feedItemType?: string;
  getCustomForYouSection?: () => Observable<FeatureSection>;
  getCustomMapButton?: () => Observable<{ component: Type<any> }>;

  search?(
    searchTerm: string,
    page?: number,
    size?: number,
  ): Observable<{ items: T[]; total: number }>;
}

export interface FeedResult<T> {
  items: T[];
  totalRecords: number;
}

export interface MapIntegration {
  category: Category;
  poi: Poi[];
}

export interface ModuleFullSettings {
  moduleName: string;
  settings: FeedUserSettings;
  isVisible: boolean;
  feedItemType: string;
}

export interface FeedUserSettings {
  sources: string[];
  categoryIds: string[];
}

export interface FeatureSection {
  component: Type<any>;
}
