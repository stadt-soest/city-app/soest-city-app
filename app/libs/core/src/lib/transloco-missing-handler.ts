import { Injectable } from '@angular/core';
import { TranslocoMissingHandler } from '@jsverse/transloco';

@Injectable({ providedIn: 'root' })
export class TranslocoMissingTranslationHandler
  implements TranslocoMissingHandler
{
  handle(key: string) {
    console.error('translation missing:', key);
  }
}
