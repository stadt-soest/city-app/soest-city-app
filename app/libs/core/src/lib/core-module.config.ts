import { InjectionToken, Type } from '@angular/core';
import { PoiFeedItem } from './models/poi.model';
import { AbstractFeedCardComponent } from './interfaces/feed-card.abstract';

export interface CoreModuleConfig {
  apiBaseUrl: string;
  production: boolean;
  map: MapConfig;
  translocoLanguages?: string[];
  translocoDefaultLanguage?: string;
  poiCardComponent: Type<AbstractFeedCardComponent<PoiFeedItem>>;
  poiForYouComponent: Type<AbstractFeedCardComponent<PoiFeedItem>>;
}

export interface MapConfig {
  marker: {
    color: string;
    scale: number;
  };
  antialias: boolean;
  pixelRatio: number;
  styleUrlDark: string;
  styleUrlLight: string;
  center: [number, number];
  zoom: number;
}

export const CORE_MODULE_CONFIG = new InjectionToken<CoreModuleConfig>(
  'CoreModuleConfig',
);
