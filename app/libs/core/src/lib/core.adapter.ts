import { Observable } from 'rxjs';
import { Poi } from './models/poi.model';
import { Category } from './models/category.model';
import { FeedItem } from './models/feed-item.model';
import { FeedFilterCriteria } from './models/feed-filter-criteria.model';

export abstract class CoreAdapter {
  abstract getPoiById(id: string): Observable<Poi>;

  abstract searchPois(
    searchTerm: string,
    page?: number,
    size?: number,
  ): Observable<FeedItem[]>;

  abstract getPois(categoryIds: string[]): Observable<Poi[]>;

  abstract getPoiCategories(): Observable<Category[]>;

  abstract uploadSingleImage(file: File): Observable<string | null>;

  abstract getFeed(
    page: number,
    filterCriteria?: FeedFilterCriteria,
  ): Observable<FeedItem[]>;
}
