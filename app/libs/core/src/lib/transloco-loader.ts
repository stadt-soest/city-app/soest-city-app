import { inject, Injectable } from '@angular/core';
import { Translation, TranslocoLoader } from '@jsverse/transloco';
import { HttpClient } from '@angular/common/http';
import { catchError, of } from 'rxjs';

@Injectable()
export class TranslocoHttpLoader implements TranslocoLoader {
  private readonly http = inject(HttpClient);

  getTranslation(langPath: string) {
    const sanitizedLangPath = langPath
      ? langPath.replace(/_/g, '-').replace(/^\/+/, '')
      : '';
    const url = `/assets/i18n/${sanitizedLangPath}.json`;

    return this.http.get<Translation>(url).pipe(
      catchError((error) => {
        console.error(
          `Error loading translation for ${sanitizedLangPath}:`,
          error,
        );
        return of({});
      }),
    );
  }
}
