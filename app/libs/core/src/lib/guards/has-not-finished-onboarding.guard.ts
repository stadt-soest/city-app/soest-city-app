import { inject } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivateFn,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { OnboardingService } from '../services/onboarding/onboarding.service';
import { ROUTES } from '../constants/routes.constant';

export const hasNotFinishedOnboardingGuard: CanActivateFn = async (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
): Promise<boolean | UrlTree> => {
  const router: Router = inject(Router);
  const onboardingService: OnboardingService = inject(OnboardingService);
  const finishedOnboarding = await onboardingService.hasFinishedOnboarding();

  if (state.url === ROUTES.onboarding || finishedOnboarding) {
    return true;
  }

  return router.parseUrl(ROUTES.onboarding);
};
