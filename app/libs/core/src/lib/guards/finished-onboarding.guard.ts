import { inject } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivateFn,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { OnboardingService } from '../services/onboarding/onboarding.service';
import { ROUTES } from '../constants/routes.constant';

export const finishedOnboardingGuard: CanActivateFn = async (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
): Promise<boolean | UrlTree> => {
  const router: Router = inject(Router);
  const onboardingService: OnboardingService = inject(OnboardingService);
  const finishedOnboarding = await onboardingService.hasFinishedOnboarding();

  if (finishedOnboarding && state.url.includes(ROUTES.onboarding)) {
    return router.parseUrl(ROUTES.forYou);
  }

  return true;
};
