import { createAnimation, Animation } from '@ionic/angular';

/* eslint-disable @typescript-eslint/no-non-null-assertion */
export const customMdEnterAnimation = (baseEl: HTMLElement): Animation => {
  const backdrop = baseEl.querySelector('ion-backdrop');
  const modalWrapper = baseEl.shadowRoot?.querySelector('.modal-wrapper');

  const backdropAnimation = createAnimation()
    .addElement(backdrop!)
    .fromTo('opacity', 0, 0.4);

  const wrapperAnimation = createAnimation()
    .addElement(modalWrapper!)
    .keyframes([
      { offset: 0, opacity: 0, transform: 'translateY(100%)' },
      { offset: 1, opacity: 1, transform: 'translateY(0%)' },
    ]);

  return createAnimation()
    .addElement(baseEl)
    .easing('cubic-bezier(0.36, 0.66, 0.04, 1)')
    .duration(400)
    .addAnimation([backdropAnimation, wrapperAnimation]);
};

export const customMdLeaveAnimation = (baseEl: HTMLElement): Animation => {
  const backdrop = baseEl.querySelector('ion-backdrop');
  const modalWrapper = baseEl.shadowRoot?.querySelector('.modal-wrapper');

  const backdropAnimation = createAnimation()
    .addElement(backdrop!)
    .fromTo('opacity', 0.4, 0);

  const wrapperAnimation = createAnimation()
    .addElement(modalWrapper!)
    .keyframes([
      { offset: 0, transform: 'translateY(0px)' },
      { offset: 1, transform: 'translateY(40px)' },
    ]);

  return createAnimation()
    .addElement(baseEl)
    .easing('cubic-bezier(0.47, 0, 0.745, 0.715)')
    .duration(200)
    .addAnimation([backdropAnimation, wrapperAnimation]);
};
