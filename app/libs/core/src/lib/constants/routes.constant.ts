export const ROUTES = {
  onboarding: '/onboarding',
  forYou: '/for-you',
  search: '/search',
  settingsMenu: '/for-you/settings',
  notifications: '/for-you/settings/notifications',
  permissions: '/for-you/settings/permissions',
  forYouSettings: '/for-you/settings/module-preferences',
  settings: '/settings',
  details: '/details',
};
