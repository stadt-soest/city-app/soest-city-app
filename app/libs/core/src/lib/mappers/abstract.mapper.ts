export abstract class AbstractMapper<TDto, TModel> {
  mapToModelArray(dtos: TDto[]): TModel[] {
    return dtos.reduce((acc: TModel[], dto: TDto) => {
      try {
        const feedItem = this.mapToModel(dto);
        acc.push(feedItem);
      } catch (error) {
        console.error(error);
      }
      return acc;
    }, []);
  }

  abstract mapToModel(dto: TDto): TModel;
}
