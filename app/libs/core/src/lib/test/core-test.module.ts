import { Injectable, ModuleWithProviders, NgModule } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from '../core.module';
import { CoreModuleConfig } from '../core-module.config';
import { CoreAdapter } from '../core.adapter';
import { of } from 'rxjs';
import { AboutService } from '../services/about/about.service';

const MODULES = [CoreModule, RouterTestingModule, HttpClientTestingModule];

@Injectable()
export class MockAboutService {
  loadAbout() {
    return of({ appVersion: '1.0.0' });
  }
}

const mockCoreConfig: CoreModuleConfig = {
  translocoLanguages: ['de', 'en'],
  translocoDefaultLanguage: 'de',
  apiBaseUrl: 'http://localhost:8080',
  production: false,
  map: {
    marker: {
      color: '#A50000',
      scale: 0.6,
    },
    antialias: true,
    pixelRatio: 1,
    styleUrlDark: `https://api.maptiler.com/maps/basic-v2-dark/style.json?key=dblMN05NITZj5G23jfcd`,
    styleUrlLight: `https://api.maptiler.com/maps/basic-v2/style.json?key=dblMN05NITZj5G23jfcd`,
    center: [8.106023420256752, 51.5723841235784] as [number, number],
    zoom: 14,
  },
  poiCardComponent: undefined as any,
  poiForYouComponent: undefined as any,
};

@NgModule({
  imports: [...MODULES],
  exports: [...MODULES],
})
export class CoreTestModule {
  static forRoot(): ModuleWithProviders<CoreTestModule> {
    const filterValidProviders = (providers: any[] = []): any[] =>
      providers.filter((provider) => provider !== undefined);

    const coreModuleProviders =
      CoreModule.forRoot(mockCoreConfig).providers || [];

    const validProviders = filterValidProviders(coreModuleProviders);

    return {
      ngModule: CoreTestModule,
      providers: [
        ...validProviders,
        CoreAdapter,
        { provide: AboutService, useClass: MockAboutService },
      ],
    };
  }
}
