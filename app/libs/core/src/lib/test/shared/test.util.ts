import { ModuleCategory, ModuleInfo } from '../../models/menu.model';

export const createModuleInfo = (overrides?: Partial<ModuleInfo>): ModuleInfo =>
  ({
    id: 'id',
    name: 'name',
    category: ModuleCategory.information,
    icon: 'icon',
    baseRoutingPath: '/baseRoutingPath',
    defaultVisibility: true,
    settingsPageAvailable: true,
    ...overrides,
  }) as ModuleInfo;
