import { TranslocoService } from '@jsverse/transloco';
import { CategoryLocalization } from './poi.model';

export abstract class BaseCategory {
  id: string;
  localizedNames: Array<CategoryLocalization>;

  protected constructor(
    id: string,
    localizedNames: Array<CategoryLocalization> = [],
  ) {
    this.id = id;
    this.localizedNames = localizedNames;
  }

  private static getLocalizedNameFromList(
    localizedNames: Array<CategoryLocalization>,
    fallback: string,
    currentLocale: string,
  ): string {
    const localization = localizedNames.find(
      (localizedName) =>
        localizedName.language.toLowerCase() === currentLocale.toLowerCase(),
    );
    return localization?.value ?? fallback;
  }

  getLocalizedName(currentLocale: string): string {
    return BaseCategory.getLocalizedNameFromList(
      this.localizedNames,
      this.id,
      currentLocale,
    );
  }
}
