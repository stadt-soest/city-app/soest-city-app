export class DeviceEnvironment {
  deviceModelName = '';
  operatingSystemName = '';
  operatingSystemVersion = '';
  appVersion = '';
  browserName = '';
  usedAs = '';
  appSettings = '';
}
