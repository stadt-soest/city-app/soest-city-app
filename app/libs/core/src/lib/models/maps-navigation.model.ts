export enum MapsNavigationApp {
  /*
   eslint @typescript-eslint/naming-convention : 0
  */
  APPLE_MAPS = 'apple_maps',
  GOOGLE_MAPS = 'google_maps',
  WAZE = 'waze',
}
