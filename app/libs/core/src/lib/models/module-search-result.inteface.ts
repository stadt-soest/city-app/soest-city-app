export interface ModuleSearchResult<T> {
  moduleId: string;
  items: T[];
  total: number;
}
