import { TranslocoService } from '@jsverse/transloco';
import { Type } from '@angular/core';
import { Category, Language } from './category.model';
import {
  FeedItemLayoutType,
  IFeedCardComponent,
  FeedItem,
} from './feed-item.model';
import { Feature } from 'geojson';
import { TranslatableString } from './translatable-string.model';

export class Poi {
  id: string;
  name: string;
  translatableName?: TranslatableString;
  url?: string;
  location: PoiLocation;
  contactPoint: PoiContactPoint;
  dateModified: string;
  categories: PoiCategory[];
  icon?: Icon;
  modulePath?: string;

  constructor(options: PoiOptions) {
    this.id = options.id;
    this.name = options.name;
    this.translatableName = options.translatableName;
    this.url = options.url;
    this.location = options.location;
    this.contactPoint = options.contactPoint;
    this.dateModified = options.dateModified;
    this.categories = options.categories;
    this.icon = options.icon;
    this.modulePath = options.modulePath;
  }

  static toGeoJSONFeatures(
    pois: Poi[],
    selectedCategory: Category | null,
    currentLocale: string,
    selectedPoiId?: string,
  ): Feature[] {
    return pois.map((poi) => {
      const isSelected = poi.id === selectedPoiId;
      return {
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: [
            poi.location.coordinates.longitude,
            poi.location.coordinates.latitude,
          ],
        },
        properties: {
          title: selectedCategory?.getLocalizedName(currentLocale)
            ? poi.name
            : poi.categories[0]?.getLocalizedName(currentLocale),
          poiId: poi.id,
          iconKey: `${poi.icon?.key}-${
            poi.categories.find((cat) => cat.id === selectedCategory?.id)
              ?.selectedIconBackground ?? 'default'
          }`,
          backgroundColor:
            poi.categories.find((cat) => cat.id === selectedCategory?.id)
              ?.selectedIconBackground ?? '#A50000',
          modulePath: poi.modulePath,
          isSelected,
        },
      };
    });
  }

  static createFeedItem(
    poiWithDistance: PoiWithDistance,
    components: {
      cardComponent: Type<IFeedCardComponent<any>>;
      forYouComponent: Type<IFeedCardComponent<any>>;
    },
  ): PoiFeedItem {
    return {
      id: poiWithDistance.poi.id,
      icon: 'map',
      moduleId: 'map',
      baseRoutePath: 'map',
      moduleName: 'tabs.map.title',
      pluralModuleName: 'tabs.map.title',
      relevancy: 0,
      title: poiWithDistance.poi.name,
      creationDate: new Date(poiWithDistance.poi.dateModified),
      cardComponent: components.cardComponent,
      forYouComponent: components.forYouComponent,
      categories: poiWithDistance.poi.categories,
      distance: poiWithDistance.distance,
      layoutType: FeedItemLayoutType.card,
    };
  }
}

export interface Icon {
  key: string;
}

export class PoiLocation {
  street?: string;
  postalCode?: string;
  city?: string;
  coordinates: GeoCoordinates;

  constructor(
    coordinates: GeoCoordinates,
    street?: string,
    postalCode?: string,
    city?: string,
  ) {
    this.street = street;
    this.postalCode = postalCode;
    this.city = city;
    this.coordinates = coordinates;
  }
}

export class PoiCategory {
  id?: string | null;
  sourceCategory?: string;
  localizedNames: Array<CategoryLocalization>;
  selectedIconBackground?: string;

  constructor(
    id: string,
    localizedNames: Array<CategoryLocalization>,
    sourceCategory?: string,
    iconBackground?: string,
  ) {
    this.id = id;
    this.sourceCategory = sourceCategory;
    this.localizedNames = localizedNames;
    this.selectedIconBackground = iconBackground;
  }

  static of(
    id: string | null,
    sourceCategory: string,
    localizedNames: Array<CategoryLocalization>,
    iconBackground?: string,
  ): PoiCategory {
    return new PoiCategory(
      id ?? '',
      localizedNames,
      sourceCategory,
      iconBackground,
    );
  }

  getLocalizedName(currentLocale: string): string | null | undefined {
    const localization = this.localizedNames.find(
      (localizedName) => localizedName.language === currentLocale.toUpperCase(),
    );
    return localization ? localization.value : this.id;
  }
}

export interface CategoryLocalization {
  value: string;
  language: Language;
}

export class PoiContactPoint {
  email: string;
  phone: string;
  contactPerson: string;

  constructor(email: string, phone: string, contactPerson: string) {
    this.email = email;
    this.phone = phone;
    this.contactPerson = contactPerson;
  }
}

export class GeoCoordinates {
  latitude: number;
  longitude: number;

  constructor(latitude: number, longitude: number) {
    this.latitude = latitude;
    this.longitude = longitude;
  }
}

export class PoiWithDistance {
  poi: Poi;
  distance?: number;

  constructor(poi: Poi, distance?: number) {
    this.poi = poi;
    this.distance = distance;
  }
}

export interface PoiFeedItem extends FeedItem {
  distance?: number;
  categories: PoiCategory[];
}

interface PoiOptions {
  id: string;
  name: string;
  translatableName?: TranslatableString;
  url: string;
  location: PoiLocation;
  contactPoint: PoiContactPoint;
  dateModified: string;
  categories: PoiCategory[];
  icon?: Icon;
  modulePath?: string;
}
