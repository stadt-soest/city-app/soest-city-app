import { Type } from '@angular/core';

export interface IFeedCardComponent<T extends FeedItem> {
  feedItem: T;
}

export class FeedItem {
  id = '';
  moduleId = '';
  moduleName = '';
  pluralModuleName = '';
  baseRoutePath = '';
  icon = '';
  title = '';
  image?: Image | null = null;
  cardComponent!: Type<IFeedCardComponent<any>>;
  forYouComponent!: Type<IFeedCardComponent<any>>;
  creationDate: Date = new Date();
  relevancy = 0;
  layoutType!: FeedItemLayoutType;

  constructor(data: Partial<FeedItem>) {
    Object.assign(this, data);
  }
}

export interface Image {
  imageSource?: string;
  description?: string | null;
  copyrightHolder?: string | null;
  thumbnail?: ImageFile | null;
  highRes?: ImageFile | null;
}

export interface ImageFile {
  filename?: string;
  width?: number | null;
  height?: number | null;
}

export enum FeedItemLayoutType {
  card = 'card',
  item = 'item',
}
