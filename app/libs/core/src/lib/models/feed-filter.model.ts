import { FeedFilterType } from '../enums/feed-filter.enum';

export interface FeedFilter {
  filter: FeedFilterType;
  period: FilterPeriod;
}

export interface FilterPeriod {
  startDate?: Date;
  endDate?: Date;
}
