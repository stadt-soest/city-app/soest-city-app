export interface TranslatableString {
  key: string;
  params?: Record<string, any>;
}
