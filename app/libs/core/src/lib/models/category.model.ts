import { Type } from '@angular/core';
import { ModuleInfo } from './menu.model';
import { BaseCategory } from './base-category.model';

export class Category extends BaseCategory {
  sourceCategories?: Array<string>;
  icon?: string | null;
  source: 'API' | 'MODULE';
  moduleInfo?: ModuleInfo;
  groupCategory?: GroupCategory;
  dashboardComponent?: Type<any>;
  detailsComponent?: Type<any>;

  constructor({
    id,
    sourceCategories,
    icon,
    localizedNames = [],
    moduleInfo,
    source = 'MODULE',
    groupCategory,
    dashboardComponent,
    detailsComponent,
  }: {
    id: string;
    sourceCategories?: Array<string>;
    icon?: string | null;
    localizedNames?: Array<CategoryLocalization>;
    moduleInfo?: ModuleInfo;
    source?: 'API' | 'MODULE';
    groupCategory?: GroupCategory;
    dashboardComponent?: Type<any>;
    detailsComponent?: Type<any>;
  }) {
    super(id, localizedNames);
    this.sourceCategories = sourceCategories;
    this.icon = icon;
    this.moduleInfo = moduleInfo;
    this.source = source;
    this.groupCategory = groupCategory;
    this.dashboardComponent = dashboardComponent;
    this.detailsComponent = detailsComponent;
  }

  static of({
    id,
    moduleInfo,
    sourceCategories,
    icon,
    localizedNames,
    groupCategory,
    dashboardComponent,
    detailsComponent,
  }: {
    id: string;
    moduleInfo: ModuleInfo;
    sourceCategories?: Array<string>;
    icon?: string | null;
    localizedNames?: Array<CategoryLocalization>;
    groupCategory?: GroupCategory;
    dashboardComponent?: Type<any>;
    detailsComponent?: Type<any>;
  }): Category {
    return new Category({
      id,
      sourceCategories,
      icon,
      localizedNames,
      moduleInfo,
      source: 'MODULE',
      groupCategory,
      dashboardComponent,
      detailsComponent,
    });
  }
}

export class GroupCategory extends BaseCategory {
  constructor(id: string, localizedNames: Array<CategoryLocalization>) {
    super(id, localizedNames);
  }
}

interface CategoryLocalization {
  value: string;
  language: Language;
}

export type Language = 'DE' | 'EN';
