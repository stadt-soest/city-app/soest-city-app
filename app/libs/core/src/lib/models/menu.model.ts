export type ModuleInfo = {
  id: string;
  name: string;
  category: ModuleCategory;
  icon: string;
  baseRoutingPath: string;
  defaultVisibility: boolean;
  settingsPageAvailable: boolean;
  isAboutAppItself: boolean;
  relevancy: number;
  forYouRelevant: boolean;
  showInCategoriesPage: boolean;
  orderInCategory: number;
};

export enum ModuleCategory {
  information = 'categories.information',
  onTheWay = 'categories.on_the_way',
  yourCity = 'categories.your_city',
  help = 'categories.help',
}

export class MenuItemGroup {
  category: ModuleCategory;
  menuItems: MenuItem[];

  constructor(category: ModuleCategory, menuItems: MenuItem[]) {
    this.category = category;
    this.menuItems = menuItems;
  }
}

export class MenuItem {
  title: string;
  icon: string;
  baseRoutingPath: string;

  constructor(title: string, baseRoutingPath: string, icon: string) {
    this.icon = icon;
    this.title = title;
    this.baseRoutingPath = baseRoutingPath;
  }
}
