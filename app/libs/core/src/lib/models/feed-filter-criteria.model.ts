export interface FeedFilterCriteria {
  type?: string;
  sources?: string[];
  categoryIds?: string[];
}
