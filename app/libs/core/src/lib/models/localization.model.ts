export type Language = 'DE' | 'EN';

export interface Localization {
  value: string;
  language: Language;
}
