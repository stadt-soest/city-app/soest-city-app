export interface OnboardingModuleInfo {
  title: string;
  description: string;
  image: string;
}
