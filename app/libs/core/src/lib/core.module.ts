import {
  provideHttpClient,
  withInterceptorsFromDi,
} from '@angular/common/http';
import {
  InjectionToken,
  ModuleWithProviders,
  NgModule,
  Optional,
  SkipSelf,
  inject,
  provideAppInitializer,
} from '@angular/core';
import { About } from './models/about.model';
import { CommonModule } from '@angular/common';
import { AboutService } from './services/about/about.service';
import { NavService } from './services/nav/nav.service';
import { UpdateService } from './services/update/update.service';
import { ForYouService } from './services/for-you/for-you.service';
import { MenuService } from './services/menu/menu.service';
import { OnboardingService } from './services/onboarding/onboarding.service';
import { StorageService } from './services/storage/storage.service';
import { ModuleRegistryService } from './services/module-registry/module-registry.service';
import { NavigationService } from './services/navigation/navigation.service';
import { ImageService } from './services/image/image.service';
import { SettingsPersistenceService } from './services/settings-persistence/settings-persistence-service';
import { FeedUpdateService } from './services/feed-update/feed-update.service';
import { JsonParsingService } from './services/json/json-parsing.service';
import { ErrorHandlingService } from './services/error-handling/error-handling.service';
import { BaseShareService } from './services/share/base-share.service';
import { BrowserService } from './services/browser/browser.service';
import { TranslationService } from './services/translation/translation.service';
import { DeviceService } from './services/device/device.service';
import { MapsNavigationService } from './services/maps-navigation/maps-navigation.service';
import { SearchService } from './services/search/search.service';
import { CalendarService } from './services/calendar/calendar.service';
import { KeyboardService } from './services/keyboard/keyboard.service';
import { StorageMigrationService } from './services/storage-migration/storage-migration.service';
import { NotificationService } from './services/notification/notification.service';
import { PoiService } from './services/poi/poi.service';
import { ToastService } from './services/toast/toast.service';
import { ConnectivityService } from './services/connectivty/connectivity.service';
import { PrivacyService } from './services/privacy/privacy.service';
import { DomPurifyConfigService } from './services/dom-purify-config/dom-purify-config.service';
import { TabRefreshService } from './services/tab-refresh/tab-refresh.service';
import { PdfService } from './services/pdf/pdf.service';
import { TitleStrategy } from '@angular/router';
import { TitleStrategyService } from './services/title-strategy.service';
import { TitleService } from './services/title.service';
import { DocumentLanguageService } from './services/app-init/document-language.service';
import { AppUrlOpeningService } from './services/app-init/app-url-opening.service';
import { ScreenOrientationService } from './services/app-init/screen-orientation.service';
import { AppInitializationService } from './services/app-init/app-initialization.service';
import {
  provideTransloco,
  TRANSLOCO_MISSING_HANDLER,
  TranslocoService,
} from '@jsverse/transloco';
import { TranslocoMissingTranslationHandler } from './transloco-missing-handler';
import { CORE_MODULE_CONFIG, CoreModuleConfig } from './core-module.config';
import { TranslocoHttpLoader } from './transloco-loader';
import { provideTranslocoMessageformat } from '@jsverse/transloco-messageformat';
import { Calendar } from '@awesome-cordova-plugins/calendar/ngx';
import { AlertService } from './services/alert.service';

const MODULES = [CommonModule];

const SERVICES = [
  provideHttpClient(withInterceptorsFromDi()),
  Calendar,
  AboutService,
  AlertService,
  NavService,
  provideAppInitializer(() => {
    const initializerFn = (
      (aboutService: AboutService) => (): Promise<About> =>
        aboutService.loadAbout()
    )(inject(AboutService));
    return initializerFn();
  }),
  UpdateService,
  ForYouService,
  MenuService,
  OnboardingService,
  StorageService,
  ModuleRegistryService,
  NavigationService,
  ImageService,
  SettingsPersistenceService,
  FeedUpdateService,
  JsonParsingService,
  ErrorHandlingService,
  BaseShareService,
  BrowserService,
  TranslationService,
  DeviceService,
  MapsNavigationService,
  SearchService,
  CalendarService,
  KeyboardService,
  StorageMigrationService,
  NotificationService,
  PoiService,
  ToastService,
  ConnectivityService,
  PrivacyService,
  DomPurifyConfigService,
  TabRefreshService,
  PdfService,
  provideAppInitializer(() => {
    const initializerFn = (
      (storageMigrationService: StorageMigrationService) => (): Promise<any> =>
        storageMigrationService.migrateData()
    )(inject(StorageMigrationService));
    return initializerFn();
  }),
  {
    provide: TitleStrategy,
    useClass: TitleStrategyService,
  },
  TitleService,
  DocumentLanguageService,
  AppUrlOpeningService,
  ScreenOrientationService,
  AppInitializationService,
  {
    provide: TRANSLOCO_MISSING_HANDLER,
    useClass: TranslocoMissingTranslationHandler,
  },
];

const OTHER_PROVIDERS = [{ provide: 'Window', useValue: window }];
export const MAP_STYLE_URL = new InjectionToken<string>('MapStyleUrl');

@NgModule({
  imports: [...MODULES],
  exports: [...MODULES],
})
export class CoreModule {
  constructor(
    @Optional() @SkipSelf() parentModule: CoreModule,
    private readonly translocoService: TranslocoService,
  ) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import in your base AppModule only.',
      );
    }

    const browserLang = navigator.language.split('-')[0];
    const availableLangs =
      this.translocoService.getAvailableLangs() as string[];
    const isLangSupported = availableLangs.includes(browserLang);
    const lang = isLangSupported
      ? browserLang
      : this.translocoService.getDefaultLang();
    this.translocoService.setActiveLang(lang);
  }

  static forRoot(config: CoreModuleConfig): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule,
      providers: [
        { provide: CORE_MODULE_CONFIG, useValue: config },
        provideTransloco({
          config: {
            availableLangs: config.translocoLanguages,
            defaultLang: config.translocoDefaultLanguage,
            reRenderOnLangChange: true,
            prodMode: config.production,
          },
          loader: TranslocoHttpLoader,
        }),
        provideTranslocoMessageformat(),
        ...SERVICES,
        ...OTHER_PROVIDERS,
      ],
    };
  }
}
