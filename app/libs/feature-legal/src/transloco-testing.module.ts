import {
  TranslocoTestingModule,
  TranslocoTestingOptions,
} from '@jsverse/transloco';
import en from './lib/i18n/en.json';
import de from './lib/i18n/de.json';

export const getTranslocoModule = (options: TranslocoTestingOptions = {}) => {
  const testingOptions: TranslocoTestingOptions = {
    langs: {
      en,
      de,

      'feature_legal/de': de,

      'feature_legal/en': en,
    },
    translocoConfig: {
      availableLangs: ['en', 'de'],
      defaultLang: 'en',
    },
    preloadLangs: true,
    ...options,
  };

  (testingOptions.translocoConfig as any).scopeMapping = {
    feature_legal: 'feature_legal',
  };

  return TranslocoTestingModule.forRoot(testingOptions);
};
