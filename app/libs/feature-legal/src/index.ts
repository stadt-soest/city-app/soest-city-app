export { FeatureLegalModule } from './lib/feature-legal.module';
export {
  LEGAL_NOTICE_URL,
  PRIVACY_POLICY_URL,
} from './lib/feature-legal.module';
