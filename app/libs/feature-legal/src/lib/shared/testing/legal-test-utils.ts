import { ModuleCategory, ModuleInfo } from '@sw-code/urbo-core';

export const mockLegalModuleInfo: ModuleInfo = {
  id: 'legal',
  name: 'Datenschutz',
  category: ModuleCategory.information,
  icon: 'policy',
  baseRoutingPath: '/legal',
  defaultVisibility: true,
  settingsPageAvailable: false,
  isAboutAppItself: false,
  relevancy: 0.5,
  forYouRelevant: false,
  showInCategoriesPage: false,
  orderInCategory: 1,
};
