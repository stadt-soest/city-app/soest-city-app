import { Component, Input } from '@angular/core';
import { SafeHtml } from '@angular/platform-browser';
import { IonListHeader } from '@ionic/angular/standalone';

@Component({
  selector: 'lib-license-section',
  template: `
    <div>
      <ion-list-header class="settings-subheader" [attr.aria-label]="title">
        {{ title }}
      </ion-list-header>
      <p class="settings-text" [innerHTML]="text"></p>
    </div>
  `,
  styleUrls: ['./license-section.component.scss'],
  imports: [IonListHeader],
})
export class LicenseSectionComponent {
  @Input() title!: string;
  @Input() text!: SafeHtml;
}
