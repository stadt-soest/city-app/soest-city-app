import { Component, inject } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  CustomButtonColorScheme,
  CustomButtonType,
  CustomVerticalButtonComponent,
} from '@sw-code/urbo-ui';
import { PRIVACY_POLICY_URL } from '../../feature-legal.module';
import { BrowserService } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-privacy-policy-content',
  templateUrl: './privacy-policy-content.component.html',
  imports: [TranslocoDirective, CustomVerticalButtonComponent],
})
export class PrivacyPolicyContentComponent {
  privacyPolicyUrl = inject(PRIVACY_POLICY_URL);
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
  private readonly browserService = inject(BrowserService);

  async openPrivacyPolicy(event: Event) {
    event.preventDefault();
    await this.browserService.openInAppBrowser(this.privacyPolicyUrl);
  }
}
