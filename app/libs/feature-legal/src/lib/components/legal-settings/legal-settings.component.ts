import { Component, inject, OnInit } from '@angular/core';
import { IonList, IonListHeader } from '@ionic/angular/standalone';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import { CheckboxSettingComponent } from '@sw-code/urbo-ui';
import { LicenseSectionComponent } from '../license-section/license-section.component';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { PrivacyService } from '@sw-code/urbo-core';
import { take } from 'rxjs';

@Component({
  selector: 'lib-legal-settings',
  templateUrl: './legal-settings.component.html',
  styleUrls: ['./legal-settings.component.scss'],
  imports: [
    TranslocoDirective,
    IonList,
    IonListHeader,
    CheckboxSettingComponent,
    LicenseSectionComponent,
  ],
})
export class LegalSettingsComponent implements OnInit {
  appTrackingActive = false;
  countdownFontText!: SafeHtml;
  openSansFontText!: SafeHtml;
  vollkornFontText!: SafeHtml;
  materialIconsText!: SafeHtml;
  private readonly privacyService = inject(PrivacyService);
  private readonly translocoService = inject(TranslocoService);
  private readonly sanitizer = inject(DomSanitizer);

  constructor() {
    this.translocoService
      .selectTranslateObject('feature_legal.dashboard.licenses')
      .pipe(take(1))
      .subscribe((licenses) => {
        this.countdownFontText = this.sanitizer.bypassSecurityTrustHtml(
          licenses.countdown_font_text,
        );
        this.openSansFontText = this.sanitizer.bypassSecurityTrustHtml(
          licenses.opensans_font_text,
        );
        this.vollkornFontText = this.sanitizer.bypassSecurityTrustHtml(
          licenses.vollkorn_font_text,
        );
        this.materialIconsText = this.sanitizer.bypassSecurityTrustHtml(
          licenses.material_icons_text,
        );
      });
  }

  async ngOnInit(): Promise<void> {
    this.appTrackingActive = await this.privacyService.isTrackingActive();
  }

  activateAppTracking(): void {
    this.privacyService.activateTracking();
    this.appTrackingActive = true;
  }

  deactivateAppTracking(): void {
    this.privacyService.deactivateTracking();
    this.appTrackingActive = false;
  }
}
