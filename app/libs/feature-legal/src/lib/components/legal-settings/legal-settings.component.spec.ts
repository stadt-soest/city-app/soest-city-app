import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { LegalSettingsComponent } from './legal-settings.component';
import { LEGAL_MODULE_INFO } from '../../feature-legal.module';
import { mockLegalModuleInfo } from '../../shared/testing/legal-test-utils';
import { CoreTestModule, PrivacyService } from '@sw-code/urbo-core';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../transloco-testing.module';

describe('LegalSettingsComponent', () => {
  let component: LegalSettingsComponent;
  let fixture: ComponentFixture<LegalSettingsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: LEGAL_MODULE_INFO,
          useValue: mockLegalModuleInfo,
        },
        {
          provide: PrivacyService,
          useValue: {
            activateTracking: jest.fn(),
            deactivateTracking: jest.fn(),
            isTrackingActive: jest.fn(),
          },
        },
      ],
      imports: [
        LegalSettingsComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(LegalSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
