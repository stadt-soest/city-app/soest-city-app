import { Component, inject } from '@angular/core';
import {
  CustomButtonColorScheme,
  CustomButtonType,
  CustomVerticalButtonComponent,
} from '@sw-code/urbo-ui';
import { TranslocoPipe, TranslocoService } from '@jsverse/transloco';
import { AlertService, StorageService, ToastService } from '@sw-code/urbo-core';
import { firstValueFrom, take } from 'rxjs';

@Component({
  selector: 'lib-user-data-clear',
  template: `
    <lib-custom-vertical-button
      class="custom-vertical-button"
      [startIcon]="'delete_forever'"
      [buttonType]="customButtonType.button"
      [colorScheme]="customButtonColorScheme.hint"
      (click)="presentClearConfirmation()"
      [title]="'feature_legal.dashboard.clear_data.button_label' | transloco"
    ></lib-custom-vertical-button>
  `,
  imports: [CustomVerticalButtonComponent, TranslocoPipe],
  providers: [AlertService],
})
export class UserDataClearComponent {
  protected readonly customButtonType = CustomButtonType;
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  private readonly storageService = inject(StorageService);
  private readonly alertService = inject(AlertService);
  private readonly translocoService = inject(TranslocoService);
  private readonly toastService = inject(ToastService);

  async presentClearConfirmation(): Promise<void> {
    await this.alertService.presentAlert(
      'feature_legal.dashboard.clear_data.confirm_header',
      'feature_legal.dashboard.clear_data.confirm_message',
      'feature_legal.dashboard.clear_data.confirm_cancel',
      'feature_legal.dashboard.clear_data.confirm_clear',
      () => this.clearAllUserData(),
    );
  }

  async clearAllUserData(): Promise<void> {
    this.storageService
      .clear()
      .then(() => {
        this.presentSuccessToast();
      })
      .catch(async (error) => {
        console.error('Failed to clear user data:', error);
        await this.presentErrorToast();
      });
  }

  private async presentSuccessToast() {
    const message = await firstValueFrom(
      this.translocoService
        .selectTranslate('feature_legal.dashboard.clear_data.success')
        .pipe(take(1)),
    );

    await this.toastService.presentToast(message, 3000, 'top', 'success');
  }

  private async presentErrorToast() {
    const message = await firstValueFrom(
      this.translocoService
        .selectTranslate('feature_legal.dashboard.clear_data.error')
        .pipe(take(1)),
    );

    await this.toastService.presentToast(message, 3000, 'top', 'danger');
  }
}
