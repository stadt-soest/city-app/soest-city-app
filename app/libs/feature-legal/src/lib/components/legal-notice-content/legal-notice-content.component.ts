import { Component, inject } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  CustomButtonColorScheme,
  CustomButtonType,
  CustomVerticalButtonComponent,
} from '@sw-code/urbo-ui';
import { LEGAL_NOTICE_URL } from '../../feature-legal.module';
import { BrowserService } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-legal-notice-content',
  templateUrl: './legal-notice-content.component.html',
  imports: [TranslocoDirective, CustomVerticalButtonComponent],
})
export class LegalNoticeContentComponent {
  legalNoticeUrl = inject(LEGAL_NOTICE_URL);
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
  private readonly browserService = inject(BrowserService);

  async openLegalNotice(event: Event) {
    event.preventDefault();
    await this.browserService.openInAppBrowser(this.legalNoticeUrl);
  }
}
