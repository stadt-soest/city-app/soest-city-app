import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LEGAL_MODULE_INFO } from '../feature-legal.module';
import { LegalPageComponent } from './legal-page.component';
import { mockLegalModuleInfo } from '../shared/testing/legal-test-utils';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../transloco-testing.module';

describe('LegalPage', () => {
  let component: LegalPageComponent;
  let fixture: ComponentFixture<LegalPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: LEGAL_MODULE_INFO,
          useValue: mockLegalModuleInfo,
        },
      ],
      imports: [
        LegalPageComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
    });

    fixture = TestBed.createComponent(LegalPageComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
