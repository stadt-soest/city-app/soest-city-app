import { Component } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { IonGrid } from '@ionic/angular/standalone';
import { LegalSettingsComponent } from '../components/legal-settings/legal-settings.component';
import { PrivacyPolicyContentComponent } from '../components/privacy-policy-content/privacy-policy-content.component';
import { LegalNoticeContentComponent } from '../components/legal-notice-content/legal-notice-content.component';
import { UserDataClearComponent } from '../components/user-data-clear/user-data-clear.component';
import { CondensedHeaderLayoutComponent } from '@sw-code/urbo-ui';

@Component({
  selector: 'lib-legal',
  templateUrl: './legal-page.component.html',
  styleUrls: ['./legal-page.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    IonGrid,
    LegalSettingsComponent,
    PrivacyPolicyContentComponent,
    LegalNoticeContentComponent,
    UserDataClearComponent,
  ],
})
export class LegalPageComponent {}
