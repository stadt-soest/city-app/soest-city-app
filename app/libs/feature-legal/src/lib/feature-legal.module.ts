import {
  Inject,
  InjectionToken,
  ModuleWithProviders,
  NgModule,
  inject,
  provideAppInitializer,
} from '@angular/core';
import { from, of } from 'rxjs';
import { UIModule } from '@sw-code/urbo-ui';
import { provideTranslocoScope } from '@jsverse/transloco';
import {
  FeatureModule,
  ModuleCategory,
  ModuleInfo,
  ModuleRegistryService,
} from '@sw-code/urbo-core';

const MODULES = [UIModule];
const PROVIDERS = [
  provideTranslocoScope({
    scope: 'feature-legal',
    alias: 'feature_legal',
    loader: {
      en: () => import('./i18n/en.json'),
      de: () => import('./i18n/de.json'),
    },
  }),
];
export const LEGAL_MODULE_INFO = new InjectionToken<ModuleInfo>(
  'LegalModuleInfoHolderToken',
);
export const PRIVACY_POLICY_URL = new InjectionToken<string>(
  'PRIVACY_POLICY_URL',
);
export const LEGAL_NOTICE_URL = new InjectionToken<string>('LEGAL_NOTICE_URL');
const moduleId = 'legal';

@NgModule({
  imports: [...MODULES],
  exports: [...MODULES],
  providers: [
    ...PROVIDERS,
    {
      provide: LEGAL_MODULE_INFO,
      useValue: {
        id: moduleId,
        name: 'feature_legal.module_name',
        category: ModuleCategory.help,
        icon: 'assured_workload',
        baseRoutingPath: moduleId,
        defaultVisibility: true,
        settingsPageAvailable: false,
        isAboutAppItself: true,
        relevancy: 0,
        forYouRelevant: false,
        showInCategoriesPage: false,
      },
    },
  ],
})
export class FeatureLegalModule {
  readonly featureModule: FeatureModule<null>;

  constructor(
    @Inject(LEGAL_MODULE_INFO) private readonly legalModuleInfo: ModuleInfo,
  ) {
    this.featureModule = {
      initializeDefaultSettings: () => {
        //EMPTY
      },
      feedables: () => of({ items: [], totalRecords: 0 }),
      feedablesUnfiltered: () => from([]),
      info: this.legalModuleInfo,
      routes: [
        {
          path: `for-you/settings/${moduleId}`,
          loadComponent: () =>
            import('./pages/legal-page.component').then(
              (m) => m.LegalPageComponent,
            ),
          title: 'feature_legal.dashboard.main_headings',
        },
      ],
    };
  }

  static forRoot(): ModuleWithProviders<FeatureLegalModule> {
    return {
      ngModule: FeatureLegalModule,
      providers: [
        provideAppInitializer(() => {
          const initializerFn = (
            (registry: ModuleRegistryService, module: FeatureLegalModule) =>
            () =>
              registry.registerFeature(module.featureModule)
          )(inject(ModuleRegistryService), inject(FeatureLegalModule));
          return initializerFn();
        }),
      ],
    };
  }
}
