import { inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, firstValueFrom } from 'rxjs';
import { DataService } from './geojson-data.service';
import { FUNFAIR_CONFIG } from '../funfair-tokens';
import { Map as MapLibreMap } from 'maplibre-gl';

@Injectable({ providedIn: 'root' })
export class FunfairIconService {
  private readonly http = inject(HttpClient);
  private readonly dataService = inject(DataService);
  private readonly iconsLoadedSubject = new BehaviorSubject<boolean>(false);
  iconsLoaded$ = this.iconsLoadedSubject.asObservable();
  private readonly iconCache: Map<string, HTMLImageElement> = new Map();
  private readonly funfairConfig = inject(FUNFAIR_CONFIG);

  async loadIcons(mapInstance: MapLibreMap, isDarkMode = false): Promise<void> {
    try {
      await this.dataService.ensureGeoJSONDataLoaded();

      const centroidGeoJSON = await firstValueFrom(
        this.dataService.getFilteredCentroidGeoJSON(),
      );
      if (!centroidGeoJSON) {
        console.warn('No centroid GeoJSON data available for loading icons.');
        return;
      }

      const iconNames = new Set(
        centroidGeoJSON.features
          .map((feature) => feature.properties?.['icon'])
          .filter(Boolean) as string[],
      );

      await Promise.all(
        [...iconNames].map((iconName) =>
          this.loadIconSet(mapInstance, iconName, isDarkMode),
        ),
      );

      this.iconsLoadedSubject.next(true);
    } catch (error) {
      console.error('Error loading icons:', error);
    }
  }

  private async loadIconSet(
    mapInstance: MapLibreMap,
    iconName: string,
    isDarkMode: boolean,
  ): Promise<void> {
    await Promise.all([
      this.loadIcon(mapInstance, iconName, isDarkMode, false),
      this.loadIcon(mapInstance, iconName, isDarkMode, true),
    ]);
  }

  private async loadIcon(
    mapInstance: MapLibreMap,
    iconName: string,
    isDarkMode: boolean,
    isDisabled: boolean,
  ): Promise<void> {
    const suffix = isDisabled ? '-disabled' : '';
    const fullIconName = iconName + suffix;

    if (this.iconCache.has(fullIconName)) {
      const cachedImage = this.iconCache.get(fullIconName);
      if (cachedImage && !mapInstance.hasImage(fullIconName)) {
        mapInstance.addImage(fullIconName, cachedImage);
      }
      return;
    }

    const iconPath = `/assets/feature-funfair/icons${suffix}/${
      isDarkMode ? 'dark' : 'light'
    }/${iconName}.svg`;
    try {
      const svgText = await firstValueFrom(
        this.http.get(iconPath, { responseType: 'text' }),
      );
      const image = await this.svgToImage(svgText);

      this.iconCache.set(fullIconName, image);
      if (!mapInstance.hasImage(fullIconName)) {
        mapInstance.addImage(fullIconName, image);
      }
    } catch (error) {
      console.error(`Error loading icon ${iconName}:`, error);
    }
  }

  private async svgToImage(svgData: string): Promise<HTMLImageElement> {
    return new Promise((resolve, reject) => {
      const image = new Image();
      const parser = new DOMParser();
      const svgDoc = parser.parseFromString(svgData, 'image/svg+xml');
      const svgElement = svgDoc.documentElement;

      const scaledSize = 40 * this.funfairConfig.map.pixelRatio;
      svgElement.setAttribute('width', scaledSize.toString());
      svgElement.setAttribute('height', scaledSize.toString());

      if (!svgElement.hasAttribute('viewBox')) {
        svgElement.setAttribute('viewBox', '0 0 40 40');
      }

      const serializer = new XMLSerializer();
      const modifiedSvgData = serializer.serializeToString(svgElement);
      const blob = new Blob([modifiedSvgData], { type: 'image/svg+xml' });
      const url = URL.createObjectURL(blob);

      image.onload = () => {
        resolve(image);
        URL.revokeObjectURL(url);
      };
      image.onerror = () => {
        reject(new Error('Failed to load SVG image'));
        URL.revokeObjectURL(url);
      };

      image.src = url;
    });
  }
}
