import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

interface ModalDismissData {
  selectedCategory: string;
  categoryName: string;
  categoryNameKey: string;
}

@Injectable({
  providedIn: 'root',
})
export class ModalDismissalService {
  private readonly dismissEvent = new Subject<void>();
  dismissEvent$ = this.dismissEvent.asObservable();

  private readonly dismissDataEvent = new Subject<ModalDismissData>();
  dismissDataEvent$ = this.dismissDataEvent.asObservable();

  triggerDismissEvent() {
    this.dismissEvent.next();
  }

  triggerDismissWithData(data: ModalDismissData) {
    this.dismissDataEvent.next(data);
  }
}
