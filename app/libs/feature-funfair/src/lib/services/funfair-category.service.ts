import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {
  FunfairCategory,
  PredefinedCategory,
} from '../models/funfair-category.model';

@Injectable({ providedIn: 'root' })
export class FunfairCategoryService {
  private readonly currentCategorySubject =
    new BehaviorSubject<FunfairCategory>(PredefinedCategory.all);
  currentCategory$ = this.currentCategorySubject.asObservable();

  setCurrentCategory(value: FunfairCategory) {
    this.currentCategorySubject.next(value);
  }
}
