import { inject, Injectable } from '@angular/core';
import { Feature, Geometry } from 'geojson';
import { BehaviorSubject, Observable } from 'rxjs';
import { StorageService } from '@sw-code/urbo-core';
import { Properties } from '../models/feature.model';

const FUNFAIR_BOOKMARKED_KEY = 'funfair_bookmarked';

@Injectable({ providedIn: 'root' })
export class FunfairBookmarkService {
  private readonly storageService = inject(StorageService);
  private readonly bookmarkedPOIsSubject = new BehaviorSubject<
    Feature<Geometry, Properties>[]
  >([]);

  constructor() {
    this.initBookmarks();
  }

  getBookmarkedPOIs$(): Observable<Feature<Geometry, Properties>[]> {
    return this.bookmarkedPOIsSubject.asObservable();
  }

  getBookmarkedPOIs(): Feature<Geometry, Properties>[] {
    return this.bookmarkedPOIsSubject.getValue();
  }

  async isPOIBookmarked(id: string): Promise<boolean> {
    const bookmarks = this.getBookmarkedPOIs();
    return bookmarks.some(
      (bookmark) => bookmark.properties['id'].toString() === id,
    );
  }

  async addBookmark(feature: Feature<Geometry, Properties>): Promise<void> {
    const bookmarks = this.getBookmarkedPOIs();
    if (
      !bookmarks.some(
        (bookmark) => bookmark.properties['id'] === feature.properties['id'],
      )
    ) {
      const updatedBookmarks = [...bookmarks, feature];
      this.bookmarkedPOIsSubject.next(updatedBookmarks);
      await this.storageService.set(
        FUNFAIR_BOOKMARKED_KEY,
        JSON.stringify(updatedBookmarks),
      );
    }
  }

  async removeBookmark(id: string): Promise<void> {
    const bookmarks = this.getBookmarkedPOIs();
    const updatedBookmarks = bookmarks.filter(
      (bookmark) => bookmark.properties['id'].toString() !== id,
    );
    this.bookmarkedPOIsSubject.next(updatedBookmarks);
    await this.storageService.set(
      FUNFAIR_BOOKMARKED_KEY,
      JSON.stringify(updatedBookmarks),
    );
  }

  private async loadInitialBookmarks() {
    const bookmarksJson = await this.storageService.get(FUNFAIR_BOOKMARKED_KEY);
    const bookmarks = bookmarksJson ? JSON.parse(bookmarksJson) : [];
    this.bookmarkedPOIsSubject.next(bookmarks);
  }

  private initBookmarks() {
    this.loadInitialBookmarks().catch((error) => {
      console.error('Error loading Funfair bookmarks:', error);
    });
  }
}
