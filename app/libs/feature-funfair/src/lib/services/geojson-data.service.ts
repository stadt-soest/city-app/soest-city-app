import { inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, catchError, from, Observable, of } from 'rxjs';
import { Feature, FeatureCollection, Geometry } from 'geojson';
import { Category, CategoryGroup } from '../models/category-groups';
import { Properties } from '../models/feature.model';
import { FunfairBookmarkService } from './funfair-bookmark.service';
import { ThemeService } from '@sw-code/urbo-ui';
import { categoryGroups } from '../components/funfair-base-filter/subcomponents/funfair-filter/funfair-category-data';
import { PredefinedCategory } from '../models/funfair-category.model';
import { getMappedCategoryName } from '../shared/funfair-group-util';
import { categoryIconMap } from '../models/funfair-category-icons';
import { centerOfMass } from '@turf/turf';

@Injectable({ providedIn: 'root' })
export class DataService {
  categoryGroups: CategoryGroup[] = categoryGroups;
  private geojsonData!: FeatureCollection<Geometry, Properties>;
  private centroidGeoJSON!: FeatureCollection<Geometry, Properties>;

  private readonly favoritesGeoJSONSubject =
    new BehaviorSubject<FeatureCollection<Geometry, Properties> | null>(null);
  private readonly filteredGeoJSONSubject =
    new BehaviorSubject<FeatureCollection<Geometry, Properties> | null>(null);
  private readonly filteredCentroidGeoJSONSubject =
    new BehaviorSubject<FeatureCollection<Geometry, Properties> | null>(null);

  private selectedCategory = '';
  private readonly http = inject(HttpClient);
  private readonly funfairBookmarkService = inject(FunfairBookmarkService);
  private readonly isDarkMode = inject(ThemeService).isDarkMode();

  getFilteredGeoJSON(): Observable<FeatureCollection<
    Geometry,
    Properties
  > | null> {
    return this.filteredGeoJSONSubject.asObservable();
  }

  getFilteredCentroidGeoJSON(): Observable<FeatureCollection<
    Geometry,
    Properties
  > | null> {
    return this.filteredCentroidGeoJSONSubject.asObservable();
  }

  getFavoritesGeoJSON(): Observable<FeatureCollection<
    Geometry,
    Properties
  > | null> {
    return this.favoritesGeoJSONSubject.asObservable();
  }

  async getFeaturesByCategoryName(
    categoryName: string,
  ): Promise<Feature<Geometry, Properties>[]> {
    await this.ensureGeoJSONDataLoaded();

    if (categoryName === PredefinedCategory.favorites) {
      return this.funfairBookmarkService.getBookmarkedPOIs();
    }

    const matchedCategory = this.categoryGroups
      .flatMap((group) => group.categories)
      .find((cat) => cat.name === categoryName);

    if (!matchedCategory) {
      console.warn(`Category not found for name: ${categoryName}`);
      return [];
    }

    return this.getFeaturesByCategory(matchedCategory);
  }

  loadGeoJSON(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.http
        .get<FeatureCollection<Geometry, Properties>>(
          '/assets/feature-funfair/kirmes-geojson.json',
        )
        .pipe(
          catchError((error) => {
            console.error('Failed to load GeoJSON data:', error);
            const emptyFeatureCollection: FeatureCollection<
              Geometry,
              Properties
            > = {
              type: 'FeatureCollection',
              features: [],
            };
            return of(emptyFeatureCollection);
          }),
        )
        .subscribe({
          next: (data) => {
            data.features = data.features.map((feature) => {
              const mappedCategory = getMappedCategoryName(
                feature.properties?.['category'] || '',
              );
              return {
                ...feature,
                properties: {
                  ...feature.properties,
                  id: feature.properties?.['id'].toString(),
                  name: feature.properties?.['name'] || mappedCategory,
                  category: mappedCategory,
                  darkMode: this.isDarkMode,
                },
              };
            });

            this.geojsonData = data;
            this.computeCentroids();
            this.filterGeoJSON();
            this.loadFavoritesGeoJSON();
            resolve();
          },
          error: (error) => {
            console.error('Error loading GeoJSON:', error);
            reject(
              new Error(error instanceof Error ? error.message : String(error)),
            );
          },
        });
    });
  }

  setSelectedCategories(categories: string): void {
    this.selectedCategory = categories;
    this.filterGeoJSON();
  }

  async ensureGeoJSONDataLoaded(): Promise<void> {
    if (!this.geojsonData) {
      await this.loadGeoJSON();
    }
  }

  async getFeatureById(
    id: string,
  ): Promise<Feature<Geometry, Properties> | undefined> {
    if (!this.geojsonData) {
      await this.loadGeoJSON();
    }

    return this.geojsonData.features.find(
      (feature) => feature.properties?.['id'] === id,
    );
  }

  async getFeaturesByCategory(
    category: Category,
  ): Promise<Feature<Geometry, Properties>[]> {
    await this.ensureGeoJSONDataLoaded();
    return this.geojsonData.features.filter((feature) =>
      category.name.includes(feature.properties?.['category']),
    );
  }

  async searchFeaturesByName(
    searchTerm: string,
  ): Promise<Feature<Geometry, Properties>[]> {
    await this.ensureGeoJSONDataLoaded();
    return this.geojsonData.features.filter((feature) =>
      feature.properties?.['name'].toLowerCase().includes(searchTerm),
    );
  }

  getFeaturesByCategoryName$(
    categoryName: string,
  ): Observable<Feature<Geometry, Properties>[]> {
    return from(this.getFeaturesByCategoryName(categoryName));
  }

  private getIconForFeature(feature: Feature<Geometry, Properties>): string {
    const categoryIcon = feature.properties?.[
      'categoryIcon'
    ] as keyof typeof categoryIconMap;
    const isDisabled = feature.properties?.['isDisabled'];

    if (categoryIcon && categoryIconMap[categoryIcon]) {
      return categoryIconMap[categoryIcon] + (isDisabled ? '-disabled' : '');
    }

    return (
      this.getIconNameForCategory(feature.properties['category']) +
      (isDisabled ? '-disabled' : '')
    );
  }

  private computeCentroids(): void {
    const centroidFeatures: Feature<Geometry, Properties>[] = [];

    this.geojsonData.features.forEach((feature) => {
      if (
        feature.geometry.type === 'Polygon' ||
        feature.geometry.type === 'MultiPolygon'
      ) {
        const centroidPoint = centerOfMass(feature, {
          properties: feature.properties,
        });

        centroidPoint.properties = {
          ...centroidPoint.properties,
          icon: this.getIconForFeature(feature),
        };

        centroidFeatures.push(centroidPoint as Feature<Geometry, Properties>);
      }
    });

    this.centroidGeoJSON = {
      type: 'FeatureCollection',
      features: centroidFeatures,
    };
    this.filteredCentroidGeoJSONSubject.next(this.centroidGeoJSON);
  }

  private filterGeoJSON(): void {
    let fillColor: string;
    if (!this.geojsonData || !this.centroidGeoJSON) {
      return;
    }

    const filteredFeatures = this.geojsonData.features.map((feature) => {
      const isSelected =
        this.selectedCategory.includes(PredefinedCategory.all) ||
        this.selectedCategory.includes(PredefinedCategory.favorites) ||
        this.selectedCategory.includes(feature.properties?.['category']);

      if (isSelected) {
        fillColor = this.isDarkMode
          ? feature.properties?.['primary_fill_dark']
          : feature.properties?.['primary_fill_light'];
      } else {
        fillColor = this.isDarkMode
          ? feature.properties?.['secondary_fill_dark']
          : feature.properties?.['secondary_fill_light'];
      }

      return {
        ...feature,
        properties: {
          ...feature.properties,
          isDisabled: !isSelected,
          fillColor,
        },
      };
    });

    const filteredCentroidFeatures = this.centroidGeoJSON.features.map(
      (feature) => {
        const featureSourceCategory = feature.properties?.['category'] || '';
        const isSelected =
          this.selectedCategory.includes(PredefinedCategory.all) ||
          this.selectedCategory.includes(PredefinedCategory.favorites) ||
          this.getSourceCategoriesFromSelectedCategories().includes(
            featureSourceCategory,
          );

        if (isSelected) {
          fillColor = this.isDarkMode
            ? feature.properties?.['primary_fill_dark']
            : feature.properties?.['primary_fill_light'];
        } else {
          fillColor = this.isDarkMode
            ? feature.properties?.['secondary_fill_dark']
            : feature.properties?.['secondary_fill_light'];
        }

        return {
          ...feature,
          properties: {
            ...feature.properties,
            isDisabled: !isSelected,
            fillColor,
          },
        };
      },
    );

    this.filteredGeoJSONSubject.next({
      ...this.geojsonData,
      features: filteredFeatures,
    });

    this.filteredCentroidGeoJSONSubject.next({
      ...this.centroidGeoJSON,
      features: filteredCentroidFeatures,
    });
  }

  private getSourceCategoriesFromSelectedCategories(): string[] {
    const selectedCategory = this.selectedCategory;
    for (const group of this.categoryGroups) {
      for (const category of group.categories) {
        if (category.name === selectedCategory) {
          return category.sourceCategories;
        }
      }
    }
    return [];
  }

  private getIconNameForCategory(categoryName: string): string {
    for (const group of this.categoryGroups) {
      for (const category of group.categories) {
        if (category.name === categoryName) {
          return category.icon;
        }
      }
    }
    return '';
  }

  private loadFavoritesGeoJSON(): void {
    const favorites = this.funfairBookmarkService.getBookmarkedPOIs();
    const favoriteFeatures = this.geojsonData.features.filter((feature) =>
      favorites.some(
        (fav) => fav.properties?.['id'] === feature.properties?.['id'],
      ),
    );

    this.favoritesGeoJSONSubject.next({
      type: 'FeatureCollection',
      features: favoriteFeatures,
    });
  }
}
