import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {
  FeedItem,
  FeedItemLayoutType,
  FeedResult,
  ModuleInfo,
} from '@sw-code/urbo-core';
import { FunfairCardComponent } from '../../components/funfair-card/funfair-card.component';

@Injectable()
export class FunfairLoaderService {
  getPriorityFeedables(
    funfairModuleInfo: ModuleInfo,
  ): Observable<FeedResult<FeedItem>> {
    const funfairPriorityFeedItem: FeedItem = {
      id: '1',
      moduleId: funfairModuleInfo.id,
      moduleName: 'feature_fair.module_name',
      pluralModuleName: 'feature_fair.module_name_singular',
      baseRoutePath: funfairModuleInfo.baseRoutingPath,
      icon: funfairModuleInfo.icon,
      title: '',
      creationDate: new Date(),
      cardComponent: FunfairCardComponent,
      forYouComponent: FunfairCardComponent,
      relevancy: 1,
      layoutType: FeedItemLayoutType.card,
    };

    const result: FeedResult<FeedItem> = {
      items: [funfairPriorityFeedItem],
      totalRecords: 1,
    };

    return of(result);
  }
}
