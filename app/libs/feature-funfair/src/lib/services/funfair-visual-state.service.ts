import { Injectable, signal } from '@angular/core';
import { toDate } from 'date-fns-tz';

@Injectable({
  providedIn: 'root',
})
export class FunfairVisualStateService {
  private readonly showConfettiAnimation$ = signal<boolean>(true);
  private readonly showCountdown$ = signal<boolean>(true);
  private readonly targetTime: Date;

  private readonly timeZone = 'Europe/Berlin';

  constructor() {
    this.targetTime = this.getTargetTime();
  }

  get showConfettiAnimation() {
    return this.showConfettiAnimation$();
  }

  get showCountdown() {
    this.checkCountdownState();
    return this.showCountdown$();
  }

  get remainingTime(): number {
    const now = Date.now();
    const remainingTime = Math.floor((this.targetTime.getTime() - now) / 1000);
    return remainingTime;
  }

  private get countdownEnded() {
    return this.remainingTime <= 0;
  }

  disableConfettiAnimation() {
    if (this.showConfettiAnimation$()) {
      this.showConfettiAnimation$.set(false);
    }
  }

  private getTargetTime(): Date {
    const funFairStart = '2024-11-06T12:00:00';
    return toDate(funFairStart, { timeZone: this.timeZone });
  }

  private checkCountdownState() {
    if (this.countdownEnded) {
      this.disableCountdown();
    }
  }

  private disableCountdown() {
    this.showCountdown$.set(false);
  }
}
