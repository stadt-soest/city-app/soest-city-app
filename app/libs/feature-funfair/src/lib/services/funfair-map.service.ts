import { inject, Injectable } from '@angular/core';
import { Feature, Geometry } from 'geojson';
import { BehaviorSubject, Observable } from 'rxjs';
import { FunfairIconService } from './funfair-icon.service';
import { TranslocoService } from '@jsverse/transloco';
import { ThemeService } from '@sw-code/urbo-ui';
import { FunfairBookmarkService } from './funfair-bookmark.service';
import { FUNFAIR_CONFIG } from '../funfair-tokens';
import { EaseToOptions, LngLatLike, Map as MapLibreMap } from 'maplibre-gl';
import { getGeometryCenter } from '../shared/funfair-geometry-utils';
import { Properties } from '../models/feature.model';
import {
  FunfairCategory,
  PredefinedCategory,
} from '../models/funfair-category.model';

@Injectable({ providedIn: 'root' })
export class FunfairMapService {
  mapReadyPromise: Promise<MapLibreMap>;
  private readonly mapInstanceSubject = new BehaviorSubject<MapLibreMap | null>(
    null,
  );
  private funfairMapInstance?: MapLibreMap;
  private mapReadyResolver!: (map: MapLibreMap) => void;
  private readonly iconService = inject(FunfairIconService);
  private readonly translocoService = inject(TranslocoService);
  private readonly themeService = inject(ThemeService);
  private readonly bookmarkService = inject(FunfairBookmarkService);
  private readonly funfairConfig = inject(FUNFAIR_CONFIG);

  constructor() {
    this.mapReadyPromise = new Promise<MapLibreMap>((resolve) => {
      this.mapReadyResolver = resolve;
    });
  }

  get mapInstance$(): Observable<MapLibreMap | null> {
    return this.mapInstanceSubject.asObservable();
  }

  get mapInstance() {
    return this.funfairMapInstance;
  }

  async setMapInstance(mapInstance: MapLibreMap) {
    this.mapInstanceSubject.next(mapInstance);
    this.funfairMapInstance = mapInstance;
    this.mapReadyResolver(mapInstance);
    this.iconService.loadIcons(mapInstance, this.themeService.isDarkMode());
    this.setMapLanguage(mapInstance, this.translocoService.getActiveLang());
  }

  flyToCenter(): void {
    const center = this.funfairConfig.map.center;
    this.mapInstance?.flyTo({
      center,
      zoom: this.mapInstance?.getMinZoom() + 0.3,
      speed: 1.2,
      essential: true,
    });
  }

  flyToFeature(feature: Feature<Geometry, Properties>): void {
    const center = getGeometryCenter(feature);
    this.mapInstance?.flyTo({
      center,
      zoom: 18,
      speed: 1.2,
      essential: true,
    });
  }

  async panToLocation(center: LngLatLike, offsetY = 0, zoomLevel = 18) {
    if (!this.mapInstance) {
      await this.mapReadyPromise;
    }

    this.mapInstance?.flyTo({
      center,
      zoom: zoomLevel,
      speed: 1.2,
      essential: true,
      offset: [0, offsetY],
    });
  }

  easeTo(options: EaseToOptions): void {
    this.mapInstance?.easeTo({
      ...options,
      essential: true,
    });
  }

  clearMapInstance() {
    this.funfairMapInstance = undefined;
    this.mapReadyPromise = new Promise<MapLibreMap>((resolve) => {
      this.mapReadyResolver = resolve;
    });
  }

  async setLayoutProperty(layerId: string, property: string, value: any) {
    if (!this.mapInstance) {
      await this.mapReadyPromise;
    }

    if (this.mapInstance?.getLayer(layerId)) {
      this.mapInstance.setLayoutProperty(layerId, property, value);
    } else {
      console.warn(`Layer ${layerId} does not exist on the map.`);
    }
  }

  async setHighlighting(
    selectedCategoryId: FunfairCategory | null,
    selectedFeatureId: string | null,
  ): Promise<void> {
    const map = this.mapInstance;

    if (!this.mapInstance) {
      await this.mapReadyPromise;
    }

    if (
      !map?.getLayer('active-centroid-layer') ||
      !map?.getLayer('inactive-centroid-layer')
    ) {
      console.warn('Layers not yet available. Waiting for map to be idle...');
      map?.once('idle', () => {
        this.setHighlighting(selectedCategoryId, selectedFeatureId);
      });
      return;
    }

    const isDarkMode = this.themeService.isDarkMode();
    const primaryFill = isDarkMode ? 'primary_fill_dark' : 'primary_fill_light';
    const secondaryFill = isDarkMode
      ? 'secondary_fill_dark'
      : 'secondary_fill_light';

    if (selectedFeatureId && selectedCategoryId) {
      map?.setFilter('active-centroid-layer', [
        '==',
        ['get', 'id'],
        selectedFeatureId,
      ]);
      map?.setFilter('inactive-centroid-layer', [
        'all',
        ['!=', ['get', 'id'], selectedFeatureId],
      ]);
      map?.setPaintProperty('filtered-geojson-fill-layer', 'fill-color', [
        'case',
        ['==', ['get', 'id'], selectedFeatureId],
        ['get', primaryFill],
        ['get', secondaryFill],
      ]);
    } else if (selectedCategoryId === PredefinedCategory.favorites) {
      const favoriteIds = this.bookmarkService
        .getBookmarkedPOIs()
        .map((feature) => feature.id?.toString())
        .filter(Boolean) as string[];

      map?.setFilter('active-centroid-layer', [
        'in',
        ['get', 'id'],
        ['literal', favoriteIds],
      ]);
      map?.setFilter('inactive-centroid-layer', [
        '!',
        ['in', ['get', 'id'], ['literal', favoriteIds]],
      ]);
      map?.setPaintProperty('filtered-geojson-fill-layer', 'fill-color', [
        'case',
        ['in', ['get', 'id'], ['literal', favoriteIds]],
        ['get', primaryFill],
        ['get', secondaryFill],
      ]);
      return;
    } else if (
      selectedCategoryId &&
      selectedCategoryId !== PredefinedCategory.all
    ) {
      map?.setFilter('active-centroid-layer', [
        '==',
        ['get', 'category'],
        selectedCategoryId,
      ]);
      map?.setFilter('inactive-centroid-layer', [
        '!=',
        ['get', 'category'],
        selectedCategoryId,
      ]);
      map?.setPaintProperty('filtered-geojson-fill-layer', 'fill-color', [
        'match',
        ['get', 'category'],
        selectedCategoryId,
        ['get', primaryFill],
        ['get', secondaryFill],
      ]);
    } else {
      map?.setFilter('active-centroid-layer', null);
      map?.setFilter('inactive-centroid-layer', ['==', 'display', false]);
      map?.setPaintProperty('filtered-geojson-fill-layer', 'fill-color', [
        'get',
        primaryFill,
      ]);
    }
  }

  async getCenter(): Promise<LngLatLike> {
    if (!this.mapInstance) {
      await this.mapReadyPromise;
    }

    return this.mapInstance?.getCenter() ?? { lng: 0, lat: 0 };
  }

  async getZoom(): Promise<number> {
    if (!this.mapInstance) {
      await this.mapReadyPromise;
    }

    return this.mapInstance?.getZoom() ?? 14;
  }

  private async setMapLanguage(
    map: MapLibreMap,
    language: string,
  ): Promise<void> {
    const languageMap: { [key: string]: string } = { en: 'en', de: 'de' };
    const mapLanguageCode = languageMap[language] || 'en';

    map.getStyle().layers.forEach((layer) => {
      if (layer.type === 'symbol' && layer.layout?.['text-field']) {
        map.setLayoutProperty(layer.id, 'text-field', [
          'coalesce',
          ['get', `name:${mapLanguageCode}`],
          ['get', 'name'],
        ]);
      }
    });
  }
}
