import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class FunfairNavigationService {
  private readonly isOnCategoryDetailPageSubject = new BehaviorSubject<boolean>(
    false,
  );
  isOnCategoryDetailPage$ = this.isOnCategoryDetailPageSubject.asObservable();

  private readonly highlightFeatureSubject = new BehaviorSubject<{
    category: string;
    featureId: string | null;
  }>({
    category: '',
    featureId: null,
  });
  highlightFeature$ = this.highlightFeatureSubject.asObservable();

  setIsOnCategoryDetailPage(value: boolean) {
    this.isOnCategoryDetailPageSubject.next(value);
  }

  setHighlightFeature(category: string, featureId: string | null) {
    this.highlightFeatureSubject.next({ category, featureId });
  }
}
