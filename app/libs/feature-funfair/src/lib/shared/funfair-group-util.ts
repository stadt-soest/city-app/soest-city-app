import { categoryGroups } from '../components/funfair-base-filter/subcomponents/funfair-filter/funfair-category-data';

export const getNameKeyFromCategoryName = (categoryName: string): string => {
  for (const group of categoryGroups) {
    for (const category of group.categories) {
      if (category.name === categoryName) {
        return category.nameKey;
      }
    }
  }

  throw Error();
};

export const getMappedCategoryName = (
  rawCategoryName: string,
): string | null => {
  for (const group of categoryGroups) {
    for (const category of group.categories) {
      if (category.sourceCategories.includes(rawCategoryName)) {
        return category.name;
      }
    }
  }
  return null;
};

export const getCategoryFromSourceCategory = (
  sourceCategory: string,
): string => {
  for (const group of categoryGroups) {
    for (const category of group.categories) {
      if (category.sourceCategories.includes(sourceCategory)) {
        return category.name;
      }
    }
  }
  return '';
};
