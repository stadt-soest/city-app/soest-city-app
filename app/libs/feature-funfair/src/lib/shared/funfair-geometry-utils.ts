import { Feature, Geometry } from 'geojson';
import { centerOfMass } from '@turf/turf';
import { Properties } from '../models/feature.model';

export const getGeometryCenter = (
  feature: Feature<Geometry, Properties>,
): [number, number] => {
  if (!feature?.geometry) {
    throw new Error('Invalid feature: geometry is missing');
  }

  if (feature.geometry.type === 'Point') {
    const [lng, lat] = feature.geometry.coordinates;
    if (typeof lng !== 'number' || typeof lat !== 'number') {
      throw new Error('Invalid Point coordinates');
    }
    return [lng, lat];
  } else if (
    feature.geometry.type === 'Polygon' ||
    feature.geometry.type === 'MultiPolygon'
  ) {
    const centroidFeature = centerOfMass(feature);
    const [lng, lat] = centroidFeature.geometry.coordinates;
    if (typeof lng !== 'number' || typeof lat !== 'number') {
      throw new Error('Invalid centroid coordinates calculated');
    }
    return [lng, lat];
  } else {
    throw new Error(
      `Unsupported geometry type for center calculation: ${feature.geometry}`,
    );
  }
};
