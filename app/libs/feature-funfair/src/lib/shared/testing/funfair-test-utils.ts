import { ModuleCategory, ModuleInfo } from '@sw-code/urbo-core';

export const mockFunfairModuleInfo = (): ModuleInfo =>
  new ModuleInfo(
    'funfair',
    'Kirmes',
    ModuleCategory.onTheWay,
    'Attractions',
    '/funfair',
    true,
    false,
    false,
    0.2,
  );
