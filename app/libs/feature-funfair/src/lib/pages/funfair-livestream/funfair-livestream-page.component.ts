import { Component, inject } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  ActionButtonBarComponent,
  CondensedHeaderLayoutComponent,
} from '@sw-code/urbo-ui';
import { NgFor } from '@angular/common';
import { BaseShareService } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-funfair-livestream',
  templateUrl: './funfair-livestream-page.component.html',
  styleUrls: ['funfair-livestream-page.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    NgFor,
    ActionButtonBarComponent,
  ],
})
export class FunfairLivestreamPageComponent {
  sections = [
    {
      title: 'feature_funfair.livestream.first_section.title',
      content: 'feature_funfair.livestream.first_section.content',
      linkUrl: 'https://www.so-ist-soest.de/de/webcams.php',
    },
  ];
  private readonly shareService = inject(BaseShareService);

  shareLink(linkUrl: string) {
    this.shareService.share('', linkUrl, linkUrl);
  }
}
