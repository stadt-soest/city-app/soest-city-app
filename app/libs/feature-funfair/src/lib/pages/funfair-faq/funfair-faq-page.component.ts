import { Component, inject } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  ActionButtonBarComponent,
  CondensedHeaderLayoutComponent,
} from '@sw-code/urbo-ui';
import { NgFor } from '@angular/common';
import { BaseShareService, PdfService } from '@sw-code/urbo-core';
import { Router } from '@angular/router';
import { getCategoryFromSourceCategory } from '../../shared/funfair-group-util';

@Component({
  selector: 'lib-funfair-faq',
  templateUrl: './funfair-faq-page.component.html',
  styleUrls: ['funfair-faq-page.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    NgFor,
    ActionButtonBarComponent,
  ],
})
export class FunfairFaqPageComponent {
  sections = [
    {
      title: 'feature_funfair.faq.lost_and_found.title',
      content: 'feature_funfair.faq.lost_and_found.content',
      linkUrl: '',
      category: 'Infostand Stadt Soest',
      boothNumber: 213,
      pdfUrl: '',
    },
    {
      title: 'feature_funfair.faq.special_trains.title',
      content: 'feature_funfair.faq.special_trains.content',
      pdfUrl:
        'assets/feature-funfair/timetables/train/24-Plakat-Zugverbindungen.pdf',
      linkUrl: '',
      category: '',
      boothNumber: -1,
    },
    {
      title: 'feature_funfair.faq.parking_info.title',
      content: 'feature_funfair.faq.parking_info.content',
      linkUrl:
        'https://www.rlg-online.de/fahrgast/aktuelles/soester-allerheiligenkirmes#c20106',
      category: '',
      boothNumber: -1,
      pdfUrl: '',
    },
  ];
  private readonly shareService = inject(BaseShareService);
  private readonly router = inject(Router);
  private readonly pdfService = inject(PdfService);

  shareLink(linkUrl: string) {
    this.shareService.share('', linkUrl, linkUrl);
  }

  async openPDF(filePath: string) {
    await this.pdfService.openPDF(filePath);
  }

  navigateToFunfairMap(featureId: number, category: string) {
    this.router.navigate(['map/feature/funfair'], {
      queryParams: {
        id: featureId,
        category: getCategoryFromSourceCategory(category),
      },
    });
  }
}
