import { Component, inject } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  ActionButtonBarComponent,
  CondensedHeaderLayoutComponent,
} from '@sw-code/urbo-ui';
import { NgFor } from '@angular/common';
import { BaseShareService } from '@sw-code/urbo-core';
import { Router } from '@angular/router';
import { getCategoryFromSourceCategory } from '../../shared/funfair-group-util';

@Component({
  selector: 'lib-funfair-new-attractions',
  templateUrl: './funfair-new-attractions-page.component.html',
  styleUrls: ['funfair-new-attractions-page.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    NgFor,
    ActionButtonBarComponent,
  ],
})
export class FunfairNewAttractionsPageComponent {
  sections = [
    {
      title: 'feature_funfair.new_attractions.atlantis.title',
      content: 'feature_funfair.new_attractions.atlantis.content',
      featureId: 314,
      category: 'Laufgeschäft',
      linkUrl:
        'https://www.allerheiligenkirmes.de/de/veranstaltungen/herbst/allerheiligenkirmes/allgemeine-infos.php#anchor_88a50659_Accordion-NEU--Atlantis',
    },
    {
      title: 'feature_funfair.new_attractions.booster_maxxx.title',
      content: 'feature_funfair.new_attractions.booster_maxxx.content',
      featureId: 74,
      category: 'Fahrgeschäft',
      linkUrl:
        'https://www.allerheiligenkirmes.de/de/veranstaltungen/herbst/allerheiligenkirmes/allgemeine-infos.php#anchor_41a19fa9_Accordion-NEU--Booster-Maxxx',
    },
    {
      title: 'feature_funfair.new_attractions.brasil.title',
      content: 'feature_funfair.new_attractions.brasil.content',
      featureId: 124,
      category: 'Laufgeschäft',
      linkUrl:
        'https://www.allerheiligenkirmes.de/de/veranstaltungen/herbst/allerheiligenkirmes/allgemeine-infos.php#anchor_a9bc0a4d_Accordion-NEU--Brasil',
    },
    {
      title: 'feature_funfair.new_attractions.europa_rad.title',
      content: 'feature_funfair.new_attractions.europa_rad.content',
      featureId: 328,
      category: 'Fahrgeschäft',
      linkUrl:
        'https://www.allerheiligenkirmes.de/de/veranstaltungen/herbst/allerheiligenkirmes/allgemeine-infos.php#anchor_5a2945dd_Accordion-NEU--Europa-Rad',
    },
    {
      title: 'feature_funfair.new_attractions.look_360.title',
      content: 'feature_funfair.new_attractions.look_360.content',
      featureId: 216,
      category: 'Fahrgeschäft',
      linkUrl:
        'https://www.allerheiligenkirmes.de/de/veranstaltungen/herbst/allerheiligenkirmes/allgemeine-infos.php#anchor_4bf755b2_Accordion-NEU--Look-360',
    },
    {
      title: 'feature_funfair.new_attractions.loop_fighter.title',
      content: 'feature_funfair.new_attractions.loop_fighter.content',
      featureId: 217,
      category: 'Fahrgeschäft',
      linkUrl:
        'https://www.allerheiligenkirmes.de/de/veranstaltungen/herbst/allerheiligenkirmes/allgemeine-infos.php#anchor_a0d49e91_Accordion-NEU--Loop-Fighter',
    },
    {
      title: 'feature_funfair.new_attractions.montgolfiere.title',
      content: 'feature_funfair.new_attractions.montgolfiere.content',
      featureId: 291,
      category: 'Fahrgeschäft',
      linkUrl:
        'https://www.allerheiligenkirmes.de/de/veranstaltungen/herbst/allerheiligenkirmes/allgemeine-infos.php#anchor_e50938cb_Accordion-NEU--Montgolfiere',
    },
  ];
  private readonly shareService = inject(BaseShareService);
  private readonly router = inject(Router);

  shareLink(linkUrl: string) {
    this.shareService.share('', linkUrl, linkUrl);
  }

  navigateToFunfairMap(featureId: number, category: string) {
    this.router.navigate(['map/feature/funfair'], {
      queryParams: {
        id: featureId,
        category: getCategoryFromSourceCategory(category),
      },
    });
  }
}
