import {
  Component,
  DestroyRef,
  inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  IonContent,
  IonModal,
  ModalController,
  ViewWillEnter,
  ViewWillLeave,
} from '@ionic/angular/standalone';
import { TranslocoDirective } from '@jsverse/transloco';
import { NgClass, NgIf } from '@angular/common';
import {
  CustomButtonColorScheme,
  CustomButtonType,
  CustomHorizontalButtonComponent,
  IconColor,
  IconFontSize,
  IconType,
} from '@sw-code/urbo-ui';
import {
  FunfairMapChipsComponent,
  FunfairMapChipsState,
} from '../../components/funfair-map-chips/funfair-map-chips.component';
import { FunfairCenterInfoCardComponent } from '../../components/funfair-center-info-card/funfair-center-info-card.component';
import { FunfairSearchComponent } from '../../components/funfair-search/funfair-search.component';
import { FunfairMapComponent } from '../../components/funfair-map/funfair-map.component';
import { FunfairBaseFilterComponent } from '../../components/funfair-base-filter/funfair-base-filter.component';
import {
  FunfairCategory,
  PredefinedCategory,
} from '../../models/funfair-category.model';
import { DataService } from '../../services/geojson-data.service';
import { FunfairBookmarkService } from '../../services/funfair-bookmark.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FunfairCategoryService } from '../../services/funfair-category.service';
import { FunfairMapService } from '../../services/funfair-map.service';
import { ModalDismissalService } from '../../services/funfair-modal-dismissal.service';
import { GeolocationService } from '@sw-code/urbo-core';
import { FunfairNavigationService } from '../../services/funfair-navigation.service';
import { FUNFAIR_CONFIG } from '../../funfair-tokens';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { Network } from '@capacitor/network';
import { Feature, Geometry } from 'geojson';
import { Properties } from '../../models/feature.model';
import { getNameKeyFromCategoryName } from '../../shared/funfair-group-util';
import { getGeometryCenter } from '../../shared/funfair-geometry-utils';
import { FunfairCategoryDetailListComponent } from '../../components/funfair-base-filter/subcomponents/funfair-category-detail-list/funfair-category-detail-list.component';
import { FunfairPoi } from '../../models/poi.model';
import { distinctUntilChanged, filter, firstValueFrom } from 'rxjs';
import { FunfairFilterComponent } from '../../components/funfair-base-filter/subcomponents/funfair-filter/funfair-filter.component';
import { SafeArea } from 'capacitor-plugin-safe-area';

@Component({
  selector: 'lib-funfair-map-page',
  templateUrl: './funfair-map-page.component.html',
  styleUrl: 'funfair-map-page.component.scss',
  imports: [
    TranslocoDirective,
    IonContent,
    NgClass,
    NgIf,
    CustomHorizontalButtonComponent,
    FunfairMapChipsComponent,
    FunfairCenterInfoCardComponent,
    IonModal,
    FunfairSearchComponent,
    FunfairMapComponent,
    FunfairBaseFilterComponent,
  ],
})
export class FunfairMapPageComponent
  implements ViewWillEnter, ViewWillLeave, OnInit
{
  @ViewChild('funfairMap') funfairMap!: FunfairMapComponent;
  @ViewChild(FunfairBaseFilterComponent)
  funfairMapComponentReference!: FunfairBaseFilterComponent;
  @ViewChild('searchModal') searchModal!: HTMLIonModalElement;

  isModalOpen = false;
  selectedCategoryNameKey = '';
  initialRootPage: any;
  initialRootParams: any;
  isOnline = false;
  showCenterButton = false;
  isPoiClicked = false;
  safeAreaInsets = { top: 0, bottom: 0, left: 0, right: 0 };
  protected readonly iconColor = IconColor;
  protected readonly iconType = IconType;
  protected readonly iconFontSize = IconFontSize;
  protected readonly customButtonType = CustomButtonType;
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  private funfairCategory: FunfairCategory = PredefinedCategory.all;
  private readonly modalController = inject(ModalController);
  private readonly dataService = inject(DataService);
  private readonly funfairBookmarkService = inject(FunfairBookmarkService);
  private readonly route = inject(ActivatedRoute);
  private readonly router = inject(Router);
  private readonly categoryService = inject(FunfairCategoryService);
  private readonly mapService = inject(FunfairMapService);
  private readonly destroyRef = inject(DestroyRef);
  private readonly modalDismissalService = inject(ModalDismissalService);
  private readonly geolocationService = inject(GeolocationService);
  private readonly funfairNavigationService = inject(FunfairNavigationService);
  private readonly funfairConfig = inject(FUNFAIR_CONFIG);

  get currentState(): FunfairMapChipsState {
    if (this.isModalOpen) {
      return 'modalOpen';
    } else if (this.selectedCategory === PredefinedCategory.all) {
      return 'allCategoriesSelected';
    } else {
      return 'categorySelected';
    }
  }

  get selectedCategory(): FunfairCategory {
    return this.funfairCategory;
  }

  async ngOnInit() {
    await this.fetchSafeAreaValues();
    this.modalDismissalService.dismissDataEvent$
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((data) => {
        this.selectedCategoryNameKey = data.categoryNameKey;
        this.setSelectedCategory(data.categoryName);
        this.funfairMap?.setHighlighting?.(this.selectedCategory, null);
      });

    this.subscripeToNavigationToPoi();
    await this.handleQueryParams();
    await this.checkNetworkStatus();
    await Network.addListener('networkStatusChange', (status) => {
      this.isOnline = status.connected;
    });
  }

  onShowCenterButtonChange(value: boolean) {
    this.showCenterButton = value;
  }

  async onFeatureClick(event: {
    feature: Feature<Geometry, Properties>;
    categoryName: string;
  }) {
    const { feature, categoryName } = event;
    const featureId = feature.properties['id'];
    const isFromBookmark = this.funfairBookmarkService
      .getBookmarkedPOIs()
      .some((bookmark) => bookmark.id?.toString() === featureId.toString());

    const effectiveCategoryName = isFromBookmark
      ? PredefinedCategory.favorites
      : categoryName;
    const effectiveCategoryNameKey = isFromBookmark
      ? 'feature_funfair.map.category_list.categories.favorites'
      : getNameKeyFromCategoryName(categoryName);

    if (this.selectedCategory !== effectiveCategoryName) {
      this.selectedCategoryNameKey = effectiveCategoryNameKey;
      this.setSelectedCategory(effectiveCategoryName);
    }

    this.isPoiClicked = true;

    const { offsetY } = this.calculateModalOffset();
    const center = getGeometryCenter(feature);
    await this.mapService.panToLocation(center, offsetY);

    const newParams = {
      clickedFeatureId: featureId,
      categoryName,
      categoryNameKey: this.selectedCategoryNameKey,
      isFromBookmark,
    };

    await this.closeSearchModal();

    if (this.isModalOpen) {
      this.funfairMapComponentReference.navigateToPage(
        FunfairCategoryDetailListComponent,
        newParams,
      );
    } else {
      this.initialRootPage = FunfairCategoryDetailListComponent;
      this.initialRootParams = newParams;
      this.isModalOpen = true;
    }

    this.funfairMap?.setHighlighting(effectiveCategoryName, featureId);
    this.isPoiClicked = false;
  }

  async onSearchFeatureClick(poi: FunfairPoi) {
    const featureId = poi.id;
    const effectiveCategoryName = poi.category;
    this.selectedCategoryNameKey = getNameKeyFromCategoryName(poi.category);
    this.setSelectedCategory(effectiveCategoryName);

    this.isPoiClicked = true;

    const { offsetY } = this.calculateModalOffset();
    const center = getGeometryCenter(poi.feature);
    await this.mapService.panToLocation(center, offsetY);
    this.funfairMap?.setHighlighting(effectiveCategoryName, featureId);
    this.isPoiClicked = false;
  }

  async onModalDismiss(data: any) {
    this.isModalOpen = false;
    await this.resetMapAfterModalDismiss();
    if (data?.selectedCategories) {
      this.setSelectedCategory(data.selectedCategories);
      this.selectedCategoryNameKey = data.categoryNameKey;
    }
  }

  async ionViewWillLeave() {
    await this.closeSearchModal();
    this.dismissModalIfOpen();
    this.geolocationService.stopWatchingLocation();
    this.reset();
  }

  ionViewWillEnter() {
    this.geolocationService.startWatchingLocation();
  }

  async navigateToMainMap() {
    await this.closeFilterModal();
    await this.closeSearchModal();
    await this.router.navigateByUrl('/map');
  }

  onCenterButtonClick() {
    if (this.funfairMap) {
      this.funfairMap.recenterMap();
    }
  }

  async openFilterModal() {
    await this.closeSearchModal();

    if (
      this.selectedCategory &&
      this.selectedCategory !== PredefinedCategory.all
    ) {
      this.initialRootPage = FunfairCategoryDetailListComponent;
      this.initialRootParams = {
        categoryName: this.selectedCategory,
        categoryNameKey: this.selectedCategoryNameKey,
        features:
          this.selectedCategory === PredefinedCategory.favorites
            ? this.funfairBookmarkService.getBookmarkedPOIs()
            : (await firstValueFrom(this.dataService.getFilteredGeoJSON()))
                ?.features,
      };
    } else {
      this.initialRootPage = FunfairFilterComponent;
      this.initialRootParams = {
        categoryName: this.selectedCategory,
        categoryNameKey: this.selectedCategoryNameKey,
        features:
          this.selectedCategory === PredefinedCategory.favorites
            ? this.funfairBookmarkService.getBookmarkedPOIs()
            : (await firstValueFrom(this.dataService.getFilteredGeoJSON()))
                ?.features,
      };
    }

    this.isModalOpen = true;
    await this.panToCurrentPositionOnModalOpen();
  }

  async openSearchModal() {
    this.isModalOpen = false;
    await this.searchModal.present();
  }

  async closeSearchModal() {
    await this.searchModal.dismiss();
  }

  async resetToAllCategories(): Promise<void> {
    this.funfairMap?.setHighlighting(null, null);
    this.setSelectedCategory(PredefinedCategory.all);
  }

  async closeFilterModal() {
    if (this.isModalOpen) {
      await this.modalController.dismiss();
      this.isModalOpen = false;
    }
  }

  reset() {
    this.setSelectedCategory(PredefinedCategory.all);
  }

  openCategoryDetailModal(featureId?: string) {
    this.initialRootPage = FunfairCategoryDetailListComponent;
    this.initialRootParams = {
      clickedFeatureId: featureId,
      categoryName: this.selectedCategory,
      categoryNameKey: this.selectedCategoryNameKey,
    };
    this.isModalOpen = true;
  }

  async searchPoiClicked(poi: FunfairPoi) {
    await this.onSearchFeatureClick(poi);
  }

  private async openFavoritesModal() {
    try {
      this.setSelectedCategory(PredefinedCategory.favorites);
      this.selectedCategoryNameKey =
        'feature_funfair.map.category_list.categories.favorites';

      await this.mapService.panToLocation(
        this.funfairConfig.map.center,
        undefined,
        16,
      );
      this.funfairMap?.setHighlighting(PredefinedCategory.favorites, null);

      this.initialRootPage = FunfairCategoryDetailListComponent;
      this.initialRootParams = {
        categoryName: this.selectedCategory,
        categoryNameKey: this.selectedCategoryNameKey,
        features: this.funfairBookmarkService.getBookmarkedPOIs(),
      };
      this.isModalOpen = true;
    } catch (error) {
      console.error('Error opening favorites modal:', error);
    }
  }

  private async panToCurrentPositionOnModalOpen() {
    try {
      if (
        this.isModalOpen &&
        !this.isPoiClicked &&
        this.mapService.mapInstance
      ) {
        const currentCenter = this.mapService.mapInstance.getCenter();
        const { offsetY } = this.calculateModalOffset();
        await this.mapService.panToLocation(currentCenter, offsetY, 16);
      }
    } catch (error) {
      console.error('Error in panToCurrentPositionOnModalOpen:', error);
    }
  }

  private async resetMapAfterModalDismiss() {
    try {
      const currentCenter = await this.mapService.getCenter();
      const currentZoom = await this.mapService.getZoom();
      await this.mapService.panToLocation(
        currentCenter,
        -this.calculateModalOffset().offsetY,
        currentZoom,
      );
    } catch (error) {
      console.error('Error in resetMapAfterModalDismiss:', error);
    }
  }

  private calculateModalOffset(): { modalHeight: number; offsetY: number } {
    const modalHeight = window.innerHeight * 0.7;
    const offsetY = -(modalHeight / 2) + this.safeAreaInsets.top;
    return { modalHeight, offsetY };
  }

  private async fetchSafeAreaValues() {
    try {
      const { insets } = await SafeArea.getSafeAreaInsets();
      this.safeAreaInsets = insets;
    } catch (_error) {
      /* empty */
    }
  }

  private async onFeatureClickByIdAndCategory(id: string, category: string) {
    try {
      this.selectedCategoryNameKey = getNameKeyFromCategoryName(category);
      this.setSelectedCategory(category);

      const feature = await this.dataService.getFeatureById(id);
      if (!feature) {
        console.error(`Feature with id ${id} not found.`);
        return;
      }

      setTimeout(async () => {
        const { offsetY } = this.calculateModalOffset();
        const center = getGeometryCenter(feature);
        await this.mapService.panToLocation(center, offsetY);
        this.funfairMap?.setHighlighting(category, id);

        this.isModalOpen = false;
        this.openCategoryDetailModal(id);
      }, 500);
    } catch (error) {
      console.error('Error in onFeatureClickByIdAndCategory:', error);
    }
  }

  private async handleQueryParams() {
    this.categoryService.currentCategory$
      .pipe(distinctUntilChanged(), takeUntilDestroyed(this.destroyRef))
      .subscribe(async (category) => {
        this.dataService.setSelectedCategories(category);
        this.funfairCategory = category;
        this.funfairMap?.setHighlighting(category, null);

        if (this.isModalOpen && !this.isPoiClicked) {
          const { offsetY } = this.calculateModalOffset();
          this.mapService.panToLocation(
            this.funfairConfig.map.center,
            offsetY,
            16,
          );
        } else {
          this.mapService.flyToCenter();
        }
      });

    this.route.queryParams
      .pipe(
        takeUntilDestroyed(this.destroyRef),
        filter((params) => params['id'] || params['category']),
      )
      .subscribe((params) => {
        const category = params['category'];
        const id = params['id'];

        if (category === PredefinedCategory.favorites) {
          this.openFavoritesModal();
        } else if (id && category) {
          this.onFeatureClickByIdAndCategory(id, category);
        } else if (id !== undefined) {
          this.loadFeatureById(id);
        }

        this.router.navigate([], {
          queryParams: {},
          replaceUrl: true,
        });
      });
  }

  private async loadFeatureById(id: string) {
    try {
      const feature = await this.dataService.getFeatureById(id);

      if (!feature) {
        console.error(`feature with ${id} not found`);
        return;
      }

      const { offsetY } = this.calculateModalOffset();
      const center = getGeometryCenter(feature);
      this.mapService.panToLocation(center, offsetY);
    } catch (error) {
      console.error('Error loading feature by ID:', error);
    }
  }

  private async checkNetworkStatus() {
    const status = await Network.getStatus();
    this.isOnline = status.connected;
  }

  private async dismissModalIfOpen() {
    if (this.isModalOpen) {
      await this.modalController.dismiss();
      this.isModalOpen = false;
      await this.mapService.setHighlighting(this.selectedCategory, null);
    }
  }

  private setSelectedCategory(category: FunfairCategory) {
    this.funfairCategory = category;
    this.categoryService.setCurrentCategory(category);
  }

  private subscripeToNavigationToPoi() {
    this.funfairNavigationService.highlightFeature$
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((data) => {
        this.funfairMap?.setHighlighting(data.category, data.featureId);
      });
  }
}
