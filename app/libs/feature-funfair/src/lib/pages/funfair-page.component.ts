import { Component, DestroyRef, inject, OnInit } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  CondensedHeaderLayoutComponent,
  CustomButtonColorScheme,
  CustomButtonType,
  CustomVerticalButtonComponent,
  NavigationListComponent,
  NavigationListData,
} from '@sw-code/urbo-ui';
import { FunfairMapCardComponent } from '../components/funfair-map-card/funfair-map-card.component';
import { NgIf } from '@angular/common';
import { FunfairFeatureCardComponent } from '../components/funfair-feature-card/funfair-feature-card.component';
import { Feature, Geometry } from 'geojson';
import { Properties } from '../models/feature.model';
import { Router } from '@angular/router';
import { FunfairBookmarkService } from '../services/funfair-bookmark.service';
import { PredefinedCategory } from '../models/funfair-category.model';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'lib-funfair',
  templateUrl: './funfair-page.component.html',
  styleUrls: ['./funfair-page.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    NavigationListComponent,
    FunfairMapCardComponent,
    NgIf,
    FunfairFeatureCardComponent,
    CustomVerticalButtonComponent,
  ],
})
export class FunfairPageComponent implements OnInit {
  savedFavorites: Feature<Geometry, Properties>[] = [];
  favoritesSubtitle!: number;
  favoritesContent = '';
  hasFavorites = false;
  menuItemGroups: NavigationListData[] = [
    {
      menuItems: [
        {
          baseRoutingPath: 'opening-times',
          icon: 'event',
          title: 'feature_funfair.category.opening_times',
        },
        {
          baseRoutingPath: 'new-attractions',
          icon: 'local_activity',
          title: 'feature_funfair.category.new_attractions',
        },
        {
          baseRoutingPath: 'faq',
          icon: 'help',
          title: 'feature_funfair.category.faq',
        },
        {
          baseRoutingPath: 'travel-stay',
          icon: 'directions_transit',
          title: 'feature_funfair.category.arrival_stay',
        },
        {
          baseRoutingPath: 'livestream',
          icon: 'live_tv',
          title: 'feature_funfair.category.livestream',
        },
      ],
      category: 'feature_funfair.category.information',
    },
  ];
  protected readonly customButtonType = CustomButtonType;
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  private readonly router = inject(Router);
  private readonly funfairBookmarkService = inject(FunfairBookmarkService);
  private readonly destroyRef = inject(DestroyRef);

  ngOnInit(): void {
    this.subscribeToFavorites();
  }

  async selectFavorites() {
    if (this.savedFavorites.length === 0) {
      return;
    }

    await this.router.navigate(['/map/feature/funfair'], {
      queryParams: {
        category: PredefinedCategory.favorites,
      },
    });
  }

  private subscribeToFavorites() {
    this.funfairBookmarkService
      .getBookmarkedPOIs$()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((bookmarks) => {
        this.savedFavorites = bookmarks;
        this.hasFavorites = this.savedFavorites.length > 0;
        this.favoritesSubtitle = this.savedFavorites.length;
        this.favoritesContent = this.savedFavorites
          .map((feature) => feature.properties?.['name'] || 'Unknown')
          .join(', ');
      });
  }
}
