import { Component, inject } from '@angular/core';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import {
  ActionButtonBarComponent,
  CondensedHeaderLayoutComponent,
} from '@sw-code/urbo-ui';
import { NgFor, NgIf } from '@angular/common';
import { BaseShareService, MapsNavigationService } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-funfair-opening-times',
  templateUrl: './funfair-opening-times-page.component.html',
  styleUrls: ['funfair-opening-times-page.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    NgFor,
    NgIf,
    ActionButtonBarComponent,
  ],
})
export class FunfairOpeningTimesPageComponent {
  events = [
    {
      key: 'feature_funfair.events.opening_hours',
      times: [
        { key: 'feature_funfair.events.opening_hours.times.wednesday' },
        { key: 'feature_funfair.events.opening_hours.times.thursday' },
        { key: 'feature_funfair.events.opening_hours.times.friday' },
        { key: 'feature_funfair.events.opening_hours.times.saturday' },
        { key: 'feature_funfair.events.opening_hours.times.sunday' },
      ],
      link: 'https://www.allerheiligenkirmes.de/de/veranstaltungen/herbst/allerheiligenkirmes/allgemeine-infos.php#anchor_7844a400_Oeffnungszeiten',
    },
    {
      key: 'feature_funfair.events.opening_ceremony',
      times: [
        { key: 'feature_funfair.events.opening_ceremony.times.wednesday' },
      ],
      link: 'https://www.allerheiligenkirmes.de/de/veranstaltungen/herbst/allerheiligenkirmes/programm.php',
      longitude: '8.107092018543431',
      latitude: ' 51.57178129178428',
      address: '"Look 360°" - Petrikirchhof Nord',
    },
    {
      key: 'feature_funfair.events.horse_market',
      times: [{ key: 'feature_funfair.events.horse_market.times.thursday' }],
      link: 'https://www.allerheiligenkirmes.de/de/veranstaltungen/herbst/allerheiligenkirmes/programm.php',
    },
    {
      key: 'feature_funfair.events.model_exhibition',
      times: [
        { key: 'feature_funfair.events.model_exhibition.times.daily' },
        { key: 'feature_funfair.events.model_exhibition.times.during_kirmes' },
      ],
      link: 'https://www.allerheiligenkirmes.de/de/veranstaltungen/herbst/allerheiligenkirmes/programm.php',
      longitude: '8.110040579116804',
      latitude: '51.57474262542435',
      address: 'Wiese-Gemeindehaus – Widumgasse 1',
    },
  ];
  private readonly shareService = inject(BaseShareService);
  private readonly mapsNavigationService = inject(MapsNavigationService);
  private readonly translateService = inject(TranslocoService);

  shareLink(linkUrl: string) {
    this.shareService.share('', linkUrl, linkUrl);
  }

  getActions(event: any): any[] {
    const actions = [
      {
        label: this.translateService.translate(
          'feature_funfair.buttons.in_browser',
        ),
        icon: 'open_in_new',
        linkUrl: event.link,
      },
      {
        label: this.translateService.translate('feature_funfair.buttons.share'),
        onClick: this.shareLink.bind(this, event.link),
        icon: 'share',
      },
    ];

    if (event.latitude && event.longitude) {
      actions.push({
        icon: 'directions',
        label: this.translateService.translate(
          'feature_funfair.poi_card.navigate',
        ),
        onClick: () => {
          this.navigateToLocation(event.latitude, event.longitude).catch(
            (err) => {
              console.error('Error navigating to location:', err);
            },
          );
        },
      });
    }

    return actions;
  }

  async navigateToLocation(latitude: number, longitude: number): Promise<void> {
    await this.mapsNavigationService.navigateToLocation(latitude, longitude);
  }
}
