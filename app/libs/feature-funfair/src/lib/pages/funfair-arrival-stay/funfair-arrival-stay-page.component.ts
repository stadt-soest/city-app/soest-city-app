import { Component, inject, OnInit } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  ActionButtonBarComponent,
  CondensedHeaderLayoutComponent,
  CustomButtonColorScheme,
  CustomButtonType,
  CustomVerticalButtonComponent,
} from '@sw-code/urbo-ui';
import { KeyValuePipe, NgFor } from '@angular/common';
import {
  BaseShareService,
  DeviceService,
  PdfService,
} from '@sw-code/urbo-core';
import { HttpClient } from '@angular/common/http';
import { lastValueFrom } from 'rxjs';

@Component({
  selector: 'lib-funfair-arrival-stay',
  templateUrl: './funfair-arrival-stay-page.component.html',
  styleUrls: ['funfair-arrival-stay-page.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    NgFor,
    ActionButtonBarComponent,
    CustomVerticalButtonComponent,
    KeyValuePipe,
  ],
})
export class FunfairArrivalStayPageComponent implements OnInit {
  sections = [
    {
      title: 'feature_funfair.arrival_stay.first_section.title',
      content: 'feature_funfair.arrival_stay.first_section.content',
      linkUrl:
        'https://www.so-ist-soest.de/de/veranstaltungen/herbst/allerheiligenkirmes/anreise-und-aufenthalt_ahk.php',
    },
  ];
  specialBusTimetables: { [key: string]: { file: string } } = {};
  mobileInfoUrl!: string;
  protected readonly customButtonType = CustomButtonType;
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  private readonly shareService = inject(BaseShareService);
  private readonly http = inject(HttpClient);
  private readonly deviceService = inject(DeviceService);
  private readonly pdfService = inject(PdfService);

  shareLink(linkUrl: string) {
    this.shareService.share('', linkUrl, linkUrl);
  }

  ngOnInit() {
    this.loadTimetableJSON();
    this.setMobileInfoUrl();
  }

  async loadTimetableJSON(): Promise<void> {
    try {
      this.specialBusTimetables = await lastValueFrom(
        this.http.get<any>('/assets/feature-funfair/bus-timetables.json'),
      );
    } catch (error) {
      console.error('Failed to load bus timetables data:', error);
      this.specialBusTimetables = {};
    }
  }

  async openPDF(filePath: string) {
    await this.pdfService.openPDF(filePath);
  }

  private setMobileInfoUrl() {
    if (this.deviceService.isIOS()) {
      this.mobileInfoUrl =
        'https://apps.apple.com/de/app/mobil-info/id530482296';
    } else if (this.deviceService.isAndroid()) {
      this.mobileInfoUrl =
        'https://play.google.com/store/apps/details?id=de.geomobile.busguide&hl=de';
    } else {
      this.mobileInfoUrl =
        'https://www.rlg-online.de/fahrgast/tickets-tarife/eticket';
    }
  }
}
