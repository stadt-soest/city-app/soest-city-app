import {
  Inject,
  InjectionToken,
  ModuleWithProviders,
  NgModule,
  inject,
  provideAppInitializer,
} from '@angular/core';
import { UIModule } from '@sw-code/urbo-ui';
import { FunfairLoaderService } from './services/feedables-loader/funfair-feedables-loader.service';
import { FunfairVisualStateService } from './services/funfair-visual-state.service';
import { provideTranslocoScope } from '@jsverse/transloco';
import {
  FeatureModule,
  ModuleCategory,
  ModuleInfo,
  ModuleRegistryService,
} from '@sw-code/urbo-core';
import { of } from 'rxjs';
import { FunfairMapButtonComponent } from './components/funfair-map-button/funfair-map-button.component';
import { FunfairSectionComponent } from './components/funfair-section/funfair-section.component';
import { FUNFAIR_CONFIG, FunfairConfig } from './funfair-tokens';

const MODULES = [UIModule];
const PROVIDERS = [
  FunfairLoaderService,
  FunfairVisualStateService,
  provideTranslocoScope({
    scope: 'feature-funfair',
    alias: 'feature_funfair',
    loader: {
      en: () => import('./i18n/en.json'),
      de: () => import('./i18n/de.json'),
    },
  }),
];
const moduleId = 'funfair';
export const FUNFAIR_MODULE_INFO = new InjectionToken<ModuleInfo>(
  'FunFairModuleInfoHolderToken',
);

@NgModule({
  imports: [...MODULES],
  exports: [...MODULES],
  providers: [
    ...PROVIDERS,
    {
      provide: FUNFAIR_MODULE_INFO,
      useValue: {
        id: moduleId,
        name: 'feature_funfair.module_name',
        category: ModuleCategory.onTheWay,
        icon: 'attractions',
        baseRoutingPath: moduleId,
        defaultVisibility: true,
        settingsPageAvailable: false,
        isAboutAppItself: false,
        relevancy: 0.2,
        forYouRelevant: true,
        showInCategoriesPage: true,
        orderInCategory: 3,
      },
    },
  ],
})
export class FeatureFunfairModule {
  readonly featureModule: FeatureModule<null>;

  constructor(
    @Inject(FUNFAIR_MODULE_INFO) private readonly funfairModuleInfo: ModuleInfo,
  ) {
    this.featureModule = {
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      initializeDefaultSettings: () => {},
      feedables: () => of({ items: [], totalRecords: 0 }),
      info: this.funfairModuleInfo,
      getCustomMapButton: () =>
        of({
          component: FunfairMapButtonComponent,
        }),
      routes: [
        {
          path: `categories/${moduleId}`,
          loadComponent: () =>
            import('./pages/funfair-page.component').then(
              (m) => m.FunfairPageComponent,
            ),
          title: 'feature_funfair.module_name',
        },
        {
          path: `categories/${moduleId}/new-attractions`,
          loadComponent: () =>
            import(
              './pages/funfair-new-attractions/funfair-new-attractions-page.component'
            ).then((m) => m.FunfairNewAttractionsPageComponent),
          title: 'feature_funfair.module_name',
        },
        {
          path: `categories/${moduleId}/faq`,
          loadComponent: () =>
            import('./pages/funfair-faq/funfair-faq-page.component').then(
              (m) => m.FunfairFaqPageComponent,
            ),
          title: 'feature_funfair.module_name',
        },
        {
          path: `categories/${moduleId}/opening-times`,
          loadComponent: () =>
            import(
              './pages/funfair-opening-times/funfair-opening-times-page.component'
            ).then((m) => m.FunfairOpeningTimesPageComponent),
          title: 'feature_funfair.module_name',
        },
        {
          path: `categories/${moduleId}/travel-stay`,
          loadComponent: () =>
            import(
              './pages/funfair-arrival-stay/funfair-arrival-stay-page.component'
            ).then((m) => m.FunfairArrivalStayPageComponent),
          title: 'feature_funfair.module_name',
        },
        {
          path: `categories/${moduleId}/livestream`,
          loadComponent: () =>
            import(
              './pages/funfair-livestream/funfair-livestream-page.component'
            ).then((m) => m.FunfairLivestreamPageComponent),
          title: 'feature_funfair.module_name',
        },
        {
          path: `for-you/${moduleId}`,
          loadComponent: () =>
            import('./pages/funfair-page.component').then(
              (m) => m.FunfairPageComponent,
            ),
          title: 'feature_funfair.module_name',
        },
        {
          path: `for-you/${moduleId}/new-attractions`,
          loadComponent: () =>
            import(
              './pages/funfair-new-attractions/funfair-new-attractions-page.component'
            ).then((m) => m.FunfairNewAttractionsPageComponent),
          title: 'feature_funfair.module_name',
        },
        {
          path: `for-you/${moduleId}/faq`,
          loadComponent: () =>
            import('./pages/funfair-faq/funfair-faq-page.component').then(
              (m) => m.FunfairFaqPageComponent,
            ),
          title: 'feature_funfair.module_name',
        },
        {
          path: `for-you/${moduleId}/opening-times`,
          loadComponent: () =>
            import(
              './pages/funfair-opening-times/funfair-opening-times-page.component'
            ).then((m) => m.FunfairOpeningTimesPageComponent),
          title: 'feature_funfair.module_name',
        },
        {
          path: `for-you/${moduleId}/external-information`,
          loadComponent: () =>
            import(
              './pages/funfair-arrival-stay/funfair-arrival-stay-page.component'
            ).then((m) => m.FunfairArrivalStayPageComponent),
          title: 'feature_funfair.module_name',
        },
        {
          path: `map/feature/${moduleId}`,
          loadComponent: () =>
            import('./pages/funfair-map/funfair-map-page.component').then(
              (m) => m.FunfairMapPageComponent,
            ),
          title: 'feature_funfair.module_name',
        },
      ],
      getCustomForYouSection: () =>
        of({
          component: FunfairSectionComponent,
        }),
    };
  }

  static forRoot(
    funfairConfig: FunfairConfig,
  ): ModuleWithProviders<FeatureFunfairModule> {
    return {
      ngModule: FeatureFunfairModule,
      providers: [
        {
          provide: FUNFAIR_CONFIG,
          useValue: funfairConfig,
        },
        provideAppInitializer(() => {
          const initializerFn = (
            (registry: ModuleRegistryService, module: FeatureFunfairModule) =>
            () =>
              registry.registerFeature(module.featureModule)
          )(inject(ModuleRegistryService), inject(FeatureFunfairModule));
          return initializerFn();
        }),
      ],
    };
  }
}
