import { Component, inject } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { SectionComponent } from '@sw-code/urbo-ui';
import { FunfairBookmarkService } from '../../services/funfair-bookmark.service';
import { map } from 'rxjs';
import { FunfairCardComponent } from '../funfair-card/funfair-card.component';
import { FunfairForYouMapCardComponent } from '../funfair-for-you-map-card/funfair-for-you-map-card.component';
import { FunfairFavoritesForYouCardComponent } from '../funfair-favorites-for-you-card/funfair-favorites-for-you-card.component';
import { FunfairInfoCardComponent } from '../funfair-info-card/funfair-info-card.component';
import { FunfairVisibilityCardComponent } from '../funfair-visibility-card/funfair-visibility-card.component';

@Component({
  selector: 'lib-funfair-section',
  templateUrl: './funfair-section.component.html',
  imports: [TranslocoDirective, SectionComponent],
})
export class FunfairSectionComponent {
  private readonly funfairBookmarkService = inject(FunfairBookmarkService);
  private favorites$ = this.funfairBookmarkService
    .getBookmarkedPOIs$()
    .pipe(map((favorites) => favorites.length > 0));
  items = [
    { component: FunfairCardComponent },
    { component: FunfairForYouMapCardComponent },
    {
      component: FunfairFavoritesForYouCardComponent,
      visible$: this.favorites$,
    },
    { component: FunfairInfoCardComponent },
    { component: FunfairVisibilityCardComponent },
  ];
}
