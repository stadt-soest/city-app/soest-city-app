import { Component, inject, OnDestroy, OnInit } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { IonRouterLinkWithHref } from '@ionic/angular/standalone';
import { RouterLink } from '@angular/router';
import { NgClass, NgFor, NgIf } from '@angular/common';
import { AbstractFeedCardComponent, FeedItem } from '@sw-code/urbo-core';
import { FunfairVisualStateService } from '../../services/funfair-visual-state.service';

type CountdownKey = 'days' | 'hours' | 'minutes' | 'seconds';

@Component({
  selector: 'lib-funfair-card',
  templateUrl: './funfair-card.component.html',
  styleUrls: ['./funfair-card.component.scss'],
  imports: [
    TranslocoDirective,
    IonRouterLinkWithHref,
    RouterLink,
    NgFor,
    NgIf,
    NgClass,
  ],
})
export class FunfairCardComponent
  extends AbstractFeedCardComponent<FeedItem>
  implements OnInit, OnDestroy
{
  private static readonly colorClasses = [
    'accent-teal',
    'accent-orange',
    'accent-green',
    'accent-red',
    'accent-mint',
    'accent-blue',
  ] as const;
  countdownData = { days: '00', hours: '00', minutes: '00', seconds: '00' };
  countdownEnded = false;
  private countdownCheckInterval: any;
  private readonly visualState = inject(FunfairVisualStateService);

  ngOnInit() {
    this.handleCountdown();
  }

  ngOnDestroy() {
    if (this.countdownCheckInterval) {
      clearInterval(this.countdownCheckInterval);
    }
  }

  shouldDisplayUnit(key: string): boolean {
    switch (key) {
      case 'days':
        return this.countdownData.days !== '00';
      case 'hours':
        return (
          this.shouldDisplayUnit('days') || this.countdownData.hours !== '00'
        );
      case 'minutes':
        return (
          this.shouldDisplayUnit('hours') || this.countdownData.minutes !== '00'
        );
      case 'seconds':
        return true;
      default:
        return false;
    }
  }

  getColorClass(
    index: number,
  ): (typeof FunfairCardComponent.colorClasses)[number] {
    return FunfairCardComponent.colorClasses[
      index % FunfairCardComponent.colorClasses.length
    ];
  }

  getCountdownValue(key: string): string {
    return this.countdownData[key as CountdownKey];
  }

  private handleCountdown() {
    if (this.visualState.showCountdown) {
      this.updateCountdown();
      this.startCountdownTimer();
    } else {
      this.countdownEnded = true;
    }
  }

  private startCountdownTimer(): void {
    if (!this.countdownCheckInterval && this.visualState.showCountdown) {
      this.countdownCheckInterval = setInterval(() => {
        this.updateCountdown();
        if (!this.visualState.showCountdown) {
          clearInterval(this.countdownCheckInterval);
        }
      }, 1000);
    } else {
      this.updateCountdown(0);
    }
  }

  private updateCountdown(
    remainingTime: number = this.visualState.remainingTime,
  ): void {
    const days = Math.floor(remainingTime / (60 * 60 * 24));
    const hours = Math.floor((remainingTime % (60 * 60 * 24)) / (60 * 60));
    const minutes = Math.floor((remainingTime % (60 * 60)) / 60);
    const seconds = remainingTime % 60;

    this.countdownData = {
      days: String(days).padStart(2, '0'),
      hours: String(hours).padStart(2, '0'),
      minutes: String(minutes).padStart(2, '0'),
      seconds: String(seconds).padStart(2, '0'),
    };
  }
}
