import { Component } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { FunfairFeatureCardComponent } from '../funfair-feature-card/funfair-feature-card.component';

@Component({
  selector: 'lib-funfair-info-card',
  templateUrl: './funfair-info-card.component.html',
  styleUrls: ['./funfair-info-card.component.scss'],
  imports: [TranslocoDirective, FunfairFeatureCardComponent],
})
export class FunfairInfoCardComponent {}
