import { Component, EventEmitter, Output } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  IonGrid,
  IonSegment,
  IonSegmentButton,
} from '@ionic/angular/standalone';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'lib-funfair-category-filter',
  templateUrl: './funfair-category-filter.component.html',
  styleUrls: ['./funfair-category-filter.component.scss'],
  imports: [
    TranslocoDirective,
    IonGrid,
    IonSegment,
    FormsModule,
    IonSegmentButton,
  ],
})
export class FunfairCategoryFilterComponent {
  @Output() filterChanged = new EventEmitter<FunfairFilterType>();
  selectedFilter: FunfairFilterType = FunfairFilterType.distance;

  readonly filterType = FunfairFilterType;

  onFilterChange(value: any) {
    if (value) {
      const filterType = value as FunfairFilterType;
      this.selectedFilter = filterType;
      this.filterChanged.emit(filterType);
    }
  }
}

export enum FunfairFilterType {
  distance = 'distance',
  alphabetical = 'alphabetical',
}
