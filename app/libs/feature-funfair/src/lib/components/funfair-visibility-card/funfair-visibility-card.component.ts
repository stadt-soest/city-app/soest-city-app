import { Component, inject } from '@angular/core';
import { Router } from '@angular/router';
import { ActionButtonBarComponent } from '@sw-code/urbo-ui';
import { TranslocoDirective } from '@jsverse/transloco';
import { ModuleRegistryService } from '@sw-code/urbo-core';
import { FUNFAIR_MODULE_INFO } from '../../feature-funfair-module';
import { first } from 'rxjs';

@Component({
  selector: 'lib-funfair-visibility-card',
  templateUrl: './funfair-visibility-card.component.html',
  styleUrls: ['./funfair-visibility-card.component.scss'],
  imports: [TranslocoDirective, ActionButtonBarComponent],
})
export class FunfairVisibilityCardComponent {
  private readonly router = inject(Router);
  private readonly moduleRegistryService = inject(ModuleRegistryService);
  private readonly funfairModuleInfo = inject(FUNFAIR_MODULE_INFO);

  async toggleVisibility() {
    try {
      const visibility$ =
        this.moduleRegistryService.getModuleVisibilityWithUpdates(
          this.funfairModuleInfo,
        );
      visibility$.pipe(first()).subscribe(async (isVisible: boolean) => {
        const newVisibility = !isVisible;
        await this.moduleRegistryService.saveModuleVisibilitySettings(
          this.funfairModuleInfo,
          newVisibility,
        );
      });
    } catch (error) {
      console.error('Error toggling visibility:', error);
    }
  }

  async navigateToForyouSettings() {
    await this.router.navigate(['/for-you/settings/module-preferences']);
  }
}
