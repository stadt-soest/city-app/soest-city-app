import {
  Component,
  EventEmitter,
  inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {
  IonContent,
  IonHeader,
  IonInfiniteScroll,
  IonInfiniteScrollContent,
  IonItem,
  IonList,
  IonModal,
  IonNav,
  IonNote,
  IonSearchbar,
} from '@ionic/angular/standalone';
import { NgForOf, NgIf } from '@angular/common';
import { TranslocoDirective } from '@jsverse/transloco';
import { CustomModalHeaderComponent, UIModule } from '@sw-code/urbo-ui';
import { FeatureFunfairModule } from '../../feature-funfair-module';
import { FunfairPoiCardComponent } from '../funfair-base-filter/subcomponents/funfair-poi-card/funfair-poi-card.component';
import { FunfairPoi } from '../../models/poi.model';
import { DataService } from '../../services/geojson-data.service';
import { Feature, Geometry } from 'geojson';
import { Properties } from '../../models/feature.model';

@Component({
  imports: [
    IonNav,
    IonModal,
    IonContent,
    IonHeader,
    IonItem,
    IonList,
    IonNote,
    IonSearchbar,
    NgForOf,
    NgIf,
    TranslocoDirective,
    UIModule,
    FeatureFunfairModule,
    FunfairPoiCardComponent,
    CustomModalHeaderComponent,
    IonInfiniteScroll,
    IonInfiniteScrollContent,
  ],
  selector: 'lib-funfair-search',
  templateUrl: './funfair-search.component.html',
  styleUrls: ['./funfair-search.component.scss'],
})
export class FunfairSearchComponent implements OnInit {
  @ViewChild('infiniteScroll') infiniteScroll!: IonInfiniteScroll;
  @Output() closeClicked = new EventEmitter<void>();
  @Output() featureClicked = new EventEmitter<FunfairPoi>();

  allPois: FunfairPoi[] = [];
  searchResult?: FunfairPoi[];
  displayedPois: FunfairPoi[] = [];
  selectedPoi?: FunfairPoi;
  page = 1;
  pageSize = 10;

  private readonly dataService = inject(DataService);

  ngOnInit() {
    void this.loadAllPois();
  }

  onCloseClick() {
    this.closeClicked.emit();
    delete this.selectedPoi;
    delete this.searchResult;
  }

  async performSearch(event: any) {
    const searchTerm = event.target.value?.toLowerCase().trim();
    if (!searchTerm) {
      this.resetPagination();
      this.displayedPois = this.paginate(this.allPois);
      return;
    }
    const featuresFound =
      await this.dataService.searchFeaturesByName(searchTerm);
    this.searchResult = featuresFound.map((feature) => ({
      id: `${feature.id}`,
      name: feature.properties['name'],
      category: feature.properties['category'],
      feature,
    }));

    this.resetPagination();
    this.displayedPois = this.paginate(this.searchResult);
    this.infiniteScroll.disabled = false;
  }

  highlightPoi(feature: Feature<Geometry, Properties>) {
    this.selectedPoi = this.allPois?.find(
      (poi) => poi.id === feature?.properties['id'],
    );
    this.featureClicked.emit(this.selectedPoi);
  }

  loadData(event: any) {
    this.page++;
    const newItems = this.paginate(this.searchResult ?? this.allPois);
    this.displayedPois = [...this.displayedPois, ...newItems];

    event.target.complete();

    if (
      this.displayedPois.length >=
      (this.searchResult ? this.searchResult.length : this.allPois.length)
    ) {
      event.target.disabled = true;
    }
  }

  private async loadAllPois() {
    const allFeatures = await this.dataService.searchFeaturesByName('');
    this.allPois = allFeatures
      .map((feature) => ({
        id: feature.properties['id'],
        name: feature.properties['name'],
        category: feature.properties['category'],
        feature,
      }))
      .sort((a, b) => a.name.localeCompare(b.name));
    this.displayedPois = this.paginate(this.allPois);
  }

  private paginate(data: FunfairPoi[] | undefined): FunfairPoi[] {
    if (!data) {
      return [];
    }
    const start = (this.page - 1) * this.pageSize;
    const end = start + this.pageSize;
    return data.slice(start, end);
  }

  private resetPagination() {
    this.page = 1;
    this.displayedPois = [];
  }
}
