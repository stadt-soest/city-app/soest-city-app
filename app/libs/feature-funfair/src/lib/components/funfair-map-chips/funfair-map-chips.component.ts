import {
  Component,
  DestroyRef,
  EventEmitter,
  inject,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { RouterLink } from '@angular/router';
import { IonRouterLink } from '@ionic/angular/standalone';
import { TranslocoDirective } from '@jsverse/transloco';
import { NgClass } from '@angular/common';
import { GeneralIconComponent, IconFontSize } from '@sw-code/urbo-ui';
import { FunfairNavigationService } from '../../services/funfair-navigation.service';
import { FunfairCategoryService } from '../../services/funfair-category.service';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { PredefinedCategory } from '../../models/funfair-category.model';

@Component({
  selector: 'lib-funfair-filter-chip',
  templateUrl: 'funfair-map-chips.component.html',
  styleUrl: 'funfair-map-chips.component.scss',
  imports: [
    TranslocoDirective,
    NgClass,
    GeneralIconComponent,
    RouterLink,
    IonRouterLink,
  ],
})
export class FunfairMapChipsComponent implements OnInit {
  @Input() currentState!: FunfairMapChipsState;
  @Input() selectedCategoryName!: string;
  @Input() selectedCategoryNameKey!: string;
  @Output() openModal = new EventEmitter<void>();
  @Output() openSearch = new EventEmitter<void>();
  @Output() resetToAllCategories = new EventEmitter<void>();
  isCategoryDetail = false;
  protected readonly iconFontSize = IconFontSize;
  private readonly funfairNavigationService = inject(FunfairNavigationService);
  private readonly destroyRef = inject(DestroyRef);
  private readonly categoryService = inject(FunfairCategoryService);

  async ngOnInit() {
    this.funfairNavigationService.isOnCategoryDetailPage$
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((isOnCategoryDetailPage) => {
        this.isCategoryDetail = isOnCategoryDetailPage;
      });
  }

  openFilterModal(): void {
    this.openModal.emit();
  }

  openSearchModal(): void {
    this.openSearch.emit();
  }

  onResetToAllCategories(): void {
    this.categoryService.setCurrentCategory(PredefinedCategory.all);
    this.resetToAllCategories.emit();
  }
}

export type FunfairMapChipsState =
  | 'initial'
  | 'modalOpen'
  | 'allCategoriesSelected'
  | 'categorySelected';
