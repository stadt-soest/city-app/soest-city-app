import { Component, inject } from '@angular/core';
import { Router } from '@angular/router';
import { TranslocoDirective } from '@jsverse/transloco';
import { GeneralIconComponent, IconFontSize } from '@sw-code/urbo-ui';

@Component({
  selector: 'lib-funfair-map-button',
  template: `
    <button
      *transloco="let t; scope: 'feature_funfair'"
      class="mainmap-button"
      [attr.aria-label]="t('feature_funfair.map.chips.funfair_plan')"
      (click)="navigateToFunfairMap()"
    >
      <p>{{ t('feature_funfair.map.chips.funfair_plan') }}</p>
      <lib-general-icon
        name="keyboard_arrow_right"
        [size]="iconFontSize.large"
      ></lib-general-icon>
    </button>
  `,
  styleUrls: ['./funfair-map-button.component.scss'],
  imports: [TranslocoDirective, GeneralIconComponent],
})
export class FunfairMapButtonComponent {
  readonly iconFontSize = IconFontSize;
  private readonly router = inject(Router);

  navigateToFunfairMap() {
    this.router.navigate(['map/feature/funfair']);
  }
}
