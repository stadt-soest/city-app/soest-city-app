import { Feature, Geometry, MultiPolygon, Polygon } from 'geojson';
import { Properties } from '../../../../models/feature.model';
import { getDistance } from 'geolib';
import { centerOfMass } from '@turf/turf';
import { funfairDistanceThresholds } from '../../../../models/distance-threshold.model';
import { FunfairFilterType } from '../../../funfair-category-filter/funfair-category-filter.component';

export interface FunfairFeatureSection {
  titleKey: string;
  features: Feature<Geometry, Properties>[];
}

export const funfairDistanceUtils = {
  getFeatureCoordinates: (
    feature: Feature<Geometry, Properties>,
  ): [number, number] | null => {
    if (feature.geometry.type === 'Point') {
      return feature.geometry.coordinates as [number, number];
    } else if (
      feature.geometry.type === 'Polygon' ||
      feature.geometry.type === 'MultiPolygon'
    ) {
      const center = centerOfMass(feature as Feature<Polygon | MultiPolygon>);
      return center.geometry.coordinates as [number, number];
    } else {
      return null;
    }
  },

  calculateDistances(
    features: Feature<Geometry, Properties>[],
    currentLocation: { lat: number; long: number } | null,
  ): Feature<Geometry, Properties>[] {
    if (!currentLocation) {
      return features;
    }

    return features.map((feature) => {
      const coordinates = this.getFeatureCoordinates(feature);
      if (coordinates) {
        feature.properties['distance'] = getDistance(
          { latitude: currentLocation.lat, longitude: currentLocation.long },
          { latitude: coordinates[1], longitude: coordinates[0] },
        );
      }
      return feature;
    });
  },

  groupFeaturesIntoSections: (
    features: Feature<Geometry, Properties>[],
    highlightedFeatureId: string | null,
  ): FunfairFeatureSection[] => {
    const sections: FunfairFeatureSection[] = [];

    const highlightedFeatures = features.filter(
      (feature) =>
        feature.properties?.['id'].toString() === highlightedFeatureId,
    );

    if (highlightedFeatures.length > 0) {
      sections.push({
        titleKey: 'feature_funfair.sections.your_selection',
        features: highlightedFeatures,
      });
    }

    const featuresNear = features.filter(
      (feature) =>
        feature.properties['distance'] !== undefined &&
        feature.properties['distance'] <= funfairDistanceThresholds.near,
    );

    if (featuresNear.length > 0) {
      sections.push({
        titleKey: 'feature_funfair.sections.nearby',
        features: featuresNear,
      });
    }

    const featuresMedium = features.filter(
      (feature) =>
        feature.properties['distance'] !== undefined &&
        feature.properties['distance'] > funfairDistanceThresholds.near &&
        feature.properties['distance'] <= funfairDistanceThresholds.medium,
    );

    if (featuresMedium.length > 0) {
      sections.push({
        titleKey: 'feature_funfair.sections.medium_distance',
        features: featuresMedium,
      });
    }

    const featuresFar = features.filter(
      (feature) =>
        feature.properties['distance'] !== undefined &&
        feature.properties['distance'] > funfairDistanceThresholds.medium,
    );

    if (featuresFar.length > 0) {
      sections.push({
        titleKey: 'feature_funfair.sections.far_distance',
        features: featuresFar,
      });
    }

    return sections;
  },

  filterHighlightedFeature: (
    features: Feature<Geometry, Properties>[],
    highlightedFeatureId: string | null,
  ): {
    highlightedFeatures: Feature<Geometry, Properties>[];
    remainingFeatures: Feature<Geometry, Properties>[];
  } => {
    if (!highlightedFeatureId) {
      return { highlightedFeatures: [], remainingFeatures: features };
    }

    const highlightedFeatures = features.filter(
      (feature) =>
        feature.properties?.['id']?.toString() ===
        highlightedFeatureId.toString(),
    );

    const remainingFeatures = features.filter(
      (feature) =>
        feature.properties?.['id']?.toString() !==
        highlightedFeatureId.toString(),
    );

    return { highlightedFeatures, remainingFeatures };
  },

  createSectionsBasedOnFilter: (
    allFeatures: Feature<Geometry, Properties>[],
    highlightedFeatures: Feature<Geometry, Properties>[],
    currentLocation: { lat: number; long: number } | null,
    selectedFilter: FunfairFilterType,
  ): FunfairFeatureSection[] => {
    const sections: FunfairFeatureSection[] = [];

    if (highlightedFeatures.length > 0) {
      sections.push({
        titleKey: 'feature_funfair.sections.your_selection',
        features: highlightedFeatures,
      });
    }

    if (selectedFilter === FunfairFilterType.distance && currentLocation) {
      const allFeaturesWithDistance = funfairDistanceUtils.calculateDistances(
        allFeatures,
        currentLocation,
      );
      const groupedSections = funfairDistanceUtils.groupFeaturesIntoSections(
        allFeaturesWithDistance,
        null,
      );
      sections.push(...groupedSections);
    } else if (selectedFilter === FunfairFilterType.alphabetical) {
      const alphabeticalFeatures = [...allFeatures].sort((a, b) =>
        (a.properties?.['name'] || '').localeCompare(
          b.properties?.['name'] || '',
        ),
      );
      sections.push({
        titleKey: '',
        features: alphabeticalFeatures,
      });
    }

    return sections;
  },

  resetFeatureDistances: (
    features: Feature<Geometry, Properties>[],
  ): Feature<Geometry, Properties>[] =>
    features.map((feature) => {
      if (feature.properties && feature.properties['distance'] !== undefined) {
        feature.properties['distance'] = undefined;
      }
      return feature;
    }),
};
