import {
  Component,
  DestroyRef,
  inject,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { BehaviorSubject, combineLatest, of, Subject, takeUntil } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';
import { Feature, Geometry } from 'geojson';
import {
  IonContent,
  IonHeader,
  IonNav,
  IonSkeletonText,
  ModalController,
} from '@ionic/angular/standalone';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  CustomModalHeaderComponent,
  NotificationCardComponent,
} from '@sw-code/urbo-ui';
import { AsyncPipe, NgFor, NgIf } from '@angular/common';
import {
  FunfairCategoryFilterComponent,
  FunfairFilterType,
} from '../../../funfair-category-filter/funfair-category-filter.component';
import { FunfairPoiCardComponent } from '../funfair-poi-card/funfair-poi-card.component';
import {
  funfairDistanceUtils,
  FunfairFeatureSection,
} from './funfair-distance.utils';
import {
  DeviceService,
  GeolocationService,
  SimplifiedPermissionState,
} from '@sw-code/urbo-core';
import { FunfairBookmarkService } from '../../../../services/funfair-bookmark.service';
import { FunfairCategoryService } from '../../../../services/funfair-category.service';
import { DataService } from '../../../../services/geojson-data.service';
import { FunfairNavigationService } from '../../../../services/funfair-navigation.service';
import { ModalDismissalService } from '../../../../services/funfair-modal-dismissal.service';
import { PredefinedCategory } from '../../../../models/funfair-category.model';
import { FunfairFilterComponent } from '../funfair-filter/funfair-filter.component';
import { Properties } from '../../../../models/feature.model';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'lib-funfair-category-detail-list',
  templateUrl: 'funfair-category-detail-list.component.html',
  styleUrls: ['funfair-category-detail-list.component.scss'],
  imports: [
    TranslocoDirective,
    IonHeader,
    CustomModalHeaderComponent,
    IonContent,
    NgIf,
    IonSkeletonText,
    NgFor,
    NotificationCardComponent,
    FunfairCategoryFilterComponent,
    FunfairPoiCardComponent,
    AsyncPipe,
  ],
})
export class FunfairCategoryDetailListComponent implements OnInit, OnDestroy {
  @Input() categoryName = '';
  @Input() categoryNameKey = '';
  @Input() clickedFeatureId: string | null = null;
  @Input() isFromBookmark = false;

  selectedFilter$ = new BehaviorSubject<FunfairFilterType>(
    FunfairFilterType.alphabetical,
  );
  loading$ = new BehaviorSubject<boolean>(true);
  featureSections$ = new BehaviorSubject<FunfairFeatureSection[]>([]);
  isGeolocationPermitted = false;
  protected geolocationService = inject(GeolocationService);
  permissionButton = [
    {
      icon: 'open_in_new',
      text: 'feature_funfair.sections.open',
      selected: false,
      applySelectionStyle: false,
      onSelect: async () => {
        await this.geolocationService.requestPermissions();
      },
    },
  ];
  protected readonly simplifiedPermissionState = SimplifiedPermissionState;
  protected deviceService = inject(DeviceService);
  private hasSetInitialFilter = false;
  private readonly funfairBookmarkService = inject(FunfairBookmarkService);
  private readonly ionNav = inject(IonNav);
  private readonly categoryService = inject(FunfairCategoryService);
  private readonly dataService = inject(DataService);
  private readonly funfairNavigationService = inject(FunfairNavigationService);
  private readonly modalController = inject(ModalController);
  private readonly destroyRef = inject(DestroyRef);
  private readonly modalDismissalService = inject(ModalDismissalService);
  private readonly destroy$ = new Subject<void>();

  ngOnInit() {
    this.watchPermissionAndSetFilter();
    this.initializeDataStream();
    this.setupModalDismissalSubscription();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  async close() {
    const dismissData = this.getDismissalData();
    await this.modalController.dismiss(dismissData);
  }

  async pop() {
    this.destroy$.next();
    this.destroy$.complete();
    this.categoryService.setCurrentCategory(PredefinedCategory.all);
    this.funfairNavigationService.setIsOnCategoryDetailPage(false);
    await ((await this.ionNav.canGoBack())
      ? this.ionNav.pop()
      : this.ionNav.push(FunfairFilterComponent, {}, { animated: true }));
  }

  trackById(_index: number, feature: Feature<Geometry, Properties>) {
    return feature.properties?.['id'];
  }

  onFilterChanged(filterType: FunfairFilterType) {
    this.selectedFilter$.next(filterType);
  }

  onPoiDirectionsClicked(feature: Feature<Geometry, Properties>) {
    this.clickedFeatureId = feature.properties['id'];
    this.funfairNavigationService.setHighlightFeature(
      this.getEffectiveCategory(),
      feature.properties['id'],
    );
  }

  protected isHighlighted(feature: Feature<Geometry, Properties>): boolean {
    return feature.properties?.['id'].toString() === this.clickedFeatureId;
  }

  private initializeDataStream() {
    const dataStream$ = this.isFromBookmark
      ? this.funfairBookmarkService.getBookmarkedPOIs$()
      : this.dataService.getFeaturesByCategoryName$(this.categoryName);

    combineLatest([this.selectedFilter$, dataStream$])
      .pipe(
        switchMap(([filterType, features]) => {
          if (
            filterType === FunfairFilterType.distance &&
            this.isGeolocationPermitted
          ) {
            return this.geolocationService.currentLocation$.pipe(
              switchMap((location) => {
                const featuresWithDistance =
                  funfairDistanceUtils.calculateDistances(features, location);
                this.sectionFeatures(
                  featuresWithDistance,
                  filterType,
                  location,
                );
                return of(null);
              }),
            );
          } else {
            this.sectionFeatures(features, filterType, null);
            return of(null);
          }
        }),
        catchError((_error) => {
          this.loading$.next(false);
          return of(null);
        }),
        tap(() => {
          this.loading$.next(false);
        }),
        takeUntilDestroyed(this.destroyRef),
      )
      .subscribe();
  }

  private sectionFeatures(
    features: Feature<Geometry, Properties>[],
    filterType: FunfairFilterType,
    currentLocation: {
      lat: number;
      long: number;
    } | null,
  ) {
    const { highlightedFeatures, remainingFeatures } =
      funfairDistanceUtils.filterHighlightedFeature(
        features,
        this.clickedFeatureId,
      );
    const sections = funfairDistanceUtils.createSectionsBasedOnFilter(
      remainingFeatures,
      highlightedFeatures,
      currentLocation,
      filterType,
    );
    this.featureSections$.next(sections);
  }

  private watchPermissionAndSetFilter() {
    this.geolocationService.permissionState$
      .pipe(
        switchMap((permission) => {
          if (permission === 'granted') {
            return this.geolocationService.currentLocation$;
          }
          return of(null);
        }),
        tap((location) => {
          this.isGeolocationPermitted = !!location;
          if (this.isGeolocationPermitted && !this.hasSetInitialFilter) {
            this.selectedFilter$.next(FunfairFilterType.distance);
            this.hasSetInitialFilter = true;
          }
        }),
        takeUntilDestroyed(this.destroyRef),
      )
      .subscribe();
  }

  private setupModalDismissalSubscription() {
    this.modalDismissalService.dismissEvent$
      .pipe(takeUntil(this.destroy$))
      .subscribe(async () => {
        const dismissData = await this.handleDismissal();
        this.modalDismissalService.triggerDismissWithData(dismissData);
      });
  }

  private getDismissalData() {
    return {
      selectedCategory: this.categoryName,
      categoryName: this.getEffectiveCategory(),
      categoryNameKey: this.categoryNameKey,
    };
  }

  private async handleDismissal() {
    this.funfairNavigationService.setIsOnCategoryDetailPage(false);
    this.categoryService.setCurrentCategory(this.getEffectiveCategory());
    return this.getDismissalData();
  }

  private getEffectiveCategory(): string {
    return this.isFromBookmark
      ? PredefinedCategory.favorites
      : this.categoryName;
  }
}
