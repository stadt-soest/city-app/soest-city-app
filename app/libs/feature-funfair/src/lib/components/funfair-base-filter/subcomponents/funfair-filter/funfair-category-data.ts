import { Category, CategoryGroup } from '../../../../models/category-groups';
import { CustomButtonColorScheme } from '@sw-code/urbo-ui';

export const categoryGroups: CategoryGroup[] = [
  new CategoryGroup(
    'Attraktionen',
    [
      new Category(
        'Fahrgeschäfte',
        'urbo_attractions',
        'red',
        'feature_funfair.map.category_list.categories.rides',
        ['Fahrgeschäft'],
        CustomButtonColorScheme.strong,
      ),
      new Category(
        'Laufgeschäft',
        'urbo_directions_run',
        'red',
        'feature_funfair.map.category_list.categories.running_store',
        ['Laufgeschäft'],
        CustomButtonColorScheme.strong,
      ),
    ],
    'feature_funfair.map.category_list.groups.attractions',
  ),
  new CategoryGroup(
    'Speisen und Getränke',
    [
      new Category(
        'Imbiss',
        'urbo_restaurant',
        'orange',
        'feature_funfair.map.category_list.categories.snack',
        ['Imbiss'],
        CustomButtonColorScheme.strong,
      ),
      new Category(
        'Süßwaren',
        'urbo_icecream',
        'orange',
        'feature_funfair.map.category_list.categories.confectionery',
        ['Süßwaren'],
        CustomButtonColorScheme.strong,
      ),
      new Category(
        'Ausschank',
        'urbo_liquor',
        'orange',
        'feature_funfair.map.category_list.categories.bar',
        ['Ausschank'],
        CustomButtonColorScheme.strong,
      ),
      new Category(
        'Schankzelt',
        'urbo_festival',
        'orange',
        'feature_funfair.map.category_list.categories.tent',
        ['Schankzelt'],
        CustomButtonColorScheme.strong,
      ),
    ],
    'feature_funfair.map.category_list.groups.food_and_drinks',
  ),
  new CategoryGroup(
    'Kirmes Stände',
    [
      new Category(
        'Verkaufsstände',
        'urbo_storefront',
        'green',
        'feature_funfair.map.category_list.categories.sales_stand',
        ['Verkaufsstand'],
        CustomButtonColorScheme.strong,
      ),
      new Category(
        'Spielbuden',
        'urbo_trophy',
        'green',
        'feature_funfair.map.category_list.categories.playout',
        ['Ausspielung'],
        CustomButtonColorScheme.strong,
      ),
      new Category(
        'Spezialisten',
        'urbo_star',
        'green',
        'feature_funfair.map.category_list.categories.specialists',
        ['Spezialisten'],
        CustomButtonColorScheme.strong,
      ),
    ],
    'feature_funfair.map.category_list.groups.funfair_stalls',
  ),
  new CategoryGroup(
    'Versorgung',
    [
      new Category(
        'WC',
        'urbo_wc',
        'main',
        'feature_funfair.map.category_list.categories.toilet',
        ['WC'],
        CustomButtonColorScheme.info,
      ),
      new Category(
        'Erste Hilfe',
        'urbo_medical_services',
        'main',
        'feature_funfair.map.category_list.categories.first_aid',
        ['Erste Hilfe'],
        CustomButtonColorScheme.info,
      ),
      new Category(
        'Infostand Stadt Soest',
        'urbo_info',
        'main',
        'feature_funfair.map.category_list.categories.city_of_soest_information',
        ['Infostand Stadt Soest'],
        CustomButtonColorScheme.info,
      ),
    ],
    'feature_funfair.map.category_list.groups.supply',
  ),
];
