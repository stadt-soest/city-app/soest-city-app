import {
  Component,
  EventEmitter,
  inject,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { Feature, Geometry, MultiPolygon, Point, Polygon } from 'geojson';
import { Properties } from '../../../../models/feature.model';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import { NgClass, NgIf } from '@angular/common';
import {
  ActionButtonBarComponent,
  DistanceFormatPipe,
  GeneralIconComponent,
  IconType,
} from '@sw-code/urbo-ui';
import { Router } from '@angular/router';
import { FunfairBookmarkService } from '../../../../services/funfair-bookmark.service';
import { BaseShareService, MapsNavigationService } from '@sw-code/urbo-core';
import { FunfairCategoryService } from '../../../../services/funfair-category.service';
import { funfairDistanceThresholds } from '../../../../models/distance-threshold.model';
import { PredefinedCategory } from '../../../../models/funfair-category.model';
import { firstValueFrom } from 'rxjs';
import { centerOfMass } from '@turf/turf';

@Component({
  selector: 'lib-funfair-poi-card',
  templateUrl: './funfair-poi-card.component.html',
  styleUrls: ['./funfair-poi-card.component.scss'],
  imports: [
    TranslocoDirective,
    NgClass,
    ActionButtonBarComponent,
    NgIf,
    GeneralIconComponent,
    DistanceFormatPipe,
  ],
})
export class FunfairPoiCardComponent implements OnInit {
  @Input({ required: true }) feature!: Feature<Geometry, Properties>;
  @Input() fallBackTitle = '';
  @Input() highlighted = false;
  @Input() distance?: number;
  @Input() isFromBookmark = false;
  @Output() showDirectionsEvent = new EventEmitter<
    Feature<Geometry, Properties>
  >();
  isBookmarked = false;
  protected readonly iconType = IconType;
  private readonly router = inject(Router);
  private readonly funfairBookmarkService = inject(FunfairBookmarkService);
  private readonly shareService = inject(BaseShareService);
  private readonly translocoService = inject(TranslocoService);
  private readonly mapsNavigationService = inject(MapsNavigationService);
  private readonly categoryService = inject(FunfairCategoryService);

  async ngOnInit() {
    this.isBookmarked = await this.funfairBookmarkService.isPOIBookmarked(
      this.feature.properties['id']?.toString(),
    );
  }

  getDistanceIcon(distance: number): string {
    if (distance <= funfairDistanceThresholds.near) {
      return 'stat_1';
    }
    if (distance < funfairDistanceThresholds.medium) {
      return 'stat_2';
    }
    return 'stat_3';
  }

  async toggleBookmark() {
    const id = this.feature.properties['id'].toString();

    if (this.isBookmarked) {
      await this.funfairBookmarkService.removeBookmark(id);
    } else {
      await this.funfairBookmarkService.addBookmark(this.feature);
    }

    if (this.isFromBookmark) {
      this.categoryService.setCurrentCategory(PredefinedCategory.favorites);
    }
    this.isBookmarked = !this.isBookmarked;
  }

  async showDirections() {
    const id = this.feature.properties['id'];

    this.showDirectionsEvent.emit(this.feature);

    await this.router.navigate([], {
      queryParams: { id },
    });
  }

  async shareFeature() {
    const category = this.feature.properties['category'];
    const id = this.feature.properties['id'];
    const shareUrl = `${window.location.origin}/map/feature/funfair?category=${category}&id=${id}`;
    const title = await firstValueFrom(
      this.translocoService.selectTranslate('feature_funfair.share.title'),
    );
    const text = await firstValueFrom(
      this.translocoService.selectTranslate('feature_funfair.share.text', {
        url: shareUrl,
      }),
    );

    this.shareService
      .share(title, text, shareUrl)
      .catch((error) => console.error('Error sharing:', error));
  }

  async navigateToLocation(): Promise<void> {
    try {
      const feature: Feature = this.feature;

      if (
        feature.geometry.type === 'Polygon' ||
        feature.geometry.type === 'MultiPolygon'
      ) {
        const polygonFeature = feature as Feature<Polygon | MultiPolygon>;

        const center = centerOfMass(polygonFeature);

        const [longitude, latitude] = center.geometry.coordinates;

        await this.mapsNavigationService.navigateToLocation(
          latitude,
          longitude,
        );
      } else if (feature.geometry.type === 'Point') {
        const pointFeature = feature as Feature<Point>;
        const [longitude, latitude] = pointFeature.geometry.coordinates;
        await this.mapsNavigationService.navigateToLocation(
          latitude,
          longitude,
        );
      }
    } catch (error) {
      console.error('Failed to navigate to location:', error);
    }
  }
}
