import { render, screen } from '@testing-library/angular';
import '@testing-library/jest-dom';
import { FunfairPoiCardComponent } from './funfair-poi-card.component';
import { Feature, Geometry } from 'geojson';
import { Properties } from '../../../../models/feature.model';
import { of } from 'rxjs';
import { ActionButtonBarComponent, UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../../../transloco-testing.module';
import { FunfairMapService } from '../../../../services/funfair-map.service';
import { FunfairBookmarkService } from '../../../../services/funfair-bookmark.service';
import { BaseShareService, MapsNavigationService } from '@sw-code/urbo-core';

describe('FunfairPoiCardComponent', () => {
  const mockFeature: Feature<Geometry, Properties> = {
    type: 'Feature',
    id: 1,
    properties: {
      name: 'Test Title',
    },
    geometry: {
      type: 'Point',
      coordinates: [10, 20],
    },
  };

  const mockBookmarkService = {
    isPOIBookmarked: jest.fn().mockReturnValue(of(false)),
    addBookmark: jest.fn(),
    removeBookmark: jest.fn(),
  };

  const mockMapService = {
    resetMapPadding: jest.fn(),
  };

  const mockMapsNavigationService = {
    navigateToLocation: jest.fn().mockResolvedValue(true),
  };

  const mockShareService = {
    share: jest.fn().mockResolvedValue(true),
  };

  it('should render the component and display the feature title', async () => {
    await render(FunfairPoiCardComponent, {
      imports: [
        ActionButtonBarComponent,
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      componentProviders: [
        { provide: FunfairMapService, useValue: mockMapService },
        { provide: FunfairBookmarkService, useValue: mockBookmarkService },
        { provide: BaseShareService, useValue: mockShareService },
        { provide: MapsNavigationService, useValue: mockMapsNavigationService },
      ],
      inputs: { feature: mockFeature },
    });

    const titleElement = screen.getByTestId('poi-title');
    expect(titleElement.textContent?.trim()).toBe('Test Title');
  });

  it('should render the action button bar', async () => {
    await render(FunfairPoiCardComponent, {
      imports: [
        ActionButtonBarComponent,
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      componentProviders: [
        { provide: FunfairMapService, useValue: mockMapService },
        { provide: FunfairBookmarkService, useValue: mockBookmarkService },
        { provide: BaseShareService, useValue: mockShareService },
        { provide: MapsNavigationService, useValue: mockMapsNavigationService },
      ],
      inputs: { feature: mockFeature },
    });

    const actionButtonBarElement = screen.getByTestId('action-button-bar');
    expect(actionButtonBarElement).not.toBeNull();
  });
});
