import {
  Component,
  DestroyRef,
  EventEmitter,
  inject,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { IonList } from '@ionic/angular/standalone';
import { NgFor, NgIf } from '@angular/common';
import {
  CustomButtonColorScheme,
  CustomButtonType,
  CustomHorizontalButtonComponent,
  ThemeService,
} from '@sw-code/urbo-ui';
import { Category, CategoryGroup } from '../../../../models/category-groups';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'lib-funfair-category-list',
  templateUrl: './funfair-category-list.component.html',
  styleUrls: ['./funfair-category-list.component.scss'],
  imports: [
    TranslocoDirective,
    IonList,
    NgFor,
    CustomHorizontalButtonComponent,
    NgIf,
  ],
})
export class FunfairCategoryListComponent implements OnInit {
  @Input() categoryGroups: CategoryGroup[] = [];
  @Output() allCategoriesSelected = new EventEmitter<void>();
  @Output() categorySelected = new EventEmitter<Category>();

  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;

  private isDarkMode = false;
  private readonly themeService = inject(ThemeService);
  private readonly destroyRef = inject(DestroyRef);

  ngOnInit() {
    this.subscribeToDarkModeChanges();
  }

  selectCategory(category: Category) {
    this.categorySelected.emit(category);
  }

  selectAllCategories() {
    this.allCategoriesSelected.emit();
  }

  trackByGroupId(_index: number, group: CategoryGroup): string {
    return group.title;
  }

  trackByCategoryId(_index: number, category: Category): string {
    return category.name;
  }

  getGroupImage(index: number): string {
    const theme = this.isDarkMode ? 'dark' : 'light';
    const images = [
      `/assets/feature-funfair/images/${theme}/food.svg`,
      `/assets/feature-funfair/images/${theme}/games.svg`,
      `/assets/feature-funfair/images/${theme}/flag-down.svg`,
    ];

    return images[index];
  }

  getGroupSecondImage(index: number): string | null {
    const theme = this.isDarkMode ? 'dark' : 'light';

    if (index === 0) {
      return `/assets/feature-funfair/images/${theme}/flag-down.svg`;
    } else if (index === 1) {
      return `/assets/feature-funfair/images/${theme}/flag-up.svg`;
    } else {
      return null;
    }
  }

  private subscribeToDarkModeChanges(): void {
    this.isDarkMode = this.themeService.isDarkMode();

    this.themeService.isDarkMode$
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((isDark) => {
        this.isDarkMode = isDark;
      });
  }
}
