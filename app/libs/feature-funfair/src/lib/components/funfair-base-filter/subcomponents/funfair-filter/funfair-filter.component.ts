import {
  Component,
  DestroyRef,
  inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import {
  IonContent,
  IonHeader,
  IonNav,
  ModalController,
  ViewDidLeave,
} from '@ionic/angular/standalone';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  CustomButtonColorScheme,
  CustomButtonType,
  CustomModalHeaderComponent,
  ThemeService,
} from '@sw-code/urbo-ui';
import { NgClass, NgIf } from '@angular/common';
import { FunfairFeatureCardComponent } from '../../../funfair-feature-card/funfair-feature-card.component';
import { FunfairCategoryListComponent } from '../funfair-category-list/funfair-category-list.component';
import { FunfairCategoryDetailListComponent } from '../funfair-category-detail-list/funfair-category-detail-list.component';
import { Category, CategoryGroup } from '../../../../models/category-groups';
import { Feature, Geometry } from 'geojson';
import { Properties } from '../../../../models/feature.model';
import { FunfairBookmarkService } from '../../../../services/funfair-bookmark.service';
import { categoryGroups } from './funfair-category-data';
import { DataService } from '../../../../services/geojson-data.service';
import { FunfairCategoryService } from '../../../../services/funfair-category.service';
import { FunfairVisualStateService } from '../../../../services/funfair-visual-state.service';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { PredefinedCategory } from '../../../../models/funfair-category.model';

@Component({
  selector: 'lib-funfair-filter',
  templateUrl: './funfair-filter.component.html',
  styleUrl: 'funfair-filter.component.scss',
  imports: [
    TranslocoDirective,
    IonHeader,
    CustomModalHeaderComponent,
    IonContent,
    NgClass,
    NgIf,
    FunfairFeatureCardComponent,
    FunfairCategoryListComponent,
  ],
})
export class FunfairFilterComponent implements OnInit, ViewDidLeave, OnDestroy {
  nextPage = FunfairCategoryDetailListComponent;
  categoryGroups: CategoryGroup[] = categoryGroups;
  savedFavorites: Feature<Geometry, Properties>[] = [];
  favoritesSubtitle = 0;
  favoritesContent = '';
  hasFavorites = false;
  confettiClass: ConfettiClass = '';

  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;

  private isDarkMode = false;

  private readonly modalController = inject(ModalController);
  private readonly nav = inject(IonNav);
  private readonly funfairBookmarkService = inject(FunfairBookmarkService);
  private readonly dataService = inject(DataService);
  private readonly categoryService = inject(FunfairCategoryService);
  private readonly themeService = inject(ThemeService);
  private readonly destroyRef = inject(DestroyRef);
  private readonly visualState = inject(FunfairVisualStateService);

  private get confettiMode(): ConfettiClass {
    if (this.visualState.showCountdown) {
      return '';
    } else if (this.visualState.showConfettiAnimation) {
      return 'animated';
    } else {
      return 'static';
    }
  }

  ngOnInit(): void {
    this.subscribeToFavorites();
    this.subscribeToDarkModeChanges();
    this.setConfettiClass();
  }

  ngOnDestroy(): void {
    this.visualState.disableConfettiAnimation();
    this.setConfettiClass();
  }

  ionViewDidLeave(): void {
    this.visualState.disableConfettiAnimation();
    this.setConfettiClass();
  }

  subscribeToFavorites() {
    this.funfairBookmarkService
      .getBookmarkedPOIs$()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((bookmarks) => {
        this.savedFavorites = bookmarks;
        this.hasFavorites = this.savedFavorites.length > 0;
        this.favoritesSubtitle = this.savedFavorites.length;
        this.favoritesContent = this.savedFavorites
          .map((feature) => feature.properties?.['name'])
          .join(', ');
      });
  }

  getImagePath(imageFileName: string): string {
    const theme = this.isDarkMode ? 'dark' : 'light';
    return `/assets/feature-funfair/images/${theme}/${imageFileName}`;
  }

  async selectCategory(category: Category) {
    try {
      const features = await this.dataService.getFeaturesByCategory(category);
      this.categoryService.setCurrentCategory(category.name);
      await this.nav.push(FunfairCategoryDetailListComponent, {
        features,
        categoryName: category.name,
        categoryNameKey: category.nameKey,
      });
    } catch (error) {
      console.error('Error fetching features for category:', error);
    }
  }

  async selectFavorites() {
    this.categoryService.setCurrentCategory(PredefinedCategory.favorites);
    if (this.savedFavorites.length === 0) {
      return;
    }

    await this.nav.push(
      this.nextPage,
      {
        features: this.savedFavorites,
        isFromBookmark: true,
        categoryNameKey:
          'feature_funfair.map.category_list.categories.favorites',
        categoryName: PredefinedCategory.favorites,
      },
      { animated: true },
    );
  }

  async close() {
    this.categoryService.setCurrentCategory(PredefinedCategory.all);
    await this.modalController.dismiss({
      selectedCategory: PredefinedCategory.all,
      selectedCategoryName: PredefinedCategory.all,
    });
  }

  trackByGroupId(_index: number, group: CategoryGroup): string {
    return group.title;
  }

  private setConfettiClass() {
    this.confettiClass = this.confettiMode;
  }

  private subscribeToDarkModeChanges(): void {
    this.isDarkMode = this.themeService.isDarkMode();

    this.themeService.isDarkMode$
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((isDark) => {
        this.isDarkMode = isDark;
      });
  }
}

type ConfettiClass = '' | 'animated' | 'static';
