import { render } from '@testing-library/angular';
import '@testing-library/jest-dom';
import { FunfairCategoryListComponent } from './funfair-category-list.component';
import { BehaviorSubject } from 'rxjs';
import {
  CustomButtonColorScheme,
  CustomHorizontalButtonComponent,
  ThemeService,
} from '@sw-code/urbo-ui';
import { Category, CategoryGroup } from '../../../../models/category-groups';
import { getTranslocoModule } from '../../../../../transloco-testing.module';

describe('FunfairCategoryListComponent', () => {
  let mockThemeService: Partial<ThemeService>;

  const mockCategoryGroups: CategoryGroup[] = [
    new CategoryGroup(
      'Group 1',
      [
        new Category(
          'Category 1',
          'icon1',
          'orange',
          '',
          [],
          CustomButtonColorScheme.strong,
        ),
        new Category(
          'Category 2',
          'icon2',
          'green',
          '',
          [],
          CustomButtonColorScheme.strong,
        ),
      ],
      '',
    ),
    new CategoryGroup(
      'Group 2',
      [
        new Category(
          'Category 3',
          'icon3',
          'red',
          '',
          [],
          CustomButtonColorScheme.strong,
        ),
      ],
      '',
    ),
  ];

  beforeEach(() => {
    mockThemeService = {
      isDarkMode$: new BehaviorSubject(false),
      isDarkMode: jest.fn().mockReturnValue(false),
    };
  });

  it('should correctly identify groups and categories with trackBy functions', async () => {
    const { fixture } = await render(FunfairCategoryListComponent, {
      inputs: { categoryGroups: mockCategoryGroups },
      imports: [CustomHorizontalButtonComponent, getTranslocoModule()],
      providers: [ThemeService],
    });

    const componentInstance = fixture.componentInstance;

    expect(componentInstance.trackByGroupId(0, mockCategoryGroups[0])).toBe(
      'Group 1',
    );
    expect(componentInstance.trackByGroupId(1, mockCategoryGroups[1])).toBe(
      'Group 2',
    );

    expect(
      componentInstance.trackByCategoryId(
        0,
        mockCategoryGroups[0].categories[0],
      ),
    ).toBe('Category 1');
    expect(
      componentInstance.trackByCategoryId(
        1,
        mockCategoryGroups[0].categories[1],
      ),
    ).toBe('Category 2');
  });

  it('should correctly return group image paths based on the theme', async () => {
    const { fixture } = await render(FunfairCategoryListComponent, {
      inputs: { categoryGroups: mockCategoryGroups },
      imports: [getTranslocoModule()],
      providers: [{ provide: ThemeService, useValue: mockThemeService }],
    });

    const componentInstance = fixture.componentInstance;

    expect(componentInstance.getGroupImage(0)).toBe(
      '/assets/feature-funfair/images/light/food.svg',
    );
    expect(componentInstance.getGroupImage(1)).toBe(
      '/assets/feature-funfair/images/light/games.svg',
    );
    expect(componentInstance.getGroupImage(2)).toBe(
      '/assets/feature-funfair/images/light/flag-down.svg',
    );

    (mockThemeService.isDarkMode$ as BehaviorSubject<boolean>).next(true);
    fixture.detectChanges();

    expect(componentInstance.getGroupImage(0)).toBe(
      '/assets/feature-funfair/images/dark/food.svg',
    );
    expect(componentInstance.getGroupImage(1)).toBe(
      '/assets/feature-funfair/images/dark/games.svg',
    );
    expect(componentInstance.getGroupImage(2)).toBe(
      '/assets/feature-funfair/images/dark/flag-down.svg',
    );
  });

  it('should correctly return second group image paths based on the group index', async () => {
    const { fixture } = await render(FunfairCategoryListComponent, {
      inputs: { categoryGroups: mockCategoryGroups },
      imports: [getTranslocoModule()],
      providers: [{ provide: ThemeService, useValue: mockThemeService }],
    });

    const componentInstance = fixture.componentInstance;

    expect(componentInstance.getGroupSecondImage(0)).toBe(
      '/assets/feature-funfair/images/light/flag-down.svg',
    );
    expect(componentInstance.getGroupSecondImage(1)).toBe(
      '/assets/feature-funfair/images/light/flag-up.svg',
    );
    expect(componentInstance.getGroupSecondImage(2)).toBeNull();

    (mockThemeService.isDarkMode$ as BehaviorSubject<boolean>).next(true);
    fixture.detectChanges();

    expect(componentInstance.getGroupSecondImage(0)).toBe(
      '/assets/feature-funfair/images/dark/flag-down.svg',
    );
    expect(componentInstance.getGroupSecondImage(1)).toBe(
      '/assets/feature-funfair/images/dark/flag-up.svg',
    );
    expect(componentInstance.getGroupSecondImage(2)).toBeNull();
  });
});
