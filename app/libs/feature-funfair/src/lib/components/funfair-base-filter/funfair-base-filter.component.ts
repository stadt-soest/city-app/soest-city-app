import {
  Component,
  EventEmitter,
  inject,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { IonModal, IonNav } from '@ionic/angular/standalone';
import { ModalDismissalService } from '../../services/funfair-modal-dismissal.service';

@Component({
  imports: [IonNav, IonModal],
  selector: 'lib-funfair-base-filter',
  templateUrl: './funfair-base-filter.component.html',
})
export class FunfairBaseFilterComponent {
  @Input() isModalOpen = false;
  @Input() rootPage: any;
  @Input() rootParams: any;
  @Output() modalWillDismiss = new EventEmitter<void>();
  @ViewChild('nav') private readonly nav!: IonNav;

  private readonly modalDismissalService = inject(ModalDismissalService);

  navigateToPage(page: any, params: any) {
    if (this.nav) {
      this.nav
        .setRoot(page, params)
        .catch((error) =>
          console.error('Error resetting navigation stack:', error),
        );
    }
  }

  onWillDismiss(event: any) {
    this.modalWillDismiss.emit(event.detail.data);
    this.modalDismissalService.triggerDismissEvent();
    this.isModalOpen = false;
  }
}
