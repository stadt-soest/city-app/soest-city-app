import { Component, inject } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { RouterLink } from '@angular/router';
import { IonRouterLink } from '@ionic/angular/standalone';
import {
  GeneralIconComponent,
  IconColor,
  IconFontSize,
} from '@sw-code/urbo-ui';
import { FUNFAIR_CONFIG, FunfairConfig } from '../../funfair-tokens';

@Component({
  selector: 'lib-funfair-for-you-map-card',
  templateUrl: './funfair-for-you-map-card.component.html',
  styleUrls: ['./funfair-for-you-map-card.component.scss'],
  imports: [
    TranslocoDirective,
    RouterLink,
    IonRouterLink,
    GeneralIconComponent,
  ],
})
export class FunfairForYouMapCardComponent {
  protected readonly iconFontSize = IconFontSize;
  protected readonly iconColor = IconColor;
  protected funfairConfig = inject<FunfairConfig>(FUNFAIR_CONFIG);
}
