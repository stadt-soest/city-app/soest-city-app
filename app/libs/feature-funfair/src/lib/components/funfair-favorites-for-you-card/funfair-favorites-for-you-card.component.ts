import { Component, inject, OnDestroy, OnInit } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { FunfairFeatureCardComponent } from '../funfair-feature-card/funfair-feature-card.component';
import { Properties } from '../../models/feature.model';
import { Feature, Geometry } from 'geojson';
import { FunfairBookmarkService } from '../../services/funfair-bookmark.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { PredefinedCategory } from '../../models/funfair-category.model';

@Component({
  selector: 'lib-funfair-favorites-for-you-card',
  templateUrl: './funfair-favorites-for-you-card.component.html',
  styleUrls: ['./funfair-favorites-for-you-card.component.scss'],
  imports: [TranslocoDirective, FunfairFeatureCardComponent],
})
export class FunfairFavoritesForYouCardComponent implements OnInit, OnDestroy {
  savedFavorites: Feature<Geometry, Properties>[] = [];
  favoritesSubtitle = 0;
  favoritesContent = '';
  hasFavorites = false;
  private readonly funfairBookmarkService = inject(FunfairBookmarkService);
  private readonly router = inject(Router);
  private readonly destroy$ = new Subject<void>();

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  ngOnInit(): void {
    this.subscribeToFavorites();
  }

  navigateToMap() {
    this.router.navigate(['/map/feature/funfair'], {
      queryParams: {
        category: PredefinedCategory.favorites,
      },
    });
  }

  private subscribeToFavorites() {
    this.funfairBookmarkService.getBookmarkedPOIs$().subscribe((bookmarks) => {
      this.savedFavorites = bookmarks;
      this.hasFavorites = this.savedFavorites.length > 0;
      this.favoritesSubtitle = this.savedFavorites.length;
      this.favoritesContent = this.savedFavorites
        .map((feature) => feature.properties?.['name'] || 'Unknown')
        .join(', ');
    });
  }
}
