import { Map, StyleSpecification } from 'maplibre-gl';

export const updateMapStyleWithTiles = (
  isConnected: boolean,
  isDarkMode: boolean,
  setMapStyle: (style: StyleSpecification) => void,
  mapInstance?: Map,
): void => {
  const backgroundColor = isDarkMode ? '#2B2B2B' : '#E0E0D1';
  const offlineBounds: [number, number, number, number] = [
    8.0957189953, 51.5665790443, 8.1174985325, 51.5788185287,
  ];
  const germanyBounds: [number, number, number, number] = [
    5.87, 47.27, 15.04, 55.1,
  ];
  const theme = isDarkMode ? 'dark' : 'light';

  const offlineTilesUrl = `assets/feature-funfair/maptiles/${theme}/{z}/{x}/{y}.png`;
  const onlineStyleUrl = isDarkMode
    ? `https://tiles.openfreemap.org/styles/positron`
    : `https://tiles.openfreemap.org/styles/bright`;

  if (isConnected) {
    mapInstance?.setStyle(onlineStyleUrl);
    mapInstance?.setMaxBounds(germanyBounds);
  } else {
    const offlineStyle: StyleSpecification = {
      version: 8,
      sources: {
        'offline-tiles': {
          type: 'raster',
          tiles: [offlineTilesUrl],
          tileSize: 256,
          minzoom: 14,
          maxzoom: 19,
          bounds: offlineBounds,
        },
      },
      layers: [
        {
          id: 'background',
          type: 'background',
          paint: { 'background-color': backgroundColor },
        },
        {
          id: 'offline-tiles-layer',
          type: 'raster',
          source: 'offline-tiles',
          layout: { visibility: 'visible' },
        },
      ],
    };

    setMapStyle(offlineStyle);
  }
};
