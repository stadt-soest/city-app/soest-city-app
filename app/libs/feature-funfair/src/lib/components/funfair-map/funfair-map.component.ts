import {
  Component,
  DestroyRef,
  EventEmitter,
  inject,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { BehaviorSubject, combineLatest, filter, Observable } from 'rxjs';
import { AsyncPipe, NgIf } from '@angular/common';
import { Feature, FeatureCollection, Geometry } from 'geojson';
import { Properties } from '../../models/feature.model';
import { Map as MapLibreMap, StyleSpecification } from 'maplibre-gl';
import { FUNFAIR_CONFIG } from '../../funfair-tokens';
import { DataService } from '../../services/geojson-data.service';
import { FunfairMapService } from '../../services/funfair-map.service';
import { PluginListenerHandle } from '@capacitor/core';
import { ThemeService } from '@sw-code/urbo-ui';
import { FunfairIconService } from '../../services/funfair-icon.service';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { Network } from '@capacitor/network';
import {
  AttributionControlDirective,
  ControlComponent,
  GeoJSONSourceComponent,
  GeolocateControlDirective,
  LayerComponent,
  MapComponent,
  NavigationControlDirective,
} from '@maplibre/ngx-maplibre-gl';
import { getDistance } from 'geolib';
import { updateMapStyleWithTiles } from './map-tile-utils';

@Component({
  selector: 'lib-funfair-map',
  templateUrl: './funfair-map.component.html',
  styleUrls: ['./funfair-map.component.scss'],
  imports: [
    MapComponent,
    ControlComponent,
    GeolocateControlDirective,
    GeoJSONSourceComponent,
    LayerComponent,
    AsyncPipe,
    NgIf,
    NavigationControlDirective,
    AttributionControlDirective,
  ],
})
export class FunfairMapComponent implements OnInit, OnDestroy {
  @Output() featureClick = new EventEmitter<{
    feature: Feature<Geometry, Properties>;
    categoryName: string;
  }>();
  @Output() showCenterButtonChange = new EventEmitter<boolean>();
  filteredGeoJSON$!: Observable<FeatureCollection<Geometry, Properties> | null>;
  filteredCentroidGeoJSON$!: Observable<FeatureCollection<
    Geometry,
    Properties
  > | null>;
  favoritesGeoJSON$!: Observable<FeatureCollection<
    Geometry,
    Properties
  > | null>;
  mapStyle: StyleSpecification | string = {
    version: 8,
    sources: {},
    layers: [],
  };
  hasDisabledIcons = false;
  isDarkmode = false;
  protected funfairConfig = inject(FUNFAIR_CONFIG);
  private readonly dataService = inject(DataService);
  private readonly mapService = inject(FunfairMapService);
  private readonly destroyRef = inject(DestroyRef);
  private previousShouldShowButton: boolean | null = null;
  private networkListener!: PluginListenerHandle;
  private readonly networkStatus$ = new BehaviorSubject<boolean>(true);
  private readonly themeService = inject(ThemeService);
  private readonly funfairMapIconService = inject(FunfairIconService);

  handleMapLoad(mapInstance: MapLibreMap): void {
    this.mapService.setMapInstance(mapInstance);
  }

  async ngOnInit() {
    this.isDarkmode = this.themeService.isDarkMode();
    this.funfairMapIconService.iconsLoaded$
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((iconsLoaded) => {
        if (iconsLoaded) {
          this.filteredCentroidGeoJSON$ =
            this.dataService.getFilteredCentroidGeoJSON();
        }
      });
    this.filteredGeoJSON$ = this.dataService.getFilteredGeoJSON();
    this.favoritesGeoJSON$ = this.dataService.getFavoritesGeoJSON();
    await this.dataService.loadGeoJSON();

    combineLatest([
      this.networkStatus$,
      this.themeService.isDarkMode$,
      this.mapService.mapInstance$.pipe(filter((map) => !!map)),
    ])
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(([isConnected, isDarkMode, mapInstance]) => {
        this.updateMapStyle(isConnected, isDarkMode, mapInstance ?? undefined);
      });

    const connectionStatus = await Network.getStatus();
    this.networkStatus$.next(connectionStatus.connected);

    this.networkListener = await Network.addListener(
      'networkStatusChange',
      (status) => {
        this.networkStatus$.next(status.connected);
      },
    );
  }

  ngOnDestroy() {
    this.networkListener?.remove();
    this.mapService.clearMapInstance();
  }

  setHighlighting(
    selectedCategoryId: string | null,
    selectedFeatureId: string | null,
  ) {
    this.mapService.setHighlighting(selectedCategoryId, selectedFeatureId);
  }

  recenterMap() {
    this.mapService.easeTo({
      center: this.funfairConfig.map.center,
      zoom: 13,
      essential: true,
    });
  }

  async onClickFeature(event: any) {
    const features = event.features;
    if (features && features.length > 0) {
      const clickedFeature = features[0];
      this.featureClick.emit({
        feature: clickedFeature,
        categoryName: clickedFeature.properties.category,
      });
    }
  }

  onMapMoveEnd = (event: any) => {
    const map = event.target as MapLibreMap;
    const currentCenter = map.getCenter();

    const distance = this.calculateDistance(
      [currentCenter.lng, currentCenter.lat],
      this.funfairConfig.map.center,
    );

    const threshold = 1300;

    const shouldShowButton = distance > threshold;

    if (this.previousShouldShowButton !== shouldShowButton) {
      this.showCenterButtonChange.emit(shouldShowButton);
      this.previousShouldShowButton = shouldShowButton;
    }
  };

  private updateMapStyle(
    isConnected: boolean,
    isDarkMode: boolean,
    mapInstance?: MapLibreMap,
  ) {
    updateMapStyleWithTiles(
      isConnected,
      isDarkMode,
      (style) => {
        this.mapStyle = style;
      },
      mapInstance,
    );
  }

  private calculateDistance(
    coord1: [number, number],
    coord2: [number, number],
  ): number {
    return getDistance(
      { latitude: coord1[1], longitude: coord1[0] },
      { latitude: coord2[1], longitude: coord2[0] },
    );
  }
}
