import { Component, EventEmitter, Output } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  CustomButtonColorScheme,
  CustomButtonType,
  CustomVerticalButtonComponent,
} from '@sw-code/urbo-ui';

@Component({
  selector: 'lib-funfair-center-info-card',
  templateUrl: './funfair-center-info-card.component.html',
  styleUrls: ['./funfair-center-info-card.component.scss'],
  imports: [TranslocoDirective, CustomVerticalButtonComponent],
})
export class FunfairCenterInfoCardComponent {
  @Output() buttonClick = new EventEmitter<MouseEvent>();
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
}
