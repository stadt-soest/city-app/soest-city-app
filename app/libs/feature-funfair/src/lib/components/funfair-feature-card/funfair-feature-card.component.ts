import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IonRouterLinkWithHref } from '@ionic/angular/standalone';
import { RouterLink } from '@angular/router';
import { NgClass, NgIf, NgTemplateOutlet } from '@angular/common';
import { GeneralIconComponent, IconFontSize } from '@sw-code/urbo-ui';

@Component({
  selector: 'lib-funfair-feature-card',
  templateUrl: './funfair-feature-card.component.html',
  styleUrls: ['./funfair-feature-card.component.scss'],
  imports: [
    IonRouterLinkWithHref,
    RouterLink,
    NgClass,
    NgTemplateOutlet,
    NgIf,
    GeneralIconComponent,
  ],
})
export class FunfairFeatureCardComponent {
  @Input() title = '';
  @Input() subtitle = '';
  @Input() content = '';
  @Input({ required: true }) icon = '';
  @Input() href = '';
  @Input() hasBoxShadow = true;
  @Output() clickButton = new EventEmitter<MouseEvent>();
  protected readonly iconFontSize = IconFontSize;
}
