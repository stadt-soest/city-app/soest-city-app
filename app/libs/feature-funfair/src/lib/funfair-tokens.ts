import { InjectionToken } from '@angular/core';

export interface FunfairConfig {
  appName: string;
  map: MapConfig;
}

export interface MapConfig {
  marker: {
    color: string;
    scale: number;
  };
  canvasContextAttributes: {
    antialias: boolean;
  };
  pixelRatio: number;
  styleUrlDark: string;
  styleUrlLight: string;
  center: [number, number];
  zoom: number;
}

export const FUNFAIR_CONFIG = new InjectionToken<FunfairConfig>(
  'FunfairConfig',
);
