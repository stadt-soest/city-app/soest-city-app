export enum PredefinedCategory {
  favorites = 'favorites',
  all = 'all',
}

export type FunfairCategory = PredefinedCategory | string;
