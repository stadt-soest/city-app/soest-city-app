import { CustomButtonColorScheme } from '@sw-code/urbo-ui';

export class Category {
  name: string;
  nameKey: string;
  icon: string;
  iconColor: 'red' | 'orange' | 'green' | 'main' | 'default';
  buttonColorType: CustomButtonColorScheme = CustomButtonColorScheme.strong;

  constructor(
    name: string,
    icon: string,
    iconColor: 'red' | 'orange' | 'green' | 'main' | 'default',
    nameKey: string,
    public sourceCategories: string[] = [],
    buttonColorType: CustomButtonColorScheme = CustomButtonColorScheme.strong,
  ) {
    this.name = name;
    this.icon = icon;
    this.iconColor = iconColor;
    this.buttonColorType = buttonColorType;
    this.nameKey = nameKey;
  }
}

export class CategoryGroup {
  title: string;
  titleKey: string;
  categories: Category[];

  constructor(title: string, categories: Category[], titleKey: string) {
    this.title = title;
    this.categories = categories;
    this.titleKey = titleKey;
  }
}
