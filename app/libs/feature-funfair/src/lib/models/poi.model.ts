import { Feature, Geometry } from 'geojson';
import { Properties } from './feature.model';

export class FunfairPoi {
  id!: string;
  name!: string;
  category!: string;
  feature!: Feature<Geometry, Properties>;
}
