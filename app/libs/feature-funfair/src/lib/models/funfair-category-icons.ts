export const categoryIconMap: { [key: string]: string } = {
  Wickeltisch: 'urbo_baby_changing_station',
  'Barrierefreies WC': 'urbo_accessible',
  Urinal: 'urbo_man',
};
