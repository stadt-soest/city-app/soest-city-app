import en from './lib/i18n/en.json';
import de from './lib/i18n/de.json';
import {
  TranslocoTestingModule,
  TranslocoTestingOptions,
} from '@jsverse/transloco';

export const getTranslocoModule = (options: TranslocoTestingOptions = {}) => {
  const testingOptions: TranslocoTestingOptions = {
    langs: {
      en,
      de,
      'feature_funfair/de': de,
      'feature_funfair/en': en,
    },
    translocoConfig: {
      availableLangs: ['en', 'de'],
      defaultLang: 'en',
    },
    preloadLangs: true,
    ...options,
  };

  (testingOptions.translocoConfig as any).scopeMapping = {
    feature_fair: 'feature_funfair',
  };

  return TranslocoTestingModule.forRoot(testingOptions);
};
