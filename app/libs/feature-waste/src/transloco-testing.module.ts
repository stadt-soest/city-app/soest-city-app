import en from './lib/i18n/en.json';
import de from './lib/i18n/de.json';
import {
  TranslocoTestingModule,
  TranslocoTestingOptions,
} from '@jsverse/transloco';
import { ModuleWithProviders } from '@angular/core';

export const getTranslocoModule = (
  options: TranslocoTestingOptions = {},
): ModuleWithProviders<TranslocoTestingModule> => {
  const testingOptions: TranslocoTestingOptions = {
    langs: {
      en,
      de,
      'feature_waste/de': de,
      'feature_waste/en': en,
    },
    translocoConfig: {
      availableLangs: ['en', 'de'],
      defaultLang: 'en',
    },
    preloadLangs: true,
    ...options,
  };

  (testingOptions.translocoConfig as any).scopeMapping = {
    feature_waste: 'feature_waste',
  };

  return TranslocoTestingModule.forRoot(testingOptions);
};
