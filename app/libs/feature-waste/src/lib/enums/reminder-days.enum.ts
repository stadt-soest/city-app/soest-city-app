export enum ReminderDay {
  dayBefore = 'dayBefore',
  sameDay = 'sameDay',
  none = 'none',
}
