export enum SettingsKeys {
  collectionAreaId = 'collectionAreaId',
  collectionAreaStreet = 'collectionAreaStreet',
  residualIndividualAgreement = 'residualIndividualAgreement',
  notificationStatus = 'notificationStatus',
  notificationSetupCompleted = 'notificationSetupCompleted',
  notificationReminderTime = 'notificationReminderTime',
  notificationReminderDay = 'notificationReminderDay',
  notificationsWasteTypes = 'notificationsWasteTypes',
}
