import {
  Inject,
  InjectionToken,
  ModuleWithProviders,
  NgModule,
  inject,
  provideAppInitializer,
} from '@angular/core';
import { WasteService } from './services/waste.service';
import { WasteConfigService } from './services/waste-config.service';
import { WasteStorageLoaderService } from './services/storage-loader/waste-storage-loader.service';
import { WasteReminderService } from './services/reminder/waste-reminder.service';
import { WasteFeedablesLoaderService } from './services/feedables-loader/waste-feedables-loader.service';
import { provideTranslocoScope } from '@jsverse/transloco';
import {
  FeatureModule,
  ModuleCategory,
  ModuleInfo,
  ModuleRegistryService,
} from '@sw-code/urbo-core';
import { from, of } from 'rxjs';

const PROVIDERS = [
  WasteService,
  WasteConfigService,
  WasteStorageLoaderService,
  WasteReminderService,
  WasteFeedablesLoaderService,
  provideTranslocoScope({
    scope: 'feature-waste',
    alias: 'feature_waste',
    loader: {
      en: () => import('./i18n/en.json'),
      de: () => import('./i18n/de.json'),
    },
  }),
];
export const WASTE_MODULE_INFO = new InjectionToken<ModuleInfo>(
  'WasteModuleInfoHolderToken',
);
export const ESG_IOS_STORE_URL = new InjectionToken<string>(
  'ESG_IOS_STORE_URL',
);
export const ESG_ANDROID_STORE_URL = new InjectionToken<string>(
  'ESG_ANDROID_STORE_URL',
);
export const ESG_WEB_URL = new InjectionToken<string>('ESG_WEB_URL');
const moduleId = 'waste';

@NgModule({
  providers: [
    ...PROVIDERS,
    {
      provide: WASTE_MODULE_INFO,
      useValue: {
        id: moduleId,
        name: 'feature_waste.module_name',
        category: ModuleCategory.information,
        icon: 'auto_delete',
        baseRoutingPath: moduleId,
        defaultVisibility: true,
        settingsPageAvailable: true,
        isAboutAppItself: false,
        relevancy: 0,
        forYouRelevant: true,
        showInCategoriesPage: true,
        orderInCategory: 2,
      },
    },
  ],
})
export class FeatureWasteModule {
  readonly featureModule: FeatureModule<null>;

  constructor(
    @Inject(WASTE_MODULE_INFO) private readonly wasteModuleInfo: ModuleInfo,
    private readonly wasteFeedablesLoaderService: WasteFeedablesLoaderService,
  ) {
    this.featureModule = {
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      initializeDefaultSettings: () => {},
      feedables: () => of({ items: [], totalRecords: 0 }),
      feedablesActivity: () => this.wasteFeedablesLoaderService.getFeedables(),
      feedablesUnfiltered: () => from([]),
      info: this.wasteModuleInfo,
      routes: [
        {
          path: `categories/${moduleId}`,
          loadComponent: () =>
            import('./pages/waste-page.component').then(
              (m) => m.WastePageComponent,
            ),
          title: 'feature_waste.module_name',
        },
        {
          path: `categories/${moduleId}/settings`,
          loadComponent: () =>
            import('./pages/settings/settings-page.component').then(
              (m) => m.SettingsPageComponent,
            ),
          title: 'feature_waste.waste-settings-menu.header',
        },
        {
          path: `for-you/settings/${moduleId}`,
          loadComponent: () =>
            import('./pages/settings/settings-page.component').then(
              (m) => m.SettingsPageComponent,
            ),
          title: 'feature_waste.waste-settings-menu.header',
        },
        {
          path: `for-you/${moduleId}`,
          loadComponent: () =>
            import('./pages/waste-page.component').then(
              (m) => m.WastePageComponent,
            ),
          title: 'feature_waste.module_name',
        },
        {
          path: `for-you/${moduleId}/settings`,
          loadComponent: () =>
            import('./pages/settings/settings-page.component').then(
              (m) => m.SettingsPageComponent,
            ),
          title: 'feature_waste.waste-settings-menu.header',
        },
      ],
    };
  }

  static forRoot(): ModuleWithProviders<FeatureWasteModule> {
    return {
      ngModule: FeatureWasteModule,
      providers: [
        provideAppInitializer(() => {
          const initializerFn = (
            (registry: ModuleRegistryService, module: FeatureWasteModule) =>
            () =>
              registry.registerFeature(module.featureModule)
          )(inject(ModuleRegistryService), inject(FeatureWasteModule));
          return initializerFn();
        }),
        provideAppInitializer(() => {
          const initializerFn = (
            (
              wasteReminderService: WasteReminderService,
              wasteStorageLoaderService: WasteStorageLoaderService,
            ) =>
            async () => {
              try {
                const wasteUserSettings =
                  await wasteStorageLoaderService.getWasteUserSettings();

                if (!wasteUserSettings) {
                  return;
                }
                await wasteStorageLoaderService.saveWasteSettings(
                  wasteUserSettings,
                );

                await wasteReminderService.updateReminders(
                  wasteUserSettings,
                  true,
                );
              } catch (error) {
                console.error('Error Updating Reminders:', error);
              }
            }
          )(inject(WasteReminderService), inject(WasteStorageLoaderService));
          return initializerFn();
        }),
      ],
    };
  }
}
