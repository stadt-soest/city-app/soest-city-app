import {
  FeedItemLayoutType,
  ModuleCategory,
  ModuleInfo,
} from '@sw-code/urbo-core';
import { Waste, WasteUpcoming } from '../../models/waste-date.model';
import { WasteFeedItem } from '../../models/WasteFeedItem';
import { WasteBinCardComponent } from '../../components/waste-bin-card/waste-bin-card.component';
import { WasteUserSettings } from '../../models/waste-user-settings.interface';
import { ReminderDay } from '../../enums/reminder-days.enum';
import { WasteTypeEnum } from '../../models/waste-type.enum';

export const createWasteModuleInfo: ModuleInfo = {
  id: 'waste',
  name: 'Entsorgung',
  category: ModuleCategory.information,
  icon: 'auto_delete',
  baseRoutingPath: '/waste',
  defaultVisibility: true,
  settingsPageAvailable: false,
  isAboutAppItself: false,
  relevancy: 0.5,
  forYouRelevant: false,
  showInCategoriesPage: false,
  orderInCategory: 1,
};

export const createUpcomingWaste = (
  overrides?: Partial<WasteUpcoming>,
): WasteUpcoming => ({
  allUpcomingDates: [
    {
      id: 'default-waste-date-id',
      date: 'default-waste-date',
      wasteType: WasteTypeEnum.Wastepaper,
      wasteTypeColor: '#679BFF',
      wasteTypeImagePath: 'assets/feature-waste/icons/paper_waste_icon.svg',
    },
  ],
  upcomingDatesByType: {
    Wastepaper: [
      {
        id: 'default-waste-date-id',
        date: 'default-waste-date',
        wasteType: WasteTypeEnum.Wastepaper,
        wasteTypeColor: '#679BFF',
        wasteTypeImagePath: 'assets/feature-waste/icons/paper_waste_icon.svg',
      },
    ],
  },
  ...overrides,
});

export const createWaste = (overrides?: Partial<Waste>): Waste => ({
  id: 'default-waste-id',
  city: 'default-waste-city',
  street: 'default-waste-street',
  wasteDates: [
    {
      id: 'default-waste-date-id',
      date: 'default-waste-date',
      wasteType: WasteTypeEnum.Wastepaper,
      wasteTypeColor: '#679BFF',
      wasteTypeImagePath: 'assets/feature-waste/icons/paper_waste_icon.svg',
    },
  ],
  ...overrides,
});

export const createWasteFeedItem = (
  overrides?: Partial<WasteFeedItem>,
): WasteFeedItem => ({
  wasteDate: {
    id: 'default-waste-date-id',
    date: 'default-waste-date',
    wasteType: WasteTypeEnum.Wastepaper,
    wasteTypeColor: '#679BFF',
    wasteTypeImagePath: 'assets/feature-waste/icons/paper_waste_icon.svg',
  },
  nextCollectionDate: 'default-next-collection-date',
  id: 'default-id',
  moduleId: 'waste',
  moduleName: 'feature_waste.module_name_singular',
  pluralModuleName: 'feature_waste.module_name',
  baseRoutePath: 'waste',
  icon: 'waste',
  title: 'default title',
  creationDate: new Date('2023-10-20T08:00:00Z'),
  cardComponent: WasteBinCardComponent,
  forYouComponent: WasteBinCardComponent,
  relevancy: 0,
  layoutType: FeedItemLayoutType.card,
  ...overrides,
});

export const createWasteUserSettings = (
  overrides?: Partial<WasteUserSettings>,
): WasteUserSettings => ({
  wasteCollectionId: 'default-waste-collection-id',
  collectionAreaStreet: 'default-waste-collection-area-street',
  setupCompleted: true,
  residualIndividualAgreement: true,
  status: true,
  reminderTime: 'default-waste-reminder-time',
  reminderDay: ReminderDay.dayBefore,
  wasteTypes: [WasteTypeEnum.Wastepaper],
  ...overrides,
});
