import { TestBed } from '@angular/core/testing';
import { WasteReminderService } from './waste-reminder.service';
import { WasteService } from '../waste.service';
import { WasteStorageLoaderService } from '../storage-loader/waste-storage-loader.service';
import {
  CoreTestModule,
  NotificationService,
  StorageKeyType,
} from '@sw-code/urbo-core';
import {
  LocalizedFormatDatePipe,
  LocalizedFormatDistanceTimePipePipe,
} from '@sw-code/urbo-ui';
import {
  createUpcomingWaste,
  createWasteUserSettings,
} from '../../shared/testing/waste-test-utils';
import { of } from 'rxjs';
import { getTranslocoModule } from '../../../transloco-testing.module';

describe('WasteReminderService', () => {
  let service: WasteReminderService;
  let mockWasteService: jest.Mocked<WasteService>;
  let mockWasteStorageLoaderService: jest.Mocked<WasteStorageLoaderService>;
  let mockNotificationService: jest.Mocked<NotificationService>;
  let mockLocalizedFormatDatePipe: jest.Mocked<LocalizedFormatDatePipe>;

  const notificationGroupKey = 'wasteCollection';

  beforeEach(() => {
    mockWasteService = {
      getUpcomingWasteCollections: jest.fn(),
      getNextCollectionDate: jest.fn(),
      localizedDistanceUntilNextPickup: jest.fn(),
    } as unknown as jest.Mocked<WasteService>;

    mockWasteStorageLoaderService = {
      saveWasteSettings: jest.fn(),
      saveNotificationData: jest.fn(),
      getSettingsWasteData: jest.fn(),
      removeSettings: jest.fn(),
    } as unknown as jest.Mocked<WasteStorageLoaderService>;

    mockLocalizedFormatDatePipe = {
      transform: jest.fn().mockReturnValue('Friday'),
    } as unknown as jest.Mocked<LocalizedFormatDatePipe>;

    mockNotificationService = {
      cancelNotificationsByGroup: jest.fn(),
      scheduleNotification: jest.fn(),
      cancelAllNotifications: jest.fn(),
    } as unknown as jest.Mocked<NotificationService>;

    TestBed.configureTestingModule({
      imports: [CoreTestModule.forRoot(), getTranslocoModule()],
      providers: [
        WasteReminderService,
        { provide: WasteService, useValue: mockWasteService },
        {
          provide: WasteStorageLoaderService,
          useValue: mockWasteStorageLoaderService,
        },
        { provide: NotificationService, useValue: mockNotificationService },
        LocalizedFormatDistanceTimePipePipe,
        {
          provide: LocalizedFormatDatePipe,
          useValue: mockLocalizedFormatDatePipe,
        },
      ],
    });

    service = TestBed.inject(WasteReminderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('updateReminders', () => {
    it('should not update reminders when no changes in settings or data', async () => {
      const sameSettings = createWasteUserSettings({ status: true });
      const sameData = createUpcomingWaste();

      mockWasteStorageLoaderService.getSettingsWasteData.mockImplementation(
        (key) => {
          if (key === StorageKeyType.NOTIFICATION) {
            return Promise.resolve(sameSettings);
          } else if (key === StorageKeyType.NOTIFICATION_DATA) {
            return Promise.resolve(sameData);
          }
          return Promise.reject(new Error('Invalid key'));
        },
      );

      mockWasteService.getUpcomingWasteCollections.mockReturnValue(
        of(sameData),
      );

      await service.updateReminders(sameSettings);
      expect(
        mockNotificationService.scheduleNotification,
      ).not.toHaveBeenCalled();
    });

    it('should cancel existing reminders when updating is necessary', async () => {
      mockWasteService.getUpcomingWasteCollections.mockReturnValue(
        of(createUpcomingWaste()),
      );
      mockWasteStorageLoaderService.getSettingsWasteData.mockResolvedValue(
        createWasteUserSettings(),
      );
      await service.updateReminders(createWasteUserSettings({ status: true }));
      expect(mockWasteService.getUpcomingWasteCollections).toHaveBeenCalled();
      expect(mockNotificationService.cancelAllNotifications).toHaveBeenCalled();
    });
  });
});
