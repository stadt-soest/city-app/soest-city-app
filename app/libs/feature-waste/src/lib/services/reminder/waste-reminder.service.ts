import { inject, Injectable } from '@angular/core';
import {
  generateUniqueId,
  NotificationDetails,
  NotificationService,
} from '@sw-code/urbo-core';
import { WasteService } from '../waste.service';
import { TranslocoService } from '@jsverse/transloco';
import { WasteStorageLoaderService } from '../storage-loader/waste-storage-loader.service';
import { LocalizedFormatDatePipe } from '@sw-code/urbo-ui';
import { WasteDate, WasteUpcoming } from '../../models/waste-date.model';
import { WasteUserSettings } from '../../models/waste-user-settings.interface';
import { filter, firstValueFrom, take } from 'rxjs';
import { ReminderDay } from '../../enums/reminder-days.enum';

/* eslint @typescript-eslint/naming-convention: 0 */

@Injectable()
export class WasteReminderService {
  private readonly notificationService = inject(NotificationService);
  private readonly wasteCollectionService = inject(WasteService);
  private readonly translationService = inject(TranslocoService);
  private readonly wasteStorageLoaderService = inject(
    WasteStorageLoaderService,
  );
  private readonly localizedDatePipe = inject(LocalizedFormatDatePipe);
  private readonly iconMapping: {
    [key in WasteDate.WasteTypeEnum]: {
      drawableResourceId: string;
      iconColor: string;
      iosIcon: string;
    };
  } = {
    BIOWASTE: {
      drawableResourceId: 'bio_waste_icon',
      iconColor: '#88d66d',
      iosIcon: 'bio_waste_icon.png',
    },
    YELLOW_BAG: {
      drawableResourceId: 'packaging_waste_icon',
      iconColor: '#ffeb7d',
      iosIcon: 'packaging_waste_icon.png',
    },
    WASTEPAPER: {
      drawableResourceId: 'paper_waste_icon',
      iconColor: '#679bff',
      iosIcon: 'paper_waste_icon.png',
    },
    RESIDUAL_WASTE_14DAILY: {
      drawableResourceId: 'residual_waste_icon',
      iconColor: '#b6b6b6',
      iosIcon: 'residual_waste_icon.png',
    },
    RESIDUAL_WASTE_4WEEKLY: {
      drawableResourceId: 'residual_waste_icon',
      iconColor: '#b6b6b6',
      iosIcon: 'residual_waste_icon.png',
    },
    CHRISTMAS_TREE: {
      drawableResourceId: 'christmas_tree_icon',
      iconColor: '#84d369',
      iosIcon: 'christmas_tree_icon.png',
    },
    DIAPER_WASTE: {
      drawableResourceId: 'diaper_waste_icon',
      iconColor: '#ffc107',
      iosIcon: 'diaper_waste_icon.png',
    },
  };

  async updateReminders(
    wasteUserSettings: WasteUserSettings,
    checkTranslation = false,
  ) {
    const scheduleData = await this.fetchWasteCollectionSchedule(
      wasteUserSettings.wasteCollectionId,
      wasteUserSettings.wasteTypes,
    );
    if (wasteUserSettings.status) {
      if (!scheduleData?.allUpcomingDates) {
        console.error('Failed to fetch waste collection schedule.');
        return;
      }

      await this.cancelExistingReminders();
      this.scheduleReminders(scheduleData, wasteUserSettings, checkTranslation);
      await this.storeLastUsedSettingsAndData(wasteUserSettings, scheduleData);
    }
  }

  async cancelExistingReminders() {
    await this.wasteStorageLoaderService.removeSettings();
    await this.notificationService.cancelAllNotifications();
  }

  private async storeLastUsedSettingsAndData(
    settings: WasteUserSettings,
    data: WasteUpcoming,
  ) {
    await this.wasteStorageLoaderService.saveWasteSettings(settings);
    await this.wasteStorageLoaderService.saveNotificationData(data);
  }

  private async fetchWasteCollectionSchedule(
    wasteCollectionId: string,
    wasteTypes: string[],
  ): Promise<WasteUpcoming> {
    try {
      return await firstValueFrom(
        this.wasteCollectionService.getUpcomingWasteCollections(
          wasteCollectionId,
          wasteTypes,
        ),
      );
    } catch (error) {
      throw new Error('Error fetching waste collection schedule:' + error);
    }
  }

  private scheduleReminders(
    wasteUpcoming: WasteUpcoming,
    wasteUserSettings: WasteUserSettings,
    checkTranslation: boolean,
  ) {
    const now = new Date();
    const notificationCountMap = new Map<string, number>();

    wasteUpcoming.allUpcomingDates.forEach(async (item) => {
      if (item.date && item.wasteType) {
        let currentCount = notificationCountMap.get(item.wasteType) ?? 0;
        currentCount += 1;
        notificationCountMap.set(item.wasteType, currentCount);

        const nextCollectionDate =
          this.wasteCollectionService.getNextCollectionDate(
            item.wasteType,
            currentCount,
            wasteUpcoming,
          );

        const distanceUntilNextPickUp =
          this.wasteCollectionService.localizedDistanceUntilNextPickup(
            nextCollectionDate,
            item,
          );
        const notificationDetails = await this.createNotificationDetails(
          item,
          wasteUserSettings,
          !isNaN(distanceUntilNextPickUp) ? distanceUntilNextPickUp : undefined,
          checkTranslation,
        );

        const notificationTime = new Date(notificationDetails.scheduleTime);

        if (notificationTime > now) {
          await this.notificationService.scheduleNotification(
            notificationDetails,
          );
        }
      }
    });
  }

  private async createNotificationDetails(
    item: WasteDate,
    settings: WasteUserSettings,
    distanceUntilNextPickUp: number | undefined,
    checkTranslations: boolean,
  ): Promise<NotificationDetails> {
    const notificationTime = this.calculateNotificationTime(
      item.date,
      settings,
    );

    if (checkTranslations) {
      await firstValueFrom(
        this.translationService.events$.pipe(
          filter(
            (e) =>
              e.type === 'translationLoadSuccess' &&
              e.payload.scope === 'feature-waste',
          ),
          take(1),
        ),
      );
    }

    const wasteTypeTranslationKey = `feature_waste.wasteTypes.${item.wasteType}`;
    const wasteType = this.translationService.translate(
      wasteTypeTranslationKey,
    );
    const title = this.translationService.translate(
      'feature_waste.notifications.wasteCollectionReminder',
      {
        wasteType,
      },
    );

    const formattedDateDay = this.localizedDatePipe.transform(
      new Date(item.date),
      'EEEE',
    );
    const body =
      this.translationService.translate(
        'feature_waste.notifications.reminderBody',
        {
          wasteType,
          weekDay: formattedDateDay,
        },
      ) +
      (distanceUntilNextPickUp
        ? this.translationService.translate(
            'feature_waste.waste-bin-card.again_in',
            {
              days: distanceUntilNextPickUp,
            },
          )
        : '');

    const { drawableResourceId, iconColor, iosIcon } =
      this.iconMapping[item.wasteType];

    if (!/^#[0-9A-F]{6}$/i.test(iconColor)) {
      throw new Error(
        `Invalid color provided. Must be a hex string (ex: #ff0000). Received: ${iconColor}`,
      );
    }

    return {
      id: generateUniqueId(),
      title,
      body,
      scheduleTime: notificationTime,
      drawableResourceId,
      iconColor,
      attachments: [
        {
          id: 'image',
          url: `res://Assets.xcassets/WasteIcons/${iosIcon}`,
          type: 'image/png',
        },
      ],
    };
  }

  private calculateNotificationTime(
    collectionDate: string,
    settings: WasteUserSettings,
  ): Date {
    const [hour, minute] = settings.reminderTime.split(':').map(Number);
    const date = new Date(collectionDate);
    date.setHours(hour, minute, 0);

    if (settings.reminderDay === ReminderDay.dayBefore) {
      date.setDate(date.getDate() - 1);
    }

    return date;
  }
}
