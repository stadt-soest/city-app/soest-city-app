import { inject, Injectable } from '@angular/core';
import {
  ModuleInfo,
  SettingsPersistenceService,
  StorageKeyType,
  StorageMigrationService,
} from '@sw-code/urbo-core';
import { WASTE_MODULE_INFO } from '../../feature-waste.module';
import { WasteUserSettings } from '../../models/waste-user-settings.interface';
import { SettingsKeys } from '../../enums/settings.keys.enums';
import { ReminderDay } from '../../enums/reminder-days.enum';
import { WasteUpcoming } from '../../models/waste-date.model';

@Injectable()
export class WasteStorageLoaderService {
  private readonly settingsPersistenceService = inject(
    SettingsPersistenceService,
  );
  private readonly wasteModuleInfo = inject<ModuleInfo>(WASTE_MODULE_INFO);
  private readonly storageMigrationService = inject(StorageMigrationService);

  async saveWasteSettings(wasteUserSettings: WasteUserSettings): Promise<void> {
    await this.migrateWasteTypes(wasteUserSettings);

    await this.settingsPersistenceService.saveSettings(
      this.wasteModuleInfo,
      StorageKeyType.NOTIFICATION,
      {
        [SettingsKeys.collectionAreaId]: wasteUserSettings.wasteCollectionId,
        [SettingsKeys.collectionAreaStreet]:
          wasteUserSettings.collectionAreaStreet,
        [SettingsKeys.notificationStatus]: wasteUserSettings.status,
        [SettingsKeys.residualIndividualAgreement]:
          wasteUserSettings.residualIndividualAgreement,
        [SettingsKeys.notificationSetupCompleted]:
          wasteUserSettings.setupCompleted,
        [SettingsKeys.notificationReminderTime]: wasteUserSettings.reminderTime,
        [SettingsKeys.notificationReminderDay]: wasteUserSettings.reminderDay,
        [SettingsKeys.notificationsWasteTypes]: wasteUserSettings.wasteTypes,
      },
    );
  }

  async getWasteUserSettings(): Promise<WasteUserSettings | undefined> {
    const preferences = await this.getSettings(StorageKeyType.NOTIFICATION);

    if (!preferences) {
      return;
    }

    const notificationStatusTest =
      (preferences[SettingsKeys.notificationStatus] as boolean) ?? false;
    return {
      wasteCollectionId:
        (preferences[SettingsKeys.collectionAreaId] as string) ?? '',
      collectionAreaStreet:
        (preferences[SettingsKeys.collectionAreaStreet] as string) ?? '',
      residualIndividualAgreement:
        (preferences[SettingsKeys.residualIndividualAgreement] as boolean) ??
        false,
      status: notificationStatusTest,
      setupCompleted:
        (preferences[SettingsKeys.notificationSetupCompleted] as boolean) ??
        'true',
      reminderTime:
        (preferences[SettingsKeys.notificationReminderTime] as string) ??
        '18:00',
      reminderDay:
        (preferences[SettingsKeys.notificationReminderDay] as ReminderDay) ??
        ReminderDay.dayBefore,
      wasteTypes:
        (preferences[SettingsKeys.notificationsWasteTypes] as string[]) ?? [],
    } as WasteUserSettings;
  }

  async saveNotificationData(wasteUpcoming: WasteUpcoming) {
    await this.settingsPersistenceService.saveSettings(
      this.wasteModuleInfo,
      StorageKeyType.NOTIFICATION_DATA,
      wasteUpcoming,
    );
  }

  async getSettings(
    keyType: StorageKeyType,
  ): Promise<{ [key: string]: string | boolean | string[] }> {
    const settings = await this.settingsPersistenceService.getSettings(
      this.wasteModuleInfo,
      keyType,
    );
    return typeof settings === 'string' ? JSON.parse(settings) : settings;
  }

  async getSettingsWasteData<T>(keyType: StorageKeyType): Promise<T> {
    return await this.settingsPersistenceService.getSettings(
      this.wasteModuleInfo,
      keyType,
    );
  }

  async removeSettings(): Promise<void> {
    return await this.settingsPersistenceService.removeSettings(
      this.wasteModuleInfo,
      StorageKeyType.NOTIFICATION_DATA,
    );
  }

  private async migrateWasteTypes(
    wasteUserSettings: WasteUserSettings,
  ): Promise<void> {
    try {
      const currentVersion =
        await this.storageMigrationService.getCurrentDataVersion();
      const needsMigration = ['1', '2'].includes(currentVersion);

      if (
        needsMigration &&
        !wasteUserSettings.wasteTypes.includes('CHRISTMAS_TREE')
      ) {
        wasteUserSettings.wasteTypes = [
          ...new Set([...wasteUserSettings.wasteTypes, 'CHRISTMAS_TREE']),
        ];
      }
    } catch (error) {
      console.error('Failed to migrate waste types:', error);
      throw new Error('Failed to migrate waste settings');
    }
  }
}
