import { TestBed } from '@angular/core/testing';
import { WasteStorageLoaderService } from './waste-storage-loader.service';
import {
  ModuleInfo,
  SettingsPersistenceService,
  StorageKeyType,
  StorageMigrationService,
  TranslationService,
} from '@sw-code/urbo-core';
import {
  createUpcomingWaste,
  createWasteModuleInfo,
  createWasteUserSettings,
} from '../../shared/testing/waste-test-utils';
import { WASTE_MODULE_INFO } from '../../feature-waste.module';

describe('WasteStorageLoaderService', () => {
  let service: WasteStorageLoaderService;
  let mockSettingsPersistenceService: jest.Mocked<SettingsPersistenceService>;
  let moduleInfo: ModuleInfo;

  beforeEach(() => {
    moduleInfo = createWasteModuleInfo;

    mockSettingsPersistenceService = {
      saveSettings: jest.fn(),
      getSettings: jest.fn(),
    } as unknown as jest.Mocked<SettingsPersistenceService>;

    const mockTranslationService = {
      getCurrentLocale: jest.fn(),
    };

    const mockStorageMigrationService = {
      getCurrentDataVersion: jest.fn(),
    };

    TestBed.configureTestingModule({
      providers: [
        WasteStorageLoaderService,
        {
          provide: SettingsPersistenceService,
          useValue: mockSettingsPersistenceService,
        },
        { provide: TranslationService, useValue: mockTranslationService },
        {
          provide: WASTE_MODULE_INFO,
          useValue: createWasteModuleInfo,
        },
        {
          provide: StorageMigrationService,
          useValue: mockStorageMigrationService,
        },
        WasteStorageLoaderService,
      ],
    });

    service = TestBed.inject(WasteStorageLoaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('saveWasteSettings', () => {
    it('it should save wasteUserSettings with the correct parameters when saveWasteSettings is called', async () => {
      const wasteUserSettings = createWasteUserSettings();
      await service.saveWasteSettings(wasteUserSettings);
      expect(mockSettingsPersistenceService.saveSettings).toHaveBeenCalledWith(
        moduleInfo,
        StorageKeyType.NOTIFICATION,
        {
          collectionAreaId: wasteUserSettings.wasteCollectionId,
          collectionAreaStreet: wasteUserSettings.collectionAreaStreet,
          notificationSetupCompleted: wasteUserSettings.setupCompleted,
          residualIndividualAgreement:
            wasteUserSettings.residualIndividualAgreement,
          notificationStatus: wasteUserSettings.status,
          notificationReminderTime: wasteUserSettings.reminderTime,
          notificationReminderDay: wasteUserSettings.reminderDay,
          notificationsWasteTypes: wasteUserSettings.wasteTypes,
        },
      );
    });
  });

  describe('saveNotificationData', () => {
    it('should save wasteUpcoming with the correct parameters when saveNotificationData is called', async () => {
      const wasteUpcoming = createUpcomingWaste();
      await service.saveNotificationData(wasteUpcoming);
      expect(mockSettingsPersistenceService.saveSettings).toHaveBeenCalledWith(
        moduleInfo,
        StorageKeyType.NOTIFICATION_DATA,
        wasteUpcoming,
      );
    });
  });

  describe('getSettings', () => {
    it('should call getSettings with the correct parameters when getSettings is called', async () => {
      await service.getSettings(StorageKeyType.NOTIFICATION);
      expect(mockSettingsPersistenceService.getSettings).toHaveBeenCalledWith(
        moduleInfo,
        StorageKeyType.NOTIFICATION,
      );
    });

    it('should return the correct settings when getSettings is called', async () => {
      const expectedSettings = { collectionArea: 'default-collectionArea' };
      mockSettingsPersistenceService.getSettings.mockResolvedValueOnce(
        expectedSettings,
      );
      const result = await service.getSettings(StorageKeyType.NOTIFICATION);
      expect(result).toEqual(expectedSettings);
    });
  });
});
