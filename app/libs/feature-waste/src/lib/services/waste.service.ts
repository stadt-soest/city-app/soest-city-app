import { inject, Injectable } from '@angular/core';
import {
  catchError,
  from,
  map,
  mergeMap,
  Observable,
  of,
  Subject,
  switchMap,
  take,
  throwError,
  toArray,
} from 'rxjs';
import { WasteAdapter } from '../waste.adapter';
import { WasteStorageLoaderService } from './storage-loader/waste-storage-loader.service';
import {
  ErrorInfo,
  ErrorType,
  GeolocationService,
  PoiWithDistance,
  StorageKeyType,
} from '@sw-code/urbo-core';
import { Waste, WasteDate, WasteUpcoming } from '../models/waste-date.model';
import { WasteFeedItem } from '../models/WasteFeedItem';
import { SettingsKeys } from '../enums/settings.keys.enums';
import { differenceInDays } from 'date-fns';

@Injectable()
export class WasteService {
  private readonly wasteAdapter = inject(WasteAdapter);
  private readonly wasteStorageLoaderService = inject(
    WasteStorageLoaderService,
  );
  private readonly geolocationService = inject(GeolocationService);
  private readonly wasteModuleErrorSubject = new Subject<ErrorInfo>();

  getUpcomingWasteCollections(
    street: string,
    wasteTypes: string[],
    startDate?: string,
    endDate?: string,
  ): Observable<WasteUpcoming> {
    if (!wasteTypes.length) {
      return of({ allUpcomingDates: [], upcomingDatesByType: {} });
    }

    return this.wasteAdapter
      .getUpcomingWasteCollections(street, wasteTypes, startDate, endDate)
      .pipe(
        catchError((error) => {
          this.emitModuleError(ErrorType.MODULE_API_ERROR, true);
          return throwError(() => error);
        }),
      );
  }

  getWasteCollection(searchTerm: string): Observable<Waste[]> {
    this.emitModuleError(ErrorType.MODULE_API_ERROR, false);
    return this.wasteAdapter.getWasteCollection(searchTerm).pipe(
      catchError((error) => {
        this.emitModuleError(ErrorType.MODULE_API_ERROR, true);
        return throwError(() => error);
      }),
    );
  }

  getWasteTypes(
    collectionAreaId: string,
    isIndividualAgreement: boolean,
  ): Observable<string[]> {
    let defaultWasteTypes: string[];

    if (isIndividualAgreement) {
      defaultWasteTypes = this.getDefaultWasteTypesForIndividualAgreement();
    } else {
      defaultWasteTypes = this.getDefaultWasteTypesForStandardAgreement();
    }

    return this.getUpcomingWasteCollections(
      collectionAreaId,
      defaultWasteTypes,
    ).pipe(
      map((waste) => Object.keys(waste.upcomingDatesByType || {})),
      catchError((error) => throwError(() => error)),
    );
  }

  async getDefaultWasteTypesByAgreement(): Promise<string[]> {
    const wasteUserSettings =
      await this.wasteStorageLoaderService.getWasteUserSettings();

    if (wasteUserSettings?.residualIndividualAgreement) {
      return this.getDefaultWasteTypesForIndividualAgreement();
    } else {
      return this.getDefaultWasteTypesForStandardAgreement();
    }
  }

  getUpcomingWasteCollectionsFeed(
    street: string,
    wasteTypes: string[] = [],
    startDate?: string,
    endDate?: string,
  ): Observable<{ content: WasteFeedItem[]; totalRecords: number }> {
    this.emitModuleError(ErrorType.MODULE_API_ERROR, false);
    return this.wasteAdapter
      .getUpcomingWasteCollectionsFeed(street, wasteTypes, startDate, endDate)
      .pipe(
        catchError((error) => {
          this.emitModuleError(ErrorType.MODULE_API_ERROR, true);
          return throwError(() => error);
        }),
      );
  }

  getWasteRecyclingPoisWithDistance(): Observable<PoiWithDistance[]> {
    return this.wasteAdapter.getWasteRecyclingPoisWithDistance().pipe(
      switchMap((poisWithDistance) =>
        this.calculatePoisDistance(poisWithDistance),
      ),
      catchError((error) => {
        this.emitModuleError(ErrorType.MODULE_API_ERROR, true);
        return throwError(() => error);
      }),
    );
  }

  getWasteModuleErrors(): Observable<ErrorInfo> {
    return this.wasteModuleErrorSubject.asObservable();
  }

  loadCollectionArea(): Observable<string> {
    return from(
      this.wasteStorageLoaderService
        .getSettings(StorageKeyType.NOTIFICATION)
        .then((res) => {
          const value = res?.[SettingsKeys.collectionAreaId];
          return typeof value === 'string' ? value : '';
        }),
    );
  }

  getNextCollectionDate(
    wasteType: string,
    index: number,
    nextWasteCollection: WasteUpcoming,
  ): string {
    const upcomingDates = nextWasteCollection.upcomingDatesByType?.[wasteType];

    return upcomingDates?.[index]?.date ?? '';
  }

  localizedDistanceUntilNextPickup(
    nextCollectionDate: string,
    wasteDate: WasteDate,
  ): number {
    return differenceInDays(nextCollectionDate, wasteDate.date);
  }

  getDefaultWasteTypesForIndividualAgreement(): string[] {
    return WasteUpcoming.getDefaultWasteTypesIndividual;
  }

  getDefaultWasteTypesForStandardAgreement(): string[] {
    return WasteUpcoming.getDefaultWasteTypes;
  }

  private calculatePoisDistance(
    poisWithDistance: PoiWithDistance[],
  ): Observable<PoiWithDistance[]> {
    return from(this.geolocationService.getCurrentLocation()).pipe(
      take(1),
      switchMap((currentLocation) => {
        if (currentLocation) {
          return from(poisWithDistance).pipe(
            mergeMap((poiWithDistance) =>
              from(
                this.geolocationService.calculateDistance({
                  lat: poiWithDistance.poi.location.coordinates.latitude,
                  long: poiWithDistance.poi.location.coordinates.longitude,
                }),
              ).pipe(
                map((distance) => {
                  poiWithDistance.distance = distance;
                  return poiWithDistance;
                }),
              ),
            ),
            toArray(),
          );
        } else {
          return of(poisWithDistance);
        }
      }),
    );
  }

  private emitModuleError(errorType: ErrorType, hasError: boolean) {
    this.wasteModuleErrorSubject.next({
      hasError,
      errorType,
    });
  }
}
