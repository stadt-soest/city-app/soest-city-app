import { inject, Injectable } from '@angular/core';
import { catchError, from, map, Observable, of, switchMap } from 'rxjs';
import { WasteService } from '../waste.service';
import { FeedResult } from '@sw-code/urbo-core';
import { WasteFeedItem } from '../../models/WasteFeedItem';
import { addDays, startOfToday } from 'date-fns';

@Injectable()
export class WasteFeedablesLoaderService {
  private readonly wasteService = inject(WasteService);

  getFeedables(): Observable<FeedResult<WasteFeedItem>> {
    return this.wasteService.loadCollectionArea().pipe(
      switchMap((collectionAreaId) => {
        if (!collectionAreaId) {
          return of({
            items: [],
            totalRecords: 0,
          });
        } else {
          return from(this.wasteService.getDefaultWasteTypesByAgreement()).pipe(
            switchMap((wasteTypes) =>
              this.wasteService
                .getUpcomingWasteCollectionsFeed(
                  collectionAreaId,
                  wasteTypes,
                  startOfToday().toISOString(),
                  addDays(startOfToday(), 1).toISOString(),
                )
                .pipe(
                  map((response) => ({
                    items: response.content,
                    totalRecords: response.totalRecords,
                  })),
                  catchError(() =>
                    of({
                      items: [],
                      totalRecords: 0,
                    }),
                  ),
                ),
            ),
          );
        }
      }),
    );
  }
}
