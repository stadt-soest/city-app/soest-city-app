import { Injectable } from '@angular/core';

@Injectable()
export class WasteConfigService {
  private readonly imageMapping: { [key: string]: string } = {
    WASTEPAPER: 'assets/feature-waste/icons/paper_waste_icon.svg',
    YELLOW_BAG: 'assets/feature-waste/icons/packaging_waste_icon.svg',
    BIOWASTE: 'assets/feature-waste/icons/bio_waste_icon.svg',
    RESIDUAL_WASTE_14DAILY:
      'assets/feature-waste/icons/residual_waste_icon.svg',
    RESIDUAL_WASTE_4WEEKLY:
      'assets/feature-waste/icons/residual_waste_icon.svg',
    DIAPER_WASTE: 'assets/feature-waste/icons/diaper_waste_pink.svg',
    CHRISTMAS_TREE: 'assets/feature-waste/icons/christmas_tree_icon.svg',
  };

  private readonly colorMapping: { [key: string]: string } = {
    WASTEPAPER: '#679BFF',
    YELLOW_BAG: '#FFEB7D',
    BIOWASTE: '#76DE65',
    RESIDUAL_WASTE_14DAILY: '#B6B6B6',
    RESIDUAL_WASTE_4WEEKLY: '#B6B6B6',
    DIAPER_WASTE: '#FFB6C1',
    CHRISTMAS_TREE: '#76DE65',
  };

  private readonly fallbackImagePath =
    'assets/feature-waste/icons/packaging_waste_icon.svg';
  private readonly fallbackColor = '#FFFFFF';

  getImageMapping(): { [key: string]: string } {
    return this.imageMapping;
  }

  getColorMapping(): { [key: string]: string } {
    return this.colorMapping;
  }

  getFallbackImagePath(): string {
    return this.fallbackImagePath;
  }

  getFallbackColor(): string {
    return this.fallbackColor;
  }
}
