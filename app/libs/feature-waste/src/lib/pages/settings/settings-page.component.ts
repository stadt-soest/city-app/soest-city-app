import { Component } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { WasteSettingsMenuComponent } from '../../components/waste-settings-menu/waste-settings-menu.component';
import { CondensedHeaderLayoutComponent } from '@sw-code/urbo-ui';

@Component({
  selector: 'lib-waste-settings',
  templateUrl: './settings-page.component.html',
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    WasteSettingsMenuComponent,
  ],
})
export class SettingsPageComponent {}
