import { Component, DestroyRef, inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  CondensedHeaderLayoutComponent,
  ConnectivityErrorCardComponent,
  RefresherComponent,
} from '@sw-code/urbo-ui';
import { IonCol, IonGrid, IonRow } from '@ionic/angular/standalone';
import { NgFor, NgIf } from '@angular/common';
import { SpecifyAreaCardComponent } from '../components/specify-area-card/specify-area-card.component';
import { WasteBinSkeletonComponent } from '../components/waste-bin-skeleton/waste-bin-skeleton.component';
import { WasteBinCardComponent } from '../components/waste-bin-card/waste-bin-card.component';
import { EsgBannerComponent } from '../components/esg-banner/esg-banner.component';
import { NearbyRecyclingSectionComponent } from '../components/nearby-recycling-section/nearby-recycling-section.component';
import { EsgInfoCardComponent } from '../components/esg-info-card/esg-info-card.component';
import { WasteStorageLoaderService } from '../services/storage-loader/waste-storage-loader.service';
import { WasteReminderService } from '../services/reminder/waste-reminder.service';
import {
  ErrorType,
  FeedUpdateService,
  StorageKeyType,
} from '@sw-code/urbo-core';
import { SettingsKeys } from '../enums/settings.keys.enums';
import { WasteUserSettings } from '../models/waste-user-settings.interface';
import { ReminderDay } from '../enums/reminder-days.enum';
import { addDays, startOfToday } from 'date-fns';
import { WasteDate, WasteUpcoming } from '../models/waste-date.model';
import { WasteService } from '../services/waste.service';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'lib-waste',
  templateUrl: './waste-page.component.html',
  styleUrls: ['./waste-page.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    RefresherComponent,
    IonGrid,
    NgIf,
    SpecifyAreaCardComponent,
    ConnectivityErrorCardComponent,
    WasteBinSkeletonComponent,
    IonCol,
    NgFor,
    WasteBinCardComponent,
    IonRow,
    EsgBannerComponent,
    NearbyRecyclingSectionComponent,
    EsgInfoCardComponent,
  ],
})
export class WastePageComponent implements OnInit {
  todayWasteCollection?: WasteUpcoming;
  nextWasteCollection?: WasteUpcoming;
  collectionAreaId: string | undefined;
  displayConnectivityError = false;
  contentLoadingComplete = true;
  wasteTypes: string[] = [];
  protected wasteService = inject(WasteService);
  private readonly feedUpdateService = inject(FeedUpdateService);
  private readonly router = inject(Router);
  private readonly route = inject(ActivatedRoute);
  private readonly wasteStorageLoaderService = inject(
    WasteStorageLoaderService,
  );
  private readonly wasteReminderService = inject(WasteReminderService);
  private readonly destroyRef = inject(DestroyRef);

  ngOnInit(): void {
    this.subscribeToModuleError();
    this.loadDefaultWasteTypes();
    this.loadCollectionArea();
    this.setupCollectionUpdateSubscription();
    this.updateReminders();
  }

  getUpcomingWasteTypesExcludingToday(): WasteDate[] {
    const uniqueEvents: { [key: string]: WasteDate } = {};
    const defaultTypes = this.wasteTypes;

    const todayWasteTypes = new Set(
      this.todayWasteCollection?.allUpcomingDates?.map(
        (event: { wasteType: any }) => event.wasteType,
      ) || [],
    );

    const allUpcomingDates = this.nextWasteCollection?.allUpcomingDates;

    if (allUpcomingDates) {
      for (const event of allUpcomingDates) {
        if (
          defaultTypes.includes(event.wasteType) &&
          !uniqueEvents[event.wasteType] &&
          !todayWasteTypes.has(event.wasteType)
        ) {
          uniqueEvents[event.wasteType] = event;
        }
      }
    }

    return Object.values(uniqueEvents);
  }

  navigateToSettings() {
    this.router.navigate(['settings'], { relativeTo: this.route });
  }

  getNextCollectionDate(wasteType: string, index: number): string {
    const upcomingDates =
      this.nextWasteCollection?.upcomingDatesByType?.[wasteType];
    return upcomingDates?.[index]?.date ?? '';
  }

  refreshView(event: any) {
    this.refreshWasteCollection();
    event.target.complete();
  }

  async updateReminders() {
    try {
      const preferences = await this.wasteStorageLoaderService.getSettings(
        StorageKeyType.NOTIFICATION,
      );
      if (!preferences) {
        return;
      }

      const notificationStatus = preferences[
        SettingsKeys.notificationStatus
      ] as boolean;
      const wasteUserSettings: WasteUserSettings = {
        wasteCollectionId: preferences[SettingsKeys.collectionAreaId] as string,
        collectionAreaStreet: preferences[
          SettingsKeys.collectionAreaStreet
        ] as string,
        status: notificationStatus,
        residualIndividualAgreement: false,
        setupCompleted: preferences[
          SettingsKeys.notificationSetupCompleted
        ] as boolean,
        reminderTime: preferences[
          SettingsKeys.notificationReminderTime
        ] as string,
        reminderDay: preferences[
          SettingsKeys.notificationReminderDay
        ] as ReminderDay,
        wasteTypes: preferences[
          SettingsKeys.notificationsWasteTypes
        ] as string[],
      };
      await this.wasteReminderService.updateReminders(wasteUserSettings);
    } catch (error) {
      console.error('Error Updating Reminders:', error);
    }
  }

  private setupCollectionUpdateSubscription(): void {
    this.feedUpdateService
      .getFeedUpdatedListener()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(() => {
        this.refreshWasteCollection();
      });
  }

  private fetchWasteCollection(): void {
    if (this.collectionAreaId) {
      this.contentLoadingComplete = false;
      this.wasteService
        .getUpcomingWasteCollections(
          this.collectionAreaId,
          this.wasteTypes,
          startOfToday().toISOString(),
          addDays(startOfToday(), 1).toISOString(),
        )
        .pipe(takeUntilDestroyed(this.destroyRef))
        .subscribe({
          next: (waste: WasteUpcoming) => {
            this.todayWasteCollection = waste;
          },
          error: (error) => {
            console.error('Failed to load today waste collection', error);
          },
        });

      this.wasteService
        .getUpcomingWasteCollections(
          this.collectionAreaId,
          this.wasteTypes,
          addDays(startOfToday(), 2).toISOString(),
        )
        .pipe(
          takeUntilDestroyed(this.destroyRef),
          finalize(() => (this.contentLoadingComplete = true)),
        )
        .subscribe({
          next: (waste: WasteUpcoming) => {
            this.nextWasteCollection = waste;
          },
          error: (error) => {
            console.error('Failed to load next waste collection', error);
          },
        });
    }
  }

  private async loadDefaultWasteTypes() {
    const preferences = await this.wasteStorageLoaderService.getSettings(
      StorageKeyType.NOTIFICATION,
    );
    if (!preferences) {
      return;
    }

    const residualIndividualAgreement = preferences[
      SettingsKeys.residualIndividualAgreement
    ] as boolean;

    if (residualIndividualAgreement) {
      this.wasteTypes =
        this.wasteService.getDefaultWasteTypesForIndividualAgreement();
    } else {
      this.wasteTypes =
        this.wasteService.getDefaultWasteTypesForStandardAgreement();
    }
  }

  private loadCollectionArea() {
    this.wasteService
      .loadCollectionArea()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((res) => {
        this.collectionAreaId = res;
        this.fetchWasteCollection();
      });
  }

  private subscribeToModuleError(): void {
    this.wasteService
      .getWasteModuleErrors()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((errorInfo) => {
        if (errorInfo.errorType === ErrorType.MODULE_API_ERROR) {
          this.displayConnectivityError = errorInfo.hasError;
        } else {
          this.displayConnectivityError = false;
        }
      });
  }

  private refreshWasteCollection(): void {
    this.todayWasteCollection = {
      allUpcomingDates: [],
      upcomingDatesByType: {},
    };
    this.nextWasteCollection = {
      allUpcomingDates: [],
      upcomingDatesByType: {},
    };
    this.loadDefaultWasteTypes();
    this.loadCollectionArea();
    this.fetchWasteCollection();
  }
}
