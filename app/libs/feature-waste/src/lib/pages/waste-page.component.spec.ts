import { ComponentFixture, TestBed } from '@angular/core/testing';
import { WastePageComponent } from './waste-page.component';
import { WasteService } from '../services/waste.service';
import { WasteStorageLoaderService } from '../services/storage-loader/waste-storage-loader.service';
import { MenuController } from '@ionic/angular';
import { WasteReminderService } from '../services/reminder/waste-reminder.service';
import { of } from 'rxjs';
import {
  createUpcomingWaste,
  createWasteModuleInfo,
} from '../shared/testing/waste-test-utils';
import { CoreTestModule, ErrorType } from '@sw-code/urbo-core';
import {
  ESG_ANDROID_STORE_URL,
  ESG_IOS_STORE_URL,
  ESG_WEB_URL,
  WASTE_MODULE_INFO,
} from '../feature-waste.module';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../transloco-testing.module';

describe('WastePage', () => {
  let component: WastePageComponent;
  let fixture: ComponentFixture<WastePageComponent>;
  let mockWasteService: jest.Mocked<WasteService>;
  let mockWasteStorageLoaderService: jest.Mocked<WasteStorageLoaderService>;
  let mockMenuController: jest.Mocked<MenuController>;
  let mockWasteReminderService: jest.Mocked<WasteReminderService>;

  beforeEach(() => {
    mockWasteService = {
      getUpcomingWasteCollections: jest
        .fn()
        .mockReturnValue(of(createUpcomingWaste())) as any,
      getWasteModuleErrors: jest
        .fn()
        .mockReturnValue(
          of({ hasError: true, errorType: ErrorType.MODULE_API_ERROR }),
        ),
      loadCollectionArea: jest
        .fn()
        .mockReturnValue(of('default-collection-area')),
      getDefaultWasteTypes: jest.fn(),
      loadDefaultWasteTypes: jest.fn(),
      getDefaultWasteTypesForIndividualAgreement: jest.fn(),
      getDefaultWasteTypesForStandardAgreement: jest.fn(),
      formatDays: jest.fn(),
    } as jest.Mocked<any>;

    mockWasteReminderService = {
      updateReminders: jest.fn(),
    } as unknown as jest.Mocked<WasteReminderService>;

    mockWasteStorageLoaderService = {
      saveCollectionArea: jest.fn(),
      getSettings: jest.fn().mockResolvedValue({}),
    } as unknown as jest.Mocked<WasteStorageLoaderService>;

    mockMenuController = {
      open: jest.fn(),
      close: jest.fn(),
    } as unknown as jest.Mocked<MenuController>;

    TestBed.configureTestingModule({
      providers: [
        { provide: ESG_IOS_STORE_URL, useValue: '' },
        { provide: ESG_ANDROID_STORE_URL, useValue: '' },
        { provide: ESG_WEB_URL, useValue: '' },
        { provide: WasteService, useValue: mockWasteService },
        {
          provide: WasteStorageLoaderService,
          useValue: mockWasteStorageLoaderService,
        },
        { provide: MenuController, useValue: mockMenuController },
        { provide: WasteReminderService, useValue: mockWasteReminderService },
        {
          provide: WASTE_MODULE_INFO,
          useValue: createWasteModuleInfo,
        },
      ],
      imports: [
        WastePageComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
    });

    fixture = TestBed.createComponent(WastePageComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should fetchWasteData when ngOnit is being called', async () => {
      const expectedWasteItems = createUpcomingWaste();
      component.collectionAreaId = 'default-collection-area';

      component.ngOnInit();
      await fixture.whenStable();

      expect(component.nextWasteCollection).toEqual(expectedWasteItems);
    });

    it('should set collectionAreaId when ngOnit is being called and wasteStorageLoaderService.getSetting returns settings', async () => {
      mockWasteService.loadCollectionArea.mockReturnValue(
        of('default-collection-area'),
      );

      component.ngOnInit();
      await fixture.whenStable();

      expect(component.collectionAreaId).toEqual('default-collection-area');
    });
  });
});
