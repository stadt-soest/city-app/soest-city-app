export type WasteTypeEnum =
  | 'WASTEPAPER'
  | 'YELLOW_BAG'
  | 'BIOWASTE'
  | 'RESIDUAL_WASTE_14DAILY'
  | 'RESIDUAL_WASTE_4WEEKLY'
  | 'DIAPER_WASTE'
  | 'CHRISTMAS_TREE';

export const WasteTypeEnum = {
  Wastepaper: 'WASTEPAPER' as WasteTypeEnum,
  YellowBag: 'YELLOW_BAG' as WasteTypeEnum,
  Biowaste: 'BIOWASTE' as WasteTypeEnum,
  ResidualWaste14Daily: 'RESIDUAL_WASTE_14DAILY' as WasteTypeEnum,
  ResidualWaste4Weekly: 'RESIDUAL_WASTE_4WEEKLY' as WasteTypeEnum,
  DiaperWaste: 'DIAPER_WASTE' as WasteTypeEnum,
  ChristmasTree: 'CHRISTMAS_TREE' as WasteTypeEnum,
};
