import { WasteTypeEnum } from './waste-type.enum';

export interface Waste {
  id: string;
  city: string;
  street: string;
  wasteDates: Array<WasteDate>;
}

export class WasteUpcoming {
  static readonly getDefaultWasteTypes: Array<WasteTypeEnum> = [
    'WASTEPAPER',
    'YELLOW_BAG',
    'BIOWASTE',
    'RESIDUAL_WASTE_14DAILY',
    'CHRISTMAS_TREE',
  ];
  static readonly getDefaultWasteTypesIndividual: Array<WasteTypeEnum> = [
    'WASTEPAPER',
    'YELLOW_BAG',
    'BIOWASTE',
    'RESIDUAL_WASTE_4WEEKLY',
    'CHRISTMAS_TREE',
  ];
  allUpcomingDates: Array<WasteDate> = [];
  upcomingDatesByType?: { [key: string]: Array<WasteDate> };
}

export interface WasteDate {
  id: string;
  date: string;
  wasteType: WasteDate.WasteTypeEnum;
  wasteTypeColor: string;
  wasteTypeImagePath: string;
}

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace WasteDate {
  export type WasteTypeEnum =
    | 'WASTEPAPER'
    | 'YELLOW_BAG'
    | 'BIOWASTE'
    | 'RESIDUAL_WASTE_14DAILY'
    | 'RESIDUAL_WASTE_4WEEKLY'
    | 'DIAPER_WASTE'
    | 'CHRISTMAS_TREE';
  export const WasteTypeEnum = {
    Wastepaper: 'WASTEPAPER' as WasteTypeEnum,
    YellowBag: 'YELLOW_BAG' as WasteTypeEnum,
    Biowaste: 'BIOWASTE' as WasteTypeEnum,
    ResidualWaste14Daily: 'RESIDUAL_WASTE_14DAILY' as WasteTypeEnum,
    ResidualWaste4Weekly: 'RESIDUAL_WASTE_4WEEKLY' as WasteTypeEnum,
    DiaperWaste: 'DIAPER_WASTE' as WasteTypeEnum,
    ChristmasTree: 'CHRISTMAS_TREE' as WasteTypeEnum,
  };
}
