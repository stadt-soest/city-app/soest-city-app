import { ReminderDay } from '../enums/reminder-days.enum';

export interface WasteUserSettings {
  wasteCollectionId: string;
  collectionAreaStreet: string | null;
  status: boolean;
  residualIndividualAgreement: boolean;
  reminderTime: string;
  setupCompleted: boolean;
  reminderDay: ReminderDay;
  wasteTypes: string[];
}
