import { FeedItem } from '@sw-code/urbo-core';
import { WasteDate } from './waste-date.model';

export interface WasteFeedItem extends FeedItem {
  wasteDate: WasteDate;
  nextCollectionDate: string;
}
