import { Observable } from 'rxjs';
import { Waste, WasteUpcoming } from './models/waste-date.model';
import { WasteFeedItem } from './models/WasteFeedItem';
import { PoiWithDistance } from '@sw-code/urbo-core';

export abstract class WasteAdapter {
  abstract getUpcomingWasteCollections(
    street: string,
    wasteTypes: string[],
    startDate?: string,
    endDate?: string,
  ): Observable<WasteUpcoming>;

  abstract getWasteCollection(searchTerm: string): Observable<Waste[]>;

  abstract getUpcomingWasteCollectionsFeed(
    street: string,
    wasteTypes: string[],
    startDate?: string,
    endDate?: string,
  ): Observable<{ content: WasteFeedItem[]; totalRecords: number }>;

  abstract getFilteredCategories(
    sourceCategories: string[],
  ): Observable<string[]>;

  abstract fetchPois(categoryIds: string[]): Observable<PoiWithDistance[]>;

  abstract getWasteRecyclingPoisWithDistance(): Observable<PoiWithDistance[]>;
}
