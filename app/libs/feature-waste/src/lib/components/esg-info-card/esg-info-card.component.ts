import { Component, inject } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { IonCard } from '@ionic/angular/standalone';
import { NgIf } from '@angular/common';
import {
  CustomButtonColorScheme,
  CustomButtonType,
  CustomVerticalButtonComponent,
} from '@sw-code/urbo-ui';
import {
  ESG_ANDROID_STORE_URL,
  ESG_IOS_STORE_URL,
  ESG_WEB_URL,
} from '../../feature-waste.module';
import { BrowserService, DeviceService } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-esg-info-card',
  templateUrl: './esg-info-card.component.html',
  styleUrls: ['./esg-info-card.component.scss'],
  imports: [TranslocoDirective, IonCard, NgIf, CustomVerticalButtonComponent],
})
export class EsgInfoCardComponent {
  iosStoreUrl = inject(ESG_IOS_STORE_URL);
  androidStoreUrl = inject(ESG_ANDROID_STORE_URL);
  esgWebUrl = inject(ESG_WEB_URL);
  externalUrl?: string;
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
  protected deviceService = inject(DeviceService);
  private readonly browserService = inject(BrowserService);

  async openWasteAppUrl(e: Event) {
    e.preventDefault();
    if (this.deviceService.isIOS()) {
      this.externalUrl = this.iosStoreUrl;
      await this.browserService.openInExternalBrowser(this.iosStoreUrl);
    } else if (this.deviceService.isAndroid()) {
      this.externalUrl = this.androidStoreUrl;
      await this.browserService.openInExternalBrowser(this.androidStoreUrl);
    }
  }

  async openEsgUrl(e: Event) {
    e.preventDefault();
    await this.browserService.openInExternalBrowser(this.esgWebUrl);
  }
}
