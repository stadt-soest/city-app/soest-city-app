import { Component, DestroyRef, inject, NgZone, OnInit } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { AsyncPipe, NgFor, NgIf } from '@angular/common';
import { RecyclingCardSkeletonComponent } from './recycling-card/skeleton/recycling-card-skeleton.component';
import { IonCol, Platform } from '@ionic/angular/standalone';
import {
  CustomSelectableIconTextButton,
  NotificationCardComponent,
} from '@sw-code/urbo-ui';
import { RecyclingCardComponent } from './recycling-card/recycling-card.component';
import {
  DeviceService,
  GeolocationService,
  PoiWithDistance,
  SimplifiedPermissionState,
} from '@sw-code/urbo-core';
import { WasteService } from '../../services/waste.service';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'lib-nearby-recycling-section',
  templateUrl: './nearby-recycling-section.component.html',
  styleUrls: ['./nearby-recycling-section.component.scss'],
  imports: [
    TranslocoDirective,
    NgFor,
    RecyclingCardSkeletonComponent,
    NgIf,
    IonCol,
    NotificationCardComponent,
    RecyclingCardComponent,
    AsyncPipe,
  ],
})
export class NearbyRecyclingSectionComponent implements OnInit {
  recyclingPois: PoiWithDistance[] = [];
  loadingRecyclingPois = false;
  protected geolocationService = inject(GeolocationService);
  permissionButton: CustomSelectableIconTextButton[] = [
    {
      icon: 'open_in_new',
      text: 'feature_waste.nearby-recycling.open',
      selected: false,
      applySelectionStyle: false,
      onSelect: async () => await this.geolocationService.requestPermissions(),
    },
  ];
  protected deviceService = inject(DeviceService);
  protected readonly simplifiedPermissionState = SimplifiedPermissionState;
  private readonly wasteService = inject(WasteService);
  private readonly platform = inject(Platform);
  private readonly ngZone = inject(NgZone);
  private readonly destroyRef = inject(DestroyRef);

  constructor() {
    this.platform.resume.subscribe(() => {
      this.ngZone.run(() => {
        this.geolocationService.initializePermissionStatus();
      });
    });
  }

  ngOnInit(): void {
    this.setupPermissionStateSubscription();
  }

  private setupPermissionStateSubscription(): void {
    this.geolocationService.permissionState$.subscribe((permissionState) => {
      if (permissionState === SimplifiedPermissionState.granted) {
        this.fetchWasteRecyclingPois();
      }
    });
  }

  private fetchWasteRecyclingPois(): void {
    this.loadingRecyclingPois = true;
    this.wasteService
      .getWasteRecyclingPoisWithDistance()
      .pipe(
        takeUntilDestroyed(this.destroyRef),
        finalize(() => (this.loadingRecyclingPois = false)),
      )
      .subscribe({
        next: (recyclingPois) => {
          this.recyclingPois = recyclingPois;
        },
        error: (error) => {
          console.error('Failed to load waste container', error);
        },
      });
  }
}
