import { Component, inject, Input } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  IonCard,
  IonCardSubtitle,
  IonCardTitle,
  IonCol,
  IonGrid,
  IonRow,
} from '@ionic/angular/standalone';
import { NgIf } from '@angular/common';
import {
  CustomSelectableIconTextButtonComponent,
  DistanceFormatPipe,
} from '@sw-code/urbo-ui';
import { MapsNavigationService, PoiWithDistance } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-recycling-card',
  templateUrl: './recycling-card.component.html',
  styleUrls: ['./recycling-card.component.scss'],
  imports: [
    TranslocoDirective,
    IonCard,
    IonGrid,
    IonRow,
    IonCol,
    IonCardTitle,
    NgIf,
    IonCardSubtitle,
    CustomSelectableIconTextButtonComponent,
    DistanceFormatPipe,
  ],
})
export class RecyclingCardComponent {
  @Input({ required: true }) recyclingPoi!: PoiWithDistance;
  distance: number | undefined;
  private readonly mapsNavigationService = inject(MapsNavigationService);

  async openNavigation(event: Event) {
    this.stopEventPropagationAndDefault(event);
    await this.mapsNavigationService.navigateToLocation(
      this.recyclingPoi.poi.location.coordinates.latitude,
      this.recyclingPoi.poi.location.coordinates.longitude,
    );
  }

  private stopEventPropagationAndDefault(event: Event) {
    event.preventDefault();
    event.stopPropagation();
  }
}
