import { Component } from '@angular/core';
import {
  IonCard,
  IonCardSubtitle,
  IonCardTitle,
  IonCol,
  IonGrid,
  IonRow,
  IonSkeletonText,
} from '@ionic/angular/standalone';

@Component({
  selector: 'lib-recycling-card-skeleton',
  templateUrl: './recycling-card-skeleton.component.html',
  styleUrls: ['./recycling-card-skeleton.component.scss'],
  imports: [
    IonCard,
    IonGrid,
    IonRow,
    IonCol,
    IonCardTitle,
    IonSkeletonText,
    IonCardSubtitle,
  ],
})
export class RecyclingCardSkeletonComponent {}
