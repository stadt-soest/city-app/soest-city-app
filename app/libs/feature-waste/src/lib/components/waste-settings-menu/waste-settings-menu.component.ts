import {
  Component,
  DestroyRef,
  inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import {
  IonCol,
  IonDatetime,
  IonDatetimeButton,
  IonGrid,
  IonModal,
  IonRadio,
  IonRadioGroup,
  IonRow,
  IonSearchbar,
  IonSpinner,
} from '@ionic/angular/standalone';
import { debounceTime, distinctUntilChanged, finalize } from 'rxjs/operators';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  CheckboxSettingComponent,
  ConnectivityErrorCardComponent,
} from '@sw-code/urbo-ui';
import { NgClass, NgFor, NgIf } from '@angular/common';
import { Waste, WasteDate } from '../../models/waste-date.model';
import { ReminderDay } from '../../enums/reminder-days.enum';
import { WasteService } from '../../services/waste.service';
import { WasteStorageLoaderService } from '../../services/storage-loader/waste-storage-loader.service';
import {
  AlertService,
  ConnectivityService,
  DeviceService,
  FeedUpdateService,
  StorageKeyType,
} from '@sw-code/urbo-core';
import { WasteReminderService } from '../../services/reminder/waste-reminder.service';
import { LocalNotifications } from '@capacitor/local-notifications';
import { SettingsKeys } from '../../enums/settings.keys.enums';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { Observable, of, switchMap } from 'rxjs';
import { WasteUserSettings } from '../../models/waste-user-settings.interface';

@Component({
  selector: 'lib-waste-settings-menu',
  templateUrl: './waste-settings-menu.component.html',
  styleUrls: ['./waste-settings-menu.component.scss'],
  imports: [
    TranslocoDirective,
    IonGrid,
    NgIf,
    IonCol,
    ConnectivityErrorCardComponent,
    IonRow,
    IonSearchbar,
    FormsModule,
    ReactiveFormsModule,
    IonSpinner,
    NgFor,
    NgClass,
    IonRadioGroup,
    IonRadio,
    IonDatetimeButton,
    IonModal,
    IonDatetime,
    CheckboxSettingComponent,
  ],
  providers: [AlertService],
})
export class WasteSettingsMenuComponent implements OnInit {
  @ViewChild(IonSearchbar) searchbar!: IonSearchbar;
  @ViewChild('notificationCheckbox')
  notificationCheckbox!: CheckboxSettingComponent;
  notificationsEnabled = false;
  searchControl = new FormControl();
  wasteTypeForm: FormGroup = new FormGroup({});
  searchResults: Waste[] = [];
  hasSearchResults = false;
  searchPerformed = false;
  isLoadingSearch = false;
  collectionAreaId = '';
  collectionStreet: string | null = null;
  wasteTypes: string[] = [];
  selectedWasteTypes: string[] = [];
  dateTimeBefore = '18:00';
  datetimeSameDay = '05:00';
  dayBefore: ReminderDay = ReminderDay.dayBefore;
  wasteTypeErrored = false;
  searchErrored = false;
  savingLocalStorageErrored = false;
  isConnected = true;
  notificationSetupCompleted = false;
  residualIndividualAgreement = false;
  protected readonly reminderDay = ReminderDay;
  private readonly wasteService = inject(WasteService);
  private readonly wasteStorageLoaderService = inject(
    WasteStorageLoaderService,
  );
  private readonly feedUpdateService = inject(FeedUpdateService);
  private readonly wasteReminderService = inject(WasteReminderService);
  private readonly alertService = inject(AlertService);
  private readonly connectivityService = inject(ConnectivityService);
  private readonly deviceService = inject(DeviceService);
  private readonly destroyRef = inject(DestroyRef);

  get showNotificationSettings(): boolean {
    return (
      this.deviceService.isNativePlatform() ||
      this.deviceService.isPWA() ||
      (this.deviceService.isWebPlatform() &&
        !this.deviceService.isMobileBrowser())
    );
  }

  ngOnInit(): void {
    this.setOnlineStatus();
    this.initializeComponentSettings();
  }

  async updateNotifications() {
    await this.wasteReminderService.updateReminders(
      this.getWasteUserSettings(),
    );
  }

  async onDateTimeChange(newReminderDay: ReminderDay) {
    this.dayBefore = newReminderDay;
    await this.updateNotifications();
  }

  async updateIndividualAgreement(ev: any) {
    this.residualIndividualAgreement = ev.detail.value;
    await this.loadWasteTypes();
    await this.saveWasteSettings(this.getWasteUserSettings());
    this.feedUpdateService.notifyFeedUpdate();
    await this.updateNotifications();
  }

  async saveStreetToLocalStorage(streetId: string, streetName: string) {
    try {
      await this.saveWasteSettings(
        this.getWasteUserSettings(
          this.notificationsEnabled,
          streetId,
          streetName,
        ),
      );
      await this.onAreaSettingsUpdated(streetId, streetName);
    } catch (error) {
      this.resetSearchState();
      this.initializeComponentSettings();
      this.savingLocalStorageErrored = true;
      console.error('Error saving collection area:', error);
    }
  }

  async toggleNotifications(): Promise<void> {
    if (this.notificationCheckbox.checked) {
      const status = await LocalNotifications.checkPermissions();
      if (status.display === 'granted') {
        await this.setNotificationStatus(true);
      } else if (status.display === 'denied') {
        await this.alertService.showNotificationPermissionAlert();
        this.notificationCheckbox.checked = false;
      } else {
        const permission = await LocalNotifications.requestPermissions();
        if (permission.display === 'granted') {
          await this.setNotificationStatus(true);
        } else {
          await this.setNotificationStatus(false);
          this.notificationCheckbox.checked = false;
        }
      }
    } else {
      await this.setNotificationStatus(false);
      this.notificationCheckbox.checked = false;
    }
  }

  onWasteTypeCheckedChange(wasteType: string, isChecked: boolean): void {
    this.wasteTypeForm.controls[wasteType].setValue(isChecked);
    this.updateNotifications();
  }

  private initializeComponentSettings(): void {
    this.initializeNotificationSettings().then(async (r) => {
      await this.loadWasteTypes();
      this.initSearchControl();
    });
  }

  private async setOnlineStatus() {
    this.isConnected = await this.connectivityService.checkOnlineStatus();
  }

  private async initializeNotificationSettings() {
    await this.wasteStorageLoaderService
      .getSettings(StorageKeyType.NOTIFICATION)
      .then((preferences) => {
        if (preferences) {
          this.collectionAreaId = preferences[
            SettingsKeys.collectionAreaId
          ] as string;
          this.collectionStreet = preferences[
            SettingsKeys.collectionAreaStreet
          ] as string;
          this.notificationsEnabled = preferences[
            SettingsKeys.notificationStatus
          ] as boolean;
          this.dayBefore = preferences[
            SettingsKeys.notificationReminderDay
          ] as ReminderDay;
          this.notificationSetupCompleted = preferences[
            SettingsKeys.notificationSetupCompleted
          ] as boolean;
          this.residualIndividualAgreement = preferences[
            SettingsKeys.residualIndividualAgreement
          ] as boolean;

          const reminderTime = preferences[
            SettingsKeys.notificationReminderTime
          ] as string;
          if (this.dayBefore === ReminderDay.dayBefore) {
            this.dateTimeBefore = reminderTime;
          } else if (this.dayBefore === ReminderDay.sameDay) {
            this.datetimeSameDay = reminderTime;
          }

          const wasteTypes = preferences[
            SettingsKeys.notificationsWasteTypes
          ] as string[];
          if (Array.isArray(wasteTypes)) {
            this.selectedWasteTypes = wasteTypes;
          }
        }
      });
  }

  private initSearchControl(): void {
    this.searchControl.valueChanges
      .pipe(
        takeUntilDestroyed(this.destroyRef),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap((value) => this.valueChanged(value)),
      )
      .subscribe({
        next: (response) => {
          if (response && response.length !== 0) {
            this.handleSearchResults(response);
          }
        },
        error: (error) => this.searchError(error),
      });
  }

  private valueChanged(value: string): Observable<Waste[]> {
    if (!value) {
      this.resetSearchState();
      return of([]);
    } else {
      this.isLoadingSearch = true;
      return this.wasteService
        .getWasteCollection(value.trim())
        .pipe(finalize(() => (this.isLoadingSearch = false)));
    }
  }

  private resetSearchState(): void {
    this.searchResults = [];
    this.searchbar.value = '';
    this.searchPerformed = false;
    this.hasSearchResults = false;
    this.isLoadingSearch = false;
    this.searchErrored = false;
  }

  private handleSearchResults(response: Waste[]): void {
    this.searchResults = response;
    this.hasSearchResults = this.searchResults.length > 0;
    this.searchPerformed = true;
    this.isLoadingSearch = false;
  }

  private searchError(error: any): void {
    console.error('Search failed:', error);
    this.searchErrored = true;
    this.isLoadingSearch = false;
  }

  private async loadWasteTypes() {
    if (!this.collectionAreaId) {
      return;
    }

    try {
      this.wasteTypes = [];
      this.wasteTypes =
        await this.wasteService.getDefaultWasteTypesByAgreement();
      this.initializeWasteTypeForm();
    } catch (error) {
      console.error('Error loading waste types:', error);
    }
  }

  private initializeWasteTypeForm(): void {
    const shouldActivateAll =
      this.selectedWasteTypes.length === 0 && !this.notificationSetupCompleted;

    this.wasteTypes.forEach((type) => {
      const isActivated =
        shouldActivateAll || this.selectedWasteTypes.includes(type);
      this.wasteTypeForm.addControl(type, new FormControl(isActivated));
    });
  }

  private getActivatedWasteTypes(): string[] {
    const filterType = this.residualIndividualAgreement
      ? WasteDate.WasteTypeEnum.ResidualWaste14Daily
      : WasteDate.WasteTypeEnum.ResidualWaste4Weekly;

    return Object.entries(this.wasteTypeForm.value)
      .filter(([key, value]) => value && key !== filterType)
      .map(([key]) => key);
  }

  private async onAreaSettingsUpdated(streetId: string, streetName: string) {
    this.resetSearchState();
    this.initializeComponentSettings();
    this.feedUpdateService.notifyFeedUpdate();
    if (this.notificationsEnabled) {
      await this.wasteReminderService.updateReminders(
        this.getWasteUserSettings(
          this.notificationsEnabled,
          streetId,
          streetName,
        ),
      );
    }
  }

  private async setNotificationStatus(enabled: boolean) {
    this.notificationsEnabled = enabled;
    this.notificationSetupCompleted = true;
    const wasteUserSettings = this.getWasteUserSettings(enabled);
    if (enabled) {
      const reminderSettings = this.getWasteUserSettings(true);
      this.dateTimeBefore = reminderSettings.reminderTime;
      this.dayBefore = reminderSettings.reminderDay;
      await this.wasteReminderService.updateReminders(reminderSettings);
    } else {
      await this.saveWasteSettings(wasteUserSettings);
      await this.wasteReminderService.cancelExistingReminders();
    }
  }

  private async saveWasteSettings(wasteUserSettings: WasteUserSettings) {
    await this.wasteStorageLoaderService.saveWasteSettings(wasteUserSettings);
  }

  private getWasteUserSettings(
    status?: boolean,
    streetId?: string,
    streetName?: string,
  ): WasteUserSettings {
    return {
      wasteCollectionId: streetId ?? this.collectionAreaId,
      collectionAreaStreet: streetName ?? this.collectionStreet,
      status: status ?? this.notificationsEnabled,
      residualIndividualAgreement: this.residualIndividualAgreement ?? false,
      setupCompleted: this.notificationSetupCompleted,
      reminderTime:
        this.dayBefore === ReminderDay.dayBefore
          ? this.dateTimeBefore
          : this.datetimeSameDay,
      reminderDay: this.dayBefore,
      wasteTypes: this.getActivatedWasteTypes(),
    };
  }
}
