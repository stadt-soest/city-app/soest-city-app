import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { WasteSettingsMenuComponent } from './waste-settings-menu.component';
import { WasteService } from '../../services/waste.service';
import { WasteStorageLoaderService } from '../../services/storage-loader/waste-storage-loader.service';
import { WasteReminderService } from '../../services/reminder/waste-reminder.service';
import {
  ConnectivityService,
  CoreTestModule,
  DeviceService,
} from '@sw-code/urbo-core';
import { of } from 'rxjs';
import { SettingsKeys } from '../../enums/settings.keys.enums';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../transloco-testing.module';

describe('WasteSettingsMenuComponent', () => {
  let component: WasteSettingsMenuComponent;
  let fixture: ComponentFixture<WasteSettingsMenuComponent>;
  let mockWasteService: jest.Mocked<WasteService>;
  let mockWasteStorageLoaderService: jest.Mocked<WasteStorageLoaderService>;
  let mockWasteReminderService: jest.Mocked<WasteReminderService>;
  let mockConnectivityService: jest.Mocked<ConnectivityService>;
  let mockDeviceService: jest.Mocked<DeviceService>;

  beforeEach(waitForAsync(() => {
    mockWasteService = {
      getWasteCollection: jest.fn(),
      getWasteTypes: jest.fn().mockReturnValue(of([])),
      getUpcomingWasteCollections: jest.fn().mockReturnValue(of({})),
    } as unknown as jest.Mocked<WasteService>;

    mockConnectivityService = {
      checkOnlineStatus: jest.fn().mockResolvedValue(Promise.resolve(true)),
    } as unknown as jest.Mocked<ConnectivityService>;

    mockDeviceService = {
      isMobileBrowser: jest.fn().mockReturnValue(false),
      isPWA: jest.fn().mockReturnValue(false),
      isNativePlatform: jest.fn().mockReturnValue(true),
    } as unknown as jest.Mocked<DeviceService>;

    mockWasteStorageLoaderService = {
      saveCollectionArea: jest.fn(),
      getSettings: jest.fn().mockResolvedValueOnce({
        [SettingsKeys.collectionAreaId]: '123',
        [SettingsKeys.collectionAreaStreet]: 'Test Street',
      }),
    } as unknown as jest.Mocked<WasteStorageLoaderService>;

    mockWasteReminderService = {
      updateReminders: jest.fn(),
      cancelExistingReminders: jest.fn(),
    } as unknown as jest.Mocked<WasteReminderService>;

    TestBed.configureTestingModule({
      imports: [
        WasteSettingsMenuComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        { provide: WasteService, useValue: mockWasteService },
        {
          provide: WasteStorageLoaderService,
          useValue: mockWasteStorageLoaderService,
        },
        {
          provide: WasteReminderService,
          useValue: mockWasteReminderService,
        },
        {
          provide: ConnectivityService,
          useValue: mockConnectivityService,
        },
        {
          provide: DeviceService,
          useValue: mockDeviceService,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(WasteSettingsMenuComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
