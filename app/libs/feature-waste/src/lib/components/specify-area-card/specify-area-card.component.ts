import { Component, EventEmitter, Output } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { IonCard } from '@ionic/angular/standalone';
import {
  CustomButtonColorScheme,
  CustomButtonType,
  CustomVerticalButtonComponent,
} from '@sw-code/urbo-ui';

@Component({
  selector: 'lib-specify-area-card',
  templateUrl: './specify-area-card.component.html',
  styleUrls: ['./specify-area-card.component.scss'],
  imports: [TranslocoDirective, IonCard, CustomVerticalButtonComponent],
})
export class SpecifyAreaCardComponent {
  @Output() settingsMenuRequested = new EventEmitter<void>();
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;

  openSettingsMenu() {
    this.settingsMenuRequested.emit();
  }
}
