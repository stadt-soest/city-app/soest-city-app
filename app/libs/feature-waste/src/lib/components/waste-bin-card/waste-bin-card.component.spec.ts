import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { WasteBinCardComponent } from './waste-bin-card.component';
import { WasteService } from '../../services/waste.service';
import { UITestModule } from '@sw-code/urbo-ui';
import { CoreTestModule } from '@sw-code/urbo-core';
import { getTranslocoModule } from '../../../transloco-testing.module';
import { WasteDate } from '../../models/waste-date.model';
import { advanceTo } from 'jest-date-mock';

describe('WasteBinCardComponent', () => {
  let component: WasteBinCardComponent;
  let fixture: ComponentFixture<WasteBinCardComponent>;
  let mockWasteService: jest.Mocked<WasteService>;

  beforeEach(waitForAsync(() => {
    mockWasteService = {
      localizedDistanceUntilNextPickup: jest.fn(),
    } as any;

    TestBed.configureTestingModule({
      imports: [
        WasteBinCardComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [{ provide: WasteService, useValue: mockWasteService }],
    }).compileComponents();

    fixture = TestBed.createComponent(WasteBinCardComponent);
    component = fixture.componentInstance;
    component.wasteDate = {
      id: '1',
      date: '2023-07-05',
      wasteType: WasteDate.WasteTypeEnum.Biowaste,
      wasteTypeColor: '#000000',
      wasteTypeImagePath: 'path/to/image',
    };
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('isCollectionTomorrow', () => {
    it('should return true when this.wasteDate.date is tomorrow', () => {
      advanceTo(new Date('2023-07-04T12:00:00'));
      expect(component.isCollectionTomorrow).toBe(true);
    });

    it('should return false when this.waste.date is not tomorrow', () => {
      advanceTo(new Date('2023-07-05T12:00:00'));
      expect(component.isCollectionTomorrow).toBe(false);
    });
  });
});
