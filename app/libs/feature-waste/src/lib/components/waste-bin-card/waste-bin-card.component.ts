import { Component, inject, Input } from '@angular/core';
import { isToday, isTomorrow } from 'date-fns';
import { TranslocoDirective } from '@jsverse/transloco';
import { IonCard, IonCol, IonGrid, IonRow } from '@ionic/angular/standalone';
import { NgIf, NgStyle } from '@angular/common';
import {
  LocalizedFormatDistanceTimePipePipe,
  LocalizedWeekDayPipe,
} from '@sw-code/urbo-ui';
import { WasteFeedItem } from '../../models/WasteFeedItem';
import { WasteDate } from '../../models/waste-date.model';
import { WasteService } from '../../services/waste.service';
import { AbstractFeedCardComponent } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-waste-bin-card',
  templateUrl: './waste-bin-card.component.html',
  styleUrls: ['./waste-bin-card.component.scss'],
  imports: [
    TranslocoDirective,
    IonCard,
    IonGrid,
    IonRow,
    IonCol,
    NgIf,
    NgStyle,
    LocalizedWeekDayPipe,
    LocalizedFormatDistanceTimePipePipe,
  ],
})
export class WasteBinCardComponent extends AbstractFeedCardComponent<WasteFeedItem> {
  @Input({ required: true }) wasteDate!: WasteDate;
  @Input({ required: true }) nextCollectionDate = '';
  @Input() elevation: 'subtle-elevation' | 'strong-elevation' =
    'subtle-elevation';
  private readonly wasteService = inject(WasteService);

  get isCollectionTomorrow(): boolean {
    return isTomorrow(this.wasteDate.date);
  }

  get isCollectionToday(): boolean {
    return isToday(this.wasteDate.date);
  }

  get localizedDistanceUntilNextPickup(): number {
    return this.wasteService.localizedDistanceUntilNextPickup(
      this.nextCollectionDate,
      this.wasteDate,
    );
  }
}
