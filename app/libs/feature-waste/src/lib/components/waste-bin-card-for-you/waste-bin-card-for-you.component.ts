import { Component, Input, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import { WasteBinCardComponent } from '../waste-bin-card/waste-bin-card.component';
import { WasteFeedItem } from '../../models/WasteFeedItem';
import { WasteDate } from '../../models/waste-date.model';
import { AbstractFeedCardComponent } from '@sw-code/urbo-core';
import { IonRouterLinkWithHref } from '@ionic/angular/standalone';

@Component({
  selector: 'lib-waste-bin-card-for-you',
  template: `
    <a [routerLink]="['./', 'waste']">
      <lib-waste-bin-card
        [nextCollectionDate]="nextCollectionDate"
        [wasteDate]="wasteDate"
        elevation="strong-elevation"
      >
      </lib-waste-bin-card>
    </a>
  `,
  imports: [IonRouterLinkWithHref, RouterLink, WasteBinCardComponent],
})
export class WasteBinCardForYouComponent
  extends AbstractFeedCardComponent<WasteFeedItem>
  implements OnInit
{
  @Input({ required: true }) wasteDate!: WasteDate;
  @Input({ required: true }) nextCollectionDate!: string;

  ngOnInit() {
    if (this.feedItem) {
      this.wasteDate = this.feedItem?.wasteDate;
      this.nextCollectionDate = this.feedItem?.nextCollectionDate;
    }
  }
}
