import { Component } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { IonCol } from '@ionic/angular/standalone';

@Component({
  selector: 'lib-esg-banner',
  templateUrl: './esg-banner.component.html',
  styleUrls: ['./esg-banner.component.scss'],
  imports: [TranslocoDirective, IonCol],
})
export class EsgBannerComponent {}
