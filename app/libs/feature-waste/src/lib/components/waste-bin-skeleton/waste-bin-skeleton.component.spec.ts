import { ComponentFixture, TestBed } from '@angular/core/testing';
import { WasteBinSkeletonComponent } from './waste-bin-skeleton.component';
import { UITestModule } from '@sw-code/urbo-ui';
import { CoreTestModule } from '@sw-code/urbo-core';

describe('WasteBinSkeletonComponent', () => {
  let component: WasteBinSkeletonComponent;
  let fixture: ComponentFixture<WasteBinSkeletonComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        WasteBinSkeletonComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(WasteBinSkeletonComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
