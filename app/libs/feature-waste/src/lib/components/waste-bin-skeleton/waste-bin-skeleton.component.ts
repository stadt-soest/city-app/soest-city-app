import { Component } from '@angular/core';
import {
  IonCard,
  IonCol,
  IonGrid,
  IonRow,
  IonSkeletonText,
} from '@ionic/angular/standalone';
import { NgFor } from '@angular/common';
import { TranslocoPipe } from '@jsverse/transloco';

@Component({
  selector: 'lib-waste-bin-skeleton',
  templateUrl: './waste-bin-skeleton.component.html',
  styleUrls: ['./waste-bin-skeleton.component.scss'],
  imports: [
    IonCard,
    IonGrid,
    IonRow,
    IonCol,
    IonSkeletonText,
    NgFor,
    TranslocoPipe,
  ],
})
export class WasteBinSkeletonComponent {}
