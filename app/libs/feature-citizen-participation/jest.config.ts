export default {
  displayName: 'feature-citizen-participation',
  preset: '../../jest.preset.js',
  setupFilesAfterEnv: ['<rootDir>/src/test-setup.ts'],
  coverageDirectory: '../../coverage/libs/feature-citizen-participation',
};
