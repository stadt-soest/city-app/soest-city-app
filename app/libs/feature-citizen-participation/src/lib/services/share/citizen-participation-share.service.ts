import { inject, Injectable } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';
import { BaseShareService } from '@sw-code/urbo-core';

@Injectable()
export class CitizenParticipationShareService {
  private readonly baseShareService = inject(BaseShareService);
  private readonly translateService = inject(TranslocoService);

  async shareDefectIdeasReport(): Promise<void> {
    const title = this.translateService.translate(
      'feature_citizen_participation.dashboard.DefectsIdeas',
    );
    const url = window.location.href;
    const text = this.translateService.translate(
      'feature_citizen_participation.dashboard.DefectIdeasDescription',
      {
        url,
      },
    );

    await this.baseShareService.share(title, text, url);
  }

  async shareUrbanDevelopment(): Promise<void> {
    const title = this.translateService.translate(
      'feature_citizen_participation.dashboard.urbanDevelopmentPlans',
    );
    const url = window.location.href;
    const text = this.translateService.translate(
      'feature_citizen_participation.dashboard.urbanDevelopmentPlansDescription',
      {
        url,
      },
    );

    await this.baseShareService.share(title, text, url);
  }
}
