import { TestBed } from '@angular/core/testing';
import { CitizenParticipationShareService } from './citizen-participation-share.service';
import { BaseShareService } from '@sw-code/urbo-core';
import { TranslocoService } from '@jsverse/transloco';

describe('CitizenParticipationShareService', () => {
  let service: CitizenParticipationShareService;
  let mockBaseShareService: jest.Mocked<BaseShareService>;
  let mockTranslocoService: jest.Mocked<TranslocoService>;

  beforeEach(() => {
    mockBaseShareService = {
      share: jest.fn(),
    } as unknown as jest.Mocked<BaseShareService>;

    mockTranslocoService = {
      translate: jest.fn(),
    } as unknown as jest.Mocked<TranslocoService>;

    TestBed.configureTestingModule({
      providers: [
        CitizenParticipationShareService,
        { provide: BaseShareService, useValue: mockBaseShareService },
        { provide: TranslocoService, useValue: mockTranslocoService },
      ],
    });

    service = TestBed.inject(CitizenParticipationShareService);
  });

  it('should call baseShareService.share with the correct arguments', async () => {
    const translatedText = 'Report a defect';
    mockTranslocoService.translate.mockReturnValue(translatedText);

    await service.shareDefectIdeasReport();

    expect(mockBaseShareService.share).toHaveBeenCalledWith(
      'Report a defect',
      translatedText,
      window.location.href,
    );
  });

  it('should use the translation service to get share text', async () => {
    await service.shareDefectIdeasReport();

    expect(mockTranslocoService.translate).toHaveBeenCalledWith(
      'feature_citizen_participation.dashboard.DefectsIdeas',
    );
    expect(mockTranslocoService.translate).toHaveBeenCalledWith(
      'feature_citizen_participation.dashboard.DefectIdeasDescription',
      { url: window.location.href },
    );
  });
});
