import { ModuleCategory, ModuleInfo } from '@sw-code/urbo-core';

export const mockCitizenParticipationModuleInfo: ModuleInfo = {
  id: 'citizen-participation',
  name: 'Bürgerbeteiligung',
  category: ModuleCategory.yourCity,
  icon: 'flag',
  baseRoutingPath: '/citizen-participation',
  defaultVisibility: true,
  settingsPageAvailable: false,
  isAboutAppItself: false,
  relevancy: 0.5,
  forYouRelevant: false,
  showInCategoriesPage: false,
  orderInCategory: 1,
};
