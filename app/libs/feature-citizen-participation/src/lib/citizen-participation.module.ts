import {
  Inject,
  InjectionToken,
  ModuleWithProviders,
  NgModule,
  inject,
  provideAppInitializer,
} from '@angular/core';
import { from, of } from 'rxjs';
import { provideTranslocoScope } from '@jsverse/transloco';
import {
  FeatureModule,
  ModuleCategory,
  ModuleInfo,
  ModuleRegistryService,
} from '@sw-code/urbo-core';

export const CITIZEN_PARTICIPATION_MODULE_INFO = new InjectionToken<ModuleInfo>(
  'CitizenParticipationModuleInfoHolderToken',
);
export const URBAN_DEVELOPMENT_URL = new InjectionToken<string>(
  'URBAN_DEVELOPMENT_URL',
);
export const DEFECT_IDEAS_REPORT_URL = new InjectionToken<string>(
  'DEFECT_IDEAS_REPORT_URL',
);
const moduleId = 'citizen-participation';

@NgModule({
  providers: [
    provideTranslocoScope({
      scope: 'feature-citizen-participation',
      alias: 'feature_citizen_participation',
      loader: {
        en: () => import('./i18n/en.json'),
        de: () => import('./i18n/de.json'),
      },
    }),
    {
      provide: CITIZEN_PARTICIPATION_MODULE_INFO,
      useValue: {
        id: moduleId,
        name: 'feature_citizen_participation.module_name',
        category: ModuleCategory.yourCity,
        icon: 'forum',
        baseRoutingPath: moduleId,
        defaultVisibility: true,
        settingsPageAvailable: false,
        isAboutAppItself: false,
        relevancy: 0.5,
        forYouRelevant: false,
        showInCategoriesPage: true,
        orderInCategory: 2,
      },
    },
  ],
})
export class CitizenParticipationModule {
  readonly featureModule: FeatureModule<null>;

  constructor(
    @Inject(CITIZEN_PARTICIPATION_MODULE_INFO)
    private readonly citizenParticipationModuleInfo: ModuleInfo,
  ) {
    this.featureModule = {
      feedables: () => of({ items: [], totalRecords: 0 }),
      feedablesUnfiltered: () => from([]),
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      initializeDefaultSettings: () => {},
      info: this.citizenParticipationModuleInfo,
      routes: [
        {
          path: `categories/${moduleId}`,
          loadComponent: () =>
            import('./pages/citizen-participation-page.component').then(
              (m) => m.CitizenParticipationPageComponent,
            ),
          title: 'feature_citizen_participation.dashboard.main_headings',
        },
      ],
    };
  }

  static forRoot(): ModuleWithProviders<CitizenParticipationModule> {
    return {
      ngModule: CitizenParticipationModule,
      providers: [
        provideAppInitializer(() => {
          const initializerFn = (
            (
              registry: ModuleRegistryService,
              module: CitizenParticipationModule,
            ) =>
            () =>
              registry.registerFeature(module.featureModule)
          )(inject(ModuleRegistryService), inject(CitizenParticipationModule));
          return initializerFn();
        }),
      ],
    };
  }
}
