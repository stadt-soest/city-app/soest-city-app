import { CitizenParticipationPageComponent } from './citizen-participation-page.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  CITIZEN_PARTICIPATION_MODULE_INFO,
  DEFECT_IDEAS_REPORT_URL,
  URBAN_DEVELOPMENT_URL,
} from '../citizen-participation.module';
import { mockCitizenParticipationModuleInfo } from '../shared/testing/citizen-participation-test-utils';
import { CitizenParticipationShareService } from '../services/share/citizen-participation-share.service';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../transloco-testing.module';

describe('CitizenParticipationReportPage', () => {
  let component: CitizenParticipationPageComponent;
  let fixture: ComponentFixture<CitizenParticipationPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: CITIZEN_PARTICIPATION_MODULE_INFO,
          useValue: mockCitizenParticipationModuleInfo,
        },
        {
          provide: URBAN_DEVELOPMENT_URL,
          useValue: '',
        },
        {
          provide: DEFECT_IDEAS_REPORT_URL,
          useValue: '',
        },
        CitizenParticipationShareService,
      ],
      imports: [
        CitizenParticipationPageComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
    });

    fixture = TestBed.createComponent(CitizenParticipationPageComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
