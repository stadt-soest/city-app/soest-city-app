import { Component, inject } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  ActionButtonBarComponent,
  CondensedHeaderLayoutComponent,
  CustomButtonColorScheme,
  CustomButtonType,
  CustomVerticalButtonComponent,
} from '@sw-code/urbo-ui';
import { IonGrid, IonRow } from '@ionic/angular/standalone';
import { CitizenParticipationShareService } from '../services/share/citizen-participation-share.service';
import {
  DEFECT_IDEAS_REPORT_URL,
  URBAN_DEVELOPMENT_URL,
} from '../citizen-participation.module';
import { BrowserService, DeviceService } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-citizen-participation',
  templateUrl: './citizen-participation-page.component.html',
  styleUrls: ['./citizen-participation-page.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    IonGrid,
    IonRow,
    CustomVerticalButtonComponent,
    ActionButtonBarComponent,
  ],
  providers: [CitizenParticipationShareService],
})
export class CitizenParticipationPageComponent {
  defectIdeasReportUrl = inject(DEFECT_IDEAS_REPORT_URL);
  urbanDevelopmentUrl = inject(URBAN_DEVELOPMENT_URL);
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
  private readonly browserService = inject(BrowserService);
  private readonly deviceService = inject(DeviceService);
  private readonly shareService = inject(CitizenParticipationShareService);

  async openCitizenParticipation(event: Event, url: string) {
    event.preventDefault();
    if (this.deviceService.isWebPlatform()) {
      await this.browserService.openInExternalBrowser(url);
    } else {
      await this.browserService.openInAppBrowser(url);
    }
  }

  async openInBrowser(url: string) {
    await this.browserService.openInExternalBrowser(url);
  }

  async shareDefectIdeasReport() {
    await this.shareService.shareDefectIdeasReport();
  }

  async shareUrbanDevelopment() {
    await this.shareService.shareUrbanDevelopment();
  }
}
