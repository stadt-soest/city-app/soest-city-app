import { InjectionToken } from '@angular/core';

export const IOS_STORE_URL = new InjectionToken<string>('IOS_STORE_URL');
export const ANDROID_STORE_URL = new InjectionToken<string>(
  'ANDROID_STORE_URL',
);
export const SIGNING_IMAGE_URL = new InjectionToken<string>(
  'SIGNING_IMAGE_URL',
);
