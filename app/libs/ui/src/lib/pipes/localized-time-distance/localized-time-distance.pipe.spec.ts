import { firstValueFrom } from 'rxjs';
import { LocalizedTimeDistancePipePipe } from './localized-time-distance.pipe';
import { TestBed } from '@angular/core/testing';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '../../test/ui-test.module';
import { getTranslocoModule } from '../../../transloco-testing.module';
import { TranslationService } from '@sw-code/urbo-core';
import { MockTranslationService } from '../../shared/test/mock-translation-service';
import { advanceTo } from 'jest-date-mock';

describe('LocalizedTimeDistancePipe', () => {
  let pipe: LocalizedTimeDistancePipePipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        LocalizedTimeDistancePipePipe,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        LocalizedTimeDistancePipePipe,
        { provide: TranslationService, useClass: MockTranslationService },
      ],
    });

    pipe = TestBed.inject(LocalizedTimeDistancePipePipe);
  });

  it('should format relative time from Date object', async () => {
    const now = new Date();
    const result = await firstValueFrom(pipe.transform(now));

    expect(result).toBe('just now');
  });

  it('should format relative time from DateTimePair object', async () => {
    advanceTo(new Date('2023-07-01'));

    const dateTimePair = {
      date: '2023-01-01',
      time: '10:00',
    };
    const result = await firstValueFrom(pipe.transform(dateTimePair));

    expect(result).toBe('6 months ago');
  });

  it('should return translation and relative time when time is between start and end', async () => {
    advanceTo(new Date('2023-07-01T12:00:00'));

    const startDate = new Date('2023-07-01T11:59:00');
    const endDate = new Date('2023-07-01T12:01:00');

    const expectedResult = 'Ends in 1 minute';
    const result = await firstValueFrom(pipe.transform(startDate, endDate));

    expect(result).toBe(expectedResult);
  });
});
