import { inject, Pipe, PipeTransform } from '@angular/core';
import {
  differenceInMinutes,
  formatDistanceToNowStrict,
  isAfter,
  isBefore,
  parseISO,
} from 'date-fns';
import { TranslocoService } from '@jsverse/transloco';
import { Observable, of } from 'rxjs';
import { DateInput } from '../../shared/date-time-pair.interface';
import { getDateFnsLocale } from '../../shared/date-utils';

@Pipe({
  name: 'localizedTimeDistancePipe',
  standalone: true,
})
export class LocalizedTimeDistancePipePipe implements PipeTransform {
  private readonly translocoService = inject(TranslocoService);

  transform(valueStart: DateInput, valueEnd?: DateInput): Observable<string> {
    const startDate: Date = this.parseDateInput(valueStart);
    let endDate: Date | undefined;
    const now = new Date();

    if (valueEnd) {
      endDate = this.parseDateInput(valueEnd);
    }

    const locale = getDateFnsLocale(this.translocoService.getActiveLang());

    if (endDate && isBefore(now, endDate) && isAfter(now, startDate)) {
      const timeString = formatDistanceToNowStrict(endDate, {
        addSuffix: true,
        locale,
      });

      return this.translocoService.selectTranslate(
        'duration.ends_in',
        { time: timeString },
        { scope: 'ui' },
      );
    }

    if (Math.abs(differenceInMinutes(now, startDate)) <= 1) {
      return this.translocoService.selectTranslate(
        'duration.just_now',
        {},
        { scope: 'ui' },
      );
    }

    return of(
      formatDistanceToNowStrict(startDate, { addSuffix: true, locale }),
    );
  }

  private parseDateInput(value: DateInput): Date {
    if (value instanceof Date) {
      return value;
    } else if (typeof value === 'string') {
      return parseISO(value);
    } else if (value?.date) {
      const dateTimeString = value.time
        ? `${value.date}T${value.time}`
        : value.date;
      return parseISO(dateTimeString);
    }
    throw new Error('Invalid date input');
  }
}
