import { inject, Injectable, Pipe, PipeTransform } from '@angular/core';
import { intlFormatDistance } from 'date-fns';
import { IntlFormatDistanceUnit } from 'date-fns/intlFormatDistance';
import { TranslocoService } from '@jsverse/transloco';
import { DateInput } from '../../shared/date-time-pair.interface';
import { parseDateInput } from '../utils/date.utils';

@Injectable()
@Pipe({
  name: 'localizedFormatDistanceTimePipe',
  standalone: true,
})
export class LocalizedFormatDistanceTimePipePipe implements PipeTransform {
  private readonly translocoService = inject(TranslocoService);

  transform(
    value: DateInput,
    referenceDate?: DateInput,
    unit?: IntlFormatDistanceUnit,
  ): string {
    const dateToUse = parseDateInput(value);
    const comparisonDate = parseDateInput(referenceDate) || new Date();
    const lang = this.translocoService.getActiveLang();

    if (!dateToUse) {
      return '';
    }

    return intlFormatDistance(dateToUse, comparisonDate, {
      unit: unit ?? undefined,
      locale: lang,
    });
  }
}
