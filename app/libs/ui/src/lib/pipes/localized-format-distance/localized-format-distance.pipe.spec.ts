import { TranslocoService } from '@jsverse/transloco';
import { LocalizedFormatDistanceTimePipePipe } from './localized-format-distance.pipe';
import { TestBed } from '@angular/core/testing';
import { UITestModule } from '../../test/ui-test.module';
import { getTranslocoModule } from '../../../transloco-testing.module';
import { advanceTo, clear } from 'jest-date-mock';

describe('LocalizedFormatDistanceTimePipe', () => {
  let pipe: LocalizedFormatDistanceTimePipePipe;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        LocalizedFormatDistanceTimePipePipe,
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [LocalizedFormatDistanceTimePipePipe, TranslocoService],
    }).compileComponents();

    pipe = TestBed.inject(LocalizedFormatDistanceTimePipePipe);
  });

  it('should format relative time from Date object', () => {
    const now = new Date();
    expect(pipe.transform(now)).toBe('now');
  });

  it('should format relative time from DateTimePair object', () => {
    advanceTo(new Date('2023-07-01'));

    const dateTimePair = {
      date: '2023-01-01',
      time: '10:00',
    };
    expect(pipe.transform(dateTimePair)).toBe('2 quarters ago');
    clear();
  });

  it('should handle null values in DateTimePair', () => {
    const dateTimePair = {
      date: null,
      time: null,
    };
    expect(pipe.transform(dateTimePair)).toBe('');
  });

  it('should return empty string for invalid date', () => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    expect(pipe.transform(null!)).toBe('');
  });
});
