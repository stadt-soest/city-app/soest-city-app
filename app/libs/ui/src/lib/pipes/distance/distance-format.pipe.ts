import { inject, Pipe, PipeTransform } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';

@Pipe({
  name: 'distanceFormat',
  standalone: true,
})
export class DistanceFormatPipe implements PipeTransform {
  private readonly translocoService = inject(TranslocoService);

  transform(value: number): string {
    if (value === null || value === undefined) {
      return '';
    }

    const isMetric = value < 1000;
    const formattedValue = isMetric ? value : value / 1000;

    const unitKey = isMetric ? 'ui.units.meter' : 'ui.units.kilometer';

    return this.translocoService.translate(unitKey, {
      value: Math.round(formattedValue),
    });
  }
}
