import { TestBed } from '@angular/core/testing';
import { DistanceFormatPipe } from './distance-format.pipe';
import { TranslocoService } from '@jsverse/transloco';

describe('DistanceFormatPipe', () => {
  let pipe: DistanceFormatPipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [DistanceFormatPipe],
      providers: [
        DistanceFormatPipe,
        {
          provide: TranslocoService,
          useValue: {
            translate: jest.fn(
              (key, params) =>
                `${params.value} ${key === 'ui.units.meter' ? 'm' : 'km'}`,
            ),
          },
        },
      ],
    });

    pipe = TestBed.inject(DistanceFormatPipe);
  });

  it('should return an empty string if value is null or undefined', () => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    expect(pipe.transform(null!)).toBe('');
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    expect(pipe.transform(undefined!)).toBe('');
  });

  it('should format distances below 1000 using meters', () => {
    const distance = 500.49;
    const expected = `500 m`;
    expect(pipe.transform(distance)).toBe(expected);
  });

  it('should convert and format distances of 1000 meters and above using kilometers', () => {
    const distance = 1500;
    const expected = `2 km`;
    expect(pipe.transform(distance)).toBe(expected);
  });
});
