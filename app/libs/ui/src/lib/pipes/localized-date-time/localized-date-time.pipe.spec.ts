import { TestBed } from '@angular/core/testing';
import { format, parseISO } from 'date-fns';
import { de, enUS } from 'date-fns/locale';
import { TranslocoService } from '@jsverse/transloco';
import { LocalizedDateTimePipe } from './localized-date-time.pipe';

const mockTranslocoService = {
  getActiveLang: jest.fn(),
};

describe('LocalizedDateTimePipe', () => {
  let pipe: LocalizedDateTimePipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [LocalizedDateTimePipe],
      providers: [
        LocalizedDateTimePipe,
        { provide: TranslocoService, useValue: mockTranslocoService },
      ],
    });

    pipe = TestBed.inject(LocalizedDateTimePipe);
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should format date-time correctly for English', () => {
    mockTranslocoService.getActiveLang.mockReturnValue('en');
    const date = '2023-01-01T09:00:00Z';
    const expectedResult = format(parseISO(date), 'PP, p', { locale: enUS });

    const result = pipe.transform(date);

    expect(result).toEqual(expectedResult);
  });

  it('should format date-time correctly for German', () => {
    mockTranslocoService.getActiveLang.mockReturnValue('de');
    const date = '2023-01-01T09:00:00Z';
    const expectedResult = format(parseISO(date), 'dd.MM.yyyy, HH:mm', {
      locale: de,
    });

    const result = pipe.transform(date);

    expect(result).toEqual(expectedResult);
  });

  it('should handle Date objects', () => {
    mockTranslocoService.getActiveLang.mockReturnValue('en');
    const date = new Date('2023-01-01T09:00:00Z');
    const expectedResult = format(date, 'PP, p', { locale: enUS });

    const result = pipe.transform(date);

    expect(result).toEqual(expectedResult);
  });

  it('should default to English for unknown languages', () => {
    mockTranslocoService.getActiveLang.mockReturnValue('unknown_language');
    const date = '2023-01-01T09:00:00Z';
    const expectedResult = format(parseISO(date), 'PP, p', { locale: enUS });

    const result = pipe.transform(date);

    expect(result).toEqual(expectedResult);
  });
});
