import { inject, Pipe, PipeTransform } from '@angular/core';
import { format, parseISO } from 'date-fns';
import { TranslocoService } from '@jsverse/transloco';
import { getDateFnsLocale } from '../../shared/date-utils';

@Pipe({
  name: 'localizedDateTime',
  standalone: true,
})
export class LocalizedDateTimePipe implements PipeTransform {
  private readonly translocoService = inject(TranslocoService);

  transform(value: Date | string, ...args: unknown[]): string {
    try {
      const date = typeof value === 'string' ? parseISO(value) : value;
      const lang = this.translocoService.getActiveLang();
      return this.formatDateTime(date, lang);
    } catch (error) {
      console.error('Error parsing date:', value, error);
      return '';
    }
  }

  private formatDateTime(date: Date, lang: string): string {
    const formatString = lang === 'de' ? 'dd.MM.yyyy, HH:mm' : 'PP, p';
    return format(date, formatString, { locale: getDateFnsLocale(lang) });
  }
}
