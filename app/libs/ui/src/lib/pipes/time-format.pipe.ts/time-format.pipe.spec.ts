import { TimeFormatPipe } from './time-format.pipe';

describe('TimeFormatPipe', () => {
  let pipe: TimeFormatPipe;

  beforeEach(() => {
    pipe = new TimeFormatPipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should format "HH:MM:SS" string to "HH:MM"', () => {
    const time = '13:45:30';
    expect(pipe.transform(time)).toBe('13:45');
  });

  it('should format "HH:MM" string to "HH:MM"', () => {
    const time = '08:20';
    expect(pipe.transform(time)).toBe('08:20');
  });

  it('should handle invalid time formats gracefully', () => {
    const invalidTime = 'invalid';
    expect(pipe.transform(invalidTime)).toBe('invalid');
  });

  it('should return the original string if it contains less than 2 parts', () => {
    const incompleteTime = '12';
    expect(pipe.transform(incompleteTime)).toBe('12');
  });
});
