import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeFormat',
  standalone: true,
})
export class TimeFormatPipe implements PipeTransform {
  transform(time: string): string {
    const parts = time.split(':');
    if (parts.length < 2) {
      return time;
    }

    const hours = parts[0];
    const minutes = parts[1];
    return `${hours}:${minutes}`;
  }
}
