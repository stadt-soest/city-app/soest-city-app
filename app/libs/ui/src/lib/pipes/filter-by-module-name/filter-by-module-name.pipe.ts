import { Pipe, PipeTransform } from '@angular/core';
import { FeedItem } from '@sw-code/urbo-core';

@Pipe({
  name: 'filterByModuleName',
  standalone: true,
})
export class FilterByModuleNamePipe implements PipeTransform {
  transform(feedItems: FeedItem[], moduleName: string): FeedItem[] {
    return feedItems.filter((item) => item.moduleName === moduleName);
  }
}
