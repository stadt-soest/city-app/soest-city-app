import { parseISO } from 'date-fns';
import { de, enUS, Locale } from 'date-fns/locale';
import { DateInput } from '../../shared/date-time-pair.interface';

export const parseDateInput = (
  value: DateInput | undefined,
): Date | undefined => {
  if (value instanceof Date) {
    return value;
  } else if (typeof value === 'string') {
    return parseISO(value);
  } else if (value?.date) {
    const dateTimeString = value.time
      ? `${value.date}T${value.time}`
      : value.date;
    return parseISO(dateTimeString);
  }
  return undefined;
};

export const getLocale = (lang: string): Locale => {
  if (lang === 'en') {
    return enUS;
  } else {
    return de;
  }
};
