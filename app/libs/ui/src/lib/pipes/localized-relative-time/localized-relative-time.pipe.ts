import { inject, Injectable, Pipe, PipeTransform } from '@angular/core';
import { addDays, formatRelative, intlFormatDistance, subDays } from 'date-fns';
import { IntlFormatDistanceUnit } from 'date-fns/intlFormatDistance';
import { TranslocoService } from '@jsverse/transloco';
import { LocalizedWeekDayPipe } from '../localized-week-day/localized-week-day.pipe';
import { DateInput } from '../../shared/date-time-pair.interface';
import { getLocale, parseDateInput } from '../utils/date.utils';

@Injectable()
@Pipe({
  name: 'localizedRelativeTimePipe',
  standalone: true,
})
export class LocalizedRelativeTimePipePipe implements PipeTransform {
  private readonly translocoService = inject(TranslocoService);
  private readonly localizedWeekDayPipe = new LocalizedWeekDayPipe();

  transform(
    value: DateInput,
    showWeekday: boolean,
    referenceDate?: DateInput,
    unit?: IntlFormatDistanceUnit,
  ): string {
    const dateToUse = parseDateInput(value);
    const comparisonDate = parseDateInput(referenceDate) || new Date();
    const lang = this.translocoService.getActiveLang();

    if (!dateToUse) {
      return '';
    }

    const now = new Date();
    const twoDaysBehind = subDays(now, 2);
    const sevenDaysAhead = addDays(now, 7);
    const locale = getLocale(lang);

    if (dateToUse >= twoDaysBehind && dateToUse <= sevenDaysAhead) {
      return formatRelative(dateToUse, comparisonDate, { locale });
    }

    const fourWeeksAhead = addDays(now, 28);

    let result = intlFormatDistance(dateToUse, comparisonDate, {
      unit: unit ?? undefined,
      locale: lang,
    });

    if (dateToUse <= fourWeeksAhead && showWeekday) {
      const localizedWeekday = this.localizedWeekDayPipe.transform(dateToUse);
      result = `${localizedWeekday} ${result}`;
    }

    return result;
  }
}
