import { advanceTo, clear } from 'jest-date-mock';
import { TestBed } from '@angular/core/testing';
import { LocalizedRelativeTimePipePipe } from './localized-relative-time.pipe';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '../../test/ui-test.module';
import { getTranslocoModule } from '../../../transloco-testing.module';
import { TranslationService } from '@sw-code/urbo-core';
import { MockTranslationService } from '../../shared/test/mock-translation-service';

describe('LocalizedRelativeTimePipe', () => {
  let pipe: LocalizedRelativeTimePipePipe;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        LocalizedRelativeTimePipePipe,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        LocalizedRelativeTimePipePipe,
        { provide: TranslationService, useClass: MockTranslationService },
      ],
    }).compileComponents();

    pipe = TestBed.inject(LocalizedRelativeTimePipePipe);
  });

  it('should format relative time from Date object for today', () => {
    advanceTo(new Date('2023-07-01T03:24:00'));

    const now = new Date('2023-07-01T03:24:00');
    expect(pipe.transform(now, true)).toBe('today at 3:24 AM');
  });

  it('should format relative time from Date object for tomorrow', () => {
    advanceTo(new Date('2023-06-30T03:24:00'));

    const now = new Date('2023-07-01T03:24:00');
    expect(pipe.transform(now, true)).toBe('tomorrow at 3:24 AM');
  });

  it('should format relative time from Date object for +2 days and show weekday', () => {
    advanceTo(new Date('2023-06-30T03:24:00'));

    const now = new Date('2023-07-02T03:24:00');
    expect(pipe.transform(now, true)).toBe('Sunday at 3:24 AM');
  });

  it('should format relative time from DateTimePair object', () => {
    advanceTo(new Date('2023-07-01'));

    const dateTimePair = {
      date: '2023-01-01',
      time: '10:00',
    };
    expect(pipe.transform(dateTimePair, false)).toBe('2 quarters ago');
    clear();
  });

  it('should handle null values in DateTimePair', () => {
    const dateTimePair = {
      date: null,
      time: null,
    };
    expect(pipe.transform(dateTimePair, true)).toBe('');
  });

  it('should return empty string for invalid date', () => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    expect(pipe.transform(null!, true)).toBe('');
  });
});
