import { TestBed } from '@angular/core/testing';
import { TranslocoService } from '@jsverse/transloco';
import { LocalizedWeekDayPipe } from './localized-week-day.pipe';

const mockTranslocoService = {
  getActiveLang: jest.fn(),
};

describe('LocalizedWeekDayPipe', () => {
  let pipe: LocalizedWeekDayPipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [LocalizedWeekDayPipe],
      providers: [
        LocalizedWeekDayPipe,
        { provide: TranslocoService, useValue: mockTranslocoService },
      ],
    });

    pipe = TestBed.inject(LocalizedWeekDayPipe);
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('transforms Date object to English weekday', () => {
    mockTranslocoService.getActiveLang.mockReturnValueOnce('en');
    const date = new Date('2024-03-04');
    expect(pipe.transform(date)).toBe('Monday');
  });

  it('transforms Date object to German weekday', () => {
    mockTranslocoService.getActiveLang.mockReturnValueOnce('de');
    const date = new Date('2024-03-04');
    expect(pipe.transform(date)).toBe('Montag');
  });

  it('returns empty string for null input', () => {
    expect(pipe.transform(null)).toBe('');
  });
});
