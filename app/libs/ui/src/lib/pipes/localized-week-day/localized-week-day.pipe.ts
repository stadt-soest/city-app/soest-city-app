import { inject, Injectable, Pipe, PipeTransform } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';

@Injectable()
@Pipe({
  name: 'localizedWeekDay',
  standalone: true,
})
export class LocalizedWeekDayPipe implements PipeTransform {
  private readonly translocoService = inject(TranslocoService);

  transform(value: string | Date | null): string {
    if (!value) {
      return '';
    }
    const lang = this.translocoService.getActiveLang();
    const date = value instanceof Date ? value : new Date(value);
    return date.toLocaleString(lang, { weekday: 'long' });
  }
}
