import { TranslateLocalizationPipe } from './translate-localization.pipe';
import { TestBed } from '@angular/core/testing';
import { Localization, TranslationService } from '@sw-code/urbo-core';

describe('TranslateLocalizedNamePipe', () => {
  let pipe: TranslateLocalizationPipe;

  const mockTranslationService = {
    getCurrentLocale: jest.fn(),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateLocalizationPipe],
      providers: [
        TranslateLocalizationPipe,
        { provide: TranslationService, useValue: mockTranslationService },
      ],
    });

    pipe = TestBed.inject(TranslateLocalizationPipe);

    mockTranslationService.getCurrentLocale.mockReturnValue('EN');
  });

  it('should return the correct localized value for a matching locale', () => {
    const localizations: Localization[] = [
      { value: 'Hallo', language: 'DE' },
      { value: 'Hello', language: 'EN' },
    ];
    const result = pipe.transform(localizations);
    expect(result).toBe('Hello');
  });

  it('should return an empty string for an empty array of localizations', () => {
    const result = pipe.transform([]);
    expect(result).toBe('');
  });
});
