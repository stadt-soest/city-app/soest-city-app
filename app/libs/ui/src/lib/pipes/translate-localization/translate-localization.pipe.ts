import { inject, Pipe, PipeTransform } from '@angular/core';
import { Localization, TranslationService } from '@sw-code/urbo-core';

@Pipe({
  name: 'translateLocalization',
  standalone: true,
})
export class TranslateLocalizationPipe implements PipeTransform {
  private readonly translationService = inject(TranslationService);

  transform(localizedNames: Localization[]): string {
    const localized = localizedNames.find(
      (name) => name.language === this.translationService.getCurrentLocale(),
    );
    return localized ? localized.value : '';
  }
}
