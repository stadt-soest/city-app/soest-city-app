import { TestBed } from '@angular/core/testing';
import { formatDistanceToNowStrict } from 'date-fns';
import { de, enUS } from 'date-fns/locale';
import { TranslocoService } from '@jsverse/transloco';
import { LocalizedTimeAgoPipe } from './localized-time-ago.pipe';

const mockTranslocoService = {
  getActiveLang: jest.fn(),
};

describe('LocalizedTimeAgoPipe', () => {
  let pipe: LocalizedTimeAgoPipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [LocalizedTimeAgoPipe],
      providers: [
        LocalizedTimeAgoPipe,
        { provide: TranslocoService, useValue: mockTranslocoService },
      ],
    });

    pipe = TestBed.inject(LocalizedTimeAgoPipe);
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should format date correctly for English', () => {
    mockTranslocoService.getActiveLang.mockReturnValue('en');
    const date = new Date();
    date.setHours(date.getHours() - 2);
    const expectedResult = formatDistanceToNowStrict(date, {
      addSuffix: true,
      locale: enUS,
    });

    const result = pipe.transform(date);

    expect(result).toEqual(expectedResult);
  });

  it('should format date correctly for German', () => {
    mockTranslocoService.getActiveLang.mockReturnValue('de');
    const date = new Date();
    date.setHours(date.getHours() - 2);
    const expectedResult = formatDistanceToNowStrict(date, {
      addSuffix: true,
      locale: de,
    });

    const result = pipe.transform(date);

    expect(result).toEqual(expectedResult);
  });

  it('should handle string dates', () => {
    mockTranslocoService.getActiveLang.mockReturnValue('en');
    const dateString = '2023-01-01T00:00:00Z';
    const date = new Date(dateString);
    const expectedResult = formatDistanceToNowStrict(date, {
      addSuffix: true,
      locale: enUS,
    });

    const result = pipe.transform(dateString);

    expect(result).toEqual(expectedResult);
  });

  it('should default to English for unknown languages', () => {
    mockTranslocoService.getActiveLang.mockReturnValue('unknown');
    const date = new Date();
    date.setHours(date.getHours() - 2);
    const expectedResult = formatDistanceToNowStrict(date, {
      addSuffix: true,
      locale: enUS,
    });

    const result = pipe.transform(date);

    expect(result).toEqual(expectedResult);
  });
});
