import { inject, Pipe, PipeTransform } from '@angular/core';
import { formatDistanceToNowStrict } from 'date-fns';
import { TranslocoService } from '@jsverse/transloco';
import { getDateFnsLocale } from '../../shared/date-utils';

@Pipe({
  name: 'localizedTimeAgo',
  standalone: true,
})
export class LocalizedTimeAgoPipe implements PipeTransform {
  private readonly translocoService = inject(TranslocoService);

  transform(value: Date | string, ...args: unknown[]): string {
    const date = typeof value === 'string' ? new Date(value) : value;
    const lang = this.translocoService.getActiveLang();
    const locale = getDateFnsLocale(lang);
    return formatDistanceToNowStrict(date, { addSuffix: true, locale });
  }
}
