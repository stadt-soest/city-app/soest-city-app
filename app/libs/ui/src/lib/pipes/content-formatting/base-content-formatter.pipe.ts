import { inject, Pipe, PipeTransform } from '@angular/core';
import linkifyHtml from 'linkify-html';
import DOMPurify from 'dompurify';
import { findPhoneNumbersInText, PhoneNumber } from 'libphonenumber-js/max';
import { decode } from 'html-entities';
import {
  DomPurifyConfigService,
  DomPurifyConfigType,
} from '@sw-code/urbo-core';

@Pipe({
  name: 'baseContentFormatter',
  standalone: true,
})
export class BaseContentFormatterPipe implements PipeTransform {
  private readonly configService = inject(DomPurifyConfigService);

  transform(
    text: string,
    configType: DomPurifyConfigType = DomPurifyConfigType.basic,
  ): string {
    const domPurifyConfig = this.configService.getConfig(configType);

    text = this.removeSpecialMarkup(text);
    text = this.convertHeadersToParagraphs(text);
    text = this.formatBoldText(text);
    text = DOMPurify.sanitize(text, domPurifyConfig) as string;
    if (configType !== DomPurifyConfigType.title) {
      text = this.linkifyContent(text);
    }
    text = this.formatBoldText(text);
    text = decode(text);
    return text.trim();
  }

  private removeSpecialMarkup(text: string): string {
    return text.replace(/\[(\/?vc_[^\]]*)]/g, '');
  }

  private convertHeadersToParagraphs(text: string): string {
    return text.replace(
      /<h[1-4]>(.*?)<\/h[1-4]>/gi,
      (match, content) => `<p><strong>${content}</strong></p>`,
    );
  }

  private formatBoldText(text: string): string {
    return text.replace(/\*(.*?)\*/g, '<strong>$1</strong>');
  }

  private linkifyContent(text: string): string {
    text = this.linkifyUrls(text);
    text = this.linkifyPhoneNumbers(text);
    return text;
  }

  private linkifyUrls(text: string): string {
    return linkifyHtml(text, {
      target: '_blank',
      format: (value, type) => this.formatUrl(value, type),
    });
  }

  private formatUrl(value: string, type: string): string {
    if (type === 'url') {
      const protocolPattern = /^(http:\/\/|https:\/\/)/i;
      const hasProtocol = protocolPattern.test(value);
      const urlValue = hasProtocol ? value : `https://${value}`;

      try {
        const url = new URL(urlValue);
        const hostNameWithoutWWW = url.hostname.replace(/^www\./, '');

        if (url.pathname === '/') {
          return hostNameWithoutWWW;
        } else {
          const path = truncate(url.pathname, 7);
          return hostNameWithoutWWW + path;
        }
      } catch (e) {
        return value;
      }
    }
    return value;
  }

  private linkifyPhoneNumbers(text: string): string {
    const phoneNumbers = findPhoneNumbersInText(text, 'DE');
    const phoneNumberLinks = new Map<string, string>();

    phoneNumbers.forEach(({ number: phoneNumber, startsAt, endsAt }) => {
      if (!this.isValidPhoneNumber(phoneNumber)) {
        return;
      }
      const original = text.slice(startsAt, endsAt);
      const standardized = phoneNumber.number;
      const link = `<a href="tel:${standardized}">${original}</a>`;
      phoneNumberLinks.set(original, link);
    });

    phoneNumberLinks.forEach((link, original) => {
      const regex = new RegExp(
        original.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'),
        'g',
      );
      text = text.replace(regex, link);
    });

    return text;
  }

  private isValidPhoneNumber(phoneNumber: PhoneNumber): boolean {
    const nationalNumber = phoneNumber.nationalNumber;
    return nationalNumber.length >= 7 && nationalNumber.length <= 15;
  }
}

const truncate = (str: string, maxlength: number): string =>
  str.length > maxlength ? str.slice(0, maxlength - 1) + '…' : str;
