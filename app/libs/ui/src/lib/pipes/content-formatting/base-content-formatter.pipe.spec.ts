import { BaseContentFormatterPipe } from './base-content-formatter.pipe';
import { TestBed } from '@angular/core/testing';
import {
  DomPurifyConfigService,
  DomPurifyConfigType,
} from '@sw-code/urbo-core';

describe('BaseContentFormatterPipe', () => {
  let pipe: BaseContentFormatterPipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [BaseContentFormatterPipe],
      providers: [BaseContentFormatterPipe, DomPurifyConfigService],
    });

    pipe = TestBed.inject(BaseContentFormatterPipe);
  });

  describe('formatBoldText', () => {
    it('should format bold text', () => {
      const result = pipe.transform('This is *bold* text.');
      expect(result).toBe('This is <strong>bold</strong> text.');
    });

    it('should handle text without bold formatting', () => {
      const result = pipe.transform('This is regular text.');
      expect(result).toBe('This is regular text.');
    });

    it('should handle multiple bold sections', () => {
      const result = pipe.transform('*Bold1* and *Bold2*');
      expect(result).toBe('<strong>Bold1</strong> and <strong>Bold2</strong>');
    });

    it('should not modify previously bolded sections', () => {
      const result = pipe.transform(
        '<strong>Bold1</strong> and <strong>Bold2</strong>',
      );
      expect(result).toBe('<strong>Bold1</strong> and <strong>Bold2</strong>');
    });
  });

  describe('linkifyPhoneNumbers', () => {
    it('should linkify a phone number with slashes', () => {
      const result = pipe.transform('Call me at 02921/88339.');
      expect(result).toBe(
        'Call me at <a href="tel:+49292188339">02921/88339</a>.',
      );
    });

    it('should linkify a phone number with slashes and spaces', () => {
      const result = pipe.transform('Call me at 02921 / 88339.');
      expect(result).toBe(
        'Call me at <a href="tel:+49292188339">02921 / 88339</a>.',
      );
    });

    it('should linkify a phone number with hyphens', () => {
      const result = pipe.transform('Call me at 02921-88339.');
      expect(result).toBe(
        'Call me at <a href="tel:+49292188339">02921-88339</a>.',
      );
    });

    it('should linkify a phone number with hyphens and spaces', () => {
      const result = pipe.transform('Call me at 02921 - 88339.');
      expect(result).toBe(
        'Call me at <a href="tel:+49292188339">02921 - 88339</a>.',
      );
    });

    it('should linkify a complex phone number with spaces and hyphens', () => {
      const result = pipe.transform('Call me at 02921 - 103 6110.');
      expect(result).toBe(
        'Call me at <a href="tel:+4929211036110">02921 - 103 6110</a>.',
      );
    });

    it('should linkify a phone number with country code', () => {
      const result = pipe.transform('My number is 49 153 523029.');
      expect(result).toBe(
        'My number is <a href="tel:+4949153523029">49 153 523029</a>.',
      );
    });

    it('should linkify a phone number with a plus sign and country code', () => {
      const result = pipe.transform('Contact: +49 522 4239429.');
      expect(result).toBe(
        'Contact: <a href="tel:+495224239429">+49 522 4239429</a>.',
      );
    });

    it('should not linkify phone numbers that are already linkified', () => {
      const result = pipe.transform(
        'Contact: <a href="tel:+49 522 4239429">+49 522 4239429</a>.',
      );
      expect(result).toBe(
        'Contact: <a href="tel:+495224239429">+49 522 4239429</a>.',
      );
    });
  });

  describe('linkifyUrls', () => {
    it('should linkify a simple URL and display only the domain', () => {
      const result = pipe.transform(
        'Visit https://www.example.com for more information.',
      );
      expect(result).toBe(
        'Visit <a href="https://www.example.com" target="_blank">example.com</a> for more information.',
      );
    });

    it('should linkify a URL with a path and display only the domain', () => {
      const result = pipe.transform(
        'Check out https://www.example.com/page?id=%205#f33 for details.',
      );
      expect(result).toBe(
        'Check out <a href="https://www.example.com/page?id=%205#f33" target="_blank">example.com/page</a> for details.',
      );
    });

    it("should linkify urls fully even if they don't have a protocol [reproducing issue #725]", () => {
      const result = pipe.transform(
        'See www.kreis-soest.de/amtsblatt for more.',
      );
      expect(result).toBe(
        'See <a href="http://www.kreis-soest.de/amtsblatt" target="_blank">kreis-soest.de/amtsb…</a> for more.',
      );
    });

    it('should not add www to a URL', () => {
      const result = pipe.transform(
        'Das Amtsblatt des Kreises Soest Nr. 01/2024 ist erschienen und ' +
          'steht im Internet auf https://kreis-soest.de/amtsblatt zum Download bereit. ' +
          'Interessierte Bürgerinnen und Bürger können sich über neue Ausgaben auch per Newsletter informieren lassen. ',
      );
      expect(result).toBe(
        'Das Amtsblatt des Kreises Soest Nr. 01/2024 ist erschienen und steht im Internet auf ' +
          '<a href="https://kreis-soest.de/amtsblatt" target="_blank">kreis-soest.de/amtsb…</a> zum Download bereit. ' +
          'Interessierte Bürgerinnen und Bürger können sich über neue Ausgaben auch per Newsletter informieren lassen.',
      );
    });

    it('should handle text with multiple URLs', () => {
      const result = pipe.transform(
        'Sites like https://google.com and https://example.org are popular.',
      );
      expect(result).toBe(
        'Sites like <a href="https://google.com" target="_blank">google.com</a> ' +
          'and <a href="https://example.org" target="_blank">example.org</a> are popular.',
      );
    });

    it('should not modify text without URLs', () => {
      const result = pipe.transform('This is regular text without URLs.');
      expect(result).toBe('This is regular text without URLs.');
    });

    it('should linkify a non-http URL starting with www', () => {
      const result = pipe.transform('Visit www.url.de for more information.');
      expect(result).toBe(
        'Visit <a href="http://www.url.de" target="_blank">url.de</a> for more information.',
      );
    });

    it('should linkify multiple non-http URLs', () => {
      const result = pipe.transform(
        'Check www.example.com and www.another-example.org',
      );
      expect(result).toBe(
        'Check <a href="http://www.example.com" target="_blank">example.com</a> ' +
          'and <a href="http://www.another-example.org" target="_blank">another-example.org</a>',
      );
    });

    // a questionmark at the end without any parameter is irrelevant for an url, so we can safely exclude it from the url.
    it('should not linkify surrounding punctuation, not even questionmarks', () => {
      const result = pipe.transform(
        'This is www.example.com. And www.another-example.org?',
      );
      expect(result).toBe(
        'This is <a href="http://www.example.com" target="_blank">example.com</a>. ' +
          'And <a href="http://www.another-example.org" target="_blank">another-example.org</a>?',
      );
    });

    it('should not linkify multiple urls that are already linkified', () => {
      const result = pipe.transform(
        'online unter <a href="http://www.vhssoest.de">www.vhssoest.de</a> <a href="www.vhssoest1.de">www.vhssoest.de</a>',
      );
      expect(result).toBe(
        'online unter ' +
          '<a href="http://www.vhssoest.de">www.vhssoest.de</a> ' +
          '<a href="www.vhssoest1.de">www.vhssoest.de</a>',
      );
    });
  });

  describe('linkifyEmails', () => {
    it('should linkify an email address', () => {
      const result = pipe.transform('Contact me at email@example.com.');
      expect(result).toBe(
        'Contact me at <a href="mailto:email@example.com" target="_blank">email@example.com</a>.',
      );
    });

    it('should handle multiple email addresses', () => {
      const result = pipe.transform(
        'Send emails to person1@example.com and person2@anotherdomain.org.',
      );
      expect(result).toBe(
        'Send emails to <a href="mailto:person1@example.com" target="_blank">person1@example.com</a> and ' +
          '<a href="mailto:person2@anotherdomain.org" target="_blank">person2@anotherdomain.org</a>.',
      );
    });

    it('should not linkify emails that are already linkified', () => {
      const result = pipe.transform(
        'send to <a href="mailto:person1@example.com">person1@example.com</a>',
      );
      expect(result).toBe(
        'send to <a href="mailto:person1@example.com">person1@example.com</a>',
      );
    });
  });

  it('should do everything after another without messing up previous steps', () => {
    const result = pipe.transform(
      'wart ihr schon auf www.soest.de/x? da findet ihr die telefonnummer 02921 - 103 6110 und die email adresse at@me.com '.repeat(
        3,
      ),
    );
    expect(result).toBe(
      'wart ihr schon auf <a href="http://www.soest.de/x" target="_blank">soest.de/x</a>? da findet ihr die telefonnummer <a href="tel:+4929211036110">02921 - 103 6110</a> und die email adresse <a href="mailto:at@me.com" target="_blank">at@me.com</a> '
        .repeat(3)
        .trim(),
    );
  });

  it('should strip bold and italic tags from title text when DomPurifyConfigType is Title', () => {
    const result = pipe.transform(
      '<B>bold</B> <I>italic</I> text',
      DomPurifyConfigType.title,
    );
    expect(result).toBe('bold italic text');
  });

  it('should correctly transform HTML entities to their corresponding characters', () => {
    const result = pipe.transform(
      'Muttischicht? Im Klinikum auch für Papis &#8211; und das seit 15 Jahren',
      DomPurifyConfigType.title,
    );
    expect(result).toBe(
      'Muttischicht? Im Klinikum auch für Papis – und das seit 15 Jahren',
    );
  });
});
