import { inject, Pipe, PipeTransform } from '@angular/core';
import { format, parseISO } from 'date-fns';
import { TranslocoService } from '@jsverse/transloco';
import { Observable } from 'rxjs';
import { DateInput } from '../../shared/date-time-pair.interface';
import { getDateFnsLocale } from '../../shared/date-utils';

@Pipe({
  name: 'localizedTimeRange',
  standalone: true,
})
export class LocalizedTimeRangePipe implements PipeTransform {
  private readonly translocoService = inject(TranslocoService);

  transform(startTime: DateInput, endTime: DateInput): Observable<string> {
    const start = this.extractTime(startTime);
    const end = this.extractTime(endTime, true);
    const locale = getDateFnsLocale(this.translocoService.getActiveLang());

    const formattedStartTime = format(start, 'p', { locale }).replace(
      ':00',
      '',
    );
    const formattedEndTime = this.isEndOfDay(end)
      ? ''
      : format(end, 'p', { locale }).replace(':00', '');

    if (formattedEndTime) {
      return this.translocoService.selectTranslate(
        'duration.time_range',
        {
          startTime: formattedStartTime,
          endTime: formattedEndTime,
        },
        { scope: 'ui' },
      );
    } else {
      return this.translocoService.selectTranslate(
        'duration.start_time',
        {
          startTime: formattedStartTime,
        },
        { scope: 'ui' },
      );
    }
  }

  private extractTime(input: DateInput, isEndTime = false): Date {
    if (typeof input === 'string') {
      return parseISO('1970-01-01T' + input);
    } else if (input instanceof Date) {
      return input;
    } else if (input?.time) {
      return parseISO('1970-01-01T' + input.time);
    } else {
      return parseISO(`1970-01-01T${isEndTime ? '23:59' : '00:00'}`);
    }
  }

  private isEndOfDay(date: Date): boolean {
    return (
      date.getHours() === 23 &&
      date.getMinutes() === 59 &&
      date.getSeconds() === 59
    );
  }
}
