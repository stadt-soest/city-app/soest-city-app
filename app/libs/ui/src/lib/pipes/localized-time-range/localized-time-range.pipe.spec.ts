import { firstValueFrom } from 'rxjs';
import { TestBed } from '@angular/core/testing';
import { LocalizedTimeRangePipe } from './localized-time-range.pipe';
import { getTranslocoModule } from '../../../transloco-testing.module';
import { DateTimePair } from '../../shared/date-time-pair.interface';

describe('LocalizedTimeRangePipe', () => {
  let pipe: LocalizedTimeRangePipe;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LocalizedTimeRangePipe, getTranslocoModule()],
      providers: [LocalizedTimeRangePipe],
    }).compileComponents();

    pipe = TestBed.inject(LocalizedTimeRangePipe);
  });

  it('should handle Date objects', async () => {
    const start = new Date('1970-01-01T08:00');
    const end = new Date('1970-01-01T17:00');
    const result = await firstValueFrom(pipe.transform(start, end));

    expect(result).toBe('8 AM - 5 PM.');
  });

  it('should handle date strings', async () => {
    const start = '08:00';
    const end = '17:00';
    const result = await firstValueFrom(pipe.transform(start, end));

    expect(result).toBe('8 AM - 5 PM.');
  });

  it('should handle DateTimePair objects', async () => {
    const start: DateTimePair = { date: null, time: '08:00' };
    const end: DateTimePair = { date: null, time: '17:00' };
    const result = await firstValueFrom(pipe.transform(start, end));

    expect(result).toBe('8 AM - 5 PM.');
  });

  it('should handle null time in DateTimePair', async () => {
    const start: DateTimePair = { date: null, time: null };
    const end: DateTimePair = { date: null, time: '17:00' };
    const result = await firstValueFrom(pipe.transform(start, end));

    expect(result).toBe('12 AM - 5 PM.');
  });

  it('should handle both times as null in DateTimePair', async () => {
    const start: DateTimePair = { date: null, time: null };
    const end: DateTimePair = { date: null, time: null };
    const result = await firstValueFrom(pipe.transform(start, end));

    expect(result).toBe('12 AM - 11:59 PM.');
  });

  it('should handle full time strings', async () => {
    const start = '08:30';
    const end = '17:30';
    const result = await firstValueFrom(pipe.transform(start, end));

    expect(result).toBe('8:30 AM - 5:30 PM.');
  });

  it('should handle full time strings for start and not end', async () => {
    const start = '08:30';
    const end = '17:00';
    const result = await firstValueFrom(pipe.transform(start, end));

    expect(result).toBe('8:30 AM - 5 PM.');
  });

  it('should handle full time strings for end and not start', async () => {
    const start = '08:00';
    const end = '17:30';
    const result = await firstValueFrom(pipe.transform(start, end));

    expect(result).toBe('8 AM - 5:30 PM.');
  });

  it('should handle end of day', async () => {
    const start = '08:00:00';
    const end = '23:59:59';
    const result = await firstValueFrom(pipe.transform(start, end));

    expect(result).toBe('Starts at 8 AM.');
  });
});
