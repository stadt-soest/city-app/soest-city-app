import { TestBed } from '@angular/core/testing';
import { TranslocoService } from '@jsverse/transloco';
import { UITestModule } from '../../test/ui-test.module';
import { LocalizedDateDiffPipePipe } from './localized-date-diff.pipe';
import { getTranslocoModule } from '../../../transloco-testing.module';
import { TranslationService } from '@sw-code/urbo-core';
import { DateTimePair } from '../../shared/date-time-pair.interface';

describe('LocalizedDateDiffPipe', () => {
  let pipe: LocalizedDateDiffPipePipe;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        LocalizedDateDiffPipePipe,
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        LocalizedDateDiffPipePipe,
        TranslocoService,
        TranslationService,
      ],
    }).compileComponents();

    pipe = TestBed.inject(LocalizedDateDiffPipePipe);
  });

  it('should handle Date objects', () => {
    const start = new Date('2022-01-01');
    const end = new Date('2022-01-03');
    expect(pipe.transform(start, end)).toBe('3 days long');
  });

  it('should handle date strings', () => {
    const start = '2022-01-01';
    const end = '2022-01-03';
    expect(pipe.transform(start, end)).toBe('3 days long');
  });

  it('should handle DateTimePair objects', () => {
    const start: DateTimePair = { date: '2022-01-01', time: '10:00' };
    const end: DateTimePair = { date: '2022-01-03', time: '18:00' };
    expect(pipe.transform(start, end)).toBe('3 days long');
  });

  it('should handle null time in DateTimePair', () => {
    const start: DateTimePair = { date: '2022-01-01', time: null };
    const end: DateTimePair = { date: '2022-01-03', time: null };
    expect(pipe.transform(start, end)).toBe('3 days long');
  });

  it('should throw error for invalid input', () => {
    const start = null;
    const end = '2022-01-03';
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    expect(() => pipe.transform(start!, end)).toThrow('Invalid date input');
  });
});
