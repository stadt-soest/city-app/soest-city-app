import { inject, Pipe, PipeTransform } from '@angular/core';
import { differenceInCalendarDays, parseISO } from 'date-fns';
import { TranslocoService } from '@jsverse/transloco';
import { DateInput } from '../../shared/date-time-pair.interface';

@Pipe({
  name: 'localizedDateDiffPipe',
  standalone: true,
})
export class LocalizedDateDiffPipePipe implements PipeTransform {
  private readonly translocoService = inject(TranslocoService);

  transform(startInput: DateInput, endInput: DateInput): string {
    const start = this.ensureDate(startInput);
    const end = this.ensureDate(endInput);

    const diffDays = differenceInCalendarDays(end, start) + 1;

    const durationKey = `ui.duration.day.${diffDays === 1 ? 'one' : 'other'}`;
    const durationString = this.translocoService.translate(durationKey, {
      count: diffDays,
    });
    const longString = this.translocoService.translate('ui.duration.long');

    return `${durationString} ${longString}`;
  }

  private ensureDate(input: DateInput): Date {
    if (typeof input === 'string') {
      return parseISO(input);
    } else if (input instanceof Date) {
      return input;
    } else if (input?.date) {
      const dateTimeString = input.time
        ? `${input.date}T${input.time}`
        : input.date;
      return parseISO(dateTimeString);
    }
    throw new Error('Invalid date input');
  }
}
