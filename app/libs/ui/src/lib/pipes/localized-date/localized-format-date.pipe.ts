import { inject, Injectable, Pipe, PipeTransform } from '@angular/core';
import { format, parseISO } from 'date-fns';
import { TranslocoService } from '@jsverse/transloco';
import { DateInput, DateTimePair } from '../../shared/date-time-pair.interface';
import { getDateFnsLocale } from '../../shared/date-utils';

@Injectable()
@Pipe({
  name: 'localizedFormatDate',
  standalone: true,
})
export class LocalizedFormatDatePipe implements PipeTransform {
  private readonly translocoService = inject(TranslocoService);

  transform(
    value: Date | string | DateTimePair | null,
    formatStr: string,
  ): string {
    if (!value) {
      return '';
    }

    const date = this.ensureDate(value);
    const lang = this.translocoService.getActiveLang();

    return format(date, formatStr, { locale: getDateFnsLocale(lang) });
  }

  private ensureDate(input: DateInput): Date {
    if (typeof input === 'string') {
      return parseISO(input);
    } else if (input instanceof Date) {
      return input;
    } else if (input?.date) {
      const dateTimeString = input.time
        ? `${input.date}T${input.time}`
        : input.date;
      return parseISO(dateTimeString);
    }
    throw new Error('Invalid date input');
  }
}
