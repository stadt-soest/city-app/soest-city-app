import { TestBed } from '@angular/core/testing';
import { format, parseISO } from 'date-fns';
import { de, enUS } from 'date-fns/locale';
import { TranslocoService } from '@jsverse/transloco';
import { LocalizedFormatDatePipe } from './localized-format-date.pipe';

const mockTranslocoService = {
  getActiveLang: jest.fn(),
};

describe('LocalizedFormatDatePipe', () => {
  let pipe: LocalizedFormatDatePipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [LocalizedFormatDatePipe],
      providers: [
        LocalizedFormatDatePipe,
        { provide: TranslocoService, useValue: mockTranslocoService },
      ],
    });

    pipe = TestBed.inject(LocalizedFormatDatePipe);
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should format date correctly for English', () => {
    mockTranslocoService.getActiveLang.mockReturnValue('en');
    const date = '2023-01-01T00:00:00Z';
    const expectedResult = format(parseISO(date), 'PPPP', { locale: enUS });

    const result = pipe.transform(date, 'PPPP');

    expect(result).toEqual(expectedResult);
  });

  it('should format date correctly for German', () => {
    mockTranslocoService.getActiveLang.mockReturnValue('de');
    const date = '2023-01-01T00:00:00Z';
    const expectedResult = format(parseISO(date), 'PPPP', { locale: de });

    const result = pipe.transform(date, 'PPPP');

    expect(result).toEqual(expectedResult);
  });

  it('should handle Date objects', () => {
    mockTranslocoService.getActiveLang.mockReturnValue('en');
    const date = new Date('2023-01-01T00:00:00Z');
    const expectedResult = format(date, 'PPPP', { locale: enUS });

    const result = pipe.transform(date, 'PPPP');

    expect(result).toEqual(expectedResult);
  });

  it('should default to English for unknown languages', () => {
    mockTranslocoService.getActiveLang.mockReturnValue('unknown_language');
    const date = '2023-01-01T00:00:00Z';
    const expectedResult = format(parseISO(date), 'PPPP', { locale: enUS });

    const result = pipe.transform(date, 'PPPP');

    expect(result).toEqual(expectedResult);
  });
});
