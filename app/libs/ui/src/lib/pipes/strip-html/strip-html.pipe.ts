import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stripHtml',
  standalone: true,
})
export class StripHtmlPipe implements PipeTransform {
  transform(value?: string | null): string {
    if (!value) {
      return '';
    }

    const parser = new DOMParser();
    const doc = parser.parseFromString(value, 'text/html');
    return doc.body.textContent ?? '';
  }
}
