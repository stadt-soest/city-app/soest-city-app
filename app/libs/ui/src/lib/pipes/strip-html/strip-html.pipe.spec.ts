import { StripHtmlPipe } from './strip-html.pipe';

describe('StripHtmlPipe', () => {
  let pipe: StripHtmlPipe;

  beforeEach(() => {
    pipe = new StripHtmlPipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should strip HTML tags from a string', () => {
    const htmlString = '<p>This is <strong>HTML</strong> content</p>';
    const expectedResult = 'This is HTML content';
    expect(pipe.transform(htmlString)).toBe(expectedResult);
  });

  it('should return an empty string if value is null', () => {
    expect(pipe.transform(null)).toBe('');
  });

  it('should return an empty string if value is undefined', () => {
    expect(pipe.transform(undefined)).toBe('');
  });

  it('should return the same string if there are no HTML tags', () => {
    const plainString = 'This is plain text';
    expect(pipe.transform(plainString)).toBe(plainString);
  });
});
