export enum CustomSelectableIconTextButtonTypes {
  skip,
  next,
  later,
  activate,
  finish,
}
