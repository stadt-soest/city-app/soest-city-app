export enum CustomButtonColorScheme {
  strong = 'strong',
  hint = 'hint',
  hintSecondary = 'hintSecondary',
  outlined = 'outlined',
  transparent = 'transparent',
  main = 'main',
  inherit = 'inherit',
  info = 'info',
  highlight = 'highlight',
  accentRed = 'accentRed',
}

export enum CustomButtonFontScheme {
  content = 'content',
  default = 'default',
}

export enum CustomButtonBorderScheme {
  hintBorder = 'hint-border',
  transparentBorder = 'transparent-border',
}

export enum CustomButtonType {
  button = 'button',
  link = 'link',
}
