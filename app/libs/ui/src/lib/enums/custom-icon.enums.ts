export enum IconType {
  materialIconsOutlined = 'material-icons-outlined',
  materialSymbolsOutlined = 'material-symbols-outlined',
}

export enum IconFontSize {
  tiny = '0.6875rem',
  small = '1.0625rem',
  smallMedium = '1.125rem',
  medium = '1.3rem',
  large = '1.5rem',
  xLarge = '1.75rem',
}

export enum IconColor {
  defaultInherit = 'default-inherit',
  mainForeground = 'main-foreground',
  secondaryForeground = 'secondary-foreground',
  onerrorForeground = 'onerror-foreground',
}
