export interface DateTimePair {
  date: string | null;
  time: string | null;
}

export type DateInput = Date | string | DateTimePair;
