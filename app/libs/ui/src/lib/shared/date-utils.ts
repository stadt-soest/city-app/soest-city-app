import { de, enUS, Locale } from 'date-fns/locale';

export const getDateFnsLocale = (lang: string): Locale => {
  const langCode = lang.toUpperCase();
  switch (langCode) {
    case 'DE':
      return de;
    case 'EN':
    default:
      return enUS;
  }
};
