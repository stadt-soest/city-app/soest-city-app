import { TestBed } from '@angular/core/testing';
import { ThemeService } from './theme.service';

describe('ThemeService', () => {
  let service: ThemeService;

  beforeAll(() => {
    Object.defineProperty(window, 'matchMedia', {
      writable: true,
      value: jest.fn().mockImplementation((query: string) => ({
        matches: query.includes('dark'),
        media: query,
        onchange: null,
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
        addListener: jest.fn(),
        removeListener: jest.fn(),
      })),
    });
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ThemeService],
    });
    service = TestBed.inject(ThemeService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should detect dark mode when prefers-color-scheme is dark', () => {
    jest.spyOn(window, 'matchMedia').mockImplementation((query: string) => ({
      matches: true,
      media: query,
      onchange: null,
      addEventListener: jest.fn(),
      removeEventListener: jest.fn(),
      dispatchEvent: jest.fn(),
      addListener: jest.fn(),
      removeListener: jest.fn(),
    }));

    service = new ThemeService();
    expect(service.isDarkMode()).toBe(true);
  });

  it('should detect light mode when prefers-color-scheme is light', () => {
    jest.spyOn(window, 'matchMedia').mockImplementation((query: string) => ({
      matches: false,
      media: query,
      onchange: null,
      addEventListener: jest.fn(),
      removeEventListener: jest.fn(),
      dispatchEvent: jest.fn(),
      addListener: jest.fn(),
      removeListener: jest.fn(),
    }));

    service = new ThemeService();
    expect(service.isDarkMode()).toBe(false);
  });

  describe('theme change handling', () => {
    it('should update isDarkMode$ when the media query changes to dark mode', () => {
      const listeners: { [key: string]: EventListener } = {};

      jest.spyOn(window, 'matchMedia').mockImplementation(
        (query: string): MediaQueryList => ({
          matches: false,
          media: query,
          onchange: null,
          addEventListener: (
            event: string,
            listener: EventListenerOrEventListenerObject,
          ) => {
            listeners[event] = listener as EventListener;
          },
          removeEventListener: jest.fn(),
          dispatchEvent: jest.fn(),
          addListener: jest.fn(),
          removeListener: jest.fn(),
        }),
      );

      service = new ThemeService();

      const isDarkModeValues: boolean[] = [];
      service.isDarkMode$.subscribe((value) => isDarkModeValues.push(value));

      (listeners['change'] as EventListener)({
        matches: true,
      } as MediaQueryListEvent);

      expect(isDarkModeValues).toEqual([false, true]);
    });

    it('should update isDarkMode$ when the media query changes to light mode', () => {
      const listeners: { [key: string]: EventListener } = {};

      jest.spyOn(window, 'matchMedia').mockImplementation(
        (query: string): MediaQueryList => ({
          matches: true,
          media: query,
          onchange: null,
          addEventListener: (
            event: string,
            listener: EventListenerOrEventListenerObject,
          ) => {
            listeners[event] = listener as EventListener;
          },
          removeEventListener: jest.fn(),
          dispatchEvent: jest.fn(),
          addListener: jest.fn(),
          removeListener: jest.fn(),
        }),
      );

      service = new ThemeService();

      const isDarkModeValues: boolean[] = [];
      service.isDarkMode$.subscribe((value) => isDarkModeValues.push(value));

      (listeners['change'] as EventListener)({
        matches: false,
      } as MediaQueryListEvent);

      expect(isDarkModeValues).toEqual([true, false]);
    });
  });
});
