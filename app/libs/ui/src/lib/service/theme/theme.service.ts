import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class ThemeService {
  private readonly isDarkModeSubject = new BehaviorSubject<boolean>(
    this.detectTheme(),
  );

  isDarkMode$ = this.isDarkModeSubject.asObservable();

  constructor() {
    const mediaQuery = window.matchMedia('(prefers-color-scheme: dark)');
    mediaQuery.addEventListener('change', this.handleThemeChange.bind(this));
  }

  isDarkMode(): boolean {
    return this.isDarkModeSubject.value;
  }

  private detectTheme(): boolean {
    return (
      window.matchMedia &&
      window.matchMedia('(prefers-color-scheme: dark)').matches
    );
  }

  private handleThemeChange(event: MediaQueryListEvent) {
    this.isDarkModeSubject.next(event.matches);
  }
}
