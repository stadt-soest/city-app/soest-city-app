import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SafeExternalLinksDirective } from './safe-external-links.directive';

@Component({
  template: ` <div [innerHTML]="htmlContent" libSafeExternalLinks></div>`,
  standalone: false,
})
class TestComponent {
  htmlContent = '';
}

describe('SafeExternalLinksDirective', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SafeExternalLinksDirective],
      declarations: [TestComponent],
      providers: [SafeExternalLinksDirective],
    }).compileComponents();

    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should apply target="_blank" to all anchor tags', () => {
    component.htmlContent = `
    <a href="https://example.com">Example</a>
    <a href="https://google.com">Google</a>`;
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      const anchors = fixture.nativeElement.querySelectorAll('a');
      expect(anchors.length).toBeGreaterThan(0);
      anchors.forEach((anchor: HTMLAnchorElement) => {
        expect(anchor.target).toBe('_blank');
      });
    });
  });

  it('should not overwrite existing target="_blank" on anchor tags', () => {
    component.htmlContent = `<a href="https://example.com" target="_blank">Example</a>`;
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      const anchor = fixture.nativeElement.querySelector('a');
      expect(anchor.target).toBe('_blank');
    });
  });

  it('should add rel="noopener noreferrer" to anchor tags', () => {
    component.htmlContent = `<a href="https://secure.example">Secure Example</a>`;
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      const anchor = fixture.nativeElement.querySelector('a');
      expect(anchor.rel).toBe('noopener noreferrer');
    });
  });

  it('should change existing target value to "_blank" for anchor tags', async () => {
    component.htmlContent = `<a href="https://example.com" target="_parent">Example</a>`;
    fixture.detectChanges();
    await fixture.whenStable();

    const anchor = fixture.nativeElement.querySelector('a');
    expect(anchor.target).toBe('_blank');
  });

  it('should preserve other attributes while setting target="_blank"', async () => {
    component.htmlContent = `<a href="https://example.com" class="external-link" rel="external nofollow">Example</a>`;
    fixture.detectChanges();
    await fixture.whenStable();

    const anchor = fixture.nativeElement.querySelector('a');
    expect(anchor.target).toBe('_blank');
    expect(anchor.className).toBe('external-link');
    expect(anchor.rel).toBe('noopener noreferrer');
  });

  it('should apply target="_blank" to dynamically added anchor tags', (done) => {
    component.htmlContent = `<a href="https://initial-example.com">Initial Example</a>`;
    fixture.detectChanges();

    setTimeout(() => {
      component.htmlContent = `<a href="https://dynamic-example.com">Dynamic Example</a>`;
      fixture.detectChanges();

      setTimeout(() => {
        const dynamicAnchor = fixture.nativeElement.querySelector(
          'a[href="https://dynamic-example.com"]',
        );
        expect(dynamicAnchor.target).toBe('_blank');
        done();
      }, 50);
    }, 50);
  });
});
