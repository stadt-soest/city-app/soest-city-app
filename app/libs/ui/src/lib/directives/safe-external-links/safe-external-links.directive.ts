import {
  AfterViewInit,
  Directive,
  ElementRef,
  inject,
  OnDestroy,
} from '@angular/core';

@Directive({
  selector: '[libSafeExternalLinks]',
  standalone: true,
})
export class SafeExternalLinksDirective implements AfterViewInit, OnDestroy {
  private readonly el = inject(ElementRef);
  private observer!: MutationObserver;

  ngAfterViewInit() {
    this.applyTargetBlank();
    this.observeChanges();
  }

  ngOnDestroy() {
    if (this.observer) {
      this.observer.disconnect();
    }
  }

  private applyTargetBlank() {
    const links: NodeListOf<HTMLAnchorElement> =
      this.el.nativeElement.querySelectorAll('a');
    links.forEach((link) => {
      link.target = '_blank';
      link.rel = 'noopener noreferrer';
    });
  }

  private observeChanges() {
    this.observer = new MutationObserver((mutations) => {
      mutations.forEach((mutation) => {
        if (mutation.type === 'childList' && mutation.addedNodes.length) {
          this.applyTargetBlank();
        }
      });
    });

    this.observer.observe(this.el.nativeElement, {
      childList: true,
      subtree: true,
    });
  }
}
