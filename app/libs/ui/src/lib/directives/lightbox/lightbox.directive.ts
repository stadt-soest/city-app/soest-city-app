import { Directive, ElementRef, inject, Input, OnInit } from '@angular/core';
import PhotoSwipe from 'photoswipe';

@Directive({
  selector: '[libLightbox]',
  standalone: true,
})
export class LightboxDirective implements OnInit {
  @Input() images: any[] = [];
  lightbox?: PhotoSwipe;
  private readonly el = inject(ElementRef);

  ngOnInit() {
    this.el.nativeElement.addEventListener('click', () => {
      this.openLightbox();
    });
  }

  openLightbox(index = 0) {
    const options = {
      index,
      zoom: false,
      arrowPrev: false,
      arrowNext: false,
      bgOpacity: 1,
      dataSource: this.images,
    };

    this.lightbox = new PhotoSwipe(options);
    this.lightbox.init();
  }
}
