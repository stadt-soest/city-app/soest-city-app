import { NgModule } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { register } from 'swiper/element/bundle';
import { BaseContentFormatterPipe } from './pipes/content-formatting/base-content-formatter.pipe';
import { provideTranslocoScope, TranslocoModule } from '@jsverse/transloco';
import { RouterModule } from '@angular/router';
import {
  iosTransitionAnimation,
  provideIonicAngular,
} from '@ionic/angular/standalone';
import { ThemeService } from './service/theme/theme.service';
import { SafeExternalLinksDirective } from './directives/safe-external-links/safe-external-links.directive';
import { LightboxDirective } from './directives/lightbox/lightbox.directive';
import { LocalizedWeekDayPipe } from './pipes/localized-week-day/localized-week-day.pipe';
import { LocalizedFormatDistanceTimePipePipe } from './pipes/localized-format-distance/localized-format-distance.pipe';
import { LocalizedFormatDatePipe } from './pipes/localized-date/localized-format-date.pipe';

const SERVICES = [ThemeService];
const DIRECTIVES = [SafeExternalLinksDirective, LightboxDirective];
const MODULES = [
  CommonModule,
  FormsModule,
  TranslocoModule,
  ReactiveFormsModule,
  NgOptimizedImage,
  RouterModule,
];

@NgModule({
  imports: [...MODULES, ...DIRECTIVES],
  exports: [...MODULES, ...DIRECTIVES],
  providers: [
    LocalizedWeekDayPipe,
    LocalizedFormatDistanceTimePipePipe,
    LocalizedFormatDatePipe,
    BaseContentFormatterPipe,
    ...SERVICES,
    provideTranslocoScope({
      scope: 'ui',
      loader: {
        en: () => import('./i18n/en.json'),
        de: () => import('./i18n/de.json'),
      },
    }),
    provideIonicAngular({
      mode: 'ios',
      rippleEffect: false,
      swipeBackEnabled: true,
      navAnimation: iosTransitionAnimation,
    }),
  ],
})
export class UIModule {
  constructor() {
    register();
  }
}
