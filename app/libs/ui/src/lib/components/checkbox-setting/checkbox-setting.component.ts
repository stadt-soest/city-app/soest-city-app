import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IonLabel, IonToggle } from '@ionic/angular/standalone';

@Component({
  selector: 'lib-checkbox-setting',
  templateUrl: './checkbox-setting.component.html',
  styleUrls: ['./checkbox-setting.component.scss'],
  imports: [IonLabel, IonToggle],
})
export class CheckboxSettingComponent {
  @Input({ required: true }) localizedTitle = '';
  @Input({ required: true }) checked = false;
  @Output() checkedChange = new EventEmitter<boolean>();

  toggle() {
    this.checked = !this.checked;
    this.checkedChange.emit(this.checked);
  }
}
