import { CheckboxSettingComponent } from './checkbox-setting.component';
import { render, screen } from '@testing-library/angular';
import userEvent from '@testing-library/user-event';

describe('CheckboxSettingComponent', () => {
  it('displays checkbox state based on given boolean', async () => {
    const component = await createSandboxedComponent(
      { checked: true },
      noMocks(),
    );
    expect(component.uiCheckboxChecked).toBe(true);

    await component.setChecked(true);
    expect(component.uiCheckboxChecked).toBe(true);

    await component.setChecked(false);
    expect(component.uiCheckboxChecked).toBe(false);

    await component.setChecked(true);
    expect(component.uiCheckboxChecked).toBe(true);
  });

  it('syncs boolean state based on various clicks', async () => {
    const component = await createSandboxedComponent(
      { checked: false },
      noMocks(),
    );
    expect(component.checked).toBe(false);

    await component.clickLabel();
    expect(component.checked).toBe(true);

    await component.clickLabel();
    expect(component.checked).toBe(false);

    await component.clickLabel();
    expect(component.checked).toBe(true);

    await component.clickCheckbox();
    expect(component.checked).toBe(false);

    await component.clickCheckbox();
    expect(component.checked).toBe(true);

    await component.clickLabel();
    expect(component.checked).toBe(false);
  });

  it('emits change events', async () => {
    const component = await createSandboxedComponent(
      { checked: true },
      noMocks(),
    );

    await component.clickLabel();
    await component.clickCheckbox();
    await component.clickCheckbox();
    await component.clickCheckbox();
    await component.clickLabel();
    await component.clickLabel();

    expect(component.allCheckedChangeEvents()).toStrictEqual([
      false,
      true,
      false,
      true,
      false,
      true,
    ]);
  });
});

const noMocks = () => [];

const createSandboxedComponent = async (
  initialProps: { checked: boolean },
  mocks: any[],
) => {
  const renderResult = await render(CheckboxSettingComponent, {
    componentProperties: { ...initialProps, localizedTitle: 'Test Checkbox' },
    providers: mocks,
  });

  const checkedChangeSpy = jest.spyOn(
    renderResult.fixture.componentInstance.checkedChange,
    'emit',
  );

  const label = screen.getByText(
    renderResult.fixture.componentInstance.localizedTitle,
  );
  const clickableCheckbox =
    screen.getByTestId<HTMLIonCheckboxElement>('ion-checkbox');
  const user = userEvent.setup();

  return {
    allCheckedChangeEvents: () => checkedChangeSpy.mock.calls.flat(),
    clickLabel: async () => await user.click(label),
    clickCheckbox: async () => await user.click(clickableCheckbox),

    setChecked: async (checked: boolean) =>
      await renderResult.rerender({
        componentInputs: { checked },
        partialUpdate: true,
      }),

    get checked() {
      expect(renderResult.fixture.componentInstance.checked).toBeDefined();
      return renderResult.fixture.componentInstance.checked;
    },

    get uiCheckboxChecked() {
      return clickableCheckbox.checked;
    },
  };
};
