import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { SettingsCardComponent } from './settings-card.component';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '../../test/ui-test.module';

describe('SettingsCardComponent', () => {
  let component: SettingsCardComponent;
  let fixture: ComponentFixture<SettingsCardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        SettingsCardComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(SettingsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
