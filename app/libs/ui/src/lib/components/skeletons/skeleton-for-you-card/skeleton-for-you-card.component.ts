import { Component, Input } from '@angular/core';
import {
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonRow,
  IonSkeletonText,
} from '@ionic/angular/standalone';

@Component({
  selector: 'lib-skeleton-for-you-card',
  templateUrl: './skeleton-for-you-card.component.html',
  styleUrls: ['./skeleton-for-you-card.component.scss'],
  imports: [IonCard, IonRow, IonSkeletonText, IonCardHeader, IonCardContent],
})
export class SkeletonForYouCardComponent {
  @Input() show = false;
}
