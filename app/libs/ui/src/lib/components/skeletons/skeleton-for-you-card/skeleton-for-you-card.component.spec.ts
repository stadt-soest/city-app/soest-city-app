import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SkeletonForYouCardComponent } from './skeleton-for-you-card.component';
import { UITestModule } from '../../../test/ui-test.module';
import { CoreTestModule } from '@sw-code/urbo-core';

describe('SkeletonForYouCardComponent', () => {
  let component: SkeletonForYouCardComponent;
  let fixture: ComponentFixture<SkeletonForYouCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        SkeletonForYouCardComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
      ],
      providers: [],
    }).compileComponents();

    fixture = TestBed.createComponent(SkeletonForYouCardComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
