import { Component, Input } from '@angular/core';
import { NgIf } from '@angular/common';
import { IonCol, IonRow } from '@ionic/angular/standalone';
import { SkeletonForYouCardComponent } from '../skeleton-for-you-card/skeleton-for-you-card.component';
import { NewContentTextComponent } from '../../new-content-text/new-content-text.component';

@Component({
  selector: 'lib-skeleton-feed-loading-for-you',
  templateUrl: './feed-loading-for-you-skeleton.component.html',
  styleUrls: ['./feed-loading-for-you-skeleton.component.scss'],
  imports: [
    NgIf,
    IonRow,
    IonCol,
    SkeletonForYouCardComponent,
    NewContentTextComponent,
  ],
})
export class FeedLoadingForYouSkeletonComponent {
  @Input() show = false;
}
