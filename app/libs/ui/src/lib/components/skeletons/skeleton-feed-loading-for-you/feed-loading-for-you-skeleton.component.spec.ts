import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FeedLoadingForYouSkeletonComponent } from './feed-loading-for-you-skeleton.component';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '../../../test/ui-test.module';
import { getTranslocoModule } from '../../../../transloco-testing.module';

describe('FeedLoadingForYouSkeletonComponent', () => {
  let component: FeedLoadingForYouSkeletonComponent;
  let fixture: ComponentFixture<FeedLoadingForYouSkeletonComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        FeedLoadingForYouSkeletonComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [],
    }).compileComponents();

    fixture = TestBed.createComponent(FeedLoadingForYouSkeletonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
