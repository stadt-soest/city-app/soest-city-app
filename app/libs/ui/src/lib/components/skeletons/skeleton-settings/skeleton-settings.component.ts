import { Component, Input } from '@angular/core';
import { NgFor } from '@angular/common';
import { IonSkeletonText } from '@ionic/angular/standalone';

@Component({
  selector: 'lib-skeleton-settings',
  templateUrl: './skeleton-settings.component.html',
  styleUrls: ['./skeleton-settings.component.scss'],
  imports: [NgFor, IonSkeletonText],
})
export class SkeletonSettingsComponent {
  @Input() skeletonCount = 5;
}
