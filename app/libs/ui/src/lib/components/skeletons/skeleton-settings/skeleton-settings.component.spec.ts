import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { SkeletonSettingsComponent } from './skeleton-settings.component';
import { UITestModule } from '../../../test/ui-test.module';
import { CoreTestModule } from '@sw-code/urbo-core';

describe('SkeletonSettingsComponent', () => {
  let component: SkeletonSettingsComponent;
  let fixture: ComponentFixture<SkeletonSettingsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        SkeletonSettingsComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
      ],
      providers: [],
    }).compileComponents();

    fixture = TestBed.createComponent(SkeletonSettingsComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render correct amount of ion-skeleton-text elements based on skeletonCount', () => {
    const testCount = 5;
    component.skeletonCount = testCount;
    fixture.detectChanges();

    const skeletonElements = fixture.debugElement.queryAll(
      By.css('ion-skeleton-text'),
    );

    expect(skeletonElements.length).toBe(testCount);
  });
});
