import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SkeletonFeedItemComponent } from './skeleton-feed-item.component';
import { UITestModule } from '../../../test/ui-test.module';
import { CoreTestModule } from '@sw-code/urbo-core';

describe('SkeletonFeedItemComponent', () => {
  let component: SkeletonFeedItemComponent;
  let fixture: ComponentFixture<SkeletonFeedItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        SkeletonFeedItemComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
      ],
      providers: [],
    }).compileComponents();

    fixture = TestBed.createComponent(SkeletonFeedItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
