import { Component, Input } from '@angular/core';
import { NgFor } from '@angular/common';
import { IonSkeletonText } from '@ionic/angular/standalone';

@Component({
  selector: 'lib-skeleton-feed-item',
  templateUrl: './skeleton-feed-item.component.html',
  styleUrls: ['./skeleton-feed-item.component.scss'],
  imports: [NgFor, IonSkeletonText],
})
export class SkeletonFeedItemComponent {
  @Input() show = false;
}
