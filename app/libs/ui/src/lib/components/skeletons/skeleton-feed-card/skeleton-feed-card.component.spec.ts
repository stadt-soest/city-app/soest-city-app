import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SkeletonFeedCardComponent } from './skeleton-feed-card.component';
import { UITestModule } from '../../../test/ui-test.module';
import { CoreTestModule } from '@sw-code/urbo-core';

describe('SkeletonFeedCardComponent', () => {
  let component: SkeletonFeedCardComponent;
  let fixture: ComponentFixture<SkeletonFeedCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        SkeletonFeedCardComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
      ],
      providers: [],
    }).compileComponents();

    fixture = TestBed.createComponent(SkeletonFeedCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
