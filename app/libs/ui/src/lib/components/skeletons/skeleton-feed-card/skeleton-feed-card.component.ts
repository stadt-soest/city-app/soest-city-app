import { Component } from '@angular/core';
import {
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonRow,
  IonSkeletonText,
} from '@ionic/angular/standalone';

@Component({
  selector: 'lib-skeleton-feed-card',
  templateUrl: './skeleton-feed-card.component.html',
  styleUrls: ['./skeleton-feed-card.component.scss'],
  imports: [IonCard, IonRow, IonSkeletonText, IonCardHeader, IonCardContent],
})
export class SkeletonFeedCardComponent {}
