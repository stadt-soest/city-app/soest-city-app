import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgIf } from '@angular/common';
import {
  IonInfiniteScroll,
  IonInfiniteScrollContent,
} from '@ionic/angular/standalone';

@Component({
  selector: 'lib-infinite-scroll',
  template: `
    <ion-infinite-scroll
      [attr.data-cy]="'feed-infinite-scroll'"
      (ionInfinite)="onLoadMore($event)"
      *ngIf="visible"
    >
      <ion-infinite-scroll-content></ion-infinite-scroll-content>
    </ion-infinite-scroll>
  `,
  imports: [NgIf, IonInfiniteScroll, IonInfiniteScrollContent],
})
export class InfiniteScrollComponent {
  @Input() visible = false;
  @Output() loadMore = new EventEmitter<any>();

  onLoadMore(event: any) {
    this.loadMore.emit(event);
  }
}
