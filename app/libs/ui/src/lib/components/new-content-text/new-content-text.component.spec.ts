import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NewContentTextComponent } from './new-content-text.component';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '../../test/ui-test.module';
import { getTranslocoModule } from '../../../transloco-testing.module';

describe('NewContentTextComponent', () => {
  let component: NewContentTextComponent;
  let fixture: ComponentFixture<NewContentTextComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NewContentTextComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(NewContentTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
