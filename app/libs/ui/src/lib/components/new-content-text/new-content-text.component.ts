import { Component } from '@angular/core';
import { TranslocoPipe } from '@jsverse/transloco';

@Component({
  selector: 'lib-new-content-text',
  templateUrl: './new-content-text.component.html',
  styleUrls: ['./new-content-text.component.scss'],
  imports: [TranslocoPipe],
})
export class NewContentTextComponent {}
