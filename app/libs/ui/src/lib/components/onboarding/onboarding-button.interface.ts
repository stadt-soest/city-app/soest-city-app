export interface OnboardingButton {
  icon: string;
  text: string;
  action: () => Promise<void> | void;
  selected: boolean;
  shouldApplySelectionStyle?: boolean;
  id?: string;
}
