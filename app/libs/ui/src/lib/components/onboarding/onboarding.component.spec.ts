import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OnboardingComponent } from './onboarding.component';
import { getTranslocoModule } from '../../../transloco-testing.module';

describe('OnboardingComponent', () => {
  let component: OnboardingComponent;
  let fixture: ComponentFixture<OnboardingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [OnboardingComponent, getTranslocoModule()],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OnboardingComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display input data correctly', () => {
    component.imageUrl = '/path/to/image.png';
    component.title = 'Test Title';
    component.content = 'Test Content';
    fixture.detectChanges();

    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('img')?.src).toContain('/path/to/image.png');
    expect(compiled.querySelector('.header')?.textContent).toContain(
      'Test Title',
    );
    expect(compiled.querySelector('.content')?.textContent).toContain(
      'Test Content',
    );
  });
});
