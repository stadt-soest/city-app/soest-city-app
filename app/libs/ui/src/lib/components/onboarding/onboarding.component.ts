import { Component, Input } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { IonText } from '@ionic/angular/standalone';
import { NgFor, NgIf } from '@angular/common';
import { CustomSelectableIconTextButtonComponent } from '../custom-selectable-icon-text-button/custom-selectable-icon-text-button.component';
import { OnboardingButton } from './onboarding-button.interface';
import { ModuleInfo } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-onboarding',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.scss'],
  imports: [
    TranslocoDirective,
    IonText,
    NgIf,
    NgFor,
    CustomSelectableIconTextButtonComponent,
  ],
})
export class OnboardingComponent {
  @Input() imageUrl = '';
  @Input() title = '';
  @Input() content = '';
  @Input() moreContent?: string;
  @Input() moduleInfo?: ModuleInfo;
  @Input() buttons: OnboardingButton[] = [];

  selectButton(index: number) {
    this.buttons = this.buttons.map((button, i) => ({
      ...button,
      selected: i === index,
    }));
    this.buttons[index].action();
  }
}
