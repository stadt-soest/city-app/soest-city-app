import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActionButtonBarComponent } from './action-button-bar.component';

describe('ActionButtonBarComponent', () => {
  let component: ActionButtonBarComponent;
  let fixture: ComponentFixture<ActionButtonBarComponent>;
  let mockActions: any[];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ActionButtonBarComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ActionButtonBarComponent);
    component = fixture.componentInstance;

    mockActions = [
      { label: 'Action 1', onClick: jest.fn() },
      { label: 'Action 2', onClick: jest.fn() },
    ];

    component.actions = mockActions;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
