export interface ActionBarItem {
  icon: string;
  label: string;
  onClick?: () => any;
  selected?: boolean;
  isVisible?: boolean | (() => boolean);
  linkUrl?: string;
}
