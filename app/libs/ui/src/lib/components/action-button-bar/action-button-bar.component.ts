import { Component, Input } from '@angular/core';
import { NgIf } from '@angular/common';
import { CustomSelectableIconTextButtonComponent } from '../custom-selectable-icon-text-button/custom-selectable-icon-text-button.component';
import { ActionBarItem } from './action.interface';
import {
  CustomButtonColorScheme,
  CustomButtonType,
} from '../../enums/custom-button-schemes.types.enum';

@Component({
  selector: 'lib-action-button-bar',
  templateUrl: './action-button-bar.component.html',
  styleUrls: ['./action-button-bar.component.scss'],
  imports: [NgIf, CustomSelectableIconTextButtonComponent],
})
export class ActionButtonBarComponent {
  @Input({ required: true }) actions: ActionBarItem[] = [];
  @Input() wrap = true;
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;

  isActionVisible(action: ActionBarItem): boolean {
    if (typeof action.isVisible === 'function') {
      return action.isVisible();
    }
    return action.isVisible ?? true;
  }

  handleClick(action: ActionBarItem): void {
    if (action.onClick) {
      action.onClick();
    }
  }
}
