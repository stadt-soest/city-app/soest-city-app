import { Component, Input } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { IonCard, IonCardContent } from '@ionic/angular/standalone';
import { NgFor, NgIf, NgStyle } from '@angular/common';
import { CustomSelectableIconTextButtonComponent } from '../custom-selectable-icon-text-button/custom-selectable-icon-text-button.component';
import { CustomSelectableIconTextButton } from '../custom-selectable-icon-text-button/custom-selectable-icon-text-button.interface';

@Component({
  selector: 'lib-notification-card',
  templateUrl: './notification-card.component.html',
  styleUrls: ['./notification-card.component.scss'],
  imports: [
    TranslocoDirective,
    IonCard,
    NgStyle,
    NgIf,
    IonCardContent,
    NgFor,
    CustomSelectableIconTextButtonComponent,
  ],
})
export class NotificationCardComponent {
  @Input() buttons: CustomSelectableIconTextButton[] = [];
  @Input({ required: true }) title = '';
  @Input() titleFontSize = '';
  @Input({ required: true }) titleColor = '';
  @Input() subTitle = '';
  @Input() subTitleColor = '';
  @Input({ required: true }) backgroundColor = '';
}
