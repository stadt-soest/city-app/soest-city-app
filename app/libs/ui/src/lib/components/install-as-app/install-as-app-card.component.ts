import { Component, inject, OnInit } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { IonCard, IonCardContent } from '@ionic/angular/standalone';
import { CustomVerticalButtonComponent } from '../custom-vertical-button/custom-vertical-button.component';
import { ANDROID_STORE_URL, IOS_STORE_URL } from '../../ui-tokens';
import {
  CustomButtonColorScheme,
  CustomButtonType,
} from '../../enums/custom-button-schemes.types.enum';
import { BrowserService, DeviceService } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-install-as-app-card',
  templateUrl: './install-as-app-card.component.html',
  styleUrls: ['./install-as-app-card.component.scss'],
  imports: [
    TranslocoDirective,
    IonCard,
    IonCardContent,
    CustomVerticalButtonComponent,
  ],
})
export class InstallAsAppCardComponent implements OnInit {
  iosStoreUrl = inject(IOS_STORE_URL);
  androidStoreUrl = inject(ANDROID_STORE_URL);
  osName = '';
  protected deviceService = inject(DeviceService);
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
  private readonly browserService = inject(BrowserService);

  ngOnInit() {
    this.osName = this.deviceService.getOsName();
  }

  async openAppstore() {
    if (this.osName === 'iOS') {
      await this.browserService.openInExternalBrowser(this.iosStoreUrl);
    } else if (this.osName === 'Android') {
      await this.browserService.openInExternalBrowser(this.androidStoreUrl);
    }
  }

  showInstallAsAppCard(): boolean {
    return (
      !this.deviceService.isPWA() && !this.deviceService.isNativePlatform()
    );
  }
}
