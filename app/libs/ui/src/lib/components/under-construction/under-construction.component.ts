import { Component, Input } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';

@Component({
  selector: 'lib-under-construction-component',
  template: `
    <div class="custom-component" *transloco="let t">
      <div class="image-container"></div>
      <div class="title">{{ t('ui.under_construction.content') }}</div>
      <div class="content">{{ content }}</div>
      <div class="content">{{ t('ui.under_construction.notice') }}</div>
    </div>
  `,
  styleUrls: ['./under-construction.component.scss'],
  imports: [TranslocoDirective],
})
export class UnderConstructionComponent {
  @Input() content = '';
}
