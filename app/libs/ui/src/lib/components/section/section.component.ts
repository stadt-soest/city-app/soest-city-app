import { Component, Input, Type } from '@angular/core';
import { Observable } from 'rxjs';
import { AsyncPipe, NgComponentOutlet, NgFor, NgIf } from '@angular/common';
import { IonCol, IonRow } from '@ionic/angular/standalone';
import { FeedLoadingForYouSkeletonComponent } from '../skeletons/skeleton-feed-loading-for-you/feed-loading-for-you-skeleton.component';

@Component({
  selector: 'lib-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.scss'],
  imports: [
    NgIf,
    IonRow,
    NgFor,
    IonCol,
    NgComponentOutlet,
    FeedLoadingForYouSkeletonComponent,
    AsyncPipe,
  ],
})
export class SectionComponent {
  @Input() sectionTitle = '';
  @Input() sectionSubtitle?: string;
  @Input() items: SectionItem[] = [];
}

interface SectionItem {
  component: Type<any>;
  visible$?: Observable<boolean>;
}
