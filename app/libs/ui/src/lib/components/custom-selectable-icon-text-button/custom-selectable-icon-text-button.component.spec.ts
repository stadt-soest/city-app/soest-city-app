import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { CustomSelectableIconTextButtonComponent } from './custom-selectable-icon-text-button.component';
import { CoreTestModule } from '@sw-code/urbo-core';
import { getTranslocoModule } from '../../../transloco-testing.module';

describe('CustomRadioButtonComponent', () => {
  let component: CustomSelectableIconTextButtonComponent;
  let fixture: ComponentFixture<CustomSelectableIconTextButtonComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        CustomSelectableIconTextButtonComponent,
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CustomSelectableIconTextButtonComponent);

    component = fixture.componentInstance;

    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should accept input properties', () => {
    component.icon = 'test_icon';
    component.text = 'Test Text';
    component.id = 'test_id';
    component.selected = true;
    component.applySelectionStyle = false;
    fixture.detectChanges();

    expect(component.icon).toBe('test_icon');
    expect(component.text).toBe('Test Text');
    expect(component.id).toBe('test_id');
    expect(component.selected).toBe(true);
    expect(component.applySelectionStyle).toBe(false);
  });

  it('should emit select event on click', () => {
    const emitSpy = jest.spyOn(component.selectedChange, 'emit');

    fixture.debugElement
      .query(By.css('.selectable-icon-text-button'))
      .nativeElement.click();

    expect(emitSpy).toHaveBeenCalledWith(expect.any(MouseEvent));

    emitSpy.mockRestore();
  });

  it('should display icon and text', () => {
    component.icon = 'test_icon';
    component.text = 'Test Text';
    fixture.detectChanges();

    const iconElement = fixture.nativeElement.querySelector('lib-general-icon');
    const textElement = fixture.debugElement.query(
      By.css('.button-text'),
    ).nativeElement;

    expect(iconElement).toBeTruthy();
    expect(textElement.textContent).toContain('Test Text');
  });

  it('should apply selected styles when selected', () => {
    component.selected = true;
    fixture.detectChanges();

    const radioButtonElement = fixture.debugElement.query(
      By.css('.selectable-icon-text-button'),
    ).nativeElement;
    expect(radioButtonElement.classList).toContain('selected');
  });
});
