import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgClass, NgTemplateOutlet } from '@angular/common';
import { GeneralIconComponent } from '../general-icon/general-icon.component';
import { IconFontSize, IconType } from '../../enums/custom-icon.enums';

@Component({
  selector: 'lib-custom-selectable-icon-text-button',
  templateUrl: './custom-selectable-icon-text-button.component.html',
  styleUrls: ['./custom-selectable-icon-text-button.component.scss'],
  imports: [NgTemplateOutlet, NgClass, GeneralIconComponent],
})
export class CustomSelectableIconTextButtonComponent {
  @Input() icon = '';
  @Input() text = '';
  @Input() color?: string;
  @Input() selected = false;
  @Input() id = '';
  @Input() applySelectionStyle = true;
  @Input() linkUrl?: string;
  @Output() selectedChange = new EventEmitter<MouseEvent>();
  protected readonly iconFontSize = IconFontSize;
  protected readonly iconType = IconType;

  async onClick(event: MouseEvent) {
    event.stopPropagation();
    this.selectedChange.emit(event);
  }
}
