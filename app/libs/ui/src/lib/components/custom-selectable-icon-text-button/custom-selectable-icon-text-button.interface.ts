export type CustomSelectableIconTextButton = {
  icon: string;
  text: string;
  selected?: boolean;
  applySelectionStyle?: boolean;
  color?: string;
  onSelect: () => void | Promise<void>;
};
