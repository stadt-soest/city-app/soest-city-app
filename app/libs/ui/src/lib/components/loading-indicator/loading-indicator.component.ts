import { Component, Input } from '@angular/core';
import { NgIf } from '@angular/common';
import { IonSpinner } from '@ionic/angular/standalone';

@Component({
  selector: 'lib-loading-indicator',
  templateUrl: './loading-indicator.component.html',
  styleUrls: ['./loading-indicator.component.scss'],
  imports: [NgIf, IonSpinner],
})
export class LoadingIndicatorComponent {
  @Input() isLoading = false;
}
