import { Component } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  IonCard,
  IonCardContent,
  IonRouterLink,
} from '@ionic/angular/standalone';
import { CustomVerticalButtonComponent } from '../custom-vertical-button/custom-vertical-button.component';
import { RouterLink } from '@angular/router';
import { AbstractErrorCardComponent } from '../error-card/error-card.abstract';
import {
  CustomButtonColorScheme,
  CustomButtonType,
} from '../../enums/custom-button-schemes.types.enum';

@Component({
  selector: 'lib-no-interests-selected-card',
  templateUrl: './no-interests-selected-card.component.html',
  styleUrls: ['./no-interests-selected-card.component.scss'],
  imports: [
    TranslocoDirective,
    IonCard,
    IonCardContent,
    CustomVerticalButtonComponent,
    RouterLink,
    IonRouterLink,
  ],
})
export class NoInterestsSelectedCardComponent extends AbstractErrorCardComponent {
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
}
