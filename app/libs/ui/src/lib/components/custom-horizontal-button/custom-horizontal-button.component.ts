import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgClass, NgIf, NgTemplateOutlet } from '@angular/common';
import {
  CustomButtonBorderScheme,
  CustomButtonColorScheme,
  CustomButtonFontScheme,
  CustomButtonType,
} from '../../enums/custom-button-schemes.types.enum';

@Component({
  selector: 'lib-custom-horizontal-button',
  templateUrl: './custom-horizontal-button.component.html',
  styleUrls: ['./custom-horizontal-button.component.scss'],
  imports: [NgClass, NgIf, NgTemplateOutlet],
  standalone: true,
})
export class CustomHorizontalButtonComponent {
  @Input() title = '';
  @Input() colorScheme: CustomButtonColorScheme =
    CustomButtonColorScheme.inherit;
  @Input() borderScheme: CustomButtonBorderScheme =
    CustomButtonBorderScheme.transparentBorder;
  @Input() fontScheme: CustomButtonFontScheme = CustomButtonFontScheme.default;
  @Input() endIcon = '';
  @Input() startIconColor: 'red' | 'orange' | 'green' | 'main' | 'default' =
    'default';
  @Input() startIcon = '';
  @Input() buttonType: CustomButtonType = CustomButtonType.button;
  @Input() buttonHref?: string;
  @Input() fullWidth = true;
  @Input() noPadding = false;
  @Output() buttonClick = new EventEmitter<MouseEvent>();
  @Output() linkClick = new EventEmitter<MouseEvent>();
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonBorderScheme = CustomButtonBorderScheme;
  protected readonly customButtonType = CustomButtonType;
  protected readonly customButtonFontScheme = CustomButtonFontScheme;

  getButtonClasses() {
    return {
      'button-strong': this.colorScheme === this.customButtonColorScheme.strong,
      'button-hint': this.colorScheme === this.customButtonColorScheme.hint,
      'button-hint-secondary':
        this.colorScheme === this.customButtonColorScheme.hintSecondary,
      'button-outlined':
        this.colorScheme === this.customButtonColorScheme.outlined,
      'button-transparent':
        this.colorScheme === this.customButtonColorScheme.transparent,
      'button-info': this.colorScheme === this.customButtonColorScheme.info,
      'button-highlight':
        this.colorScheme === this.customButtonColorScheme.highlight,
      'button-hint-border':
        this.borderScheme === this.customButtonBorderScheme.hintBorder,
      'button-transparent-border':
        this.borderScheme === this.customButtonBorderScheme.transparentBorder,
      'button-accent-red':
        this.colorScheme === this.customButtonColorScheme.accentRed,
      'full-width': this.fullWidth,
      'no-padding':
        this.noPadding && this.buttonType === this.customButtonType.button,
      'add-gap':
        this.buttonType === this.customButtonType.link && !this.fullWidth,
    };
  }

  onLinkClick(event: MouseEvent): void {
    event.preventDefault();
    this.linkClick.emit(event);
  }
}
