import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CustomHorizontalButtonComponent } from './custom-horizontal-button.component';
import { By } from '@angular/platform-browser';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '../../test/ui-test.module';
import {
  CustomButtonBorderScheme,
  CustomButtonColorScheme,
  CustomButtonType,
} from '../../enums/custom-button-schemes.types.enum';

describe('CustomHorizontalButtonComponent', () => {
  let component: CustomHorizontalButtonComponent;
  let fixture: ComponentFixture<CustomHorizontalButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        CustomHorizontalButtonComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
      ],
      providers: [],
    }).compileComponents();

    fixture = TestBed.createComponent(CustomHorizontalButtonComponent);
    component = fixture.componentInstance;
    component.startIcon = 'start_icon';
    component.startIcon = 'end_icon';
    component.title = 'Test Button';
    component.colorScheme = CustomButtonColorScheme.strong;
    component.borderScheme = CustomButtonBorderScheme.hintBorder;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display the correct icons and title', () => {
    component.startIcon = 'start_icon';
    component.endIcon = 'end_icon';
    component.title = 'My Button';
    fixture.detectChanges();

    const startIcon = fixture.debugElement.query(
      By.css('.iconStart'),
    ).nativeElement;
    const endIcon = fixture.debugElement.query(
      By.css('.iconEnd'),
    ).nativeElement;
    const title = fixture.debugElement.query(
      By.css('.custom-button-text'),
    ).nativeElement;

    expect(startIcon.textContent).toContain('start_icon');
    expect(endIcon.textContent).toContain('end_icon');
    expect(title.textContent).toContain('My Button');
  });

  it('should render a link when actionConfig.type is link', () => {
    component.buttonType = CustomButtonType.link;
    component.buttonHref = 'mock-href';
    fixture.detectChanges();
    const button = fixture.debugElement.query(By.css('button'));
    const link = fixture.debugElement.query(By.css('a'));
    expect(button).toBeNull();
    expect(link).not.toBeNull();
    expect(link.attributes['href']).toBe('mock-href');
  });

  it('should render a button when actionConfig.type is button', () => {
    component.buttonType = CustomButtonType.button;
    fixture.detectChanges();
    const button = fixture.debugElement.query(By.css('button'));
    const link = fixture.debugElement.query(By.css('a'));
    expect(button).not.toBeNull();
    expect(link).toBeNull();
  });
});
