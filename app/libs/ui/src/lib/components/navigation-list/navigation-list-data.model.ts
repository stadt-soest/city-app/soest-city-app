export class NavigationListData {
  category: string;
  menuItems: MenuItem[];

  constructor(category: string, menuItems: MenuItem[]) {
    this.category = category;
    this.menuItems = menuItems;
  }
}

export class MenuItem {
  title: string;
  icon: string;
  baseRoutingPath: string;

  constructor(title: string, icon: string, baseRoutingPath: string) {
    this.title = title;
    this.icon = icon;
    this.baseRoutingPath = baseRoutingPath;
  }
}
