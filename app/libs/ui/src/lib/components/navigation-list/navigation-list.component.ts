import { Component, Input } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  IonLabel,
  IonList,
  IonRouterLinkWithHref,
} from '@ionic/angular/standalone';
import { NgFor } from '@angular/common';
import { RouterLink } from '@angular/router';
import { GeneralIconComponent } from '../general-icon/general-icon.component';
import { NavigationListData } from './navigation-list-data.model';
import {
  IconColor,
  IconFontSize,
  IconType,
} from '../../enums/custom-icon.enums';

@Component({
  selector: 'lib-shared-navigation-list',
  templateUrl: './navigation-list.component.html',
  styleUrls: ['./navigation-list.component.scss'],
  imports: [
    TranslocoDirective,
    IonList,
    NgFor,
    IonRouterLinkWithHref,
    RouterLink,
    GeneralIconComponent,
    IonLabel,
  ],
})
export class NavigationListComponent {
  @Input() menuItemGroups: NavigationListData[] = [];
  protected readonly iconType = IconType;
  protected readonly iconFontSize = IconFontSize;
  protected readonly iconColor = IconColor;
}
