import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NavigationListComponent } from './navigation-list.component';
import { MenuItem, NavigationListData } from './navigation-list-data.model';
import { CoreTestModule } from '@sw-code/urbo-core';
import { getTranslocoModule } from '../../../transloco-testing.module';

describe('MenuListComponent', () => {
  let component: NavigationListComponent;
  let fixture: ComponentFixture<NavigationListComponent>;

  beforeEach(async () => {
    await setupTestBed();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display the correct number of categories', () => {
    const categoryHeaders = fixture.debugElement.queryAll(
      By.css('.category-header'),
    );
    expect(categoryHeaders.length).toBe(2);
  });

  it('should display the correct number of menu items for each category', () => {
    const menuItemsContainers = fixture.debugElement.queryAll(
      By.css('.label-checkbox-container'),
    );
    expect(menuItemsContainers.length).toBe(3);
  });

  it('should correctly display icons, excluding forward icons', () => {
    const menuItemIcons = fixture.debugElement.queryAll(
      By.css('.menu-item-icon'),
    );

    expect(menuItemIcons.length).toBe(3);
  });

  it('should correctly bind router links without encoding slashes', () => {
    const links = fixture.debugElement.queryAll(By.css('a'));

    expect(
      decodeURIComponent(links[0].nativeElement.getAttribute('href')),
    ).toBe('/home');
    expect(
      decodeURIComponent(links[1].nativeElement.getAttribute('href')),
    ).toBe('/settings');
    expect(
      decodeURIComponent(links[2].nativeElement.getAttribute('href')),
    ).toBe('/info');
  });

  const createTestMenuListData = (): NavigationListData[] => [
    new NavigationListData('Category 1', [
      new MenuItem('Item 1', 'home', '/home'),
      new MenuItem('Item 2', 'settings', '/settings'),
    ]),
    new NavigationListData('Category 2', [
      new MenuItem('Item 3', 'info', '/info'),
    ]),
  ];

  const setupTestBed = async (): Promise<void> => {
    await TestBed.configureTestingModule({
      imports: [
        NavigationListComponent,
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(NavigationListComponent);
    component = fixture.componentInstance;
    component.menuItemGroups = createTestMenuListData();
    fixture.detectChanges();
  };
});
