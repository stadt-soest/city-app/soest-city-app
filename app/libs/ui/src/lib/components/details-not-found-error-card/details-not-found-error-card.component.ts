import { Component, DestroyRef, inject, Input, OnInit } from '@angular/core';
import { filter, Observable, of, switchMap } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { NgIf } from '@angular/common';
import { IonCard } from '@ionic/angular/standalone';
import { AbstractErrorCardComponent } from '../error-card/error-card.abstract';
import { ErrorHandlingService, ModuleInfo } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-details-not-found-error-handling-card',
  templateUrl: './details-not-found-error-card.component.html',
  styleUrls: ['./details-not-found-error-card.component.scss'],
  imports: [NgIf, IonCard],
})
export class DetailsNotFoundErrorCardComponent
  extends AbstractErrorCardComponent
  implements OnInit
{
  @Input({ required: true }) detailsId = '';
  @Input({ required: true }) moduleInfo!: ModuleInfo;
  @Input({ required: true }) hasFeedItems$: Observable<boolean> = of();
  @Input({ required: true }) title = '';
  @Input({ required: true }) content = '';
  private readonly errorHandlingService = inject(ErrorHandlingService);
  private readonly destroyRef = inject(DestroyRef);

  ngOnInit(): void {
    this.hasFeedItems$
      .pipe(
        takeUntilDestroyed(this.destroyRef),
        filter((hasFeedItems) => hasFeedItems),
        switchMap(() =>
          this.errorHandlingService.hasErrorInDetails(
            this.moduleInfo,
            this.detailsId,
          ),
        ),
      )
      .subscribe((hasErrorInDetails) => {
        this.show = hasErrorInDetails;
      });
  }
}
