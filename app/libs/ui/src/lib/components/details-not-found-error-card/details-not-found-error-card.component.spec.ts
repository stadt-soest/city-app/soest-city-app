import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { DetailsNotFoundErrorCardComponent } from './details-not-found-error-card.component';
import { ErrorHandlingService } from '@sw-code/urbo-core';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '../../test/ui-test.module';
import { getTranslocoModule } from '../../../transloco-testing.module';

describe('DetailsNotFoundErrorCardComponent', () => {
  let component: DetailsNotFoundErrorCardComponent;
  let fixture: ComponentFixture<DetailsNotFoundErrorCardComponent>;
  let mockErrorHandlingService: jest.Mocked<ErrorHandlingService>;

  beforeEach(() => {
    mockErrorHandlingService = {
      hasErrorInDetails: jest.fn(),
    } as any;

    TestBed.configureTestingModule({
      imports: [
        DetailsNotFoundErrorCardComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        { provide: ErrorHandlingService, useValue: mockErrorHandlingService },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(DetailsNotFoundErrorCardComponent);
    component = fixture.componentInstance;
    component.hasFeedItems$ = of(true);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should set show to true if hasFeedItems$ is true and hasErrorInDetails is true', () => {
      mockErrorHandlingService.hasErrorInDetails.mockImplementationOnce(() =>
        of(true),
      );
      component.ngOnInit();
      expect(component.show).toBe(true);
    });

    it('should set show to false if hasFeedItems$ is true and hasErrorInDetails is false', () => {
      mockErrorHandlingService.hasErrorInDetails.mockImplementationOnce(() =>
        of(false),
      );
      component.ngOnInit();
      expect(component.show).toBe(false);
    });

    it('should set show to false if hasFeedItems$ is false and hasErrorInDetails is true', () => {
      component.hasFeedItems$ = of(false);
      component.ngOnInit();
      expect(component.show).toBe(false);
    });

    it('should set show to false if hasFeedItems$ is false and hasErrorInDetails is false', () => {
      component.hasFeedItems$ = of(false);
      mockErrorHandlingService.hasErrorInDetails.mockImplementationOnce(() =>
        of(false),
      );
      component.ngOnInit();
      expect(component.show).toBe(false);
    });
  });

  it('should call window.location.reload on onReloadClick', () => {
    const updateClickSpy = jest.spyOn(component, 'onRefreshClick');
    component.onRefreshClick();
    expect(updateClickSpy).toHaveBeenCalled();
  });
});
