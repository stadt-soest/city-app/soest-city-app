import {
  Component,
  EventEmitter,
  inject,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import { Location, NgClass, NgIf } from '@angular/common';
import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonRouterLinkWithHref,
  IonTitle,
  IonToolbar,
} from '@ionic/angular/standalone';
import { GeneralIconComponent } from '../general-icon/general-icon.component';
import { RouterLink } from '@angular/router';
import {
  CustomButtonColorScheme,
  CustomButtonType,
} from '../../enums/custom-button-schemes.types.enum';
import {
  IconColor,
  IconFontSize,
  IconType,
} from '../../enums/custom-icon.enums';

@Component({
  selector: 'lib-condensed-header-layout',
  templateUrl: './condensed-header-layout.component.html',
  styleUrls: ['./condensed-header-layout.component.scss'],
  imports: [
    TranslocoDirective,
    IonHeader,
    NgClass,
    IonToolbar,
    IonButtons,
    NgIf,
    IonButton,
    GeneralIconComponent,
    IonBackButton,
    IonTitle,
    IonRouterLinkWithHref,
    RouterLink,
    IonContent,
  ],
})
export class CondensedHeaderLayoutComponent implements OnInit {
  @Input() useMaximumBackground = false;
  @Input() pageTitle = '';
  @Input() showBackButton = true;
  @Input() settingsPageRoute = '';
  @Input() showSettingsButton = false;
  @Input() useContentFont = false;
  @Input() removeEdgeSpacing = false;
  @Input() titleLang = '';
  @Input() scrollableY = true;
  @Output() backButtonClick = new EventEmitter<void>();
  @Output() settingsMenu: EventEmitter<void> = new EventEmitter<void>();
  @ViewChild('content') content?: HTMLIonContentElement;
  isScrolled = false;
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
  protected readonly iconType = IconType;
  protected readonly iconFontSize = IconFontSize;
  protected readonly iconColor = IconColor;
  private readonly translateService = inject(TranslocoService);
  private readonly location = inject(Location);

  get defaultHref(): string {
    const path = this.location.path();
    if (!path) {
      return '/';
    }

    const segments = path.split('/');
    if (segments.length > 1) {
      segments.pop();
      return segments.join('/') || '/';
    }

    return '/';
  }

  get headerBackgroundStyle(): string {
    if (this.useMaximumBackground) {
      return this.isScrolled
        ? 'var(--Elevated-Background)'
        : 'var(--Maximum-Background)';
    } else {
      return this.isScrolled
        ? 'var(--Elevated-Background-On-Color)'
        : 'var(--Colored-Background)';
    }
  }

  ngOnInit(): void {
    this.titleLang = this.translateService.getActiveLang();
  }

  async scrollToTop(duration = 0) {
    await this.content?.scrollToTop(duration);
  }

  emitButtonClick() {
    this.backButtonClick.emit();
  }

  onContentScroll(event: any) {
    const pixelThreshold = 1;
    this.isScrolled = event.detail.scrollTop > pixelThreshold;
  }
}
