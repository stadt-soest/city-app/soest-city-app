import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MenuController } from '@ionic/angular/standalone';
import { CondensedHeaderLayoutComponent } from './condensed-header-layout.component';
import { UITestModule } from '../../test/ui-test.module';
import { getTranslocoModule } from '../../../transloco-testing.module';

describe('CondensedHeaderLayoutComponent', () => {
  let component: CondensedHeaderLayoutComponent;
  let fixture: ComponentFixture<CondensedHeaderLayoutComponent>;
  let mockMenuController: jest.Mocked<MenuController>;

  beforeEach(waitForAsync(() => {
    mockMenuController = {
      open: jest.fn(),
    } as never;

    TestBed.configureTestingModule({
      imports: [
        CondensedHeaderLayoutComponent,
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [{ provide: MenuController, useValue: mockMenuController }],
    }).compileComponents();

    fixture = TestBed.createComponent(CondensedHeaderLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onContentScroll', () => {
    it('should set isScrolled to true when scrolled past threshold', () => {
      const eventAboveThreshold = { detail: { scrollTop: 50 } };
      component.onContentScroll(eventAboveThreshold);
      expect(component.isScrolled).toBe(true);
    });

    it('should set isScrolled to false when not scrolled past threshold', () => {
      const eventBelowThreshold = { detail: { scrollTop: 0 } };
      component.onContentScroll(eventBelowThreshold);
      expect(component.isScrolled).toBe(false);
    });
  });
});
