import { Component, Input } from '@angular/core';

@Component({
  template: '',
  standalone: false,
})
export abstract class AbstractErrorCardComponent {
  @Input() show = false;

  async onRefreshClick() {
    window.location.reload();
  }
}
