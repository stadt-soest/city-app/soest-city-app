import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CustomVerticalButtonComponent } from './custom-vertical-button.component';
import { By } from '@angular/platform-browser';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '../../test/ui-test.module';
import {
  CustomButtonColorScheme,
  CustomButtonType,
} from '../../enums/custom-button-schemes.types.enum';

describe('CustomVerticalButtonComponent', () => {
  let component: CustomVerticalButtonComponent;
  let fixture: ComponentFixture<CustomVerticalButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        CustomVerticalButtonComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
      ],
      providers: [],
    }).compileComponents();

    fixture = TestBed.createComponent(CustomVerticalButtonComponent);
    component = fixture.componentInstance;
    component.routerLink = 'mock-href';
    component.buttonType = CustomButtonType.link;
    component.startIcon = 'start_icon';
    component.startIcon = 'end_icon';
    component.title = 'Test Button';
    component.colorScheme = CustomButtonColorScheme.strong;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render a link when actionConfig.type is link', () => {
    component.buttonType = CustomButtonType.link;
    component.routerLink = 'mock-href';
    fixture.detectChanges();
    const button = fixture.debugElement.query(By.css('button'));
    const link = fixture.debugElement.query(By.css('a'));
    expect(button).toBeNull();
    expect(link).not.toBeNull();
    expect(link.attributes['href']).toBe('/mock-href');
  });

  it('should render a button when actionConfig.type is button', () => {
    component.buttonType = CustomButtonType.button;
    fixture.detectChanges();
    const button = fixture.debugElement.query(By.css('button'));
    const link = fixture.debugElement.query(By.css('a'));
    expect(button).not.toBeNull();
    expect(link).toBeNull();
  });
});
