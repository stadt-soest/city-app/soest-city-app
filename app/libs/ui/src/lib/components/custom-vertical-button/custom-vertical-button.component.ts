import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgClass, NgTemplateOutlet } from '@angular/common';
import { CustomButtonContentComponent } from './sub-components/custom-button-content/custom-button-content.component';
import { IonRouterLinkWithHref } from '@ionic/angular/standalone';
import { RouterLink } from '@angular/router';
import {
  CustomButtonColorScheme,
  CustomButtonType,
} from '../../enums/custom-button-schemes.types.enum';

@Component({
  selector: 'lib-custom-vertical-button',
  templateUrl: './custom-vertical-button.component.html',
  styleUrls: ['./custom-vertical-button.component.scss'],
  imports: [
    NgTemplateOutlet,
    NgClass,
    CustomButtonContentComponent,
    IonRouterLinkWithHref,
    RouterLink,
  ],
})
export class CustomVerticalButtonComponent {
  @Output() buttonClick = new EventEmitter<MouseEvent>();
  @Input({ required: true }) title = '';
  @Input({ required: true }) startIcon = '';
  @Input() colorScheme: CustomButtonColorScheme = CustomButtonColorScheme.main;
  @Input() buttonType: CustomButtonType = CustomButtonType.button;
  @Input() routerLink?: string | any[];
  @Input() replaceUrl = false;
  @Input() externalUrl?: string;
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;

  getButtonClasses() {
    return {
      'button-inherit':
        this.colorScheme === this.customButtonColorScheme.inherit,
      'button-main': this.colorScheme === this.customButtonColorScheme.main,
    };
  }
}
