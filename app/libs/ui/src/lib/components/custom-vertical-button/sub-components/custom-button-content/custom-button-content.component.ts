import { Component, Input } from '@angular/core';
import { GeneralIconComponent } from '../../../general-icon/general-icon.component';
import { IconFontSize, IconType } from '../../../../enums/custom-icon.enums';

@Component({
  selector: 'lib-custom-button-content',
  templateUrl: './custom-button-content.component.html',
  styleUrls: ['./custom-button-content.component.scss'],
  imports: [GeneralIconComponent],
})
export class CustomButtonContentComponent {
  @Input({ required: true }) title = '';
  @Input({ required: true }) startIcon = '';
  protected readonly iconType = IconType;
  protected readonly iconFontSize = IconFontSize;
}
