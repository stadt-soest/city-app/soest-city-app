import { render, screen } from '@testing-library/angular';
import { CustomButtonContentComponent } from './custom-button-content.component';
import { GeneralIconComponent } from '../../../general-icon/general-icon.component';

describe('CustomButtonContentComponent', () => {
  it('should create', async () => {
    const { fixture } = await render(CustomButtonContentComponent);

    expect(fixture.componentInstance).toBeTruthy();
  });

  it('should display the correct icon and title', async () => {
    const { container } = await render(CustomButtonContentComponent, {
      imports: [GeneralIconComponent],
      componentProperties: {
        startIcon: 'start_icon',
        title: 'My Button',
      },
    });

    const startIcon = container.querySelector('lib-general-icon');
    const title = screen.getByText('My Button');

    expect(startIcon).not.toBeNull();
    expect(title).not.toBeNull();
  });
});
