import { Component, inject, OnInit, Type } from '@angular/core';
import { catchError, forkJoin, of } from 'rxjs';
import { NgComponentOutlet, NgFor } from '@angular/common';
import { ModuleRegistryService } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-module-map-buttons',
  templateUrl: './module-map-buttons.component.html',
  styleUrls: ['./module-map-buttons.component.scss'],
  imports: [NgFor, NgComponentOutlet],
})
export class ModuleMapButtonsComponent implements OnInit {
  customMapButtonComponents: Type<any>[] = [];
  private readonly moduleRegistry = inject(ModuleRegistryService);

  ngOnInit(): void {
    const mapModules = this.moduleRegistry.getModulesByCustomMapButton();

    if (mapModules && mapModules.length > 0) {
      const componentObservables = mapModules.map((module) =>
        module.getCustomMapButton(),
      );

      forkJoin(componentObservables)
        .pipe(
          catchError((error) => {
            console.error('Error loading custom map button components:', error);
            return of([]);
          }),
        )
        .subscribe((results) => {
          this.customMapButtonComponents = results.map(
            (result) => result.component,
          );
        });
    }
  }
}
