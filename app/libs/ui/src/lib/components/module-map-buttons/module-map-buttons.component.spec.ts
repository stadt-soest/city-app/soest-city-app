import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ModuleMapButtonsComponent } from './module-map-buttons.component';
import { UITestModule } from '../../test/ui-test.module';
import { CoreTestModule } from '@sw-code/urbo-core';

describe('ModuleMapButtonsComponent', () => {
  let component: ModuleMapButtonsComponent;
  let fixture: ComponentFixture<ModuleMapButtonsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        ModuleMapButtonsComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ModuleMapButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
