import { Component, inject, Input } from '@angular/core';
import { DatePipe, SlicePipe } from '@angular/common';
import { TimeFormatPipe } from '../../pipes/time-format.pipe.ts/time-format.pipe';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import {
  DayOfWeek,
  getCurrentDayOfWeek,
  OpeningHoursMap,
  SpecialOpeningHoursMap,
} from '@sw-code/urbo-core';
import { CustomHorizontalButtonComponent } from '../custom-horizontal-button/custom-horizontal-button.component';
import {
  CustomButtonColorScheme,
  CustomButtonType,
} from '../../enums/custom-button-schemes.types.enum';
import { UIModule } from '../../ui.module';
import {
  startOfWeek,
  addDays,
  parseISO,
  isSameDay,
  isAfter,
  isBefore,
  compareAsc,
  addWeeks,
  format,
} from 'date-fns';

@Component({
  selector: 'lib-opening-times',
  templateUrl: './opening-times.component.html',
  styleUrls: ['./opening-times.component.scss'],
  standalone: true,
  imports: [
    UIModule,
    TimeFormatPipe,
    TranslocoDirective,
    DatePipe,
    SlicePipe,
    CustomHorizontalButtonComponent,
  ],
})
export class OpeningTimesComponent {
  @Input() openingTimes: OpeningHoursMap = {
    [DayOfWeek.MONDAY]: { from: '', to: '' },
    [DayOfWeek.TUESDAY]: { from: '', to: '' },
    [DayOfWeek.WEDNESDAY]: { from: '', to: '' },
    [DayOfWeek.THURSDAY]: { from: '', to: '' },
    [DayOfWeek.FRIDAY]: { from: '', to: '' },
    [DayOfWeek.SATURDAY]: { from: '', to: '' },
    [DayOfWeek.SUNDAY]: { from: '', to: '' },
  };
  @Input() openAllWeek = false;
  @Input() specialOpeningTimes: SpecialOpeningHoursMap = {};
  showAllSpecialTimes = false;

  protected readonly customButtonType = CustomButtonType;
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly getCurrentDayOfWeek = getCurrentDayOfWeek;
  private readonly translocoService = inject(TranslocoService);

  private getStartOfCurrentWeek(): Date {
    return startOfWeek(new Date(), { weekStartsOn: 1 });
  }

  private getStartOfNextWeek(): Date {
    return addWeeks(this.getStartOfCurrentWeek(), 1);
  }

  get specialOpeningTimesArray() {
    return Object.entries(this.specialOpeningTimes)
      .map(([date, times]) => ({
        date: parseISO(date),
        ...times,
      }))
      .sort((a, b) => compareAsc(a.date, b.date));
  }

  get currentWeekSpecialOpeningTimes() {
    const startOfWeekDate = this.getStartOfCurrentWeek();
    const startOfNextWeekDate = this.getStartOfNextWeek();

    return this.specialOpeningTimesArray.filter(
      ({ date }) =>
        (isAfter(date, startOfWeekDate) || isSameDay(date, startOfWeekDate)) &&
        isBefore(date, startOfNextWeekDate),
    );
  }

  get futureSpecialOpeningTimes() {
    const startOfNextWeekDate = this.getStartOfNextWeek();

    return this.specialOpeningTimesArray.filter(
      ({ date }) =>
        isAfter(date, startOfNextWeekDate) ||
        isSameDay(date, startOfNextWeekDate),
    );
  }

  get openingTimesWithSpecials() {
    const startOfWeekDate = this.getStartOfCurrentWeek();
    const dayNames = [
      DayOfWeek.MONDAY,
      DayOfWeek.TUESDAY,
      DayOfWeek.WEDNESDAY,
      DayOfWeek.THURSDAY,
      DayOfWeek.FRIDAY,
      DayOfWeek.SATURDAY,
      DayOfWeek.SUNDAY,
    ];

    return dayNames.map((day, index) => {
      const date = addDays(startOfWeekDate, index);
      const regularHours = this.openingTimes[day];
      const specialForDay = this.currentWeekSpecialOpeningTimes.find(
        (special) => isSameDay(special.date, date),
      );

      return {
        day,
        date,
        regularHours,
        special: specialForDay,
      };
    });
  }

  getSpecialDescription(
    descriptions: { value: string; language: string }[],
  ): string {
    const currentLang = this.translocoService.getActiveLang().toUpperCase();
    const description = descriptions.find(
      (desc) => desc.language === currentLang,
    );
    return description ? description.value : '';
  }

  getRegularHoursForDate(date: Date): { from: string; to: string } {
    const dayNames = [
      DayOfWeek.MONDAY,
      DayOfWeek.TUESDAY,
      DayOfWeek.WEDNESDAY,
      DayOfWeek.THURSDAY,
      DayOfWeek.FRIDAY,
      DayOfWeek.SATURDAY,
      DayOfWeek.SUNDAY,
    ];
    const dayName = dayNames[date.getDay() === 0 ? 6 : date.getDay() - 1];
    return this.openingTimes[dayName];
  }

  formatDateWithoutYear(date: Date): string {
    return format(date, 'dd.MM.');
  }

  isValidHours(hours: { from: string; to: string }): boolean {
    if (!hours.from || !hours.to) {
      return false;
    }

    return !(hours.from === '00:00:00' && hours.to === '00:00:00');
  }
}
