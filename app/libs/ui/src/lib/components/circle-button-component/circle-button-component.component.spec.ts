import { fireEvent, render, screen } from '@testing-library/angular';
import { CircleButtonComponent } from './circle-button-component.component';
import { GeneralIconComponent } from '../general-icon/general-icon.component';
import '@testing-library/jest-dom';

describe('CircleButtonComponent with Real AppGeneralIconComponent', () => {
  it('should render the circle button component', async () => {
    await render(CircleButtonComponent, {
      imports: [GeneralIconComponent],
      inputs: {
        iconName: 'test-icon',
      },
    });

    const button = screen.getByRole('button');
    expect(button).not.toBeNull();
  });

  it('should render the app-general-icon with correct name', async () => {
    await render(CircleButtonComponent, {
      imports: [GeneralIconComponent],
      inputs: {
        iconName: 'test-icon',
      },
    });

    const icon = screen.getByTestId('general-icon');
    expect(icon.textContent?.trim()).toBe('test-icon');
  });

  it('should emit buttonClick event when button is clicked', async () => {
    const handleClick = jest.fn();

    await render(CircleButtonComponent, {
      imports: [GeneralIconComponent],
      inputs: {
        iconName: 'test-icon',
      },
      on: {
        buttonClick: handleClick,
      },
    });

    const button = screen.getByRole('button');
    fireEvent.click(button);
    expect(handleClick).toHaveBeenCalledTimes(1);
  });
});
