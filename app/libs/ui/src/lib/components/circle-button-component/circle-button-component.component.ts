import { Component, EventEmitter, Input, Output } from '@angular/core';
import { GeneralIconComponent } from '../general-icon/general-icon.component';
import { IconFontSize } from '../../enums/custom-icon.enums';

@Component({
  selector: 'lib-circle-button',
  template: `
    <div class="custom-circle-button-container">
      <button class="circle-button" (click)="handleClick()">
        <lib-general-icon
          data-testid="general-icon"
          [name]="iconName"
          [size]="iconFontSize.large"
        ></lib-general-icon>
      </button>
    </div>
  `,
  styleUrls: ['circle-button-component.component.scss'],
  imports: [GeneralIconComponent],
})
export class CircleButtonComponent {
  @Input() iconName = '';
  @Output() buttonClick = new EventEmitter<void>();
  protected readonly iconFontSize = IconFontSize;

  handleClick() {
    this.buttonClick.emit();
  }
}
