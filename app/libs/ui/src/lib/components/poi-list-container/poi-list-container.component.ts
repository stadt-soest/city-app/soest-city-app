import { Component, Input } from '@angular/core';

import { mainMapDistanceThresholds } from '@sw-code/urbo-core';
import { DistanceFormatPipe } from '../../pipes/distance/distance-format.pipe';
import {
  IconColor,
  IconFontSize,
  IconType,
} from '../../enums/custom-icon.enums';
import { UIModule } from '../../ui.module';
import { GeneralIconComponent } from '../general-icon/general-icon.component';

@Component({
  selector: 'lib-poi-list-container',
  templateUrl: './poi-list-container.component.html',
  styleUrls: ['./poi-list-container.component.scss'],
  standalone: true,
  imports: [UIModule, DistanceFormatPipe, GeneralIconComponent],
})
export class PoiListContainerComponent {
  @Input() title = '';
  @Input() distance?: number;

  protected readonly iconType = IconType;
  protected readonly iconFontSize = IconFontSize;
  protected readonly iconColor = IconColor;

  getDistanceIcon(distance: number): string {
    if (distance <= mainMapDistanceThresholds.near) {
      return 'stat_1';
    }
    if (distance < mainMapDistanceThresholds.medium) {
      return 'stat_2';
    }
    return 'stat_3';
  }
}
