import { Component, inject, Input, OnInit } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { IonList, IonListHeader, IonRow } from '@ionic/angular/standalone';
import { NgFor, NgIf } from '@angular/common';
import { SkeletonSettingsComponent } from '../skeletons/skeleton-settings/skeleton-settings.component';
import { CustomHorizontalButtonComponent } from '../custom-horizontal-button/custom-horizontal-button.component';
import { CheckboxSettingComponent } from '../checkbox-setting/checkbox-setting.component';
import {
  CustomButtonColorScheme,
  CustomButtonType,
} from '../../enums/custom-button-schemes.types.enum';
import {
  FeedUpdateService,
  JsonParsingService,
  ModuleInfo,
  ModuleSettings,
  SettingsPersistenceService,
  StorageKeyType,
} from '@sw-code/urbo-core';

@Component({
  selector: 'lib-custom-settings-list',
  templateUrl: './custom-settings-list.component.html',
  styleUrls: ['./custom-settings-list.component.scss'],
  imports: [
    TranslocoDirective,
    IonList,
    IonListHeader,
    NgIf,
    SkeletonSettingsComponent,
    IonRow,
    CustomHorizontalButtonComponent,
    NgFor,
    CheckboxSettingComponent,
  ],
})
export class CustomSettingsListComponent implements OnInit {
  @Input() title = '';
  @Input() categoryText = '';
  @Input() items: ModuleSettings[] = [];
  @Input({ required: true }) moduleInfo!: ModuleInfo;
  @Input({ required: true }) storageKeyType!: StorageKeyType;
  @Input() isLoading = true;
  @Input() skeletonCount = 5;
  settings: { [key: string]: boolean } = {};
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
  private readonly jsonParsingService = inject(JsonParsingService);
  private readonly feedUpdateService = inject(FeedUpdateService);
  private readonly settingsPersistenceService = inject(
    SettingsPersistenceService,
  );

  ngOnInit() {
    this.loadSettings();
  }

  async loadSettings() {
    const savedSettings = await this.settingsPersistenceService.getSettings(
      this.moduleInfo,
      this.storageKeyType,
    );

    if (savedSettings) {
      this.settings =
        (await this.jsonParsingService.parseJson(savedSettings)) ?? {};
    }
  }

  async switchCheckbox(item: ModuleSettings) {
    const settingKey = item.id ?? item.name;
    this.settings[settingKey] = !this.settings[settingKey];
    await this.saveSettings();
  }

  async activateAllSettings() {
    this.items.forEach((item) => {
      const settingKey = item.id ?? item.name;
      this.settings[settingKey] = true;
    });

    await this.saveSettings();
  }

  async deactivateAllSettings() {
    this.items.forEach((item) => {
      const settingKey = item.id ?? item.name;
      this.settings[settingKey] = false;
    });

    await this.saveSettings();
  }

  async saveSettings() {
    await this.settingsPersistenceService.saveSettings(
      this.moduleInfo,
      this.storageKeyType,
      this.settings,
    );

    this.feedUpdateService.notifyFeedUpdate();
  }
}
