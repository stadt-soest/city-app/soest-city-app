import { TestBed, TestModuleMetadata } from '@angular/core/testing';
import { CustomSettingsListComponent } from './custom-settings-list.component';
import {
  CoreTestModule,
  ModuleSettings,
  SettingsPersistenceService,
} from '@sw-code/urbo-core';
import { By } from '@angular/platform-browser';
import { CheckboxSettingComponent } from '../checkbox-setting/checkbox-setting.component';
import { UITestModule } from '../../test/ui-test.module';
import { getTranslocoModule } from '../../../transloco-testing.module';
import { Type } from '@angular/core';

describe('CustomSettingsListComponent-v2', () => {
  it('should display the last known state when I open the component', async () => {
    const itemsFromCms = ['news', 'events', 'news2'];
    const settingsInLocalStorage: { [key: string]: boolean } = {
      news: true,
      events: false,
      news2: true,
    };

    const component = await createSandboxComponent(
      settingsInLocalStorage,
      itemsFromCms,
    );

    expect(component.getUICheckboxStates().length).toBe(
      Object.keys(settingsInLocalStorage).length,
    );

    for (const checkbox of component.getUICheckboxStates()) {
      expect(checkbox.checked).toBe(
        settingsInLocalStorage[checkbox.settingsKey],
      );
    }
  });

  it('should save state on setting toggle', async () => {
    const itemsFromCms = ['news', 'events', 'parking'];
    const settingsInLocalStorage: { [key: string]: boolean } = {
      news: true,
      events: false,
      parking: true,
    };

    const component = await createSandboxComponent(
      settingsInLocalStorage,
      itemsFromCms,
    );

    await component.getUICheckboxStates()[0].toggle();
    component.expectLocalStorageSettingsToBe({
      news: false,
      events: false,
      parking: true,
    });
  });

  it("should save all settings as false when 'disableAll' is clicked", async () => {
    const itemsFromCms = ['news', 'events'];
    const settingsInLocalStorage: { [key: string]: boolean } = {
      news: true,
      events: false,
    };

    const sandboxedComponent = await createSandboxComponent(
      settingsInLocalStorage,
      itemsFromCms,
    );
    await sandboxedComponent.clickButtonWithTestId('disable-all');

    sandboxedComponent.expectLocalStorageSettingsToBe({
      news: false,
      events: false,
    });
    expect(
      sandboxedComponent.getUICheckboxStates().every((box) => !box.checked),
    ).toBeTruthy();
  });

  it("should save all settings as true when 'enableAll' is clicked", async () => {
    const itemsFromCms = ['news', 'events'];
    const settingsInLocalStorage: { [key: string]: boolean } = {
      news: true,
      events: false,
    };

    const sandboxedComponent = await createSandboxComponent(
      settingsInLocalStorage,
      itemsFromCms,
    );
    await sandboxedComponent.clickButtonWithTestId('enable-all');

    sandboxedComponent.expectLocalStorageSettingsToBe({
      news: true,
      events: true,
    });
    expect(
      sandboxedComponent.getUICheckboxStates().every((box) => box.checked),
    ).toBeTruthy();
  });

  const createSandboxComponent = async (
    state: { [key: string]: boolean },
    menuItems: string[],
  ) => {
    const settingsPersistenceServiceMock = {
      getSettings: async () => JSON.stringify(state),
      saveSettings: jest.fn(),
    };

    const testModuleMetadata = testModuleMetadataWithMocks([
      {
        provide: SettingsPersistenceService,
        useValue:
          settingsPersistenceServiceMock as unknown as jest.Mocked<SettingsPersistenceService>,
      },
    ]);

    const fixture = await createFixture(
      CustomSettingsListComponent,
      testModuleMetadata,
    );
    const component = fixture.componentInstance;

    component.isLoading = false;
    component.items = menuItems.map(
      (item) => ({ name: item }) as ModuleSettings,
    );

    fixture.detectChanges(true);
    await fixture.whenStable();

    // after the loading screen finishes, we need to trigger change detection again
    fixture.detectChanges(true);

    return {
      expectLocalStorageSettingsToBe: (settings: {
        [key: string]: boolean;
      }) => {
        expect(
          settingsPersistenceServiceMock.saveSettings,
        ).toHaveBeenLastCalledWith(undefined, undefined, settings);
      },

      clickButtonWithTestId: async (testId: string) => {
        const buttonDebugElement = fixture.debugElement.query(
          By.css(`[data-test-id="${testId}"]`),
        );
        expect(buttonDebugElement).toBeTruthy();

        buttonDebugElement.triggerEventHandler('buttonClick', {});

        fixture.detectChanges();
        await fixture.whenStable();
      },

      getUICheckboxStates: () => {
        const internalCheckbox = fixture.debugElement.queryAll(
          By.directive(CheckboxSettingComponent),
        );

        const internalCheckboxComponents = internalCheckbox.map(
          (checkboxComponent) =>
            checkboxComponent.componentInstance as CheckboxSettingComponent,
        );

        return internalCheckboxComponents.map((checkbox) => ({
          toggle: async () => {
            checkbox.toggle();
            fixture.detectChanges(true);
            await fixture.whenStable();
          },

          get checked() {
            return checkbox.checked;
          },

          settingsKey: checkbox.localizedTitle,
        }));
      },
    };
  };
});

const testModuleMetadataWithMocks = (providers: any[]) => ({
  imports: [
    CustomSettingsListComponent,
    CoreTestModule.forRoot(),
    UITestModule.forRoot(),
    getTranslocoModule(),
  ],
  providers,
});

const createFixture = async <T>(
  type: Type<T>,
  testModuleMetadata: TestModuleMetadata,
) => {
  await TestBed.configureTestingModule(testModuleMetadata).compileComponents();
  return TestBed.createComponent(type);
};
