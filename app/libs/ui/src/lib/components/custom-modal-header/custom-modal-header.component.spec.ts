import { fireEvent, render, screen } from '@testing-library/angular';
import { CustomModalHeaderComponent } from './custom-modal-header.component';
import { CustomHorizontalButtonComponent } from '../custom-horizontal-button/custom-horizontal-button.component';
import { getTranslocoModule } from '../../../transloco-testing.module';

describe('CustomModalHeaderComponent', () => {
  it('should render with default inputs', async () => {
    await render(CustomModalHeaderComponent, {
      imports: [CustomHorizontalButtonComponent, getTranslocoModule()],
    });

    const titleElement = screen.getByTestId('modal-title');
    expect(titleElement).not.toBeNull();
  });

  it('should display the provided title', async () => {
    const testTitle = 'Test Modal Title';

    await render(CustomModalHeaderComponent, {
      imports: [CustomHorizontalButtonComponent, getTranslocoModule()],
      inputs: { title: testTitle },
    });

    const titleElement = screen.getByTestId('modal-title');
    expect(titleElement).not.toBeNull();
    expect(titleElement.textContent?.trim()).toBe(testTitle);
  });

  it('should apply the correct background class based on the input', async () => {
    await render(CustomModalHeaderComponent, {
      imports: [CustomHorizontalButtonComponent, getTranslocoModule()],
      inputs: { backgroundColor: 'transparent-background' },
    });

    const headerElement = screen.getByTestId('modal-header');
    expect(headerElement.classList.contains('transparent-background')).toBe(
      true,
    );
  });

  it('should emit closeClick event when the close button is clicked', async () => {
    const closeClickSpy = jest.fn();

    await render(CustomModalHeaderComponent, {
      imports: [CustomHorizontalButtonComponent, getTranslocoModule()],
      on: { closeClick: closeClickSpy },
    });

    const buttonElement = screen.getByTestId('horizontal-button');
    fireEvent.click(buttonElement);

    expect(closeClickSpy).toHaveBeenCalled();
  });
});
