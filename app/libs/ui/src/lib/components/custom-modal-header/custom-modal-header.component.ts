import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { NgClass, NgIf } from '@angular/common';
import { CustomHorizontalButtonComponent } from '../custom-horizontal-button/custom-horizontal-button.component';
import { IconFontSize } from '../../enums/custom-icon.enums';
import {
  CustomButtonColorScheme,
  CustomButtonType,
} from '../../enums/custom-button-schemes.types.enum';

@Component({
  selector: 'lib-custom-modal-header',
  template: `
    <div
      class="custom-modal-header"
      *transloco="let t"
      [ngClass]="backgroundColor"
      data-testid="modal-header"
    >
      <ng-container *ngIf="showBackButton; else titleTemplate">
        <lib-custom-horizontal-button
          (click)="onBackNavigationClick()"
          [attr.aria-label]="t('ui.custom-modal-header.show_all')"
          [buttonType]="customButtonType.button"
          [startIcon]="'chevron_left'"
          [title]="t('ui.custom-modal-header.show_all')"
          [noPadding]="true"
          class="back-button"
        ></lib-custom-horizontal-button>
      </ng-container>
      <ng-template #titleTemplate>
        <div
          class="custom-modal-title"
          role="heading"
          aria-level="1"
          data-testid="modal-title"
        >
          <span>{{ title }}</span>
        </div>
      </ng-template>
      <lib-custom-horizontal-button
        class="close-button"
        (click)="onCloseClick()"
        [attr.aria-label]="t('ui.custom-modal-header.collapse')"
        [buttonType]="customButtonType.button"
        [colorScheme]="customButtonColorScheme.outlined"
        [endIcon]="'keyboard_arrow_down'"
        [title]="t('ui.custom-modal-header.collapse')"
        data-testid="horizontal-button"
      ></lib-custom-horizontal-button>
    </div>
  `,
  styleUrls: ['custom-modal-header.component.scss'],
  imports: [TranslocoDirective, NgClass, NgIf, CustomHorizontalButtonComponent],
})
export class CustomModalHeaderComponent implements OnInit {
  @Input() title = '';
  @Input() backgroundColor: 'primary-background' | 'transparent-background' =
    'primary-background';
  @Output() closeClick = new EventEmitter<void>();
  @Output() backNavigationClick = new EventEmitter<void>();
  showBackButton = true;
  protected readonly iconFontSize = IconFontSize;
  protected readonly customButtonType = CustomButtonType;
  protected readonly customButtonColorScheme = CustomButtonColorScheme;

  ngOnInit(): void {
    this.showBackButton = this.backNavigationClick.observed;
  }

  onCloseClick(): void {
    this.closeClick.emit();
  }

  onBackNavigationClick(): void {
    this.backNavigationClick.emit();
  }
}
