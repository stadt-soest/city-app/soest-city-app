import { Component, EventEmitter, Output } from '@angular/core';
import { IonRefresher, IonRefresherContent } from '@ionic/angular/standalone';

@Component({
  selector: 'lib-refresher',
  templateUrl: './refresher.component.html',
  styleUrls: ['./refresher.component.scss'],
  imports: [IonRefresher, IonRefresherContent],
})
export class RefresherComponent {
  @Output() ionRefresh = new EventEmitter();

  onRefresh(event: any) {
    this.ionRefresh.emit(event);
  }
}
