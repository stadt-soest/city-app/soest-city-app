import { Component, inject } from '@angular/core';
import { SIGNING_IMAGE_URL } from '../../ui-tokens';

@Component({
  selector: 'lib-signature',
  templateUrl: './signature.component.html',
  styleUrls: ['./signature.component.scss'],
  standalone: true,
})
export class SignatureComponent {
  signingImageUrl = inject(SIGNING_IMAGE_URL);
}
