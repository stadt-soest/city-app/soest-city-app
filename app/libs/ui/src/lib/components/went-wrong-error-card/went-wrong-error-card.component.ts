import { Component, DestroyRef, inject, Input, OnInit } from '@angular/core';
import { filter, Observable, of, switchMap } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { TranslocoDirective } from '@jsverse/transloco';
import { IonCard, IonCardContent } from '@ionic/angular/standalone';
import { CustomVerticalButtonComponent } from '../custom-vertical-button/custom-vertical-button.component';
import { AbstractErrorCardComponent } from '../error-card/error-card.abstract';
import {
  CustomButtonColorScheme,
  CustomButtonType,
} from '../../enums/custom-button-schemes.types.enum';
import { ErrorHandlingService } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-went-wrong-error-handling-card',
  templateUrl: './went-wrong-error-card.component.html',
  styleUrls: ['./went-wrong-error-card.component.scss'],
  imports: [
    TranslocoDirective,
    IonCard,
    IonCardContent,
    CustomVerticalButtonComponent,
  ],
})
export class WentWrongErrorCardComponent
  extends AbstractErrorCardComponent
  implements OnInit
{
  @Input() hasFeedItems$: Observable<boolean> = of();
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
  private readonly errorHandlingService = inject(ErrorHandlingService);
  private readonly destroyRef = inject(DestroyRef);

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.hasFeedItems$
      .pipe(
        takeUntilDestroyed(this.destroyRef),
        filter((hasFeedItems) => hasFeedItems),
        switchMap(() => this.errorHandlingService.hasErrorInAnyModule()),
      )
      .subscribe((hasErrorInAnyModule) => {
        this.show = hasErrorInAnyModule;
      });
  }
}
