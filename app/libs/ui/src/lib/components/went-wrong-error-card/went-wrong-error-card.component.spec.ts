import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { WentWrongErrorCardComponent } from './went-wrong-error-card.component';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '../../test/ui-test.module';
import { getTranslocoModule } from '../../../transloco-testing.module';

describe('WentWrongErrorCardComponent', () => {
  let component: WentWrongErrorCardComponent;
  let fixture: ComponentFixture<WentWrongErrorCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        WentWrongErrorCardComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [],
    }).compileComponents();

    fixture = TestBed.createComponent(WentWrongErrorCardComponent);
    component = fixture.componentInstance;
    component.hasFeedItems$ = of(true);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call window.location.reload on onReloadClick', () => {
    const updateClickSpy = jest.spyOn(component, 'onRefreshClick');
    component.onRefreshClick();
    expect(updateClickSpy).toHaveBeenCalled();
  });
});
