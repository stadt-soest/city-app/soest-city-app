import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ConnectivityErrorCardComponent } from './connectivity-error-card.component';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '../../test/ui-test.module';
import { getTranslocoModule } from '../../../transloco-testing.module';

describe('ConnectivityErrorMessageCardComponent', () => {
  let component: ConnectivityErrorCardComponent;
  let fixture: ComponentFixture<ConnectivityErrorCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ConnectivityErrorCardComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [],
    }).compileComponents();

    fixture = TestBed.createComponent(ConnectivityErrorCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call window.location.reload on onReloadClick', () => {
    const updateClickSpy = jest.spyOn(component, 'onRefreshClick');
    component.onRefreshClick();
    expect(updateClickSpy).toHaveBeenCalled();
  });
});
