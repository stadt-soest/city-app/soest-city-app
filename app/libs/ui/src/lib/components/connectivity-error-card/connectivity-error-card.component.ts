import { Component, Input } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { IonCard, IonCardContent } from '@ionic/angular/standalone';
import { CustomVerticalButtonComponent } from '../custom-vertical-button/custom-vertical-button.component';
import { AbstractErrorCardComponent } from '../error-card/error-card.abstract';
import {
  CustomButtonColorScheme,
  CustomButtonType,
} from '../../enums/custom-button-schemes.types.enum';
import { ModuleInfo } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-connectivity-error-handling-card',
  templateUrl: './connectivity-error-card.component.html',
  styleUrls: ['./connectivity-error-card.component.scss'],
  imports: [
    TranslocoDirective,
    IonCard,
    IonCardContent,
    CustomVerticalButtonComponent,
  ],
})
export class ConnectivityErrorCardComponent extends AbstractErrorCardComponent {
  @Input() moduleInfo?: ModuleInfo;
  @Input() title?: string;
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
}
