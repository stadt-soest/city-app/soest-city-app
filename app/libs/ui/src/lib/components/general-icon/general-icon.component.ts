import { Component, Input } from '@angular/core';
import { NgClass } from '@angular/common';
import {
  IconColor,
  IconFontSize,
  IconType,
} from '../../enums/custom-icon.enums';

@Component({
  selector: 'lib-general-icon',
  templateUrl: './general-icon.component.html',
  styleUrls: ['./general-icon.component.scss'],
  imports: [NgClass],
  standalone: true,
})
export class GeneralIconComponent {
  @Input() name = '';
  @Input() type: IconType = IconType.materialSymbolsOutlined;
  @Input() size: IconFontSize = IconFontSize.medium;
  @Input() isFilled = false;
  @Input() color?: IconColor = IconColor.defaultInherit;
}
