import { Component, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { TranslocoDirective } from '@jsverse/transloco';
import { IonCard, IonCardContent } from '@ionic/angular/standalone';
import { CustomVerticalButtonComponent } from '../custom-vertical-button/custom-vertical-button.component';
import {
  CustomButtonColorScheme,
  CustomButtonType,
} from '../../enums/custom-button-schemes.types.enum';
import { UpdateService } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-update-notifier',
  templateUrl: './update-notifier.component.html',
  styleUrls: ['./update-notifier.component.scss'],
  imports: [
    TranslocoDirective,
    IonCard,
    IonCardContent,
    CustomVerticalButtonComponent,
  ],
})
export class UpdateNotifierComponent {
  show = false;
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
  private readonly updateService = inject(UpdateService);

  constructor() {
    this.updateService.updateAvailable$
      .pipe(takeUntilDestroyed())
      .subscribe((updateAvailable) => {
        if (updateAvailable) {
          this.show = true;
        }
      });
  }

  async onUpdateClick() {
    await this.updateService.performUpdate();
    this.show = false;
  }
}
