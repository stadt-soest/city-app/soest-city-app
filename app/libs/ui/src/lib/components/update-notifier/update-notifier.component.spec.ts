import { UpdateNotifierComponent } from './update-notifier.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BehaviorSubject, Subject } from 'rxjs';
import { UpdateService } from '@sw-code/urbo-core';
import { CoreTestModule } from '@sw-code/urbo-core';
import { getTranslocoModule } from '../../../transloco-testing.module';

describe('UpdateNotifierComponent', () => {
  let component: UpdateNotifierComponent;
  let fixture: ComponentFixture<UpdateNotifierComponent>;
  let mockUpdateService: Partial<UpdateService>;
  let mockUpdateAvailableSubject: BehaviorSubject<boolean>;
  let updateAvailableSubject: Subject<boolean>;

  beforeEach(() => {
    mockUpdateAvailableSubject = new BehaviorSubject<boolean>(false);
    updateAvailableSubject = new Subject<boolean>();
    mockUpdateService = {
      updateAvailable$: mockUpdateAvailableSubject.asObservable(),
    };

    TestBed.configureTestingModule({
      imports: [
        UpdateNotifierComponent,
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [{ provide: UpdateService, useValue: mockUpdateService }],
    }).compileComponents();

    fixture = TestBed.createComponent(UpdateNotifierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize with update notification not shown', () => {
    const customToast = fixture.nativeElement.querySelector('.message');
    expect(customToast).toBeNull();
    expect(component.show).toBeFalsy();
  });

  it('should display component when update is available', () => {
    mockUpdateAvailableSubject.next(true);
    fixture.detectChanges();

    const customToast = fixture.nativeElement.querySelector('.card-content');
    expect(customToast).toBeTruthy();
    expect(customToast.textContent).toContain(
      'A new version of the app is available.',
    );
  });

  it('should hide update notification when update is not available', () => {
    mockUpdateAvailableSubject.next(false);
    fixture.detectChanges();

    const customToast = fixture.nativeElement.querySelector('.message');
    expect(customToast).toBeNull();
    expect(component.show).toBeFalsy();
  });

  it('should not emit updates after component destruction', () => {
    const updateClickSpy = jest.spyOn(component, 'onUpdateClick');
    fixture.destroy();

    mockUpdateAvailableSubject.next(true);

    expect(updateClickSpy).not.toHaveBeenCalled();
  });

  it('should not display component when no update is available', () => {
    const customToast = fixture.nativeElement.querySelector('.message');
    expect(customToast).toBeFalsy();
  });
});
