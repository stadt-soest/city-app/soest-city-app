import en from './lib/i18n/en.json';
import de from './lib/i18n/de.json';
import {
  TranslocoTestingModule,
  TranslocoTestingOptions,
} from '@jsverse/transloco';

export const getTranslocoModule = (options: TranslocoTestingOptions = {}) => {
  const testingOptions: TranslocoTestingOptions = {
    langs: {
      en,
      de,
      'ui/de': de,
      'ui/en': en,
    },
    translocoConfig: {
      availableLangs: ['en', 'de'],
      defaultLang: 'en',
      prodMode: false,
    },
    preloadLangs: true,
    ...options,
  };

  (testingOptions.translocoConfig as any).scopeMapping = {
    ui: 'ui',
  };

  return TranslocoTestingModule.forRoot(testingOptions);
};
