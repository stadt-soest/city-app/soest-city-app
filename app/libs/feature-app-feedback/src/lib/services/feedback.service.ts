import { AppFeedbackAdapter } from '../app-feedback.adapter';
import { inject, Injectable } from '@angular/core';
import { DeviceService } from '@sw-code/urbo-core';
import { from, Observable, throwError } from 'rxjs';
import { FeedbackForm } from '../models/feedback-form.model';
import { catchError, switchMap } from 'rxjs/operators';
import { FeedbackSubmission } from '../models/feedback-submission.model';
import { AccessibilityRequest } from '../models/accessibility-submission.model';

@Injectable()
export class FeedbackService {
  private readonly feedbackAdapter = inject(AppFeedbackAdapter);
  private readonly deviceService = inject(DeviceService);

  getFeedbackForm(version: number): Observable<FeedbackForm> {
    return this.feedbackAdapter.getFeedbackForm(version).pipe(
      catchError((error) => {
        console.error('Error fetching feedback form:', error);
        return throwError(() => error);
      }),
    );
  }

  postFeedbackForm(feedbackSubmission: FeedbackSubmission): Observable<any> {
    return from(this.populateDeviceInformation(feedbackSubmission)).pipe(
      switchMap((populatedFeedbackSubmission) =>
        from(this.deviceService.getDeviceId()).pipe(
          switchMap((deduplicationKey) => {
            populatedFeedbackSubmission.deduplicationKey = deduplicationKey;
            return this.feedbackAdapter.postFeedbackSubmission(
              populatedFeedbackSubmission,
            );
          }),
        ),
      ),
      catchError((error) => {
        console.error('Error posting feedback submission:', error);
        return throwError(() => error);
      }),
    );
  }

  postAccessibilityForm(
    accessibilityRequest: AccessibilityRequest,
  ): Observable<any> {
    return from(this.populateDeviceInformation(accessibilityRequest)).pipe(
      switchMap((updatedAccessibilityRequest) =>
        this.feedbackAdapter.postAccessibilityFeedbackSubmission(
          updatedAccessibilityRequest,
        ),
      ),
      catchError((error) => {
        console.error(
          'Error posting accessibility feedback submission:',
          error,
        );
        return throwError(() => error);
      }),
    );
  }

  private async populateDeviceInformation<T extends { environment?: any }>(
    form: T,
  ): Promise<T> {
    const deviceInfo = this.deviceService.getDeviceInfo();
    form.environment = { ...deviceInfo };
    return form;
  }
}
