import {
  Inject,
  InjectionToken,
  ModuleWithProviders,
  NgModule,
  inject,
  provideAppInitializer,
} from '@angular/core';
import { from, of } from 'rxjs';
import { provideTranslocoScope } from '@jsverse/transloco';
import {
  FeatureModule,
  ModuleCategory,
  ModuleInfo,
  ModuleRegistryService,
} from '@sw-code/urbo-core';

export const APP_FEEDBACK_MODULE_INFO = new InjectionToken<ModuleInfo>(
  'AppFeedbackModuleInfoHolderToken',
);
const moduleId = 'app-feedback';

@NgModule({
  providers: [
    {
      provide: APP_FEEDBACK_MODULE_INFO,
      useValue: {
        id: moduleId,
        name: 'feature_app_feedback.module_name',
        category: ModuleCategory.help,
        icon: 'feedback',
        baseRoutingPath: moduleId,
        defaultVisibility: true,
        settingsPageAvailable: false,
        isAboutAppItself: true,
        relevancy: 0,
        forYouRelevant: false,
        showInCategoriesPage: true,
        orderInCategory: 2,
      },
    },
  ],
})
export class FeatureAppFeedbackModule {
  readonly featureModule: FeatureModule<null>;

  constructor(
    @Inject(APP_FEEDBACK_MODULE_INFO)
    private readonly appFeedbackModuleInfo: ModuleInfo,
  ) {
    this.featureModule = {
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      initializeDefaultSettings: () => {},
      feedables: () => of({ items: [], totalRecords: 0 }),
      feedablesUnfiltered: () => from([]),
      info: this.appFeedbackModuleInfo,
      routes: [
        {
          path: `categories/${moduleId}`,
          loadComponent: () =>
            import('./pages/app-feedback-page.component').then(
              (m) => m.AppFeedbackPageComponent,
            ),
          title: 'feature_app_feedback.dashboard.main_headings',
        },
        {
          path: `categories/${moduleId}/questionnaire`,
          loadComponent: () =>
            import(
              './pages/questionnaire/app-feedback-questionnaire-page.component'
            ).then((m) => m.AppFeedbackQuestionnairePageComponent),
          title: 'feature_app_feedback.questionare.main_headings',
        },
        {
          path: `categories/${moduleId}/issue-report`,
          loadComponent: () =>
            import(
              './pages/issue-report/app-feedback-issue-report-page.component'
            ).then((m) => m.AppFeedbackIssueReportPageComponent),
          title: 'feature_app_feedback.issue-report.main_headings',
        },
        {
          path: `categories/${moduleId}/additional-actions`,
          loadComponent: () =>
            import(
              '../lib/pages/additional-actions/app-feedback-additional-actions.component'
            ).then((m) => m.AppFeedbackAdditionalActionsComponent),
          title: 'feature_app_feedback.additional-actions.thanks',
        },
        {
          path: `for-you/settings/${moduleId}`,
          loadComponent: () =>
            import('./pages/app-feedback-page.component').then(
              (m) => m.AppFeedbackPageComponent,
            ),
          title: 'feature_app_feedback.dashboard.main_headings',
        },
        {
          path: `for-you/settings/${moduleId}/questionnaire`,
          loadComponent: () =>
            import(
              './pages/questionnaire/app-feedback-questionnaire-page.component'
            ).then((m) => m.AppFeedbackQuestionnairePageComponent),
          title: 'feature_app_feedback.questionare.main_headings',
        },
        {
          path: `for-you/settings/${moduleId}/issue-report`,
          loadComponent: () =>
            import(
              './pages/issue-report/app-feedback-issue-report-page.component'
            ).then((m) => m.AppFeedbackIssueReportPageComponent),
          title: 'feature_app_feedback.issue-report.main_headings',
        },
        {
          path: `for-you/settings/${moduleId}/additional-actions`,
          loadComponent: () =>
            import(
              '../lib/pages/additional-actions/app-feedback-additional-actions.component'
            ).then((m) => m.AppFeedbackAdditionalActionsComponent),
          title: 'feature_app_feedback.additional-actions.thanks',
        },
      ],
    };
  }

  static forRoot(): ModuleWithProviders<FeatureAppFeedbackModule> {
    return {
      ngModule: FeatureAppFeedbackModule,
      providers: [
        provideTranslocoScope({
          scope: 'feature-app-feedback',
          alias: 'feature_app_feedback',
          loader: {
            en: () => import('./i18n/en.json'),
            de: () => import('./i18n/de.json'),
          },
        }),
        provideAppInitializer(() => {
          const initializerFn = (
            (
              registry: ModuleRegistryService,
              module: FeatureAppFeedbackModule,
            ) =>
            () =>
              registry.registerFeature(module.featureModule)
          )(inject(ModuleRegistryService), inject(FeatureAppFeedbackModule));
          return initializerFn();
        }),
      ],
    };
  }
}
