/* eslint @typescript-eslint/naming-convention: 0 */

export interface FeedbackForm {
  id?: string;
  version?: number;
  questions?: Array<FeedbackQuestion>;
}

export interface FeedbackQuestion {
  id?: string;
  questionType?: string;
  localizedQuestionName: string;
  options?: Array<FeedbackOption>;
}

export interface FeedbackOption {
  id?: string;
  localizedText: string;
}

export const FEEDBACK_FORM = {
  currentVersion: 2,
  questionnaire_character_limit: 200,
  issue_report_character_limit: 300,
};

export enum QuestionsTypes {
  SINGLE_CHOICE = 'SINGLE_CHOICE',
  FREE_TEXT = 'FREE_TEXT',
}
