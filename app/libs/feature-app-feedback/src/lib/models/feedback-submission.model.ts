import { DeviceEnvironment } from '@sw-code/urbo-core';

export interface FeedbackSubmission {
  version: number;
  deduplicationKey: string;
  environment: DeviceEnvironment;
  answers: Array<FeedbackAnswer>;
}

export class FeedbackAnswer {
  questionId?: string;
  selectedOptionId?: string | null;
  freeText?: string | null;
}
