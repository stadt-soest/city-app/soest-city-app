import { DeviceEnvironment } from '@sw-code/urbo-core';

export interface AccessibilityRequest {
  answer: string;
  environment?: DeviceEnvironment;
  imageFilenames: string[];
}
