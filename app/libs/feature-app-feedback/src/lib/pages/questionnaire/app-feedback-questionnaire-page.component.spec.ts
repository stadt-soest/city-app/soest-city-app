import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppFeedbackQuestionnairePageComponent } from './app-feedback-questionnaire-page.component';
import { AppFeedbackAdapter } from '../../app-feedback.adapter';
import { FeedbackService } from '../../services/feedback.service';
import { of } from 'rxjs';
import { TextFormattingPipe } from '../../pipes/text-formatting.pipe';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../transloco-testing.module';

describe('AppFeedbackQuestionnairePage', () => {
  let component: AppFeedbackQuestionnairePageComponent;
  let fixture: ComponentFixture<AppFeedbackQuestionnairePageComponent>;
  let mockAppFeedbackAdapter: jest.Mocked<AppFeedbackAdapter>;
  let mockFeedbackService: jest.Mocked<FeedbackService>;

  beforeEach(async () => {
    mockFeedbackService = {
      getFeedbackForm: jest.fn().mockReturnValue({}),
      postFeedbackForm: jest.fn(),
    } as unknown as jest.Mocked<FeedbackService>;

    mockAppFeedbackAdapter = {
      getFeedbackForm: jest.fn().mockReturnValue(
        of({
          version: 1,
          questions: [],
        }),
      ),
      postFeedbackSubmission: jest.fn().mockReturnValue(of({ success: true })),
      postAccessibilityFeedbackSubmission: jest
        .fn()
        .mockReturnValue(of({ success: true })),
    };

    await TestBed.configureTestingModule({
      providers: [
        { provide: FeedbackService, useValue: mockFeedbackService },
        { provide: AppFeedbackAdapter, useValue: mockAppFeedbackAdapter },
      ],
      imports: [
        AppFeedbackQuestionnairePageComponent,
        TextFormattingPipe,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AppFeedbackQuestionnairePageComponent);
    component = fixture.componentInstance;
    component.feedbackForms = {};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
