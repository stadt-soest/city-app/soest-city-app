import { Component, inject, OnInit } from '@angular/core';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import {
  CondensedHeaderLayoutComponent,
  ConnectivityErrorCardComponent,
  CustomButtonColorScheme,
  CustomButtonType,
  CustomVerticalButtonComponent,
} from '@sw-code/urbo-ui';
import {
  IonCol,
  IonGrid,
  IonLabel,
  IonList,
  IonRadio,
  IonRadioGroup,
  IonRow,
  IonSpinner,
  IonTextarea,
} from '@ionic/angular/standalone';
import { NgFor, NgIf } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FeedbackService } from '../../services/feedback.service';
import {
  FEEDBACK_FORM,
  FeedbackForm,
  FeedbackQuestion,
  QuestionsTypes,
} from '../../models/feedback-form.model';
import { FeedbackSubmission } from '../../models/feedback-submission.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ConnectivityService } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-feedback-questionnaire',
  templateUrl: './app-feedback-questionnaire-page.component.html',
  styleUrls: ['./app-feedback-questionnaire-page.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    IonGrid,
    NgIf,
    IonRow,
    ConnectivityErrorCardComponent,
    IonCol,
    IonSpinner,
    NgFor,
    IonLabel,
    IonList,
    IonRadioGroup,
    FormsModule,
    IonRadio,
    IonTextarea,
    CustomVerticalButtonComponent,
  ],
  providers: [FeedbackService],
})
export class AppFeedbackQuestionnairePageComponent implements OnInit {
  feedbackForms!: FeedbackForm;
  feedbackOptionIds: { [key: string]: string | null } = {};
  charactersMaxLength = FEEDBACK_FORM.questionnaire_character_limit;
  charactersLeft: number = FEEDBACK_FORM.questionnaire_character_limit;
  loading = false;
  isConnected = true;
  showConnectivityError = false;
  feedbackSubmissionData: FeedbackSubmission = {
    version: FEEDBACK_FORM.currentVersion,
    deduplicationKey: '',
    environment: {
      deviceModelName: '',
      operatingSystemName: '',
      operatingSystemVersion: '',
      appVersion: '',
      browserName: '',
      usedAs: '',
      appSettings: '',
    },
    answers: [],
  };
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
  protected readonly questionTypes = QuestionsTypes;
  private readonly feedbackService = inject(FeedbackService);
  private readonly translocoService = inject(TranslocoService);
  private readonly router = inject(Router);
  private readonly route = inject(ActivatedRoute);
  private readonly connectivityService = inject(ConnectivityService);

  ngOnInit() {
    this.setOnlineStatus();
    this.initFeedbackForm();
  }

  submitFeedback() {
    if (this.isFormValid()) {
      this.feedbackService
        .postFeedbackForm(this.feedbackSubmissionData)
        .subscribe(async () => {
          await this.router.navigate(['../additional-actions'], {
            relativeTo: this.route,
            replaceUrl: true,
          });
        });
    } else {
      alert(
        this.translocoService.translate(
          'feature_app_feedback.questionare.alert',
        ),
      );
    }
  }

  setFeedbackSubmissionDataTextArea(questionId: string, event: any) {
    const answerIndex = this.getAnswerIndex(questionId);
    if (answerIndex !== -1) {
      this.feedbackSubmissionData.answers[answerIndex].freeText =
        event.target.value;
    }
  }

  setFeedbackSubmissionDataRadioButtons(questionId: string) {
    const answerIndex = this.getAnswerIndex(questionId);
    if (answerIndex !== -1 && questionId in this.feedbackOptionIds) {
      this.feedbackSubmissionData.answers[answerIndex].selectedOptionId =
        this.feedbackOptionIds[questionId];
    }
  }

  isFormValid(): boolean {
    if (!this.feedbackForms?.questions?.length) {
      return false;
    }

    for (const question of this.feedbackForms.questions) {
      const answer = this.feedbackSubmissionData.answers.find(
        (a) => a.questionId === question.id,
      );
      if (!answer) {
        return false;
      }
      if (
        question.questionType === QuestionsTypes.SINGLE_CHOICE &&
        answer.selectedOptionId == null
      ) {
        return false;
      }
    }
    return true;
  }

  onFreeTextareaInput(event: any) {
    this.charactersLeft = this.charactersMaxLength - event.target.value.length;
  }

  private initFeedbackForm() {
    this.loading = true;
    this.feedbackService
      .getFeedbackForm(FEEDBACK_FORM.currentVersion)
      .subscribe({
        next: (feedbackForm) => {
          this.feedbackForms = feedbackForm;
          this.feedbackForms.questions?.forEach(
            (question: FeedbackQuestion) => {
              this.feedbackSubmissionData.answers.push({
                questionId: question.id,
                selectedOptionId: null,
                freeText: null,
              });
            },
          );
          this.loading = false;
        },
        error: (error) => {
          console.error('Failed to load feedback form', error);
          this.showConnectivityError = true;
          this.loading = false;
        },
      });
  }

  private getAnswerIndex(questionId: string) {
    return this.feedbackSubmissionData.answers.findIndex(
      (answer) => answer.questionId === questionId,
    );
  }

  private async setOnlineStatus() {
    this.isConnected = await this.connectivityService.checkOnlineStatus();
  }
}
