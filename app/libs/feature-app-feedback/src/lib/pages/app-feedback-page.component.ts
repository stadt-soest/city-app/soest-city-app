import { Component } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  CondensedHeaderLayoutComponent,
  CustomButtonColorScheme,
  CustomButtonType,
  CustomVerticalButtonComponent,
} from '@sw-code/urbo-ui';
import { IonGrid, IonRouterLink, IonRow } from '@ionic/angular/standalone';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'lib-app-feedback',
  templateUrl: './app-feedback-page.component.html',
  styleUrls: ['./app-feedback-page.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    IonGrid,
    IonRow,
    CustomVerticalButtonComponent,
    RouterLink,
    IonRouterLink,
  ],
})
export class AppFeedbackPageComponent {
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
}
