import {
  Component,
  ElementRef,
  inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { catchError, switchMap, take } from 'rxjs/operators';
import { EMPTY, Observable } from 'rxjs';
import {
  ConnectivityService,
  ImageUploadHandler,
  SelectedImage,
} from '@sw-code/urbo-core';
import { FeedbackService } from '../../services/feedback.service';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import {
  CondensedHeaderLayoutComponent,
  ConnectivityErrorCardComponent,
  CustomButtonColorScheme,
  CustomButtonType,
  CustomVerticalButtonComponent,
  GeneralIconComponent,
  IconFontSize,
  IconType,
} from '@sw-code/urbo-ui';
import {
  IonGrid,
  IonRow,
  IonTextarea,
  ToastController,
} from '@ionic/angular/standalone';
import { AsyncPipe, NgFor, NgIf } from '@angular/common';
import { AccessibilityRequest } from '../../models/accessibility-submission.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'lib-feedback-issue-report',
  templateUrl: './app-feedback-issue-report-page.component.html',
  styleUrls: ['./app-feedback-issue-report-page.component.scss'],
  providers: [ImageUploadHandler, FeedbackService],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    IonGrid,
    NgIf,
    IonRow,
    ConnectivityErrorCardComponent,
    IonTextarea,
    NgFor,
    GeneralIconComponent,
    CustomVerticalButtonComponent,
    AsyncPipe,
  ],
})
export class AppFeedbackIssueReportPageComponent implements OnInit {
  @ViewChild('fileInput') fileInput!: ElementRef<HTMLInputElement>;
  charactersMaxLength = 1000;
  charactersLeft: number = this.charactersMaxLength;
  feedbackText = '';
  feedbackSubmissionData: AccessibilityRequest = this.initialFeedbackData();
  isSubmitting = false;
  isConnected = true;
  images$!: Observable<SelectedImage[]>;
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
  protected readonly iconType = IconType;
  protected readonly iconFontSize = IconFontSize;
  private readonly router = inject(Router);
  private readonly route = inject(ActivatedRoute);
  private readonly feedbackService = inject(FeedbackService);
  private readonly imageUploadService = inject(ImageUploadHandler);
  private readonly translocoService = inject(TranslocoService);
  private readonly toastController = inject(ToastController);
  private readonly connectivityService = inject(ConnectivityService);

  ngOnInit() {
    this.setOnlineStatus();
    this.images$ = this.imageUploadService.images$;
  }

  onTextareaInput(event: any) {
    this.feedbackText = event.target.value;
    this.charactersLeft = this.charactersMaxLength - this.feedbackText.length;
  }

  onFilesSelected(event: any) {
    this.imageUploadService.addFiles(event.target.files);
  }

  removeImage(index: number) {
    this.imageUploadService.removeImage(index);
  }

  onSubmitFeedback() {
    if (!this.validateFeedback()) {
      return;
    }

    this.isSubmitting = true;

    this.prepareFeedbackData();
    if (this.imageUploadService.hasPendingImages()) {
      this.uploadImagesAndSubmitFeedback();
    } else {
      this.submitFeedback();
    }
  }

  private async setOnlineStatus() {
    this.isConnected = await this.connectivityService.checkOnlineStatus();
  }

  private validateFeedback(): boolean {
    if (!this.feedbackText.trim()) {
      void this.showAlert(
        this.translocoService.translate(
          'feature_app_feedback.issue-report.alert',
        ),
      );
      return false;
    }
    return true;
  }

  private prepareFeedbackData() {
    this.feedbackSubmissionData.answer = this.feedbackText;
  }

  private uploadImagesAndSubmitFeedback() {
    this.imageUploadService
      .uploadImages()
      .pipe(
        take(1),
        switchMap(({ uploadedFilenames }) => {
          this.feedbackSubmissionData.imageFilenames = uploadedFilenames;
          return this.submitFeedbackObservable();
        }),
        catchError(this.handleFeedbackSubmissionError),
      )
      .subscribe(this.handleFeedbackSubmissionSuccess);
  }

  private submitFeedback() {
    this.submitFeedbackObservable()
      .pipe(take(1))
      .subscribe({
        next: this.handleFeedbackSubmissionSuccess,
        error: this.handleFeedbackSubmissionError,
        complete: () => (this.isSubmitting = false),
      });
  }

  private submitFeedbackObservable() {
    return this.feedbackService.postAccessibilityForm(
      this.feedbackSubmissionData,
    );
  }

  private readonly handleFeedbackSubmissionSuccess = () => {
    this.resetForm();
    this.navigateToAdditionalActions();
  };

  private async navigateToAdditionalActions() {
    await this.router.navigate(['../additional-actions'], {
      relativeTo: this.route,
      replaceUrl: true,
    });
    this.isSubmitting = false;
  }

  private readonly handleFeedbackSubmissionError = () => {
    void this.showAlert(
      this.translocoService.translate(
        'feature_app_feedback.issue-report.submission_error',
      ),
    );
    return EMPTY;
  };

  private async showAlert(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 1500,
      position: 'bottom',
    });

    await toast.present();
  }

  private resetForm() {
    this.feedbackText = '';
    this.charactersLeft = this.charactersMaxLength;
    this.imageUploadService.reset();
    this.fileInput.nativeElement.value = '';
    this.feedbackSubmissionData = this.initialFeedbackData();
  }

  private initialFeedbackData(): AccessibilityRequest {
    return {
      answer: '',
      imageFilenames: [],
      environment: {
        deviceModelName: '',
        operatingSystemName: '',
        operatingSystemVersion: '',
        appVersion: '',
        browserName: '',
        usedAs: '',
        appSettings: '',
      },
    };
  }
}
