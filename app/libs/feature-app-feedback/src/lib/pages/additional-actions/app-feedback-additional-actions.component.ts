import { Component, inject } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  CondensedHeaderLayoutComponent,
  CustomButtonColorScheme,
  CustomButtonType,
  CustomVerticalButtonComponent,
  GeneralIconComponent,
  IconFontSize,
  IconType,
} from '@sw-code/urbo-ui';
import {
  IonCol,
  IonGrid,
  IonItem,
  IonList,
  IonRouterLink,
  IonRow,
} from '@ionic/angular/standalone';
import { RouterLink } from '@angular/router';
import { MapsNavigationService, ROUTES } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-feedback-issue-report',
  templateUrl: './app-feedback-additional-actions.component.html',
  styleUrls: ['./app-feedback-additional-actions.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    IonGrid,
    IonRow,
    IonCol,
    CustomVerticalButtonComponent,
    RouterLink,
    IonRouterLink,
    IonList,
    IonItem,
    GeneralIconComponent,
  ],
})
export class AppFeedbackAdditionalActionsComponent {
  forYouRoute = ROUTES.forYou;
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
  protected readonly iconType = IconType;
  protected readonly iconFontSize = IconFontSize;
  private readonly mapsNavigationService = inject(MapsNavigationService);

  async navigateToHelpLocation() {
    await this.mapsNavigationService.navigateToLocation(51.5704606, 8.1029018);
  }
}
