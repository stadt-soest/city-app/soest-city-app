import { Observable } from 'rxjs';
import { FeedbackForm } from './models/feedback-form.model';
import { FeedbackSubmission } from './models/feedback-submission.model';
import { AccessibilityRequest } from './models/accessibility-submission.model';

export abstract class AppFeedbackAdapter {
  abstract getFeedbackForm(version: number): Observable<FeedbackForm>;

  abstract postFeedbackSubmission(
    feedbackSubmission: FeedbackSubmission,
  ): Observable<FeedbackForm>;

  abstract postAccessibilityFeedbackSubmission(
    accessibilityRequest: AccessibilityRequest,
  ): Observable<FeedbackForm>;
}
