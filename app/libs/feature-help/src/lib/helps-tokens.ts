import { InjectionToken } from '@angular/core';

export const WASTE_APP_CONFIG_TOKEN = new InjectionToken<string>(
  'WasteAppConfigToken',
);
