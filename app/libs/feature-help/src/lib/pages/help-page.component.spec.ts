import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HelpPageComponent } from './help-page.component';
import { HELP_MODULE_INFO } from '../feature-help.module';
import { mockHelpModuleInfo } from '../shared/testing/help-test-utils';
import { WASTE_APP_CONFIG_TOKEN } from '../helps-tokens';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../transloco-testing.module';

describe('HelpPage', () => {
  let component: HelpPageComponent;
  let fixture: ComponentFixture<HelpPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: HELP_MODULE_INFO,
          useValue: mockHelpModuleInfo,
        },
        {
          provide: WASTE_APP_CONFIG_TOKEN,
          useValue: 'SoestApp',
        },
      ],
      imports: [
        HelpPageComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
    });

    fixture = TestBed.createComponent(HelpPageComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
