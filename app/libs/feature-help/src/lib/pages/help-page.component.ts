import { Component, inject } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  CondensedHeaderLayoutComponent,
  GeneralIconComponent,
  IconFontSize,
  IconType,
} from '@sw-code/urbo-ui';
import { IonCol, IonItem, IonList, IonRow } from '@ionic/angular/standalone';
import { ListingItemComponent } from '../components/listing-item/listing-item.component';
import { MapsNavigationService } from '@sw-code/urbo-core';
import { WASTE_APP_CONFIG_TOKEN } from '../helps-tokens';

@Component({
  selector: 'lib-help',
  templateUrl: './help-page.component.html',
  styleUrls: ['./help-page.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    IonRow,
    IonCol,
    IonList,
    IonItem,
    ListingItemComponent,
    GeneralIconComponent,
  ],
})
export class HelpPageComponent {
  appName: string;
  protected readonly iconType = IconType;
  protected readonly iconFontSize = IconFontSize;
  private readonly mapsNavigationService = inject(MapsNavigationService);
  private readonly appConfigName = inject<string>(WASTE_APP_CONFIG_TOKEN);

  constructor() {
    this.appName = this.appConfigName;
  }

  async navigateToHelpLocation(): Promise<void> {
    await this.mapsNavigationService.navigateToLocation(51.57045, 8.10547);
  }
}
