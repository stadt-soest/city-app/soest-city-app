import { ModuleCategory, ModuleInfo } from '@sw-code/urbo-core';

export const mockHelpModuleInfo: ModuleInfo = {
  id: 'help',
  name: 'Hilfe',
  category: ModuleCategory.help,
  icon: 'help_center',
  baseRoutingPath: '/help',
  defaultVisibility: true,
  settingsPageAvailable: false,
  isAboutAppItself: false,
  relevancy: 0.5,
  forYouRelevant: false,
  showInCategoriesPage: false,
  orderInCategory: 1,
};
