import {
  Inject,
  InjectionToken,
  ModuleWithProviders,
  NgModule,
  inject,
  provideAppInitializer,
} from '@angular/core';
import { UIModule } from '@sw-code/urbo-ui';
import { provideTranslocoScope } from '@jsverse/transloco';
import {
  FeatureModule,
  ModuleCategory,
  ModuleInfo,
  ModuleRegistryService,
} from '@sw-code/urbo-core';
import { from, of } from 'rxjs';
import { WASTE_APP_CONFIG_TOKEN } from './helps-tokens';

const MODULES = [UIModule];
const PROVIDERS = [
  provideTranslocoScope({
    scope: 'feature-help',
    alias: 'feature_help',
    loader: {
      en: () => import('./i18n/en.json'),
      de: () => import('./i18n/de.json'),
    },
  }),
];
export const HELP_MODULE_INFO = new InjectionToken<ModuleInfo>(
  'HelpModuleInfoHolderToken',
);
const moduleId = 'help';

@NgModule({
  imports: [...MODULES],
  exports: [...MODULES],
  providers: [
    ...PROVIDERS,
    {
      provide: HELP_MODULE_INFO,
      useValue: {
        id: moduleId,
        name: 'feature_help.module_name',
        category: ModuleCategory.help,
        icon: 'help_center',
        baseRoutingPath: moduleId,
        defaultVisibility: true,
        settingsPageAvailable: false,
        isAboutAppItself: true,
        relevancy: 0,
        forYouRelevant: false,
        showInCategoriesPage: true,
        orderInCategory: 1,
      },
    },
  ],
})
export class FeatureHelpModule {
  readonly featureModule: FeatureModule<null>;

  constructor(
    @Inject(HELP_MODULE_INFO) private readonly helpModuleInfo: ModuleInfo,
  ) {
    this.featureModule = {
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      initializeDefaultSettings: () => {},
      feedables: () => of({ items: [], totalRecords: 0 }),
      feedablesUnfiltered: () => from([]),
      info: this.helpModuleInfo,
      routes: [
        {
          path: `for-you/settings/${moduleId}`,
          loadComponent: () =>
            import('./pages/help-page.component').then(
              (m) => m.HelpPageComponent,
            ),
          title: 'feature_help.dashboard.main_headings',
        },
        {
          path: `categories/${moduleId}`,
          loadComponent: () =>
            import('./pages/help-page.component').then(
              (m) => m.HelpPageComponent,
            ),
          title: 'feature_help.dashboard.main_headings',
        },
      ],
    };
  }

  static forRoot(appName: string): ModuleWithProviders<FeatureHelpModule> {
    return {
      ngModule: FeatureHelpModule,
      providers: [
        {
          provide: WASTE_APP_CONFIG_TOKEN,
          useValue: appName,
        },
        provideAppInitializer(() => {
          const initializerFn = (
            (registry: ModuleRegistryService, module: FeatureHelpModule) =>
            () =>
              registry.registerFeature(module.featureModule)
          )(inject(ModuleRegistryService), inject(FeatureHelpModule));
          return initializerFn();
        }),
      ],
    };
  }
}
