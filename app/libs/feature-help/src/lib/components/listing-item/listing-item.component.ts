import { Component, Input } from '@angular/core';
import { IonRow } from '@ionic/angular/standalone';
import { GeneralIconComponent, IconFontSize, IconType } from '@sw-code/urbo-ui';

@Component({
  selector: 'lib-help-listing-item',
  templateUrl: './listing-item.component.html',
  styleUrls: ['./listing-item.component.scss'],
  imports: [IonRow, GeneralIconComponent],
})
export class ListingItemComponent {
  @Input({ required: true }) title = '';
  @Input({ required: true }) description = '';
  @Input({ required: true }) linkUrl = '';
  @Input({ required: true }) linkText = '';
  @Input({ required: true }) icon = '';
  protected readonly iconType = IconType;
  protected readonly iconFontSize = IconFontSize;
}
