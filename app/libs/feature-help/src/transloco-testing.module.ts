import en from './lib/i18n/en.json';
import de from './lib/i18n/de.json';
import {
  TranslocoTestingModule,
  TranslocoTestingOptions,
} from '@jsverse/transloco';

export const getTranslocoModule = (options: TranslocoTestingOptions = {}) => {
  const testingOptions: TranslocoTestingOptions = {
    langs: {
      en,
      de,

      'feature_help/de': de,

      'feature_help/en': en,
    },
    translocoConfig: {
      availableLangs: ['en', 'de'],
      defaultLang: 'en',
    },
    preloadLangs: true,
    ...options,
  };

  (testingOptions.translocoConfig as any).scopeMapping = {
    feature_help: 'feature_help',
  };

  return TranslocoTestingModule.forRoot(testingOptions);
};
