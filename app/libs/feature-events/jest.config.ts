process.env.TZ = 'UTC';

export default {
  displayName: 'feature-events',
  preset: '../../jest.preset.js',
  setupFilesAfterEnv: ['<rootDir>/src/test-setup.ts'],
  coverageDirectory: '../../coverage/libs/feature-events',
};
