import { TextFormattingPipe } from './text-formatting.pipe';
import { TestBed } from '@angular/core/testing';
import { DomPurifyConfigService } from '@sw-code/urbo-core';

describe('TextFormattingPipe', () => {
  let pipe: TextFormattingPipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TextFormattingPipe, DomPurifyConfigService],
    });

    pipe = TestBed.inject(TextFormattingPipe);
  });

  it('should replace line breaks with <br> tags', () => {
    const result = pipe.transform('Line1\nLine2');
    expect(result).toContain('Line1<br>Line2');
  });

  it('should replace carriage returns with <br> tags', () => {
    const result = pipe.transform('Line1\rLine2');
    expect(result).toContain('Line1<br>Line2');
  });

  it('should replace carriage return and line feed with <br> tags', () => {
    const result = pipe.transform('Line1\r\nLine2');
    expect(result).toContain('Line1<br>Line2');
  });

  it('should format bold text and replace line breaks', () => {
    const result = pipe.transform('This is *bold* text\nAnd a new line');
    expect(result).toContain(
      'This is <strong>bold</strong> text<br>And a new line',
    );
  });

  it('should linkify phone numbers and replace line breaks', () => {
    const result = pipe.transform('Call me at 02921/88339\nOr email me.');
    expect(result).toContain(
      'Call me at <a href="tel:+49292188339">02921/88339</a><br>Or email me.',
    );
  });
});
