import { Pipe, PipeTransform } from '@angular/core';
import { BaseContentFormatterPipe } from '@sw-code/urbo-ui';
import { DomPurifyConfigType } from '@sw-code/urbo-core';

@Pipe({
  name: 'eventContentFormatter',
  standalone: true,
})
export class TextFormattingPipe
  extends BaseContentFormatterPipe
  implements PipeTransform
{
  override transform(
    text: string,
    configType: DomPurifyConfigType = DomPurifyConfigType.basic,
  ): string {
    text = super.transform(text, configType);
    text = this.replaceComplexLineBreaks(text);
    return text;
  }

  private replaceComplexLineBreaks(text: string): string {
    return text.replace(/(\r\n|\r|\n)(?![\s\S]*<\/p>)/g, '<br>');
  }
}
