import { Pipe, PipeTransform } from '@angular/core';
import { LocalEventStatus } from '../../../models/local-event-status';

@Pipe({
  name: 'eventStatusStyles',
  standalone: true,
})
export class EventStatusStylesPipe implements PipeTransform {
  transform(status: LocalEventStatus | undefined): {
    backgroundColor: string;
    color: string;
  } {
    if (!status) {
      return { backgroundColor: 'transparent', color: 'inherit' };
    }
    switch (status) {
      case LocalEventStatus.updated:
        return {
          backgroundColor: 'var(--Hint-Background, #B3E8FF)',
          color: 'var(--Secondary-Foreground, rgba(0, 0, 0, 0.60))',
        };
      case LocalEventStatus.cancelled:
        return {
          backgroundColor: 'var(--Error-Background, #EC780C)',
          color: 'var(--On-Error-Foreground, #FFF)',
        };
      default:
        return { backgroundColor: 'transparent', color: 'inherit' };
    }
  }
}
