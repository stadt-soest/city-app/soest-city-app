import { EventStatusStylesPipe } from './status-styles.pipe';
import { LocalEventStatus } from '../../../models/local-event-status';

describe('StatusStylesPipe', () => {
  let pipe: EventStatusStylesPipe;

  beforeEach(() => {
    pipe = new EventStatusStylesPipe();
  });

  it('should return correct styles for updated status', () => {
    const result = pipe.transform(LocalEventStatus.updated);
    expect(result).toEqual({
      backgroundColor: 'var(--Hint-Background, #B3E8FF)',
      color: 'var(--Secondary-Foreground, rgba(0, 0, 0, 0.60))',
    });
  });

  it('should return correct styles for cancelled status', () => {
    const result = pipe.transform(LocalEventStatus.cancelled);
    expect(result).toEqual({
      backgroundColor: 'var(--Error-Background, #EC780C)',
      color: 'var(--On-Error-Foreground, #FFF)',
    });
  });

  it('should return default styles for undefined status', () => {
    const result = pipe.transform(undefined);
    expect(result).toEqual({
      backgroundColor: 'transparent',
      color: 'inherit',
    });
  });
});
