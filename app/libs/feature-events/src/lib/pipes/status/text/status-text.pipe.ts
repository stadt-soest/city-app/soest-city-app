import { inject, Pipe, PipeTransform } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';
import { LocalEventStatus } from '../../../models/local-event-status';

@Pipe({
  name: 'eventStatusText',
  standalone: true,
})
export class EventStatusTextPipe implements PipeTransform {
  private readonly translateService = inject(TranslocoService);

  transform(status: LocalEventStatus | undefined): string {
    if (!status) {
      return '';
    }
    switch (status) {
      case LocalEventStatus.updated:
        return this.translateService.translate('feature_events.updated');
      case LocalEventStatus.cancelled:
        return this.translateService.translate('feature_events.cancelled');
      default:
        return '';
    }
  }
}
