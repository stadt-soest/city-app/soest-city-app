import { TranslocoService } from '@jsverse/transloco';
import { TestBed } from '@angular/core/testing';
import { EventStatusTextPipe } from './status-text.pipe';
import { LocalEventStatus } from '../../../models/local-event-status';

describe('StatusTextPipe', () => {
  let pipe: EventStatusTextPipe;
  let mockTranslateService: jest.Mocked<TranslocoService>;

  beforeEach(() => {
    mockTranslateService = {
      translate: jest.fn((key: string) => {
        const translations: Record<string, string> = {
          'feature_events.updated': 'Updated',
          'feature_events.cancelled': 'Cancelled',
        };

        return translations[key] || key;
      }),
    } as unknown as jest.Mocked<TranslocoService>;

    TestBed.configureTestingModule({
      providers: [
        EventStatusTextPipe,
        { provide: TranslocoService, useValue: mockTranslateService },
      ],
    });

    pipe = TestBed.inject(EventStatusTextPipe);
  });

  it('should return "Updated" for updated status', () => {
    const result = pipe.transform(LocalEventStatus.updated);
    expect(result).toBe('Updated');
    expect(mockTranslateService.translate).toHaveBeenCalledWith(
      'feature_events.updated',
    );
  });

  it('should return "Cancelled" for cancelled status', () => {
    const result = pipe.transform(LocalEventStatus.cancelled);
    expect(result).toBe('Cancelled');
    expect(mockTranslateService.translate).toHaveBeenCalledWith(
      'feature_events.cancelled',
    );
  });

  it('should return empty string for undefined status', () => {
    const result = pipe.transform(undefined);
    expect(result).toBe('');
  });
});
