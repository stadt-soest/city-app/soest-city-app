import {
  Inject,
  InjectionToken,
  ModuleWithProviders,
  NgModule,
  inject,
  provideAppInitializer,
} from '@angular/core';
import { UIModule } from '@sw-code/urbo-ui';
import { EventService } from './services/event.service';
import { EventShareService } from './services/share/event-share.service';
import { EventFeedablesLoaderService } from './services/feedables-loader/event-feedables-loader.service';
import { EventsStorageLoaderService } from './services/storage-loader/events-storage-loader.service';
import { EventCalendarService } from './services/calendar/event-calendar.service';
import { EventFeedFilterService } from './services/feed-filter/event-feed-filter.service';
import { EventBookmarkService } from './services/bookmark/event-bookmark.service';
import { TextFormattingPipe } from './pipes/text-formatting/text-formatting.pipe';
import { provideTranslocoScope } from '@jsverse/transloco';
import {
  FeatureModule,
  FeedUserSettings,
  ModuleCategory,
  ModuleInfo,
  ModuleRegistryService,
} from '@sw-code/urbo-core';
import { EventFeedItem } from './models/event-feed-item.model';
import { EventOnboardingComponent } from './components/event-onboarding/event-onboarding.component';

const MODULES = [UIModule];
const PROVIDERS = [
  EventService,
  EventShareService,
  EventFeedablesLoaderService,
  EventsStorageLoaderService,
  EventCalendarService,
  EventFeedFilterService,
  EventBookmarkService,
  TextFormattingPipe,
  provideTranslocoScope({
    scope: 'feature-events',
    alias: 'feature_events',
    loader: {
      en: () => import('./i18n/en.json'),
      de: () => import('./i18n/de.json'),
    },
  }),
];
const PIPES = [TextFormattingPipe];
const moduleId = 'events';
export const EVENTS_MODULE_INFO = new InjectionToken<ModuleInfo>(
  'EventsModuleInfoHolderToken',
);
export const EVENT_BOOKMARKED_KEY = 'bookmarkedEvents';

@NgModule({
  imports: [...MODULES, ...PIPES],
  exports: [...MODULES, ...PIPES],
  providers: [
    ...PROVIDERS,
    {
      provide: EVENTS_MODULE_INFO,
      useValue: {
        id: moduleId,
        name: 'feature_events.module_name',
        category: ModuleCategory.onTheWay,
        icon: 'event',
        baseRoutingPath: moduleId,
        defaultVisibility: true,
        settingsPageAvailable: true,
        isAboutAppItself: false,
        relevancy: 0.2,
        forYouRelevant: true,
        showInCategoriesPage: true,
        orderInCategory: 1,
      },
    },
  ],
})
export class FeatureEventsModule {
  readonly featureModule: FeatureModule<EventFeedItem>;

  constructor(
    private readonly eventsService: EventService,
    private readonly feedablesLoaderService: EventFeedablesLoaderService,
    private readonly eventsStorageLoaderService: EventsStorageLoaderService,
    @Inject(EVENTS_MODULE_INFO) private readonly eventsModuleInfo: ModuleInfo,
  ) {
    this.featureModule = {
      initializeDefaultSettings: () => {
        (async () => {
          await eventsStorageLoaderService.initializeDefaultSettings(
            eventsService.getCategories(),
          );
        })();
      },
      feedables: (page: number) =>
        this.feedablesLoaderService.getFeedables(page),
      feedablesActivity: (page: number) =>
        this.feedablesLoaderService.getBookmarkedFeedables(page),
      feedablesUpcoming: (page: number) =>
        this.feedablesLoaderService.getFeedablesUpcoming(page),
      feedablesUnfiltered: (page: number) =>
        this.feedablesLoaderService.getFeedablesUnfiltered(page),
      feedItemType: 'EVENTS',
      search: (searchTerm: string, page?: number, size?: number) =>
        this.eventsService.getBySearchTerm(searchTerm, page, size),
      getUserSettings: async (): Promise<FeedUserSettings> =>
        await this.eventsStorageLoaderService.getUserSettings(),
      info: this.eventsModuleInfo,
      onboardingComponent: EventOnboardingComponent,
      routes: [
        {
          path: `for-you/${moduleId}`,
          redirectTo: 'for-you',
        },
        {
          path: `for-you/${moduleId}/:id`,
          loadComponent: () =>
            import('./pages/details/event-details-page.component').then(
              (m) => m.EventDetailsPageComponent,
            ),
        },
        {
          path: `for-you/${moduleId}/:id/occurrences`,
          loadComponent: () =>
            import(
              './pages/details/all-times-overview/all-times-overview-page.component'
            ).then((m) => m.AllTimesOverviewPageComponent),
        },
        {
          path: `for-you/settings/${moduleId}`,
          loadComponent: () =>
            import('./pages/settings/event-settings-page.component').then(
              (m) => m.EventSettingsPageComponent,
            ),
          title: 'feature_events.settings.title',
        },
        {
          path: `categories/${moduleId}`,
          loadComponent: () =>
            import('./pages/events-page.component').then(
              (m) => m.EventsPageComponent,
            ),
          title: 'feature_events.dashboard.main_title',
        },
        {
          path: `categories/${moduleId}/settings`,
          loadComponent: () =>
            import('./pages/settings/event-settings-page.component').then(
              (m) => m.EventSettingsPageComponent,
            ),
          title: 'feature_events.settings.title',
        },
        {
          path: `categories/${moduleId}/:id`,
          loadComponent: () =>
            import('./pages/details/event-details-page.component').then(
              (m) => m.EventDetailsPageComponent,
            ),
        },
        {
          path: `categories/${moduleId}/:id/occurrences`,
          loadComponent: () =>
            import(
              './pages/details/all-times-overview/all-times-overview-page.component'
            ).then((m) => m.AllTimesOverviewPageComponent),
        },
        {
          path: `search/results/${moduleId}/:id`,
          loadComponent: () =>
            import('./pages/details/event-details-page.component').then(
              (m) => m.EventDetailsPageComponent,
            ),
        },
        {
          path: `search/results/${moduleId}`,
          redirectTo: 'search',
        },
        {
          path: `search/results/${moduleId}/:id/occurrences`,
          loadComponent: () =>
            import(
              './pages/details/all-times-overview/all-times-overview-page.component'
            ).then((m) => m.AllTimesOverviewPageComponent),
        },
      ],
    };
  }

  static forRoot(): ModuleWithProviders<FeatureEventsModule> {
    return {
      ngModule: FeatureEventsModule,
      providers: [
        provideAppInitializer(() => {
          const initializerFn = (
            (registry: ModuleRegistryService, module: FeatureEventsModule) =>
            () =>
              registry.registerFeature(module.featureModule)
          )(inject(ModuleRegistryService), inject(FeatureEventsModule));
          return initializerFn();
        }),
        provideAppInitializer(() => {
          const initializerFn = (
            (
              eventsService: EventService,
              eventsStorageLoaderService: EventsStorageLoaderService,
            ) =>
            () => {
              void eventsStorageLoaderService.initializeDefaultSettings(
                eventsService.getCategories(),
              );
            }
          )(inject(EventService), inject(EventsStorageLoaderService));
          return initializerFn();
        }),
      ],
    };
  }
}
