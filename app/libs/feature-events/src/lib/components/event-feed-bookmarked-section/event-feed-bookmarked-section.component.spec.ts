import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EventFeedBookmarkedSectionComponent } from './event-feed-bookmarked-section.component';
import { UITestModule } from '@sw-code/urbo-ui';
import { CoreTestModule } from '@sw-code/urbo-core';

describe('EventFeedYourInterestSection', () => {
  let component: EventFeedBookmarkedSectionComponent;
  let fixture: ComponentFixture<EventFeedBookmarkedSectionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        EventFeedBookmarkedSectionComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(EventFeedBookmarkedSectionComponent);
    component = fixture.componentInstance;
    component.feedItemsBookmarked = [];
    component.contentLoadingComplete = true;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
