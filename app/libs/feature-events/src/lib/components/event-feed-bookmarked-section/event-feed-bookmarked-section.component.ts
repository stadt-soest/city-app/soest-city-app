import { Component, Input } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { IonCol, IonRow } from '@ionic/angular/standalone';
import { NgComponentOutlet, NgFor, NgIf } from '@angular/common';
import {
  NewContentTextComponent,
  SkeletonFeedCardComponent,
} from '@sw-code/urbo-ui';
import { EventFeedItem } from '../../models/event-feed-item.model';

@Component({
  selector: 'lib-event-bookmarked-section',
  templateUrl: './event-feed-bookmarked-section.component.html',
  styleUrls: ['./event-feed-bookmarked-section.component.scss'],
  imports: [
    TranslocoDirective,
    IonRow,
    NgFor,
    IonCol,
    NgComponentOutlet,
    NgIf,
    SkeletonFeedCardComponent,
    NewContentTextComponent,
  ],
})
export class EventFeedBookmarkedSectionComponent {
  @Input({ required: true }) feedItemsBookmarked: EventFeedItem[] = [];
  @Input({ required: true }) contentLoadingComplete = false;
}
