import { Component, Input } from '@angular/core';
import { JsonPipe, NgStyle } from '@angular/common';
import { GeneralIconComponent, IconFontSize, IconType } from '@sw-code/urbo-ui';
import { EventStatusStylesPipe } from '../../pipes/status/styles/status-styles.pipe';
import { EventStatusTextPipe } from '../../pipes/status/text/status-text.pipe';
import { LocalEventStatus } from '../../models/local-event-status';

@Component({
  selector: 'lib-event-status-content',
  templateUrl: './event-status-content.component.html',
  styleUrls: ['./event-status-content.component.scss'],
  imports: [GeneralIconComponent, EventStatusStylesPipe, EventStatusTextPipe],
})
export class EventStatusContentComponent {
  @Input() status: LocalEventStatus | undefined;
  protected readonly iconFontSize = IconFontSize;
  protected readonly iconType = IconType;
  protected readonly eventStatus = LocalEventStatus;
}
