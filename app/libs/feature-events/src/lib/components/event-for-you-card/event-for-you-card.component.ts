import { Component, inject } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  IonCard,
  IonCardContent,
  IonRouterLinkWithHref,
} from '@ionic/angular/standalone';
import { RouterLink } from '@angular/router';
import { NgIf, NgStyle } from '@angular/common';
import {
  GeneralIconComponent,
  IconColor,
  IconFontSize,
  IconType,
  LocalizedRelativeTimePipePipe,
} from '@sw-code/urbo-ui';
import { EventStatusContentComponent } from '../status-content/event-status-content.component';
import { EventStatusStylesPipe } from '../../pipes/status/styles/status-styles.pipe';
import { EventFeedItem } from '../../models/event-feed-item.model';
import { LocalEventStatus } from '../../models/local-event-status';
import { AbstractFeedCardComponent, ModuleInfo } from '@sw-code/urbo-core';
import { EVENTS_MODULE_INFO } from '../../feature-events.module';

@Component({
  selector: 'lib-event-for-you-card',
  templateUrl: './event-for-you-card.component.html',
  styleUrls: ['./event-for-you-card.component.scss'],
  imports: [
    TranslocoDirective,
    IonRouterLinkWithHref,
    RouterLink,
    IonCard,
    NgIf,
    IonCardContent,
    GeneralIconComponent,
    LocalizedRelativeTimePipePipe,
    NgStyle,
    EventStatusContentComponent,
    EventStatusStylesPipe,
  ],
})
export class EventForYouCardComponent extends AbstractFeedCardComponent<EventFeedItem> {
  eventModuleIcon: string;
  protected readonly iconType = IconType;
  protected readonly iconFontSize = IconFontSize;
  protected readonly iconColor = IconColor;
  protected readonly eventStatus = LocalEventStatus;
  private readonly eventsModuleInfo = inject<ModuleInfo>(EVENTS_MODULE_INFO);

  constructor() {
    super();
    this.eventModuleIcon = this.eventsModuleInfo.icon;
  }
}
