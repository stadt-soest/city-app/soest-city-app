import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EventForYouCardComponent } from './event-for-you-card.component';
import { UITestModule } from '@sw-code/urbo-ui';
import {
  CoreTestModule,
  ImageService,
  NavigationService,
} from '@sw-code/urbo-core';
import { getTranslocoModule } from '../../../transloco-testing.module';
import { EVENTS_MODULE_INFO } from '../../feature-events.module';
import {
  createEventFeedItem,
  createEventsModuleInfo,
} from '../../shared/testing/events-test-utils';
import { EventService } from '../../services/event.service';

describe('EventForYouCard', () => {
  let component: EventForYouCardComponent;
  let fixture: ComponentFixture<EventForYouCardComponent>;

  const mockImageService = {
    getImageUrl: jest.fn().mockReturnValue('http://localhost/sample-image-url'),
  };

  const mockNavigationService = {
    navigateToFeedItemDetails: jest.fn(),
    getFeedItemDetailsUrl: jest.fn(),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        EventForYouCardComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        { provide: ImageService, useValue: mockImageService },
        { provide: NavigationService, useValue: mockNavigationService },
        {
          provide: EVENTS_MODULE_INFO,
          useValue: createEventsModuleInfo,
        },
        EventService,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(EventForYouCardComponent);
    component = fixture.componentInstance;
    component.feedItem = createEventFeedItem();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return correct image URL', () => {
    expect(component.imageUrl).toEqual('http://localhost/sample-image-url');
  });

  describe('HTML rendering', () => {
    it('should display the title', () => {
      const titleElement = fixture.nativeElement.querySelector('.card-title');
      expect(titleElement.textContent).toContain('Default Title');
    });

    it('should display the image with the correct source URL', () => {
      const imageElement: HTMLImageElement =
        fixture.nativeElement.querySelector('img');
      expect(imageElement).toBeTruthy();
      expect(imageElement.src).toBe('http://localhost/sample-image-url');
    });
  });

  describe('navigateToDetails', () => {
    it('should navigate to feed item details', async () => {
      await component.navigateToDetails();
      expect(
        mockNavigationService.navigateToFeedItemDetails,
      ).toHaveBeenCalledWith(component.feedItem);
    });
  });
});
