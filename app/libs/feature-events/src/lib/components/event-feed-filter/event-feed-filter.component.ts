import { Component, EventEmitter, inject, OnInit, Output } from '@angular/core';
import { endOfDay, formatISO, startOfToday } from 'date-fns';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  IonCol,
  IonDatetime,
  IonDatetimeButton,
  IonGrid,
  IonModal,
  IonRow,
  IonSegment,
  IonSegmentButton,
} from '@ionic/angular/standalone';
import { FormsModule } from '@angular/forms';
import { NgIf } from '@angular/common';
import { FeedFilter, FeedFilterType } from '@sw-code/urbo-core';
import { EventFeedFilterService } from '../../services/feed-filter/event-feed-filter.service';

@Component({
  selector: 'lib-event-filter',
  templateUrl: './event-feed-filter.component.html',
  styleUrls: ['./event-feed-filter.component.scss'],
  imports: [
    TranslocoDirective,
    IonGrid,
    IonSegment,
    FormsModule,
    IonSegmentButton,
    NgIf,
    IonCol,
    IonRow,
    IonDatetimeButton,
    IonModal,
    IonDatetime,
  ],
})
export class EventFeedFilterComponent implements OnInit {
  @Output() filterChanged = new EventEmitter<FeedFilter>();
  selectedFilter: FeedFilterType = FeedFilterType.all;
  readonly eventFilterType = FeedFilterType;
  customFromDate = '';
  customToDate = '';
  todayDate = '';
  ionDatetimeFormatOptions = {
    date: {
      weekday: 'short',
      month: 'long',
      day: '2-digit',
    },
  };
  private readonly filterService = inject(EventFeedFilterService);

  ngOnInit(): void {
    this.initializeFilterDefaults();
  }

  segmentChanged(ev: any): void {
    const { startDate, endDate } = this.filterService.calculateDates(
      ev.detail.value,
      new Date(this.customFromDate),
      new Date(this.customToDate),
    );

    this.emitFilterChanged({
      filter: ev.detail.value,
      period: { startDate, endDate },
    });
  }

  onCustomDateChanged(): void {
    const fromDate = new Date(this.customFromDate);
    const toDate = new Date(this.customToDate);

    if (toDate < fromDate) {
      this.customToDate = endOfDay(fromDate).toISOString();
    }

    this.emitFilterChanged({
      filter: FeedFilterType.selectDays,
      period: { startDate: fromDate, endDate: new Date(this.customToDate) },
    });
  }

  private initializeFilterDefaults(): void {
    this.todayDate = formatISO(startOfToday());
    const { customFromDate, customToDate } =
      this.filterService.initializeSelectDays();

    this.customFromDate = formatISO(customFromDate);
    this.customToDate = formatISO(customToDate);
  }

  private emitFilterChanged(feedFilter: FeedFilter): void {
    this.filterChanged.emit(feedFilter);
  }
}
