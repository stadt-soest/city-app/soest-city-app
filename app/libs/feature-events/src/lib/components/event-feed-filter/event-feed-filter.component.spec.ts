import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EventFeedFilterComponent } from './event-feed-filter.component';
import { EventFeedFilterService } from '../../services/feed-filter/event-feed-filter.service';
import { UITestModule } from '@sw-code/urbo-ui';
import { CoreTestModule, FeedFilterType } from '@sw-code/urbo-core';

describe('EventFeedFilterComponent', () => {
  let component: EventFeedFilterComponent;
  let fixture: ComponentFixture<EventFeedFilterComponent>;
  let mockEventFeedFilterService: jest.Mocked<EventFeedFilterService>;

  beforeEach(() => {
    mockEventFeedFilterService = {
      initializeSelectDays: jest.fn().mockReturnValue({
        customFromDate: '2021-01-01',
        customToDate: '2021-01-02',
      }),
      calculateDates: jest.fn(),
    } as unknown as jest.Mocked<EventFeedFilterService>;

    TestBed.configureTestingModule({
      imports: [
        EventFeedFilterComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
      ],
      providers: [
        {
          provide: EventFeedFilterService,
          useValue: mockEventFeedFilterService,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(EventFeedFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize with correct defaults', () => {
    expect(component.selectedFilter).toBe(FeedFilterType.all);
    expect(
      mockEventFeedFilterService.initializeSelectDays,
    ).toHaveBeenCalledTimes(1);
    expect(component.customFromDate).toBe('2021-01-01T00:00:00Z');
    expect(component.customToDate).toBe('2021-01-02T00:00:00Z');
  });

  it('should emit correct filter data when segment changes', () => {
    const mockEvent = {
      detail: {
        value: FeedFilterType.today,
      },
    };
    const expectedPeriod = { startDate: new Date(), endDate: new Date() };
    mockEventFeedFilterService.calculateDates.mockReturnValue(expectedPeriod);

    const emitSpy = jest.spyOn(component.filterChanged, 'emit');

    component.segmentChanged(mockEvent);

    expect(mockEventFeedFilterService.calculateDates).toHaveBeenCalledWith(
      FeedFilterType.today,
      new Date(component.customFromDate),
      new Date(component.customToDate),
    );
    expect(emitSpy).toHaveBeenCalledWith({
      filter: FeedFilterType.today,
      period: expectedPeriod,
    });
  });

  it('should adjust end date if it is before start date and emit the correct filter', () => {
    component.customFromDate = '2023-12-25';
    component.customToDate = '2023-12-24';
    const emitSpy = jest.spyOn(component.filterChanged, 'emit');

    const expectedAdjustedDate = '2023-12-25T23:59:59.999Z';

    component.onCustomDateChanged();

    expect(component.customToDate).toBe(expectedAdjustedDate);
    expect(emitSpy).toHaveBeenCalled();
  });
});
