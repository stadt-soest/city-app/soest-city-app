import { Component } from '@angular/core';
import {
  IonCol,
  IonGrid,
  IonRow,
  IonSkeletonText,
} from '@ionic/angular/standalone';
import { NgFor } from '@angular/common';

@Component({
  selector: 'lib-event-details-skeleton',
  templateUrl: './event-details-skeleton.component.html',
  styleUrls: ['./event-details-skeleton.component.scss'],
  imports: [IonGrid, IonRow, IonCol, IonSkeletonText, NgFor],
})
export class EventDetailsSkeletonComponent {}
