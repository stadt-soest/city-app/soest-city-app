import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EventDetailsSkeletonComponent } from './event-details-skeleton.component';
import { UITestModule } from '@sw-code/urbo-ui';
import { CoreTestModule } from '@sw-code/urbo-core';

describe('EventDetailsSkeletonComponent', () => {
  let component: EventDetailsSkeletonComponent;
  let fixture: ComponentFixture<EventDetailsSkeletonComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        EventDetailsSkeletonComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
      ],
      providers: [],
    }).compileComponents();

    fixture = TestBed.createComponent(EventDetailsSkeletonComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
