import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { EventOnboardingComponent } from './event-onboarding.component';
import { UITestModule } from '@sw-code/urbo-ui';
import { CoreTestModule } from '@sw-code/urbo-core';
import { getTranslocoModule } from '../../../transloco-testing.module';
import { EVENTS_MODULE_INFO } from '../../feature-events.module';
import { createEventsModuleInfo } from '../../shared/testing/events-test-utils';

describe('EventOnboardingComponent', () => {
  let component: EventOnboardingComponent;
  let fixture: ComponentFixture<EventOnboardingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        EventOnboardingComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        {
          provide: EVENTS_MODULE_INFO,
          useFactory: () => createEventsModuleInfo,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(EventOnboardingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
