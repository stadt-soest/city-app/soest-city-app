import { Component, inject } from '@angular/core';
import { OnboardingButton, OnboardingComponent } from '@sw-code/urbo-ui';
import { provideTranslocoScope, TranslocoDirective } from '@jsverse/transloco';
import { ModuleInfo, OnboardingService } from '@sw-code/urbo-core';
import { EVENTS_MODULE_INFO } from '../../feature-events.module';

@Component({
  selector: 'lib-events-onboarding-card',
  template: `
    <lib-onboarding
      *transloco="let t"
      [imageUrl]="imageUrl"
      [title]="t(title)"
      [content]="t(content)"
      [moduleInfo]="eventsModuleInfo"
      [buttons]="buttons"
    >
    </lib-onboarding>
  `,
  imports: [TranslocoDirective, OnboardingComponent],
  providers: [
    provideTranslocoScope({
      scope: 'feature-events',
      alias: 'feature_events',
      loader: {
        en: () => import('../../i18n/en.json'),
        de: () => import('../../i18n/de.json'),
      },
    }),
  ],
})
export class EventOnboardingComponent {
  imageUrl = 'assets/illustrations/events.svg';
  title = 'feature_events.onboarding.title';
  content = 'feature_events.onboarding.content';
  buttons: OnboardingButton[];
  public eventsModuleInfo = inject<ModuleInfo>(EVENTS_MODULE_INFO);
  private readonly onboardingService = inject(OnboardingService);

  constructor() {
    this.buttons = [
      {
        icon: 'visibility_off',
        text: 'feature_events.later',
        action: () =>
          this.onboardingService.navigateToNextSlide(
            this.eventsModuleInfo,
            'later',
          ),
        selected: !this.eventsModuleInfo.defaultVisibility,
        shouldApplySelectionStyle: true,
        id: 'visibility-off-button',
      },
      {
        icon: 'favorite_border',
        text: 'feature_events.activate',
        action: () =>
          this.onboardingService.navigateToNextSlide(
            this.eventsModuleInfo,
            'activate',
          ),
        selected: this.eventsModuleInfo.defaultVisibility,
        shouldApplySelectionStyle: true,
        id: 'visibility-on-button',
      },
    ];
  }
}
