import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EventFeedCardComponent } from './event-feed-card.component';
import { UITestModule } from '@sw-code/urbo-ui';
import {
  CoreTestModule,
  ImageService,
  NavigationService,
} from '@sw-code/urbo-core';
import { EventService } from '../../services/event.service';
import { createEventFeedItem } from '../../shared/testing/events-test-utils';

describe('EventFeedCardComponent', () => {
  let component: EventFeedCardComponent;
  let fixture: ComponentFixture<EventFeedCardComponent>;

  const mockImageService = {
    getImageUrl: jest.fn().mockReturnValue('http://localhost/sample-image-url'),
  };

  const mockNavigationService = {
    navigateToFeedItemDetails: jest.fn(),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        EventFeedCardComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
      ],
      providers: [
        { provide: ImageService, useValue: mockImageService },
        { provide: NavigationService, useValue: mockNavigationService },
        EventService,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(EventFeedCardComponent);
    component = fixture.componentInstance;
    component.feedItem = createEventFeedItem();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return correct image URL', () => {
    expect(component.imageUrl).toEqual('http://localhost/sample-image-url');
  });

  describe('HTML rendering', () => {
    it('should display the title', () => {
      const titleElement = fixture.nativeElement.querySelector('.card-title');
      expect(titleElement.textContent).toContain('Default Title');
    });
    it('should display the image with the correct source URL', () => {
      const imageElement: HTMLImageElement =
        fixture.nativeElement.querySelector('img');
      expect(imageElement).toBeTruthy();
      expect(imageElement.src).toBe('http://localhost/sample-image-url');
    });
  });
});
