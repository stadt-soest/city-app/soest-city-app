import { Component } from '@angular/core';
import {
  GeneralIconComponent,
  IconColor,
  IconFontSize,
  IconType,
  LocalizedRelativeTimePipePipe,
} from '@sw-code/urbo-ui';
import {
  IonCard,
  IonCardContent,
  IonRouterLinkWithHref,
} from '@ionic/angular/standalone';
import { RouterLink } from '@angular/router';
import { NgIf, NgStyle } from '@angular/common';
import { EventStatusContentComponent } from '../status-content/event-status-content.component';
import { EventStatusStylesPipe } from '../../pipes/status/styles/status-styles.pipe';
import { EventFeedItem } from '../../models/event-feed-item.model';
import { LocalEventStatus } from '../../models/local-event-status';
import { AbstractFeedCardComponent } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-event-feed-card',
  templateUrl: './event-feed-card.component.html',
  styleUrls: ['./event-feed-card.component.scss'],
  imports: [
    IonRouterLinkWithHref,
    RouterLink,
    IonCard,
    NgIf,
    IonCardContent,
    LocalizedRelativeTimePipePipe,
    NgStyle,
    EventStatusContentComponent,
    EventStatusStylesPipe,
  ],
})
export class EventFeedCardComponent extends AbstractFeedCardComponent<EventFeedItem> {
  protected readonly iconFontSize = IconFontSize;
  protected readonly iconColor = IconColor;
  protected readonly iconType = IconType;
  protected readonly eventStatus = LocalEventStatus;
}
