import {
  Component,
  DestroyRef,
  EventEmitter,
  inject,
  Input,
  Output,
} from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  IonCol,
  IonInfiniteScroll,
  IonInfiniteScrollContent,
  IonRow,
} from '@ionic/angular/standalone';
import { NgComponentOutlet, NgFor, NgIf } from '@angular/common';
import {
  NewContentTextComponent,
  SkeletonFeedCardComponent,
} from '@sw-code/urbo-ui';
import { EventFeedItem } from '../../models/event-feed-item.model';
import { EventService } from '../../services/event.service';
import { firstValueFrom } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'lib-event-your-interest-section',
  templateUrl: './event-feed-your-interest-section.component.html',
  styleUrls: ['./event-feed-your-interest-section.component.scss'],
  imports: [
    TranslocoDirective,
    IonRow,
    NgFor,
    IonCol,
    NgComponentOutlet,
    NgIf,
    SkeletonFeedCardComponent,
    NewContentTextComponent,
    IonInfiniteScroll,
    IonInfiniteScrollContent,
  ],
})
export class EventFeedYourInterestSectionComponent {
  @Input({ required: true }) feedItemsInterests: EventFeedItem[] = [];
  @Input({ required: true }) contentLoadingComplete = false;
  @Input({ required: true }) moreContentAvailable = true;
  @Input({ required: true }) currentPageIndex = 0;
  @Input() startDate: Date | undefined;
  @Input() endDate: Date | undefined;
  @Output() feedItemsChange = new EventEmitter<EventFeedItem[]>();
  private readonly eventService = inject(EventService);
  private readonly destroyRef = inject(DestroyRef);

  async loadMoreFeedItems(event: any) {
    const bookmarkedEventIds = await firstValueFrom(
      this.eventService.getBookmarkedEventIds(),
    );
    this.contentLoadingComplete = false;
    this.currentPageIndex++;
    this.eventService
      .getFeedablesUpcoming(
        this.currentPageIndex,
        undefined,
        this.startDate ?? undefined,
        this.endDate ?? undefined,
        bookmarkedEventIds,
      )
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((feedItems) => {
        this.moreContentAvailable = feedItems.content.length > 0;
        this.feedItemsInterests.push(...feedItems.content);
        this.contentLoadingComplete = true;
        event.target.complete();
        this.feedItemsChange.emit(this.feedItemsInterests);
      });
  }
}
