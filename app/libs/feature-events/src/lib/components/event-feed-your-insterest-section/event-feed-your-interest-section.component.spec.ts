import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EventFeedYourInterestSectionComponent } from './event-feed-your-interest-section.component';
import { EventService } from '../../services/event.service';
import { of } from 'rxjs';
import { createEventFeedItem } from '../../shared/testing/events-test-utils';
import { UITestModule } from '@sw-code/urbo-ui';
import { CoreTestModule } from '@sw-code/urbo-core';

describe('EventFeedYourInterestSection', () => {
  let component: EventFeedYourInterestSectionComponent;
  let fixture: ComponentFixture<EventFeedYourInterestSectionComponent>;
  let mockEventService: jest.Mocked<EventService>;

  beforeEach(() => {
    mockEventService = {
      getFeedablesUpcoming: jest.fn().mockReturnValue(
        of({
          content: [
            createEventFeedItem({ id: '1', title: 'Item 1' }),
            createEventFeedItem({
              id: '2',
              title: 'Item 2',
            }),
          ],
          totalRecords: 2,
        }),
      ),
      getBookmarkedEventIds: jest.fn().mockReturnValue(of(['1', '2'])),
    } as unknown as jest.Mocked<EventService>;

    TestBed.configureTestingModule({
      imports: [
        EventFeedYourInterestSectionComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
      ],
      providers: [{ provide: EventService, useValue: mockEventService }],
    }).compileComponents();

    fixture = TestBed.createComponent(EventFeedYourInterestSectionComponent);
    component = fixture.componentInstance;
    component.feedItemsInterests = [];
    component.contentLoadingComplete = true;
    component.moreContentAvailable = false;
    fixture.detectChanges();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should handle no items initially and load items correctly', async () => {
    expect(component.feedItemsInterests.length).toBe(0);
    expect(mockEventService.getFeedablesUpcoming).not.toHaveBeenCalled();

    await component.loadMoreFeedItems({ target: { complete: jest.fn() } });

    expect(mockEventService.getFeedablesUpcoming).toHaveBeenCalled();
    expect(component.contentLoadingComplete).toBe(true);
  });

  describe('loadMoreFeedItems', () => {
    it('should update feed items on call', async () => {
      component.feedItemsInterests.pop();
      component.currentPageIndex = 0;
      await component.loadMoreFeedItems({ target: { complete: jest.fn() } });
      expect(component.feedItemsInterests.length).toBe(2);
      expect(component.feedItemsInterests[0].id).toBe('1');
    });

    it('should call complete on event at the end of loadMoreFeedItems', async () => {
      const complete = jest.fn();
      mockEventService.getFeedablesUpcoming.mockReturnValue(
        of({
          content: [
            createEventFeedItem({ id: '1' }),
            createEventFeedItem({ id: '2' }),
            createEventFeedItem({ id: '3' }),
          ],
          totalRecords: 3,
        }),
      );
      component.currentPageIndex = 0;
      await component.loadMoreFeedItems({ target: { complete } });
      expect(complete).toHaveBeenCalled();
    });

    it('should increment paginationPage on call', async () => {
      mockEventService.getFeedablesUpcoming.mockReturnValue(
        of({
          content: [
            createEventFeedItem({ id: '1' }),
            createEventFeedItem({ id: '2' }),
            createEventFeedItem({ id: '3' }),
          ],
          totalRecords: 3,
        }),
      );
      component.currentPageIndex = 0;
      await component.loadMoreFeedItems({ target: { complete: jest.fn() } });
      expect(component.currentPageIndex).toBe(1);
    });
  });
});
