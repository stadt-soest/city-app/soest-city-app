import { Component, DestroyRef, inject, OnInit } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { IonCol, IonGrid } from '@ionic/angular/standalone';
import { NgIf } from '@angular/common';
import {
  ConnectivityErrorCardComponent,
  CustomSettingsListComponent,
} from '@sw-code/urbo-ui';
import {
  ConnectivityService,
  ModuleInfo,
  ModuleSettings,
  StorageKeyType,
  TranslationService,
} from '@sw-code/urbo-core';
import { EventService } from '../../services/event.service';
import { EVENTS_MODULE_INFO } from '../../feature-events.module';
import { combineLatest, map } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'lib-events-settings-menu',
  templateUrl: './event-settings-menu.component.html',
  styleUrls: ['./event-settings-menu.component.scss'],
  imports: [
    TranslocoDirective,
    IonGrid,
    NgIf,
    IonCol,
    ConnectivityErrorCardComponent,
    CustomSettingsListComponent,
  ],
})
export class EventSettingsMenuComponent implements OnInit {
  eventsModule: ModuleInfo;
  uniqueCategories: ModuleSettings[] = [];
  isLoadingCategories = true;
  localLanguage: string;
  isConnected = true;
  protected readonly storageKeyType = StorageKeyType;
  private readonly eventsService = inject(EventService);
  private readonly translateService = inject(TranslationService);
  private readonly destroyRef = inject(DestroyRef);
  private readonly connectivityService = inject(ConnectivityService);
  private readonly eventsModuleInfo = inject<ModuleInfo>(EVENTS_MODULE_INFO);

  constructor() {
    this.eventsModule = this.eventsModuleInfo;
    this.localLanguage = this.translateService.getCurrentLocale();
  }

  ngOnInit() {
    this.setOnlineStatus();
    this.subscribeToFeedItems();
  }

  private async setOnlineStatus() {
    this.isConnected = await this.connectivityService.checkOnlineStatus();
  }

  private subscribeToFeedItems() {
    const categories$ = this.eventsService.getCategories().pipe(
      map((categories) =>
        categories.map((category) => {
          const localizedName =
            category.localizedNames.find(
              (loc: { language: string }) =>
                loc.language === this.localLanguage,
            ) ??
            category.localizedNames.find(
              (loc: { language: string }) => loc.language === 'EN',
            );

          return {
            id: category.id ? category.id : '',
            name: localizedName ? localizedName.value : 'Unnamed Category',
          };
        }),
      ),
    );

    combineLatest([categories$])
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(([categories]) => {
        this.uniqueCategories = categories;
        this.isLoadingCategories = false;
      });
  }
}
