import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { EventSettingsMenuComponent } from './event-settings-menu.component';
import { of } from 'rxjs';
import { EventService } from '../../services/event.service';
import {
  CoreTestModule,
  NavigationService,
  StorageService,
} from '@sw-code/urbo-core';
import { EventsStorageLoaderService } from '../../services/storage-loader/events-storage-loader.service';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../transloco-testing.module';
import { EVENTS_MODULE_INFO } from '../../feature-events.module';
import { createEventsModuleInfo } from '../../shared/testing/events-test-utils';

describe('EventsSettingsMenuComponent', () => {
  let component: EventSettingsMenuComponent;
  let fixture: ComponentFixture<EventSettingsMenuComponent>;

  const mockEventsService: jest.Mocked<EventService> = {
    getCategories: jest.fn().mockReturnValue(
      of([
        {
          id: 'cat1',
          localizedNames: [{ language: 'EN', value: 'Category 1' }],
        },
        {
          id: 'cat2',
          localizedNames: [{ language: 'EN', value: 'Category 2' }],
        },
      ]),
    ),
  } as unknown as jest.Mocked<EventService>;

  const mockStorageService: jest.Mocked<StorageService> = {
    set: jest.fn(),
    generateStorageKey: jest.fn(),
    get: jest.fn(),
  } as unknown as jest.Mocked<StorageService>;

  const mockStorageLoaderService: jest.Mocked<EventsStorageLoaderService> = {
    extractUniqueCategories: jest
      .fn()
      .mockReturnValue(['Category1', 'Category2']),
    loadSettings: jest.fn(),
  } as unknown as jest.Mocked<EventsStorageLoaderService>;

  const mockNavigationService = {
    navigateToFeedItemDetails: jest.fn().mockResolvedValue(true),
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        EventSettingsMenuComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        { provide: NavigationService, useValue: mockNavigationService },
        {
          provide: StorageService,
          useValue: mockStorageService,
        },
        {
          provide: EventService,
          useValue: mockEventsService,
        },
        {
          provide: EventsStorageLoaderService,
          useValue: mockStorageLoaderService,
        },
        {
          provide: EVENTS_MODULE_INFO,
          useValue: createEventsModuleInfo,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(EventSettingsMenuComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
