import { FeedItem } from '@sw-code/urbo-core';
import { LocalEventStatus } from './local-event-status';

export class EventFeedItem extends FeedItem {
  startDate: Date;
  status: LocalEventStatus = LocalEventStatus.scheduled;

  constructor(data: Partial<EventFeedItem>) {
    super(data);
    this.startDate = data.startDate ? new Date(data.startDate) : new Date();
  }
}
