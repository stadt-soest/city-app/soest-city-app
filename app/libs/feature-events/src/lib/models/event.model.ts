import { ImageFile, Language } from '@sw-code/urbo-core';
import { LocalEventStatus } from './local-event-status';

export class Event {
  id: string;
  selectedOccurrenceId: string;
  location?: Location;
  coordinates?: Coordinates;
  contactPoint?: ContactPoint;
  dateModified?: Date;
  title: string;
  description?: string;
  image: Image | null;
  occurrences: Array<Occurrence>;
  subTitle?: string | null;
  categoryIds: string[];

  constructor(
    public options: {
      id: string;
      selectedOccurrenceId: string;
      title: string;
      description: string;
      image: Image | null;
      occurrences: Array<Occurrence>;
      categoryIds: string[];
      dateModified?: Date;
      location?: Location;
      coordinates?: Coordinates;
      contactPoint?: ContactPoint;
      subTitle?: string | null;
    },
  ) {
    this.id = options.id;
    this.selectedOccurrenceId = options.selectedOccurrenceId;
    this.title = options.title;
    this.description = options.description;
    this.image = options.image;
    this.occurrences = options.occurrences;
    this.categoryIds = options.categoryIds;
    this.dateModified = options.dateModified;
    this.location = options.location;
    this.coordinates = options.coordinates;
    this.contactPoint = options.contactPoint;
    this.subTitle = options.subTitle;
  }

  toPrettyAddressString(): string {
    const street = this.location?.streetAddress;
    const postalCode = this.location?.postalCode;
    const city = this.location?.city;

    if (street && postalCode && city) {
      return `${street}, ${postalCode} ${city}.`;
    } else if (street && postalCode) {
      return `${street}, ${postalCode}.`;
    } else if (street) {
      return street + '.';
    } else {
      return '';
    }
  }
}

export interface Location {
  locationName?: string;
  city?: string;
  postalCode?: string;
  streetAddress?: string;
}

export interface Coordinates {
  lat: number;
  long: number;
}

export interface ContactPoint {
  telephone?: string;
  email?: string;
}

export interface Image {
  imageSource?: string;
  description?: string | null;
  copyrightHolder?: string | null;
  thumbnail: ImageFile | null;
  highRes: ImageFile | null;
}

export interface Occurrence {
  id: string;
  status: LocalEventStatus;
  source: string;
  dateCreated: string;
  start: Date;
  end: Date;
}

export interface EventCategory {
  id: string;
  sourceCategories?: Array<string>;
  localizedNames: Array<CategoryLocalization>;
}

export interface CategoryLocalization {
  value: string;
  language: Language;
}
