export enum LocalEventStatus {
  scheduled = 'scheduled',
  cancelled = 'cancelled',
  updated = 'updated',
}
