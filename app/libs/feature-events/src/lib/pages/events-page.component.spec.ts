import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EventsPageComponent } from './events-page.component';
import { EventService } from '../services/event.service';
import { MenuController } from '@ionic/angular';
import {
  ConnectivityService,
  CoreTestModule,
  ErrorType,
  UpdateService,
} from '@sw-code/urbo-core';
import { BehaviorSubject, of, Subject } from 'rxjs';
import { createEventFeedItem } from '../shared/testing/events-test-utils';
import { getTranslocoModule } from '../../transloco-testing.module';

describe('EventsPage', () => {
  let component: EventsPageComponent;
  let fixture: ComponentFixture<EventsPageComponent>;
  let mockViewContainerRef: any;
  let mockEventService: jest.Mocked<EventService>;
  let mockMenuController: jest.Mocked<MenuController>;
  let mockUpdateService: Partial<UpdateService>;
  let mockConnectivityService: jest.Mocked<ConnectivityService>;
  let mockUpdateAvailableSubject: BehaviorSubject<boolean>;
  let updateAvailableSubject: Subject<boolean>;

  beforeEach(() => {
    const mockInstances = [];
    const mockFeedItems = [
      createEventFeedItem({ id: '1' }),
      createEventFeedItem({ id: '2' }),
    ];

    mockViewContainerRef = {
      createComponent: jest.fn(() => {
        const mockInstance = { feedItem: null };
        mockInstances.push(mockInstance);
        return { instance: mockInstance };
      }),
      clear: jest.fn(),
    };

    mockEventService = {
      getFeed: jest.fn().mockReturnValue(of(mockFeedItems)),
      getEventsModuleErrors: jest
        .fn()
        .mockReturnValue(
          of({ hasError: true, errorType: ErrorType.MODULE_API_ERROR }),
        ),
      getFeedablesDateCreated: jest
        .fn()
        .mockReturnValue(of({ content: [], totalRecords: 0 })),
      getFeedablesUpcoming: jest.fn().mockReturnValue(
        of({
          content: [
            createEventFeedItem({ id: '1', title: 'Item 1' }),
            createEventFeedItem({
              id: '2',
              title: 'Item 2',
            }),
          ],
          totalRecords: 2,
        }),
      ),
      getBookmarkedEventIds: jest.fn().mockReturnValue(of(['1', '2'])),
      getCategorySettings: jest
        .fn()
        .mockReturnValue(Promise.resolve(['category1', 'category2'])),
    } as unknown as jest.Mocked<EventService>;

    mockUpdateAvailableSubject = new BehaviorSubject<boolean>(false);
    updateAvailableSubject = new Subject<boolean>();

    mockUpdateService = {
      updateAvailable$: mockUpdateAvailableSubject.asObservable(),
    };

    mockConnectivityService = {
      checkOnlineStatus: jest.fn().mockReturnValue(Promise.resolve(true)),
    } as unknown as jest.Mocked<ConnectivityService>;

    mockMenuController = {
      open: jest.fn(),
      close: jest.fn(),
    } as unknown as jest.Mocked<MenuController>;

    TestBed.configureTestingModule({
      imports: [
        EventsPageComponent,
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        { provide: EventService, useValue: mockEventService },
        { provide: UpdateService, useValue: mockUpdateService },
        { provide: MenuController, useValue: mockMenuController },
        { provide: ConnectivityService, useValue: mockConnectivityService },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(EventsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should set feedItems when SourceSettings and CategorySettings are available', async () => {
      const expectedFeedItems = [
        createEventFeedItem({ id: '1', title: 'Item 1' }),
        createEventFeedItem({
          id: '2',
          title: 'Item 2',
        }),
      ];

      component.ngOnInit();
      fixture.detectChanges();
      expect(component.feedItemsInterests.length).toEqual(
        expectedFeedItems.length,
      );
      expect(component.feedItemsInterests).toEqual(
        expect.arrayContaining(expectedFeedItems),
      );
    });

    it('should set displayConnectivityError to true when ngOnit is called and no feed items are returned', () => {
      component.ngOnInit();
      expect(component.displayConnectivityError).toBe(true);
    });

    it('should display connectivity error when there is no internet connection', async () => {
      mockConnectivityService.checkOnlineStatus.mockResolvedValue(false);

      component.ngOnInit();
      await fixture.whenStable();

      expect(component.displayConnectivityError).toBe(true);
    });
  });

  describe('HTML rendering', () => {
    it('should display main title', () => {
      const compiled = fixture.nativeElement;
      expect(compiled.querySelector('ion-title').textContent).toContain(
        'Events',
      );
    });
  });
});
