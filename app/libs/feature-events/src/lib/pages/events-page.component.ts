import {
  Component,
  DestroyRef,
  inject,
  OnInit,
  AfterViewInit,
} from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { EventFeedItem } from '../models/event-feed-item.model';
import { EventFeedBookmarkedSectionComponent } from '../components/event-feed-bookmarked-section/event-feed-bookmarked-section.component';
import { EventFeedYourInterestSectionComponent } from '../components/event-feed-your-insterest-section/event-feed-your-interest-section.component';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  CondensedHeaderLayoutComponent,
  ConnectivityErrorCardComponent,
  NoInterestsSelectedCardComponent,
  RefresherComponent,
  UpdateNotifierComponent,
} from '@sw-code/urbo-ui';
import { IonCol, IonGrid } from '@ionic/angular/standalone';
import { EventFeedFilterComponent } from '../components/event-feed-filter/event-feed-filter.component';
import { NgIf } from '@angular/common';
import { EventService } from '../services/event.service';
import {
  ConnectivityService,
  ErrorType,
  FeedFilter,
  FeedUpdateService,
} from '@sw-code/urbo-core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'lib-events-page',
  templateUrl: './events-page.component.html',
  styleUrls: ['./events-page.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    RefresherComponent,
    IonGrid,
    EventFeedFilterComponent,
    NgIf,
    IonCol,
    NoInterestsSelectedCardComponent,
    ConnectivityErrorCardComponent,
    UpdateNotifierComponent,
    EventFeedBookmarkedSectionComponent,
    EventFeedYourInterestSectionComponent,
  ],
})
export class EventsPageComponent implements OnInit, AfterViewInit {
  displayConnectivityError = false;
  displayNoInterestsSelectedError = false;
  feedItemsInterests: EventFeedItem[] = [];
  feedItemsBookmarked: EventFeedItem[] = [];
  currentPageIndex = 0;
  contentLoadingComplete = false;
  moreContentAvailable = true;
  startDate: Date | undefined;
  endDate: Date | undefined;
  private readonly eventService = inject(EventService);
  private readonly feedUpdateService = inject(FeedUpdateService);
  private readonly connectivityService = inject(ConnectivityService);
  private readonly destroyRef = inject(DestroyRef);

  ngOnInit(): void {
    this.subscribeToModuleError();
    this.setupFeedUpdateSubscription();
  }

  ngAfterViewInit(): void {
    void this.subscribeToFeedItems();
  }

  async doRefresh(event: any): Promise<void> {
    await this.refreshDataAndSubscriptions();
    event.target.complete();
  }

  async onFilterChanged(feedFilter: FeedFilter) {
    this.startDate = feedFilter.period.startDate ?? undefined;
    this.endDate = feedFilter.period.endDate ?? undefined;

    this.resetDataAndStates();
    await this.subscribeToFeedItems(this.startDate, this.endDate);
  }

  private async subscribeToFeedItems(
    startDate?: Date,
    endDate?: Date,
  ): Promise<void> {
    try {
      this.contentLoadingComplete = false;
      const isConnected = await this.connectivityService.checkOnlineStatus();
      if (!isConnected) {
        this.displayConnectivityError = true;
        this.contentLoadingComplete = true;
        return;
      }

      const categories = await this.eventService.getCategorySettings();
      const bookmarkedEventIds = await firstValueFrom(
        this.eventService.getBookmarkedEventIds(),
      );

      if (categories.length === 0) {
        this.handleNoCategoriesSelected();
      } else {
        const feedItemsInterests = await firstValueFrom(
          this.eventService
            .getFeedablesUpcoming(
              this.currentPageIndex,
              undefined,
              startDate,
              endDate,
              bookmarkedEventIds,
            )
            .pipe(takeUntilDestroyed(this.destroyRef)),
        );

        const feedItemsBookmarked =
          bookmarkedEventIds.length > 0
            ? await firstValueFrom(
                this.eventService
                  .getFeedablesUpcoming(
                    this.currentPageIndex,
                    undefined,
                    startDate,
                    endDate,
                    undefined,
                    bookmarkedEventIds,
                    1000,
                  )
                  .pipe(takeUntilDestroyed(this.destroyRef)),
              )
            : [];

        this.handleFeedItems(feedItemsInterests, feedItemsBookmarked);
      }
    } catch (error) {
      console.error('Error loading feed items:', error);
      this.contentLoadingComplete = true;
    }
  }

  private handleNoCategoriesSelected(): void {
    this.displayNoInterestsSelectedError = true;
    this.feedItemsInterests = [];
    this.contentLoadingComplete = true;
  }

  private handleFeedItems(
    feedItemsInterests: any,
    feedItemsBookmarked: any,
  ): void {
    this.feedItemsInterests = feedItemsInterests.content;
    this.feedItemsBookmarked = feedItemsBookmarked.content;
    this.moreContentAvailable = feedItemsInterests.content.length > 0;
    this.displayNoInterestsSelectedError =
      feedItemsInterests.content.length === 0 && !this.displayConnectivityError;
    this.contentLoadingComplete = true;
  }

  private setupFeedUpdateSubscription(): void {
    this.feedUpdateService
      .getFeedUpdatedListener()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(() => {
        void this.refreshDataAndSubscriptions();
      });
  }

  private async refreshDataAndSubscriptions(): Promise<void> {
    this.resetDataAndStates();
    await this.subscribeToFeedItems(this.startDate, this.endDate);
  }

  private resetDataAndStates() {
    this.feedItemsInterests = [];
    this.feedItemsBookmarked = [];
    this.contentLoadingComplete = false;
    this.moreContentAvailable = true;
    this.currentPageIndex = 0;
    this.displayConnectivityError = false;
    this.displayNoInterestsSelectedError = false;
  }

  private subscribeToModuleError(): void {
    this.eventService
      .getEventsModuleErrors()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((errorInfo) => {
        if (errorInfo.errorType === ErrorType.MODULE_API_ERROR) {
          this.displayConnectivityError = errorInfo.hasError;
        } else {
          this.displayConnectivityError = false;
        }
      });
  }
}
