import { Component } from '@angular/core';
import { EventSettingsMenuComponent } from '../../components/event-settings-menu/event-settings-menu.component';
import { TranslocoPipe } from '@jsverse/transloco';
import { CondensedHeaderLayoutComponent } from '@sw-code/urbo-ui';

@Component({
  selector: 'lib-event-settings-page',
  templateUrl: './event-settings-page.component.html',
  imports: [
    CondensedHeaderLayoutComponent,
    EventSettingsMenuComponent,
    TranslocoPipe,
  ],
})
export class EventSettingsPageComponent {}
