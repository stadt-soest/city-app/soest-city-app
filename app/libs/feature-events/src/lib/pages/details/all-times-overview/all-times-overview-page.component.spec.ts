import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AllTimesOverviewPageComponent } from './all-times-overview-page.component';
import { Router } from '@angular/router';
import { EventService } from '../../../services/event.service';
import { createEvent } from '../../../shared/testing/events-test-utils';
import { of } from 'rxjs';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../../transloco-testing.module';

describe('AllTimesOverviewPage', () => {
  let component: AllTimesOverviewPageComponent;
  let fixture: ComponentFixture<AllTimesOverviewPageComponent>;
  let mockEventService: jest.Mocked<EventService>;
  const mockEvent = createEvent();

  beforeEach(async () => {
    mockEventService = {
      getById: jest.fn().mockReturnValue(of(mockEvent)),
    } as unknown as jest.Mocked<EventService>;

    await TestBed.configureTestingModule({
      providers: [
        { provide: EventService, useValue: mockEventService },
        Router,
      ],
      imports: [
        AllTimesOverviewPageComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AllTimesOverviewPageComponent);
    component = fixture.componentInstance;
    component.occurrenceIndex = 0;
    component.eventId = '12345';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should set event correctly', () => {
      component.ngOnInit();
      expect(mockEventService.getById).toHaveBeenCalledWith('12345');
      expect(component.event).toEqual(mockEvent);
    });

    it('should set eventOccurence correctly', () => {
      component.ngOnInit();
      expect(component.eventOccurrence).toEqual(mockEvent.occurrences[0]);
    });

    it('should set detailsLoadingComplete  and isContentLoading  to false when subscribeToFeedItem is finished ', () => {
      component.ngOnInit();
      expect(component.detailsLoadingComplete).toBe(false);
      expect(component.isContentLoading).toBe(false);
    });
  });
});
