import { Component, DestroyRef, inject, OnInit } from '@angular/core';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { map, take } from 'rxjs';
import { TranslocoPipe, TranslocoService } from '@jsverse/transloco';
import { Title } from '@angular/platform-browser';
import { NgFor, NgIf } from '@angular/common';
import {
  IonCol,
  IonGrid,
  IonRouterLink,
  IonRow,
} from '@ionic/angular/standalone';
import {
  CondensedHeaderLayoutComponent,
  CustomButtonColorScheme,
  CustomButtonType,
  CustomHorizontalButtonComponent,
  LocalizedFormatDatePipe,
} from '@sw-code/urbo-ui';
import { Occurrence, Event } from '../../../models/event.model';
import { EventService } from '../../../services/event.service';

@Component({
  selector: 'lib-event-details',
  templateUrl: './all-times-overview-page.component.html',
  styleUrls: ['./all-times-overview-page.component.scss'],
  imports: [
    CondensedHeaderLayoutComponent,
    NgIf,
    IonGrid,
    IonRow,
    IonCol,
    NgFor,
    CustomHorizontalButtonComponent,
    RouterLink,
    IonRouterLink,
    TranslocoPipe,
    LocalizedFormatDatePipe,
  ],
})
export class AllTimesOverviewPageComponent implements OnInit {
  eventId: string | null;
  occurrenceIndex: number | null = null;
  event?: Event;
  eventOccurrence!: Occurrence;
  isContentLoading = true;
  feedLoadingComplete = false;
  detailsLoadingComplete = true;
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
  private readonly destroyRef = inject(DestroyRef);
  private readonly translocoService = inject(TranslocoService);
  private readonly titleService = inject(Title);
  private readonly route = inject(ActivatedRoute);
  private readonly eventService = inject(EventService);

  constructor() {
    this.eventId = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.subscribeToFeedItem();
  }

  private subscribeToFeedItem(): void {
    if (this.eventId) {
      this.eventService
        .getById(this.eventId)
        .pipe(takeUntilDestroyed(this.destroyRef))
        .subscribe({
          next: (event) => {
            this.event = event;
            this.eventOccurrence = event.occurrences[this.occurrenceIndex ?? 0];
            this.updateTitle();
            this.handleEventLoadComplete();
          },
          error: () => {
            this.handleEventLoadComplete();
          },
        });
    }
  }

  private updateTitle(): void {
    if (this.event) {
      this.translocoService
        .selectTranslate(
          'feature_events.details.all_times_overview_count',
          { count: this.event.occurrences.length },
          '',
        )
        .pipe(
          take(1),
          map((translatedTitle) => `${translatedTitle} - SoestApp`),
        )
        .subscribe((translatedTitle) => {
          this.titleService.setTitle(translatedTitle);
        });
    }
  }

  private handleEventLoadComplete(): void {
    this.detailsLoadingComplete = false;
    this.isContentLoading = false;
  }
}
