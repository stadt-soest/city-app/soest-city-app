import {
  Component,
  DestroyRef,
  inject,
  OnInit,
  ViewChild,
  ViewChildren,
  ViewContainerRef,
} from '@angular/core';
import { BehaviorSubject, take } from 'rxjs';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  AsyncPipe,
  NgComponentOutlet,
  NgFor,
  NgIf,
  NgStyle,
} from '@angular/common';
import { EventDetailsSkeletonComponent } from '../../components/event-details-skeleton/event-details-skeleton.component';
import {
  IonCol,
  IonGrid,
  IonInfiniteScroll,
  IonInfiniteScrollContent,
  IonRouterLink,
  IonRow,
} from '@ionic/angular/standalone';
import {
  ActionButtonBarComponent,
  CondensedHeaderLayoutComponent,
  ConnectivityErrorCardComponent,
  CustomButtonBorderScheme,
  CustomButtonColorScheme,
  CustomButtonType,
  CustomHorizontalButtonComponent,
  DetailsNotFoundErrorCardComponent,
  GeneralIconComponent,
  IconFontSize,
  IconType,
  LightboxDirective,
  LocalizedDateDiffPipePipe,
  LocalizedFormatDatePipe,
  LocalizedFormatDistanceTimePipePipe,
  LocalizedTimeAgoPipe,
  LocalizedTimeDistancePipePipe,
  LocalizedTimeRangePipe,
  LocalizedWeekDayPipe,
  NewContentTextComponent,
  SafeExternalLinksDirective,
  SkeletonFeedCardComponent,
} from '@sw-code/urbo-ui';
import { EventStatusContentComponent } from '../../components/status-content/event-status-content.component';
import { EventStatusStylesPipe } from '../../pipes/status/styles/status-styles.pipe';
import { EventFeedItem } from '../../models/event-feed-item.model';
import { EVENTS_MODULE_INFO } from '../../feature-events.module';
import { Event, Occurrence } from '../../models/event.model';
import { SortOptions } from '../../enums/sorting.enum';
import {
  BrowserService,
  ErrorType,
  FeedUpdateService,
  ImageService,
  MapsNavigationService,
  ModuleInfo,
  TitleService,
} from '@sw-code/urbo-core';
import { LocalEventStatus } from '../../models/local-event-status';
import { EventService } from '../../services/event.service';
import { EventShareService } from '../../services/share/event-share.service';
import { EventCalendarService } from '../../services/calendar/event-calendar.service';
import { EventBookmarkService } from '../../services/bookmark/event-bookmark.service';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'lib-event-details',
  templateUrl: './event-details-page.component.html',
  styleUrls: ['./event-details-page.component.scss'],
  imports: [
    TranslocoDirective,
    NgIf,
    EventDetailsSkeletonComponent,
    IonGrid,
    IonCol,
    LightboxDirective,
    IonRow,
    ActionButtonBarComponent,
    SafeExternalLinksDirective,
    NgFor,
    CustomHorizontalButtonComponent,
    RouterLink,
    IonRouterLink,
    ConnectivityErrorCardComponent,
    DetailsNotFoundErrorCardComponent,
    NgComponentOutlet,
    SkeletonFeedCardComponent,
    NewContentTextComponent,
    IonInfiniteScroll,
    IonInfiniteScrollContent,
    AsyncPipe,
    LocalizedTimeAgoPipe,
    LocalizedTimeDistancePipePipe,
    LocalizedFormatDatePipe,
    LocalizedDateDiffPipePipe,
    LocalizedTimeRangePipe,
    EventStatusContentComponent,
    CondensedHeaderLayoutComponent,
    EventStatusStylesPipe,
  ],
})
export class EventDetailsPageComponent implements OnInit {
  @ViewChildren('eventCard', { read: ViewContainerRef })
  containers!: ViewContainerRef[];
  @ViewChild(CondensedHeaderLayoutComponent)
  condensedHeaderLayout!: CondensedHeaderLayoutComponent;
  eventId: string | null = null;
  event: Event | null = null;
  errorModule = false;
  eventNotFound = false;
  eventOccurrence!: Occurrence;
  feedItems: EventFeedItem[] = [];
  currentPageIndex = 0;
  imageUrl: string | null = null;
  isScrolled = false;
  startDate = '';
  hasFeedItems$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  hasFeedItems = true;
  moduleInfo!: ModuleInfo;
  eventDurationDays!: number;
  isContentLoading = true;
  feedLoadingComplete = false;
  moreContentAvailable = true;
  detailsLoadingComplete = true;
  excludedEvents: string[] = [];
  isBookmarkedEvent = false;
  protected readonly iconFontSize = IconFontSize;
  protected readonly eventStatus = LocalEventStatus;
  protected readonly iconType = IconType;
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonBorderScheme = CustomButtonBorderScheme;
  protected readonly customButtonType = CustomButtonType;
  private readonly destroyRef = inject(DestroyRef);
  private readonly route = inject(ActivatedRoute);
  private readonly eventService = inject(EventService);
  private readonly imageService = inject(ImageService);
  private readonly browserService = inject(BrowserService);
  private readonly shareService = inject(EventShareService);
  private readonly mapsNavigationService = inject(MapsNavigationService);
  private readonly eventCalendarService = inject(EventCalendarService);
  private readonly eventBookmarkService = inject(EventBookmarkService);
  private readonly localizedWeekDayPipe = inject(LocalizedWeekDayPipe);
  private readonly localizedFormatDistanceTimePipe = inject(
    LocalizedFormatDistanceTimePipePipe,
  );
  private readonly feedUpdateService = inject(FeedUpdateService);
  private readonly eventsModuleInfo = inject<ModuleInfo>(EVENTS_MODULE_INFO);
  private readonly titleService = inject(TitleService);
  private readonly router = inject(Router);

  get otherOccurrences(): Occurrence[] {
    return (
      this.event?.occurrences.filter(
        (occurrence) => occurrence.id !== this.event?.selectedOccurrenceId,
      ) || []
    );
  }

  get pageTitleKey() {
    if (this.errorModule) {
      return 'ui.error-page-title.no_signal';
    } else if (this.eventNotFound) {
      return 'ui.error-page-title.not_found';
    } else {
      return '';
    }
  }

  ngOnInit(): void {
    this.subscribeToErrors();
    this.eventId = this.route.snapshot.paramMap.get('id');
    this.moduleInfo = this.eventsModuleInfo;
    this.setExcludedEvents();
    this.subscribeToFeedItem();
  }

  async scrollToTop() {
    await this.condensedHeaderLayout.scrollToTop();
  }

  async navigateToLocation(): Promise<void> {
    if (!this.event?.coordinates) {
      return;
    }

    await this.mapsNavigationService.navigateToLocation(
      this.event.coordinates.lat,
      this.event.coordinates.long,
    );
  }

  async addEventToCalendar(): Promise<void> {
    if (this.event) {
      await this.eventCalendarService.createCalendarEvent(
        this.event,
        this.eventOccurrence,
      );
    }
  }

  async bookmarkEvent() {
    if (this.event) {
      if (this.isBookmarkedEvent) {
        await this.eventBookmarkService.removeBookmark(
          this.event.selectedOccurrenceId,
        );
      } else {
        await this.eventBookmarkService.addBookmark(
          this.event.selectedOccurrenceId,
        );
      }
      this.isBookmarkedEvent = !this.isBookmarkedEvent;
      this.feedUpdateService.notifyFeedUpdate();
    }
  }

  loadMoreFeedItems(event: any) {
    if (!this.event) {
      return;
    }

    this.feedLoadingComplete = false;
    this.currentPageIndex++;
    this.eventService
      .getFeedablesGrouped(
        this.currentPageIndex,
        SortOptions.startAtAsc,
        this.excludedEvents,
      )
      .pipe(take(1))
      .subscribe((feedItems) => {
        this.moreContentAvailable = feedItems.content.length > 0;
        this.feedItems.push(...feedItems.content);
        this.feedLoadingComplete = true;
        event.target.complete();
      });
  }

  isEventDurationOneDayOrMore(startDateStr: Date, endDateStr: Date): boolean {
    const durationHours =
      (endDateStr.getTime() - startDateStr.getTime()) / (1000 * 3600);
    return durationHours >= 24;
  }

  getCombinedTitle(occurrenceStart: Date): string {
    const weekDay = this.localizedWeekDayPipe.transform(occurrenceStart);
    const timeDistance = this.localizedFormatDistanceTimePipe.transform(
      occurrenceStart,
      undefined,
      'week',
    );
    return `${weekDay}, ${timeDistance}`;
  }

  getEventDurationDays(startDate: Date, endDate: Date): number {
    return (
      Math.ceil(
        (endDate.getTime() - startDate.getTime()) / (1000 * 3600 * 24),
      ) + 1
    );
  }

  getRelativeStartDays(startDateStr: string): number {
    const today = new Date();
    const startDate = new Date(startDateStr);

    today.setHours(0, 0, 0, 0);
    startDate.setHours(0, 0, 0, 0);

    const diffTime = Math.abs(startDate.getTime() - today.getTime());
    return Math.ceil(diffTime / (1000 * 3600 * 24));
  }

  async shareEvent() {
    if (this.event) {
      await this.shareService.shareEvent(this.event);
    }
  }

  async openEventUrl() {
    if (this.event) {
      await this.browserService.openInAppBrowser(this.eventOccurrence.source);
    }
  }

  private subscribeToFeedItem(): void {
    if (this.eventId) {
      this.eventService
        .getById(this.eventId)
        .pipe(takeUntilDestroyed(this.destroyRef))
        .subscribe({
          next: (event) => {
            this.event = event;
            this.updateTitle();
            this.eventOccurrence = event.occurrences.find(
              (occurrence) => occurrence.id === this.eventId,
            ) as Occurrence;
            this.excludedEvents.push(this.event.selectedOccurrenceId);
            this.imageUrl = this.imageService.getImageUrl(event.image);
            this.eventDurationDays = this.getEventDurationDays(
              this.eventOccurrence.start,
              this.eventOccurrence.end,
            );
            this.handleEventLoadComplete();
          },
          error: () => {
            this.handleEventLoadComplete();
          },
        });
    }
  }

  private checkHasContent(): void {
    if (this.feedItems.length === 0) {
      this.hasFeedItems = false;
      this.hasFeedItems$.next(false);
    } else {
      this.hasFeedItems = true;
      this.hasFeedItems$.next(true);
    }
  }

  private handleEventLoadComplete() {
    this.detailsLoadingComplete = false;
    this.isContentLoading = false;
    this.loadBookmarkedState();
    this.subscribeToRelatedFeedItems();
  }

  private async loadBookmarkedState() {
    if (this.event) {
      this.isBookmarkedEvent =
        await this.eventBookmarkService.isEventBookmarked(
          this.event.selectedOccurrenceId,
        );
    }
  }

  private subscribeToRelatedFeedItems(): void {
    this.eventService
      .getFeedablesGrouped(
        this.currentPageIndex,
        SortOptions.startAtAsc,
        this.excludedEvents,
      )
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((feedItems) => {
        this.feedItems = feedItems.content;
        this.moreContentAvailable = feedItems.content.length > 0;
        this.feedLoadingComplete = true;
        this.checkHasContent();
      });
  }

  private subscribeToErrors(): void {
    this.eventService
      .getEventsModuleErrors()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((errorInfo) => {
        if (errorInfo.errorType === ErrorType.MODULE_API_ERROR) {
          this.errorModule = errorInfo.hasError;
        }
        if (
          errorInfo.errorType === ErrorType.DETAILS_API_ERROR &&
          this.eventId === errorInfo.id
        ) {
          if (errorInfo.hasError) {
            this.excludedEvents = [];
          }
          this.eventNotFound = errorInfo.hasError;
        }
      });
  }

  private updateTitle(): void {
    if (this.event) {
      this.titleService.setTitle(this.event.title);
    }
  }

  private setExcludedEvents() {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation?.extras.state;

    this.excludedEvents = state?.['excludeEventIds']
      ? Array.from(new Set(state['excludeEventIds'].split(',')))
      : [];
  }
}
