import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EventDetailsPageComponent } from './event-details-page.component';
import { EventService } from '../../services/event.service';
import {
  BrowserService,
  CoreTestModule,
  DeviceService,
  ErrorType,
  ImageService,
} from '@sw-code/urbo-core';
import { EventShareService } from '../../services/share/event-share.service';
import { EventCalendarService } from '../../services/calendar/event-calendar.service';
import { EventBookmarkService } from '../../services/bookmark/event-bookmark.service';
import {
  createEvent,
  createEventFeedItem,
  createEventsModuleInfo,
} from '../../shared/testing/events-test-utils';
import { of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { EVENTS_MODULE_INFO } from '../../feature-events.module';
import { TextFormattingPipe } from '../../pipes/text-formatting/text-formatting.pipe';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../transloco-testing.module';

describe('DetailsPage', () => {
  let component: EventDetailsPageComponent;
  let mockEventService: jest.Mocked<EventService>;
  let mockImageService: jest.Mocked<ImageService>;
  let mockViewContainerRef: any;
  let mockActivatedRoute: any;
  let fixture: ComponentFixture<EventDetailsPageComponent>;
  let mockShareService: jest.Mocked<EventShareService>;
  let mockBrowserService: jest.Mocked<BrowserService>;
  let mockDeviceService: jest.Mocked<DeviceService>;
  let mockEventCalendarService: jest.Mocked<EventCalendarService>;
  let mockEventBookmarkService: jest.Mocked<EventBookmarkService>;
  let windowOpenSpy: jest.SpyInstance;
  const mockEvent = createEvent();

  beforeEach(async () => {
    mockShareService = {
      shareEvent: jest.fn().mockResolvedValue(undefined),
    } as unknown as jest.Mocked<EventShareService>;

    mockDeviceService = {
      isWebPlatform: jest.fn(),
      isAndroid: jest.fn(),
    } as unknown as jest.Mocked<DeviceService>;

    mockBrowserService = {
      openInExternalBrowser: jest.fn().mockResolvedValue(undefined),
      canOpenApp: jest.fn().mockResolvedValue(false),
    } as unknown as jest.Mocked<BrowserService>;

    mockEventBookmarkService = {
      isEventBookmarked: jest.fn(),
      addBookmark: jest.fn(),
      removeBookmark: jest.fn(),
    } as unknown as jest.Mocked<EventBookmarkService>;

    const mockInstances = [];

    mockViewContainerRef = {
      createComponent: jest.fn(() => {
        const mockInstance = { feedItem: null };
        mockInstances.push(mockInstance);
        return { instance: mockInstance };
      }),
      clear: jest.fn(),
    };

    mockImageService = {
      getImageUrl: jest.fn().mockReturnValue('https://example.com/image.jpg'),
      getHighResImageUrl: jest
        .fn()
        .mockReturnValue('https://example.com/image.jpg'),
    } as unknown as jest.Mocked<ImageService>;

    mockEventService = {
      getFeedablesDateCreated: jest
        .fn()
        .mockReturnValue(of({ content: [], totalRecords: 0 })),
      getFeedablesGrouped: jest
        .fn()
        .mockReturnValue(of({ content: [], totalRecords: 0 })),
      getById: jest.fn().mockReturnValue(of(mockEvent)),
      subscribeEventsModuleErrors: jest.fn(),
      getEventsModuleErrors: jest.fn().mockReturnValue(of()),
    } as unknown as jest.Mocked<EventService>;

    mockEventCalendarService = {
      createMultipleDayEvent: jest.fn().mockReturnValue(undefined),
      calendarSupported: jest.fn().mockReturnValue(true),
    } as unknown as jest.Mocked<EventCalendarService>;

    mockActivatedRoute = {
      queryParamMap: of(),
      snapshot: {
        paramMap: {
          get: jest.fn().mockReturnValue('default-occurrence-id'),
        },
      },
    };

    await TestBed.configureTestingModule({
      providers: [
        { provide: EventService, useValue: mockEventService },
        { provide: ImageService, useValue: mockImageService },
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        { provide: EventShareService, useValue: mockShareService },
        { provide: EventCalendarService, useValue: mockEventCalendarService },
        { provide: EventBookmarkService, useValue: mockEventBookmarkService },
        {
          provide: EVENTS_MODULE_INFO,
          useFactory: () => createEventsModuleInfo,
        },
      ],
      imports: [
        EventDetailsPageComponent,
        TextFormattingPipe,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
    }).compileComponents();

    TestBed.overrideProvider(DeviceService, { useValue: mockDeviceService });
    TestBed.overrideProvider(BrowserService, { useValue: mockBrowserService });

    fixture = TestBed.createComponent(EventDetailsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    windowOpenSpy = jest.spyOn(window, 'open').mockImplementation();
  });

  afterEach(() => {
    if (windowOpenSpy) {
      windowOpenSpy.mockRestore();
    }
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should set event and imageUrl when eventId is present', () => {
      component.ngOnInit();
      expect(mockEventService.getById).toHaveBeenCalledWith(
        'default-occurrence-id',
      );
      expect(component.imageUrl).toBe('https://example.com/image.jpg');
    });

    it('should set hasFeedItems$ and  hasFeedItems to false when ngOnit is called and no feed items are returned', () => {
      mockEventService.getFeedablesGrouped.mockReturnValue(
        of({
          content: [],
          totalRecords: 0,
        }),
      );
      component.ngOnInit();
      fixture.detectChanges();
      expect(component.hasFeedItems$.getValue()).toBe(false);
      expect(component.hasFeedItems).toBe(false);
    });

    it('should set hasFeedItems$ and  hasFeedItems to true when ngOnit is called and feed items are returned', () => {
      mockEventService.getFeedablesGrouped.mockReturnValue(
        of({
          content: [createEventFeedItem({ id: '1' })],
          totalRecords: 1,
        }),
      );
      component.ngOnInit();
      expect(component.hasFeedItems$.getValue()).toBe(true);
      expect(component.hasFeedItems).toBe(true);
    });

    it('should set errorModule to true when Module API has error', () => {
      mockEventService.getEventsModuleErrors.mockReturnValue(
        of({
          hasError: true,
          errorType: ErrorType.MODULE_API_ERROR,
        }),
      );
      component.ngOnInit();
      expect(component.errorModule).toBe(true);
    });

    it('should set errorModule to false when Module API has no error', () => {
      mockEventService.getEventsModuleErrors.mockReturnValue(
        of({
          hasError: false,
          errorType: ErrorType.MODULE_API_ERROR,
        }),
      );
      component.ngOnInit();
      expect(component.errorModule).toBe(false);
    });

    it('should set errorDetails to true when Details API has error', () => {
      component.eventId = '1';
      mockEventService.getEventsModuleErrors.mockReturnValue(
        of({
          hasError: true,
          errorType: ErrorType.DETAILS_API_ERROR,
          id: '1',
        }),
      );
      component.ngOnInit();
      expect(component.eventNotFound).toBe(true);
    });

    it('should set errorDetails to false when Details API has no error', () => {
      component.eventId = '1';
      mockEventService.getEventsModuleErrors.mockReturnValue(
        of({
          hasError: false,
          errorType: ErrorType.DETAILS_API_ERROR,
          id: '1',
        }),
      );
      component.ngOnInit();
      expect(component.eventNotFound).toBe(false);
    });
  });

  describe('loadMoreFeedItems', () => {
    it('should update feed items on call', () => {
      mockEventService.getFeedablesGrouped.mockReturnValue(
        of({
          content: [
            createEventFeedItem({ id: '1' }),
            createEventFeedItem({ id: '2' }),
            createEventFeedItem({ id: '3' }),
          ],
          totalRecords: 3,
        }),
      );
      component.currentPageIndex = 0;
      component.loadMoreFeedItems({ target: { complete: jest.fn() } });
      expect(component.feedItems.length).toBe(3);
      expect(component.feedItems[0].id).toBe('1');
    });
    it('should call complete on event at the end of loadMoreFeedItems', () => {
      const complete = jest.fn();
      mockEventService.getFeedablesGrouped.mockReturnValue(
        of({
          content: [
            createEventFeedItem({ id: '1' }),
            createEventFeedItem({ id: '2' }),
            createEventFeedItem({ id: '3' }),
          ],
          totalRecords: 3,
        }),
      );
      component.currentPageIndex = 0;
      component.loadMoreFeedItems({ target: { complete } });
      expect(complete).toHaveBeenCalled();
    });

    it('should increment paginationPage on call', () => {
      mockEventService.getFeedablesGrouped.mockReturnValue(
        of({
          content: [
            createEventFeedItem({ id: '1' }),
            createEventFeedItem({ id: '2' }),
            createEventFeedItem({ id: '3' }),
          ],
          totalRecords: 3,
        }),
      );
      component.currentPageIndex = 0;
      component.loadMoreFeedItems({ target: { complete: jest.fn() } });
      expect(component.currentPageIndex).toBe(1);
    });
  });

  describe('HTML Rendering', () => {
    it('should render lib-event-event-details-skeleton when detailsLoadingComplete is true', () => {
      component.detailsLoadingComplete = true;
      fixture.detectChanges();
      const skeleton = fixture.nativeElement.querySelector(
        'lib-event-details-skeleton',
      );
      expect(skeleton).toBeTruthy();
    });
    it('should render ion-grid with events when detailsLoadingComplete is false', () => {
      component.detailsLoadingComplete = false;
      fixture.detectChanges();
      const grid = fixture.nativeElement.querySelector('ion-grid');
      expect(grid).toBeTruthy();
    });
  });

  describe('shareEvent', () => {
    it('should call shareEvent with the correct parameters', async () => {
      await component.shareEvent();

      expect(mockShareService.shareEvent).toHaveBeenCalledWith(mockEvent);
    });
  });

  describe('getEventDurationDays', () => {
    it('should return the correct duration in days', () => {
      const startDate = '2024-01-01';
      const endDate = '2024-01-05';
      const duration = component.getEventDurationDays(
        new Date(startDate),
        new Date(endDate),
      );
      expect(duration).toBe(5);
    });
  });

  describe('getRelativeStartDays', () => {
    it('should return the correct number of days to start date', () => {
      const today = new Date();
      const tomorrow = new Date(today);
      tomorrow.setDate(tomorrow.getDate() + 1);

      const relativeDays = component.getRelativeStartDays(
        tomorrow.toISOString(),
      );
      expect(relativeDays).toBe(1);
    });
  });

  describe('navigateToLocation', () => {
    it('should open Google Maps URL for web platform', async () => {
      mockDeviceService.isWebPlatform.mockReturnValue(true);
      component.event = { coordinates: { lat: '123', long: '456' } } as any;

      await component.navigateToLocation();

      expect(windowOpenSpy).toHaveBeenCalledWith(
        'https://www.google.com/maps/search/?api=1&query=123,456',
        '_blank',
      );
    });

    it('should open geo URL for non-web platform', async () => {
      mockDeviceService.isWebPlatform.mockReturnValue(false);
      mockDeviceService.isAndroid.mockReturnValue(true);
      component.event = { coordinates: { lat: '123', long: '456' } } as any;

      await component.navigateToLocation();

      expect(mockBrowserService.openInExternalBrowser).toHaveBeenCalledWith(
        'geo:0,0?q=123,456',
      );
    });
  });

  describe('isEventDurationOneDayOrMore', () => {
    it('should return true for events lasting exactly 24 hours', () => {
      const start = new Date('2024-02-17T10:00:00');
      const end = new Date('2024-02-18T10:00:00');
      expect(component.isEventDurationOneDayOrMore(start, end)).toBe(true);
    });

    it('should return false for events lasting less than 24 hours', () => {
      const start = new Date('2024-02-17T10:00:00');
      const end = new Date('2024-02-17T22:00:00');
      expect(component.isEventDurationOneDayOrMore(start, end)).toBe(false);
    });

    it('should return true for events lasting more than 24 hours', () => {
      const start = new Date('2024-02-17T10:00:00');
      const end = new Date('2024-02-18T10:00:00');
      expect(component.isEventDurationOneDayOrMore(start, end)).toBe(true);
    });

    it('should return true for multi-day events', () => {
      const start = new Date('2024-02-17T10:00:00');
      const end = new Date('2024-02-20T10:00:00');
      expect(component.isEventDurationOneDayOrMore(start, end)).toBe(true);
    });

    it('should return false  for events that run from one day to the next', () => {
      const start = new Date('2024-02-17T10:00:00');
      const end = new Date('2024-02-18T09:00:00');
      expect(component.isEventDurationOneDayOrMore(start, end)).toBe(false);
    });
  });

  describe('render eventAddress', () => {
    it('should display the correct address from eventAddressToString', () => {
      component.event = mockEvent;

      fixture.detectChanges();

      const addressElement =
        fixture.nativeElement.querySelector('.location-address');
      expect(addressElement.textContent).toContain(
        '123 Default St, 12345 Default City.',
      );
    });

    it('should display streetAddress, postalCode when available', () => {
      component.event = createEvent();
      component.event!.location = {
        locationName: 'Default Location',
        city: undefined,
        postalCode: '12345',
        streetAddress: '123 Default St',
      };

      fixture.detectChanges();

      const addressElement =
        fixture.nativeElement.querySelector('.location-address');
      expect(addressElement.textContent).toContain('123 Default St, 12345.');
    });

    it('should return null when address is not available', () => {
      component.event = createEvent();
      component.event!.location = undefined;
      fixture.detectChanges();

      const addressElement =
        fixture.nativeElement.querySelector('.location-address');
      expect(addressElement).toBe(null);
    });
  });
});
