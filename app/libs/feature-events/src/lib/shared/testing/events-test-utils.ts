import {
  FeedItemLayoutType,
  ModuleCategory,
  ModuleInfo,
  ModuleSettings,
} from '@sw-code/urbo-core';
import { EventFeedCardComponent } from '../../components/event-feed-card/event-feed-card.component';
import { EventFeedItem } from '../../models/event-feed-item.model';
import { EventForYouCardComponent } from '../../components/event-for-you-card/event-for-you-card.component';
import { Event, Image } from '../../models/event.model';
import { LocalEventStatus } from '../../models/local-event-status';

export const createEventsModuleInfo: ModuleInfo = {
  id: 'events',
  name: 'Events',
  category: ModuleCategory.onTheWay,
  icon: 'event',
  baseRoutingPath: '/events',
  defaultVisibility: true,
  settingsPageAvailable: true,
  isAboutAppItself: false,
  relevancy: 0.5,
  forYouRelevant: true,
  showInCategoriesPage: true,
  orderInCategory: 1,
};

export const createEvent = (): Event =>
  new Event({
    id: 'default-id',
    selectedOccurrenceId: 'default-occurrence-id',
    title: 'Default Title',
    description: 'Default Description',
    image: {
      imageSource: 'default-image.jpg',
      copyrightHolder: 'default-copyright-holder',
      description: 'default-image-description',
      thumbnail: {
        filename: 'default-file.jpg',
        width: 100,
        height: 100,
      },
      highRes: {
        filename: 'default-file.jpg',
        width: 100,
        height: 100,
      },
    },
    occurrences: [
      {
        id: 'default-occurrence-id',
        status: LocalEventStatus.scheduled,
        source: 'default-source',
        dateCreated: '2023-10-20T00:00:00Z',
        start: new Date('2023-10-20T08:00:00Z'),
        end: new Date('2023-10-20T12:00:00Z'),
      },
    ],
    categoryIds: [
      'Default Category id 1',
      'Default Category id 2',
      'Default Category id 3',
    ],
    dateModified: new Date('2023-10-20T00:00:00.000Z'),
    location: {
      locationName: 'Default Location',
      city: 'Default City',
      postalCode: '12345',
      streetAddress: '123 Default St',
    },
    coordinates: {
      lat: 0,
      long: 0,
    },
    contactPoint: {
      telephone: '123-456-7890',
      email: 'default@example.com',
    },
    subTitle: 'Default Subtitle',
  });

export const createEventFeedItem = (
  overrides?: Partial<EventFeedItem>,
): EventFeedItem => ({
  id: 'default-id',
  moduleId: 'events',
  moduleName: 'feature_events.module_name_singular',
  pluralModuleName: 'feature_events.module_name',
  baseRoutePath: 'events',
  icon: 'event',
  title: 'Default Title',
  creationDate: new Date('2023-10-20T00:00:00Z'),
  startDate: new Date('2023-10-20T08:00:00'),
  image: createImage(),
  status: LocalEventStatus.scheduled,
  cardComponent: EventFeedCardComponent,
  forYouComponent: EventForYouCardComponent,
  relevancy: 0,
  layoutType: FeedItemLayoutType.card,
  ...overrides,
});

export const createImage = (overrides?: Partial<Image>): Image => ({
  description: 'default-image-description',
  imageSource: 'default-image.jpg',
  thumbnail: {
    filename: 'default-file.jpg',
    width: 100,
    height: 100,
  },
  highRes: {
    filename: 'default-file.jpg',
    width: 100,
    height: 100,
  },
  ...overrides,
});

export const createModuleSettingsEvents = (
  overrides?: Partial<ModuleSettings>,
): ModuleSettings => ({
  id: undefined,
  name: 'events',
  ...overrides,
});
