import { Observable } from 'rxjs';
import { EventFeedItem } from './models/event-feed-item.model';
import { SortOptions } from './enums/sorting.enum';
import { Event, EventCategory } from './models/event.model';

export abstract class EventAdapter {
  abstract getByOccurrenceId(id: string): Observable<Event>;

  abstract getFeed(selectedCategories: string[]): Observable<EventFeedItem[]>;

  abstract getFeedablesUpcoming(
    page: number,
    categories: string[],
    bookmarkedEventIds: string[],
    excludedEvents: string[],
    startDateIso: string,
    endDateIso?: string,
    size?: number,
  ): Observable<{ content: EventFeedItem[]; totalRecords: number }>;

  abstract getFeedablesUnfiltered(
    page: number,
    startDateIso: string,
  ): Observable<{ content: EventFeedItem[]; totalRecords: number }>;

  abstract getFeedablesGrouped(
    page: number,
    sortOption: SortOptions,
    selectedCategories: string[],
    excludedEvents: string[],
    startDateIso: string,
  ): Observable<{ content: EventFeedItem[]; totalRecords: number }>;

  abstract getBySearchTerm(
    searchTerm: string,
    pageNumber: number,
    pageSize: number,
  ): Observable<{ total: number; items: EventFeedItem[] }>;

  abstract getCategories(): Observable<EventCategory[]>;
}
