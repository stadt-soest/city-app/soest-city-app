import { inject, Injectable } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';
import { BaseShareService } from '@sw-code/urbo-core';
import { Event } from '../../models/event.model';

@Injectable()
export class EventShareService {
  private readonly baseShareService = inject(BaseShareService);
  private readonly translateService = inject(TranslocoService);

  async shareEvent(event: Event): Promise<void> {
    const title = event.title;
    const url = window.location.href;
    const text = this.translateService.translate(
      'feature_events.details.share.content',
      {
        title,
        url,
      },
    );

    await this.baseShareService.share(title, text, url);
  }
}
