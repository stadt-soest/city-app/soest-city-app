import { TestBed } from '@angular/core/testing';
import { EventShareService } from './event-share.service';
import { TranslocoService } from '@jsverse/transloco';
import { BaseShareService } from '@sw-code/urbo-core';

describe('EventShareService', () => {
  let service: EventShareService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        EventShareService,
        { provide: BaseShareService, useValue: {} },
        { provide: TranslocoService, useValue: {} },
      ],
    });
    service = TestBed.inject(EventShareService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
