import { inject, Injectable } from '@angular/core';
import { forkJoin, map, Observable, of, switchMap } from 'rxjs';
import { endOfDay } from 'date-fns';
import { EventService } from '../event.service';
import { FeedResult } from '@sw-code/urbo-core';
import { EventFeedItem } from '../../models/event-feed-item.model';
import { SortOptions } from '../../enums/sorting.enum';

@Injectable()
export class EventFeedablesLoaderService {
  private readonly eventService = inject(EventService);

  getFeedables(paginationPage: number): Observable<FeedResult<EventFeedItem>> {
    return forkJoin([this.eventService.getCategorySettings()]).pipe(
      switchMap(([categories]) => {
        if (categories.length === 0) {
          return of({
            items: [],
            totalRecords: 0,
          });
        } else {
          return this.eventService
            .getFeedablesGrouped(paginationPage, SortOptions.dateCreatedAsc)
            .pipe(
              map((response) => ({
                items: response.content,
                totalRecords: response.totalRecords,
              })),
            );
        }
      }),
    );
  }

  getFeedablesUpcoming(
    paginationPage: number,
  ): Observable<FeedResult<EventFeedItem>> {
    return forkJoin([this.eventService.getCategorySettings()]).pipe(
      switchMap(([categories]) => {
        if (categories.length === 0) {
          return of({
            items: [],
            totalRecords: 0,
          });
        } else {
          return this.eventService.getFeedablesUpcoming(paginationPage).pipe(
            map((response) => ({
              items: response.content,
              totalRecords: response.totalRecords,
            })),
          );
        }
      }),
    );
  }

  getFeedablesUnfiltered(
    paginationPage: number,
  ): Observable<FeedResult<EventFeedItem>> {
    return this.eventService.getFeedablesUnfiltered(paginationPage).pipe(
      map((response) => ({
        items: response.content,
        totalRecords: response.totalRecords,
      })),
    );
  }

  getBookmarkedFeedables(
    paginationPage: number,
  ): Observable<FeedResult<EventFeedItem>> {
    return this.eventService.getBookmarkedEventIds().pipe(
      switchMap((bookmarkedEventIds) => {
        if (bookmarkedEventIds.length === 0) {
          return of({
            items: [],
            totalRecords: 0,
          });
        } else {
          return this.eventService
            .getFeedablesUpcoming(
              paginationPage,
              undefined,
              new Date(),
              endOfDay(new Date()),
              undefined,
              bookmarkedEventIds,
              1000,
            )
            .pipe(
              map((response) => ({
                items: response.content,
                totalRecords: response.totalRecords,
              })),
            );
        }
      }),
    );
  }
}
