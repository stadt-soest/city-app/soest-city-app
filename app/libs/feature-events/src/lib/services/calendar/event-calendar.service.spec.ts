import { TestBed } from '@angular/core/testing';
import { EventCalendarService } from './event-calendar.service';
import {
  BaseShareService,
  CalendarService,
  DeviceService,
} from '@sw-code/urbo-core';
import { createEvent } from '../../shared/testing/events-test-utils';
import { LocalEventStatus } from '../../models/local-event-status';

describe('EventCalendarService', () => {
  let service: EventCalendarService;
  let mockBaseShareService: Partial<BaseShareService>;

  let mockDeviceService: Partial<DeviceService>;
  let mockCalendarService: jest.Mocked<CalendarService>;

  beforeEach(() => {
    mockBaseShareService = {
      cleanUrlScheme: jest.fn().mockImplementation((url) => url),
    };

    mockCalendarService = {
      createEvent: jest.fn(),
    } as never;

    TestBed.configureTestingModule({
      providers: [
        EventCalendarService,
        { provide: BaseShareService, useValue: mockBaseShareService },
        { provide: DeviceService, useValue: mockDeviceService },
        { provide: CalendarService, useValue: mockCalendarService },
      ],
    });
    service = TestBed.inject(EventCalendarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('createMultipleDayEvent', () => {
    it('should create an event for each day between start and end dates', async () => {
      const event = createEvent();

      event.occurrences = [
        {
          id: 'default-occurrence-id',
          status: LocalEventStatus.scheduled,
          source: 'default-source',
          dateCreated: '2023-10-20T00:00:00Z',
          start: new Date('2023-10-20T08:00:00'),
          end: new Date('2023-10-22T12:00:00'),
        },
      ];

      const spy = jest.spyOn(mockCalendarService, 'createEvent');

      await service.createCalendarEvent(event, event.occurrences[0]);

      expect(spy).toHaveBeenCalledTimes(1);
    });
  });
});
