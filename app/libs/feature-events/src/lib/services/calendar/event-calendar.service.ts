import { inject, Injectable } from '@angular/core';
import { BaseShareService, CalendarService } from '@sw-code/urbo-core';
import { Event, Occurrence } from '../../models/event.model';

@Injectable()
export class EventCalendarService {
  private readonly baseShareService = inject(BaseShareService);
  private readonly calendarService = inject(CalendarService);

  async createCalendarEvent(
    event: Event,
    eventOccurence: Occurrence,
  ): Promise<void> {
    const startDate = eventOccurence.start;
    const endDate = eventOccurence.end;

    await this.calendarService.createEvent(
      eventOccurence.id,
      event.title,
      event.location?.locationName ?? '',
      event.description +
        '\n\n' +
        this.baseShareService.cleanUrlScheme(window.location.href),
      startDate,
      endDate,
    );
  }
}
