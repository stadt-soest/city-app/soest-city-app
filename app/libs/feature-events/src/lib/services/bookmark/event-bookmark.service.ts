import { inject, Injectable } from '@angular/core';
import { StorageService } from '@sw-code/urbo-core';
import { EVENT_BOOKMARKED_KEY } from '../../feature-events.module';

@Injectable()
export class EventBookmarkService {
  private readonly storageService = inject(StorageService);

  async isEventBookmarked(eventId: string): Promise<boolean> {
    const bookmarks = await this.getBookmarkedEvents();
    return bookmarks.includes(eventId);
  }

  async addBookmark(eventId: string): Promise<void> {
    const bookmarks = await this.getBookmarkedEvents();
    if (!bookmarks.includes(eventId)) {
      bookmarks.push(eventId);
      await this.storageService.set(
        EVENT_BOOKMARKED_KEY,
        JSON.stringify(bookmarks),
      );
    }
  }

  async removeBookmark(eventId: string): Promise<void> {
    let bookmarks = await this.getBookmarkedEvents();
    bookmarks = bookmarks.filter((id) => id !== eventId);
    await this.storageService.set(
      EVENT_BOOKMARKED_KEY,
      JSON.stringify(bookmarks),
    );
  }

  private async getBookmarkedEvents(): Promise<string[]> {
    const favoritesJson = await this.storageService.get(EVENT_BOOKMARKED_KEY);
    return favoritesJson ? JSON.parse(favoritesJson) : [];
  }
}
