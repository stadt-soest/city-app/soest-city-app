import { TestBed } from '@angular/core/testing';
import { EventBookmarkService } from './event-bookmark.service';
import { StorageService } from '@sw-code/urbo-core';
import { EVENT_BOOKMARKED_KEY } from '../../feature-events.module';

describe('EventBookmarkService', () => {
  let service: EventBookmarkService;
  let mockStorageService: jest.Mocked<StorageService>;

  beforeEach(() => {
    mockStorageService = {
      set: jest.fn(),
      get: jest.fn(),
    } as unknown as jest.Mocked<StorageService>;

    TestBed.configureTestingModule({
      providers: [
        EventBookmarkService,
        {
          provide: StorageService,
          useValue: mockStorageService,
        },
      ],
    });
    service = TestBed.inject(EventBookmarkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('isEventBookmarked', () => {
    it('should return true if event is saved in storage', async () => {
      const bookmarkedEvents = ['123', '456'];
      mockStorageService.get.mockResolvedValue(
        JSON.stringify(bookmarkedEvents),
      );

      const result = await service.isEventBookmarked(bookmarkedEvents[0]);
      expect(result).toEqual(true);
    });

    it('should return false if event is not saved in storage', async () => {
      const bookmarkedEvents = ['123', '456'];
      mockStorageService.get.mockResolvedValue(
        JSON.stringify(bookmarkedEvents),
      );

      const result = await service.isEventBookmarked('1337');
      expect(result).toEqual(false);
    });
  });

  describe('addBookmark', () => {
    it('should save bookmark if it has not been saved', async () => {
      const bookmarkedEvents = ['123', '456'];
      const newEventId = '789';

      mockStorageService.get.mockResolvedValueOnce(
        JSON.stringify(bookmarkedEvents),
      );

      await service.addBookmark(newEventId);

      expect(mockStorageService.set).toHaveBeenCalledWith(
        EVENT_BOOKMARKED_KEY,
        JSON.stringify([...bookmarkedEvents, newEventId]),
      );
    });

    it('should not save bookmark if it is already saved', async () => {
      const bookmarkedEvents = ['123', '456'];
      const existingEventId = '123';

      mockStorageService.get.mockResolvedValue(
        JSON.stringify(bookmarkedEvents),
      );

      await service.addBookmark(existingEventId);

      expect(mockStorageService.set).not.toHaveBeenCalled();
    });
  });

  describe('removeBookmark', () => {
    it('should remove the event from bookmarks if it exists', async () => {
      const bookmarkedEvents = ['123', '456', '789'];
      const eventIdToRemove = '456';

      mockStorageService.get.mockResolvedValue(
        JSON.stringify(bookmarkedEvents),
      );

      await service.removeBookmark(eventIdToRemove);

      const updatedBookmarks = ['123', '789'];

      expect(mockStorageService.set).toHaveBeenCalledWith(
        EVENT_BOOKMARKED_KEY,
        JSON.stringify(updatedBookmarks),
      );
    });
  });
});
