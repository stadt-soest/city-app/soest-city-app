import { inject, Injectable } from '@angular/core';
import { endOfDay, startOfDay } from 'date-fns';
import {
  ErrorHandlingService,
  ErrorInfo,
  ErrorType,
  Feedable,
  ModuleInfo,
  StorageKeyType,
  StorageService,
} from '@sw-code/urbo-core';
import { EventAdapter } from '../event.adapter';
import {
  EVENT_BOOKMARKED_KEY,
  EVENTS_MODULE_INFO,
} from '../feature-events.module';
import { EventsStorageLoaderService } from './storage-loader/events-storage-loader.service';
import {
  catchError,
  from,
  map,
  Observable,
  of,
  Subject,
  switchMap,
  throwError,
} from 'rxjs';
import { Event, EventCategory } from '../models/event.model';
import { EventFeedItem } from '../models/event-feed-item.model';
import { SortOptions } from '../enums/sorting.enum';

@Injectable()
export class EventService implements Feedable {
  private readonly eventAdapter = inject(EventAdapter);
  private readonly errorHandlingService = inject(ErrorHandlingService);
  private readonly eventsModuleInfo = inject<ModuleInfo>(EVENTS_MODULE_INFO);
  private readonly storageLoaderService = inject(EventsStorageLoaderService);
  private readonly storageService = inject(StorageService);
  private readonly eventsModuleErrorSubject = new Subject<ErrorInfo>();

  getFeed(): Observable<EventFeedItem[]> {
    this.setModuleApiError(false);
    return from(this.getCategorySettings()).pipe(
      switchMap((selectedCategories: string[]) =>
        this.eventAdapter.getFeed(selectedCategories),
      ),
      catchError((error) => {
        console.error('Failed to fetch event feed', error);
        this.setModuleApiError(true);
        return of([]);
      }),
    );
  }

  getFeedablesUpcoming(
    page: number,
    eventCategories?: string[],
    startDate?: Date,
    endDate?: Date,
    excludedEvents: string[] = [],
    bookmarkedEventIds: string[] = [],
    size = 10,
  ): Observable<{ content: EventFeedItem[]; totalRecords: number }> {
    this.setModuleApiError(false);
    return from(this.getCategorySettings()).pipe(
      switchMap((selectedCategories: string[]) => {
        const categories = eventCategories ?? selectedCategories;
        const startDateIso = startDate
          ? startOfDay(startDate).toISOString()
          : new Date().toISOString();
        const endDateIso = endDate
          ? endOfDay(endDate).toISOString()
          : undefined;

        return this.eventAdapter.getFeedablesUpcoming(
          page,
          categories,
          bookmarkedEventIds,
          excludedEvents,
          startDateIso,
          endDateIso,
          size,
        );
      }),
      catchError((error) => {
        console.error('Failed to fetch upcoming feedables', error);
        this.setModuleApiError(true);
        return of({ content: [], totalRecords: 0 });
      }),
    );
  }

  getFeedablesUnfiltered(
    page: number,
  ): Observable<{ content: EventFeedItem[]; totalRecords: number }> {
    this.setModuleApiError(false);
    const startDateIso = new Date().toISOString();
    return this.eventAdapter.getFeedablesUnfiltered(page, startDateIso).pipe(
      catchError((error) => {
        console.error('Failed to fetch unfiltered feedables', error);
        this.setModuleApiError(true);
        return of({ content: [], totalRecords: 0 });
      }),
    );
  }

  getFeedablesGrouped(
    page: number,
    sortOption: SortOptions,
    excludedEvents: string[] = [],
  ): Observable<{ content: EventFeedItem[]; totalRecords: number }> {
    this.setModuleApiError(false);
    return from(this.getCategorySettings()).pipe(
      switchMap((selectedCategories: string[]) => {
        const startDateIso = new Date().toISOString();
        return this.eventAdapter.getFeedablesGrouped(
          page,
          sortOption,
          selectedCategories,
          excludedEvents,
          startDateIso,
        );
      }),
      catchError((error) => {
        console.error('Failed to fetch grouped feedables', error);
        this.setModuleApiError(true);
        return of({ content: [], totalRecords: 0 });
      }),
    );
  }

  getById(occurenceId: string): Observable<Event> {
    this.emitEventNotFoundError(
      occurenceId,
      ErrorType.DETAILS_API_ERROR,
      false,
    );
    return this.eventAdapter.getByOccurrenceId(occurenceId).pipe(
      catchError((error) => {
        console.error('Failed to fetch event details', error);
        this.emitEventNotFoundError(
          occurenceId,
          ErrorType.DETAILS_API_ERROR,
          true,
        );
        this.errorHandlingService.setErrorState(
          this.eventsModuleInfo,
          true,
          ErrorType.DETAILS_API_ERROR,
          occurenceId,
        );
        return throwError(() => error);
      }),
    );
  }

  getEventsModuleErrors(): Observable<ErrorInfo> {
    return this.eventsModuleErrorSubject.asObservable();
  }

  getBySearchTerm(
    searchTerm: string,
    page?: number,
    size?: number,
  ): Observable<{ total: number; items: EventFeedItem[] }> {
    const pageNumber = page ?? 1;
    const pageSize = size ?? 10;

    return this.eventAdapter
      .getBySearchTerm(searchTerm, pageNumber, pageSize)
      .pipe(
        catchError((error) => {
          console.error('Failed to search events', error);
          this.emitModuleError(ErrorType.MODULE_API_ERROR, true);
          this.errorHandlingService.setErrorState(
            this.eventsModuleInfo,
            true,
            ErrorType.MODULE_API_ERROR,
          );
          return of({ items: [], total: 0 });
        }),
      );
  }

  getCategorySettings(): Promise<string[]> {
    return this.storageLoaderService.loadEnabledSettingsKeys(
      StorageKeyType.CATEGORY,
    );
  }

  getBookmarkedEventIds(): Observable<string[]> {
    return from(this.storageService.get(EVENT_BOOKMARKED_KEY)).pipe(
      map((bookmarkedEvents) =>
        bookmarkedEvents ? JSON.parse(bookmarkedEvents) : [],
      ),
    );
  }

  getCategories(): Observable<EventCategory[]> {
    this.setModuleApiError(false);
    return this.eventAdapter.getCategories().pipe(
      catchError((error) => {
        console.error('Failed to fetch categories', error);
        this.setModuleApiError(true);
        return of([]);
      }),
    );
  }

  private emitModuleError(errorType: ErrorType, hasError: boolean) {
    this.eventsModuleErrorSubject.next({
      hasError,
      errorType,
    });
  }

  private emitEventNotFoundError(
    id: string,
    errorType: ErrorType,
    hasError: boolean,
  ) {
    this.eventsModuleErrorSubject.next({
      hasError,
      errorType,
      id,
    });
  }

  private setModuleApiError(hasError: boolean) {
    this.emitModuleError(ErrorType.MODULE_API_ERROR, hasError);
    this.errorHandlingService.setErrorState(
      this.eventsModuleInfo,
      hasError,
      ErrorType.MODULE_API_ERROR,
    );
  }
}
