import { TestBed } from '@angular/core/testing';
import { EventService } from './event.service';
import { EventAdapter } from '../event.adapter';
import { Event, EventCategory } from '../models/event.model';
import { EventFeedItem } from '../models/event-feed-item.model';
import { EVENTS_MODULE_INFO } from '../feature-events.module';
import { SortOptions } from '../enums/sorting.enum';
import {
  ErrorHandlingService,
  ErrorType,
  StorageService,
} from '@sw-code/urbo-core';
import { EventsStorageLoaderService } from './storage-loader/events-storage-loader.service';
import {
  createEvent,
  createEventFeedItem,
  createEventsModuleInfo,
} from '../shared/testing/events-test-utils';
import { firstValueFrom, of, throwError } from 'rxjs';

describe('EventService', () => {
  let service: EventService;
  let mockEventAdapter: jest.Mocked<EventAdapter>;
  let mockErrorHandlingService: jest.Mocked<ErrorHandlingService>;
  let mockEventsStorageLoaderService: jest.Mocked<EventsStorageLoaderService>;
  let mockStorageService: jest.Mocked<StorageService>;

  beforeEach(() => {
    mockEventAdapter = {
      getByOccurrenceId: jest.fn(),
      getFeed: jest.fn(),
      getFeedablesUpcoming: jest.fn(),
      getFeedablesUnfiltered: jest.fn(),
      getFeedablesGrouped: jest.fn(),
      getBySearchTerm: jest.fn(),
      getCategories: jest.fn(),
    };

    mockErrorHandlingService = {
      setErrorState: jest.fn(),
      hasErrorInSpecificModule: jest.fn(),
      hasErrorInDetails: jest.fn(),
    } as unknown as jest.Mocked<ErrorHandlingService>;

    mockEventsStorageLoaderService = {
      loadEnabledSettingsKeys: jest.fn(async (): Promise<string[]> => []),
      extractUniqueCategories: jest.fn(),
    } as unknown as jest.Mocked<EventsStorageLoaderService>;

    mockStorageService = {
      get: jest.fn(),
    } as unknown as jest.Mocked<StorageService>;

    TestBed.configureTestingModule({
      providers: [
        { provide: EventAdapter, useValue: mockEventAdapter },
        { provide: ErrorHandlingService, useValue: mockErrorHandlingService },
        {
          provide: EventsStorageLoaderService,
          useValue: mockEventsStorageLoaderService,
        },
        { provide: StorageService, useValue: mockStorageService },
        { provide: EVENTS_MODULE_INFO, useValue: createEventsModuleInfo },
        EventService,
      ],
    });

    service = TestBed.inject(EventService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getFeed', () => {
    it('should return feed items when EventAdapter.getFeed succeeds', async () => {
      const expectedFeedItems: EventFeedItem[] = [createEventFeedItem()];

      mockEventAdapter.getFeed.mockReturnValue(of(expectedFeedItems));
      mockEventsStorageLoaderService.loadEnabledSettingsKeys.mockResolvedValueOnce(
        ['Category1', 'Category2'],
      );

      const feedItems = await firstValueFrom(service.getFeed());

      expect(feedItems).toEqual(expectedFeedItems);
    });

    it('should handle error when EventAdapter.getFeed fails', async () => {
      mockEventAdapter.getFeed.mockReturnValue(
        throwError(() => new Error('Test Error')),
      );

      const feedItems = await firstValueFrom(service.getFeed());

      expect(feedItems).toEqual([]);
      expect(mockErrorHandlingService.setErrorState).toHaveBeenCalledWith(
        createEventsModuleInfo,
        true,
        ErrorType.MODULE_API_ERROR,
      );
    });
  });

  describe('getFeedablesUpcoming', () => {
    const expectedItems: EventFeedItem[] = [createEventFeedItem()];

    it('should return upcoming feedables when EventAdapter.getFeedablesUpcoming succeeds', async () => {
      const mockSelectedCategories = ['Category1 Id', 'Category5 Id'];
      mockEventAdapter.getFeedablesUpcoming.mockReturnValue(
        of({ content: expectedItems, totalRecords: 1 }),
      );
      mockEventsStorageLoaderService.loadEnabledSettingsKeys.mockResolvedValueOnce(
        mockSelectedCategories,
      );

      const result = await firstValueFrom(service.getFeedablesUpcoming(0));

      expect(result).toEqual({ content: expectedItems, totalRecords: 1 });
    });

    it('should handle error when EventAdapter.getFeedablesUpcoming fails', async () => {
      mockEventAdapter.getFeedablesUpcoming.mockReturnValue(
        throwError(() => new Error('Test Error')),
      );

      const result = await firstValueFrom(service.getFeedablesUpcoming(0));

      expect(result).toEqual({ content: [], totalRecords: 0 });
      expect(mockErrorHandlingService.setErrorState).toHaveBeenCalledWith(
        createEventsModuleInfo,
        true,
        ErrorType.MODULE_API_ERROR,
      );
    });
  });

  describe('getFeedablesGrouped', () => {
    const expectedItems: EventFeedItem[] = [createEventFeedItem()];

    it('should return grouped feedables when EventAdapter.getFeedablesGrouped succeeds', async () => {
      const mockSelectedCategories = ['Category1 Id', 'Category5 Id'];
      mockEventAdapter.getFeedablesGrouped.mockReturnValue(
        of({ content: expectedItems, totalRecords: 1 }),
      );
      mockEventsStorageLoaderService.loadEnabledSettingsKeys.mockResolvedValueOnce(
        mockSelectedCategories,
      );

      const result = await firstValueFrom(
        service.getFeedablesGrouped(0, SortOptions.startAtAsc),
      );

      expect(result).toEqual({ content: expectedItems, totalRecords: 1 });
    });

    it('should handle error when EventAdapter.getFeedablesGrouped fails', async () => {
      mockEventAdapter.getFeedablesGrouped.mockReturnValue(
        throwError(() => new Error('Test Error')),
      );

      const result = await firstValueFrom(
        service.getFeedablesGrouped(0, SortOptions.startAtAsc),
      );

      expect(result).toEqual({ content: [], totalRecords: 0 });
      expect(mockErrorHandlingService.setErrorState).toHaveBeenCalledWith(
        createEventsModuleInfo,
        true,
        ErrorType.MODULE_API_ERROR,
      );
    });
  });

  describe('getById', () => {
    it('should return event when EventAdapter.getByOccurrenceId succeeds', async () => {
      const eventId = 'default-id';
      const expectedEvent: Event = createEvent();
      expectedEvent.id = eventId;

      mockEventAdapter.getByOccurrenceId.mockReturnValue(of(expectedEvent));

      const event = await firstValueFrom(service.getById(eventId));

      expect(event).toEqual(expectedEvent);
    });

    it('should handle error when EventAdapter.getByOccurrenceId fails', async () => {
      const eventId = 'default-id';
      const testError = new Error('Test Error');

      mockEventAdapter.getByOccurrenceId.mockReturnValue(
        throwError(() => testError),
      );

      await expect(firstValueFrom(service.getById(eventId))).rejects.toThrow(
        testError,
      );

      expect(mockErrorHandlingService.setErrorState).toHaveBeenCalledWith(
        createEventsModuleInfo,
        true,
        ErrorType.DETAILS_API_ERROR,
        eventId,
      );
    });
  });

  describe('getBySearchTerm', () => {
    it('should return search results when EventAdapter.getBySearchTerm succeeds', async () => {
      const searchTerm = 'test';
      const page = 1;
      const size = 10;
      const expectedItems: EventFeedItem[] = [createEventFeedItem()];
      const expectedTotal = 1;

      mockEventAdapter.getBySearchTerm.mockReturnValue(
        of({ total: expectedTotal, items: expectedItems }),
      );

      const result = await firstValueFrom(
        service.getBySearchTerm(searchTerm, page, size),
      );

      expect(result).toEqual({ total: expectedTotal, items: expectedItems });
    });

    it('should handle error when EventAdapter.getBySearchTerm fails', async () => {
      const searchTerm = 'test';
      const page = 1;
      const size = 10;

      mockEventAdapter.getBySearchTerm.mockReturnValue(
        throwError(() => new Error('Test Error')),
      );

      const result = await firstValueFrom(
        service.getBySearchTerm(searchTerm, page, size),
      );

      expect(result).toEqual({ items: [], total: 0 });
      expect(mockErrorHandlingService.setErrorState).toHaveBeenCalledWith(
        createEventsModuleInfo,
        true,
        ErrorType.MODULE_API_ERROR,
      );
    });
  });

  describe('getCategories', () => {
    it('should return categories when EventAdapter.getCategories succeeds', async () => {
      const expectedCategories: EventCategory[] = [
        {
          id: 'cat1',
          sourceCategories: [],
          localizedNames: [],
        },
      ];

      mockEventAdapter.getCategories.mockReturnValue(of(expectedCategories));

      const categories = await firstValueFrom(service.getCategories());

      expect(categories).toEqual(expectedCategories);
    });

    it('should handle error when EventAdapter.getCategories fails', async () => {
      mockEventAdapter.getCategories.mockReturnValue(
        throwError(() => new Error('Test Error')),
      );

      const categories = await firstValueFrom(service.getCategories());

      expect(categories).toEqual([]);
      expect(mockErrorHandlingService.setErrorState).toHaveBeenCalledWith(
        createEventsModuleInfo,
        true,
        ErrorType.MODULE_API_ERROR,
      );
    });
  });

  describe('getBookmarkedEventIds', () => {
    it('should return bookmarked event IDs when storage contains data', async () => {
      const bookmarkedEvents = ['event1', 'event2'];
      mockStorageService.get.mockReturnValue(
        Promise.resolve(JSON.stringify(bookmarkedEvents)),
      );

      const result = await firstValueFrom(service.getBookmarkedEventIds());

      expect(result).toEqual(bookmarkedEvents);
    });

    it('should return empty array when storage is empty', async () => {
      mockStorageService.get.mockReturnValue(Promise.resolve(null));

      const result = await firstValueFrom(service.getBookmarkedEventIds());

      expect(result).toEqual([]);
    });
  });

  describe('getCategorySettings', () => {
    it('should return category settings from EventsStorageLoaderService', async () => {
      const mockCategories = ['Category1', 'Category2'];
      mockEventsStorageLoaderService.loadEnabledSettingsKeys.mockResolvedValueOnce(
        mockCategories,
      );

      const categories = await service.getCategorySettings();

      expect(categories).toEqual(mockCategories);
    });
  });

  describe('getFeedablesUnfiltered', () => {
    const expectedItems: EventFeedItem[] = [createEventFeedItem()];

    it('should return unfiltered feedables when EventAdapter.getFeedablesUnfiltered succeeds', async () => {
      mockEventAdapter.getFeedablesUnfiltered.mockReturnValue(
        of({ content: expectedItems, totalRecords: 1 }),
      );

      const result = await firstValueFrom(service.getFeedablesUnfiltered(0));

      expect(result).toEqual({ content: expectedItems, totalRecords: 1 });
    });

    it('should handle error when EventAdapter.getFeedablesUnfiltered fails', async () => {
      mockEventAdapter.getFeedablesUnfiltered.mockReturnValue(
        throwError(() => new Error('Test Error')),
      );

      const result = await firstValueFrom(service.getFeedablesUnfiltered(0));

      expect(result).toEqual({ content: [], totalRecords: 0 });
      expect(mockErrorHandlingService.setErrorState).toHaveBeenCalledWith(
        createEventsModuleInfo,
        true,
        ErrorType.MODULE_API_ERROR,
      );
    });
  });
});
