import { TestBed } from '@angular/core/testing';
import { EventsStorageLoaderService } from './events-storage-loader.service';
import { SettingsPersistenceService, StorageKeyType } from '@sw-code/urbo-core';
import { EVENTS_MODULE_INFO } from '../../feature-events.module';
import { createEventsModuleInfo } from '../../shared/testing/events-test-utils';
import { of } from 'rxjs';

describe('EventsStorageLoaderService', () => {
  let service: EventsStorageLoaderService;
  let mockSettingsPersistenceService: jest.Mocked<SettingsPersistenceService>;

  beforeEach(() => {
    mockSettingsPersistenceService = {
      saveSettings: jest.fn(),
      getSettings: jest.fn().mockResolvedValue({}),
      getEnabledSettingsKeys: jest.fn(),
      updateSettings: jest.fn(),
    } as unknown as jest.Mocked<SettingsPersistenceService>;

    TestBed.configureTestingModule({
      providers: [
        {
          provide: SettingsPersistenceService,
          useValue: mockSettingsPersistenceService,
        },
        { provide: EVENTS_MODULE_INFO, useValue: createEventsModuleInfo },
        EventsStorageLoaderService,
      ],
    });

    service = TestBed.inject(EventsStorageLoaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('initializeDefaultSettings should save unique categories when settings do not exist', async () => {
    const mockCategories = of([
      {
        id: 'Category1',
        name: 'Default Category 1',
        localizedNames: [{ value: 'Default Category 1', language: 'EN' }],
      },
      {
        id: 'Category2',
        name: 'Default Category 2',
        localizedNames: [{ value: 'Default Category 2', language: 'EN' }],
      },
      {
        id: 'Category3',
        name: 'Default Category 3',
        localizedNames: [{ value: 'Default Category 3', language: 'EN' }],
      },
    ]);

    await service.initializeDefaultSettings(mockCategories as any);

    expect(mockSettingsPersistenceService.updateSettings).toHaveBeenCalledWith(
      expect.anything(),
      ['Category1', 'Category2', 'Category3'],
      {},
      StorageKeyType.CATEGORY,
    );
  });

  it('loadSettings should return settings based on storageKeyType', async () => {
    const storageKeyType = StorageKeyType.SOURCE;
    const expectedSettings = ['Default Source 1', 'Default Source 2'];

    mockSettingsPersistenceService.getEnabledSettingsKeys.mockResolvedValue(
      expectedSettings,
    );

    const result = await service.loadEnabledSettingsKeys(storageKeyType);

    expect(result).toEqual(expectedSettings);
  });
});
