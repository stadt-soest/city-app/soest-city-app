import { inject, Injectable } from '@angular/core';
import {
  ModuleInfo,
  SettingsPersistenceService,
  StorageKeyType,
} from '@sw-code/urbo-core';
import { EVENTS_MODULE_INFO } from '../../feature-events.module';
import { firstValueFrom, Observable } from 'rxjs';
import { EventCategory } from '../../models/event.model';

@Injectable()
export class EventsStorageLoaderService {
  private readonly settingsPersistenceService = inject(
    SettingsPersistenceService,
  );
  private readonly eventsModuleInfo = inject<ModuleInfo>(EVENTS_MODULE_INFO);

  async initializeDefaultSettings(categories: Observable<EventCategory[]>) {
    const categoryIdsArray = await firstValueFrom(categories);

    await this.updateCategorySettings(categoryIdsArray);
  }

  loadEnabledSettingsKeys(storageKeyType: StorageKeyType): Promise<string[]> {
    return this.settingsPersistenceService.getEnabledSettingsKeys(
      this.eventsModuleInfo,
      storageKeyType,
    );
  }

  async getUserSettings(): Promise<{
    sources: string[];
    categoryIds: string[];
  }> {
    const sources = ['kalender-soest.de'];
    const categoryIds = await this.loadEnabledSettingsKeys(
      StorageKeyType.CATEGORY,
    );
    return { sources, categoryIds };
  }

  private async getSettings(
    keyType: StorageKeyType,
  ): Promise<{ [key: string]: boolean }> {
    const settings = await this.settingsPersistenceService.getSettings(
      this.eventsModuleInfo,
      keyType,
    );
    return typeof settings === 'string' ? JSON.parse(settings) : settings;
  }

  private async updateCategorySettings(
    categories: EventCategory[],
  ): Promise<void> {
    const existingCategorySettings =
      (await this.getSettings(StorageKeyType.CATEGORY)) || {};
    const uniqueCategoryIds = categories.map((category) => category.id);
    await this.settingsPersistenceService.updateSettings(
      this.eventsModuleInfo,
      uniqueCategoryIds,
      existingCategorySettings,
      StorageKeyType.CATEGORY,
    );
  }
}
