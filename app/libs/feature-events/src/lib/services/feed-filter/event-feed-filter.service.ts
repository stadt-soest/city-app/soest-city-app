import { Injectable } from '@angular/core';
import { addDays, endOfDay, startOfToday } from 'date-fns';
import { FeedFilterType, FilterPeriod } from '@sw-code/urbo-core';

@Injectable()
export class EventFeedFilterService {
  calculateDates(
    filter: FeedFilterType,
    customFromDate: Date,
    customToDate: Date,
  ): FilterPeriod {
    const today = startOfToday();

    switch (filter) {
      case FeedFilterType.all:
        return { startDate: undefined, endDate: undefined };
      case FeedFilterType.today:
        return { startDate: today, endDate: today };
      case FeedFilterType.tomorrow:
        return this.calculateTomorrow(today);
      case FeedFilterType.thisWeekend:
        return this.calculateThisWeekend(today);
      case FeedFilterType.selectDays:
        return this.calculateSelectDays(customFromDate, customToDate);
      default:
        return {};
    }
  }

  initializeSelectDays(): { customFromDate: Date; customToDate: Date } {
    const today = addDays(startOfToday(), 7);

    const todayNextWeek = addDays(startOfToday(), 14);
    const customFromDate = today;
    const customToDate = todayNextWeek;

    return { customFromDate, customToDate };
  }

  private calculateTomorrow(startDate: Date): FilterPeriod {
    startDate.setDate(startDate.getDate() + 1);
    const endDate = endOfDay(startDate);
    return { startDate, endDate };
  }

  private calculateThisWeekend(today: Date): FilterPeriod {
    let weekendStart: Date;
    let weekendEnd: Date;

    switch (today.getDay()) {
      case 5:
        weekendStart = today;
        weekendEnd = addDays(weekendStart, 2);
        break;
      case 6:
        weekendStart = today;
        weekendEnd = addDays(weekendStart, 1);
        break;
      case 0:
        weekendStart = today;
        weekendEnd = today;
        break;
      default: {
        const daysUntilFriday = 5 - today.getDay();
        weekendStart = addDays(today, daysUntilFriday);
        weekendEnd = addDays(weekendStart, 2);
        break;
      }
    }

    return { startDate: weekendStart, endDate: weekendEnd };
  }

  private calculateSelectDays(
    customFromDate: Date,
    customToDate: Date,
  ): FilterPeriod {
    return {
      startDate: customFromDate,
      endDate: customToDate,
    };
  }
}
