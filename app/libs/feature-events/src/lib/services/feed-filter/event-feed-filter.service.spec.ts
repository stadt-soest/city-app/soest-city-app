import { TestBed } from '@angular/core/testing';
import { addDays, startOfToday } from 'date-fns';
import { EventFeedFilterService } from './event-feed-filter.service';
import { FeedFilterType } from '@sw-code/urbo-core';

describe('EventFeedFilterService', () => {
  let service: EventFeedFilterService;

  beforeEach(() => {
    jest.useFakeTimers().setSystemTime(new Date('2024-01-01'));

    TestBed.configureTestingModule({
      providers: [EventFeedFilterService],
    });
    service = TestBed.inject(EventFeedFilterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('calculateDates', () => {
    it('should return empty object for FeedFilterType.All', () => {
      const result = service.calculateDates(
        FeedFilterType.all,
        new Date(),
        new Date(),
      );
      expect(result).toEqual({ startDate: undefined, endDate: undefined });
    });

    it("should return today's date for FeedFilterType.Today", () => {
      const result = service.calculateDates(
        FeedFilterType.today,
        new Date(),
        new Date(),
      );
      const today = startOfToday();
      expect(result.startDate).toEqual(today);
    });

    it("should return tomorrow's date for FeedFilterType.Tomorrow", () => {
      const result = service.calculateDates(
        FeedFilterType.tomorrow,
        new Date(),
        new Date(),
      );
      const tomorrow = startOfToday();
      tomorrow.setDate(tomorrow.getDate() + 1);
      expect(result.startDate).toEqual(tomorrow);
    });

    it("should return next weekend's date for FeedFilterType.ThisWeekend if current day is in the middle of the week", () => {
      const result = service.calculateDates(
        FeedFilterType.thisWeekend,
        new Date(),
        new Date(),
      );
      expect(result.startDate).toEqual(new Date('2024-01-05'));
      expect(result.endDate).toEqual(new Date('2024-01-07'));
    });

    it("should return current weekend's date for FeedFilterType.ThisWeekend if current day is Friday", () => {
      jest.useFakeTimers().setSystemTime(new Date('2024-01-05'));
      const result = service.calculateDates(
        FeedFilterType.thisWeekend,
        new Date(),
        new Date(),
      );
      expect(result.startDate).toEqual(new Date('2024-01-05'));
      expect(result.endDate).toEqual(new Date('2024-01-07'));
    });

    it("should return current weekend's date for FeedFilterType.ThisWeekend if current day is Sunday", () => {
      jest.useFakeTimers().setSystemTime(new Date('2024-01-07'));
      const result = service.calculateDates(
        FeedFilterType.thisWeekend,
        new Date(),
        new Date(),
      );
      expect(result.startDate).toEqual(new Date('2024-01-07'));
      expect(result.endDate).toEqual(new Date('2024-01-07'));
    });

    it('should handle custom date range for FeedFilterType.SelectDays', () => {
      const customStartDate = new Date('2023-03-10');
      const customEndDate = new Date('2023-03-15');
      const result = service.calculateDates(
        FeedFilterType.selectDays,
        customStartDate,
        customEndDate,
      );
      expect(result.startDate).toEqual(new Date(customStartDate));
      expect(result.endDate).toEqual(new Date(customEndDate));
    });
  });

  describe('initializeSelectDays', () => {
    it('should set customFromDate to today + 7 days and customToDate to today + 14 days', () => {
      const { customFromDate, customToDate } = service.initializeSelectDays();

      const today = startOfToday();
      const expectedFromDate = addDays(today, 7);
      const expectedToDate = addDays(today, 14);

      expect(customFromDate).toEqual(expectedFromDate);
      expect(customToDate).toEqual(expectedToDate);
    });
  });
});
