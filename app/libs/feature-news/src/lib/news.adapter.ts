import { Observable } from 'rxjs';
import { NewsFeedItem } from './models/NewsFeedItem';
import { News } from './models/news.model';
import { NewsCategory } from './interface/news-category.interface';

export abstract class NewsAdapter {
  abstract getFeed(): Observable<NewsFeedItem[]>;

  abstract getSources(): Observable<string[]>;

  abstract getPaginatedFeed(
    page: number,
  ): Observable<{ content: NewsFeedItem[]; totalRecords: number }>;

  abstract getHighlightedPaginatedFeed(
    page: number,
  ): Observable<{ content: NewsFeedItem[]; totalRecords: number }>;

  abstract getPaginatedFilteredFeed(
    page: number,
    newsCategoryIds: string[],
    newsSources: string[],
    excludedNews: string[],
  ): Observable<{ content: NewsFeedItem[]; totalRecords: number }>;

  abstract getById(id: string): Observable<News>;

  abstract getCategories(): Observable<NewsCategory[]>;

  abstract getBySearchTerm(
    searchTerm: string,
    pageNumber: number,
    pageSize: number,
  ): Observable<{ total: number; items: NewsFeedItem[] }>;
}
