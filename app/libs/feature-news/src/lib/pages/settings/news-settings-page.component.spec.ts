import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NewsSettingsPageComponent } from './news-settings-page.component';
import { CoreTestModule, StorageService } from '@sw-code/urbo-core';
import { NewsService } from '../../services/news.service';
import { StorageLoaderService } from '../../services/storage-loader/storage-loader.service';
import { of } from 'rxjs';
import { CondensedHeaderLayoutComponent, UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../transloco-testing.module';
import { NEWS_MODULE_INFO } from '../../feature-news.module';
import { By } from '@angular/platform-browser';

describe('SettingsMenuPage', () => {
  let component: NewsSettingsPageComponent;
  let fixture: ComponentFixture<NewsSettingsPageComponent>;
  let mockStorageService: jest.Mocked<StorageService>;
  let mockNewsService: jest.Mocked<NewsService>;
  let mockStorageLoaderService: jest.Mocked<StorageLoaderService>;

  beforeEach(waitForAsync(() => {
    mockStorageService = {
      get: jest.fn(),
      getKeys: jest.fn(),
      set: jest.fn(),
      generateStorageKey: jest.fn(),
      remove: jest.fn(),
    } as unknown as jest.Mocked<StorageService>;

    mockNewsService = {
      getFeed: jest.fn().mockReturnValue(of([])),
      getCategories: jest.fn().mockReturnValue(of([])),
    } as unknown as jest.Mocked<NewsService>;

    mockStorageLoaderService = {
      getUniqueSources: jest.fn().mockResolvedValue([]),
    } as unknown as jest.Mocked<StorageLoaderService>;

    TestBed.configureTestingModule({
      imports: [
        NewsSettingsPageComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        {
          provide: NEWS_MODULE_INFO,
          useValue: {
            id: '',
            defaultVisibility: false,
            name: 'Mock News Module',
            icon: 'mock-icon',
            baseRoutingPath: '/mock-news',
            category: 'mock-category',
            settingsPageAvailable: true,
            isAboutAppItself: false,
          },
        },
        { provide: StorageService, useValue: mockStorageService },
        { provide: NewsService, useValue: mockNewsService },
        { provide: StorageLoaderService, useValue: mockStorageLoaderService },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(NewsSettingsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('HTML Rendering', () => {
    it('should pass the correct title to app-custom-toolbar', () => {
      const customToolbarComponent = fixture.debugElement.query(
        By.directive(CondensedHeaderLayoutComponent),
      ).componentInstance;
      fixture.detectChanges();
      expect(customToolbarComponent.pageTitle).toEqual('News Settings');
    });
  });
});
