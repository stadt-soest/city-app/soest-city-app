import { Component } from '@angular/core';
import { NewsSettingsMenuComponent } from '../../components/news-settings-menu/news-settings-menu.component';
import { TranslocoDirective } from '@jsverse/transloco';
import { CondensedHeaderLayoutComponent } from '@sw-code/urbo-ui';
import { ContentData } from '../../models/settings-content.model';

@Component({
  selector: 'lib-news-settings-page',
  templateUrl: './news-settings-page.component.html',
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    NewsSettingsMenuComponent,
  ],
})
export class NewsSettingsPageComponent {
  content: ContentData[] = [];
  isScrolled = false;

  onContentScroll(event: any) {
    const threshold = 40;
    this.isScrolled = event.detail.scrollTop > threshold;
  }
}
