import { Component, DestroyRef, inject, OnInit } from '@angular/core';
import { firstValueFrom, take } from 'rxjs';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  CondensedHeaderLayoutComponent,
  ConnectivityErrorCardComponent,
  NewContentTextComponent,
  NoInterestsSelectedCardComponent,
  RefresherComponent,
  SkeletonFeedCardComponent,
  UpdateNotifierComponent,
} from '@sw-code/urbo-ui';
import {
  IonCol,
  IonGrid,
  IonInfiniteScroll,
  IonInfiniteScrollContent,
  IonRow,
} from '@ionic/angular/standalone';
import { NgComponentOutlet, NgFor, NgIf } from '@angular/common';
import { NewsFeedItem } from '../models/NewsFeedItem';
import { NewsService } from '../services/news.service';
import {
  ConnectivityService,
  ErrorType,
  FeedUpdateService,
} from '@sw-code/urbo-core';
import { ActivatedRoute, Router } from '@angular/router';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'lib-news',
  templateUrl: './news-page.component.html',
  styleUrls: ['./news-page.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    RefresherComponent,
    IonGrid,
    NgIf,
    IonCol,
    NoInterestsSelectedCardComponent,
    ConnectivityErrorCardComponent,
    UpdateNotifierComponent,
    IonRow,
    NgFor,
    NgComponentOutlet,
    SkeletonFeedCardComponent,
    NewContentTextComponent,
    IonInfiniteScroll,
    IonInfiniteScrollContent,
  ],
})
export class NewsPageComponent implements OnInit {
  displayConnectivityError = false;
  displayNoInterestsSelectedError = false;
  feedItems: NewsFeedItem[] = [];
  currentPageIndex = 0;
  contentLoadingComplete = false;
  moreContentAvailable = true;
  private readonly newsService = inject(NewsService);
  private readonly feedUpdateService = inject(FeedUpdateService);
  private readonly router = inject(Router);
  private readonly route = inject(ActivatedRoute);
  private readonly connectivityService = inject(ConnectivityService);
  private readonly destroyRef = inject(DestroyRef);

  ngOnInit(): void {
    this.subscribeToModuleError();
    void this.subscribeToFeedItems();
    this.setupFeedUpdateSubscription();
  }

  loadMoreFeedItems(event: any) {
    this.contentLoadingComplete = false;
    this.currentPageIndex++;
    this.newsService
      .getPaginatedFilteredFeed(this.currentPageIndex)
      .pipe(take(1))
      .subscribe((filteredFeed) => {
        this.moreContentAvailable = filteredFeed.content.length > 0;
        this.feedItems.push(...filteredFeed.content);
        this.contentLoadingComplete = true;
        event.target.complete();
      });
  }

  async doRefresh(event: any): Promise<void> {
    this.resetDataAndStates();
    await this.subscribeToFeedItems();
    event.target.complete();
  }

  async navigateToSettings() {
    await this.router.navigate(['settings'], { relativeTo: this.route });
  }

  private resetDataAndStates(): void {
    this.currentPageIndex = 0;
    this.feedItems = [];
  }

  private setupFeedUpdateSubscription(): void {
    this.feedUpdateService
      .getFeedUpdatedListener()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(() => {
        void this.subscribeToFeedItems();
      });
  }

  private subscribeToModuleError(): void {
    this.newsService
      .getNewsModuleErrors()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((errorInfo) => {
        if (errorInfo.errorType === ErrorType.MODULE_API_ERROR) {
          this.displayConnectivityError = errorInfo.hasError;
        } else {
          this.displayConnectivityError = false;
        }
      });
  }

  private async subscribeToFeedItems(): Promise<void> {
    try {
      this.contentLoadingComplete = false;

      const isConnected = await this.connectivityService.checkOnlineStatus();
      if (!isConnected) {
        this.displayConnectivityError = true;
        this.contentLoadingComplete = true;
        return;
      }

      const hasSettings = await this.checkSourcesAndCategories();

      if (!hasSettings) {
        this.updateStateForEmptySourcesAndCategories();
      } else {
        const feedItems = await this.fetchFeedItems();
        this.updateStateWithFeedItems(feedItems);
      }
    } catch (error) {
      console.error('Failed to load news feed items:', error);
      this.displayConnectivityError = true;
      this.contentLoadingComplete = true;
    }
  }

  private async checkSourcesAndCategories(): Promise<boolean> {
    const [sources, categories] = await Promise.all([
      this.newsService.getSourceSettings(),
      this.newsService.getCategorySettings(),
    ]);
    return sources.length > 0 && categories.length > 0;
  }

  private async fetchFeedItems(): Promise<NewsFeedItem[]> {
    const feedItems = await firstValueFrom(
      this.newsService
        .getPaginatedFilteredFeed(this.currentPageIndex)
        .pipe(take(1)),
    );
    return feedItems.content;
  }

  private updateStateForEmptySourcesAndCategories(): void {
    this.displayNoInterestsSelectedError = true;
    this.feedItems = [];
    this.contentLoadingComplete = true;
  }

  private updateStateWithFeedItems(feedItems: NewsFeedItem[]): void {
    this.feedItems = feedItems;
    this.moreContentAvailable = feedItems.length > 0;
    this.displayNoInterestsSelectedError = feedItems.length <= 0;
    this.contentLoadingComplete = true;
  }
}
