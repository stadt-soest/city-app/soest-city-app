import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NewsPageComponent } from './news-page.component';
import { MenuController } from '@ionic/angular';
import { StorageLoaderService } from '../services/storage-loader/storage-loader.service';
import {
  ConnectivityService,
  CoreTestModule,
  ErrorType,
  UpdateService,
} from '@sw-code/urbo-core';
import { BehaviorSubject, of, Subject } from 'rxjs';
import { NewsService } from '../services/news.service';
import {
  createNewsFeedItem,
  createNewsModuleInfo,
} from '../shared/testing/news.test-utils';
import { NEWS_MODULE_INFO } from '../feature-news.module';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../transloco-testing.module';

describe('NewsPage', () => {
  let component: NewsPageComponent;
  let mockMenuController: jest.Mocked<MenuController>;
  let mockStorageLoaderService: jest.Mocked<StorageLoaderService>;
  let mockConnectivityService: jest.Mocked<ConnectivityService>;
  let mockUpdateService: Partial<UpdateService>;
  let mockUpdateAvailableSubject: BehaviorSubject<boolean>;
  let updateAvailableSubject: Subject<boolean>;
  let fixture: ComponentFixture<NewsPageComponent>;
  let mockNewsService: jest.Mocked<NewsService>;

  beforeEach(() => {
    mockMenuController = {
      open: jest.fn(),
      close: jest.fn(),
    } as unknown as jest.Mocked<MenuController>;

    mockStorageLoaderService = {
      loadSettings: jest.fn(),
    } as unknown as jest.Mocked<StorageLoaderService>;

    mockConnectivityService = {
      checkOnlineStatus: jest.fn().mockReturnValue(Promise.resolve(true)),
    } as unknown as jest.Mocked<ConnectivityService>;

    mockNewsService = {
      getPaginatedFilteredFeed: jest.fn().mockReturnValue(
        of({
          content: [
            createNewsFeedItem({ id: '1', title: 'Item 1' }),
            createNewsFeedItem({ id: '2', title: 'Item 2' }),
          ],
          totalRecords: 2,
        }),
      ),
      getNewsModuleErrors: jest
        .fn()
        .mockReturnValue(
          of({ hasError: true, errorType: ErrorType.MODULE_API_ERROR }),
        ),
      getSourceSettings: jest
        .fn()
        .mockReturnValue(Promise.resolve(['source1', 'Source 1'])),
      getCategorySettings: jest
        .fn()
        .mockReturnValue(Promise.resolve(['category1', 'Category 1'])),
    } as unknown as jest.Mocked<NewsService>;

    mockUpdateAvailableSubject = new BehaviorSubject<boolean>(false);
    updateAvailableSubject = new Subject<boolean>();

    mockUpdateService = {
      updateAvailable$: mockUpdateAvailableSubject.asObservable(),
    };

    TestBed.configureTestingModule({
      providers: [
        { provide: NewsService, useValue: mockNewsService },
        { provide: MenuController, useValue: mockMenuController },
        { provide: StorageLoaderService, useValue: mockStorageLoaderService },
        { provide: UpdateService, useValue: mockUpdateService },
        { provide: ConnectivityService, useValue: mockConnectivityService },
        {
          provide: NEWS_MODULE_INFO,
          useFactory: () => createNewsModuleInfo,
        },
      ],
      imports: [
        NewsPageComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
    });

    fixture = TestBed.createComponent(NewsPageComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should set feedItems when SourceSettings and CategorySettings are available', async () => {
      const expectedFeedItems = [
        createNewsFeedItem({ id: '1', title: 'Item 1' }),
        createNewsFeedItem({ id: '2', title: 'Item 2' }),
      ];

      component.ngOnInit();
      await fixture.whenStable();

      expect(component.feedItems.length).toBe(expectedFeedItems.length);
      expect(component.feedItems).toEqual(
        expect.arrayContaining(expectedFeedItems),
      );
    });

    it('should set displayConnectivityError to true when ngOnit is called and no feed items are returned', () => {
      mockNewsService.getPaginatedFilteredFeed.mockReturnValue(
        of({
          content: [],
          totalRecords: 0,
        }),
      );
      component.ngOnInit();
      expect(component.displayConnectivityError).toBe(true);
    });

    it('should display connectivity error when there is no internet connection', async () => {
      mockConnectivityService.checkOnlineStatus.mockResolvedValue(false);

      component.ngOnInit();
      await fixture.whenStable();

      expect(component.displayConnectivityError).toBe(true);
    });
  });

  describe('loadMoreFeedItems', () => {
    it('should update feed items on call', () => {
      component.feedItems.pop();
      component.currentPageIndex = 0;
      component.loadMoreFeedItems({ target: { complete: jest.fn() } });
      expect(component.feedItems.length).toBe(2);
      expect(component.feedItems[0].id).toBe('1');
    });

    it(' should call complete on event at the end of loadMoreFeedItems', () => {
      const complete = jest.fn();
      mockNewsService.getPaginatedFilteredFeed.mockReturnValue(
        of({
          content: [
            createNewsFeedItem({ id: '1' }),
            createNewsFeedItem({ id: '2' }),
            createNewsFeedItem({ id: '3' }),
          ],
          totalRecords: 3,
        }),
      );
      component.currentPageIndex = 0;
      component.loadMoreFeedItems({ target: { complete } });
      expect(complete).toHaveBeenCalled();
    });

    it('should increment paginationPage on call', () => {
      mockNewsService.getPaginatedFilteredFeed.mockReturnValue(
        of({
          content: [
            createNewsFeedItem({ id: '1' }),
            createNewsFeedItem({ id: '2' }),
            createNewsFeedItem({ id: '3' }),
          ],
          totalRecords: 3,
        }),
      );
      component.currentPageIndex = 0;
      component.loadMoreFeedItems({ target: { complete: jest.fn() } });
      expect(component.currentPageIndex).toBe(1);
    });
  });
});
