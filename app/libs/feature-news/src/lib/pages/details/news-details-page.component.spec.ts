import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { NewsDetailsPageComponent } from './news-details-page.component';
import { NewsService } from '../../services/news.service';
import { CoreTestModule, ErrorType, ImageService } from '@sw-code/urbo-core';
import { NewsShareService } from '../../services/share/news-share.service';
import {
  createNewsFeedItem,
  createNewsModuleInfo,
} from '../../shared/testing/news.test-utils';
import { NewsFeedCardComponent } from '../../components/news-feed-card/news-feed-card.component';
import { NewsForYouCardComponent } from '../../components/news-for-you-card/news-for-you-card.component';
import { NEWS_MODULE_INFO } from '../../feature-news.module';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../transloco-testing.module';

describe('DetailsPage', () => {
  let component: NewsDetailsPageComponent;
  let fixture: ComponentFixture<NewsDetailsPageComponent>;
  let mockNewsService: jest.Mocked<NewsService>;
  let mockImageService: jest.Mocked<ImageService>;
  let mockActivatedRoute: any;
  let mockViewContainerRef: any;
  let mockShareService: jest.Mocked<NewsShareService>;

  const mockNewsId = '12345';
  const mockImageUrl = 'https://example.com/image.jpg';
  const mockNewsItem = {
    title: 'Test News',
    date: new Date().toISOString(),
    text: 'Test News Text',
  };
  const mockFeedItems = [
    createNewsFeedItem({
      id: '1',
      moduleName: 'news',
      baseRoutePath: '/news',
      icon: 'newspaper',
      title: 'News 1',
      image: null,
      creationDate: new Date(),
      cardComponent: NewsFeedCardComponent,
      forYouComponent: NewsForYouCardComponent,
    }),
    createNewsFeedItem({
      id: '2',
      moduleName: 'news',
      baseRoutePath: '/news',
      icon: 'newspaper',
      title: 'News 2',
      image: null,
      creationDate: new Date(),
      cardComponent: NewsFeedCardComponent,
      forYouComponent: NewsForYouCardComponent,
    }),
  ];

  beforeEach(async () => {
    const mockInstances = [];
    mockViewContainerRef = {
      createComponent: jest.fn(() => {
        const mockInstance = { feedItem: null };
        mockInstances.push(mockInstance);
        return { instance: mockInstance };
      }),
      clear: jest.fn(),
    };

    mockShareService = {
      shareNews: jest.fn().mockResolvedValue(undefined),
    } as unknown as jest.Mocked<NewsShareService>;

    mockNewsService = {
      getById: jest.fn().mockReturnValue(of(mockNewsItem)),
      getFeed: jest.fn().mockReturnValue(of(mockFeedItems)),
      getNewsModuleErrors: jest.fn().mockReturnValue(of()),
      getPaginatedFilteredFeed: jest
        .fn()
        .mockReturnValue(of({ content: [], totalRecords: 0 })),
    } as unknown as jest.Mocked<NewsService>;

    mockImageService = {
      getImageUrl: jest.fn().mockReturnValue(mockImageUrl),
      getHighResImageUrl: jest.fn().mockReturnValue(mockImageUrl),
    } as unknown as jest.Mocked<ImageService>;

    mockActivatedRoute = {
      snapshot: {
        paramMap: {
          get: jest.fn().mockReturnValue(mockNewsId),
        },
      },
    };

    await TestBed.configureTestingModule({
      providers: [
        { provide: NewsService, useValue: mockNewsService },
        { provide: ImageService, useValue: mockImageService },
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        { provide: NewsShareService, useValue: mockShareService },
        {
          provide: NEWS_MODULE_INFO,
          useValue: createNewsModuleInfo,
        },
      ],
      imports: [
        NewsDetailsPageComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
    }).compileComponents();

    mockNewsService.getFeed.mockReturnValue(of(mockFeedItems));

    fixture = TestBed.createComponent(NewsDetailsPageComponent);
    component = fixture.componentInstance;
    component.containers = [mockViewContainerRef, mockViewContainerRef];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should set news and imageUrl when newsId is present', () => {
      component.ngOnInit();
      expect(mockNewsService.getById).toHaveBeenCalledWith(mockNewsId);
      expect(component.imageUrl).toBe(mockImageUrl);
    });

    it('it should set hasFeedItems$ and  hasFeedItems to false when ngOnit is called and no feed items are returned', () => {
      mockNewsService.getPaginatedFilteredFeed.mockReturnValue(
        of({
          content: [],
          totalRecords: 0,
        }),
      );
      component.ngOnInit();
      expect(component.hasFeedItems$.getValue()).toBe(false);
      expect(component.hasFeedItems).toBe(false);
    });

    it('it should set hasFeedItems$ and  hasFeedItems to true when ngOnit is called and feed items are returned', () => {
      mockNewsService.getPaginatedFilteredFeed.mockReturnValue(
        of({
          content: [createNewsFeedItem({ id: '1' })],
          totalRecords: 1,
        }),
      );
      component.ngOnInit();
      expect(component.hasFeedItems$.getValue()).toBe(true);
      expect(component.hasFeedItems).toBe(true);
    });

    it('should set errorModule to true when Module API has error', () => {
      mockNewsService.getNewsModuleErrors.mockReturnValue(
        of({
          hasError: true,
          errorType: ErrorType.MODULE_API_ERROR,
        }),
      );
      component.ngOnInit();
      expect(component.errorModule).toBe(true);
    });

    it('should set errorModule to false when Module API has no error', () => {
      mockNewsService.getNewsModuleErrors.mockReturnValue(
        of({
          hasError: false,
          errorType: ErrorType.MODULE_API_ERROR,
        }),
      );
      component.ngOnInit();
      expect(component.errorModule).toBe(false);
    });

    it('should set errorDetails to true when Details API has error', () => {
      component.newsId = '1';
      mockNewsService.getNewsModuleErrors.mockReturnValue(
        of({
          hasError: true,
          errorType: ErrorType.DETAILS_API_ERROR,
          id: '1',
        }),
      );
      component.ngOnInit();
      expect(component.newsNotFound).toBe(true);
    });

    it('should set errorDetails to false when Details API has no error', () => {
      component.newsId = '1';
      mockNewsService.getNewsModuleErrors.mockReturnValue(
        of({
          hasError: false,
          errorType: ErrorType.DETAILS_API_ERROR,
          id: '1',
        }),
      );
      component.ngOnInit();
      expect(component.newsNotFound).toBe(false);
    });
  });

  describe('loadMoreFeedItems', () => {
    it('it should update feed items on call', () => {
      mockNewsService.getPaginatedFilteredFeed.mockReturnValue(
        of({
          content: [
            createNewsFeedItem({ id: '1' }),
            createNewsFeedItem({ id: '2' }),
            createNewsFeedItem({ id: '3' }),
          ],
          totalRecords: 3,
        }),
      );
      component.currentPageIndex = 0;
      component.loadMoreFeedItems({ target: { complete: jest.fn() } });
      expect(component.feedItems.length).toBe(3);
      expect(component.feedItems[0].id).toBe('1');
    });

    it('it should call complete on event at the end of loadMoreFeedItems', () => {
      const complete = jest.fn();
      mockNewsService.getPaginatedFilteredFeed.mockReturnValue(
        of({
          content: [
            createNewsFeedItem({ id: '1' }),
            createNewsFeedItem({ id: '2' }),
            createNewsFeedItem({ id: '3' }),
          ],
          totalRecords: 3,
        }),
      );
      component.currentPageIndex = 0;
      component.loadMoreFeedItems({ target: { complete } });
      expect(complete).toHaveBeenCalled();
    });

    it('it should increment paginationPage on call', () => {
      mockNewsService.getPaginatedFilteredFeed.mockReturnValue(
        of({
          content: [
            createNewsFeedItem({ id: '1' }),
            createNewsFeedItem({ id: '2' }),
            createNewsFeedItem({ id: '3' }),
          ],
          totalRecords: 3,
        }),
      );
      component.currentPageIndex = 0;
      component.loadMoreFeedItems({ target: { complete: jest.fn() } });
      expect(component.currentPageIndex).toBe(1);
    });
  });

  describe('shareNews', () => {
    it('should call shareEvent with the correct parameters', async () => {
      await component.shareNews();

      expect(mockShareService.shareNews).toHaveBeenCalledWith(mockNewsItem);
    });
  });

  describe('HTML Rendering', () => {
    it('should render lib-news-details-skeleton when detailsLoadingComplete is true', () => {
      component.detailsLoadingComplete = true;
      fixture.detectChanges();
      const skeleton = fixture.nativeElement.querySelector(
        'lib-news-details-skeleton',
      );
      expect(skeleton).toBeTruthy();
    });
    it('should render ion-grid with news when detailsLoadingComplete is false', () => {
      component.detailsLoadingComplete = false;
      fixture.detectChanges();
      const grid = fixture.nativeElement.querySelector('ion-grid');
      expect(grid).toBeTruthy();
    });
  });
});
