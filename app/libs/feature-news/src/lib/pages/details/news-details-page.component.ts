import {
  Component,
  DestroyRef,
  inject,
  OnInit,
  ViewChildren,
  ViewContainerRef,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  ActionButtonBarComponent,
  CondensedHeaderLayoutComponent,
  ConnectivityErrorCardComponent,
  DetailsNotFoundErrorCardComponent,
  LightboxDirective,
  LocalizedRelativeTimePipePipe,
  NewContentTextComponent,
  SafeExternalLinksDirective,
  SkeletonFeedCardComponent,
  StripHtmlPipe,
} from '@sw-code/urbo-ui';
import { NgComponentOutlet, NgFor, NgIf } from '@angular/common';
import { NewsDetailsSkeletonComponent } from '../../components/news-details-skeleton/news-details-skeleton.component';
import {
  IonCol,
  IonGrid,
  IonInfiniteScroll,
  IonInfiniteScrollContent,
  IonLabel,
  IonRow,
} from '@ionic/angular/standalone';
import { NewsFeedItem } from '../../models/NewsFeedItem';
import { News } from '../../models/news.model';
import { BehaviorSubject, take } from 'rxjs';
import {
  BrowserService,
  ErrorType,
  ImageService,
  ModuleInfo,
  TitleService,
} from '@sw-code/urbo-core';
import { NewsService } from '../../services/news.service';
import { NewsShareService } from '../../services/share/news-share.service';
import { NEWS_MODULE_INFO } from '../../feature-news.module';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'lib-details',
  templateUrl: './news-details-page.component.html',
  styleUrls: ['./news-details-page.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    NgIf,
    NewsDetailsSkeletonComponent,
    IonGrid,
    IonCol,
    IonLabel,
    LightboxDirective,
    ActionButtonBarComponent,
    ConnectivityErrorCardComponent,
    DetailsNotFoundErrorCardComponent,
    IonRow,
    NgFor,
    NgComponentOutlet,
    SkeletonFeedCardComponent,
    NewContentTextComponent,
    IonInfiniteScroll,
    IonInfiniteScrollContent,
    LocalizedRelativeTimePipePipe,
    StripHtmlPipe,
  ],
})
export class NewsDetailsPageComponent implements OnInit {
  @ViewChildren('newsCard', { read: ViewContainerRef })
  containers!: ViewContainerRef[];
  clickedNewsItemIds: string[] = [];
  feedItems: NewsFeedItem[] = [];
  currentPageIndex = 0;
  newsId: string | null = null;
  news?: News;
  errorModule = false;
  newsNotFound = false;
  imageUrl: string | null = null;
  isScrolled = false;
  hasFeedItems$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  hasFeedItems = true;
  moduleInfo!: ModuleInfo;
  feedLoadingComplete = false;
  moreContentAvailable = true;
  detailsLoadingComplete = true;
  excludedNews: string[] = [];
  private readonly route = inject(ActivatedRoute);
  private readonly newsService = inject(NewsService);
  private readonly imageService = inject(ImageService);
  private readonly browserService = inject(BrowserService);
  private readonly shareService = inject(NewsShareService);
  private readonly newsModuleInfo = inject<ModuleInfo>(NEWS_MODULE_INFO);
  private readonly destroyRef = inject(DestroyRef);
  private readonly titleService = inject(TitleService);
  private readonly router = inject(Router);

  get pageTitleKey() {
    if (this.errorModule) {
      return 'ui.error-page-title.no_signal';
    } else if (this.newsNotFound) {
      return 'ui.error-page-title.not_found';
    } else {
      return '';
    }
  }

  ngOnInit(): void {
    this.subscribeToErrors();
    this.newsId = this.route.snapshot.paramMap.get('id');
    this.moduleInfo = this.newsModuleInfo;
    this.setExcludedNews();
    this.subscribeToFeedItem();
  }

  onNewsItemClick(newsItemId: string): void {
    if (!this.clickedNewsItemIds.includes(newsItemId)) {
      this.clickedNewsItemIds.push(newsItemId);
    }
  }

  loadMoreFeedItems(event: any) {
    if (!this.news) {
      return;
    }

    this.feedLoadingComplete = false;
    this.currentPageIndex++;
    this.newsService
      .getPaginatedFilteredFeed(
        this.currentPageIndex,
        this.newsNotFound ? [] : this.news.categoryIds,
        [],
        this.excludedNews,
      )
      .pipe(take(1))
      .subscribe((feedItems) => {
        this.moreContentAvailable = feedItems.content.length > 0;
        this.feedItems.push(...feedItems.content);
        this.feedLoadingComplete = true;
        event.target.complete();
      });
  }

  async shareNews() {
    if (this.news) {
      await this.shareService.shareNews(this.news);
    }
  }

  hasNewsUrl() {
    return this.news?.sourceUrl !== null;
  }

  async openNewsUrl() {
    if (this.news) {
      const url = this.news?.sourceUrl;
      if (url === null) {
        console.error(
          'this news article has no source link, but you are trying to open the source link',
        );
      } else {
        await this.browserService.openInAppBrowser(url);
      }
    }
  }

  private subscribeToRelatedFeedItems(): void {
    this.newsService
      .getPaginatedFilteredFeed(
        this.currentPageIndex,
        this.newsNotFound ? [] : this.news?.categoryIds,
        [],
        this.excludedNews,
      )
      .pipe(take(1))
      .subscribe((feedItems) => {
        this.feedItems = feedItems.content;
        this.moreContentAvailable = this.feedItems.length > 0;
        this.feedLoadingComplete = true;
        this.checkHasContent();
      });
  }

  private subscribeToFeedItem(): void {
    if (this.newsId) {
      this.newsService
        .getById(this.newsId)
        .pipe(take(1))
        .subscribe({
          next: (news) => {
            this.news = news;
            this.updateTitle();
            this.excludedNews.push(this.news!.id);
            this.imageUrl = this.imageService.getImageUrl(this.news!.image);
            this.handleNewsLoadComplete();
          },
          error: () => {
            this.handleNewsLoadComplete();
          },
        });
    }
  }

  private handleNewsLoadComplete(): void {
    this.detailsLoadingComplete = false;
    this.subscribeToRelatedFeedItems();
  }

  private checkHasContent(): void {
    if (this.feedItems.length === 0) {
      this.hasFeedItems = false;
      this.hasFeedItems$.next(false);
    } else {
      this.hasFeedItems = true;
      this.hasFeedItems$.next(true);
    }
  }

  private updateTitle(): void {
    if (this.news) {
      this.titleService.setTitle(this.news.title);
    }
  }

  private subscribeToErrors(): void {
    this.newsService
      .getNewsModuleErrors()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((errorInfo) => {
        if (errorInfo.errorType === ErrorType.MODULE_API_ERROR) {
          this.errorModule = errorInfo.hasError;
        }
        if (
          errorInfo.errorType === ErrorType.DETAILS_API_ERROR &&
          this.newsId === errorInfo.id
        ) {
          if (errorInfo.hasError) {
            this.excludedNews = [];
          }
          this.newsNotFound = errorInfo.hasError;
        }
      });
  }

  private setExcludedNews() {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation?.extras.state;

    this.excludedNews = state?.['excludedNewsIds']
      ? Array.from(new Set(state['excludedNewsIds'].split(',')))
      : [];
  }
}
