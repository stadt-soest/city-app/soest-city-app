export interface NewsCategory {
  id: string;
  sourceCategories?: Array<string>;
  localizedNames: Array<CategoryLocalization>;
}

export interface CategoryLocalization {
  value: string;
  language: Language;
}

export type Language = 'DE' | 'EN';
