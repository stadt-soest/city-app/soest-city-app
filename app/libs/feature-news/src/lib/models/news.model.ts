import { Image } from '@sw-code/urbo-core';

export interface News {
  id: string;
  title: string;
  subtitle?: string;
  date: Date;
  image: Image | null;
  text: string;
  source: string;
  sourceUrl: string | null;
  categoryIds: string[];
  highlight: boolean;
}
