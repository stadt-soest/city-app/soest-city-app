export class Item {
  label: string;
  checked: boolean;

  constructor(label: string, checked: boolean) {
    this.label = label;
    this.checked = checked;
  }
}

export class ContentData {
  title: string;
  items: Item[];

  constructor(title: string, items: Item[]) {
    this.title = title;
    this.items = items;
  }
}
