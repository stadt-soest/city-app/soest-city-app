import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NewsDetailsSkeletonComponent } from './news-details-skeleton.component';
import { UITestModule } from '@sw-code/urbo-ui';
import { CoreTestModule } from '@sw-code/urbo-core';

describe('NewsDetailsSkeletonComponent', () => {
  let component: NewsDetailsSkeletonComponent;
  let fixture: ComponentFixture<NewsDetailsSkeletonComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NewsDetailsSkeletonComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
      ],
      providers: [],
    }).compileComponents();

    fixture = TestBed.createComponent(NewsDetailsSkeletonComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
