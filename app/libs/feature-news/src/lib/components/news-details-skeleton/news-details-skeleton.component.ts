import { Component } from '@angular/core';
import {
  IonCol,
  IonGrid,
  IonRow,
  IonSkeletonText,
} from '@ionic/angular/standalone';
import { NgFor } from '@angular/common';

@Component({
  selector: 'lib-news-details-skeleton',
  templateUrl: './news-details-skeleton.component.html',
  styleUrls: ['./news-details-skeleton.component.scss'],
  imports: [IonGrid, IonCol, IonSkeletonText, IonRow, NgFor],
})
export class NewsDetailsSkeletonComponent {}
