import { Component, inject } from '@angular/core';
import { provideTranslocoScope, TranslocoDirective } from '@jsverse/transloco';
import { OnboardingButton, OnboardingComponent } from '@sw-code/urbo-ui';
import { ModuleInfo, OnboardingService } from '@sw-code/urbo-core';
import { NEWS_MODULE_INFO } from '../../feature-news.module';

@Component({
  selector: 'lib-news-onboarding-card',
  template: `
    <lib-onboarding
      *transloco="let t"
      [imageUrl]="imageUrl"
      [title]="t(title)"
      [content]="t(content)"
      [moduleInfo]="newsModuleInfo"
      [buttons]="buttons"
    >
    </lib-onboarding>
  `,
  imports: [TranslocoDirective, OnboardingComponent],
  providers: [
    provideTranslocoScope({
      scope: 'feature-news',
      alias: 'feature_news',
      loader: {
        en: () => import('../../i18n/en.json'),
        de: () => import('../../i18n/de.json'),
      },
    }),
  ],
})
export class NewsOnboardingComponent {
  imageUrl = 'assets/illustrations/news.svg';
  title = 'feature_news.onboarding.title';
  content = 'feature_news.onboarding.content';
  buttons: OnboardingButton[];
  public newsModuleInfo = inject<ModuleInfo>(NEWS_MODULE_INFO);
  private readonly onboardingService = inject(OnboardingService);

  constructor() {
    this.buttons = [
      {
        icon: 'visibility_off',
        text: 'feature_news.later',
        action: () =>
          this.onboardingService.navigateToNextSlide(
            this.newsModuleInfo,
            'later',
          ),
        selected: !this.newsModuleInfo.defaultVisibility,
        shouldApplySelectionStyle: true,
        id: 'visibility-off-button',
      },
      {
        icon: 'favorite_border',
        text: 'feature_news.activate',
        action: () =>
          this.onboardingService.navigateToNextSlide(
            this.newsModuleInfo,
            'activate',
          ),
        selected: this.newsModuleInfo.defaultVisibility,
        shouldApplySelectionStyle: true,
        id: 'visibility-on-button',
      },
    ];
  }
}
