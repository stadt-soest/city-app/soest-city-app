import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NewsOnboardingComponent } from './news-onboarding.component';
import { UITestModule } from '@sw-code/urbo-ui';
import { CoreTestModule } from '@sw-code/urbo-core';
import { getTranslocoModule } from '../../../transloco-testing.module';
import { NEWS_MODULE_INFO } from '../../feature-news.module';
import { createNewsModuleInfo } from '../../shared/testing/news.test-utils';

describe('NewsOnboardingCardComponent', () => {
  let component: NewsOnboardingComponent;
  let fixture: ComponentFixture<NewsOnboardingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        NewsOnboardingComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        {
          provide: NEWS_MODULE_INFO,
          useValue: () => createNewsModuleInfo,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(NewsOnboardingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
