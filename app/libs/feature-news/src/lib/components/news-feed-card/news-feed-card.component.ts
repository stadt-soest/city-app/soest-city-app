import { Component } from '@angular/core';
import {
  IonCard,
  IonCardContent,
  IonRouterLinkWithHref,
} from '@ionic/angular/standalone';
import { RouterLink } from '@angular/router';
import { NgIf } from '@angular/common';
import { LocalizedRelativeTimePipePipe } from '@sw-code/urbo-ui';
import { NewsFeedItem } from '../../models/NewsFeedItem';
import { AbstractFeedCardComponent } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-news-feed-card',
  templateUrl: './news-feed-card.component.html',
  styleUrls: ['./news-feed-card.component.scss'],
  imports: [
    IonRouterLinkWithHref,
    RouterLink,
    IonCard,
    NgIf,
    IonCardContent,
    LocalizedRelativeTimePipePipe,
  ],
})
export class NewsFeedCardComponent extends AbstractFeedCardComponent<NewsFeedItem> {}
