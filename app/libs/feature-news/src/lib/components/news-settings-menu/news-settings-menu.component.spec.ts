import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NewsSettingsMenuComponent } from './news-settings-menu.component';
import { NewsService } from '../../services/news.service';
import { of } from 'rxjs';
import { StorageLoaderService } from '../../services/storage-loader/storage-loader.service';
import {
  ConnectivityService,
  CoreTestModule,
  NavigationService,
} from '@sw-code/urbo-core';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../transloco-testing.module';
import { NEWS_MODULE_INFO } from '../../feature-news.module';
import { createNewsModuleInfo } from '../../shared/testing/news.test-utils';

describe('NewsSettingsMenuComponent', () => {
  let component: NewsSettingsMenuComponent;
  let fixture: ComponentFixture<NewsSettingsMenuComponent>;

  const mockNewsService: jest.Mocked<NewsService> = {
    getFeed: jest.fn().mockReturnValue(of([])),
    getCategories: jest.fn().mockReturnValue(
      of([
        {
          id: 'Default Category 1',
          localizedNames: [
            { language: 'EN', value: 'Category 1' },
            { language: 'FR', value: 'Catégorie 1' },
          ],
        },
        {
          id: 'Default Category 2',
          localizedNames: [{ language: 'EN', value: 'Category 2' }],
        },
      ]),
    ),
  } as unknown as jest.Mocked<NewsService>;

  const mockStorageLoaderService: jest.Mocked<StorageLoaderService> = {
    getUniqueSources: jest.fn().mockResolvedValue(['Source1', 'Source2']),
    getUserSettings: jest.fn().mockResolvedValue({
      sources: ['Source1', 'Source2'],
      categoryIds: ['Default Category 1', 'Default Category 2'],
    }),
  } as unknown as jest.Mocked<StorageLoaderService>;

  const mockConnectivityService: jest.Mocked<ConnectivityService> = {
    checkOnlineStatus: jest.fn().mockResolvedValue(true),
  } as unknown as jest.Mocked<ConnectivityService>;

  const mockNavigationService = {
    navigateToFeedItemDetails: jest.fn().mockResolvedValue(true),
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        NewsSettingsMenuComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        { provide: NavigationService, useValue: mockNavigationService },
        {
          provide: NewsService,
          useValue: mockNewsService,
        },
        {
          provide: StorageLoaderService,
          useValue: mockStorageLoaderService,
        },
        {
          provide: ConnectivityService,
          useValue: mockConnectivityService,
        },
        {
          provide: NEWS_MODULE_INFO,
          useValue: createNewsModuleInfo,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(NewsSettingsMenuComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  }));

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set uniqueSources and uniqueCategories based on StorageLoaderService and NewsService data', async () => {
    component.ngOnInit();
    fixture.detectChanges();

    expect(component.uniqueSources).toEqual([
      { name: 'Source1' },
      { name: 'Source2' },
    ]);
    expect(component.uniqueCategories).toEqual([
      { id: 'Default Category 1', name: 'Category 1' },
      { id: 'Default Category 2', name: 'Category 2' },
    ]);
  });

  it('should set isConnected to true if connectivity service returns online status', async () => {
    component.ngOnInit();
    expect(component.isConnected).toBe(true);
    expect(mockConnectivityService.checkOnlineStatus).toHaveBeenCalled();
  });

  it('should correctly render ui-custom-settings-list', async () => {
    component.ngOnInit();
    fixture.detectChanges();
    const settingsMenu = fixture.nativeElement.querySelector(
      'lib-custom-settings-list',
    );
    expect(settingsMenu).toBeTruthy();
  });
});
