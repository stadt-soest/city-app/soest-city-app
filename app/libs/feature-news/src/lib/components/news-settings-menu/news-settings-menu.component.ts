import { Component, DestroyRef, inject, OnInit } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { TranslocoDirective } from '@jsverse/transloco';
import { IonCol, IonGrid } from '@ionic/angular/standalone';
import { NgIf } from '@angular/common';
import {
  ConnectivityErrorCardComponent,
  CustomSettingsListComponent,
} from '@sw-code/urbo-ui';
import {
  ConnectivityService,
  ModuleInfo,
  ModuleSettings,
  StorageKeyType,
  TranslationService,
} from '@sw-code/urbo-core';
import { NewsService } from '../../services/news.service';
import { StorageLoaderService } from '../../services/storage-loader/storage-loader.service';
import { NEWS_MODULE_INFO } from '../../feature-news.module';
import { combineLatest, map } from 'rxjs';

@Component({
  selector: 'lib-news-settings-menu',
  templateUrl: './news-settings-menu.component.html',
  styleUrls: ['./news-settings-menu.component.scss'],
  imports: [
    TranslocoDirective,
    IonGrid,
    NgIf,
    IonCol,
    ConnectivityErrorCardComponent,
    CustomSettingsListComponent,
  ],
})
export class NewsSettingsMenuComponent implements OnInit {
  isLoadingSources = true;
  isLoadingCategories = true;
  isConnected = true;
  uniqueSources: ModuleSettings[] = [];
  uniqueCategories: ModuleSettings[] = [];
  newsModule!: ModuleInfo;
  localLanguage = 'de';
  protected readonly storageKeyType = StorageKeyType;
  private readonly newsService = inject(NewsService);
  private readonly storageLoaderService = inject(StorageLoaderService);
  private readonly translateService = inject(TranslationService);
  private readonly connectivityService = inject(ConnectivityService);
  private readonly newsModuleInfo = inject<ModuleInfo>(NEWS_MODULE_INFO);
  private readonly destroyRef = inject(DestroyRef);

  ngOnInit() {
    this.setOnlineStatus();
    this.newsModule = this.newsModuleInfo;
    this.localLanguage = this.translateService.getCurrentLocale();
    this.subscribeToFeedItems();
  }

  private async setOnlineStatus() {
    this.isConnected = await this.connectivityService.checkOnlineStatus();
  }

  private async subscribeToFeedItems() {
    const sources$ = this.storageLoaderService
      .getUniqueSources()
      .then((sources) => sources.map((source) => ({ name: source })));

    const categories$ = this.newsService.getCategories().pipe(
      map((categories) =>
        categories.map((category) => {
          const localizedName =
            category.localizedNames.find(
              (loc: { language: string }) =>
                loc.language === this.localLanguage,
            ) ??
            category.localizedNames.find(
              (loc: { language: string }) => loc.language === 'EN',
            );

          return {
            id: category.id ? category.id : '',
            name: localizedName ? localizedName.value : 'Unnamed Category',
          };
        }),
      ),
    );

    combineLatest([sources$, categories$])
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(([sources, categories]) => {
        this.uniqueSources = sources;
        this.isLoadingSources = false;
        this.uniqueCategories = categories;
        this.isLoadingCategories = false;
      });
  }
}
