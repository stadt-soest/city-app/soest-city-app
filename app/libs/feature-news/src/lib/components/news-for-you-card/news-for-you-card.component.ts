import { Component, inject } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  IonCard,
  IonCardContent,
  IonRouterLinkWithHref,
} from '@ionic/angular/standalone';
import { RouterLink } from '@angular/router';
import { NgIf } from '@angular/common';
import {
  GeneralIconComponent,
  IconColor,
  IconFontSize,
  IconType,
  LocalizedRelativeTimePipePipe,
} from '@sw-code/urbo-ui';
import { NewsFeedItem } from '../../models/NewsFeedItem';
import { AbstractFeedCardComponent, ModuleInfo } from '@sw-code/urbo-core';
import { NEWS_MODULE_INFO } from '../../feature-news.module';

@Component({
  selector: 'lib-news-for-you-card',
  templateUrl: './news-for-you-card.component.html',
  styleUrls: ['./news-for-you-card.component.scss'],
  imports: [
    TranslocoDirective,
    IonRouterLinkWithHref,
    RouterLink,
    IonCard,
    NgIf,
    IonCardContent,
    GeneralIconComponent,
    LocalizedRelativeTimePipePipe,
  ],
})
export class NewsForYouCardComponent extends AbstractFeedCardComponent<NewsFeedItem> {
  newsModuleIcon: string;
  protected readonly iconType = IconType;
  protected readonly iconFontSize = IconFontSize;
  protected readonly iconColor = IconColor;
  private readonly newsModuleInfo = inject<ModuleInfo>(NEWS_MODULE_INFO);

  constructor() {
    super();
    this.newsModuleIcon = this.newsModuleInfo.icon;
  }
}
