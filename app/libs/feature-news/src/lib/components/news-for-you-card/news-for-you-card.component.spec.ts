import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NewsForYouCardComponent } from './news-for-you-card.component';
import {
  CoreTestModule,
  ImageService,
  NavigationService,
} from '@sw-code/urbo-core';
import { getTranslocoModule } from '../../../transloco-testing.module';
import { NEWS_MODULE_INFO } from '../../feature-news.module';
import {
  createNewsFeedItem,
  createNewsModuleInfo,
} from '../../shared/testing/news.test-utils';
import { NewsService } from '../../services/news.service';

describe('NewsFeedCardComponent', () => {
  let component: NewsForYouCardComponent;
  let fixture: ComponentFixture<NewsForYouCardComponent>;

  const mockImageService = {
    getImageUrl: jest.fn().mockReturnValue('http://localhost/sample-image-url'),
  };

  const mockNavigationService = {
    navigateToFeedItemDetails: jest.fn(),
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NewsForYouCardComponent,
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        { provide: ImageService, useValue: mockImageService },
        { provide: NavigationService, useValue: mockNavigationService },
        {
          provide: NEWS_MODULE_INFO,
          useValue: createNewsModuleInfo,
        },
        NewsService,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(NewsForYouCardComponent);
    component = fixture.componentInstance;

    component.feedItem = createNewsFeedItem({
      id: '1',
      baseRoutePath: '/news',
      moduleName: 'News',
      forYouComponent: NewsForYouCardComponent,
      cardComponent: NewsForYouCardComponent,
      icon: 'circle-exclamation',
      title: 'Breaking: New Tech Product Launches!',
      image: {
        imageSource: 'default-image.jpg',
        thumbnail: {
          filename: 'default-file.jpg',
          width: 100,
          height: 100,
        },
        highRes: {
          filename: 'default-file.jpg',
          width: 100,
          height: 100,
        },
      },
      creationDate: new Date('2023-10-06T12:00:00Z'),
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return correct image URL', () => {
    expect(component.imageUrl).toEqual('http://localhost/sample-image-url');
  });

  describe('HTML rendering', () => {
    it('should display the title', () => {
      const titleElement = fixture.nativeElement.querySelector('.card-title');
      expect(titleElement.textContent).toContain(
        'Breaking: New Tech Product Launches!',
      );
    });

    it('should display the image with the correct source URL', () => {
      const imageElement: HTMLImageElement =
        fixture.nativeElement.querySelector('img');
      expect(imageElement).toBeTruthy();
      expect(imageElement.src).toBe('http://localhost/sample-image-url');
    });
  });

  describe('navigateToDetails', () => {
    it('should navigate to feed item details', async () => {
      await component.navigateToDetails();
      expect(
        mockNavigationService.navigateToFeedItemDetails,
      ).toHaveBeenCalledWith(component.feedItem);
    });
  });
});
