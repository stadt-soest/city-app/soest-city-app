import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { StorageLoaderService } from './storage-loader.service';
import {
  ModuleInfo,
  SettingsPersistenceService,
  StorageKeyType,
} from '@sw-code/urbo-core';
import { NewsAdapter } from '../../news.adapter';
import { createNewsModuleInfo } from '../../shared/testing/news.test-utils';
import { NEWS_MODULE_INFO } from '../../feature-news.module';
import { NewsCategory } from '../../interface/news-category.interface';

describe('StorageLoaderService', () => {
  let service: StorageLoaderService;
  let mockSettingsPersistenceService: jest.Mocked<SettingsPersistenceService>;
  let mockNewsAdapter: jest.Mocked<NewsAdapter>;
  let moduleInfo: ModuleInfo;

  beforeEach(() => {
    moduleInfo = createNewsModuleInfo;

    mockSettingsPersistenceService = {
      saveSettings: jest.fn(),
      getSettings: jest.fn(),
      getEnabledSettingsKeys: jest.fn(),
      updateSettings: jest.fn(),
    } as unknown as jest.Mocked<SettingsPersistenceService>;

    mockNewsAdapter = {
      getSources: jest
        .fn()
        .mockReturnValue(of(['Default Source 1', 'Default Source 2'])),
    } as unknown as jest.Mocked<NewsAdapter>;

    TestBed.configureTestingModule({
      providers: [
        {
          provide: SettingsPersistenceService,
          useValue: mockSettingsPersistenceService,
        },
        { provide: NEWS_MODULE_INFO, useValue: moduleInfo },
        { provide: NewsAdapter, useValue: mockNewsAdapter },
        StorageLoaderService,
      ],
    });

    service = TestBed.inject(StorageLoaderService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call updateSettings with correct values when initializeDefaultSettings is called', async () => {
    const newsCategoriesObservable = of([
      { id: 'Default Category 1' },
      { id: 'Default Category Id 3' },
    ] as NewsCategory[]);

    mockSettingsPersistenceService.getSettings.mockResolvedValueOnce({});
    mockSettingsPersistenceService.getSettings.mockResolvedValueOnce({});

    const expectedSourcesSettingsKeys = [
      'Default Source 1',
      'Default Source 2',
    ];
    const expectedCategoriesSettingsKeys = [
      'Default Category 1',
      'Default Category Id 3',
    ];

    await service.initializeDefaultSettings(newsCategoriesObservable);

    expect(mockSettingsPersistenceService.updateSettings).toHaveBeenCalledWith(
      moduleInfo,
      expect.arrayContaining(expectedSourcesSettingsKeys),
      {},
      StorageKeyType.SOURCE,
    );

    expect(mockSettingsPersistenceService.updateSettings).toHaveBeenCalledWith(
      moduleInfo,
      expect.arrayContaining(expectedCategoriesSettingsKeys),
      {},
      StorageKeyType.CATEGORY,
    );
  });

  it('loadEnabledSettingsKeys should return settings based on storageKeyType', async () => {
    const storageKeyType = StorageKeyType.SOURCE;
    const expectedSettings = ['Default Source 1', 'Default Source 2'];

    mockSettingsPersistenceService.getEnabledSettingsKeys.mockResolvedValue(
      expectedSettings,
    );

    const result = await service.loadEnabledSettingsKeys(storageKeyType);

    expect(result).toEqual(expectedSettings);
  });

  it('getUniqueSources should return unique sources from NewsAdapter', async () => {
    const expectedSources = ['Default Source 1', 'Default Source 2'];
    const result = await service.getUniqueSources();

    expect(mockNewsAdapter.getSources).toHaveBeenCalled();
    expect(result).toEqual(expectedSources);
  });

  it('getUserSettings should return user settings for sources and categories', async () => {
    const expectedSources = ['Source 1', 'Source 2'];
    const expectedCategories = ['Category 1', 'Category 2'];

    mockSettingsPersistenceService.getEnabledSettingsKeys
      .mockResolvedValueOnce(expectedSources)
      .mockResolvedValueOnce(expectedCategories);

    const result = await service.getUserSettings();

    expect(result).toEqual({
      sources: expectedSources,
      categoryIds: expectedCategories,
    });
  });
});
