import { inject, Injectable } from '@angular/core';
import {
  FeedUserSettings,
  ModuleInfo,
  SettingsPersistenceService,
  StorageKeyType,
} from '@sw-code/urbo-core';
import { NEWS_MODULE_INFO } from '../../feature-news.module';
import { NewsAdapter } from '../../news.adapter';
import { firstValueFrom, Observable } from 'rxjs';
import { NewsCategory } from '../../interface/news-category.interface';

@Injectable()
export class StorageLoaderService {
  private readonly settingsPersistenceService = inject(
    SettingsPersistenceService,
  );
  private readonly newsModuleInfo = inject<ModuleInfo>(NEWS_MODULE_INFO);
  private readonly newsAdapter = inject(NewsAdapter);

  async initializeDefaultSettings(
    categories: Observable<NewsCategory[]>,
  ): Promise<void> {
    const categoryIdsArray = await firstValueFrom(categories);

    await this.updateSourceSettings();
    await this.updateCategorySettings(categoryIdsArray);
  }

  async loadEnabledSettingsKeys(
    storageKeyType: StorageKeyType,
  ): Promise<string[]> {
    return this.settingsPersistenceService.getEnabledSettingsKeys(
      this.newsModuleInfo,
      storageKeyType,
    );
  }

  async getUniqueSources(): Promise<string[]> {
    const sources = await firstValueFrom(this.newsAdapter.getSources());
    return sources || [];
  }

  async getUserSettings(): Promise<FeedUserSettings> {
    const sources = await this.loadEnabledSettingsKeys(StorageKeyType.SOURCE);
    const categoryIds = await this.loadEnabledSettingsKeys(
      StorageKeyType.CATEGORY,
    );
    return { sources, categoryIds };
  }

  private async updateSourceSettings(): Promise<void> {
    const existingSourceSettings =
      (await this.getSettings(StorageKeyType.SOURCE)) || {};
    const uniqueSources = await this.getUniqueSources();
    await this.settingsPersistenceService.updateSettings(
      this.newsModuleInfo,
      uniqueSources,
      existingSourceSettings,
      StorageKeyType.SOURCE,
    );
  }

  private async updateCategorySettings(
    categories: NewsCategory[],
  ): Promise<void> {
    const existingCategorySettings =
      (await this.getSettings(StorageKeyType.CATEGORY)) || {};
    const uniqueCategoryIds = categories.map((category) => category.id);
    await this.settingsPersistenceService.updateSettings(
      this.newsModuleInfo,
      uniqueCategoryIds,
      existingCategorySettings,
      StorageKeyType.CATEGORY,
    );
  }

  private async getSettings(
    keyType: StorageKeyType,
  ): Promise<{ [key: string]: boolean }> {
    const settings = await this.settingsPersistenceService.getSettings(
      this.newsModuleInfo,
      keyType,
    );
    return typeof settings === 'string' ? JSON.parse(settings) : settings;
  }
}
