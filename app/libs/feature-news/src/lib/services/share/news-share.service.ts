import { inject, Injectable } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';
import { BaseShareService } from '@sw-code/urbo-core';
import { News } from '../../models/news.model';

@Injectable()
export class NewsShareService {
  private readonly baseShareService = inject(BaseShareService);
  private readonly translateService = inject(TranslocoService);

  async shareNews(news: News): Promise<void> {
    const title = news.title;
    const url = window.location.href;
    const text = this.translateService.translate(
      'feature_news.details.share.content',
      {
        title,
        url,
      },
    );

    await this.baseShareService.share(title, text, url);
  }
}
