import { TestBed } from '@angular/core/testing';
import { TranslocoService } from '@jsverse/transloco';
import { NewsShareService } from './news-share.service';
import { BaseShareService } from '@sw-code/urbo-core';

describe('NewsShareService', () => {
  let service: NewsShareService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        NewsShareService,
        { provide: BaseShareService, useValue: {} },
        { provide: TranslocoService, useValue: {} },
      ],
    });
    service = TestBed.inject(NewsShareService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
