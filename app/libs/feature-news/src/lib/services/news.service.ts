import { inject, Injectable } from '@angular/core';
import {
  catchError,
  combineLatest,
  from,
  Observable,
  of,
  Subject,
  switchMap,
  throwError,
} from 'rxjs';
import {
  ErrorHandlingService,
  ErrorInfo,
  ErrorType,
  Feedable,
  ModuleInfo,
  StorageKeyType,
} from '@sw-code/urbo-core';
import { NEWS_MODULE_INFO } from '../feature-news.module';
import { StorageLoaderService } from './storage-loader/storage-loader.service';
import { NewsAdapter } from '../news.adapter';
import { NewsFeedItem } from '../models/NewsFeedItem';
import { News } from '../models/news.model';
import { NewsCategory } from '../interface/news-category.interface';

@Injectable()
export class NewsService implements Feedable {
  private readonly errorHandlingService = inject(ErrorHandlingService);
  private readonly newsModuleInfo = inject<ModuleInfo>(NEWS_MODULE_INFO);
  private readonly storageLoaderService = inject(StorageLoaderService);
  private readonly newsModuleErrorSubject = new Subject<ErrorInfo>();
  private readonly newsAdapter = inject(NewsAdapter);

  getFeed(): Observable<NewsFeedItem[]> {
    this.emitModuleError(ErrorType.MODULE_API_ERROR, false);
    return this.newsAdapter.getFeed().pipe(
      catchError(() => {
        this.emitModuleError(ErrorType.MODULE_API_ERROR, true);
        this.errorHandlingService.setErrorState(
          this.newsModuleInfo,
          true,
          ErrorType.MODULE_API_ERROR,
        );
        return of([]);
      }),
    );
  }

  getPaginatedFeed(
    page: number,
  ): Observable<{ content: NewsFeedItem[]; totalRecords: number }> {
    this.setModuleApiError(false);
    return this.newsAdapter.getPaginatedFeed(page).pipe(
      catchError(() => {
        this.setModuleApiError(true);
        return of({ content: [], totalRecords: 0 });
      }),
    );
  }

  getHighlightedPaginatedFeed(
    page: number,
  ): Observable<{ content: NewsFeedItem[]; totalRecords: number }> {
    this.setModuleApiError(false);
    return this.newsAdapter.getHighlightedPaginatedFeed(page).pipe(
      catchError(() => {
        this.setModuleApiError(true);
        return of({ content: [], totalRecords: 0 });
      }),
    );
  }

  getPaginatedFilteredFeed(
    page: number,
    newsCategoryIds?: string[],
    newsSources?: string[],
    excludedNews: string[] = [],
  ): Observable<{ content: NewsFeedItem[]; totalRecords: number }> {
    this.setModuleApiError(false);
    return combineLatest([
      from(this.getSourceSettings()),
      from(this.getCategorySettings()),
    ]).pipe(
      switchMap(([sources, categoryIds]) =>
        this.newsAdapter
          .getPaginatedFilteredFeed(
            page,
            newsCategoryIds ?? categoryIds,
            newsSources ?? sources,
            excludedNews,
          )
          .pipe(
            catchError(() => {
              this.setModuleApiError(true);
              return of({ content: [], totalRecords: 0 });
            }),
          ),
      ),
    );
  }

  getById(id: string): Observable<News> {
    this.emitNewsNotFoundError(id, ErrorType.DETAILS_API_ERROR, false);
    return this.newsAdapter.getById(id).pipe(
      catchError((error) => {
        this.emitNewsNotFoundError(id, ErrorType.DETAILS_API_ERROR, true);
        this.errorHandlingService.setErrorState(
          this.newsModuleInfo,
          true,
          ErrorType.DETAILS_API_ERROR,
          id,
        );
        return throwError(() => error);
      }),
    );
  }

  getCategories(): Observable<NewsCategory[]> {
    this.setModuleApiError(false);
    return this.newsAdapter.getCategories().pipe(
      catchError((error) => {
        console.error('Failed to fetch categories', error);
        this.setModuleApiError(true);
        return of([]);
      }),
    );
  }

  getBySearchTerm(
    searchTerm: string,
    page?: number,
    size?: number,
  ): Observable<{ total: number; items: NewsFeedItem[] }> {
    const pageNumber = page ?? 1;
    const pageSize = size ?? 10;

    this.emitModuleError(ErrorType.MODULE_API_ERROR, false);
    return this.newsAdapter
      .getBySearchTerm(searchTerm, pageNumber, pageSize)
      .pipe(
        catchError(() => {
          this.emitModuleError(ErrorType.MODULE_API_ERROR, true);
          this.errorHandlingService.setErrorState(
            this.newsModuleInfo,
            true,
            ErrorType.MODULE_API_ERROR,
          );
          return of({ items: [], total: 0 });
        }),
      );
  }

  getNewsModuleErrors(): Observable<ErrorInfo> {
    return this.newsModuleErrorSubject.asObservable();
  }

  getSourceSettings(): Promise<string[]> {
    return this.storageLoaderService.loadEnabledSettingsKeys(
      StorageKeyType.SOURCE,
    );
  }

  getCategorySettings(): Promise<string[]> {
    return this.storageLoaderService.loadEnabledSettingsKeys(
      StorageKeyType.CATEGORY,
    );
  }

  private emitModuleError(errorType: ErrorType, hasError: boolean) {
    this.newsModuleErrorSubject.next({
      hasError,
      errorType,
    });
  }

  private emitNewsNotFoundError(
    id: string,
    errorType: ErrorType,
    hasError: boolean,
  ) {
    this.newsModuleErrorSubject.next({
      hasError,
      errorType,
      id,
    });
  }

  private setModuleApiError(hasError: boolean) {
    this.emitModuleError(ErrorType.MODULE_API_ERROR, hasError);
    this.errorHandlingService.setErrorState(
      this.newsModuleInfo,
      hasError,
      ErrorType.MODULE_API_ERROR,
    );
  }
}
