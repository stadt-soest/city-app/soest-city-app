import { inject, Injectable } from '@angular/core';
import { catchError, forkJoin, map, Observable, of, switchMap } from 'rxjs';
import { NewsService } from '../news.service';
import { FeedResult } from '@sw-code/urbo-core';
import { NewsFeedItem } from '../../models/NewsFeedItem';

@Injectable()
export class NewsFeedablesLoaderService {
  private readonly newsService = inject(NewsService);

  getFeedables(paginationPage: number): Observable<FeedResult<NewsFeedItem>> {
    return forkJoin([
      this.newsService.getSourceSettings(),
      this.newsService.getCategorySettings(),
    ]).pipe(
      switchMap(([sources, categories]) => {
        if (sources.length === 0 && categories.length === 0) {
          return of({
            items: [],
            totalRecords: 0,
          });
        } else {
          return this.newsService.getPaginatedFilteredFeed(paginationPage).pipe(
            map((response) => ({
              items: response.content,
              totalRecords: response.totalRecords,
            })),
          );
        }
      }),
    );
  }

  getUnfilteredFeedables(
    paginationPage: number,
  ): Observable<FeedResult<NewsFeedItem>> {
    return this.newsService.getPaginatedFeed(paginationPage).pipe(
      map((response) => ({
        items: response.content,
        totalRecords: response.totalRecords,
      })),
    );
  }

  getHighlightedFeedables(
    paginationPage: number,
  ): Observable<FeedResult<NewsFeedItem>> {
    return this.newsService.getHighlightedPaginatedFeed(paginationPage).pipe(
      map((response) => {
        if (!response.content) {
          return {
            items: [],
            totalRecords: 0,
          };
        }
        return {
          items: response.content,
          totalRecords: response.totalRecords,
        };
      }),
      catchError((error) => {
        console.error('Error loading highlighted feedables:', error);
        return of({ items: [], totalRecords: 0 });
      }),
    );
  }
}
