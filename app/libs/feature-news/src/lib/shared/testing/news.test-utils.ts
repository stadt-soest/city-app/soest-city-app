import {
  FeedItemLayoutType,
  Image,
  ModuleCategory,
  ModuleInfo,
  ModuleSettings,
} from '@sw-code/urbo-core';
import { News } from '../../models/news.model';
import { NewsFeedItem } from '../../models/NewsFeedItem';
import { NewsFeedCardComponent } from '../../components/news-feed-card/news-feed-card.component';
import { NewsForYouCardComponent } from '../../components/news-for-you-card/news-for-you-card.component';

export const createNewsModuleInfo: ModuleInfo = {
  id: 'news',
  name: 'News',
  category: ModuleCategory.information,
  icon: 'campaign',
  baseRoutingPath: '/news',
  defaultVisibility: true,
  settingsPageAvailable: false,
  isAboutAppItself: false,
  relevancy: 0.5,
  forYouRelevant: false,
  showInCategoriesPage: false,
  orderInCategory: 1,
};

export const createNews = (overrides?: Partial<News>): News => ({
  id: 'default-id',
  title: 'default title',
  date: new Date('2023-10-20T08:00:00Z'),
  text: '',
  source: 'Default Source 1',
  categoryIds: ['Default Category id 1'],
  sourceUrl: null,
  highlight: false,
  image: {
    imageSource: 'default-image.jpg',
    description: 'default-image-description',
    copyrightHolder: 'default-image-copyright-holder',
    thumbnail: {
      filename: 'default-file.jpg',
      width: 100,
      height: 100,
    },
    highRes: {
      filename: 'default-file.jpg',
      width: 100,
      height: 100,
    },
  },
  ...overrides,
});

export const createNewsFeedItem = (
  overrides?: Partial<NewsFeedItem>,
): NewsFeedItem => ({
  id: 'default-id',
  moduleId: 'news',
  moduleName: 'feature_news.module_name_singular',
  pluralModuleName: 'feature_news.module_name',
  baseRoutePath: 'news',
  icon: 'campaign',
  title: 'default title',
  creationDate: new Date('2023-10-20T08:00:00Z'),
  image: createImage(),
  cardComponent: NewsFeedCardComponent,
  forYouComponent: NewsForYouCardComponent,
  relevancy: 0,
  layoutType: FeedItemLayoutType.card,
  ...overrides,
});

export const createImage = (overrides?: Partial<Image>): Image => ({
  description: 'default-image-description',
  imageSource: 'default-image.jpg',
  thumbnail: {
    filename: 'default-file.jpg',
    width: 100,
    height: 100,
  },
  highRes: {
    filename: 'default-file.jpg',
    width: 100,
    height: 100,
  },
  ...overrides,
});

export const createModuleSettingsNews = (
  overrides?: Partial<ModuleSettings>,
): ModuleSettings => ({
  id: undefined,
  name: 'news',
  ...overrides,
});
