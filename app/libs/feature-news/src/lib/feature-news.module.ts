import {
  Inject,
  InjectionToken,
  ModuleWithProviders,
  NgModule,
  inject,
  provideAppInitializer,
} from '@angular/core';
import { provideTranslocoScope } from '@jsverse/transloco';
import { NewsService } from './services/news.service';
import { NewsShareService } from './services/share/news-share.service';
import { StorageLoaderService } from './services/storage-loader/storage-loader.service';
import { NewsFeedablesLoaderService } from './services/feedables-loader/news-feedables-loader.service';
import {
  FeatureModule,
  FeedUserSettings,
  ModuleCategory,
  ModuleInfo,
  ModuleRegistryService,
} from '@sw-code/urbo-core';
import { NewsFeedItem } from './models/NewsFeedItem';
import { NewsOnboardingComponent } from './components/news-onboarding/news-onboarding.component';

const PROVIDERS = [
  NewsService,
  NewsShareService,
  StorageLoaderService,
  NewsFeedablesLoaderService,
  provideTranslocoScope({
    scope: 'feature-news',
    alias: 'feature_news',
    loader: {
      en: () => import('./i18n/en.json'),
      de: () => import('./i18n/de.json'),
    },
  }),
];
export const NEWS_MODULE_INFO = new InjectionToken<ModuleInfo>(
  'NewsModuleInfoHolderToken',
);
const moduleId = 'news';

@NgModule({
  providers: [
    ...PROVIDERS,
    {
      provide: NEWS_MODULE_INFO,
      useValue: {
        id: moduleId,
        name: 'feature_news.module_name',
        category: ModuleCategory.information,
        icon: 'campaign',
        baseRoutingPath: moduleId,
        defaultVisibility: true,
        settingsPageAvailable: true,
        isAboutAppItself: false,
        relevancy: 0.1,
        forYouRelevant: true,
        showInCategoriesPage: true,
        orderInCategory: 1,
      },
    },
  ],
})
export class FeatureNewsModule {
  readonly featureModule: FeatureModule<NewsFeedItem>;

  constructor(
    private readonly newsService: NewsService,
    private readonly feedablesLoaderService: NewsFeedablesLoaderService,
    private readonly storageLoaderService: StorageLoaderService,
    @Inject(NEWS_MODULE_INFO) private readonly newsModuleInfo: ModuleInfo,
  ) {
    this.featureModule = {
      initializeDefaultSettings: () => {
        (async () => {
          await this.storageLoaderService.initializeDefaultSettings(
            newsService.getCategories(),
          );
        })();
      },
      feedables: (page: number) =>
        this.feedablesLoaderService.getFeedables(page),
      feedablesUnfiltered: (page: number) =>
        this.feedablesLoaderService.getUnfilteredFeedables(page),
      feedablesHighlighted: (page: number) =>
        this.feedablesLoaderService.getHighlightedFeedables(page),
      feedItemType: 'NEWS',
      search: (searchTerm: string, page?: number, size?: number) =>
        this.newsService.getBySearchTerm(searchTerm, page, size),
      onboardingComponent: NewsOnboardingComponent,
      info: this.newsModuleInfo,
      getUserSettings: async (): Promise<FeedUserSettings> =>
        await this.storageLoaderService.getUserSettings(),
      routes: [
        {
          path: `for-you/${moduleId}`,
          redirectTo: 'for-you',
        },
        {
          path: `for-you/${moduleId}/:id`,
          loadComponent: () =>
            import('./pages/details/news-details-page.component').then(
              (m) => m.NewsDetailsPageComponent,
            ),
        },
        {
          path: `for-you/settings/${moduleId}`,
          loadComponent: () =>
            import('./pages/settings/news-settings-page.component').then(
              (m) => m.NewsSettingsPageComponent,
            ),
          title: 'feature_news.settings.title',
        },
        {
          path: `categories/${moduleId}`,
          loadComponent: () =>
            import('./pages/news-page.component').then(
              (m) => m.NewsPageComponent,
            ),
          title: 'feature_news.module_name',
        },
        {
          path: `categories/${moduleId}/settings`,
          loadComponent: () =>
            import('./pages/settings/news-settings-page.component').then(
              (m) => m.NewsSettingsPageComponent,
            ),
          title: 'feature_news.settings.title',
        },
        {
          path: `categories/${moduleId}/:id`,
          loadComponent: () =>
            import('./pages/details/news-details-page.component').then(
              (m) => m.NewsDetailsPageComponent,
            ),
        },
        {
          path: `search/results/${moduleId}/:id`,
          loadComponent: () =>
            import('./pages/details/news-details-page.component').then(
              (m) => m.NewsDetailsPageComponent,
            ),
        },
        {
          path: `search/results/${moduleId}`,
          redirectTo: 'search',
        },
      ],
    };
  }

  static forRoot(): ModuleWithProviders<FeatureNewsModule> {
    return {
      ngModule: FeatureNewsModule,
      providers: [
        provideAppInitializer(() => {
          const initializerFn = (
            (registry: ModuleRegistryService, module: FeatureNewsModule) =>
            () =>
              registry.registerFeature(module.featureModule)
          )(inject(ModuleRegistryService), inject(FeatureNewsModule));
          return initializerFn();
        }),
        provideAppInitializer(() => {
          const initializerFn = (
            (
              newsService: NewsService,
              newsStorageLoaderService: StorageLoaderService,
            ) =>
            () => {
              void newsStorageLoaderService.initializeDefaultSettings(
                newsService.getCategories(),
              );
            }
          )(inject(NewsService), inject(StorageLoaderService));
          return initializerFn();
        }),
      ],
    };
  }
}
