export { FeatureNewsModule } from './lib/feature-news.module';
export { News } from './lib/models/news.model';
export { NewsFeedItem } from './lib/models/NewsFeedItem';
export { NewsCategory } from './lib/interface/news-category.interface';
export { NewsAdapter } from './lib/news.adapter';
export { NEWS_MODULE_INFO } from './lib/feature-news.module';
export { BaseContentFormatterPipe } from '@sw-code/urbo-ui';
export { NewsFeedCardComponent } from './lib/components/news-feed-card/news-feed-card.component';
export { NewsForYouCardComponent } from './lib/components/news-for-you-card/news-for-you-card.component';
