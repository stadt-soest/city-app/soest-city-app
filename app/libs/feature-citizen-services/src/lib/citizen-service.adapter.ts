import { Observable } from 'rxjs';
import {
  CitizenService,
  CitizenServiceFeedItem,
  Tag,
} from './models/citizen-service.model';

export abstract class CitizenServiceAdapter {
  abstract getCitizenServices(
    tagId?: string,
    favorite?: boolean,
  ): Observable<CitizenService[]>;

  abstract getTags(): Observable<Tag[]>;

  abstract getTagById(id: string): Observable<Tag>;

  abstract getById(id: string): Observable<CitizenService>;

  abstract getBySearchTerm(
    searchTerm: string,
    pageNumber: number,
    pageSize: number,
  ): Observable<{ total: number; items: CitizenServiceFeedItem[] }>;
}
