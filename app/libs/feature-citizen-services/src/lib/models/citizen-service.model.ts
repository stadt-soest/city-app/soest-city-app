import { FeedItem, Localization } from '@sw-code/urbo-core';

export interface CitizenService {
  id: string;
  title: string;
  description: string;
  url: string;
  category?: string;
}

export interface Tag {
  id: string;
  localizedNames: Array<Localization>;
}

export type CitizenServiceFeedItem = FeedItem;
