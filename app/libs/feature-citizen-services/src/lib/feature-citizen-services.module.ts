import {
  Inject,
  InjectionToken,
  ModuleWithProviders,
  NgModule,
  inject,
  provideAppInitializer,
} from '@angular/core';
import { provideTranslocoScope } from '@jsverse/transloco';
import { CitizenServiceService } from './services/citizen-service.service';
import { CitizenServiceShareService } from './services/share/citizen-service-share.service';
import {
  FeatureModule,
  ModuleCategory,
  ModuleInfo,
  ModuleRegistryService,
} from '@sw-code/urbo-core';
import { CitizenServiceFeedItem } from './models/citizen-service.model';
import { from, of } from 'rxjs';
import { CitizenServiceOnboardingComponent } from './components/citizen-service-onboarding/citizen-service-onboarding.component';

const PROVIDERS = [
  CitizenServiceService,
  CitizenServiceShareService,
  provideTranslocoScope({
    scope: 'feature-citizen-services',
    alias: 'feature_citizen_services',
    loader: {
      en: () => import('./i18n/en.json'),
      de: () => import('./i18n/de.json'),
    },
  }),
];
const moduleId = 'townhall';
export const CITIZEN_SERVICES_MODULE_INFO = new InjectionToken<ModuleInfo>(
  'CitizenServicesModuleInfoHolderToken',
);

@NgModule({
  providers: [
    ...PROVIDERS,
    {
      provide: CITIZEN_SERVICES_MODULE_INFO,
      useValue: {
        id: moduleId,
        name: 'feature_citizen_services.module_name',
        category: ModuleCategory.yourCity,
        icon: 'contract',
        baseRoutingPath: moduleId,
        defaultVisibility: true,
        settingsPageAvailable: false,
        isAboutAppItself: false,
        relevancy: 0.8,
        forYouRelevant: false,
        showInCategoriesPage: true,
        orderInCategory: 1,
      },
    },
  ],
})
export class FeatureCitizenServicesModule {
  readonly featureModule: FeatureModule<CitizenServiceFeedItem>;

  constructor(
    @Inject(CITIZEN_SERVICES_MODULE_INFO)
    private readonly citizenServicesModuleInfo: ModuleInfo,
    private readonly citizenServiceService: CitizenServiceService,
  ) {
    this.featureModule = {
      feedables: () => of({ items: [], totalRecords: 0 }),
      feedablesUnfiltered: () => from([]),
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      initializeDefaultSettings: () => {},
      search: (searchTerm: string, page?: number, size?: number) =>
        this.citizenServiceService.getBySearchTerm(searchTerm, page, size),
      info: this.citizenServicesModuleInfo,
      routes: [
        {
          path: `categories/${moduleId}`,
          loadComponent: () =>
            import('./pages/citizen-services-page.component').then(
              (m) => m.CitizenServicesPageComponent,
            ),
          title: 'feature_citizen_services.module_name',
        },
        {
          path: `categories/${moduleId}/:id`,
          loadComponent: () =>
            import(
              './pages/citizen-service-details/service-details-page.component'
            ).then((m) => m.ServiceDetailsPageComponent),
        },
        {
          path: `categories/${moduleId}/category/:tagId`,
          loadComponent: () =>
            import(
              './pages/citizen-service-tag-details-page/citizen-service-tag-details-page.component'
            ).then((m) => m.CategoryDetailsPageComponent),
        },
        {
          path: `search/results/${moduleId}/:id`,
          loadComponent: () =>
            import(
              './pages/citizen-service-details/service-details-page.component'
            ).then((m) => m.ServiceDetailsPageComponent),
        },
        {
          path: `search/results/${moduleId}`,
          redirectTo: 'search',
        },
      ],
      onboardingComponent: CitizenServiceOnboardingComponent,
    };
  }

  static forRoot(): ModuleWithProviders<FeatureCitizenServicesModule> {
    return {
      ngModule: FeatureCitizenServicesModule,
      providers: [
        provideAppInitializer(() => {
          const initializerFn = (
            (
              registry: ModuleRegistryService,
              module: FeatureCitizenServicesModule,
            ) =>
            () =>
              registry.registerFeature(module.featureModule)
          )(
            inject(ModuleRegistryService),
            inject(FeatureCitizenServicesModule),
          );
          return initializerFn();
        }),
      ],
    };
  }
}
