import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CitizenServicesPageComponent } from './citizen-services-page.component';
import { CitizenServiceService } from '../services/citizen-service.service';
import { Router } from '@angular/router';
import { CitizenServiceAdapter } from '../citizen-service.adapter';
import { of } from 'rxjs';
import {
  mockCitizenService,
  mockCitizenServicesModuleInfo,
  mockTag,
} from '../shared/testing/citizen-services-test-utils';
import { CITIZEN_SERVICES_MODULE_INFO } from '../feature-citizen-services.module';
import { CoreTestModule } from '@sw-code/urbo-core';
import { getTranslocoModule } from '../../transloco-testing.module';

describe('CitizenServicePageComponent', () => {
  let component: CitizenServicesPageComponent;
  let fixture: ComponentFixture<CitizenServicesPageComponent>;
  let citizenServiceService: CitizenServiceService;
  let router: Router;
  let mockCitizenServiceAdapter: jest.Mocked<CitizenServiceAdapter>;

  beforeEach(() => {
    mockCitizenServiceAdapter = {
      getCitizenServices: jest.fn().mockReturnValue(of([mockCitizenService()])),
      getTags: jest.fn().mockReturnValue(of([mockTag()])),
      getTagById: jest.fn(),
      getById: jest.fn(),
      getBySearchTerm: jest.fn(),
    } as unknown as jest.Mocked<CitizenServiceAdapter>;

    TestBed.configureTestingModule({
      providers: [
        {
          provide: CITIZEN_SERVICES_MODULE_INFO,
          useFactory: () => mockCitizenServicesModuleInfo,
        },
        { provide: CitizenServiceAdapter, useValue: mockCitizenServiceAdapter },
        CitizenServiceService,
      ],
      imports: [
        CitizenServicesPageComponent,
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
    });

    fixture = TestBed.createComponent(CitizenServicesPageComponent);
    router = TestBed.inject(Router);
    citizenServiceService = TestBed.inject(CitizenServiceService);
    component = fixture.componentInstance;

    component.allServices = [mockCitizenService()];

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize data correctly in ngOnInit', () => {
    jest
      .spyOn(citizenServiceService, 'getCitizenServices')
      .mockReturnValue(of([mockCitizenService()]));
    jest
      .spyOn(citizenServiceService, 'getTags')
      .mockReturnValue(of([mockTag()]));

    component.ngOnInit();

    expect(component.popularServices.length).toBeGreaterThan(0);
    expect(component.serviceTags.length).toBeGreaterThan(0);
  });

  it('should navigate to the correct route in onFolderSelected', () => {
    const navigateSpy = jest
      .spyOn(router, 'navigate')
      .mockImplementation(() => Promise.resolve(true));
    component.onFolderSelected('1');
    expect(navigateSpy).toHaveBeenCalledWith(['/townhall/category-details/1']);
  });

  it('should display the sub-header with translated text', () => {
    const subHeaderElement =
      fixture.debugElement.nativeElement.querySelector('.sub-header');
    expect(subHeaderElement.innerHTML.toString().trim()).toBe(
      'Here you can discover the online services of the Town Hall (Rathaus Online).',
    );
  });
});
