import { Component, inject, OnInit } from '@angular/core';
import {
  CondensedHeaderLayoutComponent,
  ConnectivityErrorCardComponent,
  RefresherComponent,
} from '@sw-code/urbo-ui';
import { IonGrid } from '@ionic/angular/standalone';
import { NgIf } from '@angular/common';
import { ServiceListComponent } from '../components/service-list/service-list.component';
import { TagFolderListComponent } from '../components/tag-folder-list/tag-folder-list.component';
import { TranslocoPipe } from '@jsverse/transloco';
import { CitizenService, Tag } from '../models/citizen-service.model';
import { Router } from '@angular/router';
import { CitizenServiceService } from '../services/citizen-service.service';
import { ConnectivityService } from '@sw-code/urbo-core';
import { finalize, take } from 'rxjs';

@Component({
  selector: 'lib-citizen-services',
  templateUrl: './citizen-services-page.component.html',
  styleUrls: ['./citizen-services-page.component.scss'],
  imports: [
    CondensedHeaderLayoutComponent,
    RefresherComponent,
    IonGrid,
    ConnectivityErrorCardComponent,
    NgIf,
    ServiceListComponent,
    TagFolderListComponent,
    TranslocoPipe,
  ],
})
export class CitizenServicesPageComponent implements OnInit {
  popularServices: CitizenService[] = [];
  allServices: CitizenService[] = [];
  loadingPopularServices = true;
  loadingServiceTags = true;
  serviceTags: Tag[] = [];
  isConnected = true;
  private readonly router = inject(Router);
  private readonly citizenServiceService = inject(CitizenServiceService);
  private readonly connectivityService = inject(ConnectivityService);

  ngOnInit(): void {
    this.setOnlineStatus();
    this.loadPopularServices();
    this.loadServiceCategories();
  }

  async onFolderSelected(tagId: string): Promise<void> {
    await this.router.navigate([`/townhall/category-details/${tagId}`]);
  }

  refreshView(event: any) {
    this.resetState();
    this.loadPopularServices();
    this.loadServiceCategories();
    event.target.complete();
  }

  private async setOnlineStatus() {
    this.isConnected = await this.connectivityService.checkOnlineStatus();
  }

  private loadServiceCategories(): void {
    this.citizenServiceService
      .getTags()
      .pipe(
        take(1),
        finalize(() => {
          this.loadingServiceTags = false;
        }),
      )
      .subscribe({
        next: (tags: Tag[]): void => {
          this.serviceTags = tags.map((tag: Tag) => ({
            id: tag.id,
            localizedNames: tag.localizedNames,
          }));
        },
        error: (err) => {
          console.error(err);
        },
      });
  }

  private loadPopularServices(): void {
    this.citizenServiceService
      .getCitizenServices(undefined, true)
      .pipe(
        take(1),
        finalize(() => {
          this.loadingPopularServices = false;
        }),
      )
      .subscribe({
        next: (services) => {
          this.popularServices = services;
        },
        error: (err) => {
          console.error(err);
        },
      });
  }

  private resetState() {
    this.popularServices = [];
    this.serviceTags = [];
    this.loadingPopularServices = true;
    this.loadingServiceTags = true;
  }
}
