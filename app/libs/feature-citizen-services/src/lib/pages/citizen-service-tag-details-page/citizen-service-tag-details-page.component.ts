import { Component, inject, OnInit } from '@angular/core';
import { IonGrid } from '@ionic/angular/standalone';
import { ServiceListComponent } from '../../components/service-list/service-list.component';
import {
  CondensedHeaderLayoutComponent,
  RefresherComponent,
  TranslateLocalizationPipe,
} from '@sw-code/urbo-ui';
import { CitizenService, Tag } from '../../models/citizen-service.model';
import { ActivatedRoute } from '@angular/router';
import { CitizenServiceService } from '../../services/citizen-service.service';
import { TitleService, TranslationService } from '@sw-code/urbo-core';
import { finalize, take } from 'rxjs';

@Component({
  selector: 'lib-tag-details-page',
  templateUrl: './citizen-service-tag-details-page.component.html',
  styleUrls: ['./citizen-service-tag-details-page.component.scss'],
  providers: [TranslateLocalizationPipe],
  imports: [
    CondensedHeaderLayoutComponent,
    RefresherComponent,
    IonGrid,
    ServiceListComponent,
    TranslateLocalizationPipe,
  ],
})
export class CategoryDetailsPageComponent implements OnInit {
  tagId: string;
  tag: Tag | null = null;
  citizenServices: CitizenService[] = [];
  loadingCitizenServices = true;
  currentLocale: string;
  private readonly route = inject(ActivatedRoute);
  private readonly citizenServiceService = inject(CitizenServiceService);
  private readonly translationService = inject(TranslationService);
  private readonly titleService = inject(TitleService);
  private readonly translateLocalization = inject(TranslateLocalizationPipe);

  constructor() {
    this.tagId = this.route.snapshot.paramMap.get('tagId') ?? '';
    this.currentLocale = this.translationService.getCurrentLocale();
  }

  ngOnInit(): void {
    this.getTag(this.tagId);
    this.loadServices(this.tagId);
  }

  refreshView(event: any) {
    this.resetState();
    this.getTag(this.tagId);
    this.loadServices(this.tagId);
    event.target.complete();
  }

  private getTag(tagId: string) {
    this.citizenServiceService
      .getTagById(tagId)
      .pipe(take(1))
      .subscribe({
        next: (tag) => {
          this.tag = tag;
          this.updateTitle();
        },
      });
  }

  private loadServices(tag: string): void {
    this.citizenServiceService
      .getCitizenServices(tag)
      .pipe(
        take(1),
        finalize(() => {
          this.loadingCitizenServices = false;
        }),
      )
      .subscribe({
        next: (services) => {
          this.citizenServices = services;
        },
        error: (err) => console.error(err),
      });
  }

  private resetState() {
    this.tag = null;
    this.citizenServices = [];
    this.loadingCitizenServices = true;
  }

  private updateTitle(): void {
    if (this.tag) {
      this.titleService.setTitle(
        this.translateLocalization.transform(this.tag.localizedNames),
      );
    }
  }
}
