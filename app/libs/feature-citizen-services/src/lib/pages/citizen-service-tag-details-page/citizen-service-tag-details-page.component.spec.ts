import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { CitizenServiceService } from '../../services/citizen-service.service';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../transloco-testing.module';
import { CoreTestModule } from '@sw-code/urbo-core';
import { ActivatedRoute } from '@angular/router';
import {
  mockCitizenService,
  mockTag,
} from '../../shared/testing/citizen-services-test-utils';
import { of, throwError } from 'rxjs';
import { CategoryDetailsPageComponent } from './citizen-service-tag-details-page.component';

describe('CategoryDetailsPageComponent', () => {
  let component: CategoryDetailsPageComponent;
  let fixture: ComponentFixture<CategoryDetailsPageComponent>;
  let mockCitizenServiceService: jest.Mocked<CitizenServiceService>;
  let mockActivatedRoute;

  beforeEach(waitForAsync(() => {
    mockCitizenServiceService = {
      getCitizenServices: jest.fn(),
      getById: jest.fn(),
      getTagById: jest.fn(),
    } as unknown as jest.Mocked<CitizenServiceService>;

    mockActivatedRoute = {
      snapshot: {
        paramMap: {
          get: jest.fn().mockReturnValue('test-id'),
        },
        queryParams: { title: 'test-title' },
      },
    };

    TestBed.configureTestingModule({
      imports: [
        CategoryDetailsPageComponent,
        UITestModule.forRoot(),
        getTranslocoModule(),
        CoreTestModule.forRoot(),
      ],
      providers: [
        { provide: CitizenServiceService, useValue: mockCitizenServiceService },
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CategoryDetailsPageComponent);
    component = fixture.componentInstance;
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize and load services', () => {
    const mockServices = [mockCitizenService(), mockCitizenService()];
    mockCitizenServiceService.getCitizenServices.mockReturnValue(
      of(mockServices),
    );
    mockCitizenServiceService.getTagById.mockReturnValue(
      of(mockTag({ id: 'test-id' })),
    );

    fixture.detectChanges();

    expect(component.tagId).toBe('test-id');
    expect(mockCitizenServiceService.getCitizenServices).toHaveBeenCalled();
    expect(component.citizenServices).toEqual(mockServices);
  });

  it('should handle errors when loading services', () => {
    const consoleErrorSpy = jest
      .spyOn(console, 'error')
      .mockImplementation(jest.fn());
    mockCitizenServiceService.getCitizenServices.mockReturnValue(
      throwError(() => new Error('Error')),
    );
    mockCitizenServiceService.getTagById.mockReturnValue(
      of(mockTag({ id: 'test-id' })),
    );

    fixture.detectChanges();

    expect(consoleErrorSpy).toHaveBeenCalledWith(new Error('Error'));
  });
});
