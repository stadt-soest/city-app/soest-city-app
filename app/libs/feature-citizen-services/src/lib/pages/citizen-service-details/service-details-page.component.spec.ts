import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { ServiceDetailsPageComponent } from './service-details-page.component';
import { CitizenServiceService } from '../../services/citizen-service.service';
import {
  BrowserService,
  CoreTestModule,
  DeviceService,
} from '@sw-code/urbo-core';
import { mockCitizenService } from '../../shared/testing/citizen-services-test-utils';
import { getTranslocoModule } from '../../../transloco-testing.module';
import { CitizenServiceShareService } from '../../services/share/citizen-service-share.service';

describe('ServiceDetailsPage', () => {
  let component: ServiceDetailsPageComponent;
  let fixture: ComponentFixture<ServiceDetailsPageComponent>;
  let mockCitizenServiceService: jest.Mocked<CitizenServiceService>;
  let mockDeviceService: jest.Mocked<DeviceService>;

  beforeEach(async () => {
    mockCitizenServiceService = {
      getById: jest.fn().mockReturnValue(of(mockCitizenService())),
    } as any;

    mockDeviceService = {
      isWebPlatform: jest.fn().mockReturnValue(false),
      isNativePlatform: jest.fn().mockReturnValue(true),
    } as any;

    const mockActivatedRoute = {
      snapshot: {
        paramMap: {
          get: jest.fn().mockReturnValue('mock-id'),
        },
      },
    };

    await TestBed.configureTestingModule({
      imports: [
        ServiceDetailsPageComponent,
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        { provide: CitizenServiceService, useValue: mockCitizenServiceService },
        { provide: DeviceService, useValue: mockDeviceService },
        BrowserService,
        CitizenServiceShareService,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ServiceDetailsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create and load service details', () => {
    expect(component).toBeTruthy();
    expect(mockCitizenServiceService.getById).toHaveBeenCalledWith('mock-id');
  });

  it('should call openUrl on BrowserService with the correct URL', () => {
    const spy = jest.spyOn(TestBed.inject(BrowserService), 'openInAppBrowser');
    const mockService = mockCitizenService();
    const mockEvent = {
      preventDefault: jest.fn(),
    } as unknown as MouseEvent;

    component.citizenService = mockService;

    component.openService(mockEvent, mockService);

    expect(mockEvent.preventDefault).toHaveBeenCalled();
    expect(spy).toHaveBeenCalledWith(mockService.url);
  });

  it('should call openInBrowser on BrowserService', () => {
    const spy = jest.spyOn(
      TestBed.inject(BrowserService),
      'openInExternalBrowser',
    );
    component.citizenService = mockCitizenService();

    component.openInBrowser();

    expect(spy).toHaveBeenCalledWith(component.citizenService?.url);
  });

  it('should call shareCitizenService on CitizenServiceShareService', () => {
    const spy = jest.spyOn(
      TestBed.inject(CitizenServiceShareService),
      'shareCitizenService',
    );
    component.citizenService = mockCitizenService();

    component.shareCitizenService();

    expect(spy).toHaveBeenCalledWith(component.citizenService);
  });
});
