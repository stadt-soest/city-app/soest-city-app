import { Component, inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslocoDirective, TranslocoPipe } from '@jsverse/transloco';
import {
  ActionButtonBarComponent,
  CondensedHeaderLayoutComponent,
  ConnectivityErrorCardComponent,
  CustomButtonColorScheme,
  CustomButtonType,
  CustomVerticalButtonComponent,
} from '@sw-code/urbo-ui';
import { IonCol, IonGrid, IonRow } from '@ionic/angular/standalone';
import { NgIf } from '@angular/common';
import { CitizenService } from '../../models/citizen-service.model';
import { CitizenServiceShareService } from '../../services/share/citizen-service-share.service';
import {
  BrowserService,
  ConnectivityService,
  DeviceService,
  TitleService,
} from '@sw-code/urbo-core';
import { CitizenServiceService } from '../../services/citizen-service.service';
import { take } from 'rxjs';

@Component({
  selector: 'lib-service-details',
  templateUrl: './service-details-page.component.html',
  styleUrls: ['./service-details-page.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    IonGrid,
    NgIf,
    IonCol,
    ConnectivityErrorCardComponent,
    CustomVerticalButtonComponent,
    IonRow,
    ActionButtonBarComponent,
    TranslocoPipe,
  ],
})
export class ServiceDetailsPageComponent implements OnInit {
  citizenService?: CitizenService;
  serviceId: string | null;
  isConnected = true;
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
  private readonly route = inject(ActivatedRoute);
  private readonly shareService = inject(CitizenServiceShareService);
  private readonly browserService = inject(BrowserService);
  private readonly citizenServiceService = inject(CitizenServiceService);
  private readonly connectivityService = inject(ConnectivityService);
  private readonly deviceService = inject(DeviceService);
  private readonly titleService = inject(TitleService);

  constructor() {
    this.serviceId = this.route.snapshot.paramMap.get('id');
  }

  get pageTitleKey() {
    if (!this.isConnected) {
      return 'ui.error-page-title.no_signal';
    } else {
      return '';
    }
  }

  ngOnInit(): void {
    this.setOnlineStatus();
    if (this.serviceId) {
      this.loadServiceDetails(this.serviceId);
    }
  }

  async openInBrowser() {
    if (this.citizenService) {
      await this.browserService.openInExternalBrowser(this.citizenService.url);
    }
  }

  async openService(
    event: MouseEvent | KeyboardEvent,
    citizenService: CitizenService,
  ) {
    event.preventDefault();
    if (this.deviceService.isWebPlatform()) {
      await this.browserService.openInExternalBrowser(citizenService.url);
    }
    await this.browserService.openInAppBrowser(citizenService.url);
  }

  async shareCitizenService() {
    if (this.citizenService) {
      await this.shareService.shareCitizenService(this.citizenService);
    }
  }

  private async setOnlineStatus() {
    this.isConnected = await this.connectivityService.checkOnlineStatus();
  }

  private loadServiceDetails(serviceId: string): void {
    this.citizenServiceService
      .getById(serviceId)
      .pipe(take(1))
      .subscribe({
        next: (service) => {
          this.citizenService = service;
          this.updateTitle();
        },
        error: (err) => console.error(err),
      });
  }

  private updateTitle(): void {
    if (this.citizenService) {
      this.titleService.setTitle(this.citizenService.title);
    }
  }
}
