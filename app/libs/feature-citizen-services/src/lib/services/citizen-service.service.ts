// citizen-service.service.ts
import { inject, Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CitizenServiceAdapter } from '../citizen-service.adapter';
import {
  CitizenService,
  CitizenServiceFeedItem,
  Tag,
} from '../models/citizen-service.model';

@Injectable()
export class CitizenServiceService {
  private readonly citizenServiceAdapter = inject(CitizenServiceAdapter);

  getCitizenServices(
    tagId?: string,
    favorite?: boolean,
  ): Observable<CitizenService[]> {
    return this.citizenServiceAdapter.getCitizenServices(tagId, favorite).pipe(
      catchError((error) => {
        this.handleError(error);
        return throwError(() => error);
      }),
    );
  }

  getTags(): Observable<Tag[]> {
    return this.citizenServiceAdapter.getTags().pipe(
      catchError((error) => {
        this.handleError(error);
        return throwError(() => error);
      }),
    );
  }

  getTagById(id: string): Observable<Tag> {
    return this.citizenServiceAdapter.getTagById(id).pipe(
      catchError((error) => {
        this.handleError(error);
        return throwError(() => error);
      }),
    );
  }

  getById(id: string): Observable<CitizenService> {
    return this.citizenServiceAdapter.getById(id).pipe(
      catchError((error) => {
        this.handleError(error);
        return throwError(() => error);
      }),
    );
  }

  getBySearchTerm(
    searchTerm: string,
    page?: number,
    size?: number,
  ): Observable<{ total: number; items: CitizenServiceFeedItem[] }> {
    const pageNumber = page ?? 1;
    const pageSize = size ?? 10;

    return this.citizenServiceAdapter
      .getBySearchTerm(searchTerm, pageNumber, pageSize)
      .pipe(
        catchError((error) => {
          this.handleError(error);
          return throwError(() => error);
        }),
      );
  }

  private handleError(error: any) {
    console.error('An error occurred:', error);
  }
}
