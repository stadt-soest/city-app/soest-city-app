import { inject, Injectable } from '@angular/core';
import { TranslocoService } from '@jsverse/transloco';
import { BaseShareService } from '@sw-code/urbo-core';
import { CitizenService } from '../../models/citizen-service.model';

@Injectable()
export class CitizenServiceShareService {
  private readonly baseShareService = inject(BaseShareService);
  private readonly translateService = inject(TranslocoService);

  async shareCitizenService(citizenService: CitizenService): Promise<void> {
    const title = citizenService.title;
    const url = window.location.href;
    const text = this.translateService.translate(
      'feature_citizen_services.details_page.share.content',
      { title, url },
    );

    await this.baseShareService.share(title, text, url);
  }
}
