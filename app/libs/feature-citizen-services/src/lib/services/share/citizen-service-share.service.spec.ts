import { CitizenServiceShareService } from './citizen-service-share.service';
import { TranslocoService } from '@jsverse/transloco';
import { TestBed } from '@angular/core/testing';
import { BaseShareService } from '@sw-code/urbo-core';
import { mockCitizenService } from '../../shared/testing/citizen-services-test-utils';

describe('CitizenServiceShareService', () => {
  let service: CitizenServiceShareService;
  let mockBaseShareService: jest.Mocked<BaseShareService>;
  let mockTranslocoService: jest.Mocked<TranslocoService>;

  beforeEach(() => {
    mockBaseShareService = {
      share: jest.fn(),
    } as unknown as jest.Mocked<BaseShareService>;

    mockTranslocoService = {
      translate: jest.fn(),
    } as unknown as jest.Mocked<TranslocoService>;

    TestBed.configureTestingModule({
      providers: [
        CitizenServiceShareService,
        { provide: BaseShareService, useValue: mockBaseShareService },
        { provide: TranslocoService, useValue: mockTranslocoService },
      ],
    });

    service = TestBed.inject(CitizenServiceShareService);
  });

  it('should call baseShareService.share with the correct arguments', async () => {
    const citizenService = mockCitizenService();

    const translatedText = 'Share this service';
    mockTranslocoService.translate.mockReturnValue(translatedText);

    await service.shareCitizenService(citizenService);

    expect(mockBaseShareService.share).toHaveBeenCalledWith(
      citizenService.title,
      translatedText,
      window.location.href,
    );
  });

  it('should use the translation service to get share text', async () => {
    const citizenService = mockCitizenService();

    const translatedText = 'Share this service';
    mockTranslocoService.translate.mockReturnValue(translatedText);

    await service.shareCitizenService(citizenService);

    expect(mockTranslocoService.translate).toHaveBeenCalledWith(
      'feature_citizen_services.details_page.share.content',
      {
        title: 'Service 1',
        url: 'http://localhost/',
      },
    );
  });
});
