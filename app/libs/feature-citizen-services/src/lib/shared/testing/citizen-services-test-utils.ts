import { ModuleCategory, ModuleInfo } from '@sw-code/urbo-core';
import { CitizenService, Tag } from '../../models/citizen-service.model';

export const mockCitizenServicesModuleInfo: ModuleInfo = {
  id: 'citizen-services',
  name: 'Bürgerbüro Online',
  category: ModuleCategory.yourCity,
  icon: 'contract',
  baseRoutingPath: '/citizen-services',
  defaultVisibility: true,
  settingsPageAvailable: false,
  isAboutAppItself: false,
  relevancy: 0.5,
  forYouRelevant: false,
  showInCategoriesPage: false,
  orderInCategory: 1,
};

export const mockCitizenService = (
  overrides?: Partial<CitizenService>,
): CitizenService =>
  ({
    id: '1',
    title: 'Service 1',
    description: 'Description 1',
    url: 'https://example.com/service1',
    ...overrides,
  }) as CitizenService;

export const mockTag = (overrides?: Partial<Tag>): Tag =>
  ({
    id: '1',
    localizedNames: [
      {
        language: 'EN',
        value: 'Tag 1',
      },
      {
        language: 'DE',
        value: 'Tag 1',
      },
    ],
    ...overrides,
  }) as Tag;
