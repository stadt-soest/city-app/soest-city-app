import { Component, Input } from '@angular/core';
import { NgFor, NgIf } from '@angular/common';
import { CitizenServiceMenuSkeletonComponent } from '../citizen-service-menu-skeleton/citizen-service-menu-skeleton.component';
import { IonList, IonRouterLinkWithHref } from '@ionic/angular/standalone';
import { RouterLink } from '@angular/router';
import {
  GeneralIconComponent,
  IconColor,
  IconFontSize,
  IconType,
  TranslateLocalizationPipe,
} from '@sw-code/urbo-ui';
import { Tag } from '../../models/citizen-service.model';

@Component({
  selector: 'lib-category-folder-list',
  templateUrl: './tag-folder-list.component.html',
  styleUrls: ['./tag-folder-list.component.scss'],
  imports: [
    NgIf,
    CitizenServiceMenuSkeletonComponent,
    IonList,
    NgFor,
    IonRouterLinkWithHref,
    RouterLink,
    GeneralIconComponent,
    TranslateLocalizationPipe,
  ],
})
export class TagFolderListComponent {
  @Input({ required: true }) tags: Tag[] = [];
  @Input({ required: true }) loading = false;
  protected readonly iconType = IconType;
  protected readonly iconFontSize = IconFontSize;
  protected readonly iconColor = IconColor;
}
