import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TagFolderListComponent } from './tag-folder-list.component';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '@sw-code/urbo-ui';
import { Tag } from '../../models/citizen-service.model';

describe('CategoryFolderListComponent', () => {
  let component: TagFolderListComponent;
  let fixture: ComponentFixture<TagFolderListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        TagFolderListComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(TagFolderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render each category passed as input', () => {
    const categories: Tag[] = [
      {
        id: '1',
        localizedNames: [{ language: 'EN', value: 'Category 1' }],
      },
      {
        id: '2',
        localizedNames: [{ language: 'EN', value: 'Category 1' }],
      },
      {
        id: '3',
        localizedNames: [{ language: 'EN', value: 'Category 1' }],
      },
    ];
    component.tags = categories;
    fixture.detectChanges();

    const categoryElements: NodeListOf<HTMLElement> =
      fixture.nativeElement.querySelectorAll('.item-title');
    expect(categoryElements.length).toBe(categories.length);
    categoryElements.forEach((element: HTMLElement, index: number) => {
      expect(element.textContent?.trim()).toBe(
        categories[index].localizedNames[0].value,
      );
    });
  });
});
