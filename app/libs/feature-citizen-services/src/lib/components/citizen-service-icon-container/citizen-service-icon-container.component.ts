import { Component, Input } from '@angular/core';
import {
  GeneralIconComponent,
  IconColor,
  IconFontSize,
  IconType,
} from '@sw-code/urbo-ui';
import { CitizenService } from '../../models/citizen-service.model';

@Component({
  selector: 'lib-citizen-service-icon',
  templateUrl: './citizen-service-icon-container.component.html',
  styleUrls: ['citizen-service-icon-container.component.scss'],
  imports: [GeneralIconComponent],
})
export class CitizenServiceIconContainerComponent {
  @Input({ required: true }) title!: string;
  @Input() item?: CitizenService;
  protected readonly iconType = IconType;
  protected readonly iconFontSize = IconFontSize;
  protected readonly iconColor = IconColor;
}
