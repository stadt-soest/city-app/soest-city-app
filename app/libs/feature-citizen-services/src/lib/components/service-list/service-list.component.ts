import { Component, Input } from '@angular/core';
import { NgFor, NgIf } from '@angular/common';
import { CitizenServiceMenuSkeletonComponent } from '../citizen-service-menu-skeleton/citizen-service-menu-skeleton.component';
import { IonList, IonRouterLinkWithHref } from '@ionic/angular/standalone';
import { RouterLink } from '@angular/router';
import { CitizenServiceIconContainerComponent } from '../citizen-service-icon-container/citizen-service-icon-container.component';
import { CitizenService } from '../../models/citizen-service.model';

@Component({
  selector: 'lib-service-list',
  templateUrl: './service-list.component.html',
  imports: [
    NgIf,
    CitizenServiceMenuSkeletonComponent,
    IonList,
    NgFor,
    IonRouterLinkWithHref,
    RouterLink,
    CitizenServiceIconContainerComponent,
  ],
})
export class ServiceListComponent {
  @Input({ required: true }) services: CitizenService[] = [];
  @Input({ required: true }) loading = false;
}
