import { Component, Input } from '@angular/core';
import { IonList, IonSkeletonText } from '@ionic/angular/standalone';
import { NgFor } from '@angular/common';

@Component({
  selector: 'lib-citizen-service-menu-skeleton',
  templateUrl: './citizen-service-menu-skeleton.component.html',
  styleUrls: ['citizen-service-menu-skeleton.component.scss'],
  imports: [IonList, NgFor, IonSkeletonText],
})
export class CitizenServiceMenuSkeletonComponent {
  @Input({ required: true }) skeletonCount!: number;
}
