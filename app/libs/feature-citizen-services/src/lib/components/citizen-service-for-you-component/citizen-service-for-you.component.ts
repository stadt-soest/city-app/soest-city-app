import { Component } from '@angular/core';
import { IonRouterLinkWithHref } from '@ionic/angular/standalone';
import { RouterLink } from '@angular/router';
import { CitizenServiceIconContainerComponent } from '../citizen-service-icon-container/citizen-service-icon-container.component';
import { CitizenServiceFeedItem } from '../../models/citizen-service.model';
import { AbstractFeedCardComponent } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-citizen-service-for-you',
  template: `
    <a [routerLink]="['.', feedItem.id]" [href]="'.' + feedItem.id">
      <lib-citizen-service-icon
        [title]="feedItem.title"
        lang="de"
        translate="no"
      ></lib-citizen-service-icon>
    </a>
  `,
  imports: [
    IonRouterLinkWithHref,
    RouterLink,
    CitizenServiceIconContainerComponent,
  ],
})
export class CitizenServiceForYouComponent extends AbstractFeedCardComponent<CitizenServiceFeedItem> {}
