import { Component, inject } from '@angular/core';
import { provideTranslocoScope, TranslocoDirective } from '@jsverse/transloco';
import { OnboardingButton, OnboardingComponent } from '@sw-code/urbo-ui';
import { ModuleInfo, OnboardingService } from '@sw-code/urbo-core';
import { CITIZEN_SERVICES_MODULE_INFO } from '../../feature-citizen-services.module';

@Component({
  selector: 'lib-citizen-services-onboarding-card',
  template: `
    <lib-onboarding
      *transloco="let t"
      [imageUrl]="imageUrl"
      [title]="t(title)"
      [content]="t(content)"
      [moduleInfo]="citizenServiceModuleInfo"
      [buttons]="buttons"
    >
    </lib-onboarding>
  `,
  imports: [TranslocoDirective, OnboardingComponent],
  providers: [
    provideTranslocoScope({
      scope: 'feature-citizen-services',
      alias: 'feature_citizen_services',
      loader: {
        en: () => import('../../i18n/en.json'),
        de: () => import('../../i18n/de.json'),
      },
    }),
  ],
})
export class CitizenServiceOnboardingComponent {
  imageUrl = 'assets/illustrations/citizen_services.svg';
  title = 'feature_citizen_services.onboarding.title';
  content = 'feature_citizen_services.onboarding.content';
  buttons: OnboardingButton[];
  citizenServiceModuleInfo = inject<ModuleInfo>(CITIZEN_SERVICES_MODULE_INFO);
  private readonly onboardingService = inject(OnboardingService);

  constructor() {
    this.buttons = [
      {
        icon: 'visibility_off',
        text: 'feature_citizen_services.later',
        action: () =>
          this.onboardingService.navigateToNextSlide(
            this.citizenServiceModuleInfo,
            'later',
          ),
        selected: !this.citizenServiceModuleInfo.defaultVisibility,
        shouldApplySelectionStyle: true,
        id: 'visibility-off-button',
      },
      {
        icon: 'favorite_border',
        text: 'feature_citizen_services.activate',
        action: () =>
          this.onboardingService.navigateToNextSlide(
            this.citizenServiceModuleInfo,
            'activate',
          ),
        selected: this.citizenServiceModuleInfo.defaultVisibility,
        shouldApplySelectionStyle: true,
        id: 'visibility-on-button',
      },
    ];
  }
}
