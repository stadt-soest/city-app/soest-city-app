import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { CitizenServiceOnboardingComponent } from './citizen-service-onboarding.component';
import { UITestModule } from '@sw-code/urbo-ui';
import { CoreTestModule } from '@sw-code/urbo-core';
import { getTranslocoModule } from '../../../transloco-testing.module';
import { CITIZEN_SERVICES_MODULE_INFO } from '../../feature-citizen-services.module';
import { mockCitizenServicesModuleInfo } from '../../shared/testing/citizen-services-test-utils';

describe('CitizenServiceOnboardingComponent', () => {
  let component: CitizenServiceOnboardingComponent;
  let fixture: ComponentFixture<CitizenServiceOnboardingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        CitizenServiceOnboardingComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        {
          provide: CITIZEN_SERVICES_MODULE_INFO,
          useValue: mockCitizenServicesModuleInfo,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CitizenServiceOnboardingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
