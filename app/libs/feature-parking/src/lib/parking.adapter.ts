import { Observable } from 'rxjs';
import { Parking } from './models/parking.model';
import { ParkingFeedItem } from './models/parking-feed-item';
import { MapIntegration, ModuleInfo } from '@sw-code/urbo-core';

export abstract class ParkingAdapter {
  abstract getParkingAreas(): Observable<Parking[]>;

  abstract getParkingAreaById(id: string): Observable<Parking>;

  abstract getFavoriteParkingFeedables(
    favoriteIds: Set<string>,
  ): Observable<ParkingFeedItem[]>;

  abstract getParkingPois(moduleInfo: ModuleInfo): Observable<MapIntegration>;
}
