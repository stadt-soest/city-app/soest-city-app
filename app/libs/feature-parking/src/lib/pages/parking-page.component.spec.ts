import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ParkingPageComponent } from './parking-page.component';
import {
  ParkingAreaCategory,
  ParkingWithDistance,
} from '../models/parking.model';
import { firstValueFrom, of } from 'rxjs';
import { createMockParking } from '../shared/testing/parking-test-utils';
import {
  CoreTestModule,
  DeviceService,
  GeolocationService,
  SimplifiedPermissionState,
  StorageService,
} from '@sw-code/urbo-core';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../transloco-testing.module';
import { ParkingService } from '../services/parking.service';
import { PARKING_CONFIG } from '../parking-tokens';

const mockCategorizedParkingData$ = of({
  data: new Map<ParkingAreaCategory, ParkingWithDistance[]>([
    [
      'favorite',
      [new ParkingWithDistance(createMockParking({ id: 'fav1' }), 100)],
    ],
    [
      'close',
      [new ParkingWithDistance(createMockParking({ id: 'close1' }), 200)],
    ],
    [
      'free',
      [new ParkingWithDistance(createMockParking({ id: 'free1' }), 300)],
    ],
  ]),
  loading: false,
});

const parkingServiceMock = {
  categorizedParkingData$: mockCategorizedParkingData$,
  loadAndCategorizeParkingData: jest.fn(),
};

describe('ParkingPage', () => {
  let component: ParkingPageComponent;
  let fixture: ComponentFixture<ParkingPageComponent>;
  let mockGeolocationService: Partial<GeolocationService>;
  let mockDeviceService: Partial<DeviceService>;

  beforeEach(async () => {
    mockGeolocationService = {
      permissionState$: of(SimplifiedPermissionState.granted),
    };

    mockDeviceService = {
      isNativePlatform: jest.fn(),
    };

    await TestBed.configureTestingModule({
      imports: [
        ParkingPageComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        { provide: ParkingService, useValue: parkingServiceMock },
        { provide: GeolocationService, useValue: mockGeolocationService },
        { provide: DeviceService, useValue: mockDeviceService },
        { provide: PARKING_CONFIG, useValue: {} },
        {
          provide: StorageService,
          useValue: {
            get: jest.fn(),
            getKeys: jest.fn(),
            set: jest.fn(),
            remove: jest.fn(),
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ParkingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should return favorite parking areas', async () => {
    const result = await firstValueFrom(mockCategorizedParkingData$);
    const parkingDataMap = result.data;

    const favorites = component.getParkingAreasByCategory(
      'favorite',
      parkingDataMap,
    );
    expect(favorites.length).toBe(1);
    expect(favorites[0].area.id).toBe('fav1');
  });

  describe('showPermissionCard', () => {
    it('should not render app-notification-card when permissionState is granted', () => {
      mockDeviceService.isNativePlatform = jest.fn().mockReturnValue(true);
      fixture.detectChanges();

      const notificationCard = fixture.nativeElement.querySelector(
        'lib-notification-card',
      );
      expect(notificationCard).toBeFalsy();
    });
  });
});
