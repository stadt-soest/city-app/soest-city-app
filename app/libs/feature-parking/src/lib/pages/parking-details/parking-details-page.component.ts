import { Component, inject, Input, OnInit } from '@angular/core';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import {
  IonContent,
  IonGrid,
  IonHeader,
  IonSkeletonText,
  ModalController,
} from '@ionic/angular/standalone';
import {
  CondensedHeaderLayoutComponent,
  ConnectivityErrorCardComponent,
  CustomModalHeaderComponent,
  CustomSelectableIconTextButtonComponent,
  DistanceFormatPipe,
  GeneralIconComponent,
  IconFontSize,
  IconType,
  OpeningTimesComponent,
} from '@sw-code/urbo-ui';
import { AsyncPipe, NgClass, NgIf } from '@angular/common';
import { BadgeContainerComponent } from '../../components/badge-container/badge-container.component';
import {
  Category,
  FeedUpdateService,
  MapNavigationService,
  MapsNavigationService,
  NavService,
  PageType,
  StorageService,
  TitleService,
} from '@sw-code/urbo-core';
import {
  firstValueFrom,
  from,
  map,
  Observable,
  of,
  switchMap,
  take,
  tap,
} from 'rxjs';
import { ParkingWithDistance } from '../../models/parking.model';
import { ActivatedRoute } from '@angular/router';
import { ParkingService } from '../../services/parking.service';
import { PARKING_FAVORITES_KEY } from '../../feature-parking.module';
import { ParkingPageComponent } from '../parking-page.component';

@Component({
  selector: 'lib-parking-details',
  templateUrl: './parking-details-page.component.html',
  styleUrls: ['./parking-details-page.component.scss'],
  imports: [
    TranslocoDirective,
    IonHeader,
    CustomModalHeaderComponent,
    IonContent,
    IonGrid,
    NgIf,
    ConnectivityErrorCardComponent,
    IonSkeletonText,
    GeneralIconComponent,
    BadgeContainerComponent,
    CustomSelectableIconTextButtonComponent,
    NgClass,
    CondensedHeaderLayoutComponent,
    AsyncPipe,
    DistanceFormatPipe,
    OpeningTimesComponent,
  ],
})
export class ParkingDetailsPageComponent implements OnInit {
  @Input({ required: true }) poiId = '';
  @Input({ required: true }) categoryName = '';
  @Input({ required: true }) categories: Category[] = [];
  @Input({ required: true }) categoryId = '';
  parkingId$!: Observable<string>;
  parking$!: Observable<ParkingWithDistance | null>;
  isFavoriteParking$!: Observable<boolean>;
  error = false;
  currentLanguage = 'de';

  protected readonly iconType = IconType;
  protected readonly iconFontSize = IconFontSize;
  private parking?: ParkingWithDistance | null;
  private readonly route = inject(ActivatedRoute);
  private readonly feedUpdateService = inject(FeedUpdateService);
  private readonly parkingService = inject(ParkingService);
  private readonly mapsNavigationService = inject(MapsNavigationService);
  private readonly storageService = inject(StorageService);
  private readonly translocoService = inject(TranslocoService);
  private readonly titleService = inject(TitleService);
  private readonly mapNavigationService = inject(MapNavigationService);
  private readonly modalCtrl = inject(ModalController);
  private readonly navService = inject(NavService);

  get pageTitleKey() {
    if (this.error) {
      return 'ui.error-page-title.no_signal';
    } else {
      return '';
    }
  }

  ngOnInit(): void {
    this.parkingId$ = of(
      (this.poiId?.trim() !== '' ? this.poiId : null) ??
        this.route.snapshot.paramMap.get('id') ??
        '',
    );
    this.parking$ = this.parkingId$.pipe(
      switchMap((id) =>
        id ? this.parkingService.getParkingById(id) : of(null),
      ),
      tap((parking) => {
        if (parking) {
          this.parking = parking;
          Promise.resolve().then(() => {
            this.mapNavigationService.setIsOnCategoryDetailsPage(true);
            this.mapNavigationService.setIsOnCategoryListPage(false);
          });
          this.mapNavigationService.setUpdateMap(
            this.categoryId,
            this.poiId,
            [PageType.poiDetails],
            PageType.poiDetails,
            parking?.area?.displayName ?? '',
          );
          this.titleService.setTitle(parking.area.displayName);
        }
      }),
      tap({
        error: () => {
          this.error = true;
          console.error('Failed to fetch parking details');
        },
      }),
    );

    this.translocoService.langChanges$.subscribe(
      (lang) => (this.currentLanguage = lang),
    );

    this.isFavoriteParking$ = this.parking$.pipe(
      switchMap((parking) =>
        parking ? this.loadFavoriteState$(parking.area.id) : of(false),
      ),
    );
  }

  toggleFavorite(): void {
    this.parking$
      .pipe(
        take(1),
        switchMap((parking) => {
          if (!parking) {
            return of(null);
          }

          return from(this.storageService.get(PARKING_FAVORITES_KEY)).pipe(
            switchMap((favoritesJson) => {
              let favorites: string[] = favoritesJson
                ? JSON.parse(favoritesJson)
                : [];
              const isFavorite = favorites.includes(parking.area.id);

              if (isFavorite) {
                favorites = favorites.filter((id) => id !== parking.area.id);
              } else {
                favorites.push(parking.area.id);
              }

              return from(
                this.storageService.set(
                  PARKING_FAVORITES_KEY,
                  JSON.stringify(favorites),
                ),
              ).pipe(
                tap(() => (this.isFavoriteParking$ = of(!isFavorite))),
                tap(() => this.feedUpdateService.notifyFeedUpdate()),
              );
            }),
          );
        }),
      )
      .subscribe({
        error: (err) => console.error('Failed to update favorite status:', err),
      });
  }

  async pop() {
    this.mapNavigationService.setIsOnCategoryDetailsPage(false);
    this.mapNavigationService.setUpdateMap(
      this.categoryId,
      this.poiId,
      [PageType.poiList],
      PageType.poiList,
      this.parking?.area.displayName ?? '',
    );

    this.mapNavigationService.setUpdateMap(
      this.categoryId,
      this.poiId,
      [PageType.poiList],
      PageType.poiList,
      this.parking?.area.displayName ?? '',
    );

    if (await this.navService.canGoBack()) {
      await this.navService.popPage();
    } else {
      await this.navService.pushPage(
        ParkingPageComponent,
        {
          fromMap: true,
          categoryId: this.categoryId,
          categories: this.categories,
          categoryName: this.categoryName,
        },
        {
          animated: true,
          direction: 'back',
        },
      );
    }
  }

  async openMaps(): Promise<void> {
    if (this.parking) {
      await this.mapsNavigationService.navigateToLocation(
        this.parking.area.coordinates.lat,
        this.parking.area.coordinates.long,
      );
    }
  }

  async close() {
    const parking = await firstValueFrom(this.parking$);
    this.mapNavigationService.setIsOnCategoryDetailsPage(false);
    await this.modalCtrl.dismiss({
      categoryId: this.categoryId,
      categoryDetailsId: this.poiId,
      selectedCategories: [this.categoryName],
      pageType: PageType.poiDetails,
      updateMarkers: true,
      categoryName: parking?.area.displayName ?? '',
    });
  }

  private loadFavoriteState$(parkingId: string): Observable<boolean> {
    return from(this.storageService.get(PARKING_FAVORITES_KEY)).pipe(
      map((favoritesJson) => {
        const favorites: string[] = favoritesJson
          ? JSON.parse(favoritesJson)
          : [];
        return favorites.includes(parkingId);
      }),
    );
  }
}
