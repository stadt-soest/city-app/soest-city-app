import { Component, DestroyRef, inject, Input, OnInit } from '@angular/core';
import {
  TranslocoDirective,
  TranslocoPipe,
  TranslocoService,
} from '@jsverse/transloco';
import {
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonRow,
  IonText,
  ModalController,
} from '@ionic/angular/standalone';
import {
  CondensedHeaderLayoutComponent,
  ConnectivityErrorCardComponent,
  CustomButtonType,
  CustomModalHeaderComponent,
  CustomSelectableIconTextButton,
  LocalizedTimeDistancePipePipe,
  NotificationCardComponent,
  RefresherComponent,
} from '@sw-code/urbo-ui';
import { AsyncPipe, NgFor, NgIf } from '@angular/common';
import { ParkingCardComponent } from '../components/parking-card/parking-card.component';
import { ParkingFeedCardSkeletonComponent } from '../components/parking-feed-card-skeleton/parking-feed-card-skeleton.component';
import {
  Category,
  DeviceService,
  FeedUpdateService,
  GeolocationService,
  MapNavigationService,
  NavService,
  PageType,
  SimplifiedPermissionState,
} from '@sw-code/urbo-core';
import {
  ParkingAreaCategory,
  ParkingWithDistance,
} from '../models/parking.model';
import { firstValueFrom, interval, Observable, take } from 'rxjs';
import { ParkingService } from '../services/parking.service';
import { PARKING_CONFIG } from '../parking-tokens';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'lib-parking',
  templateUrl: './parking-page.component.html',
  styleUrls: ['./parking-page.component.scss'],
  imports: [
    TranslocoDirective,
    IonHeader,
    CustomModalHeaderComponent,
    IonContent,
    NgIf,
    IonGrid,
    IonText,
    ConnectivityErrorCardComponent,
    IonCol,
    NotificationCardComponent,
    NgFor,
    IonRow,
    ParkingCardComponent,
    ParkingFeedCardSkeletonComponent,
    CondensedHeaderLayoutComponent,
    RefresherComponent,
    AsyncPipe,
    TranslocoPipe,
    LocalizedTimeDistancePipePipe,
  ],
})
export class ParkingPageComponent implements OnInit {
  @Input() fromMap = false;
  @Input() categoryId = '';
  @Input() categoryName = '';
  @Input() categories: Category[] = [];
  parkingCategories: ParkingAreaCategory[] = [
    'favorite',
    'close',
    'further',
    'free',
    'notAvailable',
  ];
  categorizedParkingData$!: Observable<{
    data: Map<ParkingAreaCategory, ParkingWithDistance[]>;
    loading: boolean;
  }>;
  displayConnectivityError = false;
  lastUpdated: Date | null = null;
  title = '';
  protected readonly simplifiedPermissionState = SimplifiedPermissionState;
  protected readonly customButtonType = CustomButtonType;
  protected deviceService = inject(DeviceService);
  protected geolocationService = inject(GeolocationService);
  permissionButton: CustomSelectableIconTextButton[] = [
    {
      icon: 'open_in_new',
      text: 'feature_parking.dashboard.open',
      selected: false,
      applySelectionStyle: false,
      onSelect: async () => await this.geolocationService.requestPermissions(),
    },
  ];
  private readonly parkingService = inject(ParkingService);
  private readonly feedUpdateService = inject(FeedUpdateService);
  private readonly modalCtrl = inject(ModalController);
  private readonly mapNavigationService = inject(MapNavigationService);
  private readonly navService = inject(NavService);
  private readonly translateService = inject(TranslocoService);
  private readonly destroyRef = inject(DestroyRef);
  private readonly parkingConfig = inject(PARKING_CONFIG);

  async ngOnInit(): Promise<void> {
    this.categorizedParkingData$ = this.parkingService.categorizedParkingData$;
    this.parkingService.loadAndCategorizeParkingData();
    this.title = await firstValueFrom(
      this.translateService.selectTranslate(
        'module_name',
        {},
        { scope: 'feature-parking' },
      ),
    );
    this.setModuleUpdateCategoryName(this.title);
    this.setupPermissionStateSubscription();
    this.setupTimeUpdate();
    this.setupParkingUpdateSubscription();
    this.mapNavigationService.popPageList$
      .pipe(take(1))
      .subscribe(async () => await this.pop());
    this.updateMap();
  }

  ionViewWillEnter() {
    Promise.resolve().then(() => {
      this.mapNavigationService.setIsOnCategoryListPage(true);
    });
  }

  public async doRefresh(
    event?: CustomEvent<{ complete: () => void }>,
  ): Promise<void> {
    this.parkingService.loadAndCategorizeParkingData();
    event?.detail?.complete();
  }

  async close() {
    this.mapNavigationService.setIsOnCategoryListPage(false);
    const selectedCategory = this.categories.find(
      (category) => category.id === this.categoryId,
    );
    this.setModuleUpdateCategoryName(undefined);
    await this.modalCtrl.dismiss({
      categoryId: this.categoryId,
      selectedCategories: [this.categoryName],
      categoryName: this.title,
      pageType: PageType.poiList,
      updateMarkers: true,
      selectedCategory,
    });
  }

  async pop() {
    this.mapNavigationService.setIsOnCategoryListPage(false);
    this.mapNavigationService.setUpdateMap(
      '',
      '',
      [PageType.filterList],
      PageType.filterList,
      this.title,
    );
    this.setModuleUpdateCategoryName(undefined);

    await this.navService.pushPage(
      this.parkingConfig.initialMapModalComponent,
      {
        categories: this.categories,
      },
      {
        animated: true,
        direction: 'back',
      },
    );
  }

  getParkingAreasByCategory(
    category: ParkingAreaCategory,
    data: Map<ParkingAreaCategory, ParkingWithDistance[]>,
  ): ParkingWithDistance[] {
    return data.get(category) ?? [];
  }

  private setupPermissionStateSubscription(): void {
    this.geolocationService.permissionState$.subscribe((permissionState) => {
      if (permissionState === SimplifiedPermissionState.granted) {
        this.parkingService.loadAndCategorizeParkingData();
      }
    });
  }

  private setupTimeUpdate(): void {
    interval(10000)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(() => {
        if (this.lastUpdated !== null) {
          this.lastUpdated = new Date(this.lastUpdated.getTime());
        }
      });
  }

  private setupParkingUpdateSubscription(): void {
    this.feedUpdateService
      .getFeedUpdatedListener()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(async () => await this.doRefresh());
  }

  private updateMap() {
    if (this.fromMap) {
      Promise.resolve().then(() => {
        this.mapNavigationService.setIsOnCategoryListPage(true);
      });
      this.mapNavigationService.setUpdateMap(
        this.categoryId,
        '',
        [PageType.poiList],
        PageType.poiList,
        this.title,
      );
    }
  }

  private setModuleUpdateCategoryName(categoryName: string | undefined) {
    this.mapNavigationService.setModuleUpdateCategoryName(categoryName);
  }
}
