import { Injectable } from '@angular/core';
import {
  Parking,
  ParkingAreaCategory,
  ParkingStatus,
  ParkingWithDistance,
} from '../models/parking.model';

@Injectable()
export class ParkingSortService {
  categorizeParkingAreas(
    parkingAreas: ParkingWithDistance[],
    favorites: Set<string>,
  ): Map<ParkingAreaCategory, ParkingWithDistance[]> {
    this.sortParkingAreasBeforeCategorization(parkingAreas);

    const categorizedParkingAreas = new Map<
      ParkingAreaCategory,
      ParkingWithDistance[]
    >();
    const categorizedAreas: Record<ParkingAreaCategory, ParkingWithDistance[]> =
      {
        favorite: [],
        close: [],
        further: [],
        free: [],
        notAvailable: [],
      };

    parkingAreas.forEach((area) => {
      if (favorites.has(area.area.id)) {
        categorizedAreas.favorite.push(area);
      } else if (!this.isAvailableArea(area)) {
        categorizedAreas.notAvailable.push(area);
      }
    });

    const availableAreas = parkingAreas.filter(
      (area) => this.isAvailableArea(area) && !favorites.has(area.area.id),
    );

    if (availableAreas.some((area) => area.distance !== undefined)) {
      availableAreas.forEach((area, index) => {
        if (index < 2) {
          categorizedAreas.close.push(area);
        } else if (index >= 2 && index < 4) {
          categorizedAreas.further.push(area);
        } else {
          categorizedAreas.free.push(area);
        }
      });
    } else {
      availableAreas.forEach((area) => categorizedAreas.free.push(area));
    }

    Object.entries(categorizedAreas).forEach(([category, areas]) => {
      categorizedParkingAreas.set(category as ParkingAreaCategory, areas);
    });

    return categorizedParkingAreas;
  }

  categorizeFavorites(
    parkingAreas: Parking[],
    favorites: Set<string>,
  ): ParkingWithDistance[] {
    const parkingWithDistances = parkingAreas.map(
      (area) => new ParkingWithDistance(area),
    );
    const categorized = this.categorizeParkingAreas(
      parkingWithDistances,
      favorites,
    );
    return categorized.get('favorite') || [];
  }

  private isAvailableArea(parkingWithDistance: ParkingWithDistance): boolean {
    const openingHours = parkingWithDistance.area.currentDayOpeningHours;
    const isOpen = openingHours.from && openingHours.to;
    return (
      parkingWithDistance.area.parkingCapacityRemaining > 5 &&
      parkingWithDistance.area.status === ParkingStatus.OPEN &&
      Boolean(isOpen)
    );
  }

  private sortParkingAreasBeforeCategorization(
    parkingAreas: ParkingWithDistance[],
  ): void {
    parkingAreas.sort((a, b) => {
      if (a.distance !== undefined && b.distance !== undefined) {
        return a.distance - b.distance;
      } else {
        return (
          b.area.parkingCapacityRemaining - a.area.parkingCapacityRemaining
        );
      }
    });
  }
}
