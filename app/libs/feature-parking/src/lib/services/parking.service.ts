import { inject, Injectable } from '@angular/core';
import {
  BehaviorSubject,
  catchError,
  combineLatest,
  firstValueFrom,
  from,
  map,
  Observable,
  of,
  switchMap,
  take,
  throwError,
} from 'rxjs';
import { ParkingAdapter } from '../parking.adapter';
import {
  GeolocationService,
  MapIntegration,
  ModuleInfo,
  SimplifiedPermissionState,
  StorageService,
} from '@sw-code/urbo-core';
import { ParkingSortService } from './parking-sort.service';
import {
  PARKING_FAVORITES_KEY,
  PARKING_MODULE_INFO,
} from '../feature-parking.module';
import {
  Parking,
  ParkingAreaCategory,
  ParkingWithDistance,
} from '../models/parking.model';
import { ParkingFeedItem } from '../models/parking-feed-item';
import { getDistance } from 'geolib';

@Injectable()
export class ParkingService {
  private readonly parkingAdapter = inject(ParkingAdapter);
  private readonly geolocationService = inject(GeolocationService);
  private readonly storageService = inject(StorageService);
  private readonly parkingSortService = inject(ParkingSortService);
  private readonly parkingModuleInfo = inject<ModuleInfo>(PARKING_MODULE_INFO);
  private readonly loading$ = new BehaviorSubject<boolean>(false);
  private readonly parkingData$ = new BehaviorSubject<
    Map<ParkingAreaCategory, ParkingWithDistance[]>
  >(new Map());

  get categorizedParkingData$(): Observable<{
    data: Map<ParkingAreaCategory, ParkingWithDistance[]>;
    loading: boolean;
  }> {
    return combineLatest([
      this.parkingData$.asObservable(),
      this.loading$.asObservable(),
    ]).pipe(
      map(([data, loading]) => ({
        data,
        loading,
      })),
    );
  }

  getParkingById(id: string): Observable<ParkingWithDistance> {
    return this.parkingAdapter.getParkingAreaById(id).pipe(
      switchMap((parking) =>
        from(this.addLocationDataIfPermitted(new ParkingWithDistance(parking))),
      ),
      catchError((error) => {
        console.error('Failed to fetch parking by ID:', error);
        return throwError(() => error);
      }),
    );
  }

  getParkingPois(): Observable<MapIntegration> {
    return this.parkingAdapter.getParkingPois(this.parkingModuleInfo).pipe(
      catchError((error) => {
        console.error('Failed to fetch parking POIs:', error);
        return throwError(() => error);
      }),
    );
  }

  getFavoriteParkingFeedables(): Observable<ParkingFeedItem[]> {
    return from(this.fetchFavoriteParkingIds()).pipe(
      switchMap((favorites) =>
        this.parkingAdapter.getFavoriteParkingFeedables(favorites),
      ),
      catchError((error) => {
        console.error('Failed to fetch favorite parking feedables:', error);
        return of([]);
      }),
    );
  }

  loadAndCategorizeParkingData(): void {
    this.loading$.next(true);
    from(this.fetchParkingAreas())
      .pipe(
        switchMap(async (parkingAreas) => {
          const favorites = await this.fetchFavoriteParkingIds();
          await this.calculateDistancesForParkingAreas(parkingAreas);
          return this.parkingSortService.categorizeParkingAreas(
            parkingAreas,
            favorites,
          );
        }),
      )
      .subscribe({
        next: (categorizedParking) => {
          this.parkingData$.next(categorizedParking);
          this.loading$.next(false);
        },
        error: (error) => {
          console.error('Failed to load and categorize parking data:', error);
          this.loading$.next(false);
        },
      });
  }

  private async fetchParkingAreas(): Promise<ParkingWithDistance[]> {
    try {
      const parkingAreas: Parking[] = await firstValueFrom(
        this.parkingAdapter.getParkingAreas(),
      );
      return parkingAreas.map((area) => new ParkingWithDistance(area));
    } catch (error) {
      console.error('Failed to fetch parking areas:', error);
      return [];
    }
  }

  private async fetchFavoriteParkingIds(): Promise<Set<string>> {
    const favoritesJson = await this.storageService.getObject<string[]>(
      PARKING_FAVORITES_KEY,
    );
    return new Set(favoritesJson ?? []);
  }

  private async calculateDistancesForParkingAreas(
    parkingAreas: ParkingWithDistance[],
  ): Promise<void> {
    const permissionState = await this.getLocationPermissionState();
    if (permissionState === SimplifiedPermissionState.granted) {
      const currentLocation = await firstValueFrom(
        this.geolocationService.currentLocation$.pipe(take(1)),
      );
      if (currentLocation) {
        parkingAreas.forEach((parkingArea) => {
          this.calculateDistanceIfPermitted(parkingArea, currentLocation);
        });
      }
    }
  }

  private calculateDistanceIfPermitted(
    parking: ParkingWithDistance,
    currentLocation: {
      lat: number;
      long: number;
    },
  ): ParkingWithDistance {
    if (
      currentLocation &&
      parking.area.coordinates.lat &&
      parking.area.coordinates.long
    ) {
      parking.distance = getDistance(
        { latitude: currentLocation.lat, longitude: currentLocation.long },
        {
          latitude: parking.area.coordinates.lat,
          longitude: parking.area.coordinates.long,
        },
      );
    }
    return parking;
  }

  private async addLocationDataIfPermitted(
    parking: ParkingWithDistance,
  ): Promise<ParkingWithDistance> {
    const permissionState = await this.getLocationPermissionState();
    if (permissionState === SimplifiedPermissionState.granted) {
      const currentLocation =
        await this.geolocationService.getCurrentLocation();
      if (currentLocation) {
        return this.calculateDistanceIfPermitted(parking, currentLocation);
      }
    }
    return parking;
  }

  private async getLocationPermissionState(): Promise<SimplifiedPermissionState> {
    return await firstValueFrom(
      this.geolocationService.permissionState$.pipe(take(1)),
    );
  }
}
