import { TestBed } from '@angular/core/testing';
import { ParkingSortService } from './parking-sort.service';
import { createMockParkingWithDistance } from '../shared/testing/parking-test-utils';
import { ParkingStatus } from '../models/parking.model';

describe('ParkingSortService', () => {
  let service: ParkingSortService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ParkingSortService],
    });
    service = TestBed.inject(ParkingSortService);
  });

  it('should correctly categorize parking areas based on status and favorites', () => {
    const favorites = new Set(['1', '3']);
    const parkingAreas = [
      createMockParkingWithDistance('1', ParkingStatus.OPEN, 20, 5),
      createMockParkingWithDistance('2', ParkingStatus.OPEN, 30, 10),
      createMockParkingWithDistance('3', ParkingStatus.CLOSED, 0),
      createMockParkingWithDistance('4', ParkingStatus.OPEN, 15, 15),
      createMockParkingWithDistance('5', ParkingStatus.OPEN, 50, 25),
      createMockParkingWithDistance('6', ParkingStatus.OPEN, 15, 30),
    ];

    const categorized = service.categorizeParkingAreas(parkingAreas, favorites);
    expect(categorized.get('favorite')?.length).toEqual(2);
    expect(categorized.get('close')?.length).toEqual(2);
    expect(categorized.get('further')?.length).toEqual(2);
    expect(categorized.get('free')?.length).toEqual(0);
    expect(categorized.get('notAvailable')?.length).toEqual(0);
  });
});
