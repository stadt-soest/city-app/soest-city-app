import { InjectionToken, Type } from '@angular/core';

export interface ParkingConfig {
  initialMapModalComponent: Type<unknown>;
}

export const PARKING_CONFIG = new InjectionToken<ParkingConfig>(
  'ParkingConfig',
);
