import {
  Parking,
  ParkingStatus,
  ParkingWithDistance,
} from '../../models/parking.model';

export const createMockParking = ({
  id = 'default-id',
  name = 'Default Parking Area',
  status = ParkingStatus.OPEN,
  coordinates = { lat: 40.712776, long: -74.005974 },
  openingHours = {
    MONDAY: { from: '08:00', to: '18:00' },
    TUESDAY: { from: '08:00', to: '18:00' },
    WEDNESDAY: { from: '08:00', to: '18:00' },
    THURSDAY: { from: '08:00', to: '18:00' },
    FRIDAY: { from: '08:00', to: '18:00' },
    SATURDAY: { from: '10:00', to: '16:00' },
    SUNDAY: { from: '10:00', to: '16:00' },
  },
  parkingCapacityRemaining = 50,
  address = { street: '123 Main St', postalCode: '10001', city: 'New York' },
  maximumAllowedHeight = '2.1m',
  priceDescription = [],
  description = [],
  displayName = 'Parking garage',
  parkingType = 'GARAGE',
}: Partial<Parking> = {}): Parking =>
  new Parking({
    id,
    name,
    status,
    coordinates,
    openingHours,
    parkingCapacityRemaining,
    address,
    maximumAllowedHeight,
    priceDescription,
    description,
    displayName,
    parkingType,
  });

export const createMockParkingWithDistance = (
  id: string,
  status: ParkingStatus,
  remaining: number,
  distance?: number,
): ParkingWithDistance => {
  const mockParking = createMockParking({
    id,
    status,
    parkingCapacityRemaining: remaining,
  });
  return new ParkingWithDistance(mockParking, distance);
};
