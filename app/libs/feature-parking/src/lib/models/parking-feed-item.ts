import { FeedItem } from '@sw-code/urbo-core';
import { ParkingWithDistance } from './parking.model';

export interface ParkingFeedItem extends FeedItem {
  parkingWithDistance: ParkingWithDistance;
}
