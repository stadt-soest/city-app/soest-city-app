/* eslint @typescript-eslint/naming-convention : 0 */

import {
  Category,
  DateRange,
  getCurrentDayOfWeek,
  GroupCategory,
  Localization,
  ModuleInfo,
  OpeningHoursMap,
  Poi,
  PoiCategory,
  SpecialOpeningHoursMap,
} from '@sw-code/urbo-core';
import { ParkingPageComponent } from '../pages/parking-page.component';
import { ParkingDetailsPageComponent } from '../pages/parking-details/parking-details-page.component';

export class Parking {
  id: string;
  name: string;
  displayName: string;
  parkingType: ParkingType;
  status: ParkingStatus;
  coordinates: Coordinates;
  openingHours: OpeningHoursMap;
  specialOpeningHours?: SpecialOpeningHoursMap;
  parkingCapacityRemaining: number;
  address: ParkingAddress;
  maximumAllowedHeight?: string | null;
  priceDescription: Localization[];
  description: Localization[];

  constructor(options: ParkingOptions) {
    this.id = options.id;
    this.name = options.name;
    this.status = options.status;
    this.coordinates = options.coordinates;
    this.openingHours = options.openingHours;
    this.specialOpeningHours = options.specialOpeningHours;
    this.parkingCapacityRemaining = options.parkingCapacityRemaining;
    this.address = options.address;
    this.maximumAllowedHeight = options.maximumAllowedHeight;
    this.priceDescription = options.priceDescription;
    this.description = options.description;
    this.displayName = options.displayName;
    this.parkingType = options.parkingType;
  }

  get currentDayOpeningHours(): DateRange {
    const todayKey = new Date().toISOString().split('T')[0];
    const special = this.specialOpeningHours?.[todayKey];
    return special
      ? { from: special.from, to: special.to }
      : this.openingHours[getCurrentDayOfWeek()];
  }

  get openAllDay(): boolean {
    const todayHours = this.currentDayOpeningHours;
    return todayHours.from === '00:00:00' && todayHours.to === '23:59:00';
  }

  get openAllWeek(): boolean {
    const today = new Date();

    const dayOfWeek = today.getDay();
    const diffToMonday = (dayOfWeek + 6) % 7;
    const startOfWeek = new Date(today);
    startOfWeek.setHours(0, 0, 0, 0);
    startOfWeek.setDate(today.getDate() - diffToMonday);

    const endOfWeek = new Date(startOfWeek);
    endOfWeek.setDate(startOfWeek.getDate() + 6);
    endOfWeek.setHours(23, 59, 59, 999);

    if (this.specialOpeningHours) {
      for (const dateKey in this.specialOpeningHours) {
        const specialDate = new Date(dateKey);
        if (specialDate >= startOfWeek && specialDate <= endOfWeek) {
          return false;
        }
      }
    }

    const todayHours = this.currentDayOpeningHours;
    return todayHours.from === '00:00:00' && todayHours.to === '23:59:00';
  }

  get closedToday(): boolean {
    const todayHours = this.currentDayOpeningHours;
    return (
      !todayHours.from ||
      (todayHours.from === '00:00:00' && !todayHours.to) ||
      todayHours.to === '00:00:00'
    );
  }

  static createPoiCategory(parking: Parking): PoiCategory {
    return PoiCategory.of(
      'parking',
      '',
      [
        {
          value: 'Parking',
          language: 'EN',
        },
        {
          value: 'Parken',
          language: 'DE',
        },
      ],
      Parking.getStatusBackgroundColor(parking),
    );
  }

  static createCategory(moduleInfo: ModuleInfo): Category {
    return Category.of({
      id: 'parking',
      moduleInfo,
      sourceCategories: [],
      icon: 'directions_car',
      localizedNames: [
        { value: 'Parking', language: 'EN' },
        { value: 'Parken', language: 'DE' },
      ],
      groupCategory: parkingGroupCategory,
      dashboardComponent: ParkingPageComponent,
      detailsComponent: ParkingDetailsPageComponent,
    });
  }

  static createPoi(parking: Parking, moduleInfo: ModuleInfo): Poi {
    return new Poi({
      id: parking.id,
      name: '',
      translatableName: Parking.getParkingTitleKey(parking),
      url: '',
      location: {
        city: 'Soest',
        postalCode: '59494',
        street: '',
        coordinates: {
          latitude: parking.coordinates.lat,
          longitude: parking.coordinates.long,
        },
      },
      contactPoint: {
        email: '',
        phone: '',
        contactPerson: '',
      },
      dateModified: '',
      categories: [Parking.createPoiCategory(parking)],
      icon: {
        key: 'directions_car',
      },
      modulePath: moduleInfo.baseRoutingPath,
    });
  }

  static getParkingTitleKey(parking: Parking): {
    key: string;
    params?: Record<string, any>;
  } {
    if (
      parking.status === ParkingStatus.CLOSED ||
      parking.parkingCapacityRemaining < 0
    ) {
      return { key: 'feature_parking.dashboard.closed' };
    } else if (parking.status === ParkingStatus.UNKNOWN) {
      return { key: 'feature_parking.dashboard.unknown' };
    } else {
      return {
        key: 'feature_parking.dashboard.available_spots',
        params: { spots: parking.parkingCapacityRemaining },
      };
    }
  }

  static getStatusBackgroundColor(parking: Parking): string {
    if (Parking.isParkingDisturbed(parking)) {
      return 'rgba(236, 120, 12, 1)';
    } else if (parking.status === ParkingStatus.OPEN) {
      return 'rgba(30, 79, 90, 1)';
    }
    return 'rgba(30, 79, 90, 1)';
  }

  private static isParkingDisturbed(parking: Parking): boolean {
    return (
      parking.status === ParkingStatus.CLOSED ||
      parking.parkingCapacityRemaining <= 0 ||
      parking.status === ParkingStatus.UNKNOWN
    );
  }
}

interface ParkingAddress {
  street: string;
  postalCode: string;
  city: string;
}

const parkingGroupCategory = new GroupCategory(
  'bbea43bf-4b20-48ef-b937-b4f8fe2bcae1',
  [],
);

export type ParkingType =
  | 'GARAGE'
  | 'PARKING_HOUSE'
  | 'UNDERGROUND'
  | 'PARKING_SPOT';

export enum ParkingStatus {
  OPEN = 'OPEN',
  CLOSED = 'CLOSED',
  UNKNOWN = 'UNKNOWN',
}

export type Coordinates = { lat: number; long: number };

export class ParkingWithDistance {
  constructor(
    public area: Parking,
    public distance?: number,
  ) {}

  get parkingIcon(): string {
    switch (this.area.parkingType) {
      case 'GARAGE':
      case 'UNDERGROUND':
        return 'house_siding';
      case 'PARKING_SPOT':
        return 'local_parking';
      default:
        return 'local_parking';
    }
  }

  get parkingTypeTranslationKey(): string {
    switch (this.area.parkingType) {
      case 'PARKING_SPOT':
        return 'feature_parking.dashboard.parking_spot';
      case 'GARAGE':
        return 'feature_parking.dashboard.parking_house';
      case 'UNDERGROUND':
        return 'feature_parking.dashboard.underground_car_park';
      default:
        return 'feature_parking.dashboard.parking_spot';
    }
  }

  getLocalizedPriceWithLineBreaks(lang: string): string {
    const priceDesc = this.area.priceDescription.find(
      (desc) => desc.language === lang.toUpperCase(),
    );
    if (!priceDesc) {
      return '';
    }

    return priceDesc.value.replace(/, /g, ',\n');
  }

  getLocalizedDescription(lang: string): string {
    const desc = this.area.description.find(
      (description) => description.language === lang.toUpperCase(),
    );
    if (!desc) {
      return '';
    }

    return desc.value;
  }

  getSpecialOpeningHoursDescription(date: string, lang: string): string {
    const specialOpeningHours = this.area.specialOpeningHours?.[date];
    if (!specialOpeningHours) {
      return '';
    }

    const description = specialOpeningHours.descriptions.find(
      (desc) => desc.language === lang.toUpperCase(),
    );
    return description ? description.value : '';
  }
}

export type ParkingAreaCategory =
  | 'favorite'
  | 'free'
  | 'close'
  | 'further'
  | 'notAvailable';

interface ParkingOptions {
  id: string;
  name: string;
  status: ParkingStatus;
  coordinates: Coordinates;
  openingHours: OpeningHoursMap;
  specialOpeningHours?: SpecialOpeningHoursMap;
  parkingCapacityRemaining: number;
  address: ParkingAddress;
  maximumAllowedHeight?: string | null;
  priceDescription: Localization[];
  description: Localization[];
  displayName: string;
  parkingType: ParkingType;
}
