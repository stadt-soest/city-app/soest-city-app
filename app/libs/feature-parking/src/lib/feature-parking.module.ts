import {
  Inject,
  InjectionToken,
  ModuleWithProviders,
  NgModule,
  inject,
  provideAppInitializer,
} from '@angular/core';
import { ParkingSortService } from './services/parking-sort.service';
import { ParkingService } from './services/parking.service';
import { provideTranslocoScope } from '@jsverse/transloco';
import {
  FeatureModule,
  ModuleCategory,
  ModuleInfo,
  ModuleRegistryService,
} from '@sw-code/urbo-core';
import { from, map, of } from 'rxjs';
import { PARKING_CONFIG, ParkingConfig } from './parking-tokens';

const PROVIDERS = [
  ParkingService,
  ParkingSortService,
  provideTranslocoScope({
    scope: 'feature-parking',
    alias: 'feature_parking',
    loader: {
      en: () => import('./i18n/en.json'),
      de: () => import('./i18n/de.json'),
    },
  }),
];
export const PARKING_MODULE_INFO = new InjectionToken<ModuleInfo>(
  'ParkingModuleInfoHolderToken',
);
export const PARKING_FAVORITES_KEY = 'parkingFavorites';

const moduleId = 'parking';

@NgModule({
  providers: [
    ...PROVIDERS,
    {
      provide: PARKING_MODULE_INFO,
      useValue: {
        id: moduleId,
        name: 'feature_parking.module_name',
        category: ModuleCategory.onTheWay,
        icon: 'directions_car',
        baseRoutingPath: moduleId,
        defaultVisibility: true,
        settingsPageAvailable: false,
        isAboutAppItself: false,
        relevancy: 0.5,
        forYouRelevant: true,
        showInCategoriesPage: true,
        orderInCategory: 2,
      },
    },
  ],
})
export class FeatureParkingModule {
  readonly featureModule: FeatureModule<null>;

  constructor(
    @Inject(PARKING_MODULE_INFO) private readonly parkingModuleInfo: ModuleInfo,
    private readonly parkingService: ParkingService,
  ) {
    this.featureModule = {
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      initializeDefaultSettings: () => {},
      registerMap: () => parkingService.getParkingPois(),
      feedablesActivity: () =>
        this.parkingService.getFavoriteParkingFeedables().pipe(
          map((parkingFeedItems) => ({
            items: parkingFeedItems,
            totalRecords: parkingFeedItems.length,
          })),
        ),
      feedables: () => of({ items: [], totalRecords: 0 }),
      feedablesUnfiltered: () => from([]),
      info: this.parkingModuleInfo,
      routes: [
        {
          path: `categories/${parkingModuleInfo.id}`,
          loadComponent: () =>
            import('./pages/parking-page.component').then(
              (m) => m.ParkingPageComponent,
            ),
          title: 'feature_parking.module_name',
        },
        {
          path: `categories/${parkingModuleInfo.id}/:id`,
          loadComponent: () =>
            import(
              './pages/parking-details/parking-details-page.component'
            ).then((m) => m.ParkingDetailsPageComponent),
        },
        {
          path: `map/feature/${parkingModuleInfo.id}`,
          loadComponent: () =>
            import('./pages/parking-page.component').then(
              (m) => m.ParkingPageComponent,
            ),
          title: 'feature_parking.module_name',
        },
        {
          path: `map/feature/${parkingModuleInfo.id}/:id`,
          loadComponent: () =>
            import(
              './pages/parking-details/parking-details-page.component'
            ).then((m) => m.ParkingDetailsPageComponent),
        },
        {
          path: `for-you/${moduleId}/:id`,
          loadComponent: () =>
            import(
              './pages/parking-details/parking-details-page.component'
            ).then((m) => m.ParkingDetailsPageComponent),
        },
      ],
    };
  }

  static forRoot(
    parkingConfig: ParkingConfig,
  ): ModuleWithProviders<FeatureParkingModule> {
    return {
      ngModule: FeatureParkingModule,
      providers: [
        {
          provide: PARKING_CONFIG,
          useValue: parkingConfig,
        },
        provideAppInitializer(() => {
          const initializerFn = (
            (registry: ModuleRegistryService, module: FeatureParkingModule) =>
            () =>
              registry.registerFeature(module.featureModule)
          )(inject(ModuleRegistryService), inject(FeatureParkingModule));
          return initializerFn();
        }),
      ],
    };
  }
}
