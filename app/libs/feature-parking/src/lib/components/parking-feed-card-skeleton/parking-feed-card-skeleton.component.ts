import { Component } from '@angular/core';
import {
  IonCard,
  IonCol,
  IonGrid,
  IonRow,
  IonSkeletonText,
} from '@ionic/angular/standalone';

@Component({
  selector: 'lib-parking-feed-card-skeleton',
  templateUrl: './parking-feed-card-skeleton.component.html',
  styleUrls: ['./parking-feed-card-skeleton.component.scss'],
  imports: [IonCard, IonGrid, IonRow, IonCol, IonSkeletonText],
})
export class ParkingFeedCardSkeletonComponent {}
