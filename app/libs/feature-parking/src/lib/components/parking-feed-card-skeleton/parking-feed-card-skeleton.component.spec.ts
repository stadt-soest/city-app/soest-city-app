import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ParkingFeedCardSkeletonComponent } from './parking-feed-card-skeleton.component';
import { UITestModule } from '@sw-code/urbo-ui';
import { CoreTestModule } from '@sw-code/urbo-core';
import { getTranslocoModule } from '../../../transloco-testing.module';

describe('ParkingFeedCardSkeletonComponent', () => {
  let component: ParkingFeedCardSkeletonComponent;
  let fixture: ComponentFixture<ParkingFeedCardSkeletonComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        ParkingFeedCardSkeletonComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ParkingFeedCardSkeletonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
