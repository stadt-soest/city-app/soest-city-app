import {
  Component,
  EventEmitter,
  inject,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  IonCard,
  IonCardSubtitle,
  IonCardTitle,
  IonCol,
  IonGrid,
  IonRouterLinkWithHref,
  IonRow,
} from '@ionic/angular/standalone';
import {
  CustomSelectableIconTextButtonComponent,
  DistanceFormatPipe,
  GeneralIconComponent,
  IconFontSize,
  IconType,
  TimeFormatPipe,
} from '@sw-code/urbo-ui';
import { NgIf } from '@angular/common';
import { BadgeContainerComponent } from '../badge-container/badge-container.component';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { ParkingWithDistance } from '../../models/parking.model';
import {
  Category,
  DateRange,
  FeedUpdateService,
  MapsNavigationService,
  NavService,
  StorageService,
} from '@sw-code/urbo-core';
import { PARKING_FAVORITES_KEY } from '../../feature-parking.module';
import { ParkingDetailsPageComponent } from '../../pages/parking-details/parking-details-page.component';

@Component({
  selector: 'lib-parking-card',
  templateUrl: './parking-card.component.html',
  styleUrls: ['./parking-card.component.scss'],
  imports: [
    TranslocoDirective,
    IonCard,
    IonGrid,
    IonRow,
    IonCol,
    IonCardTitle,
    NgIf,
    IonCardSubtitle,
    GeneralIconComponent,
    BadgeContainerComponent,
    CustomSelectableIconTextButtonComponent,
    DistanceFormatPipe,
    TimeFormatPipe,
  ],
})
export class ParkingCardComponent implements OnInit {
  @Input({ required: true }) parkingArea!: ParkingWithDistance;
  @Input() isForYouFeed = false;
  @Input() elevation: 'subtle-elevation' | 'strong-elevation' =
    'subtle-elevation';
  @Input() routerLink?: string[];
  @Output() favoriteChanged: EventEmitter<null> = new EventEmitter();

  @Input() fromMap = false;
  @Input() categoryName!: string;
  @Input() categories: Category[] = [];
  @Input() categoryId = '';

  isFavoriteParking = false;
  currentDayOpeningHours?: DateRange;
  protected readonly iconType = IconType;
  protected readonly iconFontSize = IconFontSize;
  private readonly mapsNavigationService = inject(MapsNavigationService);
  private readonly storageService = inject(StorageService);
  private readonly feedUpdateService = inject(FeedUpdateService);
  private readonly navService = inject(NavService);
  private readonly router = inject(Router);
  private readonly route = inject(ActivatedRoute);

  async ngOnInit() {
    await this.loadFavoriteState();
  }

  async toggleFavorite(event: Event) {
    this.stopEventPropagationAndDefault(event);

    const favoritesJson = await this.storageService.get(PARKING_FAVORITES_KEY);
    let favorites: string[] = favoritesJson ? JSON.parse(favoritesJson) : [];

    if (this.isFavoriteParking) {
      favorites = favorites.filter(
        (id: string) => id !== this.parkingArea.area.id,
      );
    } else {
      favorites.push(this.parkingArea.area.id);
    }

    await this.storageService.set(
      PARKING_FAVORITES_KEY,
      JSON.stringify(favorites),
    );
    this.isFavoriteParking = !this.isFavoriteParking;
    this.favoriteChanged.emit();
    this.feedUpdateService.notifyFeedUpdate();
  }

  async navigateToParkingDetailsPage() {
    if (this.fromMap) {
      await this.navigateToParkingAreaInMap();
    } else {
      await this.navigateToParkingAreaInModule();
    }
  }

  async openNavigation(event: Event) {
    this.stopEventPropagationAndDefault(event);
    await this.mapsNavigationService.navigateToLocation(
      this.parkingArea.area.coordinates.lat,
      this.parkingArea.area.coordinates.long,
    );
  }

  private stopEventPropagationAndDefault(event: Event) {
    event.preventDefault();
    event.stopPropagation();
  }

  private async loadFavoriteState() {
    const favoritesJson = await this.storageService.get(PARKING_FAVORITES_KEY);
    const favorites: string[] = favoritesJson ? JSON.parse(favoritesJson) : [];
    this.isFavoriteParking = favorites.includes(this.parkingArea.area.id);
  }

  private async navigateToParkingAreaInMap() {
    await this.navService.pushPage(
      ParkingDetailsPageComponent,
      {
        poiId: this.parkingArea.area.id,
        categoryName: this.categoryName,
        categories: this.categories,
        categoryId: this.categoryId,
      },
      { animated: true },
    );
  }

  private async navigateToParkingAreaInModule() {
    await this.router.navigate(this.routerLink ?? [this.parkingArea.area.id], {
      relativeTo: this.route,
    });
  }
}
