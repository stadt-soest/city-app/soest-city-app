import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ParkingCardComponent } from './parking-card.component';
import { CoreTestModule } from '@sw-code/urbo-core';
import { getTranslocoModule } from '../../../transloco-testing.module';
import { ParkingWithDistance } from '../../models/parking.model';
import { createMockParking } from '../../shared/testing/parking-test-utils';

describe('ParkingCardComponent', () => {
  let component: ParkingCardComponent;
  let fixture: ComponentFixture<ParkingCardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        ParkingCardComponent,
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ParkingCardComponent);
    component = fixture.componentInstance;
    component.parkingArea = new ParkingWithDistance(
      createMockParking({ name: 'PH Example Parking' }),
    );
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display the correct name without prefix', () => {
    const title = fixture.nativeElement.querySelector('.header ion-card-title');
    expect(title.textContent).toContain('Parking garage');
  });

  it('should display translated display name based on area prefix', () => {
    const displayName = fixture.nativeElement.querySelector(
      '.parking-display-name',
    ).textContent;
    expect(displayName).toContain('Parking garage');
  });
});
