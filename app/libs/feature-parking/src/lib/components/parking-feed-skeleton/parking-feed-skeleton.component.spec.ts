import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ParkingFeedSkeletonComponent } from './parking-feed-skeleton.component';
import { UITestModule } from '@sw-code/urbo-ui';
import { CoreTestModule } from '@sw-code/urbo-core';
import { getTranslocoModule } from '../../../transloco-testing.module';

describe('ParkingFeedSkeletonComponent', () => {
  let component: ParkingFeedSkeletonComponent;
  let fixture: ComponentFixture<ParkingFeedSkeletonComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        ParkingFeedSkeletonComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ParkingFeedSkeletonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
