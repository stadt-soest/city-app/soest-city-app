import { Component } from '@angular/core';
import { IonCol, IonRow, IonSkeletonText } from '@ionic/angular/standalone';
import { NgFor } from '@angular/common';
import { ParkingFeedCardSkeletonComponent } from '../parking-feed-card-skeleton/parking-feed-card-skeleton.component';

@Component({
  selector: 'lib-parking-feed-skeleton',
  templateUrl: './parking-feed-skeleton.component.html',
  styleUrls: ['./parking-feed-skeleton.component.scss'],
  imports: [
    IonRow,
    IonSkeletonText,
    NgFor,
    IonCol,
    ParkingFeedCardSkeletonComponent,
  ],
})
export class ParkingFeedSkeletonComponent {}
