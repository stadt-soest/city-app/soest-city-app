import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { NgClass } from '@angular/common';
import { GeneralIconComponent, IconFontSize, IconType } from '@sw-code/urbo-ui';
import { Parking } from '../../models/parking.model';

@Component({
  selector: 'lib-parking-badge-container',
  templateUrl: './badge-container.component.html',
  styleUrls: ['./badge-container.component.scss'],
  imports: [TranslocoDirective, NgClass, GeneralIconComponent],
})
export class BadgeContainerComponent implements OnChanges {
  @Input({ required: true }) parking!: Parking;
  status: BadgeStatus = BadgeStatus.Unknown;
  protected readonly badgeStatus = BadgeStatus;
  protected readonly iconFontSize = IconFontSize;
  protected readonly iconType = IconType;

  get badgeClasses(): Record<string, boolean> {
    return {
      'badge-negative':
        this.status === BadgeStatus.LowCapacity ||
        this.status === BadgeStatus.Unknown ||
        this.status === BadgeStatus.Closed,
      'badge-high': this.status === BadgeStatus.HighCapacityAvailable,
    };
  }

  get badgeIcon(): string {
    switch (this.status) {
      case BadgeStatus.LowCapacity:
      case BadgeStatus.Unknown:
      case BadgeStatus.Closed:
        return 'do_not_disturb_on';
      case BadgeStatus.HighCapacityAvailable:
        return 'check';
      default:
        return '';
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['parking'] && this.parking) {
      this.updateStatusAndSpots();
    }
  }

  private updateStatusAndSpots(): void {
    if (this.parking.parkingCapacityRemaining < 0) {
      this.status = BadgeStatus.Unknown;
      return;
    }

    switch (this.parking.status) {
      case 'CLOSED':
        this.status = BadgeStatus.Closed;
        break;
      case 'UNKNOWN':
        this.status = BadgeStatus.Unknown;
        break;
      default:
        this.handleOpenStatus();
        break;
    }
  }

  private handleOpenStatus(): void {
    if (this.parking && this.parking.parkingCapacityRemaining < 5) {
      this.status = BadgeStatus.LowCapacity;
    } else {
      this.status = BadgeStatus.HighCapacityAvailable;
    }
  }
}

export enum BadgeStatus {
  LowCapacity = 'LowCapacity',
  HighCapacityAvailable = 'HighCapacityAvailable',
  Unknown = 'Unknown',
  Closed = 'Closed',
}
