import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  BadgeContainerComponent,
  BadgeStatus,
} from './badge-container.component';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../transloco-testing.module';

/* eslint @typescript-eslint/naming-convention: 0 */

describe('BadgeContainerComponent', () => {
  let component: BadgeContainerComponent;
  let fixture: ComponentFixture<BadgeContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        BadgeContainerComponent,
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(BadgeContainerComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('badgeClasses', () => {
    it('should return correct classes for closed status', () => {
      component.status = BadgeStatus.Closed;
      expect(component.badgeClasses).toEqual({
        'badge-high': false,
        'badge-negative': true,
      });
    });

    it('should return correct classes for high capacity', () => {
      component.status = BadgeStatus.HighCapacityAvailable;
      expect(component.badgeClasses).toEqual({
        'badge-high': true,
        'badge-negative': false,
      });
    });
  });

  describe('badgeIcon', () => {
    it('should return correct icon for closed status', () => {
      component.status = BadgeStatus.Closed;
      expect(component.badgeIcon).toBe('do_not_disturb_on');
    });

    it('should return correct icon for high capacity available', () => {
      component.status = BadgeStatus.HighCapacityAvailable;
      expect(component.badgeIcon).toBe('check');
    });
  });
});
