import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ParkingCardComponent } from '../parking-card/parking-card.component';
import { RouterLink } from '@angular/router';
import { IonRouterLink } from '@ionic/angular/standalone';
import { ParkingFeedItem } from '../../models/parking-feed-item';
import { ParkingWithDistance } from '../../models/parking.model';
import { AbstractFeedCardComponent } from '@sw-code/urbo-core';

@Component({
  selector: 'lib-parking-card-for-you',
  template: `
    <lib-parking-card
      [routerLink]="navigationLink"
      [parkingArea]="parkingArea"
      [isForYouFeed]="true"
      elevation="strong-elevation"
    ></lib-parking-card>
  `,
  imports: [ParkingCardComponent, RouterLink, IonRouterLink],
})
export class ParkingCardForYouComponent
  extends AbstractFeedCardComponent<ParkingFeedItem>
  implements OnInit
{
  @Input({ required: true }) parkingArea!: ParkingWithDistance;
  @Output() favoriteChanged: EventEmitter<null> = new EventEmitter();
  navigationLink: string[] = [];

  ngOnInit() {
    if (this.feedItem) {
      this.parkingArea = this.feedItem?.parkingWithDistance;
      this.navigationLink = [
        '.',
        this.feedItem.baseRoutePath,
        this.feedItem.id,
      ];
    }
  }
}
