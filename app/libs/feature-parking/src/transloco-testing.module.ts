import en from './lib/i18n/en.json';
import de from './lib/i18n/de.json';
import {
  TranslocoConfig,
  TranslocoTestingModule,
  TranslocoTestingOptions,
} from '@jsverse/transloco';

export const getTranslocoModule = (options: TranslocoTestingOptions = {}) => {
  const testingOptions: TranslocoTestingOptions = {
    langs: {
      en,
      de,

      'feature_parking/de': de,

      'feature_parking/en': en,
    },
    translocoConfig: {
      availableLangs: ['en', 'de'],
      defaultLang: 'en',
    },
    preloadLangs: true,
    ...options,
  };

  (
    testingOptions.translocoConfig as TranslocoConfig & {
      scopeMapping?: Record<string, string>;
    }
  ).scopeMapping = {
    feature_parking: 'feature_parking',
  };

  return TranslocoTestingModule.forRoot(testingOptions);
};
