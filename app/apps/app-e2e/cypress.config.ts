import { defineConfig } from 'cypress';
import { nxE2EPreset } from '@nx/cypress/plugins/cypress-preset';
import cypressSplit = require('cypress-split');

const nxPreset = nxE2EPreset(__filename, {
  cypressDir: 'src',
  webServerCommands: {
    default:
      'pnpm exec nx run app:serve --configuration=with-proxy --watch=false',
    production: 'pnpm exec nx run app:serve-static',
  },
  ciWebServerCommand:
    'pnpm exec nx run app:serve --configuration=with-proxy --watch=false',
  ciBaseUrl: 'http://localhost:4200',
});

export default defineConfig({
  e2e: {
    ...nxPreset,
    baseUrl: 'http://localhost:4200',
    includeShadowDom: true,
    setupNodeEvents: async (on, config) => {
      if (typeof nxPreset.setupNodeEvents === 'function') {
        await nxPreset.setupNodeEvents(on, config);
      }

      cypressSplit(on, config);
      return config;
    },
  },
});
