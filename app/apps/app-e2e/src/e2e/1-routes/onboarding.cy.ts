import { APP_ROUTES } from '../../common/routes';

describe('Onboarding Accessibility', () => {
  it('should access the main Map page', () => {
    const onboarding = APP_ROUTES.on_boarding;

    cy.visit(onboarding, {
      onBeforeLoad: (window) => {
        window.localStorage.setItem(
          'CapacitorStorage.finishedOnboarding',
          'false',
        );
      },
    });
    cy.url().should('include', onboarding);
  });

  it('should skip onboarding and redirect to /for-you', () => {
    const onboardingRoute = APP_ROUTES.on_boarding;
    const forYouRoute = APP_ROUTES.tabs.for_you.main;

    cy.visit(onboardingRoute, {
      onBeforeLoad: (window) => {
        window.localStorage.setItem(
          'CapacitorStorage.finishedOnboarding',
          'false',
        );
      },
    });

    cy.get('[data-cy="skip-button"]').click();

    cy.url().should('include', forYouRoute);
    cy.window().then((window) => {
      expect(
        window.localStorage.getItem('CapacitorStorage.finishedOnboarding'),
      ).to.equal('true');
    });
  });

  it('should redirect to /for-you if onboarding is already finished', () => {
    const onboardingRoute = APP_ROUTES.on_boarding;
    const forYouRoute = APP_ROUTES.tabs.for_you.main;

    cy.visit(onboardingRoute, {
      onBeforeLoad: (window) => {
        window.localStorage.setItem(
          'CapacitorStorage.finishedOnboarding',
          'true',
        );
      },
    });

    cy.url().should('include', forYouRoute);
  });

  it('should click through onboarding pages, finish onboarding and redirect to /for-you', () => {
    const onboardingRoute = APP_ROUTES.on_boarding;
    const forYouRoute = APP_ROUTES.tabs.for_you.main;

    cy.visit(onboardingRoute, {
      onBeforeLoad: (window) => {
        window.localStorage.setItem(
          'CapacitorStorage.finishedOnboarding',
          'false',
        );
      },
    });

    cy.get('[data-cy="visibility-on-button"]').click({ multiple: true });
    cy.get('[data-cy="finish-button"]').click();

    cy.url().should('include', forYouRoute);
    cy.window().then((window) => {
      expect(
        window.localStorage.getItem('CapacitorStorage.finishedOnboarding'),
      ).to.equal('true');
    });
  });
});
