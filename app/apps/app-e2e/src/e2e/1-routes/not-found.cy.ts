import { APP_ROUTES } from '../../common/routes';

describe('Not found Accessibility', () => {
  it('should access the main Map page', () => {
    const randomUrl = Math.random().toString();

    cy.visit(randomUrl);
    cy.url().should('include', APP_ROUTES.not_found);
  });
});
