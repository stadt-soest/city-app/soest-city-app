import { APP_ROUTES } from '../../common/routes';
import { API_IDS } from '../../common/api-ids';

describe('Map Tab Accessibility and Functionality', () => {
  describe('Main Map Page Access and tab-bar navigation', () => {
    it('should access the main Map page', () => {
      const mapTab = APP_ROUTES.tabs.map.main;
      cy.visit(mapTab);
      cy.url().should('include', mapTab);
      cy.checkBackButtonAbsent();
    });

    it('should refresh map page when tabbar map is clicked again', () => {
      cy.visit(APP_ROUTES.tabs.for_you.main);

      cy.get('[data-cy="tab-map-button"]').click();

      cy.url().should('include', APP_ROUTES.tabs.map.main);

      cy.get('[data-cy="tab-map-button"]').click();

      cy.get('ion-modal').should('not.exist');
    });
  });

  describe('Poi Details Access', () => {
    it('should access details for a specific POI using ID from fixture', () => {
      const mapPoiDetails = APP_ROUTES.tabs.map.details(API_IDS.POI_ID);

      cy.visit(mapPoiDetails);
      cy.url().should('include', mapPoiDetails);
      cy.testBackNavigation(APP_ROUTES.tabs.map.main);
    });
  });

  describe('Map Filters Modal Interaction', () => {
    it('should open and close modal', () => {
      cy.visit(APP_ROUTES.tabs.for_you.main);
      cy.visit(APP_ROUTES.tabs.map.main);
      cy.get('[data-cy="open-filter-modal-button"]').click();
      closeMapFilterModal();
      cy.get('ion-modal').should('not.exist');
    });
  });

  function closeMapFilterModal() {
    cy.get('.custom-modal-header > .close-button > .custom-button').click();
  }
});
