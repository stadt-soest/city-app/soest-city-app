import { APP_ROUTES } from '../../common/routes';
import { API_IDS } from '../../common/api-ids';

describe('Categories Tab Accessibility and Functionality', () => {
  beforeEach(() => {
    cy.visit(APP_ROUTES.tabs.category.main);
  });

  describe('Main Categories Page access and tab-bar navigation', () => {
    it('should access the main Categories page', () => {
      const categoryTab = APP_ROUTES.tabs.category.main;
      cy.url().should('include', categoryTab);
      cy.checkBackButtonAbsent();
    });

    it('should refresh page and reset scroll position when tab-categories-button is clicked', () => {
      cy.visit(APP_ROUTES.tabs.for_you.main);

      cy.get('[data-cy="tab-categories-button"]').click();

      cy.url().should('include', APP_ROUTES.tabs.category.main);

      cy.get('ion-content')
        .find('.inner-scroll')
        .then(($scrollContainer) => {
          $scrollContainer[0].scrollTo(0, $scrollContainer[0].scrollHeight);
          expect($scrollContainer[0].scrollTop).to.be.greaterThan(0);
        });

      cy.get('[data-cy="tab-categories-button"]').click();

      cy.get('ion-content')
        .find('.inner-scroll')
        .should(($scrollContainer) => {
          expect($scrollContainer[0].scrollTop).to.equal(0);
        });
    });
  });

  describe('Category Dashboards', () => {
    it('should access the News category dashboard', () => {
      const newsDashboard = APP_ROUTES.tabs.category.news.dashboard;
      cy.visit(newsDashboard).url().should('include', newsDashboard);
      cy.testBackNavigation(APP_ROUTES.tabs.category.main);
    });

    it('should access the Events category dashboard', () => {
      const eventsDashboard = APP_ROUTES.tabs.category.events.dashboard;
      cy.visit(eventsDashboard).url().should('include', eventsDashboard);
      cy.testBackNavigation(APP_ROUTES.tabs.category.main);
    });

    it('should access the Town Hall category dashboard', () => {
      const townHallDashboard = APP_ROUTES.tabs.category.town_hall.dashboard;
      cy.visit(townHallDashboard);
      cy.url().should('include', townHallDashboard);
      cy.testBackNavigation(APP_ROUTES.tabs.category.main);
    });

    it('should access the Waste category dashboard', () => {
      const wasteDashboard = APP_ROUTES.tabs.category.waste.dashboard;
      cy.visit(wasteDashboard);
      cy.url().should('include', wasteDashboard);
      cy.testBackNavigation(APP_ROUTES.tabs.category.main);
    });

    it('should access the Parking category dashboard', () => {
      const parkingDashboard = APP_ROUTES.tabs.category.parking.dashboard;
      cy.visit(parkingDashboard);
      cy.url().should('include', parkingDashboard);
      cy.testBackNavigation(APP_ROUTES.tabs.category.main);
    });

    it('should access the Report a Defect category dashboard', () => {
      const reportADefectDashboard =
        APP_ROUTES.tabs.category.report_a_defect.dashboard;
      cy.visit(reportADefectDashboard)
        .url()
        .should('include', reportADefectDashboard);
      cy.testBackNavigation(APP_ROUTES.tabs.category.main);
    });

    it('should access the App Feedback category dashboard', () => {
      const appFeedbackDashboard =
        APP_ROUTES.tabs.category.app_feedback.dashboard;
      cy.visit(appFeedbackDashboard);
      cy.url().should('include', appFeedbackDashboard);
      cy.testBackNavigation(APP_ROUTES.tabs.category.main);
    });
  });

  describe('Category Details Pages', () => {
    it('should access a specific News category details page using ID from fixture', () => {
      const newsDetails = APP_ROUTES.tabs.category.news.details(
        API_IDS.NEWS_ID,
      );
      cy.visit(newsDetails);
      cy.url().should('include', newsDetails);
      cy.testBackNavigation(APP_ROUTES.tabs.category.news.dashboard);
    });

    it('should access a specific Events category details page using ID from fixture', () => {
      const eventDetails = APP_ROUTES.tabs.category.events.details(
        API_IDS.EVENT_ID,
      );
      cy.visit(eventDetails);
      cy.url().should('include', eventDetails);
      cy.testBackNavigation(APP_ROUTES.tabs.category.events.dashboard);
    });

    it('should access a specific Town Hall category details page using ID from fixture', () => {
      const townHallDetails = APP_ROUTES.tabs.category.town_hall.details(
        API_IDS.CITIZEN_SERVICE_ID,
      );
      cy.visit(townHallDetails);
      cy.url().should('include', townHallDetails);
      cy.testBackNavigation(APP_ROUTES.tabs.category.town_hall.dashboard);
    });

    it('should access a specific Town Hall category by category ID from fixture', () => {
      const townHallCategory = APP_ROUTES.tabs.category.town_hall.category(
        API_IDS.CITIYZEN_SERVICE_CATEGORY_ID,
      );
      cy.visit(townHallCategory);
      cy.url().should('include', townHallCategory);
      cy.testBackNavigation(APP_ROUTES.tabs.category.town_hall.dashboard);
    });
  });

  describe('Category Settings Pages', () => {
    it('should access the Event category settings page', () => {
      const eventSettings = APP_ROUTES.tabs.category.events.settings;
      cy.visit(eventSettings);
      cy.url().should('include', eventSettings);
      cy.testBackNavigation(APP_ROUTES.tabs.category.events.dashboard);
    });

    it('should access the News category settings page', () => {
      const newsSettings = APP_ROUTES.tabs.category.news.settings;
      cy.visit(newsSettings);
      cy.url().should('include', newsSettings);
      cy.testBackNavigation(APP_ROUTES.tabs.category.news.dashboard);
    });

    it('should access the Waste category settings page', () => {
      const wasteSettings = APP_ROUTES.tabs.category.waste.settings;
      cy.visit(wasteSettings);
      cy.url().should('include', wasteSettings);
      cy.testBackNavigation(APP_ROUTES.tabs.category.waste.dashboard);
    });
  });

  describe('App Feedback Pages', () => {
    it('should access the App Feedback Questionnaire page', () => {
      const appFeedbackQuestionnaire =
        APP_ROUTES.tabs.category.app_feedback.questionnaire;
      cy.visit(appFeedbackQuestionnaire);
      cy.url().should('include', appFeedbackQuestionnaire);
      cy.testBackNavigation(APP_ROUTES.tabs.category.app_feedback.dashboard);
    });

    it('should access the App Feedback Issue Report page', () => {
      const appFeedbackIssueReport =
        APP_ROUTES.tabs.category.app_feedback.issue_report;
      cy.visit(appFeedbackIssueReport)
        .url()
        .should('include', appFeedbackIssueReport);
      cy.testBackNavigation(APP_ROUTES.tabs.category.app_feedback.dashboard);
    });
  });
});
