import { APP_ROUTES } from '../../common/routes';
import { API_IDS } from '../../common/api-ids';

describe('Search Tab Accessibility and Functionality', () => {
  beforeEach(() => {
    cy.visit(APP_ROUTES.tabs.search.main);
  });

  describe('Main Search Page access and tab-bar focus', () => {
    it('should access the main Search page', () => {
      cy.url().should('include', APP_ROUTES.tabs.search.main);
    });

    it('should focus tabbar when tabbar search is clicked again', () => {
      cy.visit(APP_ROUTES.tabs.for_you.main);

      cy.get('[data-cy="tab-search-button"]').click();

      cy.url().should('include', APP_ROUTES.tabs.search.main);

      cy.get('[data-cy="tab-search-button"]').click();

      cy.get('ion-searchbar .searchbar-input').should('have.focus');
    });
  });

  describe('Search Results Access by Category', () => {
    it('should access Town Hall search results with a search term', () => {
      const searchTerm = 'm';
      const townHallSearchResults =
        APP_ROUTES.tabs.search.town_hall.all_results(searchTerm);

      cy.visit(townHallSearchResults);
      cy.url().should('include', townHallSearchResults);
    });

    it('should access News search results with a search term', () => {
      const searchTerm = 'm';
      const newsSearchResults =
        APP_ROUTES.tabs.search.news.all_results(searchTerm);

      cy.visit(newsSearchResults);
      cy.url().should('include', newsSearchResults);
    });

    it('should access Events search results with a search term', () => {
      const searchTerm = 'm';
      const eventSearchResults =
        APP_ROUTES.tabs.search.events.all_results(searchTerm);

      cy.visit(eventSearchResults);
      cy.url().should('include', eventSearchResults);
    });
  });

  describe('Details Page Access by ID', () => {
    it('should access a specific Town Hall details page using ID from fixture', () => {
      const townHallDetails = APP_ROUTES.tabs.search.town_hall.details(
        API_IDS.CITIZEN_SERVICE_ID,
      );

      cy.visit(townHallDetails);
      cy.url().should('include', townHallDetails);
    });

    it('should access a specific News details page using ID from fixture', () => {
      const newsDetails = APP_ROUTES.tabs.search.news.details(API_IDS.NEWS_ID);

      cy.visit(newsDetails);
      cy.url().should('include', newsDetails);
    });

    it('should access a specific Events details page using ID from fixture', () => {
      const eventDetails = APP_ROUTES.tabs.search.events.details(
        API_IDS.EVENT_ID,
      );

      cy.visit(eventDetails);
      cy.url().should('include', eventDetails);
    });
  });
});
