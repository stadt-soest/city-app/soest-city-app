import { APP_ROUTES, for_you_feed_category } from '../../common/routes';
import { API_IDS } from '../../common/api-ids';

describe('For You Tab Accessibility', () => {
  const setupLocalStorage = (window: Cypress.AUTWindow) => {
    window.localStorage.setItem('CapacitorStorage.finishedOnboarding', 'true');
    window.localStorage.setItem(
      'CapacitorStorage.storageKey_CategorySettings_events',
      '{"6517f1ac-4599-4b32-9165-db9df9ba92dd":true,"3d3b38ff-f8b8-43be-92e6-a119d5ab9138":true,"7825644b-dd7f-43d5-b064-67bdc1f9dc58":true,"741475d2-76ed-4ff5-bb62-fc1981ee9a37":true,"7e4073ea-fb27-430c-bd6b-8d531f712cfb":true,"c6d2151b-453a-4353-9c0c-f18265d38448":true,"824a8e52-fc33-45c6-bc94-e03d44f31ec2":true,"f6a50b85-4832-494b-8941-4e002758679e":true,"a33e6c97-05ca-44cd-9326-4cf10bd5d2b3":true,"93fad8ee-4c41-48f1-a6ec-e10431aea98b":true,"5c14150a-0ccc-4140-bd4e-c4c1e752bb7e":true,"82da86f8-5ef9-4351-a0f3-0e2c8824edae":true,"7b1b5147-7e67-4641-b28e-9dda6dd7061d":true,"86e4a769-f1f6-4c99-b7b4-1b4435a4250a":true,"b7578c7c-f794-4332-a5cc-11bbd35f4b73":true,"14dbf132-8a31-4af6-bc0b-e258db820e8a":true,"dbe40a87-ef30-4aca-b2f8-08aa5afbe950":true,"87677a94-745e-4505-92bc-863025d39079":true}',
    );
    window.localStorage.setItem(
      'CapacitorStorage.storageKey_CategorySettings_news',
      '{"c5320a5c-e222-4cf7-b921-544c18c3ba61":true,"2325910e-af1a-4cd1-b563-aedb9219deb0":true,"a25e2c60-744d-4f45-bc74-cf9e9c294267":true,"065aa034-79e2-4549-8fc2-1a77f8ed4fc3":true,"7ee7cc48-c434-4993-8d48-b2e090df6107":true,"e269ae08-3823-494e-86fe-bec2e8c7873b":true,"0f46683e-e539-4601-b53d-ad9c683010bf":true,"bdf4536f-ac87-4923-8e7a-5096f8638f62":true,"bc61fea7-3b68-4ebc-9f7b-a864b9fff365":true,"d0e24016-ebda-47a3-b278-ad0c32c51ac5":true,"eae7f8c2-1f32-40f2-98e6-d1a02499ca62":true,"5921f086-2ae3-4d27-ad28-275494c10e15":true,"4a0d6502-66a7-43e6-9490-775424a234b2":true,"6dda4689-8ea8-4549-a228-a572b943cff4":true}',
    );
    window.localStorage.setItem(
      'CapacitorStorage.storageKey_SourceSettings_news',
      '{"KlinikumStadtSoest":true,"Stadt Soest":true,"So ist Soest":true,"Kreis Soest":true,"Kulturbüro Soest":true}',
    );
  };

  const clickFirstAvailableItem = (selector: string) => {
    cy.get(selector)
      .should('have.length.greaterThan', 0)
      .then((elements) => {
        cy.wrap(elements.eq(0)).click();
      });
  };

  const assertNavigationToDetailsPage = () => {
    cy.url().should('match', /\/for-you\/(events|news)\//);
  };

  beforeEach(() => {
    cy.visit(APP_ROUTES.tabs.for_you.main);
  });

  describe('For-You Page access and tab-bar navigation ', () => {
    it('should display the welcome card and no errors', () => {
      cy.get('app-welcome-feed-card').should('be.visible');
      cy.get('lib-connectivity-error-handling-card').should('not.exist');
    });

    it('should access the main For You page', () => {
      const forYouTab = APP_ROUTES.tabs.for_you.main;
      cy.visit(APP_ROUTES.tabs.for_you.main);
      cy.url().should('include', forYouTab);
      cy.checkBackButtonAbsent();
    });

    it('should refresh page when tabbar for you is clicked again', () => {
      cy.get('ion-content')
        .find('.inner-scroll')
        .then(($scrollContainer) => {
          $scrollContainer[0].scrollTo(0, $scrollContainer[0].scrollHeight);
        });

      cy.get('[data-cy="tab-for-you-button"]').click();

      cy.get('ion-content')
        .find('.inner-scroll')
        .should(($scrollContainer) => {
          expect($scrollContainer[0].scrollTop).to.equal(0);
        });
    });
  });

  describe('Feed sections', () => {
    beforeEach(() => {
      cy.visit(APP_ROUTES.tabs.for_you.main, {
        onBeforeLoad: setupLocalStorage,
      });
    });

    it('should load feed items in recent section, click one of the first three items, and navigate to its details page', () => {
      //clickFirstAvailableItem('[data-cy="feed-navigation-col"]');
      //assertNavigationToDetailsPage();
    });

    it('should load feed items in recently published, click one of the first three items, and navigate to its details page', () => {
      //clickFirstAvailableItem('[data-cy="feed-infinite-col"]');
      //assertNavigationToDetailsPage();
    });
  });

  describe('Infinite Scroll', () => {
    beforeEach(() => {
      cy.visit(APP_ROUTES.tabs.for_you.main, {
        onBeforeLoad: setupLocalStorage,
      });
    });

    it('should load more items on infinite scroll', () => {
      cy.get('[data-cy="feed-infinite-col"]').then(($items) => {
        cy.get('ion-content')
          .find('.inner-scroll')
          .then(($scrollContainer) => {
            $scrollContainer[0].scrollTo(0, $scrollContainer[0].scrollHeight);

            cy.get('[data-cy="feed-infinite-col"]').should(
              'have.length.greaterThan',
              $items.length,
            );
          });
      });
    });
  });

  describe('Error Handling', () => {
    beforeEach(() => {
      cy.visit(APP_ROUTES.tabs.for_you.main);
    });
    it('should show went wrong error card when one endpoint fails', () => {
      cy.intercept('GET', '**/api/v1/feed-events**', {
        forceNetworkError: true,
      });
      cy.reload();

      cy.get('lib-went-wrong-error-handling-card').should('be.visible');
    });

    it('should show connectivity error-handling card when all endpoints fail', () => {
      const endpoints = [
        '**/feed-events**',
        '**/grouped-feed-events**',
        '**/news**',
      ];

      endpoints.forEach((endpoint, index) => {
        cy.intercept('GET', endpoint, { forceNetworkError: true }).as(
          `getEndpoint${index + 1}`,
        );
      });

      cy.reload();

      cy.get('lib-connectivity-error-handling-card').should('be.visible');
    });
  });

  describe('Settings Navigation', () => {
    it('should access the For You settings page', () => {
      const forYouSettings = APP_ROUTES.tabs.for_you.settings;
      cy.visit(forYouSettings);
      cy.url().should('include', forYouSettings);
      cy.testBackNavigation(APP_ROUTES.tabs.for_you.main);
    });

    it('should access the Module Preferences page', () => {
      const modulePreferences = APP_ROUTES.tabs.for_you.module_preferences;
      cy.visit(modulePreferences);
      cy.url().should('include', modulePreferences);
      cy.testBackNavigation(APP_ROUTES.tabs.for_you.main);
    });

    it('should access the News settings page', () => {
      const newsSettings = APP_ROUTES.tabs.for_you.news.settings;
      cy.visit(newsSettings);
      cy.url().should('include', newsSettings);
      cy.testBackNavigation(APP_ROUTES.tabs.for_you.settings);
    });

    it('should access the Events settings page', () => {
      const eventSettings = APP_ROUTES.tabs.for_you.events.settings;
      cy.visit(eventSettings);
      cy.url().should('include', eventSettings);
      cy.testBackNavigation(APP_ROUTES.tabs.for_you.settings);
    });

    it('should access the App Feedback settings page', () => {
      const appFeedback = APP_ROUTES.tabs.for_you.app_feedback.settings;
      cy.visit(appFeedback);
      cy.url().should('include', appFeedback);
      cy.testBackNavigation(APP_ROUTES.tabs.for_you.settings);
    });
  });

  describe('Details Pages Using Fixture IDs', () => {
    it('should access a specific News details page using ID from fixture', () => {
      const newsDetails = APP_ROUTES.tabs.for_you.news.details(API_IDS.NEWS_ID);
      cy.visit(newsDetails);
      cy.url().should('include', newsDetails);
      cy.testBackNavigation(APP_ROUTES.tabs.for_you.main);
    });

    it('should access a specific Events details page using ID from fixture', () => {
      const eventDetails = APP_ROUTES.tabs.for_you.events.details(
        API_IDS.EVENT_ID,
      );
      cy.visit(eventDetails);
      cy.url().should('include', eventDetails);
      cy.testBackNavigation(APP_ROUTES.tabs.for_you.main);
    });
  });

  describe('App Feedback Pages', () => {
    it('should access the Questionnaire page within App Feedback', () => {
      const questionnaire = APP_ROUTES.tabs.for_you.app_feedback.questionnaire;
      cy.visit(questionnaire);
      cy.url().should('include', questionnaire);
      cy.testBackNavigation(APP_ROUTES.tabs.for_you.app_feedback.settings);
    });

    it('should access the Issue Report page within App Feedback', () => {
      const issueReport = APP_ROUTES.tabs.for_you.app_feedback.issue_report;
      cy.visit(issueReport);
      cy.url().should('include', issueReport);
      cy.testBackNavigation(APP_ROUTES.tabs.for_you.app_feedback.settings);
    });
  });

  describe('Feed Categories', () => {
    it('should access "new" feed category within For You', () => {
      const feedCategory = for_you_feed_category.NEW;
      const newFeedCategory = APP_ROUTES.tabs.for_you.feed(feedCategory);
      cy.visit(newFeedCategory);
      cy.url().should('include', newFeedCategory);
      cy.testBackNavigation(APP_ROUTES.tabs.for_you.main);
    });

    it('should access "upcoming events" feed category within For You', () => {
      const feedCategory = for_you_feed_category.UPCOMING_EVENTS;
      const upcomingEventsFeedCategory =
        APP_ROUTES.tabs.for_you.feed(feedCategory);
      cy.visit(upcomingEventsFeedCategory);
      cy.url().should('include', upcomingEventsFeedCategory);
      cy.testBackNavigation(APP_ROUTES.tabs.for_you.main);
    });
  });
});
