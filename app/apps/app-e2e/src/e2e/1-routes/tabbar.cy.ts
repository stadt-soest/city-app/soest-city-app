import { APP_ROUTES } from '../../common/routes';

describe('Tabbar Navigation', () => {
  describe('Tabbar Accessibility', () => {
    beforeEach(() => {
      cy.visit(APP_ROUTES.tabs.for_you.main);
    });

    it('should navigate to search on tabbar search click', () => {
      cy.get('[data-cy="tab-search-button"]').click();
      cy.url().should('include', APP_ROUTES.tabs.search.main);
    });

    it('should navigate to map on tabbar map click', () => {
      cy.get('[data-cy="tab-map-button"]').click();
      cy.url().should('include', APP_ROUTES.tabs.map.main);
    });

    it('should navigate to categories on tabbar categories click', () => {
      cy.get('[data-cy="tab-categories-button"]').click();
      cy.url().should('include', APP_ROUTES.tabs.category.main);
    });

    it('should navigate to for-you on tabbar for-you click', () => {
      cy.visit(APP_ROUTES.tabs.map.main);
      cy.get('[data-cy="tab-for-you-button"]').click();
      cy.url().should('include', APP_ROUTES.tabs.for_you.main);
    });
  });
});
