export const APP_ROUTES = {
  on_boarding: '/onboarding',
  not_found: '/not-found',
  tabs: {
    for_you: {
      main: '/for-you',
      settings: '/for-you/settings',
      feed: (feedCategory: for_you_feed_category) =>
        `/for-you/feed/${feedCategory}`,
      module_preferences: '/for-you/settings/module-preferences',
      news: {
        details: (id: string) => `/for-you/news/${id}`,
        settings: '/for-you/settings/news',
      },
      events: {
        details: (id: string) => `/for-you/events/${id}`,
        settings: '/for-you/settings/events',
      },
      app_feedback: {
        settings: '/for-you/settings/app-feedback',
        questionnaire: '/for-you/settings/app-feedback/questionnaire',
        issue_report: '/for-you/settings/app-feedback/issue-report',
      },
    },
    search: {
      main: '/search',
      town_hall: {
        all_results: (searchTerm: string) =>
          `/search/results/townhall?searchTerm=${encodeURIComponent(
            searchTerm,
          )}`,
        details: (id: any) => `/search/results/townhall/${id}`,
      },
      news: {
        all_results: (searchTerm: string) =>
          `/search/results/news?searchTerm=${encodeURIComponent(searchTerm)}`,
        details: (id: any) => `/search/results/news/${id}`,
      },
      events: {
        all_results: (searchTerm: string) =>
          `/search/results/events?searchTerm=${encodeURIComponent(searchTerm)}`,
        details: (id: string) => `/search/results/events/${id}`,
      },
    },
    map: {
      main: '/map',
      show_all: (categoryId: string) => `/map/list/${categoryId}`,
      details: (id: string) => `/map/${id}`,
    },
    category: {
      main: '/categories',
      news: {
        dashboard: '/categories/news',
        details: (id: string) => `/categories/news/${id}`,
        settings: '/categories/news/settings',
      },
      events: {
        dashboard: '/categories/events',
        details: (id: string) => `/categories/events/${id}`,
        settings: '/categories/events/settings',
      },
      town_hall: {
        dashboard: '/categories/townhall',
        details: (id: string) => `/categories/townhall/${id}`,
        category: (category_id: string) =>
          `/categories/townhall/category/${category_id}`,
      },
      waste: {
        dashboard: '/categories/waste',
        settings: '/categories/waste/settings',
      },
      parking: {
        dashboard: '/categories/parking',
      },
      report_a_defect: {
        dashboard: '/categories/citizen-participation',
      },
      app_feedback: {
        dashboard: '/categories/app-feedback',
        questionnaire: '/categories/app-feedback/questionnaire',
        issue_report: '/categories/app-feedback/issue-report',
      },
      contact_soest: {
        dashboard: '/categories/contact',
      },
      help: {
        dashboard: '/categories/help',
      },
    },
  },
};

export enum for_you_feed_category {
  NEW = 'new',
  UPCOMING_EVENTS = 'upcoming-events',
}
