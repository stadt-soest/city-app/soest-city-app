export const skipOnboarding = () => {
  cy.window().then((window) => {
    window.localStorage.setItem('CapacitorStorage.finishedOnboarding', 'true');
  });
};
