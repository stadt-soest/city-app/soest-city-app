Cypress.Commands.add('checkBackButtonAbsent', () => {
  cy.getBySel('back-button').should('not.exist');
  cy.getBySel('close-modal-back-button').should('not.exist');
});

Cypress.Commands.add('testBackNavigation', (expectedUrl: string) => {
  cy.getBySel('back-button').click();
  cy.url().should('include', expectedUrl);
});

Cypress.Commands.add('getBySel', (selector, ...args) => {
  return cy.get(`[data-test=${selector}]`, ...args);
});

Cypress.Commands.add('getBySelLike', (selector, ...args) => {
  return cy.get(`[data-test*=${selector}]`, ...args);
});
