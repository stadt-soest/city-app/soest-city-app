import './commands';
import failOnConsoleError from 'cypress-fail-on-console-error';
import { skipOnboarding } from '../common/skip-onboarding';

failOnConsoleError({
  consoleMessages: ['Http failure response for'],
});

beforeEach(() => {
  skipOnboarding();
});
