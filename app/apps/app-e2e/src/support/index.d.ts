declare global {
  namespace Cypress {
    interface Chainable {
      checkBackButtonAbsent(): Chainable<void>;
      testBackNavigation(expectedUrl: string): Chainable<void>;
      getBySel(
        selector: string,
        ...args: any[]
      ): Chainable<JQuery<HTMLElement>>;
      getBySelLike(
        selector: string,
        ...args: any[]
      ): Chainable<JQuery<HTMLElement>>;
    }
  }
}

export {};
