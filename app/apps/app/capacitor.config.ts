import { CapacitorConfig } from '@capacitor/cli';
/// <reference types="@capacitor-community/safe-area" />

const config: CapacitorConfig = {
  appId: 'de.soest.urbo.app',
  appName: 'SoestApp',
  webDir: '../../dist/apps/app/browser',
  server: {
    hostname: 'soestapp.de',
    androidScheme: 'https',
  },
  plugins: {
    SafeArea: {
      enabled: true,
      customColorsForSystemBars: true,
      statusBarColor: '#00000000',
      statusBarContent: 'light',
      navigationBarColor: '#00000000',
      navigationBarContent: 'light',
      offset: 0,
    },
  },
};

export default config;
