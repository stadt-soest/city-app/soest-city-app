import { Capacitor } from '@capacitor/core';

export const unregisterServiceWorkerIfNative = () => {
  if ('serviceWorker' in navigator && Capacitor.isNativePlatform()) {
    navigator.serviceWorker.getRegistrations().then((registrations) => {
      for (const registration of registrations) {
        registration.unregister();
      }
    });

    caches
      ?.keys()
      .then((keyList) => Promise.all(keyList.map((key) => caches.delete(key))));
  }
};
