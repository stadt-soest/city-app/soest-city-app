import {
  enableProdMode,
  ErrorHandler,
  importProvidersFrom,
  isDevMode,
} from '@angular/core';
import { environment } from './environments/environment';
import { Capacitor } from '@capacitor/core';
import { DarkMode, IsDarkModeResult } from '@aparajita/capacitor-dark-mode';
import { SafeArea } from '@capacitor-community/safe-area';
import { FeatureNewsModule, NewsAdapter } from '@sw-code/urbo-feature-news';
import {
  EventAdapter,
  FeatureEventsModule,
} from '@sw-code/urbo-feature-events';
import {
  ESG_ANDROID_STORE_URL,
  ESG_IOS_STORE_URL,
  ESG_WEB_URL,
  FeatureWasteModule,
  WasteAdapter,
} from '@sw-code/urbo-feature-waste';
import {
  CitizenServiceAdapter,
  FeatureCitizenServicesModule,
} from '@sw-code/urbo-feature-citizen-services';
import {
  FeatureParkingModule,
  ParkingAdapter,
} from '@sw-code/urbo-feature-parking';
import { CategoryFilterComponent } from './app/pages/map/components/category-filter/category-filter.component';
import {
  CitizenParticipationModule,
  DEFECT_IDEAS_REPORT_URL,
  URBAN_DEVELOPMENT_URL,
} from '@sw-code/urbo-feature-citizen-participation';
import { FeatureHelpModule } from '@sw-code/urbo-feature-help';
import { APP_CONFIG, APP_CONFIG_TOKEN } from './app/app-tokens';
import {
  AppFeedbackAdapter,
  FeatureAppFeedbackModule,
} from '@sw-code/urbo-feature-app-feedback';
import {
  FeatureLegalModule,
  LEGAL_NOTICE_URL,
  PRIVACY_POLICY_URL,
} from '@sw-code/urbo-feature-legal';
import { PoiCardComponent } from './app/pages/map/components/poi-card/poi-card.component';
import { bootstrapApplication, BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app/app.component';
import { AppRoutingModule } from './app/app-routing.module';
import { provideServiceWorker } from '@angular/service-worker';
import { MatomoModule } from 'ngx-matomo-client';
import { GlobalErrorHandlerService } from './app/global-error-handler.service';
import { RouteReuseStrategy } from '@angular/router';
import { IonicRouteStrategy } from '@ionic/angular';
import { darkModeMediaQuery } from './dark-mode-config';
import { unregisterServiceWorkerIfNative } from './unregister-service-worker';
import {
  Configuration as CoreApiConfiguration,
  CoreApiModule,
} from '@sw-code/urbo-backend-api';
import {
  Configuration as SearchApiConfiguration,
  SearchApiModule,
} from '@sw-code/urbo-search-api';
import {
  ANDROID_STORE_URL,
  IOS_STORE_URL,
  SIGNING_IMAGE_URL,
} from '@sw-code/urbo-ui';
import {
  CoreAdapter,
  CoreModule,
  CoreModuleConfig,
  MAP_STYLE_URL,
} from '@sw-code/urbo-core';
import { DefaultCoreAdapter } from '@sw-code/urbo-core-adapter';
import { DefaultCitizenServiceAdapter } from '@sw-code/urbo-citizen-services-adapter';
import { DefaultEventAdapter } from '@sw-code/urbo-events-adapter';
import { DefaultNewsAdapter } from '@sw-code/urbo-news-adapter';
import { DefaultWasteAdapter } from '@sw-code/urbo-waste-adapter';
import { DefaultFeedbackAdapter } from '@sw-code/urbo-app-feedback-adapter';
import { DefaultParkingAdapter } from '@sw-code/urbo-parking-adapter';

if (environment.production) {
  enableProdMode();
}

const FEATURE_MODULES = [
  FeatureNewsModule.forRoot(),
  FeatureEventsModule.forRoot(),
  FeatureWasteModule.forRoot(),
  FeatureCitizenServicesModule.forRoot(),
  FeatureParkingModule.forRoot({
    initialMapModalComponent: CategoryFilterComponent,
  }),
  CitizenParticipationModule.forRoot(),
  FeatureHelpModule.forRoot(APP_CONFIG.name),
  FeatureAppFeedbackModule.forRoot(),
  FeatureLegalModule.forRoot(),
];

const coreConfig: CoreModuleConfig = {
  ...environment,
  translocoLanguages: ['de', 'en'],
  translocoDefaultLanguage: 'de',
  poiCardComponent: PoiCardComponent,
  poiForYouComponent: PoiCardComponent,
};

const ADAPTER_PROVIDERS = [
  { provide: CoreAdapter, useClass: DefaultCoreAdapter },
  { provide: AppFeedbackAdapter, useClass: DefaultFeedbackAdapter },
  { provide: CitizenServiceAdapter, useClass: DefaultCitizenServiceAdapter },
  { provide: EventAdapter, useClass: DefaultEventAdapter },
  { provide: NewsAdapter, useClass: DefaultNewsAdapter },
  { provide: WasteAdapter, useClass: DefaultWasteAdapter },
  { provide: ParkingAdapter, useClass: DefaultParkingAdapter },
];

const CLIENT_MODULES = [CoreApiModule, SearchApiModule];

const CLIENT_CONFIGURATION = [
  {
    provide: CoreApiConfiguration,
    useFactory: () =>
      new CoreApiConfiguration({ basePath: environment.apiBaseUrl }),
  },
  {
    provide: SearchApiConfiguration,
    useFactory: () =>
      new SearchApiConfiguration({
        basePath: environment.searchApiBaseUrl,
        credentials: {
          BearerAuth: environment.searchApiKey,
        },
      }),
  },
];

bootstrapApplication(AppComponent, {
  providers: [
    importProvidersFrom(
      BrowserModule,
      AppRoutingModule,
      CoreModule.forRoot(coreConfig),
      ...CLIENT_MODULES,
      MatomoModule.forRoot({
        disabled: !environment.production,
        trackerUrl: environment.matomo.url,
        siteId: environment.matomo.siteId,
        enableJSErrorTracking: environment.matomo.jsErrorTrackingEnabled,
        requireConsent: 'none',
      }),
      ...FEATURE_MODULES,
    ),
    ...ADAPTER_PROVIDERS,
    ...CLIENT_CONFIGURATION,
    { provide: ErrorHandler, useClass: GlobalErrorHandlerService },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: APP_CONFIG_TOKEN, useValue: APP_CONFIG },
    {
      provide: PRIVACY_POLICY_URL,
      useValue:
        environment.links.module.featureLegal.privacyPolicyUrl ||
        console.warn('PRIVACY_POLICY_URL is not set'),
    },
    {
      provide: LEGAL_NOTICE_URL,
      useValue:
        environment.links.module.featureLegal.legalNoticeUrl ||
        console.warn('LEGAL_NOTICE_URL is not set'),
    },
    {
      provide: URBAN_DEVELOPMENT_URL,
      useValue:
        environment.links.module.featureCitizenParticipation
          .urbanDevelopmentUrl ||
        console.warn('URBAN_DEVELOPMENT_URL is not set'),
    },
    {
      provide: DEFECT_IDEAS_REPORT_URL,
      useValue:
        environment.links.module.featureCitizenParticipation
          .defectIdeasReportUrl ||
        console.warn('DEFECT_IDEAS_REPORT_URL is not set'),
    },
    {
      provide: ANDROID_STORE_URL,
      useValue:
        environment.links.general.androidStoreUrl ||
        console.warn('ANDROID_STORE_URL is not set'),
    },
    {
      provide: IOS_STORE_URL,
      useValue:
        environment.links.general.iosStoreUrl ||
        console.warn('IOS_STORE_URL is not set'),
    },
    {
      provide: ESG_IOS_STORE_URL,
      useValue:
        environment.links.module.featureWaste.esgIosStoreUrl ||
        console.warn('ESG_IOS_STORE_URL is not set'),
    },
    {
      provide: ESG_ANDROID_STORE_URL,
      useValue:
        environment.links.module.featureWaste.esgAndroidStoreUrl ||
        console.warn('ESG_ANDROID_STORE_URL is not set'),
    },
    {
      provide: ESG_WEB_URL,
      useValue:
        environment.links.module.featureWaste.esgWebUrl ||
        console.warn('ESG_WEB_URL is not set'),
    },
    {
      provide: MAP_STYLE_URL,
      useFactory: () =>
        darkModeMediaQuery.matches
          ? environment.map.styleUrlDark
          : environment.map.styleUrlLight,
    },
    {
      provide: SIGNING_IMAGE_URL,
      useFactory: () =>
        darkModeMediaQuery.matches
          ? environment.darkmodeSigningImage
          : environment.lightmodeSigningImage,
    },
    provideServiceWorker('ngsw-worker.js', {
      enabled: !isDevMode(),
      registrationStrategy: 'registerWhenStable:30000',
    }),
  ],
});

unregisterServiceWorkerIfNative();

try {
  if (Capacitor.getPlatform() === 'android') {
    DarkMode.isDarkMode().then(async (result: IsDarkModeResult) => {
      await SafeArea.enable({
        config: {
          customColorsForSystemBars: true,
          statusBarColor: result.dark ? '#343C38' : '#EEE7DC',
          statusBarContent: result.dark ? 'light' : 'dark',
          navigationBarColor: result.dark ? '#445551' : '#D8D0C5',
          navigationBarContent: result.dark ? 'light' : 'dark',
        },
      });
    });
  }
} catch (err: any) {
  console.error('Could not set status bar style for Android', err);
}
