import { APP_CONFIG } from '../app/app-tokens';

export const baseEnvironment = {
  production: false,
  appName: APP_CONFIG.name,
  searchApiKey:
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOm51bGwsImFwaUtleVVpZCI6ImI0ZTAxMWQ0LTcxNzItNGZjOC1hMTBlLWU1NDVjZDUzNjI2NCIsInNlYXJjaFJ1bGVzIjp7IioiOnsiZmlsdGVyIjoidGVuYW50SWRlbnRpZmllciA9IHNvZXN0In19fQ.ALK6HUmfse4mg8Y38bgsB56q7uuD4_0BXXZ7W0EWonE',
  darkmodeSigningImage: 'assets/signing/dark_swcode.svg',
  lightmodeSigningImage: 'assets/signing/light_swcode.svg',
  matomo: {
    url: 'https://analytics.swcode.io',
    siteId: '11',
    jsErrorTrackingEnabled: true,
  },
  map: {
    marker: {
      color: '#A50000',
      scale: 0.6,
    },
    antialias: true,
    pixelRatio: window.devicePixelRatio || 1,
    styleUrlDark: `https://tiles.openfreemap.org/styles/positron`,
    styleUrlLight: `https://tiles.openfreemap.org/styles/bright`,
    center: [8.106023420256752, 51.5723841235784] as [number, number],
    zoom: 14,
  },
  links: {
    general: {
      iosStoreUrl: 'https://apps.apple.com/lu/app/soestapp/id6476777998',
      androidStoreUrl:
        'https://play.google.com/store/apps/details?id=de.soest.urbo.app',
    },
    module: {
      featureCitizenParticipation: {
        urbanDevelopmentUrl:
          'https://beteiligung.nrw.de/portal/soest/beteiligung/themen?status=NA&format=Bauleitplan',
        defectIdeasReportUrl:
          'https://beteiligung.nrw.de/portal/soest/beteiligung/themen/1001823',
      },
      featureLegal: {
        privacyPolicyUrl: 'https://swcode.io/datenschutzerklaerung-soest-app',
        legalNoticeUrl: 'https://www.soest.de/impressum-soest-app',
      },
      featureWaste: {
        esgIosStoreUrl:
          'https://apps.apple.com/de/app/abfall-app-kreis-soest/id1673679569',
        esgAndroidStoreUrl:
          'https://play.google.com/store/apps/details?id=com.devlabor.mobile.deponie.soest&hl=gsw&gl=US&pli=1',
        esgWebUrl: 'https://www.esg-soest.de/soest',
      },
    },
  },
};
