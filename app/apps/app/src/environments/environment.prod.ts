import { baseEnvironment } from './enviroment.base';

export const environment = {
  ...baseEnvironment,
  production: true,
  apiBaseUrl: 'https://api.urbo.digital',
  searchApiBaseUrl: 'https://search-urbo.swcode.io',
  matomo: {
    url: 'https://analytics.stadtsoest.de',
    siteId: '10',
    jsErrorTrackingEnabled: true,
  },
};
