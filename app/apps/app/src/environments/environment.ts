import { baseEnvironment } from './enviroment.base';

export const environment = {
  ...baseEnvironment,
  apiBaseUrl: 'http://localhost:8080',
  searchApiBaseUrl: 'http://localhost:7700',
  searchApiKey: 'masterKey',
};
