import { baseEnvironment } from './enviroment.base';

export const environment = {
  ...baseEnvironment,
  apiBaseUrl: '/api',
  searchApiBaseUrl: '/search-api',
};
