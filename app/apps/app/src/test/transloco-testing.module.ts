import en from '../assets/i18n/en.json';
import de from '../assets/i18n/de.json';
import {
  TranslocoTestingModule,
  TranslocoTestingOptions,
} from '@jsverse/transloco';

export const getTranslocoModule = (options: TranslocoTestingOptions = {}) =>
  TranslocoTestingModule.forRoot({
    langs: { en, de },
    translocoConfig: {
      availableLangs: ['en', 'de'],
      defaultLang: 'en',
    },
    preloadLangs: true,
    ...options,
  });
