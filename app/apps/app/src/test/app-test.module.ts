import { ModuleWithProviders, NgModule } from '@angular/core';
import { APP_CONFIG, APP_CONFIG_TOKEN } from '../app/app-tokens';
import {
  LEGAL_NOTICE_URL,
  PRIVACY_POLICY_URL,
} from '@sw-code/urbo-feature-legal';
import { environment } from '../environments/environment';
import {
  DEFECT_IDEAS_REPORT_URL,
  URBAN_DEVELOPMENT_URL,
} from '@sw-code/urbo-feature-citizen-participation';
import {
  ANDROID_STORE_URL,
  IOS_STORE_URL,
  SIGNING_IMAGE_URL,
} from '@sw-code/urbo-ui';
import {
  ESG_ANDROID_STORE_URL,
  ESG_IOS_STORE_URL,
  ESG_WEB_URL,
} from '@sw-code/urbo-feature-waste';
import { MAP_STYLE_URL } from '@sw-code/urbo-core';
import { darkModeMediaQuery } from '../dark-mode-config';

@NgModule({})
export class AppTestModule {
  static forRoot(): ModuleWithProviders<AppTestModule> {
    return {
      ngModule: AppTestModule,
      providers: [
        { provide: APP_CONFIG_TOKEN, useValue: APP_CONFIG },
        {
          provide: PRIVACY_POLICY_URL,
          useValue:
            environment.links.module.featureLegal.privacyPolicyUrl ||
            'default-privacy-policy-url',
        },
        {
          provide: LEGAL_NOTICE_URL,
          useValue:
            environment.links.module.featureLegal.legalNoticeUrl ||
            'default-legal-notice-url',
        },
        {
          provide: URBAN_DEVELOPMENT_URL,
          useValue:
            environment.links.module.featureCitizenParticipation
              .urbanDevelopmentUrl || 'default-urban-development-url',
        },
        {
          provide: DEFECT_IDEAS_REPORT_URL,
          useValue:
            environment.links.module.featureCitizenParticipation
              .defectIdeasReportUrl || 'default-defect-report-url',
        },
        {
          provide: ANDROID_STORE_URL,
          useValue:
            environment.links.general.androidStoreUrl ||
            'default-android-store-url',
        },
        {
          provide: IOS_STORE_URL,
          useValue:
            environment.links.general.iosStoreUrl || 'default-ios-store-url',
        },
        {
          provide: ESG_IOS_STORE_URL,
          useValue:
            environment.links.module.featureWaste.esgIosStoreUrl ||
            'default-esg-ios-store-url',
        },
        {
          provide: ESG_ANDROID_STORE_URL,
          useValue:
            environment.links.module.featureWaste.esgAndroidStoreUrl ||
            'default-esg-android-store-url',
        },
        {
          provide: ESG_WEB_URL,
          useValue:
            environment.links.module.featureWaste.esgWebUrl ||
            'default-esg-web-url',
        },
        {
          provide: MAP_STYLE_URL,
          useFactory: () =>
            darkModeMediaQuery.matches
              ? environment.map.styleUrlDark
              : environment.map.styleUrlLight,
        },
        {
          provide: SIGNING_IMAGE_URL,
          useFactory: () =>
            darkModeMediaQuery.matches
              ? environment.darkmodeSigningImage
              : environment.lightmodeSigningImage,
        },
      ],
    };
  }
}
