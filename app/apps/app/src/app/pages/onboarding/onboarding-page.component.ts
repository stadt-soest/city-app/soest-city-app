import {
  Component,
  CUSTOM_ELEMENTS_SCHEMA,
  DestroyRef,
  ElementRef,
  inject,
  OnDestroy,
  OnInit,
  Type,
  ViewChild,
} from '@angular/core';
import { IonContent, IonicSlides } from '@ionic/angular/standalone';
import { IntroOnboardingComponent } from './components/intro-onboarding.component';
import { NgComponentOutlet, NgFor, NgIf } from '@angular/common';
import { DiscoverOnboardingComponent } from './components/discover-onboarding.component';
import {
  CustomSelectableIconTextButtonTypes,
  LoadingIndicatorComponent,
} from '@sw-code/urbo-ui';
import {
  ModuleInfo,
  ModuleRegistryService,
  OnboardingService,
  ROUTES,
} from '@sw-code/urbo-core';
import { Router } from '@angular/router';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding-page.component.html',
  styleUrls: ['./onboarding-page.component.scss'],
  imports: [
    IonContent,
    IntroOnboardingComponent,
    NgFor,
    NgIf,
    NgComponentOutlet,
    DiscoverOnboardingComponent,
    LoadingIndicatorComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class OnboardingPageComponent implements OnInit, OnDestroy {
  @ViewChild('swiper', { static: true }) swiperContainer!: ElementRef;
  swiperModules = [IonicSlides];
  sliders: {
    component?: Type<any>;
    moduleInfo: ModuleInfo;
    isLaterSelected: boolean;
    isActivateSelected: boolean;
  }[] = [];
  isLoading = false;
  currentIndex = 0;
  private readonly router = inject(Router);
  private readonly moduleRegistryService = inject(ModuleRegistryService);
  private readonly onboardingService = inject(OnboardingService);
  private readonly destroyRef = inject(DestroyRef);

  async ngOnInit() {
    this.subscribeToOnboardingEvents();
    await this.initializeSliders();
  }

  ngOnDestroy() {
    if (this.swiperContainer?.nativeElement.swiper) {
      this.swiperContainer.nativeElement.swiper.destroy(true, true);
    }
  }

  async navigateToNextSlide(
    moduleInfo?: ModuleInfo,
    type?: CustomSelectableIconTextButtonTypes,
  ) {
    if (moduleInfo && type) {
      const isVisible = type === CustomSelectableIconTextButtonTypes.activate;
      await this.moduleRegistryService.saveModuleVisibilitySettings(
        moduleInfo,
        isVisible,
      );
    }
    this.swiperContainer.nativeElement.swiper.slideNext();
    this.updateCurrentIndex();
  }

  private async initializeSliders() {
    this.sliders = this.moduleRegistryService.getOnboardingComponents();
    await this.moduleRegistryService.initializeDefaultModuleSettings();
    await this.onboardingService.setRadioButtonSelection(this.sliders);
    this.swiperContainer.nativeElement.swiper.on('slideChange', () => {
      this.updateCurrentIndex();
    });
  }

  private subscribeToOnboardingEvents() {
    this.onboardingService.navigateNext
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(async () => {
        await this.navigateToNextSlide();
      });

    this.onboardingService.finishOnboardingEmitter
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(async () => {
        await this.finishOnboarding();
      });
  }

  private async finishOnboarding() {
    this.isLoading = true;
    try {
      await this.onboardingService.setFinishedOnboarding();
      await this.router.navigateByUrl(ROUTES.forYou, { replaceUrl: true });
    } catch (error) {
      console.error('Error finishing onboarding:', error);
    } finally {
      this.isLoading = false;
    }
  }

  private updateCurrentIndex() {
    this.currentIndex = this.swiperContainer.nativeElement.swiper.activeIndex;
  }
}
