import { OnboardingPageComponent } from './onboarding-page.component';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ChangeDetectorRef, Component } from '@angular/core';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import {
  CoreTestModule,
  createModuleInfo,
  ModuleCategory,
  ModuleInfo,
  ModuleRegistryService,
  SettingsPersistenceService,
} from '@sw-code/urbo-core';
import { AppTestModule } from '../../../test/app-test.module';
import { UITestModule } from '@sw-code/urbo-ui';

@Component({
  selector: 'app-mock-component',
  template: '<p>Mock Component</p>',
  standalone: false,
})
export class MockComponent {}

describe('OnboardingPage', () => {
  let component: OnboardingPageComponent;
  let fixture: ComponentFixture<OnboardingPageComponent>;
  let moduleRegistryService: ModuleRegistryService;
  let changeDetectorRef: ChangeDetectorRef;
  const mockSliderComponent = MockComponent;
  let settingsPersistenceServiceMock: jest.Mocked<SettingsPersistenceService>;

  const mockModuleInfo: ModuleInfo = createModuleInfo({
    name: 'name',
    category: ModuleCategory.information,
    icon: 'icon',
    baseRoutingPath: '/baseRoutingPath',
  });
  const mockSliders = [
    {
      component: mockSliderComponent,
      moduleInfo: mockModuleInfo,
      isActivateSelected: false,
      isLaterSelected: true,
    },
  ];

  beforeEach(waitForAsync(() => {
    settingsPersistenceServiceMock = {
      saveSettings: jest.fn(),
      getSettings: jest.fn(),
      getEnabledSettingsKeys: jest.fn(),
      getSettingsValues: jest.fn().mockReturnValue([true]),
    } as any;

    TestBed.configureTestingModule({
      declarations: [MockComponent],
      imports: [
        OnboardingPageComponent,
        AppTestModule.forRoot(),
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        RouterTestingModule.withRoutes([
          { path: 'tabs/for-you', component: MockComponent },
        ]),
      ],
      providers: [
        {
          provide: SettingsPersistenceService,
          useValue: settingsPersistenceServiceMock,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(OnboardingPageComponent);

    component = fixture.componentInstance;
    component.sliders = mockSliders;
    moduleRegistryService = TestBed.inject(ModuleRegistryService);
    changeDetectorRef = fixture.componentRef.injector.get(ChangeDetectorRef);
    changeDetectorRef.detectChanges();
  }));

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    test('ngOnInit should initialize sliders', () => {
      jest
        .spyOn(moduleRegistryService, 'getOnboardingComponents')
        .mockReturnValue(mockSliders);

      component.ngOnInit();

      expect(component.sliders.length).toBe(1);
      expect(component.sliders[0].component).toBe(MockComponent);
    });
  });

  describe('navigateToNextSlide', () => {
    it('navigateToNextSlide should call slideNext on swiper', () => {
      const slideNextSpy = jest.fn();
      component.swiperContainer = {
        nativeElement: {
          swiper: { slideNext: slideNextSpy, destroy: jest.fn() },
        },
      } as any;

      component.navigateToNextSlide();

      expect(slideNextSpy).toHaveBeenCalled();
    });
  });

  describe('render', () => {
    it('should render correct number of slides', () => {
      fixture.detectChanges();

      const slides = fixture.debugElement.queryAll(By.css('swiper-slide'));
      expect(slides.length).toBe(component.sliders.length + 2);
    });
  });
});
