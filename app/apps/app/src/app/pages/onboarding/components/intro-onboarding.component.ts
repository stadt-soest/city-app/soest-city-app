import { Component, inject, OnInit } from '@angular/core';
import {
  OnboardingButton,
  OnboardingComponent,
  UIModule,
} from '@sw-code/urbo-ui';
import { OnboardingService } from '@sw-code/urbo-core';
import { AppConfig } from '../../../interfaces/app-config.model';
import { APP_CONFIG_TOKEN } from '../../../app-tokens';

@Component({
  selector: 'app-intro-onboarding',
  template: `
    <lib-onboarding
      *transloco="let t"
      [imageUrl]="imageUrl"
      [title]="t(title, { app_name: appName })"
      [content]="t(content)"
      [buttons]="buttons"
    >
    </lib-onboarding>
  `,
  imports: [UIModule, OnboardingComponent],
})
export class IntroOnboardingComponent implements OnInit {
  imageUrl = 'assets/illustrations/hi.svg';
  title = 'onboarding.intro.header';
  content = 'onboarding.intro.content';
  buttons: OnboardingButton[] = [];
  appName: string;
  private readonly onboardingService = inject(OnboardingService);
  private readonly appConfig = inject<AppConfig>(APP_CONFIG_TOKEN);

  constructor() {
    this.appName = this.appConfig.name;
  }

  ngOnInit() {
    this.buttons = [
      {
        icon: 'close',
        text: 'onboarding.intro.skip',
        action: () => this.onboardingService.finishOnboarding(),
        selected: false,
        id: 'skip-button',
      },
      {
        icon: 'arrow_forward',
        text: 'onboarding.intro.next',
        action: () => this.onboardingService.navigateToNextSlide(),
        selected: false,
        id: 'next-button',
      },
    ];
  }
}
