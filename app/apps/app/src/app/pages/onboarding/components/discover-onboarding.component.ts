import { Component, inject, OnInit } from '@angular/core';
import {
  OnboardingButton,
  OnboardingComponent,
  UIModule,
} from '@sw-code/urbo-ui';
import { OnboardingService } from '@sw-code/urbo-core';

@Component({
  selector: 'app-discover-onboarding',
  template: `
    <lib-onboarding
      *transloco="let t"
      [imageUrl]="imageUrl"
      [title]="t(title)"
      [content]="t(content)"
      [buttons]="buttons"
    ></lib-onboarding>
  `,
  imports: [UIModule, OnboardingComponent],
})
export class DiscoverOnboardingComponent implements OnInit {
  imageUrl = 'assets/illustrations/discover.svg';
  title = 'onboarding.header';
  content = 'onboarding.content';
  buttons: OnboardingButton[] = [];
  private readonly onboardingService = inject(OnboardingService);

  ngOnInit() {
    this.buttons = [
      {
        icon: 'arrow_forward',
        text: 'onboarding.start',
        action: () => this.onboardingService.finishOnboarding(),
        selected: false,
        id: 'finish-button',
      },
    ];
  }
}
