import { Component, inject } from '@angular/core';
import { RouterLink } from '@angular/router';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  CondensedHeaderLayoutComponent,
  CustomButtonColorScheme,
  CustomButtonType,
  CustomSelectableIconTextButton,
  CustomVerticalButtonComponent,
  NotificationCardComponent,
} from '@sw-code/urbo-ui';
import { IonGrid, IonRouterLink } from '@ionic/angular/standalone';
import { NavigationService, ROUTES } from '@sw-code/urbo-core';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found-page.component.html',
  styleUrls: ['./not-found-page.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    IonGrid,
    NotificationCardComponent,
    CustomVerticalButtonComponent,
    RouterLink,
    IonRouterLink,
  ],
})
export class NotFoundPageComponent {
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
  protected readonly routes = ROUTES;
  private readonly navigationService = inject(NavigationService);
  notificationButtons: CustomSelectableIconTextButton[] = [
    {
      icon: 'search',
      text: 'not_found.to_search',
      selected: false,
      applySelectionStyle: false,
      color: 'var(--On-Error-Foreground, #FFF)',
      onSelect: async () => {
        await this.navigationService.navigateToSearchTab(true);
      },
    },
  ];

  async goToApp() {
    await this.navigationService.navigateToForYou(true);
  }
}
