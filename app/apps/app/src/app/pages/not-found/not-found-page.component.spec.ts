import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NotFoundPageComponent } from './not-found-page.component';
import { CoreTestModule, NavigationService } from '@sw-code/urbo-core';
import { UITestModule } from '@sw-code/urbo-ui';

describe('NotFoundPage', () => {
  let component: NotFoundPageComponent;
  let fixture: ComponentFixture<NotFoundPageComponent>;
  let navigationService: NavigationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NotFoundPageComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(NotFoundPageComponent);
    component = fixture.componentInstance;
    navigationService = TestBed.inject(NavigationService);
    jest
      .spyOn(navigationService, 'navigateToSearchTab')
      .mockImplementation(() => Promise.resolve(true));
    jest
      .spyOn(navigationService, 'navigateToForYou')
      .mockImplementation(() => Promise.resolve(true));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to search tab when notification button is selected', async () => {
    await component.notificationButtons[0].onSelect();
    expect(navigationService.navigateToSearchTab).toHaveBeenCalledWith(true);
  });

  it('should navigate to ForYou page when big button is clicked', async () => {
    await component.goToApp();
    expect(navigationService.navigateToForYou).toHaveBeenCalledWith(true);
  });
});
