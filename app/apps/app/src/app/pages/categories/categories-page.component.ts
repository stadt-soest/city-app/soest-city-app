import {
  Component,
  DestroyRef,
  inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  CondensedHeaderLayoutComponent,
  NavigationListComponent,
} from '@sw-code/urbo-ui';
import {
  MenuItemGroup,
  MenuService,
  TabRefreshService,
} from '@sw-code/urbo-core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'app-categories',
  templateUrl: 'categories-page.component.html',
  imports: [
    TranslocoDirective,
    NavigationListComponent,
    CondensedHeaderLayoutComponent,
  ],
})
export class CategoriesPageComponent implements OnInit {
  @ViewChild(CondensedHeaderLayoutComponent)
  condensedHeaderLayout!: CondensedHeaderLayoutComponent;
  menuItemGroups: MenuItemGroup[] = [];
  private readonly menuService = inject(MenuService);
  private readonly refreshService = inject(TabRefreshService);
  private readonly destroyRef = inject(DestroyRef);
  private readonly scrollToTopDuration: number = 500;

  ngOnInit(): void {
    this.menuItemGroups = this.menuService.getMenuItems(
      (moduleInfo) => moduleInfo.showInCategoriesPage,
    );
    this.subscribeToTabRefresh();
  }

  async refreshView() {
    await this.condensedHeaderLayout.scrollToTop(this.scrollToTopDuration);
  }

  private subscribeToTabRefresh() {
    this.refreshService.refresh$
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(async () => {
        await this.refreshView();
      });
  }
}
