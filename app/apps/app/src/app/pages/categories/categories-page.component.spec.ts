import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CategoriesPageComponent } from './categories-page.component';
import {
  CoreTestModule,
  MenuService,
  ModuleCategory,
} from '@sw-code/urbo-core';
import { getTranslocoModule } from '../../../test/transloco-testing.module';

describe('CategoriesPage', () => {
  let component: CategoriesPageComponent;
  let fixture: ComponentFixture<CategoriesPageComponent>;
  let menuService: jest.Mocked<MenuService>;

  beforeEach(async () => {
    const menuSpy = {
      getMenuItems: jest.fn(),
    };

    await TestBed.configureTestingModule({
      providers: [{ provide: MenuService, useValue: menuSpy }],
      imports: [
        CategoriesPageComponent,
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(CategoriesPageComponent);
    component = fixture.componentInstance;
    menuService = TestBed.inject(MenuService) as jest.Mocked<MenuService>;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should load menu items from service', () => {
      const mockMenuItems = [
        {
          category: ModuleCategory.information,
          menuItems: [
            { title: 'Item 1', baseRoutingPath: '/path1', icon: 'icon' },
          ],
        },
      ];

      menuService.getMenuItems.mockReturnValue(mockMenuItems);
      component.ngOnInit();
      expect(component.menuItemGroups).toEqual(mockMenuItems);
    });
  });

  describe('HTML Rendering', () => {
    it('should display correct title', async () => {
      const compiled = fixture.nativeElement;
      const titleElement = compiled.querySelector('ion-title');
      expect(titleElement.textContent).toContain('Categories');
    });
  });
});
