import { Component, DestroyRef, inject, Input, OnInit } from '@angular/core';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import {
  IonContent,
  IonHeader,
  IonSkeletonText,
  ModalController,
} from '@ionic/angular/standalone';
import {
  CondensedHeaderLayoutComponent,
  CustomButtonType,
  CustomModalHeaderComponent,
  CustomSelectableIconTextButton,
  NotificationCardComponent,
  RefresherComponent,
} from '@sw-code/urbo-ui';
import { AsyncPipe, NgFor, NgIf } from '@angular/common';
import { PoiCardComponent } from '../components/poi-card/poi-card.component';
import { ActivatedRoute, RouterLink } from '@angular/router';
import {
  Category,
  CORE_MODULE_CONFIG,
  DeviceService,
  GeolocationService,
  MapNavigationService,
  MapService,
  NavService,
  PageType,
  Poi,
  PoiFeedItem,
  PoiService,
  SimplifiedPermissionState,
  TitleService,
} from '@sw-code/urbo-core';
import { firstValueFrom, of, switchMap, take } from 'rxjs';
import { CategoryFilterComponent } from '../components/category-filter/category-filter.component';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'app-poi-list',
  templateUrl: './poi-list-page.component.html',
  styleUrls: ['./poi-list-page.component.scss'],
  imports: [
    TranslocoDirective,
    IonHeader,
    CustomModalHeaderComponent,
    IonContent,
    NgIf,
    NotificationCardComponent,
    NgFor,
    PoiCardComponent,
    IonSkeletonText,
    CondensedHeaderLayoutComponent,
    RefresherComponent,
    RouterLink,
    AsyncPipe,
  ],
})
export class PoiListPageComponent implements OnInit {
  @Input() categoryId = '';
  @Input() categoryName = '';
  @Input() categories: Category[] = [];
  poiFeedItems: PoiFeedItem[] = [];
  pageTitle = '';
  pageTitleParams: any = {};
  selectedCategoryId: string | null = null;
  isLoading = true;
  usingCurrentLocation = false;
  public translateService = inject(TranslocoService);
  protected readonly customButtonType = CustomButtonType;
  protected readonly simplifiedPermissionState = SimplifiedPermissionState;
  protected geolocationService = inject(GeolocationService);
  permissionButton: CustomSelectableIconTextButton[] = [
    {
      icon: 'open_in_new',
      text: 'map.location_permission_card.open',
      selected: false,
      applySelectionStyle: false,
      onSelect: async () => await this.geolocationService.requestPermissions(),
    },
  ];
  protected deviceService = inject(DeviceService);
  private readonly poiService = inject(PoiService);
  private readonly route = inject(ActivatedRoute);
  private readonly destroyRef = inject(DestroyRef);
  private readonly mapService = inject(MapService);
  private readonly titleService = inject(TitleService);
  private readonly modalCtrl = inject(ModalController);
  private readonly mapNavigationService = inject(MapNavigationService);
  private readonly navService = inject(NavService);
  private readonly config = inject(CORE_MODULE_CONFIG);

  async ngOnInit() {
    this.mapNavigationService.popPageList$.pipe(take(1)).subscribe(async () => {
      await this.pop();
    });
    this.setupLocationSubscription();
    if (this.categoryId) {
      this.selectedCategoryId = this.categoryId;
      await this.fetchAndLoadPois();
    } else {
      this.route.paramMap.pipe(take(1)).subscribe(async (params) => {
        this.selectedCategoryId = params.get('category-id');
        if (this.selectedCategoryId) {
          await this.fetchAndLoadPois();
        }
      });
    }
  }

  ionViewWillEnter() {
    Promise.resolve().then(() => {
      this.mapNavigationService.setIsOnCategoryListPage(true);
    });
  }

  async pop() {
    this.mapNavigationService.setIsOnCategoryListPage(false);
    this.mapNavigationService.setUpdateMap(
      '',
      '',
      [PageType.filterList],
      PageType.filterList,
      this.pageTitle,
    );
    await this.navService.pushPage(
      CategoryFilterComponent,
      {
        categories: this.categories,
      },
      {
        animated: true,
        direction: 'back',
      },
    );
  }

  async doRefresh(event: any): Promise<void> {
    this.poiFeedItems = [];
    await this.fetchAndLoadPois();
    event.target.complete();
  }

  getCategoryName(categoryId: string) {
    const firstPoiWithDistance = this.poiFeedItems[0];
    if (firstPoiWithDistance?.categories) {
      return firstPoiWithDistance.categories
        .find((category) => category.id === categoryId)
        ?.getLocalizedName(this.translateService.getActiveLang());
    }
    return null;
  }

  setPageTitle(currentLocation?: { lat: number; long: number }) {
    if (this.poiFeedItems.length > 0) {
      const categoryName = this.selectedCategoryId
        ? this.getCategoryName(this.selectedCategoryId)
        : null;
      this.pageTitle = this.determineTitleKey(currentLocation);
      this.pageTitleParams = { term: categoryName };
    }
  }

  async close() {
    const selectedCategory = this.categories.find(
      (category) => category.id === this.categoryId,
    );

    this.mapNavigationService.setIsOnCategoryListPage(false);
    await this.modalCtrl.dismiss({
      categoryId: this.selectedCategoryId,
      selectedCategories: [this.categoryName],
      categoryName: this.pageTitle,
      pageType: PageType.poiList,
      updateMarkers: true,
      selectedCategory,
    });
  }

  private setupLocationSubscription(): void {
    this.geolocationService.permissionState$
      .pipe(
        takeUntilDestroyed(this.destroyRef),
        switchMap((permissionState) => {
          if (permissionState === SimplifiedPermissionState.granted) {
            this.usingCurrentLocation = true;
            return this.geolocationService.getCurrentLocation();
          } else {
            this.usingCurrentLocation = false;
            return of(this.mapService.getCurrentCenter());
          }
        }),
      )
      .subscribe((currentLocation) => {
        this.loadPoisByCategoryWithDistance(currentLocation ?? undefined);
      });
  }

  private async fetchAndLoadPois() {
    const currentLocation = await this.getCurrentLocation();
    this.loadPoisByCategoryWithDistance(currentLocation ?? undefined);
  }

  private async getCurrentLocation(): Promise<{
    lat: number;
    long: number;
  } | null> {
    const permissionState = await firstValueFrom(
      this.geolocationService.permissionState$.pipe(take(1)),
    );
    if (permissionState === SimplifiedPermissionState.granted) {
      const location = await this.geolocationService.getCurrentLocation();
      return location || null;
    } else {
      if (this.mapService.getCurrentCenter()) {
        return this.mapService.getCurrentCenter();
      }
      return null;
    }
  }

  private loadPoisByCategoryWithDistance(currentLocation?: {
    lat: number;
    long: number;
  }) {
    if (!this.selectedCategoryId) {
      return;
    }

    this.isLoading = true;
    this.poiService
      .getPoisWithDistance([this.selectedCategoryId], currentLocation)
      .pipe(take(1))
      .subscribe({
        next: (poisWithDistance) => {
          this.poiFeedItems = poisWithDistance.map((poi) =>
            Poi.createFeedItem(poi, {
              cardComponent: this.config.poiCardComponent,
              forYouComponent: this.config.poiForYouComponent,
            }),
          );
          this.setPageTitle(currentLocation);
          this.updateTitle();
          this.isLoading = false;
          this.mapNavigationService.setUpdateMap(
            this.selectedCategoryId ?? '',
            '',
            [PageType.poiList],
            PageType.poiList,
            this.pageTitle,
          );
        },
        error: (error) => {
          console.error('Failed to load POIs with distance', error);
          this.isLoading = false;
        },
      });
  }

  private determineTitleKey(currentLocation?: {
    lat: number;
    long: number;
  }): string {
    if (!currentLocation) {
      return 'map.poi_details.results_no_distance';
    }
    return this.usingCurrentLocation
      ? 'map.poi_details.results'
      : 'map.poi_details.results_center';
  }

  private updateTitle() {
    if (this.categoryId) {
      this.pageTitle = this.getCategoryName(this.categoryId) ?? '';
    } else {
      this.translateService
        .selectTranslate(this.pageTitle, this.pageTitleParams, '')
        .pipe(take(1))
        .subscribe((translatedTitle) => {
          this.titleService.setTitle(translatedTitle);
        });
    }
  }
}
