import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PoiListPageComponent } from './poi-list-page.component';
import {
  CoreTestModule,
  GeolocationService,
  MAP_STYLE_URL,
  MapConfigService,
  MapService,
  Poi,
  PoiService,
  SimplifiedPermissionState,
} from '@sw-code/urbo-core';
import { of, throwError } from 'rxjs';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../../test/transloco-testing.module';
import { ActivatedRoute } from '@angular/router';

describe('PoiListPage', () => {
  let component: PoiListPageComponent;
  let fixture: ComponentFixture<PoiListPageComponent>;

  beforeEach(async () => {
    const mockGeolocationService = {
      permissionState$: of(SimplifiedPermissionState.granted),
      getCurrentLocation: jest.fn().mockResolvedValue({ lat: 0, long: 0 }),
    };

    await TestBed.configureTestingModule({
      imports: [
        PoiListPageComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        MapService,
        MapConfigService,
        { provide: MAP_STYLE_URL, useValue: 'www.map-style-url.com' },
        { provide: PoiService, useValue: mockPoiService },
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        { provide: GeolocationService, useValue: mockGeolocationService },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(PoiListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load POIs on init if permission granted', async () => {
    const poisWithDistance = [
      {
        poi: {
          id: '1',
          name: 'Test POI',
          dateModified: new Date().toString(),
          categories: [],
        } as unknown as Poi,
        distance: 100,
      },
    ];
    mockPoiService.getPoisWithDistance.mockReturnValue(of(poisWithDistance));

    await component.ngOnInit();
    fixture.detectChanges();

    await fixture.whenStable();

    expect(mockPoiService.getPoisWithDistance).toHaveBeenCalledWith(['123'], {
      lat: 0,
      long: 0,
    });

    const components = {
      cardComponent: undefined as any,
      forYouComponent: undefined as any,
    };

    const expectedPoiFeedItems = poisWithDistance.map((poiWithDistance) =>
      Poi.createFeedItem(poiWithDistance, components),
    );

    expect(component.poiFeedItems).toEqual(expectedPoiFeedItems);
  });

  it('should handle error when loading POIs fails', async () => {
    const consoleSpy = jest
      .spyOn(console, 'error')
      .mockImplementation(jest.fn());

    mockPoiService.getPoisWithDistance.mockImplementation(() => {
      return throwError(() => new Error('Failed to load'));
    });

    await component.ngOnInit();
    fixture.detectChanges();
    await fixture.whenStable();

    expect(consoleSpy).toHaveBeenCalledWith(
      'Failed to load POIs with distance',
      expect.any(Error),
    );
  });
});

const mockPoiService = {
  getPoisWithDistance: jest.fn().mockReturnValue(of()),
};

const mockActivatedRoute = {
  paramMap: of(new Map([['category-id', '123']])),
};
