import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { DetailsSkeletonComponent } from './details-skeleton.component';
import { UITestModule } from '@sw-code/urbo-ui';
import { CoreTestModule } from '@sw-code/urbo-core';

describe('ChipsComponent', () => {
  let component: DetailsSkeletonComponent;
  let fixture: ComponentFixture<DetailsSkeletonComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        DetailsSkeletonComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(DetailsSkeletonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
