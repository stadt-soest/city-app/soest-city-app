import {
  Component,
  DestroyRef,
  EventEmitter,
  inject,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { NgClass } from '@angular/common';
import {
  GeneralIconComponent,
  IconFontSize,
  IconType,
  ModuleMapButtonsComponent,
} from '@sw-code/urbo-ui';
import { Category, MapNavigationService } from '@sw-code/urbo-core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'app-map-chips',
  templateUrl: './chips.component.html',
  styleUrls: ['./chips.component.scss'],
  imports: [
    TranslocoDirective,
    NgClass,
    GeneralIconComponent,
    ModuleMapButtonsComponent,
  ],
})
export class ChipsComponent implements OnInit {
  @Input({ required: true }) categories: Category[] = [];
  @Input() currentState: MapChipsState = 'initial';
  @Input() selectedCategoryName = '';
  @Input() isNetworkConnected = true;
  @Output() openModal = new EventEmitter<void>();
  @Output() closeModal = new EventEmitter<void>();
  @Output() resetToAllCategories = new EventEmitter<void>();
  @Output() popPage = new EventEmitter<void>();
  isCategoryList = false;
  isCategoryDetails = false;

  protected readonly iconType = IconType;
  protected readonly iconFontSize = IconFontSize;
  private readonly destroyRef = inject(DestroyRef);
  private readonly mapNavigationService = inject(MapNavigationService);

  ngOnInit() {
    this.mapNavigationService.isOnCategoryListPage$
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((isOnCategoryListPage) => {
        this.isCategoryList = isOnCategoryListPage;
      });

    this.mapNavigationService.isOnCategoryDetailsPage$
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((isOnCategoryDetailPage) => {
        this.isCategoryDetails = isOnCategoryDetailPage;
      });
  }

  openFilterModal(): void {
    this.openModal.emit();
  }

  onResetToAllCategories(): void {
    this.resetToAllCategories.emit();
  }
}

export type MapChipsState =
  | 'initial'
  | 'modalOpen'
  | 'allCategoriesSelected'
  | 'categorySelected'
  | 'categoryDetailsSelected';
