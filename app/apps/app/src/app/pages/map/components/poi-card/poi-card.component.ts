import { Component, inject, Input } from '@angular/core';
import { IonRouterLinkWithHref } from '@ionic/angular/standalone';
import { RouterLink } from '@angular/router';
import { MapDetailsPageComponent } from '../../map-details/map-details-page.component';
import {
  AbstractFeedCardComponent,
  Category,
  NavService,
  PoiFeedItem,
} from '@sw-code/urbo-core';
import {
  IconColor,
  IconFontSize,
  IconType,
  PoiListContainerComponent,
  UIModule,
} from '@sw-code/urbo-ui';

@Component({
  selector: 'app-poi-card',
  templateUrl: './poi-card.component.html',
  standalone: true,
  imports: [
    UIModule,
    IonRouterLinkWithHref,
    RouterLink,
    PoiListContainerComponent,
  ],
})
export class PoiCardComponent extends AbstractFeedCardComponent<PoiFeedItem> {
  @Input() poiWithDistanceId = '';
  @Input() categoryName = '';
  @Input() categories: Category[] = [];
  nextPage = MapDetailsPageComponent;
  protected readonly iconType = IconType;
  protected readonly iconFontSize = IconFontSize;
  protected readonly iconColor = IconColor;
  private readonly navService = inject(NavService);

  async selectPoi() {
    await this.navService.pushPage(
      this.nextPage,
      {
        poiId: this.poiWithDistanceId,
        categoryName: this.categoryName,
        categories: this.categories,
      },
      { animated: true },
    );
  }
}
