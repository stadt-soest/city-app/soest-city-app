import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ChipsComponent } from './chips.component';
import { ModalController } from '@ionic/angular/standalone';
import { UITestModule } from '@sw-code/urbo-ui';
import { CoreTestModule } from '@sw-code/urbo-core';

describe('ChipsComponent', () => {
  let component: ChipsComponent;
  let fixture: ComponentFixture<ChipsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        ChipsComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
      ],
      providers: [{ provide: ModalController, useValue: {} }],
    }).compileComponents();

    fixture = TestBed.createComponent(ChipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
