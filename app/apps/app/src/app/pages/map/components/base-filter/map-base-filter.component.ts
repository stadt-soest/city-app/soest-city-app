import {
  AfterViewInit,
  Component,
  inject,
  Input,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { IonNav } from '@ionic/angular/standalone';
import { NavService } from '@sw-code/urbo-core';

@Component({
  imports: [IonNav],
  providers: [NavService],
  selector: 'app-map-base-filter',
  templateUrl: './map-base-filter.component.html',
})
export class MapBaseFilterComponent implements AfterViewInit, OnDestroy {
  @ViewChild(IonNav) ionNav!: IonNav;
  @Input() rootPage: any;
  @Input() rootParams: any;
  private readonly navService = inject(NavService);

  ngAfterViewInit() {
    this.navService.setNav(this.ionNav);
  }

  ngOnDestroy() {
    this.navService.setNav(null);
  }
}
