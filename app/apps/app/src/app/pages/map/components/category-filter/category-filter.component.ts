import { Component, inject, Input } from '@angular/core';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import {
  IonContent,
  IonHeader,
  ModalController,
} from '@ionic/angular/standalone';
import {
  CustomButtonColorScheme,
  CustomButtonType,
  CustomHorizontalButtonComponent,
  CustomModalHeaderComponent,
} from '@sw-code/urbo-ui';
import {
  Category,
  GroupCategory,
  NavService,
  PageType,
} from '@sw-code/urbo-core';
import { PoiListPageComponent } from '../../poi-list/poi-list-page.component';

@Component({
  selector: 'app-category-filter',
  templateUrl: './category-filter.component.html',
  styleUrls: ['./category-filter.component.scss'],
  imports: [
    TranslocoDirective,
    IonHeader,
    CustomModalHeaderComponent,
    IonContent,
    CustomHorizontalButtonComponent,
  ],
})
export class CategoryFilterComponent {
  @Input({ required: true }) categories: Category[] = [];
  public translateService = inject(TranslocoService);
  nextPage = PoiListPageComponent;
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
  private readonly modalCtrl = inject(ModalController);
  private readonly navService = inject(NavService);

  async selectCategory(category: Category) {
    if (category.source === 'API') {
      await this.navService.pushPage(
        this.nextPage,
        {
          categoryId: category.id,
          categories: this.categories,
          categoryName: category.groupCategory?.getLocalizedName(
            this.translateService.getActiveLang(),
          ),
        },
        { animated: true },
      );
    } else if (category.dashboardComponent) {
      await this.navService.pushPage(
        category.dashboardComponent,
        {
          categoryId: category.id,
          categories: this.categories,
          fromMap: true,
        },
        { animated: true },
      );
    }
  }

  getGroupedCategories(): { group: GroupCategory; categories: Category[] }[] {
    const groupedCategories: {
      [key: string]: { group: GroupCategory; categories: Category[] };
    } = {};

    this.categories?.forEach((category) => {
      if (category.groupCategory) {
        const groupId = category.groupCategory.id;
        if (!groupedCategories[groupId]) {
          groupedCategories[groupId] = {
            group: category.groupCategory,
            categories: [],
          };
        }
        groupedCategories[groupId].categories.push(category);
      }
    });

    return Object.values(groupedCategories);
  }

  async close() {
    await this.modalCtrl.dismiss({
      pageType: PageType.filterList,
      selectedCategory: null,
      updateMarkers: true,
    });
  }
}
