import { Component } from '@angular/core';
import { IonCol, IonRow, IonSkeletonText } from '@ionic/angular/standalone';

@Component({
  selector: 'app-details-skeleton',
  templateUrl: './details-skeleton.component.html',
  styleUrls: ['./details-skeleton.component.scss'],
  imports: [IonSkeletonText, IonRow, IonCol],
})
export class DetailsSkeletonComponent {}
