import {
  Component,
  DestroyRef,
  inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  IonCol,
  IonContent,
  IonRouterOutlet,
  IonRow,
  ModalController,
  ViewWillLeave,
} from '@ionic/angular/standalone';
import { NgClass } from '@angular/common';
import { TranslocoDirective } from '@jsverse/transloco';
import { ConnectivityErrorCardComponent } from '@sw-code/urbo-ui';
import {
  ChipsComponent,
  MapChipsState,
} from './components/chips/chips.component';
import {
  Category,
  ConnectivityService,
  customMdEnterAnimation,
  customMdLeaveAnimation,
  MapInteractionService,
  MapNavigationService,
  MapService,
  PageType,
  Poi,
  PoiService,
  TabRefreshService,
  TabType,
  ToastService,
  TranslationService,
} from '@sw-code/urbo-core';
import { Router } from '@angular/router';
import { catchError, filter, forkJoin, of, take, throwError } from 'rxjs';
import { MapBaseFilterComponent } from './components/base-filter/map-base-filter.component';
import { CategoryFilterComponent } from './components/category-filter/category-filter.component';
import { PoiListPageComponent } from './poi-list/poi-list-page.component';
import { MapDetailsPageComponent } from './map-details/map-details-page.component';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-map',
  templateUrl: './map-page.component.html',
  styleUrls: ['./map-page.component.scss'],
  imports: [
    IonContent,
    NgClass,
    IonRow,
    TranslocoDirective,
    IonCol,
    ConnectivityErrorCardComponent,
    ChipsComponent,
  ],
})
export class MapPageComponent implements OnInit, ViewWillLeave {
  @ViewChild(ChipsComponent) chipsComponent?: ChipsComponent;
  categories: Category[] = [];
  selectedCategory: Category | null = null;
  selectedCategories: PageType[] = [];
  selectedCategoryId = '';
  selectedCategoryName = '';
  selectedCategoryDetailsId = '';
  selectedPageType: PageType = PageType.filterList;
  isModalOpen = false;
  isConnected = true;

  private readonly poiService = inject(PoiService);
  private readonly toastService = inject(ToastService);
  private readonly translationService = inject(TranslationService);
  private readonly mapService = inject(MapService);
  private readonly mapInteractionService = inject(MapInteractionService);
  private readonly routerOutlet = inject(IonRouterOutlet);
  private readonly modalController = inject(ModalController);
  private readonly connectivityService = inject(ConnectivityService);
  private readonly refreshService = inject(TabRefreshService);
  private readonly destroyRef = inject(DestroyRef);
  private readonly mapNavigationService = inject(MapNavigationService);
  private readonly router = inject(Router);

  private allPois: Poi[] = [];
  private modulePois: Poi[] = [];
  private hardcodedCategoryPois: Poi[] = [];
  private isActive = false;
  private readonly wasteCategoryId = '26bad60f-445b-40f4-b5da-addc877247d8';

  get currentState(): MapChipsState {
    if (this.isModalOpen) {
      return 'modalOpen';
    } else if (this.selectedPageType === PageType.filterList) {
      return 'allCategoriesSelected';
    } else if (this.selectedPageType === PageType.poiList) {
      return 'categorySelected';
    } else if (this.selectedPageType === PageType.poiDetails) {
      return 'categoryDetailsSelected';
    } else {
      return 'initial';
    }
  }

  async ngOnInit() {
    await this.initializeComponent();
  }

  async ionViewWillLeave() {
    this.isActive = false;
    await this.closeFilterModal();
  }

  async ionViewWillEnter() {
    this.isActive = true;
    this.fetchCategories();
  }

  async refreshView() {
    if (this.isConnected) {
      await this.closeFilterModal();
      this.mapService.resetMap();
      await this.resetCategories();
    }
  }

  fetchCategories() {
    this.poiService
      .getAllCategories()
      .pipe(
        take(1),
        catchError((error) => {
          const message = this.translationService.translate({
            key: 'errors.unable_to_load',
            params: {
              item: this.translationService.translate({
                key: 'items.categories',
              }),
            },
          });
          this.toastService.presentToast(message);
          return throwError(() => new Error(error));
        }),
      )
      .subscribe(async (categories) => {
        this.categories = categories;
        this.closeCategoriesModal();
        this.loadPois();
      });
  }

  async closeFilterModal() {
    if (this.isModalOpen) {
      const topModal = await this.modalController.getTop();
      if (topModal) {
        await topModal.dismiss({
          pageType: this.selectedPageType,
          selectedCategory: this.selectedCategory,
          selectedCategories: this.selectedCategories,
          categoryName: this.selectedCategoryName,
          categoryId: this.selectedCategoryId,
          categoryDetailsId: this.selectedCategoryDetailsId,
          updateMarkers: false,
        });
        await topModal.onDidDismiss();
      }
      this.isModalOpen = false;
    }
  }

  async resetCategories(): Promise<void> {
    this.selectedPageType = PageType.filterList;
    this.selectedCategory = null;
    await this.updateVisibleMarkers();
  }

  closeCategoriesModal(): void {
    this.isModalOpen = false;
  }

  async openFilterModal() {
    if (this.isModalOpen) {
      return;
    }
    this.isModalOpen = true;
    await this.updateControlPositions('top-right');
    const component = MapBaseFilterComponent;
    let componentProps: any = {
      rootPage: CategoryFilterComponent,
      rootParams: {
        categoryName: this.selectedCategoryName,
        features: this.categories,
        categories: this.categories,
      },
    };

    if (this.selectedPageType === PageType.poiList) {
      if (this.selectedCategory?.source === 'API') {
        componentProps = {
          rootPage: PoiListPageComponent,
          rootParams: {
            categoryName: this.selectedCategoryName,
            features: this.categories,
            categories: this.categories,
            categoryId: this.selectedCategoryId,
          },
        };
      } else {
        componentProps = {
          rootPage: this.selectedCategory?.dashboardComponent,
          rootParams: {
            categoryId: this.selectedCategory?.id,
            categories: this.categories,
            fromMap: true,
          },
        };
      }
    }

    if (this.selectedPageType === PageType.poiDetails) {
      if (this.selectedCategory?.source === 'API') {
        componentProps = {
          rootPage: MapDetailsPageComponent,
          rootParams: {
            poiId: this.selectedCategoryDetailsId,
            categoryName: this.selectedCategoryName,
            categories: this.categories,
          },
        };
      } else {
        componentProps = {
          rootPage: this.selectedCategory?.detailsComponent,
          rootParams: {
            poiId: this.selectedCategoryDetailsId,
            categoryId: this.selectedCategoryId,
            categoryName: this.selectedCategoryName,
            categories: this.categories,
          },
        };
      }
    }

    const modal = await this.modalController.create({
      mode: 'ios',
      presentingElement: this.routerOutlet.nativeEl,
      backdropDismiss: false,
      enterAnimation: customMdEnterAnimation,
      leaveAnimation: customMdLeaveAnimation,
      showBackdrop: false,
      cssClass: 'map-modal',
      focusTrap: false,
      component,
      componentProps,
    });

    if (!this.isActive) {
      await this.closeFilterModal();
      return;
    }

    this.isModalOpen = true;
    await modal.present();

    this.panToMapCenterOnModalOpen();

    modal.onWillDismiss().then(async (event: any) => {
      this.isModalOpen = false;
      await this.updateControlPositions('bottom-right');
      const data = event.data;
      if (data?.pageType) {
        this.selectedPageType = data.pageType;
        if (data.pageType === PageType.filterList) {
          this.selectedCategories = [PageType.filterList];
          this.selectedCategoryId = '';
          this.selectedCategory = data.selectedCategory;
        } else if (data.pageType === PageType.poiList) {
          this.selectedCategories = data.selectedCategories;
          this.selectedCategoryName =
            this.mapNavigationService.getCurrentModuleCategoryName() ??
            data.categoryName;
          this.selectedCategoryId = data.categoryId;
          this.selectedCategory =
            data.selectedCategory || this.findCategoryById(data.categoryId);
        } else if (data.pageType === PageType.poiDetails) {
          this.selectedCategoryDetailsId = data.categoryDetailsId;
          this.selectedCategoryId = data.categoryId;
          this.selectedCategoryName = data.categoryName;
          this.selectedCategory =
            this.findCategoryById(data.categoryId) || null;
        }
        if (data.updateMarkers) {
          await this.updateVisibleMarkers();
        }
      } else {
        this.persistModalState();
      }
    });
  }

  persistModalState() {
    this.mapNavigationService.setUpdateMap(
      this.selectedCategoryId,
      this.selectedCategoryDetailsId,
      this.selectedCategories,
      this.selectedPageType,
      this.selectedCategoryName,
    );
  }

  loadPois() {
    forkJoin({
      allPois: this.fetchAllPois(),
      modulePois: this.fetchModulePois(),
      hardcodedCategoryPois: this.fetchHardcodedCategoryPois(),
    })
      .pipe(
        take(1),
        catchError((error) => {
          const combinedError = new Error('Error fetching POIs');
          combinedError.stack = `${error}`;
          return throwError(() => combinedError);
        }),
      )
      .subscribe(async ({ allPois, modulePois, hardcodedCategoryPois }) => {
        if (modulePois.length === 0 && hardcodedCategoryPois.length === 0) {
          const message = this.translationService.translate({
            key: 'errors.unable_to_load',
            params: {
              item: this.translationService.translate({ key: 'items.pois' }),
            },
          });
          this.toastService.presentToast(message);
          return;
        }

        this.allPois = this.translatePois(allPois);
        this.modulePois = this.translatePois(modulePois);
        this.hardcodedCategoryPois = this.translatePois(hardcodedCategoryPois);

        await this.updateVisibleMarkers();
      });
  }

  async updateVisibleMarkers() {
    await this.waitForMapReady();
    let poisToDisplay: Poi[];
    let offsetY = 0;
    let selectedPoi: Poi | undefined;

    if (
      this.selectedPageType === PageType.poiDetails &&
      this.selectedCategoryDetailsId
    ) {
      selectedPoi = this.allPois.find(
        (poi) => poi.id === this.selectedCategoryDetailsId,
      );
      if (selectedPoi) {
        const categoryId = selectedPoi?.categories?.[0]?.id ?? '';
        this.selectedCategory = this.findCategoryById(categoryId);
        poisToDisplay = this.filterPoisByCategory();

        if (this.isModalOpen) {
          offsetY = this.calculateModalOffset().offsetY;
        }

        this.mapService.panToLocation(
          selectedPoi.location.coordinates.latitude,
          selectedPoi.location.coordinates.longitude,
          offsetY,
        );
      } else {
        poisToDisplay = [];
      }
    } else if (this.selectedCategory) {
      this.mapService.resetMap();
      poisToDisplay = this.filterPoisByCategory();
    } else {
      this.mapService.resetMap();
      poisToDisplay = [...this.modulePois, ...this.hardcodedCategoryPois];
    }

    this.panToMapCenterOnModalOpen();

    const features = Poi.toGeoJSONFeatures(
      poisToDisplay,
      this.selectedCategory,
      this.translationService.getCurrentLocale(),
      this.selectedCategoryDetailsId,
    );

    this.mapService.addFeatures(features, async (poiId) => {
      const clickedPoi = poisToDisplay.find((poi) => poi.id === poiId);
      const categoryId = clickedPoi?.categories?.[0]?.id ?? '';
      this.selectedCategory = this.findCategoryById(categoryId);
      const topModal = await this.modalController.getTop();
      if (topModal) {
        await this.modalController.dismiss({
          updateMarkers: false,
          categoryId,
          categoryDetailsId: poiId,
          pageType: PageType.poiDetails,
        });
        this.isModalOpen = false;
      }

      this.selectedPageType = PageType.poiDetails;
      this.selectedCategoryDetailsId = poiId;

      this.selectedCategoryId = categoryId;
      await this.openFilterModal();
    });
  }

  private async initializeComponent() {
    await this.setOnlineStatus();
    this.mapService.initializeMap('main-map');
    this.subscribeToTabRefresh();
    this.subscribeToMapEvents();
    this.isActive = true;
  }

  private translatePois(pois: Poi[]): Poi[] {
    return pois.map((poi) => {
      const translatedPoi = { ...poi };

      if (translatedPoi.translatableName) {
        translatedPoi.name = this.translationService.translate(
          translatedPoi.translatableName,
        );
      }

      return translatedPoi;
    });
  }

  private async updateControlPositions(
    position: 'top-right' | 'bottom-right',
  ): Promise<void> {
    try {
      this.mapService.updateControlPositions(position);
    } catch (error) {
      console.error('Failed to update control positions:', error);
    }
  }

  private subscribeToMapEvents() {
    this.mapNavigationService.mapEvents$.subscribe(async (event) => {
      if (event) {
        this.selectedCategoryId = event.categoryId;
        this.selectedCategory = this.findCategoryById(event.categoryId);
        this.selectedCategoryDetailsId = event.categoryDetailsId;
        this.selectedCategories = event.selectedCategories;
        this.selectedPageType = event.pageType;
        this.selectedCategoryName = event.categoryName;
        await this.updateVisibleMarkers();
      }
    });

    this.mapNavigationService.updateModuleCategoryName$
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((categoryName) => {
        if (categoryName) {
          this.selectedCategoryName = categoryName;
        }
      });

    this.mapInteractionService.mapInteracted
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(async () => {
        await this.closeFilterModal();
      });
  }

  private fetchAllPois() {
    return this.poiService.getAllPois().pipe(
      take(1),
      catchError(() => {
        const message = this.translationService.translate({
          key: 'errors.unable_to_load',
          params: {
            item: this.translationService.translate({ key: 'items.pois' }),
          },
        });
        this.toastService.presentToast(message);
        return of([]);
      }),
    );
  }

  private fetchModulePois() {
    return this.poiService.getPoisFromModules().pipe(
      take(1),
      catchError(() => of([])),
    );
  }

  private fetchHardcodedCategoryPois() {
    return this.poiService.getPois([this.wasteCategoryId]).pipe(
      take(1),
      catchError(() => of([])),
    );
  }

  private filterPoisByCategory(): Poi[] {
    return this.selectedCategory
      ? this.allPois.filter((poi) =>
          poi.categories.some(
            (category) => category.id === this.selectedCategory?.id,
          ),
        )
      : this.allPois;
  }

  private async setOnlineStatus() {
    this.isConnected = await this.connectivityService.checkOnlineStatus();
  }

  private subscribeToTabRefresh() {
    this.refreshService.refresh$
      .pipe(
        filter((tabType) => tabType === TabType.map),
        takeUntilDestroyed(this.destroyRef),
      )
      .subscribe(async () => {
        if (this.router.url === '/map') {
          await this.refreshView();
        }
      });
  }

  private findCategoryById(id: string): Category | null {
    return this.categories.find((category) => category.id === id) || null;
  }

  private async waitForMapReady(): Promise<void> {
    if (this.mapService.isMapReady()) {
      return;
    }
    await new Promise<void>((resolve) => {
      this.mapService.onMapReady.pipe(take(1)).subscribe(() => {
        resolve();
      });
    });
  }

  private panToMapCenterOnModalOpen() {
    if (this.isModalOpen && this.selectedPageType !== PageType.poiDetails) {
      const { offsetY } = this.calculateModalOffset();
      this.mapService.panToLocation(
        environment.map.center[1],
        environment.map.center[0],
        offsetY,
        environment.map.zoom,
      );
    }
  }

  private calculateModalOffset(): { modalHeight: number; offsetY: number } {
    const modalHeight = window.innerHeight * 0.7;
    const offsetY = -modalHeight / 2;
    return { modalHeight, offsetY };
  }
}
