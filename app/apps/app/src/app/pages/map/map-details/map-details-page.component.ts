import { Component, inject, Input, OnInit } from '@angular/core';
import {
  IonContent,
  IonHeader,
  ModalController,
} from '@ionic/angular/standalone';
import {
  ActionButtonBarComponent,
  CondensedHeaderLayoutComponent,
  CustomModalHeaderComponent,
  LocalizedTimeDistancePipePipe,
} from '@sw-code/urbo-ui';
import {
  TranslocoDirective,
  TranslocoPipe,
  TranslocoService,
} from '@jsverse/transloco';
import { DetailsSkeletonComponent } from '../components/details-skeleton/details-skeleton.component';
import { AsyncPipe, NgIf } from '@angular/common';
import {
  BrowserService,
  Category,
  MapNavigationService,
  MapsNavigationService,
  NavService,
  PageType,
  Poi,
  PoiService,
  SimplifiedPermissionState,
  TitleService,
  TranslationService,
} from '@sw-code/urbo-core';
import { ActivatedRoute } from '@angular/router';
import { finalize, take } from 'rxjs';
import { PoiListPageComponent } from '../poi-list/poi-list-page.component';

@Component({
  selector: 'app-map-details',
  templateUrl: './map-details-page.component.html',
  styleUrls: ['./map-details-page.component.scss'],
  imports: [
    IonHeader,
    CustomModalHeaderComponent,
    TranslocoDirective,
    IonContent,
    DetailsSkeletonComponent,
    NgIf,
    ActionButtonBarComponent,
    CondensedHeaderLayoutComponent,
    AsyncPipe,
    TranslocoPipe,
    LocalizedTimeDistancePipePipe,
  ],
})
export class MapDetailsPageComponent implements OnInit {
  @Input() poiId = '';
  @Input() categoryName = '';
  @Input() categories: Category[] = [];
  poi?: Poi;
  loading = true;
  private readonly route = inject(ActivatedRoute);
  private readonly poiService = inject(PoiService);
  private readonly mapsNavigationService = inject(MapsNavigationService);
  private readonly browserService = inject(BrowserService);
  private readonly translationService = inject(TranslationService);
  private readonly titleService = inject(TitleService);
  private readonly mapNavigationService = inject(MapNavigationService);
  private readonly modalCtrl = inject(ModalController);
  private readonly navService = inject(NavService);

  ngOnInit() {
    Promise.resolve().then(() => {
      this.mapNavigationService.setIsOnCategoryDetailsPage(true);
      this.mapNavigationService.setIsOnCategoryListPage(false);
    });

    this.mapNavigationService.popPageDetails$
      .pipe(take(1))
      .subscribe(async () => {
        await this.pop();
      });
    if (this.poiId) {
      this.loadPoiDetails(this.poiId);
    } else {
      this.route.paramMap.subscribe((params) => {
        const poiId = params.get('id');
        if (poiId) {
          this.loadPoiDetails(poiId);
        }
      });
    }
  }

  async pop() {
    this.mapNavigationService.setIsOnCategoryDetailsPage(false);

    this.mapNavigationService.setUpdateMap(
      this.poi?.categories?.[0]?.id ?? '',
      this.poi?.id ?? '',
      [PageType.poiList],
      PageType.poiList,
      this.poi?.name ?? '',
    );
    if (await this.navService.canGoBack()) {
      await this.navService.popPage();
    } else {
      await this.navService.pushPage(
        PoiListPageComponent,
        {
          categoryId: this.poi?.categories?.[0]?.id,
          categories: this.categories,
          categoryName: this.categoryName,
        },
        {
          animated: true,
          direction: 'back',
        },
      );
    }
  }

  async close() {
    this.mapNavigationService.setIsOnCategoryDetailsPage(false);
    await this.modalCtrl.dismiss({
      categoryId: this.poi?.categories?.[0]?.id,
      categoryDetailsId: this.poi?.id,
      selectedCategories: [this.categoryName],
      pageType: PageType.poiDetails,
      categoryName: this.poi?.name ?? '',
      updateMarkers: true,
    });
  }

  openPoiUrl() {
    if (!this.poi?.url) {
      return;
    }
    this.browserService.openInExternalBrowser(this.poi.url);
  }

  loadPoiDetails(id: string) {
    this.loading = true;
    this.poiService
      .getPoiById(id)
      .pipe(
        take(1),
        finalize(() => {
          this.loading = false;
        }),
      )
      .subscribe((poi: Poi) => {
        this.poi = poi;
        this.updateTitle();

        this.mapNavigationService.setUpdateMap(
          this.poi?.categories?.[0]?.id ?? '',
          this.poi?.id ?? '',
          [PageType.poiDetails],
          PageType.poiDetails,
          this.poi?.name ?? '',
        );
      });
  }

  getLocalizedCategoryNames(): string {
    if (!this.poi?.categories || this.poi.categories.length === 0) {
      return '';
    }

    return this.poi.categories
      .map((category) =>
        category.getLocalizedName(this.translationService.getCurrentLocale()),
      )
      .filter((name) => !!name)
      .join(', ');
  }

  async navigateToLocation(): Promise<void> {
    if (!this.poi?.location.coordinates) {
      return;
    }

    await this.mapsNavigationService.navigateToLocation(
      this.poi.location.coordinates.latitude,
      this.poi.location.coordinates.longitude,
    );
  }

  private updateTitle() {
    if (this.poi) {
      this.titleService.setTitle(this.poi.name);
    }
  }
}
