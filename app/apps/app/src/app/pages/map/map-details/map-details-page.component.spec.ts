import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { MapDetailsPageComponent } from './map-details-page.component';
import {
  BrowserService,
  CoreTestModule,
  MapsNavigationService,
  Poi,
  PoiContactPoint,
  PoiLocation,
  PoiService,
} from '@sw-code/urbo-core';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../../test/transloco-testing.module';

describe('MapDetailsPage', () => {
  let component: MapDetailsPageComponent;
  let fixture: ComponentFixture<MapDetailsPageComponent>;

  const poi = new Poi({
    id: 'id',
    name: 'name',
    url: 'https://example.com',
    location: new PoiLocation({ latitude: 0, longitude: 0 }),
    contactPoint: new PoiContactPoint('email', 'phone', 'contactPerson'),
    dateModified: new Date().toISOString(),
    categories: [],
    icon: undefined,
    modulePath: 'modulePath',
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MapDetailsPageComponent,
        UITestModule.forRoot(),
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        { provide: PoiService, useValue: mockPoiService },
        { provide: MapsNavigationService, useValue: mockMapsNavigationService },
        { provide: BrowserService, useValue: mockBrowserService },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(MapDetailsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.poi = poi;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load POI details on init', () => {
    const testPoi = {
      id: '123',
      name: 'Test POI',
      categories: [],
      location: {},
    };
    mockPoiService.getPoiById.mockReturnValue(of(testPoi));

    component.ngOnInit();

    expect(mockPoiService.getPoiById).toHaveBeenCalledWith('123');
    expect(component.poi).toEqual(testPoi);
  });

  it('should navigate to location', async () => {
    await component.navigateToLocation();

    expect(mockMapsNavigationService.navigateToLocation).toHaveBeenCalledWith(
      0,
      0,
    );
  });

  it('should open POI URL when available', () => {
    component.openPoiUrl();

    expect(mockBrowserService.openInExternalBrowser).toHaveBeenCalledWith(
      'https://example.com',
    );
  });

  it('should return empty string when there are no categories', () => {
    component.poi = { categories: [] } as unknown as Poi;

    const categoryNames = component.getLocalizedCategoryNames();

    expect(categoryNames).toEqual('');
  });

  const mockPoiService = {
    getPoiById: jest.fn().mockReturnValue(of(poi)),
  };

  const mockMapsNavigationService = {
    navigateToLocation: jest.fn().mockImplementation(() => Promise.resolve()),
  };

  const mockBrowserService = {
    openInExternalBrowser: jest.fn(),
  };

  const mockActivatedRoute = {
    paramMap: of(new Map([['id', '123']])),
  };
});
