import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MapPageComponent } from './map-page.component';
import {
  CoreTestModule,
  MAP_STYLE_URL,
  MapInteractionService,
  MapService,
  PoiService,
  ToastService,
} from '@sw-code/urbo-core';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../test/transloco-testing.module';
import { IonRouterOutlet } from '@ionic/angular/standalone';
import { of, throwError } from 'rxjs';

describe('MapPage', () => {
  let component: MapPageComponent;
  let fixture: ComponentFixture<MapPageComponent>;

  const mockRouterOutlet = {
    nativeEl: document.createElement('div'),
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MapPageComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        { provide: IonRouterOutlet, useValue: mockRouterOutlet },
        { provide: PoiService, useValue: mockPoiService },
        { provide: ToastService, useValue: mockToastService },
        { provide: MapService, useValue: mockMapService },
        { provide: MAP_STYLE_URL, useValue: 'mock-style-url' },
        MapInteractionService,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize the map on ngOnInit', () => {
    expect(mockMapService.initializeMap).toHaveBeenCalled();
  });

  it('fetchCategories should fetch categories successfully', () => {
    const categories = [{ id: '1', name: 'Category 1' }];
    mockPoiService.getAllCategories.mockReturnValue(of(categories));
    component.fetchCategories();
    expect(component.categories).toEqual(categories);
  });

  it('fetchCategories should handle error', () => {
    mockPoiService.getAllCategories.mockReturnValue(
      throwError(() => new Error('Error fetching categories')),
    );
    component.fetchCategories();
    expect(mockToastService.presentToast).toHaveBeenCalledWith(
      'Unable to load categories. Please check your connection and try again.',
    );
  });
});

const mockPoiService = {
  getAllCategories: jest.fn().mockReturnValue(of([])),
  getAllPois: jest.fn().mockReturnValue(of([])),
  getCategories: jest.fn().mockReturnValue(of([])),
  getPois: jest.fn().mockReturnValue(of([])),
  getPoisFromModules: jest.fn().mockReturnValue(of([])),
};

const mockToastService = {
  presentToast: jest.fn(),
};

const mockMapService = {
  registerPoiClickCallback: jest.fn(),
  initializeMap: jest.fn(),
  addMarker: jest.fn(),
  clearMarkers: jest.fn(),
  adjustMapViewToFitMarkers: jest.fn(),
  cleanup: jest.fn(),
  onMapReady: of(),
};
