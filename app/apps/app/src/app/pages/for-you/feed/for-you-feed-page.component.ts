import { Component, inject, OnInit } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  CondensedHeaderLayoutComponent,
  ConnectivityErrorCardComponent,
  FeedLoadingForYouSkeletonComponent,
  RefresherComponent,
  UpdateNotifierComponent,
  WentWrongErrorCardComponent,
} from '@sw-code/urbo-ui';
import {
  IonCol,
  IonInfiniteScroll,
  IonInfiniteScrollContent,
} from '@ionic/angular/standalone';
import { NgComponentOutlet, NgIf } from '@angular/common';
import { AbstractForYouPageComponent } from '../../../components/for-you/for-you-abstract/abstract-for-you-page.component';
import { FeedCategory } from '../../../enums/feed-category';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs';

@Component({
  selector: 'app-for-you-feed',
  templateUrl: './for-you-feed-page.component.html',
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    RefresherComponent,
    ConnectivityErrorCardComponent,
    WentWrongErrorCardComponent,
    UpdateNotifierComponent,
    IonCol,
    NgComponentOutlet,
    FeedLoadingForYouSkeletonComponent,
    NgIf,
    IonInfiniteScroll,
    IonInfiniteScrollContent,
  ],
})
export class ForYouFeedPageComponent
  extends AbstractForYouPageComponent
  implements OnInit
{
  readonly feedCategory = FeedCategory;
  category = '';
  title = '';
  page = 2;
  private readonly route = inject(ActivatedRoute);

  get shouldShowNewInfiniteScroll() {
    return (
      this.category === FeedCategory.new &&
      this.feedItemsNew.length > 0 &&
      this.moreFeedItemsNewAvailable &&
      !this.loadingNewFeedItems
    );
  }

  get shouldShowUpcomingEventsInfiniteScroll() {
    return (
      this.category === this.feedCategory.upcomingEvents &&
      this.feedItemsUpcomingEvents.length > 0 &&
      this.moreFeedItemsUpcomingEventsAvailable &&
      !this.loadingUpcomingEventsFeedItems
    );
  }

  override ngOnInit() {
    this.category = this.route.snapshot.paramMap.get('category') ?? '';
    super.ngOnInit();
    if (this.category === FeedCategory.new) {
      this.moreFeedItemsUpcomingEventsAvailable = false;
    } else if (this.category === FeedCategory.upcomingEvents) {
      this.moreFeedItemsNewAvailable = false;
    }
  }

  refreshView(event?: any) {
    this.clearStates();
    if (this.category === FeedCategory.new) {
      this.loadNewFeedItems(1, event);
    } else if (this.category === FeedCategory.upcomingEvents) {
      this.loadUpcomingEventsFeedItems(0, event);
    }
  }

  loadMoreFeedItems(event: any) {
    if (this.category === FeedCategory.new) {
      this.forYouService
        .getFilteredFeedItems(this.page)
        .pipe(take(1))
        .subscribe((feedItems) => {
          this.moreFeedItemsNewAvailable = feedItems.length > 0;
          this.feedItemsNew.push(...feedItems);
          this.page++;
          event.target.complete();
        });
    } else if (this.category === FeedCategory.upcomingEvents) {
      this.forYouService
        .getFilteredFeedItemsUpcoming(this.page)
        .pipe(take(1))
        .subscribe((feedItemsUpcomingEvents) => {
          this.feedItemsUpcomingEvents.push(...feedItemsUpcomingEvents);
          this.moreFeedItemsUpcomingEventsAvailable =
            feedItemsUpcomingEvents.length > 0;
          this.page++;
          event.target.complete();
        });
    }
  }

  private loadNewFeedItems(page: number, event?: any) {
    this.loadingNewFeedItems = true;
    this.forYouService
      .getFilteredFeedItems(page)
      .pipe(take(1))
      .subscribe((feedItems) => {
        this.moreFeedItemsNewAvailable = feedItems.length > 0;
        this.feedItemsNew.push(...feedItems);
        this.loadingNewFeedItems = false;
        this.page++;
        event?.target.complete();
      });
  }

  private loadUpcomingEventsFeedItems(page: number, event?: any) {
    this.loadingUpcomingEventsFeedItems = true;
    this.forYouService
      .getFilteredFeedItemsUpcoming(page)
      .pipe(take(1))
      .subscribe((feedItemsUpcomingEvents) => {
        this.feedItemsUpcomingEvents.push(...feedItemsUpcomingEvents);
        this.moreFeedItemsUpcomingEventsAvailable =
          feedItemsUpcomingEvents.length > 0;
        this.loadingUpcomingEventsFeedItems = false;
        this.page++;
        event?.target.complete();
      });
  }

  private clearStates() {
    this.page = 1;
    this.feedItemsNew = [];
    this.feedItemsUpcomingEvents = [];
    this.loadingNewFeedItems = true;
    this.loadingUpcomingEventsFeedItems = true;
    this.moreFeedItemsNewAvailable = true;
    this.moreFeedItemsUpcomingEventsAvailable = true;
  }
}
