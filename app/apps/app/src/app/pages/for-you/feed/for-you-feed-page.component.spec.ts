import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ForYouFeedPageComponent } from './for-you-feed-page.component';
import { UITestModule } from '@sw-code/urbo-ui';
import { Component } from '@angular/core';
import {
  AbstractFeedCardComponent,
  CoreTestModule,
  FeedItem,
  ForYouService,
  UpdateService,
} from '@sw-code/urbo-core';
import { SwUpdate, VersionReadyEvent } from '@angular/service-worker';
import { BehaviorSubject, of, Subject } from 'rxjs';
import { getTranslocoModule } from '../../../../test/transloco-testing.module';
import { FeedCategory } from '../../../enums/feed-category';

describe('ForYouFeedPage', () => {
  @Component({
    template: '<p>Mock</p>',
  })
  class MockFeedCardComponent extends AbstractFeedCardComponent<FeedItem> {}

  let component: ForYouFeedPageComponent;
  let fixture: ComponentFixture<ForYouFeedPageComponent>;

  let mockSwUpdate: Partial<SwUpdate>;
  let versionUpdatesSubject: Subject<VersionReadyEvent>;
  let mockForYouService: jest.Mocked<ForYouService>;
  const mockedFeedItems = of([
    {
      id: 'test id',
      moduleId: 'test module id',
      moduleName: 'test modul name',
      baseRoutePath: '/test2',
      icon: 'test icon',
      title: 'Sample Title 1',
      text: 'Sample Text 1',
      date: new Date('2023-10-01T00:00:00'),
      cardComponent: MockFeedCardComponent,
      forYouComponent: MockFeedCardComponent,
    },
    {
      id: 'test id2',
      moduleId: 'test module id2',
      moduleName: 'test modul name2',
      baseRoutePath: '/test2',
      icon: 'test icon2',
      title: 'Sample Title 2',
      text: 'Sample Text 2',
      date: new Date('2023-10-01T00:00:00'),
      cardComponent: MockFeedCardComponent,
      forYouComponent: MockFeedCardComponent,
    },
  ]);

  beforeEach(async () => {
    versionUpdatesSubject = new BehaviorSubject<VersionReadyEvent>({
      type: 'VERSION_READY',
      currentVersion: { hash: 'old-hash', appData: undefined },
      latestVersion: { hash: 'new-hash', appData: undefined },
    });

    mockSwUpdate = {
      isEnabled: true,
      versionUpdates: versionUpdatesSubject.asObservable(),
    };

    mockForYouService = {
      getFilteredFeedItems: jest.fn().mockReturnValue(mockedFeedItems),
      hasContent: jest.fn().mockReturnValue(of(true)),
      hasContentUpcoming: jest.fn().mockReturnValue(of(true)),
      hasContentUnfiltered: jest.fn().mockReturnValue(of(true)),
      getFilteredFeedItemsUpcoming: jest.fn().mockReturnValue(mockedFeedItems),
      getUnfilteredFeedItems: jest.fn().mockReturnValue(mockedFeedItems),
      getHighlightedFeedItems: jest.fn().mockReturnValue(mockedFeedItems),
      getActivityFeedItems: jest.fn().mockReturnValue(mockedFeedItems),
      getPriorityFeedItems: jest.fn().mockReturnValue(mockedFeedItems),
    } as any;

    await TestBed.configureTestingModule({
      providers: [
        UpdateService,
        {
          provide: SwUpdate,
          useValue: mockSwUpdate,
        },
        { provide: ForYouService, useValue: mockForYouService },
      ],
      imports: [
        ForYouFeedPageComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ForYouFeedPageComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('feedItemsNew should be set to 2 after ngOnInit', () => {
      component.ngOnInit();
      expect(component.feedItemsNew.length).toBe(2);
    });

    it('feedItemsUpcomingEvents should be set to 2 after ngOnInit', () => {
      component.ngOnInit();
      expect(component.feedItemsUpcomingEvents.length).toBe(2);
    });
  });

  describe('loadMoreFeedItems', () => {
    it('should push new feed items to feedItemsNew when category is NEW', () => {
      component.category = FeedCategory.new;
      component.loadMoreFeedItems({ target: { complete: jest.fn() } });
      expect(component.feedItemsNew.length).toBe(4);
    });

    it('should push new feed items to feedItemsUpcomingEvents when category is UPCOMING_EVENTS', () => {
      component.category = 'UPCOMING_EVENTS';
      component.loadMoreFeedItems({ target: { complete: jest.fn() } });
      expect(component.feedItemsUpcomingEvents.length).toBe(4);
    });

    it('should complete target when category is NEW and subscription complete', () => {
      component.category = FeedCategory.new;
      const complete = jest.fn();
      component.loadMoreFeedItems({ target: { complete } });
      expect(complete).toHaveBeenCalled();
    });

    it('should complete target when category is UPCOMING_EVENTS and subscription complete', () => {
      component.category = FeedCategory.upcomingEvents;
      const complete = jest.fn();
      component.loadMoreFeedItems({ target: { complete } });
      expect(complete).toHaveBeenCalled();
    });

    it('should set moreFeedItemsNewAvailable to false when category is NEW and getFeedItems does not return anything', () => {
      component.category = FeedCategory.new;
      mockForYouService.getFilteredFeedItems.mockReturnValue(of([]));
      component.loadMoreFeedItems({ target: { complete: jest.fn() } });
      expect(component.moreFeedItemsNewAvailable).toBe(false);
    });

    it(
      'should set moreFeedItemsUpcomingEventsAvailable to false when category is UPCOMING_EVENTS ' +
        'and getFeedItemsUpcoming does not return anything',
      () => {
        component.category = FeedCategory.upcomingEvents;
        mockForYouService.getFilteredFeedItemsUpcoming.mockReturnValue(of([]));
        component.loadMoreFeedItems({ target: { complete: jest.fn() } });
        expect(component.moreFeedItemsUpcomingEventsAvailable).toBe(false);
      },
    );
  });
});
