import { Component, inject, OnInit } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  CondensedHeaderLayoutComponent,
  NavigationListComponent,
  NavigationListData,
  SignatureComponent,
} from '@sw-code/urbo-ui';
import { ModuleInfo, ModuleRegistryService } from '@sw-code/urbo-core';

@Component({
  selector: 'app-settings-menu-page',
  templateUrl: './settings-menu-page.component.html',
  styleUrls: ['./settings-menu-page.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    NavigationListComponent,
    SignatureComponent,
  ],
})
export class SettingsMenuPageComponent implements OnInit {
  menuItemGroups: NavigationListData[] = [];
  contentFeatures: ModuleInfo[] = [];
  metaFeatures: ModuleInfo[] = [];
  private readonly moduleRegistry = inject(ModuleRegistryService);

  ngOnInit(): void {
    this.initializeMenuGroups();
  }

  private initializeMenuGroups(): void {
    const allModules = this.moduleRegistry.getModuleInfos();
    this.contentFeatures = this.filterContentFeatures(allModules);
    this.metaFeatures = this.filterMetaFeatures(allModules);
    this.menuItemGroups = this.createMenuGroups();
  }

  private filterContentFeatures(modules: ModuleInfo[]): ModuleInfo[] {
    return modules.filter(
      (module) => module.settingsPageAvailable && !module.isAboutAppItself,
    );
  }

  private filterMetaFeatures(modules: ModuleInfo[]): ModuleInfo[] {
    return modules.filter((module) => module.isAboutAppItself);
  }

  private createMenuGroups(): NavigationListData[] {
    const forYouSettingsMenuItem = {
      title: 'for_you.for_you_menu_title',
      icon: 'auto_awesome',
      baseRoutingPath: 'module-preferences',
    };

    return [
      {
        category: 'settings.sections.customizeContent',
        menuItems: [
          forYouSettingsMenuItem,
          ...this.mapToMenuItems(this.contentFeatures),
        ],
      },
      {
        category: 'settings.sections.aboutApp',
        menuItems: this.mapToMenuItems(this.metaFeatures),
      },
    ];
  }

  private mapToMenuItems(features: ModuleInfo[]): any[] {
    return features.map((feature) => ({
      title: feature.name,
      icon: feature.icon,
      baseRoutingPath: feature.baseRoutingPath,
    }));
  }
}
