import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SettingsMenuPageComponent } from './settings-menu-page.component';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular/standalone';
import {
  CoreTestModule,
  createModuleInfo,
  MenuService,
  ModuleCategory,
  ModuleRegistryService,
  NavigationService,
} from '@sw-code/urbo-core';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../../test/transloco-testing.module';

describe('SettingsMenuPage', () => {
  let component: SettingsMenuPageComponent;
  let fixture: ComponentFixture<SettingsMenuPageComponent>;
  let mockModuleRegistryService: jest.Mocked<ModuleRegistryService>;
  let mockNavigationService: jest.Mocked<NavigationService>;
  let mockNavController: jest.Mocked<NavController>;
  let router: jest.Mocked<Router>;

  beforeEach(async () => {
    const menuSpy = {
      getMenuItems: jest.fn(),
    };

    mockModuleRegistryService = {
      getMatchingModuleInfo: jest.fn().mockReturnValue(
        createModuleInfo({
          name: 'moduleName',
          category: ModuleCategory.information,
          icon: 'moduleIcon',
          baseRoutingPath: 'moduleBaseRoutingPath',
        }),
      ),
      getModuleInfos: jest.fn().mockReturnValue([]),
      getModuleVisibility: jest.fn(),
      saveModuleVisibilitySettings: jest.fn(),
    } as any;

    mockNavigationService = {
      navigateToForyouSettings: jest.fn(),
    } as any;

    mockNavController = {
      navigateForward: jest.fn().mockReturnValue(Promise.resolve(true)),
    } as any;

    await TestBed.configureTestingModule({
      providers: [
        { provide: MenuService, useValue: menuSpy },
        { provide: ModuleRegistryService, useValue: mockModuleRegistryService },
        { provide: NavigationService, useValue: mockNavigationService },
        { provide: NavController, useValue: mockNavController },
      ],
      imports: [
        SettingsMenuPageComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(SettingsMenuPageComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router) as jest.Mocked<Router>;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
