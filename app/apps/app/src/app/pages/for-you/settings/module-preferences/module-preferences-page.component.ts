import { Component, inject, OnInit } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  CheckboxSettingComponent,
  CondensedHeaderLayoutComponent,
  SkeletonSettingsComponent,
} from '@sw-code/urbo-ui';
import { IonList, IonListHeader } from '@ionic/angular/standalone';
import { NgFor, NgIf } from '@angular/common';
import {
  FeedUpdateService,
  MenuItem,
  MenuItemGroup,
  MenuService,
  ModuleInfo,
  ModuleRegistryService,
} from '@sw-code/urbo-core';

@Component({
  selector: 'app-module-preferences-settings',
  templateUrl: './module-preferences-page.component.html',
  styleUrls: ['./module-preferences-page.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    IonListHeader,
    NgIf,
    SkeletonSettingsComponent,
    NgFor,
    IonList,
    CheckboxSettingComponent,
  ],
})
export class ModulePreferencesPageComponent implements OnInit {
  menuItemGroups: MenuItemGroup[] = [];
  toggleStates: { [key: string]: boolean } = {};
  moduleInfos: ModuleInfo[] = [];
  isLoadingModulePreferences = true;
  private readonly menuService = inject(MenuService);
  private readonly moduleRegistryService = inject(ModuleRegistryService);
  private readonly feedUpdateService = inject(FeedUpdateService);

  async ngOnInit() {
    this.moduleInfos = this.moduleRegistryService.getModuleInfos(
      (moduleInfo) => moduleInfo.forYouRelevant,
    );
    this.menuItemGroups = this.menuService.getMenuItems(
      (moduleInfo) => moduleInfo.forYouRelevant,
    );
    await this.loadToggleStates();
    this.isLoadingModulePreferences = false;
  }

  async loadToggleStates() {
    for (const group of this.menuItemGroups ?? []) {
      for (const item of group.menuItems) {
        this.toggleStates[item.baseRoutingPath] =
          await this.moduleRegistryService.getModuleVisibility(
            this.moduleInfos,
            item,
          );
      }
    }
  }

  isLastItemOfLastGroup(index: number, menuItemGroup: MenuItemGroup): boolean {
    const lastGroupIndex = this.menuItemGroups.length - 1;
    const isLastGroup = menuItemGroup === this.menuItemGroups[lastGroupIndex];
    const isLastItem = index === menuItemGroup.menuItems.length - 1;

    return isLastGroup && isLastItem;
  }

  async toggleChanged(menuItem: MenuItem, checked: boolean) {
    const matchingModuleInfo = this.moduleRegistryService.getMatchingModuleInfo(
      this.moduleInfos,
      menuItem.baseRoutingPath,
    );

    if (matchingModuleInfo) {
      await this.moduleRegistryService.saveModuleVisibilitySettings(
        matchingModuleInfo,
        checked,
      );
      this.feedUpdateService.notifyFeedUpdate();
    }
  }
}
