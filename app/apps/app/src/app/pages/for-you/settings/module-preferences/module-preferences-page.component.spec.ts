import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ModulePreferencesPageComponent } from './module-preferences-page.component';
import {
  CoreTestModule,
  createModuleInfo,
  MenuItemGroup,
  MenuService,
  ModuleCategory,
  ModuleRegistryService,
  SettingsPersistenceService,
} from '@sw-code/urbo-core';
import { getTranslocoModule } from '../../../../../test/transloco-testing.module';

describe('ModulePreferencesPage', () => {
  let component: ModulePreferencesPageComponent;
  let fixture: ComponentFixture<ModulePreferencesPageComponent>;
  let menuService: jest.Mocked<MenuService>;

  let mockSettingsPersistenceService: jest.Mocked<SettingsPersistenceService>;
  let mockModuleRegistryService: jest.Mocked<ModuleRegistryService>;

  beforeEach(async () => {
    const menuSpy = {
      getMenuItems: jest.fn(),
    };

    mockSettingsPersistenceService = {
      getSettingsValues: jest.fn(),
      saveSettings: jest.fn(),
    } as any;

    mockModuleRegistryService = {
      getMatchingModuleInfo: jest.fn().mockReturnValue(
        createModuleInfo({
          name: 'moduleName',
          category: ModuleCategory.information,
          icon: 'moduleIcon',
          baseRoutingPath: 'moduleBaseRoutingPath',
        }),
      ),
      getModuleInfos: jest.fn(),
      getModuleVisibility: jest.fn(),
      saveModuleVisibilitySettings: jest.fn(),
    } as any;

    await TestBed.configureTestingModule({
      providers: [
        { provide: MenuService, useValue: menuSpy },
        {
          provide: SettingsPersistenceService,
          useValue: mockSettingsPersistenceService,
        },
        { provide: ModuleRegistryService, useValue: mockModuleRegistryService },
      ],
      imports: [
        ModulePreferencesPageComponent,
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ModulePreferencesPageComponent);
    component = fixture.componentInstance;
    menuService = TestBed.inject(MenuService) as jest.Mocked<MenuService>;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should initialize properties using services', async () => {
      const mockMenuItems: MenuItemGroup[] = [
        {
          category: ModuleCategory.information,
          menuItems: [
            { title: 'Item 1', baseRoutingPath: '/path1', icon: 'icon1' },
          ],
        },
      ];

      menuService.getMenuItems.mockReturnValue(mockMenuItems);
      await component.ngOnInit();
      expect(component.menuItemGroups).toEqual(mockMenuItems);
    });
  });

  describe('ion-toggle component', () => {
    beforeEach(waitForAsync(() => {
      const mockMenuItems: MenuItemGroup[] = [
        {
          category: ModuleCategory.information,
          menuItems: [
            { title: 'Item 1', baseRoutingPath: '/path1', icon: 'icon1' },
            { title: 'Item 2', baseRoutingPath: '/path2', icon: 'icon2' },
          ],
        },
      ];
      menuService.getMenuItems.mockReturnValue(mockMenuItems);
      component.ngOnInit();
      fixture.detectChanges();
    }));

    it('should call toggleChanged function with correct parameters when toggle value changes', async () => {
      const toggleChangedSpy = jest.spyOn(component, 'toggleChanged');
      const compiled = fixture.nativeElement;
      const toggleElement = compiled.querySelector('ion-toggle');

      toggleElement.click();

      expect(toggleChangedSpy).toHaveBeenCalledWith(
        expect.objectContaining({
          title: 'Item 1',
          baseRoutingPath: '/path1',
        }),
        true,
      );
    });
  });

  describe('HTML Rendering', () => {
    it('should display correct title from translation', () => {
      const compiled = fixture.nativeElement;
      const titleElement = compiled.querySelector('ion-title');
      expect(titleElement.textContent).toBe('Your "For you"');
    });

    it('should display menu items from menuItemGroups', () => {
      const mockMenuItems: MenuItemGroup[] = [
        {
          category: ModuleCategory.help,
          menuItems: [
            { title: 'Item 1', baseRoutingPath: '/path1', icon: 'icon1' },
          ],
        },
      ];
      menuService.getMenuItems.mockReturnValue(mockMenuItems);
      component.ngOnInit();
      fixture.detectChanges();

      const compiled = fixture.nativeElement;
      const itemElements = compiled.querySelectorAll(
        'ion-list > lib-checkbox-setting',
      );

      expect(itemElements.length).toBe(1);
      expect(itemElements[0].textContent).toContain('Item 1');
    });
  });

  describe('isLastItemOfLastGroup', () => {
    beforeEach(async () => {
      component.menuItemGroups = [
        {
          category: ModuleCategory.information,
          menuItems: [
            {
              title: 'Freetime Item 1',
              baseRoutingPath: '/freetime1',
              icon: 'icon1',
            },
            {
              title: 'Freetime Item 2',
              baseRoutingPath: '/freetime2',
              icon: 'icon2',
            },
          ],
        },
        {
          category: ModuleCategory.information,
          menuItems: [
            { title: 'Todo Item 1', baseRoutingPath: '/todo1', icon: 'icon3' },
          ],
        },
        {
          category: ModuleCategory.help,
          menuItems: [
            {
              title: 'Co-creation Item 1',
              baseRoutingPath: '/cocreation1',
              icon: 'icon4',
            },
            {
              title: 'Co-creation Item 2',
              baseRoutingPath: '/cocreation2',
              icon: 'icon5',
            },
            {
              title: 'Co-creation Item 3',
              baseRoutingPath: '/cocreation3',
              icon: 'icon6',
            },
          ],
        },
      ];
    });

    it('should return true for the last item of the last group', () => {
      const lastGroupIndex = component.menuItemGroups.length - 1;
      const lastItemIndex =
        component.menuItemGroups[lastGroupIndex].menuItems.length - 1;

      expect(
        component.isLastItemOfLastGroup(
          lastItemIndex,
          component.menuItemGroups[lastGroupIndex],
        ),
      ).toBe(true);
    });

    it('should return false for an item that is not the last item of the last group', () => {
      const middleGroupIndex = 1;
      const itemIndex = 0;

      expect(
        component.isLastItemOfLastGroup(
          itemIndex,
          component.menuItemGroups[middleGroupIndex],
        ),
      ).toBe(false);
    });

    it('should return false for the last item of a group that is not the last group', () => {
      const notLastGroupIndex = 0;
      const lastItemIndex =
        component.menuItemGroups[notLastGroupIndex].menuItems.length - 1;

      expect(
        component.isLastItemOfLastGroup(
          lastItemIndex,
          component.menuItemGroups[notLastGroupIndex],
        ),
      ).toBe(false);
    });

    it('should return false for the first item of the last group', () => {
      const lastGroupIndex = component.menuItemGroups.length - 1;
      const firstItemIndex = 0;

      expect(
        component.isLastItemOfLastGroup(
          firstItemIndex,
          component.menuItemGroups[lastGroupIndex],
        ),
      ).toBe(false);
    });
  });
});
