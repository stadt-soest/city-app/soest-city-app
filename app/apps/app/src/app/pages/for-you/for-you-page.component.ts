import { Component, inject, OnInit, ViewChild } from '@angular/core';
import { MenuController } from '@ionic/angular/standalone';
import { HighlightedSectionComponent } from '../../components/for-you/highlighted-section/highlighted-section.component';
import { AsyncPipe, NgComponentOutlet, NgFor, NgIf } from '@angular/common';
import { YourDaySectionComponent } from '../../components/for-you/your-day-section/your-day-section.component';
import { FeedNavigationSectionComponent } from '../../components/for-you/feed-navigation-section/feed-navigation-section.component';
import { FeedInfiniteSectionComponent } from '../../components/for-you/feed-infinite-section/feed-infinite-section.component';
import { TranslocoDirective } from '@jsverse/transloco';
import {
  CondensedHeaderLayoutComponent,
  InfiniteScrollComponent,
  RefresherComponent,
} from '@sw-code/urbo-ui';
import { FeedPrioritySectionComponent } from '../../components/for-you/feed-priority-section/feed-priority-section.component';
import { AbstractForYouPageComponent } from '../../components/for-you/for-you-abstract/abstract-for-you-page.component';
import { FeedCategory } from '../../enums/feed-category';
import {
  CustomSectionService,
  FeatureSection,
  FeedItem,
  TabRefreshService,
  UpdateService,
} from '@sw-code/urbo-core';
import { firstValueFrom, Observable, take } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'app-for-you',
  templateUrl: 'for-you-page.component.html',
  imports: [
    TranslocoDirective,
    RefresherComponent,
    HighlightedSectionComponent,
    NgFor,
    NgComponentOutlet,
    YourDaySectionComponent,
    FeedNavigationSectionComponent,
    NgIf,
    FeedInfiniteSectionComponent,
    InfiniteScrollComponent,
    AsyncPipe,
    FeedPrioritySectionComponent,
    CondensedHeaderLayoutComponent,
  ],
})
export class ForYouPageComponent
  extends AbstractForYouPageComponent
  implements OnInit
{
  @ViewChild(FeedPrioritySectionComponent)
  prioritySection!: FeedPrioritySectionComponent;
  @ViewChild(CondensedHeaderLayoutComponent)
  condensedHeaderLayout!: CondensedHeaderLayoutComponent;
  readonly feedCategory = FeedCategory;
  feedItemsUnfiltered: FeedItem[] = [];
  moreFeedItemsUnfilteredAvailable = true;
  olderFeedItemsPage = 2;
  unfilteredFeedItemsPage = 1;
  updateAvailable = false;
  customSections$!: Observable<FeatureSection[]>;
  private readonly menu = inject(MenuController);
  private readonly updateService = inject(UpdateService);
  private readonly menuSettingKey = 'settingsMenuDashboard';
  private readonly scrollToTopDuration: number = 500;
  private readonly refreshService = inject(TabRefreshService);
  private readonly customSectionService = inject(CustomSectionService);

  get shouldShowOlderInfiniteScroll(): boolean {
    return (
      this.feedItemsOlder.length > 0 &&
      this.moreFeedItemsNewAvailable &&
      !this.loadingOlderFeedItems
    );
  }

  get shouldShowUnfilteredInfiniteScroll(): boolean {
    return (
      this.feedItemsUnfiltered.length > 0 &&
      this.moreFeedItemsUnfilteredAvailable &&
      !this.loadingUnfilteredFeedItems
    );
  }

  override ngOnInit() {
    super.ngOnInit();
    this.loadCustomSections();
    this.subscribeToTabRefresh();
    this.subscribeToUnfilteredFeedItems();
    this.subscribeToUpdateService();
  }

  async openSettingsMenu() {
    await this.menu.open(this.menuSettingKey);
  }

  async closeSettingsMenu() {
    await this.menu.close(this.menuSettingKey);
  }

  ionViewWillEnter(): void {
    this.menu.enable(true, this.menuSettingKey);
  }

  loadMoreOlderFeedItems(event: any) {
    this.loadingOlderFeedItems = true;
    this.forYouService
      .getFilteredFeedItems(this.olderFeedItemsPage)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((feedItems) => {
        this.moreFeedItemsNewAvailable = feedItems.length > 0;
        this.feedItemsOlder.push(...feedItems);
        this.loadingOlderFeedItems = false;
        this.olderFeedItemsPage++;
        event?.target?.complete();
      });
  }

  loadMoreUnfilteredFeedItems(event: any) {
    this.loadingUnfilteredFeedItems = true;
    this.forYouService
      .getUnfilteredFeedItems(this.unfilteredFeedItemsPage)
      .pipe(take(1))
      .subscribe((feedItemsUnfiltered) => {
        this.moreFeedItemsUnfilteredAvailable = feedItemsUnfiltered.length > 0;
        this.feedItemsUnfiltered.push(...feedItemsUnfiltered);
        this.loadingUnfilteredFeedItems = false;
        this.unfilteredFeedItemsPage++;
        event?.target?.complete();
      });
  }

  async refreshView(event?: any): Promise<void> {
    this.olderFeedItemsPage = 1;
    this.unfilteredFeedItemsPage = 1;

    this.resetDataAndStates();
    await this.refreshData();
    this.prioritySection?.updateWelcomeCard();
    await this.condensedHeaderLayout.scrollToTop(this.scrollToTopDuration);
    event?.target.complete();
  }

  private resetDataAndStates(): void {
    this.feedItemsNew = [];
    this.feedItemsOlder = [];
    this.feedItemsUpcomingEvents = [];
    this.feedItemsUnfiltered = [];
    this.activityFeedItems = [];

    this.loadingNewFeedItems = true;
    this.loadingUpcomingEventsFeedItems = true;
  }

  private async refreshData(): Promise<void> {
    await Promise.all([
      this.subscribeToFeedItems(),
      this.subscribeToUpcomingEventsFeedItems(),
      this.subscribeToUnfilteredFeedItems(),
    ]);
  }

  private async subscribeToUnfilteredFeedItems(): Promise<void> {
    const feedItemsUnfiltered = await firstValueFrom(
      this.forYouService.getUnfilteredFeedItems(1).pipe(take(1)),
    );
    this.feedItemsUnfiltered = feedItemsUnfiltered;
    this.moreFeedItemsUnfilteredAvailable = feedItemsUnfiltered.length > 0;
    this.loadingOlderFeedItems = false;
  }

  private async subscribeToUpdateService() {
    this.updateService.updateAvailable$
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((updateAvailable) => {
        if (updateAvailable) {
          this.updateAvailable = true;
        }
      });
  }

  private subscribeToTabRefresh() {
    this.refreshService.refresh$
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(() => {
        this.refreshView();
      });
  }

  private loadCustomSections(): void {
    this.customSections$ = this.customSectionService.loadCustomSections();
  }
}
