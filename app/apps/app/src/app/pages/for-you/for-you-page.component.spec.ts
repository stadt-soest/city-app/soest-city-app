import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { Component } from '@angular/core';
import {
  AbstractFeedCardComponent,
  CoreTestModule,
  FeedItem,
  ForYouService,
  StorageService,
  UpdateService,
} from '@sw-code/urbo-core';
import { MenuController } from '@ionic/angular/standalone';
import { ForYouPageComponent } from './for-you-page.component';
import { AppTestModule } from '../../../test/app-test.module';
import { getTranslocoModule } from '../../../test/transloco-testing.module';

describe('ForYouPage', () => {
  @Component({
    template: '<p>Mock</p>',
  })
  class MockFeedCardComponent extends AbstractFeedCardComponent<FeedItem> {}

  let mockMenuController: jest.Mocked<MenuController>;
  let component: ForYouPageComponent;
  let fixture: ComponentFixture<ForYouPageComponent>;
  let mockForYouService: jest.Mocked<ForYouService>;
  let mockStorageService: jest.Mocked<StorageService>;

  const mockedFeedItems = of([
    {
      id: 'test id',
      moduleId: 'test module id',
      moduleName: 'test modul name',
      baseRoutePath: '/test2',
      icon: 'test icon',
      title: 'Sample Title 1',
      text: 'Sample Text 1',
      date: new Date('2023-10-01T00:00:00'),
      cardComponent: MockFeedCardComponent,
      forYouComponent: MockFeedCardComponent,
    },
    {
      id: 'test id2',
      moduleId: 'test module id2',
      moduleName: 'test modul name2',
      baseRoutePath: '/test2',
      icon: 'test icon2',
      title: 'Sample Title 2',
      text: 'Sample Text 2',
      date: new Date('2023-10-01T00:00:00'),
      cardComponent: MockFeedCardComponent,
      forYouComponent: MockFeedCardComponent,
    },
  ]);

  beforeEach(() => {
    mockForYouService = {
      getFilteredFeedItems: jest.fn().mockReturnValue(mockedFeedItems),
      hasContent: jest.fn().mockResolvedValue(of(true)),
      hasContentUpcoming: jest.fn().mockResolvedValue(of(true)),
      hasContentUnfiltered: jest.fn().mockResolvedValue(of(true)),
      getFilteredFeedItemsUpcoming: jest.fn().mockReturnValue(mockedFeedItems),
      getHighlightedFeedItems: jest.fn().mockResolvedValue(mockedFeedItems),
      getUnfilteredFeedItems: jest.fn().mockReturnValue(mockedFeedItems),
      getActivityFeedItems: jest.fn().mockResolvedValue(mockedFeedItems),
      getPriorityFeedItems: jest.fn().mockResolvedValue(mockedFeedItems),
    } as any;

    mockMenuController = {
      open: jest.fn(),
      close: jest.fn(),
      enable: jest.fn(),
    } as any;

    mockStorageService = {
      get: jest.fn(),
      getKeys: jest.fn(),
      set: jest.fn(),
      generateStorageKey: jest.fn(),
      remove: jest.fn(),
    } as unknown as jest.Mocked<StorageService>;

    class UpdateServiceStub {
      updateAvailable$ = of(false);
      performUpdate = jest.fn();
    }

    TestBed.configureTestingModule({
      imports: [
        ForYouPageComponent,
        CoreTestModule.forRoot(),
        AppTestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        { provide: UpdateService, useClass: UpdateServiceStub },
        { provide: ForYouService, useValue: mockForYouService },
        { provide: MenuController, useValue: mockMenuController },
        { provide: StorageService, useValue: mockStorageService },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ForYouPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ionViewWillEnter', () => {
    it('should enable menu', () => {
      component.ionViewWillEnter();
      expect(mockMenuController.enable).toHaveBeenCalledWith(
        true,
        'settingsMenuDashboard',
      );
    });
  });

  describe('openSettingsMenu', () => {
    it('should call the open method of the MenuController with the menuSettingKey', async () => {
      await component.openSettingsMenu();
      expect(mockMenuController.open).toHaveBeenCalledWith(
        'settingsMenuDashboard',
      );
    });
  });

  describe('closeSettingsMenu', () => {
    it('should call the close method of the MenuController with the menuSettingKey', async () => {
      await component.closeSettingsMenu();
      expect(mockMenuController.close).toHaveBeenCalledWith(
        'settingsMenuDashboard',
      );
    });
  });

  describe('loadMoreUnfilteredFeedItems', () => {
    it('should push new feed items to feedItemsNew when loadMoreUnfilteredFeedItems is called', () => {
      component.feedItemsUnfiltered = [];
      component.loadMoreUnfilteredFeedItems({});
      expect(component.feedItemsUnfiltered.length).toBe(2);
    });

    it('should set moreFeedItemsUnfilteredAvailable  to false when getFeedItems does not return anything', () => {
      mockForYouService.getUnfilteredFeedItems.mockReturnValue(of([]));
      component.loadMoreUnfilteredFeedItems({});
      expect(component.moreFeedItemsUnfilteredAvailable).toBe(false);
    });
  });

  describe('loadMoreOlderFeedItems', () => {
    it('should push new feed items to feedItemsOlder when loadMoreOlderFeedItems is called', () => {
      component.feedItemsNew = [];
      component.loadMoreOlderFeedItems({});
      expect(component.feedItemsOlder.length).toBe(2);
    });

    it('should set moreFeedItemsNewAvailable to false when getFeedItems does not return anything', () => {
      mockForYouService.getFilteredFeedItems.mockReturnValue(of([]));
      component.loadMoreOlderFeedItems({});
      expect(component.moreFeedItemsNewAvailable).toBe(false);
    });
  });

  describe('HTML Rendering', () => {
    it('should display correct title', async () => {
      const compiled = fixture.nativeElement;
      const titleElement = compiled.querySelector('ion-title');
      expect(titleElement.textContent).toContain('For You');
    });
  });
});
