import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { SearchPageComponent } from './search-page.component';
import {
  CoreTestModule,
  ModuleSearchResult,
  NavigationService,
  SearchService,
} from '@sw-code/urbo-core';
import { of } from 'rxjs';
import { getTranslocoModule } from '../../../test/transloco-testing.module';

describe('SearchPage', () => {
  let component: SearchPageComponent;
  let fixture: ComponentFixture<SearchPageComponent>;
  let searchServiceMock: Partial<SearchService<any>>;
  let navigationServiceMock: Partial<NavigationService>;

  const mockSearchResults: ModuleSearchResult<any>[] = [
    {
      moduleId: 'testModule',
      items: [
        { id: 1, name: 'Test Item 1' },
        { id: 2, name: 'Test Item 2' },
      ],
      total: 2,
    },
  ];

  beforeEach(waitForAsync(() => {
    searchServiceMock = {
      search: jest.fn((term) => of([])),
      getLastSearches: jest.fn(() => of(['testSearch1', 'testSearch2'])),
      initLastSearches: jest.fn(),
      updateSearchHistory: jest.fn(),
      getSortedModuleIdsByAggregateRelevancy: jest.fn(),
    };

    navigationServiceMock = {
      navigateToFeedItemDetails: jest.fn().mockResolvedValue(true),
    };

    TestBed.configureTestingModule({
      imports: [
        SearchPageComponent,
        CoreTestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [
        { provide: SearchService, useValue: searchServiceMock },
        { provide: NavigationService, useValue: navigationServiceMock },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(SearchPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display correct header', async () => {
    const compiled = fixture.nativeElement;
    const settingsHeader = compiled.querySelector('ion-title');
    expect(settingsHeader.textContent).toContain('Search');
  });

  it('should call searchService.search when performSearch is called', () => {
    const searchTerm = 'test';
    component.searchControl.setValue(searchTerm);
    component.performSearch();
    expect(searchServiceMock.search).toHaveBeenCalledWith(searchTerm);
  });

  it('should display search-skeleton when a search is in progress', () => {
    component.isLoading = true;
    fixture.detectChanges();
    const searchSkeleton = fixture.nativeElement.querySelector(
      'app-search-skeleton',
    );
    expect(searchSkeleton).not.toBeNull();
  });

  it('should not display loading indicator when not loading', () => {
    component.isLoading = false;
    fixture.detectChanges();
    const loadingIndicator = fixture.nativeElement.querySelector(
      '.loading-container ion-spinner',
    );
    expect(loadingIndicator).toBeNull();
  });

  it('should display no results message when there are no search results', () => {
    component.searchResults$ = of([]);
    component.searchPerformed = true;
    fixture.detectChanges();
    const noResultsMessage = fixture.nativeElement.querySelector(
      '.no-results-message',
    );
    expect(noResultsMessage).not.toBeNull();
    expect(noResultsMessage.textContent).toContain(
      "orry, I haven't found anything on this. \n \n \nMaybe I didn't quite understand what you mean. " +
        'Please try again with a different term.',
    );
  });

  it('should display search results and then clear them when the search input is cleared', () => {
    searchServiceMock.search = jest.fn().mockReturnValue(of(mockSearchResults));

    component.searchControl.setValue('test');
    component.performSearch();
    fixture.detectChanges();

    let searchResultItems =
      fixture.nativeElement.querySelectorAll('.search-item');
    expect(searchResultItems.length).toBeGreaterThan(0);

    component.searchControl.setValue('');
    fixture.detectChanges();

    searchResultItems = fixture.nativeElement.querySelectorAll('.search-item');
    expect(searchResultItems.length).toBe(0);
  });
});
