import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { SearchDetailsPageComponent } from './search-details-page.component';
import { CoreTestModule, PoiService, SearchService } from '@sw-code/urbo-core';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../../test/transloco-testing.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute, convertToParamMap, Params } from '@angular/router';

describe('SearchDetailsPage', () => {
  let component: SearchDetailsPageComponent;
  let fixture: ComponentFixture<SearchDetailsPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        SearchDetailsPageComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
        RouterTestingModule,
      ],
      providers: [SearchService, PoiService],
    }).compileComponents();

    fixture = TestBed.createComponent(SearchDetailsPageComponent);
    component = fixture.componentInstance;

    const activatedRoute = TestBed.inject(ActivatedRoute);
    const params: Params = { moduleId: 'moduleId' };
    activatedRoute.snapshot.params = convertToParamMap(params);
    activatedRoute.snapshot.queryParams = { searchTerm: 'searchTerm' };

    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
