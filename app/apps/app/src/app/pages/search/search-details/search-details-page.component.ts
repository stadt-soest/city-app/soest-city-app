import { Component, inject, OnInit } from '@angular/core';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import {
  CondensedHeaderLayoutComponent,
  FeedLoadingForYouSkeletonComponent,
  RefresherComponent,
  SkeletonFeedItemComponent,
} from '@sw-code/urbo-ui';
import {
  IonCol,
  IonInfiniteScroll,
  IonInfiniteScrollContent,
  IonRow,
} from '@ionic/angular/standalone';
import { NgComponentOutlet, NgFor, NgIf } from '@angular/common';
import {
  FeedItem,
  FeedItemLayoutType,
  ModuleRegistryService,
  PoiService,
  SearchService,
  TitleService,
} from '@sw-code/urbo-core';
import { ActivatedRoute } from '@angular/router';
import { switchMap, take } from 'rxjs';

@Component({
  selector: 'app-search-details',
  templateUrl: './search-details-page.component.html',
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    RefresherComponent,
    IonRow,
    NgFor,
    IonCol,
    NgComponentOutlet,
    NgIf,
    FeedLoadingForYouSkeletonComponent,
    SkeletonFeedItemComponent,
    IonInfiniteScroll,
    IonInfiniteScrollContent,
  ],
})
export class SearchDetailsPageComponent implements OnInit {
  moduleId = '';
  moduleNameLocalizationId = '';
  searchResults: FeedItem[] = [];
  currentPage = 1;
  pageSize = 20;
  isLoading = false;
  hasMoreResults = true;
  totalResults = 0;
  currentSearchTerm = '';
  protected readonly feedItemLayoutType = FeedItemLayoutType;
  private readonly route = inject(ActivatedRoute);
  private readonly searchService = inject<SearchService<FeedItem>>(
    SearchService<FeedItem>,
  );
  private readonly modules = inject(ModuleRegistryService);
  private readonly poiService = inject(PoiService);
  private readonly titleService = inject(TitleService);
  private readonly translateService = inject(TranslocoService);

  get shouldShowInfiniteScroll() {
    return (
      this.searchResults.length > 0 && this.hasMoreResults && !this.isLoading
    );
  }

  ngOnInit() {
    this.route.params.pipe(take(1)).subscribe((params) => {
      this.moduleId = params['moduleId'];

      const maybeModuleInfo = this.modules.getById(this.moduleId)?.info;

      if (maybeModuleInfo !== undefined) {
        this.moduleNameLocalizationId = maybeModuleInfo.name;
      } else {
        console.error(`invalid url (module '${this.moduleId}' does not exist)`);
      }

      this.handleModuleInit();
    });
  }

  loadMoreResults(event?: any) {
    if (this.isLoading || !this.hasMoreResults) {
      return;
    }

    this.isLoading = true;
    const searchTerm =
      this.route.snapshot.queryParamMap.get('searchTerm') ?? '';

    this.currentPage++;

    this.searchService
      .searchByModule(
        searchTerm,
        this.moduleId,
        this.currentPage,
        this.pageSize,
      )
      .pipe(take(1))
      .subscribe((results) => {
        this.searchResults.push(...results.items);
        this.totalResults = results.total;
        this.hasMoreResults =
          (this.currentPage + 1) * this.pageSize < this.totalResults;
        this.isLoading = false;
        event?.target.complete();
      });
  }

  hasLayoutType(type: FeedItemLayoutType): boolean {
    return this.searchResults.some((result) => result.layoutType === type);
  }

  refreshView(event: any) {
    this.resetState();
    this.handleModuleInit();
    event.target.complete();
  }

  private handleModuleInit() {
    if (this.moduleId === 'map') {
      this.moduleNameLocalizationId = 'search.poi_results_title';
      this.initPoiSearch();
    } else {
      this.initOtherModuleSearch();
    }
  }

  private initPoiSearch() {
    this.route.queryParams
      .pipe(
        switchMap((queryParams) => {
          this.currentSearchTerm = queryParams['searchTerm'];
          return this.poiService.searchPois(this.currentSearchTerm);
        }),
        take(1),
      )
      .subscribe((results) => {
        this.searchResults = results;
        this.isLoading = false;
        this.updateTitle();
      });
  }

  private initOtherModuleSearch() {
    this.route.queryParams
      .pipe(
        switchMap((queryParams) => {
          this.currentSearchTerm = queryParams['searchTerm'];
          return this.searchService.searchByModule(
            this.currentSearchTerm,
            this.moduleId,
            this.currentPage,
            this.pageSize,
          );
        }),
        take(1),
      )
      .subscribe((results) => {
        this.searchResults = results.items;
        this.totalResults = results.total;
        this.hasMoreResults =
          (this.currentPage + 1) * this.pageSize < this.totalResults;
        this.updateTitle();
      });
  }

  private resetState() {
    this.searchResults = [];
    this.currentPage = 1;
    this.isLoading = false;
    this.hasMoreResults = true;
    this.totalResults = 0;
  }

  private updateTitle() {
    const searchTerm = this.currentSearchTerm;
    const moduleNameTranslationId = this.moduleNameLocalizationId;

    this.translateService
      .selectTranslate(moduleNameTranslationId, {}, '')
      .pipe(
        switchMap((moduleName) =>
          this.translateService.selectTranslate(
            'search.main_title_with_term_and_module',
            {
              searchTerm,
              moduleName,
            },
            '',
          ),
        ),
        take(1),
      )
      .subscribe((translatedTitle) => {
        this.titleService.setTitle(translatedTitle);
      });
  }
}
