import {
  AfterViewInit,
  Component,
  DestroyRef,
  inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Observable, of } from 'rxjs';
import { RouterLink } from '@angular/router';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import {
  IonRouterLink,
  IonSearchbar,
  IonText,
} from '@ionic/angular/standalone';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AsyncPipe, NgFor, NgIf, SlicePipe } from '@angular/common';
import { SearchSkeletonComponent } from './search-skeleton/search-skeleton.component';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import {
  CondensedHeaderLayoutComponent,
  ConnectivityErrorCardComponent,
  CustomButtonColorScheme,
  CustomButtonFontScheme,
  CustomButtonType,
  CustomHorizontalButtonComponent,
  GeneralIconComponent,
  IconColor,
  IconFontSize,
  IconType,
} from '@sw-code/urbo-ui';
import {
  ConnectivityService,
  FeedItem,
  KeyboardService,
  ModuleSearchResult,
  SearchService,
  TabRefreshService,
} from '@sw-code/urbo-core';

@Component({
  selector: 'app-search',
  templateUrl: 'search-page.component.html',
  styleUrls: ['search-page.component.scss'],
  imports: [
    TranslocoDirective,
    CondensedHeaderLayoutComponent,
    ConnectivityErrorCardComponent,
    IonSearchbar,
    FormsModule,
    ReactiveFormsModule,
    NgIf,
    SearchSkeletonComponent,
    NgFor,
    GeneralIconComponent,
    IonText,
    CustomHorizontalButtonComponent,
    RouterLink,
    IonRouterLink,
    AsyncPipe,
    SlicePipe,
  ],
})
export class SearchPageComponent implements OnInit, AfterViewInit {
  @ViewChild(IonSearchbar) searchbar!: IonSearchbar;
  searchControl = new FormControl();
  searchResults$!: Observable<ModuleSearchResult<FeedItem>[]>;
  lastSearchTerms$!: Observable<string[]>;
  hasSearchResults = false;
  searchPerformed = false;
  feedItems: ModuleSearchResult<FeedItem>[] = [];
  isLoading = false;
  moduleMap = new Map<string, string>();
  isConnected = true;
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
  protected readonly customButtonFontScheme = CustomButtonFontScheme;
  protected readonly iconType = IconType;
  protected readonly iconFontSize = IconFontSize;
  protected readonly iconColor = IconColor;
  private readonly searchService = inject<SearchService<FeedItem>>(
    SearchService<FeedItem>,
  );
  private readonly keyboardService = inject(KeyboardService);
  private readonly connectivityService = inject(ConnectivityService);
  private readonly destroyRef = inject(DestroyRef);
  private readonly refreshService = inject(TabRefreshService);
  private readonly translateService = inject(TranslocoService);

  ngOnInit() {
    this.setOnlineStatus();
    this.initSearchControl();
    this.initLastSearchTerms();
    this.subscribeToTabRefresh();
  }

  ngAfterViewInit() {
    this.setSearchbarAriaLabel();
  }

  async refreshView() {
    this.resetSearchState();
    await this.searchbar.setFocus();
  }

  performSearch(): void {
    const searchTerm = this.searchControl.value?.trim();
    if (!searchTerm) {
      this.handleEmptySearch();
      return;
    }

    this.isLoading = true;

    this.searchbar?.getInputElement().then((input) => input.blur());
    this.searchService.updateSearchHistory(searchTerm);
    this.executeSearch(searchTerm);
  }

  autoFillSearchTermAndSearch(term: string): void {
    this.searchControl.setValue(term);
    this.performSearch();
  }

  deleteSearchTerm(term: string): void {
    this.searchService.deleteSearchTerm(term).then(() => {
      this.initLastSearchTerms();
    });
  }

  private setSearchbarAriaLabel(): void {
    this.searchbar?.getInputElement().then((input) => {
      input.setAttribute(
        'aria-label',
        this.translateService.translate('search.main_title'),
      );
    });
  }

  private processSearchResults(results: ModuleSearchResult<FeedItem>[]): void {
    this.isLoading = false;
    this.keyboardService.hideKeyboard();
    this.searchPerformed = true;

    this.sortFeedItemsByTotal(results);
    this.populateModuleMap(results);

    this.hasSearchResults = results.some((result) => result.items.length > 0);
  }

  private sortFeedItemsByTotal(results: ModuleSearchResult<FeedItem>[]): void {
    const moduleItemCounts = new Map<string, number>();

    results.forEach((item) => {
      moduleItemCounts.set(item.moduleId, item.total);
    });

    const sortedModuleIds = Array.from(moduleItemCounts.entries())
      .sort((a, b) => a[1] - b[1])
      .map(([moduleId]) => moduleId);

    this.feedItems = sortedModuleIds.flatMap((moduleId) =>
      results.filter((item) => item.moduleId === moduleId),
    );
  }

  private async setOnlineStatus() {
    this.isConnected = await this.connectivityService.checkOnlineStatus();
  }

  private populateModuleMap(results: ModuleSearchResult<FeedItem>[]): void {
    this.moduleMap.clear();
    results.forEach((result) => {
      if (result.items.length > 0) {
        this.moduleMap.set(result.moduleId, result.items[0].pluralModuleName);
      }
    });
  }

  private initSearchControl(): void {
    this.searchControl.valueChanges.subscribe((value) => {
      if (value === '') {
        this.searchResults$ = of([]);
        this.searchPerformed = false;
        this.hasSearchResults = false;
        this.feedItems = [];
      }
    });
    this.resetSearchState();
  }

  private initLastSearchTerms(): void {
    this.searchService.initLastSearches();
    this.lastSearchTerms$ = this.searchService.getLastSearches();
  }

  private executeSearch(searchTerm: string): void {
    this.searchService.search(searchTerm).subscribe({
      next: (results) => this.processSearchResults(results),
      error: (error) => {
        console.error('Search failed:', error);
        this.isLoading = false;
      },
    });
  }

  private handleEmptySearch(): void {
    this.resetSearchState();
  }

  private resetSearchState(): void {
    if (this.searchbar) {
      this.searchResults$ = of([]);
      this.searchbar.value = '';
      this.hasSearchResults = false;
      this.searchPerformed = false;
      this.feedItems = [];
    }
  }

  private subscribeToTabRefresh() {
    this.refreshService.refresh$
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(() => {
        this.refreshView();
      });
  }
}
