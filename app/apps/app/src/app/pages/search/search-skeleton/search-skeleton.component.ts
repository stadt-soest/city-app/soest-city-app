import { Component } from '@angular/core';
import { NgFor } from '@angular/common';
import { IonRow, IonSkeletonText } from '@ionic/angular/standalone';

@Component({
  selector: 'app-search-skeleton',
  templateUrl: './search-skeleton.component.html',
  styleUrls: ['./search-skeleton.component.scss'],
  imports: [NgFor, IonSkeletonText, IonRow],
})
export class SearchSkeletonComponent {}
