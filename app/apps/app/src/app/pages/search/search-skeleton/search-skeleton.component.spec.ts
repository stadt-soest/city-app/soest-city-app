import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { SearchSkeletonComponent } from './search-skeleton.component';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../../test/transloco-testing.module';

describe('SearchSkeletonComponent', () => {
  let component: SearchSkeletonComponent;
  let fixture: ComponentFixture<SearchSkeletonComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        SearchSkeletonComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [],
    }).compileComponents();

    fixture = TestBed.createComponent(SearchSkeletonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
