import { InjectionToken } from '@angular/core';
import { AppConfig } from './interfaces/app-config.model';

export const APP_CONFIG: AppConfig = {
  name: 'SoestApp',
};
export const APP_CONFIG_TOKEN = new InjectionToken<AppConfig>('AppConfigToken');
