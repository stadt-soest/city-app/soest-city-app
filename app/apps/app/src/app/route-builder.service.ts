import { inject, Injectable } from '@angular/core';
import { TabsComponent } from './components/tabs/tabs.component';
import {
  finishedOnboardingGuard,
  hasNotFinishedOnboardingGuard,
  ModuleRegistryService,
} from '@sw-code/urbo-core';

@Injectable({
  providedIn: 'root',
})
export class RouteBuilderService {
  private readonly moduleRegistry = inject(ModuleRegistryService);

  getRoutes() {
    const featureRoutes = this.moduleRegistry.getFeatureRoutes();

    return [
      {
        path: 'not-found',
        loadComponent: () =>
          import('./pages/not-found/not-found-page.component').then(
            (m) => m.NotFoundPageComponent,
          ),
        title: 'not_found.title',
      },
      {
        path: 'onboarding',
        loadComponent: () =>
          import('./pages/onboarding/onboarding-page.component').then(
            (m) => m.OnboardingPageComponent,
          ),
        canActivate: [finishedOnboardingGuard],
      },
      {
        path: '',
        component: TabsComponent,
        children: [
          {
            path: '',
            canActivateChild: [hasNotFinishedOnboardingGuard],
            children: [
              {
                path: '',
                pathMatch: 'full',
                redirectTo: 'for-you',
              },
              {
                path: 'for-you',
                loadComponent: () =>
                  import('./pages/for-you/for-you-page.component').then(
                    (m) => m.ForYouPageComponent,
                  ),
                title: 'for_you.main_title',
              },
              {
                path: 'for-you/settings',
                loadComponent: () =>
                  import(
                    './pages/for-you/settings/settings-menu-page.component'
                  ).then((m) => m.SettingsMenuPageComponent),
                title: 'for_you.preferences',
              },
              {
                path: 'for-you/settings/module-preferences',
                loadComponent: () =>
                  import(
                    './pages/for-you/settings/module-preferences/module-preferences-page.component'
                  ).then((m) => m.ModulePreferencesPageComponent),
                title: 'for_you.for_you_menu_title',
              },
              {
                path: 'for-you/feed/:category',
                loadComponent: () =>
                  import(
                    './pages/for-you/feed/for-you-feed-page.component'
                  ).then((m) => m.ForYouFeedPageComponent),
              },
              {
                path: 'for-you/feed',
                pathMatch: 'full',
                redirectTo: 'for-you',
              },
              {
                path: 'search',
                loadComponent: () =>
                  import('./pages/search/search-page.component').then(
                    (m) => m.SearchPageComponent,
                  ),
                title: 'search.main_title',
              },
              {
                path: 'search/results',
                redirectTo: 'search',
              },
              {
                path: 'search/results/:moduleId',
                loadComponent: () =>
                  import(
                    './pages/search/search-details/search-details-page.component'
                  ).then((m) => m.SearchDetailsPageComponent),
              },
              {
                path: 'map/list',
                pathMatch: 'full',
                redirectTo: 'map',
              },
              {
                path: 'map/feature',
                pathMatch: 'full',
                redirectTo: 'map',
              },
              {
                path: 'map',
                loadComponent: () =>
                  import('./pages/map/map-page.component').then(
                    (m) => m.MapPageComponent,
                  ),
                title: 'tabs.map.title',
              },
              {
                path: 'map/:id',
                loadComponent: () =>
                  import(
                    './pages/map/map-details/map-details-page.component'
                  ).then((m) => m.MapDetailsPageComponent),
              },
              {
                path: 'search/results/map/:id',
                loadComponent: () =>
                  import(
                    './pages/map/map-details/map-details-page.component'
                  ).then((m) => m.MapDetailsPageComponent),
              },
              {
                path: 'map/list/:category-id',
                loadComponent: () =>
                  import('./pages/map/poi-list/poi-list-page.component').then(
                    (m) => m.PoiListPageComponent,
                  ),
              },
              {
                path: 'categories',
                loadComponent: () =>
                  import('./pages/categories/categories-page.component').then(
                    (m) => m.CategoriesPageComponent,
                  ),
                title: 'categories.main_title',
              },
            ],
          },
          ...featureRoutes,
        ],
      },
      {
        path: '**',
        redirectTo: 'not-found',
      },
    ];
  }
}
