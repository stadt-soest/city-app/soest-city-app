import { ErrorHandler, inject, Injectable } from '@angular/core';
import { MatomoTracker } from 'ngx-matomo-client';

@Injectable()
export class GlobalErrorHandlerService implements ErrorHandler {
  matomoTracker = inject(MatomoTracker);

  handleError(error: any): void {
    this.matomoTracker.trackEvent(
      'Exception',
      error.message || error.toString(),
      'Crash',
      1,
    );
    console.error(error);
  }
}
