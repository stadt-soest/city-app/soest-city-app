import { Component, inject, OnInit, ViewChild } from '@angular/core';
import { IonTabBar, IonTabButton, IonTabs } from '@ionic/angular/standalone';
import { TranslocoPipe } from '@jsverse/transloco';
import { GeneralIconComponent, IconFontSize, IconType } from '@sw-code/urbo-ui';
import { MapPageComponent } from '../../pages/map/map-page.component';
import {
  AppInitializationService,
  KeyboardService,
  TabRefreshService,
  TabType,
} from '@sw-code/urbo-core';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.component.html',
  styleUrls: ['tabs.component.scss'],
  imports: [
    IonTabs,
    IonTabBar,
    IonTabButton,
    GeneralIconComponent,
    TranslocoPipe,
  ],
})
export class TabsComponent implements OnInit {
  lastClickedTab: TabType = TabType.forYou;
  protected readonly tabType = TabType;
  protected readonly iconType = IconType;
  protected readonly iconFontSize = IconFontSize;
  @ViewChild(IonTabs, { static: true }) private readonly ionTabs!: IonTabs;
  @ViewChild(MapPageComponent) private readonly mapPage?: MapPageComponent;
  private readonly appInitService = inject(AppInitializationService);
  private readonly keyboardService = inject(KeyboardService);
  private readonly refreshService = inject(TabRefreshService);

  get isKeyboardOpen(): boolean {
    return this.keyboardService.isKeyboardOpen;
  }

  ngOnInit() {
    this.appInitService.setupBackButtonHandler(this.ionTabs);
  }

  async onTabClicked(tabType: TabType) {
    if (tabType === this.lastClickedTab) {
      this.refreshService.triggerRefresh(tabType);
    }
    this.lastClickedTab = tabType;
  }

  async onTabChange(event: any) {
    if (this.mapPage && event !== TabType.map) {
      await this.mapPage.closeFilterModal();
    }
  }
}
