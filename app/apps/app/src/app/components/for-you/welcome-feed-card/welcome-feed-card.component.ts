import { Component, DestroyRef, inject, OnInit } from '@angular/core';
import { interval } from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { TranslocoDirective } from '@jsverse/transloco';
import { IonCol, IonRow } from '@ionic/angular/standalone';
import { AsyncPipe, NgIf } from '@angular/common';
import {
  IconFontSize,
  IconType,
  LocalizedTimeDistancePipePipe,
} from '@sw-code/urbo-ui';

@Component({
  selector: 'app-welcome-feed-card',
  templateUrl: './welcome-feed-card.component.html',
  styleUrls: ['./welcome-feed-card.component.scss'],
  imports: [
    TranslocoDirective,
    IonCol,
    IonRow,
    NgIf,
    AsyncPipe,
    LocalizedTimeDistancePipePipe,
  ],
})
export class WelcomeFeedCardComponent implements OnInit {
  lastUpdated: Date = new Date();
  hourOfDay = new Date().getHours();
  protected readonly iconType = IconType;
  protected readonly iconFontSize = IconFontSize;
  private readonly destroyRef = inject(DestroyRef);

  ngOnInit() {
    this.setupTimeUpdate();
  }

  public updateLastUpdatedDate(): void {
    this.lastUpdated = new Date();
  }

  private setupTimeUpdate(): void {
    interval(10000)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(() => {
        if (this.lastUpdated !== null) {
          this.lastUpdated = new Date(this.lastUpdated.getTime());
        }
      });
  }
}
