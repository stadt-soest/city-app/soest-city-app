import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslocoService } from '@jsverse/transloco';
import { WelcomeFeedCardComponent } from './welcome-feed-card.component';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../../test/transloco-testing.module';

describe('WelcomeFeedCard', () => {
  let component: WelcomeFeedCardComponent;
  let fixture: ComponentFixture<WelcomeFeedCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        WelcomeFeedCardComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [TranslocoService],
    }).compileComponents();

    fixture = TestBed.createComponent(WelcomeFeedCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
