import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FeedNavigationSectionComponent } from './feed-navigation-section.component';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../../test/transloco-testing.module';

describe('FeedNavigationSectionComponent', () => {
  let component: FeedNavigationSectionComponent;
  let fixture: ComponentFixture<FeedNavigationSectionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        FeedNavigationSectionComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [],
    }).compileComponents();

    fixture = TestBed.createComponent(FeedNavigationSectionComponent);
    component = fixture.componentInstance;
    component.feedItems = [];
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
