import { Component, inject, Input } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { IonCol, IonRow } from '@ionic/angular/standalone';
import { NgComponentOutlet, NgFor, NgIf } from '@angular/common';
import {
  CustomButtonColorScheme,
  CustomButtonType,
  CustomHorizontalButtonComponent,
  FeedLoadingForYouSkeletonComponent,
} from '@sw-code/urbo-ui';
import { FeedCategory } from '../../../enums/feed-category';
import { FeedItem, ROUTES } from '@sw-code/urbo-core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-feed-navigation-section',
  templateUrl: './feed-navigation-section.component.html',
  styleUrls: ['./feed-navigation-section.component.scss'],
  imports: [
    TranslocoDirective,
    IonRow,
    NgFor,
    IonCol,
    NgComponentOutlet,
    NgIf,
    CustomHorizontalButtonComponent,
    FeedLoadingForYouSkeletonComponent,
  ],
})
export class FeedNavigationSectionComponent {
  @Input() feedCategory!: FeedCategory;
  @Input() feedItemsInitialData = true;
  @Input() titleKey = '';
  @Input() feedItems: FeedItem[] = [];
  @Input() moreFeedItemsAvailable = true;
  @Input() loadingFeedItems = false;
  protected readonly customButtonColorScheme = CustomButtonColorScheme;
  protected readonly customButtonType = CustomButtonType;
  protected readonly routes = ROUTES;
  private readonly router = inject(Router);

  async navigateToFeed(category: FeedCategory) {
    await this.router.navigateByUrl(this.getFeedUrl(category));
  }

  getFeedUrl(category: FeedCategory): string {
    return `${ROUTES.forYou}/feed/${category}`;
  }
}
