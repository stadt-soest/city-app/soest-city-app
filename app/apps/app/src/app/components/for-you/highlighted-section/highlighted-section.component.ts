import { Component, Input } from '@angular/core';
import { NgComponentOutlet, NgFor, NgIf } from '@angular/common';
import { IonCol, IonRow } from '@ionic/angular/standalone';
import { TranslocoDirective } from '@jsverse/transloco';
import { FeedItem } from '@sw-code/urbo-core';

@Component({
  selector: 'app-highlighted-section',
  templateUrl: './highlighted-section.component.html',
  styleUrls: ['./highlighted-section.component.scss'],
  imports: [NgIf, IonRow, TranslocoDirective, NgFor, IonCol, NgComponentOutlet],
})
export class HighlightedSectionComponent {
  @Input() highlightedFeedItems: FeedItem[] = [];
  @Input() loadingFeedItems = false;
}
