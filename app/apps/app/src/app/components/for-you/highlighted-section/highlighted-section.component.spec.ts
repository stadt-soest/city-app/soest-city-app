import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HighlightedSectionComponent } from './highlighted-section.component';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../../test/transloco-testing.module';

describe('HighlightedSectionComponent', () => {
  let component: HighlightedSectionComponent;
  let fixture: ComponentFixture<HighlightedSectionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HighlightedSectionComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [],
    }).compileComponents();

    fixture = TestBed.createComponent(HighlightedSectionComponent);
    component = fixture.componentInstance;
    component.highlightedFeedItems = [];
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
