import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { YourDaySectionComponent } from './your-day-section.component';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../../test/transloco-testing.module';

describe('FeedInfiniteSectionComponent', () => {
  let component: YourDaySectionComponent;
  let fixture: ComponentFixture<YourDaySectionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        YourDaySectionComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [],
    }).compileComponents();

    fixture = TestBed.createComponent(YourDaySectionComponent);
    component = fixture.componentInstance;
    component.activityFeedItems = [];
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
