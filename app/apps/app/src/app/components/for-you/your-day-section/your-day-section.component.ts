import { Component, Input } from '@angular/core';
import { NgComponentOutlet, NgFor, NgIf } from '@angular/common';
import { IonCol, IonRow } from '@ionic/angular/standalone';
import { TranslocoDirective } from '@jsverse/transloco';
import { FeedLoadingForYouSkeletonComponent } from '@sw-code/urbo-ui';
import { FeedItem } from '@sw-code/urbo-core';

@Component({
  selector: 'app-your-day-section',
  templateUrl: './your-day-section.component.html',
  styleUrls: ['./your-day-section.component.scss'],
  imports: [
    NgIf,
    IonRow,
    TranslocoDirective,
    NgFor,
    IonCol,
    NgComponentOutlet,
    FeedLoadingForYouSkeletonComponent,
  ],
})
export class YourDaySectionComponent {
  @Input() activityFeedItems: FeedItem[] = [];
  @Input() loadingFeedItems = false;
}
