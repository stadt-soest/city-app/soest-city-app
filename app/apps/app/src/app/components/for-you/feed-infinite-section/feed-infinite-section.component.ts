import { Component, Input } from '@angular/core';
import { IonCol, IonRow } from '@ionic/angular/standalone';
import { NgComponentOutlet, NgFor, NgIf } from '@angular/common';
import { FeedItem } from '@sw-code/urbo-core';

@Component({
  selector: 'app-feed-infinite-section',
  templateUrl: './feed-infinite-section.component.html',
  styleUrls: ['./feed-infinite-section.component.scss'],
  imports: [IonCol, NgIf, IonRow, NgFor, NgComponentOutlet],
})
export class FeedInfiniteSectionComponent {
  @Input() feedItemsInitialData = true;
  @Input() titleKey = '';
  @Input() subtitleKey?: string;
  @Input() feedItems: FeedItem[] = [];
  @Input() moreFeedItemsAvailable = true;
  @Input() loadingFeedItems = false;
}
