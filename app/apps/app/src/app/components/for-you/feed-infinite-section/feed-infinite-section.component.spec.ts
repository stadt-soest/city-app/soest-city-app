import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FeedInfiniteSectionComponent } from './feed-infinite-section.component';
import { CoreTestModule } from '@sw-code/urbo-core';
import { UITestModule } from '@sw-code/urbo-ui';
import { getTranslocoModule } from '../../../../test/transloco-testing.module';

describe('FeedInfiniteSectionComponent', () => {
  let component: FeedInfiniteSectionComponent;
  let fixture: ComponentFixture<FeedInfiniteSectionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        FeedInfiniteSectionComponent,
        CoreTestModule.forRoot(),
        UITestModule.forRoot(),
        getTranslocoModule(),
      ],
      providers: [],
    }).compileComponents();

    fixture = TestBed.createComponent(FeedInfiniteSectionComponent);
    component = fixture.componentInstance;
    component.feedItems = [];
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
