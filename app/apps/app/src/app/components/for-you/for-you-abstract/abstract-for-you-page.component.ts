import { Component, DestroyRef, inject, OnInit } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import {
  BehaviorSubject,
  combineLatest,
  firstValueFrom,
  map,
  Subscription,
  take,
} from 'rxjs';
import {
  ErrorHandlingService,
  FeedItem,
  FeedUpdateService,
  ForYouService,
} from '@sw-code/urbo-core';

@Component({
  template: '',
  standalone: false,
})
export abstract class AbstractForYouPageComponent implements OnInit {
  feedItemsNew: FeedItem[] = [];
  feedItemsOlder: FeedItem[] = [];
  activityFeedItems: FeedItem[] = [];
  feedItemsUpcomingEvents: FeedItem[] = [];
  highlightedFeedItems: FeedItem[] = [];
  priorityFeedItems: FeedItem[] = [];
  moreFeedItemsNewAvailable = true;
  moreFeedItemsUpcomingEventsAvailable = true;
  loadingNewFeedItems = true;
  loadingUpcomingEventsFeedItems = true;
  loadingOlderFeedItems = true;
  loadingUnfilteredFeedItems = false;
  hasFeedItems$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  displayConnectivityError = false;
  feedUpdateSubscription: Subscription = Subscription.EMPTY;
  feedItemsNewInitialData = true;
  feedItemsUpcomingEventsInitialData = true;
  protected destroyRef = inject(DestroyRef);
  protected forYouService = inject(ForYouService);
  protected feedUpdateService = inject(FeedUpdateService);
  protected errorHandlingService = inject(ErrorHandlingService);

  ngOnInit(): void {
    void this.subscribeToFeedItems();
    this.subscribeToContentAvailability();
    this.setupFeedUpdateSubscription();
    void this.subscribeToUpcomingEventsFeedItems();
  }

  async subscribeToFeedItems(): Promise<void> {
    const [
      feedItems,
      activityFeedItems,
      priorityFeedItems,
      highlightedFeedItems,
    ] = await Promise.all([
      firstValueFrom(this.forYouService.getFilteredFeedItems(1).pipe(take(1))),
      firstValueFrom(this.forYouService.getActivityFeedItems(0).pipe(take(1))),
      firstValueFrom(this.forYouService.getPriorityFeedItems().pipe(take(1))),
      firstValueFrom(
        this.forYouService.getHighlightedFeedItems().pipe(take(1)),
      ),
    ]);
    this.activityFeedItems = activityFeedItems;
    this.priorityFeedItems = priorityFeedItems;
    this.highlightedFeedItems = highlightedFeedItems;

    this.feedItemsNew = feedItems;
    this.feedItemsOlder = feedItems.slice(5);
    this.moreFeedItemsNewAvailable = feedItems.length > 0;
    this.feedItemsNewInitialData = this.moreFeedItemsNewAvailable;
    this.loadingNewFeedItems = false;
  }

  async subscribeToUpcomingEventsFeedItems(): Promise<void> {
    const feedItemsUpcomingEvents = await firstValueFrom(
      this.forYouService
        .getFilteredFeedItemsUpcoming(0)
        .pipe(takeUntilDestroyed(this.destroyRef)),
    );
    this.feedItemsUpcomingEvents = feedItemsUpcomingEvents;
    this.moreFeedItemsUpcomingEventsAvailable =
      feedItemsUpcomingEvents.length > 0;
    this.feedItemsUpcomingEventsInitialData =
      this.moreFeedItemsUpcomingEventsAvailable;
    this.loadingUpcomingEventsFeedItems = false;
  }

  subscribeToContentAvailability(): void {
    combineLatest([
      this.forYouService.hasContent(),
      this.forYouService.hasContentUpcoming(),
      this.forYouService.hasContentUnfiltered(),
    ])
      .pipe(
        takeUntilDestroyed(this.destroyRef),
        map(
          ([hasContent, hasContentUpcoming, hasContentUnfiltered]) =>
            hasContent || hasContentUpcoming || hasContentUnfiltered,
        ),
      )
      .subscribe((hasContent) => {
        this.hasFeedItems$.next(hasContent);
      });

    this.errorHandlingService
      .hasErrorInAllModules()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((hasErrorInAllModules) => {
        this.displayConnectivityError = hasErrorInAllModules;
      });
  }

  private setupFeedUpdateSubscription(): void {
    this.feedUpdateSubscription = this.feedUpdateService
      .getFeedUpdatedListener()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(() => {
        void this.subscribeToFeedItems();
        void this.subscribeToUpcomingEventsFeedItems();
      });
  }
}
