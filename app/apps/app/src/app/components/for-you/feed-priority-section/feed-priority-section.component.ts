import { Component, Input, ViewChild } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { NgComponentOutlet, NgFor, NgIf } from '@angular/common';
import { IonCol } from '@ionic/angular/standalone';
import {
  ConnectivityErrorCardComponent,
  InstallAsAppCardComponent,
  UpdateNotifierComponent,
  WentWrongErrorCardComponent,
} from '@sw-code/urbo-ui';
import { WelcomeFeedCardComponent } from '../welcome-feed-card/welcome-feed-card.component';
import { FeedItem } from '@sw-code/urbo-core';

@Component({
  selector: 'app-priority-section',
  templateUrl: './feed-priority-section.component.html',
  imports: [
    ConnectivityErrorCardComponent,
    WentWrongErrorCardComponent,
    NgIf,
    NgFor,
    IonCol,
    NgComponentOutlet,
    UpdateNotifierComponent,
    InstallAsAppCardComponent,
    WelcomeFeedCardComponent,
  ],
})
export class FeedPrioritySectionComponent {
  @Input() priorityFeedItems: FeedItem[] = [];
  @Input() displayConnectivityError = false;
  @Input() hasFeedItems$!: BehaviorSubject<boolean>;
  @ViewChild(WelcomeFeedCardComponent)
  welcomeFeedCard!: WelcomeFeedCardComponent;

  updateWelcomeCard() {
    this.welcomeFeedCard?.updateLastUpdatedDate();
  }
}
