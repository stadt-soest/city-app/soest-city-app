import { Component, inject } from '@angular/core';
import { IonApp, IonRouterOutlet } from '@ionic/angular/standalone';
import { AppInitializationService } from '@sw-code/urbo-core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  imports: [IonApp, IonRouterOutlet],
})
export class AppComponent {
  private readonly appInitializationService = inject(AppInitializationService);

  constructor() {
    this.appInitializationService.initializeApp();
  }
}
