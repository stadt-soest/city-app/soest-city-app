import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, ROUTES } from '@angular/router';
import { RouteBuilderService } from './route-builder.service';

@NgModule({
  imports: [
    RouterModule.forRoot([], { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
  providers: [
    {
      provide: ROUTES,
      useFactory: (routeBuilder: RouteBuilderService) =>
        routeBuilder.getRoutes(),
      deps: [RouteBuilderService],
      multi: true,
    },
  ],
})
export class AppRoutingModule {}
