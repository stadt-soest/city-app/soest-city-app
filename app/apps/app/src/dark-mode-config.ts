export const darkModeMediaQuery = window.matchMedia(
  '(prefers-color-scheme: dark)',
);

export const isDarkMode = (): boolean => darkModeMediaQuery.matches;
