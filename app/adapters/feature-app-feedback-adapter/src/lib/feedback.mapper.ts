import { TranslationService } from '@sw-code/urbo-core';
import { inject, Injectable } from '@angular/core';
import {
  AccessibilityRequestDto,
  FeedbackFormDto,
  FeedbackOptionDto,
  FeedbackQuestionDto,
  FeedbackSubmissionRequestDto,
  LocalizationDto,
} from '@sw-code/urbo-backend-api';
import {
  AccessibilityRequest,
  FeedbackForm,
  FeedbackOption,
  FeedbackQuestion,
  FeedbackSubmission,
} from '@sw-code/urbo-feature-app-feedback';

@Injectable({ providedIn: 'root' })
export class FeedbackMapper {
  private readonly translationService = inject(TranslationService);

  mapToModel(dto: FeedbackFormDto): FeedbackForm {
    return {
      id: dto.id,
      version: dto.version,
      questions: dto.questions
        ? dto.questions.map((questionDto) =>
            this.mapQuestionToModel(questionDto),
          )
        : [],
    };
  }

  mapFeedbackSubmissionToDto(
    feedbackSubmission: FeedbackSubmission,
  ): FeedbackSubmissionRequestDto {
    return {
      version: feedbackSubmission.version,
      deduplicationKey: feedbackSubmission.deduplicationKey,
      environment: feedbackSubmission.environment,
      answers: feedbackSubmission.answers.map((answer) => ({
        questionId: answer.questionId,
        selectedOptionId: answer.selectedOptionId,
        freeText: answer.freeText,
      })),
    };
  }

  mapToAccessibilityRequestDto(
    accessibilityRequest: AccessibilityRequest,
  ): AccessibilityRequestDto {
    return {
      answer: accessibilityRequest.answer,
      environment: {
        deviceModelName:
          accessibilityRequest.environment?.deviceModelName ?? '',
        operatingSystemName:
          accessibilityRequest.environment?.deviceModelName ?? '',
        operatingSystemVersion:
          accessibilityRequest.environment?.deviceModelName ?? '',
        appVersion: accessibilityRequest.environment?.deviceModelName ?? '',
        browserName: accessibilityRequest.environment?.deviceModelName ?? '',
        usedAs: accessibilityRequest.environment?.deviceModelName ?? '',
        appSettings: accessibilityRequest.environment?.appSettings ?? '',
      },
      imageFilenames: accessibilityRequest.imageFilenames,
    };
  }

  private mapQuestionToModel(dto: FeedbackQuestionDto): FeedbackQuestion {
    return {
      id: dto.id,
      questionType: dto.questionType,
      localizedQuestionName: this.findLocalizationForCurrentLocale(
        dto.questionName || [],
      ),
      options: dto.options
        ? dto.options.map((optionDto) => this.mapOptionToModel(optionDto))
        : [],
    };
  }

  private mapOptionToModel(dto: FeedbackOptionDto): FeedbackOption {
    return {
      id: dto.id,
      localizedText: this.findLocalizationForCurrentLocale(
        dto.localizedTexts || [],
      ),
    };
  }

  private findLocalizationForCurrentLocale(
    translations: LocalizationDto[],
  ): string {
    let maybeTranslation = translations.find(
      (translation) =>
        translation.language === this.translationService.getCurrentLocale(),
    );

    maybeTranslation ??= translations[0];
    return maybeTranslation?.value ?? '';
  }
}
