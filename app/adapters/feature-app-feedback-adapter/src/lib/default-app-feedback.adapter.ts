import { inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { FeedbackMapper } from './feedback.mapper';
import {
  AccessibilityFeedbackApiClient,
  FeedbackApiClient,
} from '@sw-code/urbo-backend-api';
import {
  AccessibilityRequest,
  AppFeedbackAdapter,
  FeedbackForm,
  FeedbackSubmission,
} from '@sw-code/urbo-feature-app-feedback';

@Injectable()
export class DefaultFeedbackAdapter implements AppFeedbackAdapter {
  private readonly feedbackApiClient = inject(FeedbackApiClient);
  private readonly accessibilityFeedbackApiClient = inject(
    AccessibilityFeedbackApiClient,
  );
  private readonly feedbackMapper = inject(FeedbackMapper);

  getFeedbackForm(version: number): Observable<FeedbackForm> {
    return this.feedbackApiClient.v1FeedbackFormGet(version).pipe(
      take(1),
      map((response) => this.feedbackMapper.mapToModel(response)),
    );
  }

  postFeedbackSubmission(
    feedbackSubmission: FeedbackSubmission,
  ): Observable<any> {
    const feedbackSubmissionDto =
      this.feedbackMapper.mapFeedbackSubmissionToDto(feedbackSubmission);
    return this.feedbackApiClient
      .v1FeedbackSubmissionsPost(feedbackSubmissionDto)
      .pipe(take(1));
  }

  postAccessibilityFeedbackSubmission(
    accessibilityRequest: AccessibilityRequest,
  ): Observable<any> {
    const feedbackFormDto =
      this.feedbackMapper.mapToAccessibilityRequestDto(accessibilityRequest);
    return this.accessibilityFeedbackApiClient
      .v1AccessibilityFeedbackSubmissionsPost(feedbackFormDto)
      .pipe(take(1));
  }
}
