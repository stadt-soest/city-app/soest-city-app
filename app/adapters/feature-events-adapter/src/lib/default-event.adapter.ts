import { inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import {
  CategoryApiClient,
  CategoryType,
  EventApiClient,
  EventFeedApiClient,
} from '@sw-code/urbo-backend-api';
import { GroupedFeedEventsApiClient as EventSearchApiClient } from '@sw-code/urbo-search-api';
import { EventMapper } from './mapper/event.mapper';
import { EventFeedMapper } from './mapper/event-feed.mapper';
import {
  Event,
  EventAdapter,
  EventCategory,
  EventFeedItem,
  SortOptions,
} from '@sw-code/urbo-feature-events';

@Injectable()
export class DefaultEventAdapter implements EventAdapter {
  private readonly eventApiClient = inject(EventApiClient);
  private readonly eventFeedApiClient = inject(EventFeedApiClient);
  private readonly eventSearchApiClient = inject(EventSearchApiClient);
  private readonly eventMapper = inject(EventMapper);
  private readonly eventFeedMapper = inject(EventFeedMapper);
  private readonly categoryClient = inject(CategoryApiClient);

  getByOccurrenceId(id: string): Observable<Event> {
    return this.eventApiClient.v1EventOccurrencesIdEventGet(id).pipe(
      take(1),
      map((response) => this.eventMapper.mapToModel(response, id)),
    );
  }

  getFeed(selectedCategories: string[]): Observable<EventFeedItem[]> {
    return this.eventFeedApiClient
      .v1GroupedFeedEventsGet(
        0,
        10,
        [SortOptions.startAtAsc],
        undefined,
        selectedCategories,
      )
      .pipe(
        take(1),
        map((response) =>
          this.eventFeedMapper.mapToModelArray(response.content ?? []),
        ),
      );
  }

  getFeedablesUpcoming(
    page: number,
    categories: string[],
    bookmarkedEventIds: string[],
    excludedEvents: string[],
    startDateIso: string,
    endDateIso?: string,
    size = 10,
  ): Observable<{ content: EventFeedItem[]; totalRecords: number }> {
    return this.eventFeedApiClient
      .v1FeedEventsGet(
        page,
        size,
        [SortOptions.startAtAsc],
        categories,
        bookmarkedEventIds,
        undefined,
        excludedEvents,
        undefined,
        startDateIso,
        endDateIso,
      )
      .pipe(
        take(1),
        map((response) => ({
          content: this.eventFeedMapper.mapToModelArray(response.content ?? []),
          totalRecords: response.page?.totalElements ?? 0,
        })),
      );
  }

  getFeedablesUnfiltered(
    page: number,
    startDateIso: string,
  ): Observable<{ content: EventFeedItem[]; totalRecords: number }> {
    return this.eventFeedApiClient
      .v1FeedEventsGet(
        page,
        10,
        [SortOptions.startAtAsc],
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        startDateIso,
      )
      .pipe(
        take(1),
        map((response) => ({
          content: this.eventFeedMapper.mapToModelArray(response.content ?? []),
          totalRecords: response.page?.totalElements ?? 0,
        })),
      );
  }

  getFeedablesGrouped(
    page: number,
    sortOption: SortOptions,
    selectedCategories: string[],
    excludedEvents: string[],
    startDateIso: string,
  ): Observable<{ content: EventFeedItem[]; totalRecords: number }> {
    return this.eventFeedApiClient
      .v1GroupedFeedEventsGet(
        page,
        10,
        [sortOption],
        undefined,
        selectedCategories,
        undefined,
        undefined,
        excludedEvents,
        undefined,
        startDateIso,
      )
      .pipe(
        take(1),
        map((response) => ({
          content: this.eventFeedMapper.mapToModelArray(response.content ?? []),
          totalRecords: response.page?.totalElements ?? 0,
        })),
      );
  }

  getBySearchTerm(
    searchTerm: string,
    pageNumber: number,
    pageSize: number,
  ): Observable<{ total: number; items: EventFeedItem[] }> {
    return this.eventSearchApiClient
      .indexesGroupedFeedEventsSearchPost({
        page: pageNumber,
        hitsPerPage: pageSize,
        sort: ['startAt:asc'],
        q: searchTerm,
      })
      .pipe(
        take(1),
        map((response) => ({
          items: this.eventFeedMapper.mapToModelArray(response.hits ?? []),
          total: response.totalHits ?? 0,
        })),
      );
  }

  getCategories(): Observable<EventCategory[]> {
    return this.categoryClient.v1CategoriesGet(CategoryType.Events).pipe(
      take(1),
      map((response) => this.eventMapper.mapToCategories(response)),
    );
  }
}
