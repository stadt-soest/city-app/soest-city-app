import {
  CategoryResponseDto,
  EventResponseDto,
  EventStatus,
  OccurrenceDto,
} from '@sw-code/urbo-backend-api';
import { inject, Injectable } from '@angular/core';
import {
  Event,
  EventCategory,
  LocalEventStatus,
  Occurrence,
  TextFormattingPipe,
} from '@sw-code/urbo-feature-events';
import { DomPurifyConfigType } from '@sw-code/urbo-core';
import { ImageMapper } from './image.mapper';

@Injectable({ providedIn: 'root' })
export class EventMapper {
  private readonly contentFormatter = inject(TextFormattingPipe);

  mapToModel(dto: EventResponseDto, selectOccurenceId: string): Event {
    return new Event({
      id: dto.id,
      selectedOccurrenceId: selectOccurenceId,
      title: this.contentFormatter.transform(
        dto.title ?? '',
        DomPurifyConfigType.title,
      ),
      description: this.contentFormatter.transform(
        dto.description ?? '',
        DomPurifyConfigType.description,
      ),
      image: ImageMapper.mapImage(dto.image),
      occurrences: dto.occurrences.map(
        (occurrenceDto: OccurrenceDto): Occurrence => ({
          id: occurrenceDto.id,
          status: this.mapOpenApiCoreEventStatusToEventStatus(
            occurrenceDto.status,
          ),
          source: occurrenceDto.source,
          dateCreated: occurrenceDto.dateCreated,
          start: new Date(occurrenceDto.start),
          end: new Date(occurrenceDto.end),
        }),
      ),
      categoryIds:
        dto.categories
          ?.map((category) => category.id)
          .filter((id): id is string => Boolean(id)) ?? [],
      dateModified: dto.occurrences[0].dateCreated
        ? new Date(dto.occurrences[0].dateCreated)
        : undefined,
      location: dto.location
        ? {
            locationName: dto.location.locationName ?? undefined,
            city: dto.location.city ?? undefined,
            postalCode: dto.location.postalCode ?? undefined,
            streetAddress: dto.location.address ?? undefined,
          }
        : undefined,
      coordinates:
        dto.location?.coordinates?.latitude !== undefined &&
        dto.location.coordinates.longitude !== undefined
          ? {
              lat: dto.location.coordinates.latitude,
              long: dto.location.coordinates.longitude,
            }
          : undefined,
      contactPoint: dto.contactPoint
        ? {
            telephone: dto.contactPoint.telephone ?? undefined,
            email: dto.contactPoint.email ?? undefined,
          }
        : undefined,
      subTitle: this.contentFormatter.transform(
        dto.subTitle ?? '',
        DomPurifyConfigType.title,
      ),
    });
  }

  public mapToCategories(
    categoryResponseArray: Array<CategoryResponseDto>,
  ): Array<EventCategory> {
    return categoryResponseArray.map((category) => ({
      id: category.id,
      sourceCategories: category.sourceCategories,
      localizedNames: category.localizedNames.map((localizedName) => ({
        value: localizedName.value,
        language: localizedName.language,
      })),
    }));
  }

  mapOpenApiCoreEventStatusToEventStatus(
    status: EventStatus,
  ): LocalEventStatus {
    switch (status) {
      case 'RESCHEDULED':
      case 'POSTPONED':
        return LocalEventStatus.updated;
      case 'CANCELLED':
        return LocalEventStatus.cancelled;
      default:
        return LocalEventStatus.scheduled;
    }
  }
}
