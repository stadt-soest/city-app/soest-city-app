import {
  AbstractMapper,
  FeedItemLayoutType,
  ModuleInfo,
} from '@sw-code/urbo-core';
import { inject, Injectable } from '@angular/core';
import { EventFeedResponseDto } from '@sw-code/urbo-backend-api';
import {
  EventFeedCardComponent,
  EventFeedItem,
  EventForYouCardComponent,
  EVENTS_MODULE_INFO,
} from '@sw-code/urbo-feature-events';
import { EventMapper } from './event.mapper';
import { ImageMapper } from './image.mapper';

@Injectable({ providedIn: 'root' })
export class EventFeedMapper extends AbstractMapper<
  EventFeedResponseDto,
  EventFeedItem
> {
  private readonly eventsModuleInfo = inject<ModuleInfo>(EVENTS_MODULE_INFO);
  private readonly eventMapper = inject(EventMapper);

  mapToModel(dto: EventFeedResponseDto): EventFeedItem {
    if (!dto.id || !dto.title) {
      throw new Error(`Required properties are missing in DTO: ${dto.id}`);
    }

    return {
      id: dto.eventOccurrenceId,
      moduleId: this.eventsModuleInfo.id,
      moduleName: 'feature_events.module_name_singular',
      pluralModuleName: 'feature_events.module_name',
      baseRoutePath: this.eventsModuleInfo.baseRoutingPath,
      icon: this.eventsModuleInfo.icon,
      title: dto.title,
      image: ImageMapper.mapImage(dto.image),
      creationDate: new Date(dto.dateCreated),
      startDate: new Date(dto.startAt),
      cardComponent: EventFeedCardComponent,
      forYouComponent: EventForYouCardComponent,
      relevancy: this.eventsModuleInfo.relevancy,
      layoutType: FeedItemLayoutType.card,
      status: this.eventMapper.mapOpenApiCoreEventStatusToEventStatus(
        dto.status,
      ),
    };
  }
}
