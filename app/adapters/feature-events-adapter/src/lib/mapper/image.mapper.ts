import { ImageDto } from '@sw-code/urbo-search-api';
import { ImageFile } from '@sw-code/urbo-core';

export class ImageMapper {
  static mapImage(image?: ImageDto | null): {
    description: string;
    imageSource: string;
    thumbnail: { filename: string; width: number; height: number };
    highRes: { filename: string; width: number; height: number };
    copyrightHolder?: string;
  } | null {
    if (!image) {
      return null;
    }

    const mapThumbnailOrHighRes = (imagePart?: ImageFile | null) => ({
      filename: imagePart?.filename ?? '',
      width: imagePart?.width ?? 0,
      height: imagePart?.height ?? 0,
    });

    return {
      description: image.description ?? '',
      imageSource: image.imageSource ?? '',
      copyrightHolder: image.copyrightHolder ?? undefined,
      thumbnail: mapThumbnailOrHighRes(image.thumbnail),
      highRes: mapThumbnailOrHighRes(image.highRes),
    };
  }
}
