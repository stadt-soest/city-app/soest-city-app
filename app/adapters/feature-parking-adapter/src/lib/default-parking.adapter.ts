import { inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import {
  ParkingApiClient,
  V1ParkingAreasGet200Response,
} from '@sw-code/urbo-backend-api';
import { ParkingMapper } from './parking.mapper';
import {
  Parking,
  ParkingAdapter,
  ParkingFeedItem,
  ParkingWithDistance,
} from '@sw-code/urbo-feature-parking';
import { MapIntegration, ModuleInfo } from '@sw-code/urbo-core';

@Injectable()
export class DefaultParkingAdapter implements ParkingAdapter {
  private readonly parkingApiClient = inject(ParkingApiClient);
  private readonly parkingMapper = inject(ParkingMapper);

  getParkingAreas(): Observable<Parking[]> {
    return this.parkingApiClient.v1ParkingAreasGet(0, 2000).pipe(
      take(1),
      map((response: V1ParkingAreasGet200Response) =>
        this.parkingMapper.mapToParking(response.content ?? []),
      ),
    );
  }

  getFavoriteParkingFeedables(
    favoriteIds: Set<string>,
  ): Observable<ParkingFeedItem[]> {
    return this.getParkingAreas().pipe(
      map((parkingAreas) => {
        const favoriteParkingAreas = parkingAreas
          .filter((area) => favoriteIds.has(area.id))
          .map((area) => new ParkingWithDistance(area));
        return favoriteParkingAreas.map(
          this.parkingMapper.parkingToFeedItem.bind(this.parkingMapper),
        );
      }),
    );
  }

  getParkingAreaById(id: string): Observable<Parking> {
    return this.parkingApiClient.v1ParkingAreasIdGet(id).pipe(
      take(1),
      map((response) =>
        this.parkingMapper.transformParkingAreaResponseToParking(response),
      ),
    );
  }

  getParkingPois(moduleInfo: ModuleInfo): Observable<MapIntegration> {
    return this.parkingApiClient.v1ParkingAreasGet(0, 2000).pipe(
      take(1),
      map((response: V1ParkingAreasGet200Response) => {
        const parkingAreas = this.parkingMapper.mapToParking(
          response.content ?? [],
        );
        return {
          category: Parking.createCategory(moduleInfo),
          poi: parkingAreas.map((parking) =>
            Parking.createPoi(parking, moduleInfo),
          ),
        };
      }),
    );
  }
}
