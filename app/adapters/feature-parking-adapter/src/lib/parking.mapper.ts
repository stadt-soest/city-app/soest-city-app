import { inject, Injectable } from '@angular/core';
import {
  DayOfWeek,
  FeedItemLayoutType,
  ModuleInfo,
  OpeningHoursMap,
  SpecialOpeningHoursMap,
} from '@sw-code/urbo-core';
import {
  Parking,
  PARKING_MODULE_INFO,
  ParkingCardForYouComponent,
  ParkingFeedItem,
  ParkingStatus,
  ParkingWithDistance,
} from '@sw-code/urbo-feature-parking';
import {
  ParkingAreaResponseDto,
  ParkingOpeningHours,
  ParkingSpecialOpeningHoursResponse,
} from '@sw-code/urbo-backend-api';

@Injectable({ providedIn: 'root' })
export class ParkingMapper {
  private readonly parkingModuleInfo = inject<ModuleInfo>(PARKING_MODULE_INFO);
  private readonly statusMap = {
    OPEN: ParkingStatus.OPEN,
    CLOSED: ParkingStatus.CLOSED,
    DISTURBANCE: ParkingStatus.UNKNOWN,
  };

  mapToParking(parkingAreas: ParkingAreaResponseDto[]): Parking[] {
    return parkingAreas.map((area) =>
      this.transformParkingAreaResponseToParking(area),
    );
  }

  transformParkingAreaResponseToParking(area: ParkingAreaResponseDto): Parking {
    const {
      id,
      name,
      status,
      location,
      capacity,
      openingTimes,
      specialOpeningTimes,
      displayName,
      parkingType,
      priceDescription,
      description,
      maximumAllowedHeight,
    } = area;

    return new Parking({
      id,
      name,
      status: this.statusMap[status],
      coordinates: {
        lat: location.coordinates.latitude,
        long: location.coordinates.longitude,
      },
      openingHours: this.mapOpeningHours(openingTimes),
      specialOpeningHours: specialOpeningTimes
        ? this.mapSpecialOpeningHours(specialOpeningTimes)
        : undefined,
      parkingCapacityRemaining: capacity.remaining,
      address: {
        street: location.address.street,
        postalCode: location.address.postalCode,
        city: location.address.city,
      },
      maximumAllowedHeight: maximumAllowedHeight ?? null,
      priceDescription,
      description,
      displayName,
      parkingType,
    });
  }

  parkingToFeedItem(parking: ParkingWithDistance): ParkingFeedItem {
    return {
      id: parking.area.id,
      moduleId: this.parkingModuleInfo.id,
      moduleName: 'feature_parking.module_name',
      pluralModuleName: 'feature_parking.module_name',
      baseRoutePath: this.parkingModuleInfo.baseRoutingPath,
      icon: this.parkingModuleInfo.icon,
      title: parking.area.name,
      image: null,
      creationDate: new Date(),
      cardComponent: ParkingCardForYouComponent,
      forYouComponent: ParkingCardForYouComponent,
      relevancy: this.parkingModuleInfo.relevancy,
      parkingWithDistance: parking,
      layoutType: FeedItemLayoutType.card,
    };
  }

  private mapOpeningHours(openingTimes: {
    [key: string]: ParkingOpeningHours;
  }): OpeningHoursMap {
    return Object.values(DayOfWeek).reduce((acc, day) => {
      const hours = openingTimes[day];
      acc[day] = { from: hours?.from, to: hours?.to };
      return acc;
    }, {} as OpeningHoursMap);
  }

  private mapSpecialOpeningHours(
    specialOpeningTimes: ParkingSpecialOpeningHoursResponse[],
  ): SpecialOpeningHoursMap {
    const mappedSpecialOpeningHours: SpecialOpeningHoursMap = {};

    specialOpeningTimes.forEach(({ date, from, to, descriptions }) => {
      mappedSpecialOpeningHours[date] = {
        from: from ?? '',
        to: to ?? '',
        descriptions: descriptions ?? [],
      };
    });

    return mappedSpecialOpeningHours;
  }
}
