import {
  CITIZEN_SERVICES_MODULE_INFO,
  CitizenService,
  CitizenServiceForYouComponent,
  Tag,
} from '@sw-code/urbo-feature-citizen-services';
import { inject, Injectable } from '@angular/core';
import { FeedItem, FeedItemLayoutType, ModuleInfo } from '@sw-code/urbo-core';
import { CitizenServiceResponseDto, TagDto } from '@sw-code/urbo-backend-api';

@Injectable({ providedIn: 'root' })
export class CitizenServiceMapper {
  private readonly citizenServicesModuleInfo = inject<ModuleInfo>(
    CITIZEN_SERVICES_MODULE_INFO,
  );

  mapToModels(dtos: CitizenServiceResponseDto[]): CitizenService[] {
    return dtos.map((dto) => this.mapToModel(dto));
  }

  mapToTags(dtos: TagDto[]): Tag[] {
    return dtos.map((dto: TagDto) => this.mapToTag(dto));
  }

  mapToTag(dto: TagDto): Tag {
    if (!dto.id || !dto.localizedNames) {
      throw new Error(`Required properties are missing in DTO: ${dto.id}`);
    }

    return {
      id: dto.id,
      localizedNames: dto.localizedNames ?? [],
    };
  }

  mapToModel(dto: CitizenServiceResponseDto): CitizenService {
    if (!dto.id || !dto.url || !dto.title) {
      throw new Error(`Required properties are missing in DTO: ${dto.id}`);
    }

    return {
      id: dto.id,
      title: dto.title,
      description: dto.description ?? '',
      url: dto.url,
    };
  }

  mapToFeedItemArray(dtos: CitizenServiceResponseDto[]): FeedItem[] {
    return dtos.map((dto) => this.mapToFeedItem(dto));
  }

  mapToFeedItem(dto: CitizenServiceResponseDto): FeedItem {
    if (!dto.id || !dto.url || !dto.title) {
      throw new Error(`Required properties are missing in DTO: ${dto.id}`);
    }

    return {
      baseRoutePath: this.citizenServicesModuleInfo.baseRoutingPath,
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      cardComponent: undefined!,
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      creationDate: undefined!,
      forYouComponent: CitizenServiceForYouComponent,
      icon: this.citizenServicesModuleInfo.icon,
      moduleId: this.citizenServicesModuleInfo.id,
      moduleName: 'feature_citizen_services.module_name_singular',
      pluralModuleName: 'feature_citizen_services.module_name',
      id: dto.id,
      title: dto.title,
      relevancy: this.citizenServicesModuleInfo.relevancy,
      layoutType: FeedItemLayoutType.item,
    };
  }
}
