import { inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import {
  CitizenService,
  CitizenServiceAdapter,
  CitizenServiceFeedItem,
  Tag,
} from '@sw-code/urbo-feature-citizen-services';
import {
  CitizenServiceDto,
  CitizenServicesApiClient,
  TagDto,
} from '@sw-code/urbo-search-api';
import {
  CitizenServiceApiClient,
  CitizenServiceResponseDto,
  V1CitizenServicesGet200Response,
} from '@sw-code/urbo-backend-api';
import { CitizenServiceMapper } from './citizen-service.mapper';

@Injectable()
export class DefaultCitizenServiceAdapter implements CitizenServiceAdapter {
  private readonly citizenServiceApiClient = inject(CitizenServiceApiClient);
  private readonly citizenServiceSearchApiClient = inject(
    CitizenServicesApiClient,
  );
  private readonly citizenServiceMapper = inject(CitizenServiceMapper);

  getCitizenServices(
    tagId?: string,
    favorite?: boolean,
  ): Observable<CitizenService[]> {
    return this.citizenServiceApiClient
      .v1CitizenServicesGet(tagId, undefined, undefined, favorite)
      .pipe(
        take(1),
        map((response: V1CitizenServicesGet200Response) =>
          this.citizenServiceMapper.mapToModels(response.content ?? []),
        ),
      );
  }

  getTags(): Observable<Tag[]> {
    return this.citizenServiceApiClient.v1CitizenServicesTagsGet().pipe(
      take(1),
      map((response: TagDto[]) =>
        this.citizenServiceMapper.mapToTags(response ?? []),
      ),
    );
  }

  getTagById(id: string): Observable<Tag> {
    return this.citizenServiceApiClient.v1CitizenServicesTagsIdGet(id).pipe(
      take(1),
      map((response: TagDto) => this.citizenServiceMapper.mapToTag(response)),
    );
  }

  getById(id: string): Observable<CitizenService> {
    return this.citizenServiceApiClient.v1CitizenServicesIdGet(id).pipe(
      take(1),
      map((response) => this.citizenServiceMapper.mapToModel(response)),
    );
  }

  getBySearchTerm(
    searchTerm: string,
    pageNumber: number,
    pageSize: number,
  ): Observable<{ total: number; items: CitizenServiceFeedItem[] }> {
    return this.citizenServiceSearchApiClient
      .indexesCitizenServicesSearchPost({
        q: searchTerm,
        page: pageNumber,
        hitsPerPage: pageSize,
      })
      .pipe(
        take(1),
        map((response) => {
          const normalizedHits = (response.hits ?? []).map(
            (hit: CitizenServiceDto) => ({
              ...hit,
              editable: false,
            }),
          ) as CitizenServiceResponseDto[];

          const items =
            this.citizenServiceMapper.mapToFeedItemArray(normalizedHits);
          const total = response.totalHits ?? 0;

          return { items, total };
        }),
      );
  }
}
