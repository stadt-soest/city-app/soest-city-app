export const sanitizeValue = (value?: string): string =>
  value?.replace(/['"\\]/g, '\\$&') ?? '';

export const buildTypeFilter = (type?: string): string | undefined =>
  type?.trim() ? `type = '${sanitizeValue(type)}'` : undefined;

export const buildSourcesFilter = (sources?: string[]): string | undefined =>
  sources?.length
    ? sources
        .filter((source) => source?.trim())
        .map((source) => `source = '${sanitizeValue(source)}'`)
        .join(' OR ')
    : undefined;

export const buildCategoryIdsFilter = (
  categoryIds?: string[],
): string | undefined =>
  categoryIds?.length
    ? categoryIds
        .filter((id) => id?.trim())
        .map((id) => `categoryIds = '${sanitizeValue(id)}'`)
        .join(' OR ')
    : undefined;

export const combineFilters = (
  filters: Array<string | undefined>,
): string | undefined => {
  const validFilters = filters.filter((filter) => !!filter) as string[];
  return validFilters.length > 0 ? validFilters.join(' AND ') : undefined;
};
