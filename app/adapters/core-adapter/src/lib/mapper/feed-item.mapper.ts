import {
  EventFeed,
  IndexesFeedSearchPost200ResponseAllOfHitsInner,
} from '@sw-code/urbo-search-api';
import { FeedItem, FeedItemLayoutType, ModuleInfo } from '@sw-code/urbo-core';
import {
  NewsFeedCardComponent,
  NewsFeedItem,
  NewsForYouCardComponent,
} from '@sw-code/urbo-feature-news';
import {
  EventFeedCardComponent,
  EventFeedItem,
  EventForYouCardComponent,
  LocalEventStatus,
} from '@sw-code/urbo-feature-events';
import { EventStatus } from '@sw-code/urbo-backend-api';

export class FeedItemMapper {
  static mapFeedItems(
    hits: IndexesFeedSearchPost200ResponseAllOfHitsInner[],
    newsModuleInfo: ModuleInfo,
    eventModuleInfo: ModuleInfo,
  ): FeedItem[] {
    return hits.flatMap(
      (hit: IndexesFeedSearchPost200ResponseAllOfHitsInner) => {
        switch (hit.type) {
          case 'EVENTS':
            return FeedItemMapper.fromEventFeed([hit], eventModuleInfo);
          case 'NEWS':
            return FeedItemMapper.fromNewsFeed([hit], newsModuleInfo);
          default:
            console.warn(`Unhandled feed item type: ${(hit as any).type}`);
            return [];
        }
      },
    );
  }

  static fromNewsFeed(
    data: IndexesFeedSearchPost200ResponseAllOfHitsInner[],
    moduleInfo: ModuleInfo,
  ): NewsFeedItem[] {
    return data.map(
      (newsItem) =>
        new NewsFeedItem({
          id: newsItem.id,
          moduleId: moduleInfo.id,
          moduleName: moduleInfo.name,
          pluralModuleName: moduleInfo.name,
          baseRoutePath: moduleInfo.baseRoutingPath,
          icon: moduleInfo.icon,
          title: newsItem.title,
          image: newsItem.image
            ? {
                imageSource: newsItem.image.imageSource,
                thumbnail: newsItem.image.thumbnail,
                highRes: newsItem.image.highRes,
                description: newsItem.image.description,
              }
            : null,
          creationDate: new Date(newsItem.publishedDate),
          cardComponent: NewsFeedCardComponent,
          forYouComponent: NewsForYouCardComponent,
          relevancy: moduleInfo.relevancy,
          layoutType: FeedItemLayoutType.card,
        }),
    );
  }

  static fromEventFeed(
    data: IndexesFeedSearchPost200ResponseAllOfHitsInner[],
    moduleInfo: ModuleInfo,
  ): EventFeedItem[] {
    return data.filter(FeedItemMapper.isEventFeed).map(
      (event) =>
        new EventFeedItem({
          id: 'eventOccurrenceId' in event ? event.eventOccurrenceId : '',
          moduleId: moduleInfo.id,
          moduleName: 'feature_events.module_name_singular',
          pluralModuleName: 'feature_events.module_name',
          baseRoutePath: moduleInfo.baseRoutingPath,
          icon: moduleInfo.icon,
          title: event.title,
          image: event.image
            ? {
                imageSource: event.image.imageSource,
                thumbnail: event.image.thumbnail,
                highRes: event.image.highRes,
                description: event.image.description,
              }
            : null,
          creationDate: new Date(event.publishedDate),
          startDate: new Date('startAt' in event ? event.startAt : ''),
          cardComponent: EventFeedCardComponent,
          forYouComponent: EventForYouCardComponent,
          relevancy: moduleInfo.relevancy,
          layoutType: FeedItemLayoutType.card,
          status: FeedItemMapper.mapOpenApiSearchEventStatusToEventStatus(
            event.status,
          ),
        }),
    );
  }

  static isEventFeed(
    item: IndexesFeedSearchPost200ResponseAllOfHitsInner,
  ): item is EventFeed {
    return item.type === 'EVENTS';
  }

  static mapOpenApiSearchEventStatusToEventStatus(
    status: EventStatus,
  ): LocalEventStatus {
    switch (status) {
      case 'RESCHEDULED':
      case 'POSTPONED':
        return LocalEventStatus.updated;
      case 'CANCELLED':
        return LocalEventStatus.cancelled;
      default:
        return LocalEventStatus.scheduled;
    }
  }
}
