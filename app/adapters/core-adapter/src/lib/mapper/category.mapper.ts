import { CategoryResponseDto } from '@sw-code/urbo-backend-api';
import { Category, GroupCategory } from '@sw-code/urbo-core';

export const mapCategory = (dto: CategoryResponseDto): Category =>
  new Category({
    id: dto.id,
    sourceCategories: dto.sourceCategories,
    icon: dto.icon?.key,
    localizedNames: dto.localizedNames,
    moduleInfo: undefined,
    source: 'API',
    groupCategory: dto.categoryGroup
      ? mapGroupCategory(dto.categoryGroup)
      : undefined,
  });

export const mapGroupCategory = (dto: CategoryResponseDto): GroupCategory =>
  new GroupCategory(dto.id, dto.localizedNames);

export const mapCategoryArray = (dtos: CategoryResponseDto[]): Category[] =>
  dtos.map(mapCategory);
