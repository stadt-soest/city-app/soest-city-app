import { inject, Injectable } from '@angular/core';
import {
  Category,
  CORE_MODULE_CONFIG,
  CoreAdapter,
  FeedFilterCriteria,
  FeedItem,
  Poi,
} from '@sw-code/urbo-core';
import {
  CategoryApiClient,
  CategoryResponseDto,
  CategoryType,
  FileApiClient,
  PoisApiClient,
  V1PoisGet200Response,
} from '@sw-code/urbo-backend-api';
import {
  FeedApiClient,
  PoisApiClient as SearchPoiClient,
  SearchRequestDto,
} from '@sw-code/urbo-search-api';
import { Observable, of } from 'rxjs';
import { map, take } from 'rxjs/operators';
import {
  createFeedItem,
  mapPoi,
  mapPoiWithDistance,
} from './mapper/poi.mapper';
import {
  buildCategoryIdsFilter,
  buildSourcesFilter,
  buildTypeFilter,
  combineFilters,
} from './feed-query-builder';
import { FeedItemMapper } from './mapper/feed-item.mapper';
import { mapCategory } from './mapper/category.mapper';
import { NEWS_MODULE_INFO } from '@sw-code/urbo-feature-news';
import { EVENTS_MODULE_INFO } from '@sw-code/urbo-feature-events';

@Injectable()
export class DefaultCoreAdapter implements CoreAdapter {
  private readonly categoryClient = inject(CategoryApiClient);
  private readonly poiClient = inject(PoisApiClient);
  private readonly searchPoiClient = inject(SearchPoiClient);
  private readonly fileApiClient = inject(FileApiClient);
  private readonly feedApi = inject(FeedApiClient);
  private readonly config = inject(CORE_MODULE_CONFIG);
  private readonly newsModuleInfo = inject(NEWS_MODULE_INFO);
  private readonly eventModuleInfo = inject(EVENTS_MODULE_INFO);

  getPoiCategories(): Observable<Category[]> {
    return this.categoryClient.v1CategoriesGet(CategoryType.Pois).pipe(
      take(1),
      map((response: CategoryResponseDto[]) =>
        response.map((dto) => mapCategory(dto)),
      ),
    );
  }

  getPoiById(id: string): Observable<Poi> {
    return this.poiClient.v1PoisIdGet(id).pipe(
      take(1),
      map((dto) => mapPoi(dto)),
    );
  }

  getPois(categoryIds: string[]): Observable<Poi[]> {
    return this.poiClient.v1PoisGet(0, 2000, undefined, categoryIds).pipe(
      take(1),
      map((response: V1PoisGet200Response) =>
        (response.content ?? []).map((dto) => mapPoi(dto)),
      ),
    );
  }

  searchPois(
    searchTerm: string,
    page?: number,
    size?: number,
  ): Observable<FeedItem[]> {
    const pageNumber = page ?? 1;
    const pageSize = size ?? 10;

    return this.searchPoiClient
      .indexesPoisSearchPost({
        hitsPerPage: pageSize,
        page: pageNumber,
        q: searchTerm,
      })
      .pipe(
        take(1),
        map(
          (response) =>
            response.hits?.map((dto) => {
              const poi = mapPoiWithDistance(dto).poi;
              return createFeedItem(poi, {
                forYouComponent: this.config.poiForYouComponent,
                cardComponent: this.config.poiCardComponent,
              });
            }) ?? [],
        ),
      );
  }

  uploadSingleImage(file: File): Observable<string | null> {
    return this.fileApiClient.filesPost(file).pipe(
      take(1),
      map((filename) => filename),
    );
  }

  getFeed(
    page: number,
    filterCriteria?: FeedFilterCriteria,
  ): Observable<FeedItem[]> {
    if (
      (filterCriteria?.type && filterCriteria.type.length === 0) ||
      (filterCriteria?.sources && filterCriteria.sources.length === 0) ||
      (filterCriteria?.categoryIds && filterCriteria.categoryIds.length === 0)
    ) {
      return of([]);
    }

    const typeFilter = buildTypeFilter(filterCriteria?.type);
    const sourcesFilter = buildSourcesFilter(filterCriteria?.sources);
    const categoryIdsFilter = buildCategoryIdsFilter(
      filterCriteria?.categoryIds,
    );

    const filterQuery = combineFilters([
      typeFilter,
      sourcesFilter,
      categoryIdsFilter,
    ]);

    const searchRequestDto = {
      page,
      sort: ['publishedDate:desc'],
      filter: filterQuery ? [filterQuery] : undefined,
      hitsPerPage: 10,
    } as SearchRequestDto;

    return this.feedApi.indexesFeedSearchPost(searchRequestDto).pipe(
      take(1),
      map((response) =>
        FeedItemMapper.mapFeedItems(
          response.hits ?? [],
          this.newsModuleInfo,
          this.eventModuleInfo,
        ),
      ),
    );
  }
}
