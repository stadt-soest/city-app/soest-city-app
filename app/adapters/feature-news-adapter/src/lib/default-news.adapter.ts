import { inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import {
  CategoryApiClient,
  CategoryType,
  NewsApiClient,
  NewsResponseDto,
} from '@sw-code/urbo-backend-api';
import {
  NewsApiClient as NewsSearchApiClient,
  NewsDto,
} from '@sw-code/urbo-search-api';
import { NewsMapper } from './news.mapper';
import {
  News,
  NewsAdapter,
  NewsCategory,
  NewsFeedItem,
} from '@sw-code/urbo-feature-news';

@Injectable()
export class DefaultNewsAdapter implements NewsAdapter {
  private readonly newsApiClient = inject(NewsApiClient);
  private readonly newsSearchApiClient = inject(NewsSearchApiClient);
  private readonly categoryApiClient = inject(CategoryApiClient);
  private readonly newsMapper = inject(NewsMapper);

  getFeed(): Observable<NewsFeedItem[]> {
    return this.newsApiClient.v1NewsGet(0, 2000).pipe(
      take(1),
      map((response) =>
        this.newsMapper.mapToFeedItemArray(response.content ?? []),
      ),
    );
  }

  getSources(): Observable<string[]> {
    return this.newsApiClient.v1NewsSourcesGet().pipe(take(1));
  }

  getPaginatedFeed(
    page: number,
  ): Observable<{ content: NewsFeedItem[]; totalRecords: number }> {
    return this.newsApiClient.v1NewsGet(page, 10, ['date,desc']).pipe(
      take(1),
      map((response) => ({
        content: this.newsMapper.mapToFeedItemArray(response.content ?? []),
        totalRecords: response.page?.totalElements ?? 0,
      })),
    );
  }

  getHighlightedPaginatedFeed(
    page: number,
  ): Observable<{ content: NewsFeedItem[]; totalRecords: number }> {
    return this.newsApiClient
      .v1NewsGet(page, 10, ['date,desc'], undefined, undefined, undefined, true)
      .pipe(
        take(1),
        map((response) => ({
          content: this.newsMapper.mapToFeedItemArray(response.content ?? []),
          totalRecords: response.page?.totalElements ?? 0,
        })),
      );
  }

  getPaginatedFilteredFeed(
    page: number,
    newsCategoryIds: string[],
    newsSources: string[],
    excludedNews: string[],
  ): Observable<{ content: NewsFeedItem[]; totalRecords: number }> {
    return this.newsApiClient
      .v1NewsGet(
        page,
        10,
        ['date,desc'],
        newsCategoryIds,
        newsSources,
        excludedNews,
      )
      .pipe(
        take(1),
        map((response) => ({
          content: this.newsMapper.mapToFeedItemArray(response.content ?? []),
          totalRecords: response.page?.totalElements ?? 0,
        })),
      );
  }

  getById(id: string): Observable<News> {
    return this.newsApiClient.v1NewsIdGet(id).pipe(
      take(1),
      map((response) => this.newsMapper.mapToModel(response)),
    );
  }

  getCategories(): Observable<NewsCategory[]> {
    return this.categoryApiClient.v1CategoriesGet(CategoryType.News).pipe(
      take(1),
      map((response) => this.newsMapper.mapToCategories(response)),
    );
  }

  getBySearchTerm(
    searchTerm: string,
    pageNumber: number,
    pageSize: number,
  ): Observable<{ total: number; items: NewsFeedItem[] }> {
    return this.newsSearchApiClient
      .indexesNewsSearchPost({
        q: searchTerm,
        page: pageNumber,
        hitsPerPage: pageSize,
      })
      .pipe(
        take(1),
        map((response) => {
          const normalizedHits = (response.hits ?? []).map((hit: NewsDto) => ({
            ...hit,
            editable: false,
          })) as NewsResponseDto[];

          const items = this.newsMapper.mapToFeedItemArray(normalizedHits);
          const total = response.totalHits ?? 0;

          return { items, total };
        }),
      );
  }
}
