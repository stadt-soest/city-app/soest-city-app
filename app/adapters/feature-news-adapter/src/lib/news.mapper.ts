import {
  CategoryResponseDto,
  NewsResponseDto,
} from '@sw-code/urbo-backend-api';
import { inject, Injectable } from '@angular/core';
import {
  AbstractMapper,
  DomPurifyConfigType,
  FeedItemLayoutType,
  ModuleInfo,
} from '@sw-code/urbo-core';
import {
  News,
  NEWS_MODULE_INFO,
  NewsCategory,
  NewsFeedCardComponent,
  NewsFeedItem,
  NewsForYouCardComponent,
} from '@sw-code/urbo-feature-news';
import { BaseContentFormatterPipe } from '@sw-code/urbo-ui';
import { ImageMapper } from './news-image.mapper';

@Injectable({ providedIn: 'root' })
export class NewsMapper extends AbstractMapper<NewsResponseDto, News> {
  private readonly newsModuleInfo = inject<ModuleInfo>(NEWS_MODULE_INFO);
  private readonly contentFormatter = inject(BaseContentFormatterPipe);

  mapToFeedItem(dto: NewsResponseDto): NewsFeedItem {
    try {
      return {
        id: dto.id,
        moduleId: this.newsModuleInfo.id,
        cardComponent: NewsFeedCardComponent,
        forYouComponent: NewsForYouCardComponent,
        baseRoutePath: this.newsModuleInfo.baseRoutingPath,
        moduleName: 'feature_news.module_name_singular',
        pluralModuleName: 'feature_news.module_name',
        icon: this.newsModuleInfo.icon,
        title: dto.title,
        image: ImageMapper.mapImage(dto.image),
        creationDate: dto.date ? new Date(dto.date) : new Date(),
        relevancy: this.newsModuleInfo.relevancy,
        layoutType: FeedItemLayoutType.card,
      };
    } catch (error) {
      console.error('Mapping of News failed in the NewsMapper:', error);
      throw new Error('Error while mapping News.');
    }
  }

  mapToFeedItemArray(dtos: NewsResponseDto[]): NewsFeedItem[] {
    return dtos.reduce((acc: NewsFeedItem[], dto: NewsResponseDto) => {
      try {
        const feedItem = this.mapToFeedItem(dto);
        acc.push(feedItem);
      } catch (error) {
        console.error(`Failed to map news item: ${error}`);
      }
      return acc;
    }, []);
  }

  override mapToModel(dto: NewsResponseDto): News {
    if (!dto.id || !dto.title || !dto.date || !dto.content) {
      throw new Error(
        `Required properties are missing in following DTO: ${dto.id}`,
      );
    }
    try {
      return {
        id: dto.id,
        title: this.contentFormatter.transform(
          dto.title,
          DomPurifyConfigType.title,
        ),
        subtitle: this.contentFormatter.transform(
          dto.subtitle ?? '',
          DomPurifyConfigType.title,
        ),
        text: this.contentFormatter.transform(
          dto.content,
          DomPurifyConfigType.description,
        ),
        categoryIds:
          dto.categories
            ?.map((category) => category.id)
            .filter((id): id is string => Boolean(id)) ?? [],
        source: dto.source,
        sourceUrl: dto.link,
        image: ImageMapper.mapImage(dto.image),
        date: dto.date ? new Date(dto.date) : new Date(),
        highlight: dto.highlight ?? false,
      };
    } catch (error) {
      console.error('Mapping of News failed in the NewsMapper:', error);
      throw new Error('Error while mapping News.');
    }
  }

  public mapToCategories(
    categoryResponseArray: Array<CategoryResponseDto>,
  ): Array<NewsCategory> {
    return categoryResponseArray.map((category) => ({
      id: category.id,
      sourceCategories: category.sourceCategories,
      localizedNames: category.localizedNames.map((localizedName) => ({
        value: localizedName.value,
        language: localizedName.language,
      })),
    }));
  }
}
