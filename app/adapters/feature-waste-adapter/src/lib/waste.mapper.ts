import { inject, Injectable } from '@angular/core';
import { FeedItemLayoutType, ModuleInfo } from '@sw-code/urbo-core';
import {
  Waste,
  WASTE_MODULE_INFO,
  WasteBinCardComponent,
  WasteBinCardForYouComponent,
  WasteConfigService,
  WasteDate,
  WasteFeedItem,
  WasteUpcoming,
} from '@sw-code/urbo-feature-waste';
import { IndexesWasteCollectionLocationsSearchPost200Response } from '@sw-code/urbo-search-api';
import {
  UpcomingWasteProjection,
  WasteDateDTO,
} from '@sw-code/urbo-backend-api';

@Injectable({ providedIn: 'root' })
export class WasteMapper {
  private readonly wasteModuleInfo = inject<ModuleInfo>(WASTE_MODULE_INFO);
  private readonly wasteConfigService = inject(WasteConfigService);

  mapToWasteUpcoming(waste: UpcomingWasteProjection): WasteUpcoming {
    const imageMapping = this.wasteConfigService.getImageMapping();
    const colorMapping = this.wasteConfigService.getColorMapping();
    const fallbackImagePath = this.wasteConfigService.getFallbackImagePath();
    const fallbackColor = this.wasteConfigService.getFallbackColor();

    const upcomingDatesByType: { [key: string]: Array<WasteDate> } = {};

    if (waste.upcomingDatesByType) {
      Object.keys(waste.upcomingDatesByType).forEach((wasteTypeKey) => {
        const dates = waste.upcomingDatesByType?.[wasteTypeKey] ?? [];
        upcomingDatesByType[wasteTypeKey] = dates.map((date: WasteDateDTO) => ({
          id: date.id,
          date: date.date,
          wasteType: date.wasteType as WasteDate.WasteTypeEnum,
          wasteTypeColor: colorMapping[date.wasteType] ?? fallbackColor,
          wasteTypeImagePath: imageMapping[date.wasteType] ?? fallbackImagePath,
        }));
      });
    }

    return {
      allUpcomingDates: (waste.allUpcomingDates || []).map(
        (date: WasteDateDTO) => ({
          id: date.id,
          date: date.date,
          wasteType: date.wasteType,
          wasteTypeColor: colorMapping[date.wasteType] ?? fallbackColor,
          wasteTypeImagePath: imageMapping[date.wasteType] ?? fallbackImagePath,
        }),
      ),
      upcomingDatesByType,
    };
  }

  mapToWasteArray(
    wasteResponseArray: IndexesWasteCollectionLocationsSearchPost200Response,
  ): Waste[] {
    const imageMapping = this.wasteConfigService.getImageMapping();
    const colorMapping = this.wasteConfigService.getColorMapping();
    const fallbackImagePath = this.wasteConfigService.getFallbackImagePath();
    const fallbackColor = this.wasteConfigService.getFallbackColor();
    return (
      wasteResponseArray.hits?.map((wasteResponse) => ({
        id: wasteResponse.id,
        city: wasteResponse.city,
        street: wasteResponse.street,
        wasteDates: wasteResponse.wasteDates.map((date) => ({
          id: date.id,
          date: date.date,
          wasteType: date.wasteType as WasteDate.WasteTypeEnum,
          wasteTypeColor: colorMapping[date.wasteType] ?? fallbackColor,
          wasteTypeImagePath: imageMapping[date.wasteType] ?? fallbackImagePath,
        })),
      })) || []
    );
  }

  mapToFeedItemArray(
    todayWaste: UpcomingWasteProjection,
    wasteUpcoming: WasteUpcoming,
  ): WasteFeedItem[] {
    const imageMapping = this.wasteConfigService.getImageMapping();
    const colorMapping = this.wasteConfigService.getColorMapping();
    const fallbackImagePath = this.wasteConfigService.getFallbackImagePath();
    const fallbackColor = this.wasteConfigService.getFallbackColor();

    return (
      todayWaste.allUpcomingDates?.map((wasteDateDto: WasteDateDTO) => {
        const wasteDate = {
          id: wasteDateDto.id,
          date: wasteDateDto.date,
          wasteType: wasteDateDto.wasteType as WasteDate.WasteTypeEnum,
          wasteTypeColor: colorMapping[wasteDateDto.wasteType] ?? fallbackColor,
          wasteTypeImagePath:
            imageMapping[wasteDateDto.wasteType] ?? fallbackImagePath,
        };

        return {
          id: wasteDateDto.id,
          moduleId: this.wasteModuleInfo.id,
          moduleName: 'feature_waste.module_name_singular',
          pluralModuleName: 'feature_waste.module_name',
          baseRoutePath: this.wasteModuleInfo.baseRoutingPath,
          icon: this.wasteModuleInfo.icon,
          title: '',
          wasteDate,
          nextCollectionDate: this.getNextCollectionDate(
            wasteUpcoming,
            wasteDateDto.wasteType,
            1,
          ),
          creationDate: new Date(),
          cardComponent: WasteBinCardComponent,
          forYouComponent: WasteBinCardForYouComponent,
          layoutType: FeedItemLayoutType.card,
          relevancy: 1,
        };
      }) || []
    );
  }

  getNextCollectionDate(
    wasteUpcoming: WasteUpcoming,
    wasteType: string,
    index: number,
  ): string {
    const upcomingDates = wasteUpcoming.upcomingDatesByType?.[wasteType];
    return upcomingDates?.[index]?.date ?? '';
  }
}
