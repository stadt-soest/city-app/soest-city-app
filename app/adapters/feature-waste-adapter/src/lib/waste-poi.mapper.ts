import {
  MappedCategoryDto,
  PoiLocationDto,
  PoiResponseDto,
} from '@sw-code/urbo-backend-api';
import {
  Poi,
  PoiCategory,
  PoiContactPoint,
  PoiLocation,
} from '@sw-code/urbo-core';

export const mapPoi = (dto: PoiResponseDto): Poi => {
  const categories = dto.categories?.map(mapPoiCategory) ?? [];
  const location = mapPoiLocation(dto.location);
  const contactPoint = mapPoiContactPoint(dto);
  const icon = dto.categories?.[0]?.icon;

  return new Poi({
    id: dto.id,
    name: dto.name,
    url: dto.url ?? '',
    location,
    contactPoint,
    dateModified: dto.dateModified,
    categories,
    icon: icon ?? undefined,
  });
};

export const mapPoiCategory = (dto: MappedCategoryDto): PoiCategory =>
  new PoiCategory(dto.id ?? '', dto.localizedNames ?? [], dto.sourceCategory);

export const mapPoiLocation = (dto: PoiLocationDto): PoiLocation =>
  new PoiLocation(dto.coordinates, dto.street, dto.postalCode, dto.city);

export const mapPoiContactPoint = (dto: PoiResponseDto): PoiContactPoint =>
  new PoiContactPoint(
    dto.contactPoint.email,
    dto.contactPoint.telephone,
    dto.contactPoint.contactPerson,
  );
