import { inject, Injectable } from '@angular/core';
import { Observable, switchMap } from 'rxjs';
import { map, take } from 'rxjs/operators';
import {
  CategoryApiClient,
  CategoryType,
  PoisApiClient,
  V1PoisGet200Response,
  WasteApiClient,
} from '@sw-code/urbo-backend-api';
import {
  IndexesWasteCollectionLocationsSearchPost200Response,
  WasteCollectionsApiClient,
} from '@sw-code/urbo-search-api';
import { WasteMapper } from './waste.mapper';
import {
  Waste,
  WasteAdapter,
  WasteFeedItem,
  WasteUpcoming,
} from '@sw-code/urbo-feature-waste';
import { PoiWithDistance } from '@sw-code/urbo-core';
import { mapPoi } from './waste-poi.mapper';

@Injectable()
export class DefaultWasteAdapter implements WasteAdapter {
  private readonly wasteApiClient = inject(WasteApiClient);
  private readonly poiClient = inject(PoisApiClient);
  private readonly categoryClient = inject(CategoryApiClient);
  private readonly wasteMapper = inject(WasteMapper);
  private readonly wasteCollectionsApiClient = inject(
    WasteCollectionsApiClient,
  );

  getUpcomingWasteCollections(
    street: string,
    wasteTypes: string[],
    startDate?: string,
    endDate?: string,
  ): Observable<WasteUpcoming> {
    const startInstant = startDate
      ? new Date(startDate).toISOString()
      : undefined;
    const endInstant = endDate ? new Date(endDate).toISOString() : undefined;

    return this.wasteApiClient
      .v11WasteCollectionsWasteIdUpcomingWasteCollectionDatesGet(
        street,
        startInstant,
        endInstant,
        wasteTypes,
      )
      .pipe(
        take(1),
        map((response) => this.wasteMapper.mapToWasteUpcoming(response)),
      );
  }

  getWasteCollection(searchTerm: string): Observable<Waste[]> {
    return this.wasteCollectionsApiClient
      .indexesWasteCollectionLocationsSearchPost({
        hitsPerPage: 15,
        page: 1,
        q: searchTerm,
      })
      .pipe(
        take(1),
        map((response: IndexesWasteCollectionLocationsSearchPost200Response) =>
          this.wasteMapper.mapToWasteArray(response ?? { content: [] }),
        ),
      );
  }

  getUpcomingWasteCollectionsFeed(
    street: string,
    wasteTypes: string[],
    startDate?: string,
    endDate?: string,
  ): Observable<{ content: WasteFeedItem[]; totalRecords: number }> {
    return this.getUpcomingWasteCollections(street, wasteTypes, startDate).pipe(
      switchMap((wasteUpcomingResponse) => {
        const wasteUpcoming = this.wasteMapper.mapToWasteUpcoming(
          wasteUpcomingResponse ?? [],
        );

        return this.getUpcomingWasteCollections(
          street,
          wasteTypes,
          startDate,
          endDate,
        ).pipe(
          map((wasteTodayResponse) => {
            const wasteToday = this.wasteMapper.mapToFeedItemArray(
              wasteTodayResponse ?? [],
              wasteUpcoming,
            );

            return {
              content: wasteToday,
              totalRecords: wasteToday.length ?? 0,
            };
          }),
        );
      }),
    );
  }

  getFilteredCategories(sourceCategories: string[]): Observable<string[]> {
    return this.categoryClient.v1CategoriesGet(CategoryType.Pois).pipe(
      take(1),
      map((categories) =>
        categories
          .filter(
            (category) =>
              category.sourceCategories &&
              sourceCategories.some((sc) =>
                category.sourceCategories?.includes(sc),
              ),
          )
          .map((category) => category.id),
      ),
    );
  }

  fetchPois(categoryIds: string[]): Observable<PoiWithDistance[]> {
    return this.poiClient.v1PoisGet(0, 2000, undefined, categoryIds).pipe(
      take(1),
      map((response: V1PoisGet200Response) =>
        (response.content ?? []).map((dto) => new PoiWithDistance(mapPoi(dto))),
      ),
    );
  }

  getWasteRecyclingPoisWithDistance(): Observable<PoiWithDistance[]> {
    const sourceCategories = [
      'Altglascontainer',
      'Kleidercontainer',
      'Wertstoffhof',
    ];
    return this.getFilteredCategories(sourceCategories).pipe(
      switchMap((categoryIds) => this.fetchPois(categoryIds)),
    );
  }
}
