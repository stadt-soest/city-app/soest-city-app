const nxPreset = require('@nx/jest/preset').default;

const esmModules = [
  'photoswipe',
  'flat',
  '@ionic',
  '@awesome-cordova-plugins',
  'ionicons',
  '@stencil',
  '.*\\.mjs',
];

module.exports = {
  ...nxPreset,
  transform: {
    '^.+\\.(ts|mjs|js|html)$': [
      'jest-preset-angular',
      {
        tsconfig: '<rootDir>/tsconfig.spec.json',
        stringifyContentPathRegex: '\\.(html|svg)$',
      },
    ],
  },
  transformIgnorePatterns: [
    `node_modules/(?!(?:\\.pnpm/)?(${esmModules.join('|')}))`,
  ],
  snapshotSerializers: [
    'jest-preset-angular/build/serializers/no-ng-attributes',
    'jest-preset-angular/build/serializers/ng-snapshot',
    'jest-preset-angular/build/serializers/html-comment',
  ],
  setupFilesAfterEnv: ['<rootDir>/../../test-setup.ts'],
};
