# Table of Contents

* [Introduction and Goals](#introduction-and-goals)
* [Requirements Overview](#requirements-overview)
    * [Main Features](#main-features)
    * [User Requirements](#user-requirements)
* [Quality Goals](#quality-goals)
    * [Table 1. Quality Goals](#table-1-quality-goals)
* [Stakeholders](#stakeholders)
    * [Table 2. Stakeholders](#table-2-stakeholders)
* [Architecture Constraints](#architecture-constraints)
    * [Technical Constraints](#technical-constraints)
        * [Table 3. List of Technical Constraints](#table-3-list-of-technical-constraints)
    * [Organizational Constraints](#organizational-constraints)
        * [Table 4. List of Organizational Constraints](#table-4-list-of-organizational-constraints)
    * [Conventions](#conventions)
        * [Table 5. List of Conventions](#table-5-list-of-conventions)
* [System Scope and Context](#system-scope-and-context)
    * [Business Context](#business-context)
        * [Residents](#residents)
        * [City Officials](#city-officials)
        * [Smart City App](#smart-city-app)
        * [City Management System](#city-management-system)
        * [Spring Application](#spring-application)
        * [Orion Context Broker](#orion-context-broker)
        * [Node-RED](#node-red)
        * [City APIs](#city-apis)
    * [Technical Context](#technical-context)
* [Solution Strategy](#solution-strategy)
    * [Architecture Decisions and Justifications](#architecture-decisions-and-justifications)
        * [1. Use of Kubernetes for Deployment](#1-use-of-kubernetes-for-deployment)
        * [2. CI/CD with GitHub Actions](#2-cicd-with-github-actions)
        * [3. Spring Boot for Backend](#3-spring-boot-for-backend)
        * [4. Ionic for Frontend](#4-ionic-for-frontend)
        * [5. MeiliSearch for Advanced Search Capabilities](#5-meilisearch-for-advanced-search-capabilities)
* [Building Block View](#building-block-view)
    * [Whitebox Overall System](#whitebox-overall-system)
        * [Overview Diagram](#overview-diagram)
        * [Contained Building Blocks](#contained-building-blocks)
        * [Important Interfaces](#important-interfaces)
        * [Blackbox View of Smart City App](#blackbox-view-of-smart-city-app)
        * [Blackbox View of City Management System](#blackbox-view-of-city-management-system)
        * [Blackbox View of Spring Application](#blackbox-view-of-spring-application)
        * [Blackbox View of Orion Context Broker](#blackbox-view-of-orion-context-broker)
        * [Blackbox View of Node-RED](#blackbox-view-of-node-red)
* [Runtime View](#runtime-view)
    * [Runtime Scenario 1: User Reports an Issue](#runtime-scenario-1-user-reports-an-issue)
    * [Runtime Scenario 2: City Data Update](#runtime-scenario-2-city-data-update)
    * [Runtime Scenario 3: Image Processing when Fetching Data from Orion Context Broker](#runtime-scenario-3-image-processing-when-fetching-data-from-orion-context-broker)
* [Deployment View](#deployment-view)
    * [Overview Diagram](#overview-diagram-1)
    * [Motivation](#motivation)
    * [Quality and/or Performance Features](#quality-andor-performance-features)
    * [Mapping of Building Blocks to Infrastructure](#mapping-of-building-blocks-to-infrastructure)
    * [Infrastructure Level 1: CI/CD Pipeline](#infrastructure-level-1-cicd-pipeline)
    * [Infrastructure Level 2: Kubernetes Cluster](#infrastructure-level-2-kubernetes-cluster)
    * [Infrastructure Level 3: Mobile Deployment](#infrastructure-level-3-mobile-deployment)
    * [CI/CD Pipeline Description](#cicd-pipeline-description)
* [Architecture Decisions](#architecture-decisions)
    * [ADR 1: Use of Kubernetes for Deployment](#adr-1-use-of-kubernetes-for-deployment)
    * [ADR 2: CI/CD with GitHub Actions](#adr-2-cicd-with-github-actions)
    * [ADR 3: Spring Boot for Backend](#adr-3-spring-boot-for-backend)
    * [ADR 4: Ionic and Capacitor for Frontend](#adr-4-ionic-and-capacitor-for-frontend)
    * [ADR 5: MeiliSearch for Advanced Search Capabilities](#adr-5-meilisearch-for-advanced-search-capabilities)
    * [Summary of ADRs](#summary-of-adrs)
* [Quality Requirements](#quality-requirements)
    * [Quality Tree](#quality-tree)
    * [Quality Scenarios](#quality-scenarios)
* [Glossary](#glossary)

# Introduction and Goals

The Urbo smart city app aims to enhance urban living by integrating various city services into a single, user-friendly
platform. The app, developed using Ionic for cross-platform support, ensures
compatibility with web, native apps, and
progressive web apps. The backend, built
with Kotlin Spring Boot, supports
robust data processing and integration with
city systems. The CMS frontend, developed in Angular, provides city officials with a powerful
tool for managing and
monitoring city data. High priority is placed on user experience (UX) and user interface (UI) design. Urbo integrates
with the city's data platform using the FIWARE standard, operated via Orion.

## Requirements Overview

## Main Features

- **High Priority on UX/UI**: Focus on delivering a user-friendly and visually appealing interface.
- **User Engagement**: Provides various services to residents, enhancing their interaction with the city's offerings.
- **FIWARE Standard Integration**: Seamless integration with the city’s data platform using
  the FIWARE standard, managed
  via Orion.
- **Cross-Platform Support**: Developed with Ionic, Urbo supports web, native apps, and
  progressive web apps, ensuring
  accessibility on various devices.
- **CMS Frontend**: An Angular-based CMS for city officials to manage and update city data.

## User Requirements

- The application must provide seamless integration with existing city data platforms.
- It should support a wide range of devices and platforms, ensuring accessibility for all users.
- The CMS should be intuitive and easy to use for city officials, allowing them to manage data efficiently.
- The application should prioritize user experience, providing a visually appealing and easy-to-navigate interface.
- It must handle real-time data efficiently, providing up-to-date information to users.

By focusing on these requirements and features, Urbo aims to enhance urban living through effective data management and
user engagement, providing valuable services to city residents.

## Quality Goals

### Table 1. Quality Goals

| Nr. | Quality              | Motivation                                                                                                                                                       |
|-----|----------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1   | Usability            | The app should be exceptionally user-friendly and accessible to all citizens of the city, regardless of age, ensuring ease of use for every age group.           |
| 2   | Functional Stability | The app must maintain high stability and availability, reliably performing its required functions under all conditions to meet the needs of a diverse user base. | 
| 3   | Maintainability      | The architecture should allow easy modifications and updates to the system.                                                                                      |
| 4   | Transferability      | The application should be easy to adapt to different environments or new technologies.                                                                           |

## Stakeholders

The following lists contains the most important personas for this application

### Table 2. Stakeholders

| Role/Name                      | Contact                            | Expectations                                                                                                                                                                               |
|--------------------------------|------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Stadt Soest / Stadtlabor Soest | app@soest.de / stadtlabor@soest.de | Ensure the app meets city regulations and integrates with the city's existing infrastructure. Ensure the app addresses local needs and provides high usability for residents and visitors. |
| Residents                      | info@swcode.io                     | Easy access to city services and information, user-friendly interface, reliable performance.                                                                                               |
| Development Team               | info@swcode.io                     | Clear requirements and constraints, access to necessary resources and tools.                                                                                                               |
| UX/UI Designers                | info@swcode.io                     | High priority on UX/UI, ensure the app is user-friendly and accessible.                                                                                                                    |

# Architecture Constraints

The constraints on this project are reflected in the final solution. This section shows them and if applicable,
their motivation.

## Technical Constraints

### Table 3. List of Technical Constraints

| Constraint                               | Background and / or motivation                                                                                           |
|------------------------------------------|--------------------------------------------------------------------------------------------------------------------------|
| **Software and programming constraints** |
| TC1                                      | Cross-Platform Support                                                                                                   |
|                                          | The application must be developed using Ionic to ensure it runs on web, native apps, and progressive web apps.           |
| TC2                                      | Backend Framework                                                                                                        |
|                                          | The backend must be implemented using Kotlin Spring Boot to ensure scalability and performance.                          |
| TC3                                      | Frontend Framework                                                                                                       |
|                                          | The CMS frontend must be built using Angular for robust data management by city officials.                               |
| **Operating System Constraints**         |
| TC4                                      | OS Independent Development                                                                                               |
|                                          | The application should be compilable on all major operating systems (Windows, macOS, Linux).                             |
| TC5                                      | Deployable to a Linux Server                                                                                             |
|                                          | The application should be deployable through standard means on a Linux-based server.                                     |
| **Hardware Constraints**                 |
| TC6                                      | Memory Friendly                                                                                                          |
|                                          | Memory can be limited (due to availability on a shared host or deployment to a cloud-based host). Minimize memory usage. |

## Organizational Constraints

### Table 4. List of Organizational Constraints

| Constraint                                             | Background and / or motivation                                                                                                              |
|--------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------|
| **OC1 Team**                                           | Dedicated team including developers, designers, and project managers to ensure timely and efficient development and maintenance.            |
| **OC2 Time Schedule**                                  | The project must adhere to a strict timeline with specific milestones for development, testing, and deployment.                             |
| **OC3 IDE Independent Project Setup**                  | The project setup should be IDE-independent, allowing development using various IDEs or command line tools for flexibility.                 |
| **OC4 Configuration and Version Control / Management** | Utilize a private Git repository with a comprehensive commit history and a public master branch pushed to Open CoDe for transparency.       |
| **OC5 Testing**                                        | Implement robust testing protocols, including unit tests and integration tests, to ensure high code quality and coverage.                   |
| **OC6 Published under an Open Source License**         | The project, including its documentation, should be published as open source under the Apache 2 License to encourage community involvement. |

## Conventions

### Table 5. List of Conventions

| Conventions                   | Background and / or motivation                                                                                                 |
|-------------------------------|--------------------------------------------------------------------------------------------------------------------------------|
| C1 Architecture Documentation | Structure based on the English arc42-Template, adapted for the Urbo project.                                                   |
| C2 Coding Conventions         | The project uses the recommended coding conventions for Kotlin, Angular, and Spring to ensure consistency and maintainability. |
| C3 Language                   | English is used throughout the project to ensure accessibility and understanding for an international audience.                |
| C4 Naming Conventions         | Standard naming conventions for Kotlin, Angular, and Spring are followed to enhance code readability and maintainability.      |
| C5 Dependency Management      | Use of dependency management tools like Gradle for Kotlin and Spring, and npm for Angular to manage project dependencies.      |
| C6 Testing Practices          | Implement best practices for unit testing and integration testing using JUnit for Kotlin, Jest for Angular, and Spring Test.   |
| C7 Code Reviews               | Regular code reviews are conducted to ensure code quality, adherence to conventions, and knowledge sharing among the team.     |
| C8 Documentation              | Comprehensive documentation is maintained, including API documentation, user guides, and developer guides.                     |
| C9 Version Control            | Use Git for version control with a branching strategy that includes feature branches, development branches, and main branches. |

# System Scope and Context

The Urbo app aims to streamline access to various city services for residents while providing city officials with a
robust tool for data management and analytics. The app will enable residents to report issues, access city information,
and interact with various city services seamlessly. City officials will have access to a CMS for managing this data and
monitoring city metrics.

## Business Context

```mermaid
graph TD
    Residents[Residents] -->|Use App| App[Smart City App] -->|API Calls| SpringApplication
    CityOfficials[City Officials] -->|Manage Data| Cms[City Management System] -->|API Calls| SpringApplication
    SpringApplication -->|Receives Data| Orion[Orion Context Broker]
    NodeRED[Node-RED] -->|Sends Mapped Data| Orion
    NodeRED -->|Fetches Data| CityAPIs[City APIs]

    subgraph User
        Residents[Residents]
        CityOfficials[City Officials]
    end

    subgraph Frontend
        App[Smart City App]
        Cms[City Management System]
    end

    subgraph Backend
        SpringApplication[Spring Application]
    end

    subgraph Integration
        Orion[Orion Context Broker]
        NodeRED[Node-RED]
        CityAPIs[City APIs]
    end

    linkStyle 0 stroke:#66f,stroke-width:2px,fill:none,stroke-dasharray:5,5
    linkStyle 2 stroke:#66f,stroke-width:2px,fill:none,stroke-dasharray:5,5
    linkStyle 4 stroke:#f6f,stroke-width:2px,fill:none,stroke-dasharray:5,5
```

### Residents

A resident uses the Smart City App to interact with city services, report issues, access city information, and more. The
app provides a user-friendly interface to ensure seamless interaction with the various services offered by the city.

### City Officials

City officials use the City Management System to manage and monitor city data. This system provides tools for data
analysis, report generation, and real-time monitoring of city metrics to aid in effective city management.

### Smart City App

The Smart City App serves as the primary interface for residents to access city services. It makes API calls to the
Spring Application backend to fetch necessary data and functionalities required by the residents.

### City Management System

The City Management System is used by city officials to manage and monitor data. It interacts with the Spring
Application backend through API calls to handle various data management tasks.

### Spring Application

The Spring Application is the core backend system built with Kotlin
and Spring Boot. It handles all business logic,
processes API requests from the Smart City App and City Management System, and interacts with external systems for data
integration.

### Orion Context Broker

The Spring Application receives data from the Orion Context Broker,
which is part of the FIWARE ecosystem. The Orion
Context Broker manages and persists data from various sources in a standardized format.

### Node-RED

Node-RED fetches data from City APIs, maps it to the FIWARE
standard, and sends the mapped data to the Orion Context
Broker. This ensures that the data is standardized and can be effectively integrated with the city's data platform.

### City APIs

The City APIs provide various data points and information needed by the Node-RED
system. Node-RED fetches this data,
processes it, and sends it to the Orion Context Broker in a
standardized format.

## Technical Context

```mermaid
graph TD
    Residents[Residents] -->|HTTPS| App[Smart City App]
    CityOfficials[City Officials] -->|HTTPS| Cms[City Management System]
    App -->|HTTPS/REST| SpringApplication
    Cms -->|HTTPS/REST| SpringApplication
    Orion -->|Sync Data| SpringApplication
    SpringApplication -->|JDBC| PostgreSQL[PostgreSQL Database]
    SpringApplication -->|S3 API| S3[Telekom S3]
    SpringApplication -->|HTTP| Imaginary[Imaginary]
    SpringApplication -->|HTTP| Meilisearch[Meilisearch]
    NodeRED[Node-RED] -->|HTTP/REST| Orion
    NodeRED -->|HTTP/REST| CityAPIs[City APIs]

    subgraph Urbo
        subgraph User
            Residents[Residents]
            CityOfficials[City Officials]
        end

        subgraph Frontend
            App[Smart City App]
            Cms[City Management System]
        end

        subgraph Backend
            SpringApplication[Spring Application]
            PostgreSQL[PostgreSQL Database]
            S3[Telekom S3]
            Imaginary[Imaginary]
            Meilisearch[Meilisearch]
        end
    end

    subgraph Integration
        Orion[Orion Context Broker]
        NodeRED[Node-RED]
        CityAPIs[City APIs]
    end

    linkStyle 0 stroke:#66f,stroke-width:2px,fill:none,stroke-dasharray:5,5
    linkStyle 1 stroke:#66f,stroke-width:2px,fill:none,stroke-dasharray:5,5
    linkStyle 2 stroke:#66f,stroke-width:2px,fill:none,stroke-dasharray:5,5
    linkStyle 3 stroke:#66f,stroke-width:2px,fill:none,stroke-dasharray:5,5
    linkStyle 4 stroke:#f6f,stroke-width:2px,fill:none,stroke-dasharray:5,5
    linkStyle 5 stroke:#6f6,stroke-width:2px,fill:none,stroke-dasharray:5,5
    linkStyle 6 stroke:#6f6,stroke-width:2px,fill:none,stroke-dasharray:5,5
    linkStyle 7 stroke:#6f6,stroke-width:2px,fill:none,stroke-dasharray:5,5
    linkStyle 8 stroke:#6f6,stroke-width:2px,fill:none,stroke-dasharray:5,5
```

The Technical Context diagram outlines the interactions and data flows between the primary components of the Urbo
system. This diagram highlights the synchronization of data from
the Orion Context Broker to the backend and
distinguishes between components that are part of the Urbo system and those that are external.

**Components**

- **Urbo**:
    - **User Layer**:
        - **Residents**: Utilize the Smart City App to access city services securely over HTTPS.
        - **City Officials**: Use the City Management System to manage and monitor city data securely over HTTPS.

    - **Frontend Layer**:
        - **Smart City App**: Connects to the backend via HTTPS/REST API calls to the Spring Application.
        - **City Management System**: Interacts with the backend via HTTPS/REST API calls to the Spring Application.

    - **Backend Layer**:
        - **Spring Application**: The core backend system, built with Kotlin
          and Spring Boot, responsible for business
          logic, data processing, and serving data to the frontend applications.
        - **PostgreSQL Database**: Provides persistent data storage, accessed via JDBC by the
          Spring Application.
        - **Telekom S3**: Stores media files, accessed via the S3 API by the Spring Application.
        - **Imaginary**: Microservice for image processing, accessed via HTTP by the
          Spring Application.
        - **Meilisearch**: Search engine for fast and relevant search capabilities, accessed
          via HTTP by the Spring
          Application.

- **Integration**:
    - **Node-RED**: Fetches data from City APIs over HTTP/REST, maps this data to
      the FIWARE
      standard, and sends the mapped
      data to the Orion Context Broker over HTTP/REST.
    - **City APIs**: Provide various data points required by the system.
    - **Orion Context Broker**: Manages and persists city data as part of
      the **FIWARE** ecosystem,
      synchronizing this data with
      the Spring Application to ensure the backend is up-to-date.

| Component              | Interaction  | Channel                | Protocol   |
|------------------------|--------------|------------------------|------------|
| Residents              | Use App      | Smart City App         | HTTPS      |
| City Officials         | Manage Data  | City Management System | HTTPS      |
| Smart City App         | API Calls    | Spring Application     | HTTPS/REST |
| City Management System | API Calls    | Spring Application     | HTTPS/REST |
| Spring Application     | Sync Data    | Orion Context Broker   | HTTP/REST  |
| Spring Application     | Uses         | PostgreSQL Database    | JDBC       |
| Spring Application     | Uses         | Telekom S3             | S3 API     |
| Spring Application     | Uses         | Imaginary              | HTTP       |
| Spring Application     | Uses         | Meilisearch            | HTTP       |
| Node-RED               | Sends Data   | Orion Context Broker   | HTTP/REST  |
| Node-RED               | Fetches Data | City APIs              | HTTP/REST  |

**Data Flow and Integration:**

- **Data Serving**: The backend serves data to the Smart City App and the City Management System by querying the
  PostgreSQL
  database. This ensures that both residents and city officials have access to current information.
- **Data Synchronization**: The Spring Application periodically synchronizes data from
  the Orion Context Broker to the
  PostgreSQL database. This process ensures data consistency and reliability across the
  system.
- **Integration Flow**: Node-RED acts as the intermediary for data integration, fetching raw
  data from City APIs,
  mapping it
  to the FIWARE standard, and sending it to
  the Orion Context Broker. The Spring
  Application then fetches this data from
  the Orion Context Broker to keep the database updated.
- **Data Processing**: Asynchronous processing tasks, such as image handling and complex searches, are managed by the
  Spring
  Application. Images uploaded by users are processed using the Imaginary service,
  and search requests are handled by
  Meilisearch to provide efficient and relevant search results.

# Solution Strategy

The Urbo Smart City application aims to streamline access to various city services for residents while providing city
officials with robust tools for data management and analytics. The application is designed to be cross-platform,
leveraging modern technologies to ensure high performance, security, and user satisfaction.

## Architecture Decisions and Justifications

### 1. Use of Kubernetes for Deployment

Kubernetes was chosen for its robust orchestration capabilities, which allow for easy scaling
and management of
application components. Kubernetes ensures high availability and provides seamless updates and
rollbacks, making it an
ideal choice for a scalable and reliable deployment environment.

### 2. CI/CD with GitHub Actions

Continuous integration and continuous deployment (CI/CD) are essential for maintaining the quality
and consistency of the application. The team needs a reliable CI/CD pipeline to automate the build,
test, and deployment processes. We decided to use GitHub Actions for managing the CI/CD process. GitHub Actions offers
comprehensive support for building, testing, and deploying applications across multiple platforms.

### 3. Spring Boot for Backend

Spring Boot provides a comprehensive framework for building robust and
scalable backend applications. It supports
microservices architecture and RESTful APIs, which are essential for handling complex business logic and data processing
in the Urbo app. Spring Boot's extensive community support and integration
capabilities make it a reliable choice.

### 4. Ionic for Frontend

Ionic was chosen for its ability to build cross-platform mobile applications from a
single codebase. This ensures
consistency across web, native apps, and progressive web apps, reducing development effort and
time. Ionic's focus on
modern web technologies (HTML, CSS, JavaScript) aligns well with the development team's expertise.

### 5. MeiliSearch for Advanced Search Capabilities

Meilisearch offers advanced search capabilities with flexible input, providing a more
user-friendly search experience
compared to traditional databases like PostgreSQL. Its ability to deliver fast and
relevant search results is crucial
for enhancing user engagement in the Urbo app. Meilisearch's ease of integration and
optimization capabilities make it a
suitable choice.

## Summary of Justifications

Each technology was chosen based on its ability to meet specific requirements of the Urbo Smart City application.
Kubernetes ensures scalability and reliability for deployments, while GitHub Actions automates CI/CD processes.
Spring Boot provides a robust backend framework, and Ionic enables cross-platform development.
Meilisearch enhances search functionalities.

By carefully selecting these technologies, the Urbo project aims to deliver a high-quality application that meets the
needs of both residents and city officials, ensuring an efficient, scalable, and user-friendly solution.

# Building Block View

## Whitebox Overall System

### Overview Diagram

```mermaid
graph TD
Residents[Residents] --> App[Smart City App]
CityOfficials[City Officials] --> Cms[City Management System]
App --> SpringApplication[Spring Application]
Cms --> SpringApplication
SpringApplication --> Orion[Orion Context Broker]
NodeRED[Node-RED] --> Orion
NodeRED --> CityAPIs[City APIs]
SpringApplication --> PostgreSQL[PostgreSQL Database]
SpringApplication --> S3[Telekom S3]
SpringApplication --> Imaginary[Imaginary]
SpringApplication --> Meilisearch[Meilisearch]

    subgraph User
        Residents[Residents]
        CityOfficials[City Officials]
    end

    subgraph Frontend
        App[Smart City App]
        Cms[City Management System]
    end

    subgraph Backend
        SpringApplication[Spring Application]
        PostgreSQL[PostgreSQL Database]
        S3[Telekom S3]
        Imaginary[Imaginary]
        Meilisearch[Meilisearch]
    end

    subgraph Integration
        Orion[Orion Context Broker]
        NodeRED[Node-RED]
        CityAPIs[City APIs]
    end
```

### Contained Building Blocks

- **Smart City App**: The frontend application used by residents to interact with city services.
- **City Management System**: The frontend application used by city officials to manage and monitor city data.
- **Spring Application**: The backend system built with Kotlin
  and Spring Boot, handling all business logic and data
  processing.
- **Orion Context Broker**: Part of
  the FIWARE ecosystem, used for managing and persisting
  city data.
- **Node-RED**: Responsible for fetching data from City APIs, mapping it
  to FIWARE
  standards, and sending it to the
  Orion Context Broker.
- **City APIs**: External APIs managed by the city, providing various data points required by the system.
- **PostgreSQL Database**: Used for persistent data storage.
- **Telekom S3**: Used for storing media files.
- **Imaginary**: A microservice used for image processing.
- **Meilisearch**: A search engine used for fast and relevant search capabilities within
  the system.

### Important Interfaces

- **API Gateway**: Handles incoming API requests from the Smart City App and City Management System to the Spring
  Application.
- **Data Integration Interface**: Facilitates communication between the Spring Application and
  the Orion Context Broker.
- **Data Fetching Interface**: Used by Node-RED to fetch data from City APIs and send mapped
  data to the Orion Context
  Broker.
- **Message Queue**: Manages asynchronous processing tasks such as image processing and data updates.

### Blackbox View of Smart City App

```mermaid
graph TD
SmartCityApp -->|REST API| SpringApplication
SmartCityApp -->|Provides Interface for| Residents
```

- **Purpose/Responsibility**: Provide residents with a user-friendly interface to access city services.
- **Interface(s)**: REST API
- **Quality/Performance Characteristics**: High responsiveness, user-friendly UI
- **Fulfilled Requirements**: User access to city services
- **Open Issues/Problems/Risks**: Continuous updates required to improve user experience

### Blackbox View of City Management System

```mermaid
graph TD
CMS[City Management System] -->|REST API| Spring[Spring Application]
CMS[City Management System] -->|Provides Interface for| CityOfficials[City Officials]
```

- **Purpose/Responsibility**: Enable city officials to manage and monitor city data.
- **Interface(s)**: REST API
- **Quality/Performance Characteristics**: High performance, secure access
- **Fulfilled Requirements**: Data management and monitoring for city officials
- **Open Issues/Problems/Risks**: Security updates and maintenance

### Blackbox View of Spring Application

```mermaid
graph TD
SMC[Smart City App] -->|Consumes REST API| Spring[Spring Application]
CMS[City Management System] -->|Consumes REST API| Spring[Spring Application]
Spring[Spring Application] -->|Syncs Data from| Orion[Orion Context Broker]
Spring[Spring Application] -->|Stores and Retrieves Data| Psql[PostgreSQL Database]
Spring[Spring Application] -->|Uses S3 API| S3[Telekom S3]
Spring[Spring Application] -->|Uses HTTP| Imaginary
Spring[Spring Application] -->|Uses HTTP| Meilisearch
```

- **Purpose/Responsibility**: Handle business logic and data processing.
- **Interface(s)**: REST API, JDBC, S3 API, HTTP
- **Quality/Performance Characteristics**: High reliability, scalability
- **Fulfilled Requirements**: Backend processing and data management
- **Open Issues/Problems/Risks**: Performance tuning and optimization

### Blackbox View of Orion Context Broker

```mermaid
graph TD
Orion[Orion Context Broker] -->|HTTP/REST| Spring[Spring Application]
Orion[Orion Context Broker] -->|Receives Data| NodeRED
Orion[Orion Context Broker] -->|Provides Data| CityAPIs
```

- **Purpose/Responsibility**: Manage and persist city data.
- **Interface(s)**: HTTP/REST
- **Quality/Performance Characteristics**: Data consistency, integration with FIWARE
- **Fulfilled Requirements**: Standardized data management
- **Open Issues/Problems/Risks**: Integration challenges with external systems

### Blackbox View of Node-RED

```mermaid
graph TD
NodeRED -->|HTTP/REST| Orion[Orion Context Broker]
NodeRED -->|Fetches Data| CityAPIs
```

- **Purpose/Responsibility**: Fetch and map data from City APIs to FIWARE standards.
- **Interface(s)**: HTTP/REST
- **Quality/Performance Characteristics**: Flexibility, ease of integration
- **Fulfilled Requirements**: Data fetching and mapping
- **Open Issues/Problems/Risks**: Reliability of data sources

## Runtime View

### Runtime Scenario 1: User Reports an Issue

1. **Residents** use the **Smart City App** to report an issue.
2. The **Smart City App** sends a **REST API** request to the **Spring Application**.
3. The **Spring Application** processes the request and stores the issue in the PostgreSQL Database.
4. The **Spring Application** sends a notification to the **City Management System**.
5. **City Officials and Developers** use the **City Management System** to view and manage the reported issue.

```mermaid
sequenceDiagram
    participant Residents
    participant Smart City App
    participant Spring Application
    participant PostgreSQL Database
    participant City Management System
    Residents->>Smart City App: Report Issue
    Smart City App->>Spring Application: REST API Request
    Spring Application->>PostgreSQL Database: Store Issue
    Spring Application->>City Management System: Notification
    City Management System->>Developers/City Officials: View Issue
```

### Runtime Scenario 2: City Data Update

1. **Node-RED** fetches data from **City APIs** at scheduled intervals.
2. **Node-RED** maps the data to FIWARE standards.
3. **Node-RED** sends the mapped data to
   the Orion Context Broker via **HTTP/REST**.
4. The **Orion Context Broker** stores the data.
5. The **Spring Application** periodically fetches updated data from
   the Orion Context Broker.

```mermaid
sequenceDiagram
    participant NodeRED
    participant City APIs
    participant Orion Context Broker
    participant Spring Application
    NodeRED->>City APIs: Fetch Data
    NodeRED->>NodeRED: Map Data to FIWARE
    NodeRED->>Orion Context Broker: Send Mapped Data (HTTP/REST)
    Orion Context Broker->>Orion Context Broker: Store Data
    Spring Application->>Orion Context Broker: Fetch Updated Data
```

### Runtime Scenario 3: Image Processing when Fetching Data from Orion Context Broker

1. **Spring Application** fetches data from the Orion Context Broker
   via a REST API request.
2. The **Spring Application** detects an image that needs processing.
3. The **Spring Application** sends a message to the **Message Queue**, including the URL of the image.
4. **Async Task Handlers** download the image using the URL and process the image using
   the Imaginary service.
5. Processed image data is stored back in **Telekom Cloud S3** and the Spring Application is notified.

```mermaid
sequenceDiagram
    participant Spring Application
    participant Orion Context Broker
    participant Telekom S3
    participant Message Queue
    participant Async Task Handlers
    participant Imaginary
    Spring Application->>Orion Context Broker: Fetch Data (REST API)
    Orion Context Broker->>Spring Application: Return Data with Image URL
    Spring Application->>Message Queue: Queue Image Processing Task with URL
    Message Queue->>Async Task Handlers: Process Task
    Async Task Handlers->>Spring Application: Get Image URL
    Async Task Handlers->>Telekom S3: Download Image
    Async Task Handlers->>Imaginary: Process Image
    Imaginary->>Telekom S3: Store Processed Image
    Async Task Handlers->>Spring Application: Notify Processing Complete
```

# Deployment View

## Overview Diagram

```mermaid
graph LR
    User[git push] --> GitHub
    GitHub --> BuildTest[Build & Test]
    BuildTest --> PublishStage[Publish]
    PublishStage --> DeployStage[Deploy]
    PublishStage --> |publish docker images| GitHubPackages[GitHub Packages]
    DeployStage[Deploy] --> Kubernetes[Kubernetes]
    DeployStage[Deploy] --> PlayStore[Google Playstore]
    DeployStage[Deploy] --> AppStore[Apple App Store]
    GitHubPackages -->|pulls docker image| Kubernetes[Kubernetes]
    Kubernetes --> App
    Kubernetes --> Backend
    Kubernetes --> CMS

    subgraph Kubernetes Cluster
    Kubernetes
    App
    Backend
    CMS
    end

    subgraph Mobile Deployment
    PlayStore
    AppStore
    end

    subgraph GitHub Actions
        BuildTest
        PublishStage
        DeployStage
    end
```

## Motivation

The deployment strategy for the Urbo Smart City application leverages GitHub Actions
for CI/CD, Kubernetes for managing the deployment and scaling of the application
components, and the respective app stores for distributing the mobile apps. This approach
ensures that the application is highly available, scalable, and can be easily maintained
and updated across all platforms.

## Quality and/or Performance Features

- **Scalability**: Kubernetes allows for easy scaling of application components based on load.
- **High Availability**: Applications can be distributed across multiple nodes to ensure high availability.
- **Automation**: GitHub Actions automates the build, test, and deployment process, reducing manual intervention and
  the potential for errors.
- **Flexibility**: Helm charts provide a flexible way to manage Kubernetes deployments.
- **Multi-Platform Support**: The CI/CD pipeline supports building and deploying web, Android, and iOS applications.

## Mapping of Building Blocks to Infrastructure

- **Backend**: Deploys the Spring Application, responsible for business logic and data processing.
- **Frontend**: Deploys the Smart City App for residents.
- **CMS**: Deploys the City Management System for city officials.
- **Mobile Deployment**: Deploys the hybrid mobile apps to the Google Play Store and Apple App Store.

## Infrastructure Level 1: CI/CD Pipeline

- **GitHub Actions**: Manages the build, test, and deployment process for all application components.

## Infrastructure Level 2: Kubernetes Cluster

- **Backend**: Hosts the Spring Application.
- **Frontend**: Hosts the Smart City App.
- **CMS**: Hosts the City Management System.

## Infrastructure Level 3: Mobile Deployment

- **Google Play Store**: Distributes the Android version of the Smart City App.
- **Apple App Store**: Distributes the iOS version of the Smart City App.

## CI/CD Pipeline Description

The CI/CD pipeline is managed using GitHub Actions and includes:

#### 1. Build & Test Stage

- **Trigger**: Push events to main branch and pull requests
- **Parallel Jobs**:
    - Backend (Spring):
        - Unit tests with JUnit
        - Integration tests with Testcontainers
        - SonarQube code analysis
        - Docker image build
    - Frontend (Ionic/Angular):
        - Component tests with Jest
        - SonarQube code analysis
        - E2E tests with Cypress
        - Progressive Web App build
        - Mobile app binaries generation

#### 2. Publish Stage

- **Artifacts**:
    - Docker images pushed to GitHub Container Registry
    - Mobile app artifacts stored as workflow artifacts

#### 3. Deploy Stage

- **Web Deployment**:
    - Helm chart deployment to Kubernetes
    - Canary rollout strategy
    - Automated rollback on health check failures
- **Mobile Deployment**:
    - Android: Automatic upload to Google Play Console
    - iOS: TestFlight submission
    - Production releases require maintainer approval

#### 4. Dependency Management

- **Dependabot Configuration**:
    - Daily dependency scans
    - Auto-PRs for security updates
    - Version update policies:
      | Update Type | Automation Level |
      |-------------|-------------------|
      | Security | Auto-merge if CI passes |
      | Minor | Create PR with CI check |
      | Major | Manual intervention |

# Architecture Decisions

## ADR 1: Use of Kubernetes for Deployment

**Context:**
The Urbo smart city app needs to support a scalable and highly available deployment environment to handle varying loads
and ensure high uptime. The deployment should also support seamless updates and rollback capabilities.

**Decision:**
We decided to use Kubernetes for managing the deployment of the application
components. Kubernetes provides robust
orchestration capabilities, enabling us to scale application components as needed and ensure high availability.

**Consequences:**

- **Positive:**
    - Easy scaling of application components based on load.
    - High availability through distribution of pods across multiple nodes.
    - Seamless updates and rollbacks.
- **Negative:**
    - Requires additional expertise to manage Kubernetes clusters.
    - Initial setup and configuration can be complex.

## ADR 2: CI/CD with GitHub Actions

**Context:**
Continuous integration and continuous deployment (CI/CD) are essential for maintaining the
quality and consistency of the Urbo application. The team needs a reliable CI/CD pipeline to
automate the build, test, and deployment processes.

**Decision:**
We decided to use GitHub Actions to manage the CI/CD pipeline. GitHub Actions offers
comprehensive support for building, testing, and deploying applications across multiple
platforms, and integrates well with GitHub repositories.

**Consequences:**

- **Positive:**
    - Automated build, test, and deployment processes reduce manual effort and potential errors.
    - Easy integration with GitHub repositories and third-party services.
    - Supports advanced workflows and environment-specific deployments.

- **Negative:**
    - Requires a learning curve for team members new to GitHub Actions.
    - Dependence on GitHub’s environment for hosted runners.

## ADR 3: Spring Boot for Backend

**Context:**
The backend of the Urbo app needs to be robust, scalable, and capable of handling complex business logic and data
processing. The chosen framework should support microservices architecture and RESTful APIs.

**Decision:**
We decided to use Spring Boot for the backend
development. Spring Boot provides a comprehensive framework for building
Java applications with built-in support for microservices and RESTful APIs.

**Consequences:**

- **Positive:**
    - Robust framework with extensive community support.
    - Built-in support for microservices and RESTful APIs.
    - Easy integration with other Spring ecosystem components.
- **Negative:**
    - Potentially steep learning curve for developers new to Spring Boot.
    - Requires careful management of dependencies to avoid conflicts.

## ADR 4: Ionic and Capacitor for Frontend

**Context:**
The frontend of the Urbo app needs to support multiple platforms (web, native apps, and progressive web apps) with a
single codebase. The chosen framework should facilitate cross-platform development and ensure a consistent user
experience.

**Decision:**
We chose Ionic for the frontend development, supplemented with Capacitor to enable native functionality. Ionic allows
for building cross-platform mobile applications from a single codebase, ensuring consistency across different platforms,
while Capacitor provides access to native APIs and capabilities.

**Consequences:**

- **Positive:**
    - Single codebase for web, native apps, and progressive web apps.
    - Consistent user experience across platforms.
    - Reduced development effort and time.
- **Negative:**
    - Performance may not be as optimal as native applications for highly demanding tasks.
    - Requires knowledge of web technologies (HTML, CSS, JavaScript) for development.

## ADR 5: MeiliSearch for Advanced Search Capabilities

**Context:**
The Urbo app requires advanced search capabilities to provide fast and relevant search results to users. The search
solution should be flexible and user-friendly.

**Decision:**
We chose Meilisearch for the search
functionalities. Meilisearch offers flexible search
input and advanced search
features, providing a more user-friendly search experience compared to traditional databases
like PostgreSQL.

**Consequences:**

- **Positive:**
    - Advanced search capabilities with flexible input.
    - Fast and relevant search results.
    - Easy to integrate with existing systems.
- **Negative:**
    - Additional service to manage and maintain.
    - Requires proper indexing and optimization for best performance.

## Summary of ADRs

Each architectural decision was made to address specific needs and requirements of the Urbo smart city app. The
decisions were based on careful consideration of the context, benefits, and potential drawbacks. By documenting these
decisions in ADR format, we provide a clear rationale and record for future reference.

# Quality Requirements

## Quality Tree

- **Performance**
    - Response time < 1s for 95% of requests
    - Handle 1000 concurrent users
- **Security**
    - HTTPS for all communications
    - Data encryption at rest and in transit
- **Usability**
    - Intuitive UI/UX
    - Mobile-friendly design
- **Maintainability**
    - Modular architecture
    - Comprehensive documentation
- **Scalability**
    - Horizontal scaling of application components

## Quality Scenarios

1. **Performance**: The system should be able to handle 1000 concurrent users with a response time of less than 1 second
   for 95% of requests.
2. **Security**: All data should be encrypted both at rest and in transit, with HTTPS enforced for all communications.
3. **Usability**: The application should provide an intuitive user interface that is easy to navigate, with a design
   that is optimized for mobile devices.
4. **Maintainability**: The system should have a modular architecture, allowing for easy updates and maintenance.
   Comprehensive documentation should be provided for all components.
5. **Scalability**: The application should be able to scale horizontally to handle increased load,
   with Kubernetes
   managing the scaling of application pods.

# Glossary

| Term                                                                   | Definition                                                                                                                                                                |
|------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [FIWARE](https://www.fiware.org/)                                      | A framework of open-source platform components to accelerate the development of Smart Solutions.                                                                          |
| [Node-RED](https://nodered.org/)                                       | A flow-based development tool for visual programming developed originally by IBM for wiring together hardware devices, APIs, and online services.                         |
| [Orion Context Broker](https://fiware-orion.readthedocs.io/en/master/) | A component of the FIWARE ecosystem that enables the management of context information at large scale.                                                                    |
| [Spring Boot](https://spring.io/projects/spring-boot)                  | An open-source Java-based framework used to create microservices.                                                                                                         |
| [PostgreSQL](https://www.postgresql.org/)                              | A powerful, open-source object-relational database system.                                                                                                                |
| Telekom S3                                                             | A scalable object storage service offered by Telekom Cloud Services.                                                                                                      |
| [Imaginary](https://github.com/h2non/imaginary)                        | A fast, HTTP microservice for high-level image processing.                                                                                                                |
| [Meilisearch](https://www.meilisearch.com/)                            | An open-source search engine that offers fast and relevant search experience.                                                                                             |
| REST API                                                               | Representational State Transfer Application Programming Interface, a set of rules for interacting with web services.                                                      |
| [Kubernetes](https://kubernetes.io/)                                   | An open-source container orchestration platform that automates the deployment, scaling, and management of containerized applications.                                     |
| CI/CD                                                                  | Continuous Integration and Continuous Deployment, a method to frequently deliver apps to customers by introducing automation into the stages of app development.          |
| GitHub Actions                                                         | A CI/CD platform integrated directly into GitHub that allows you to automate, customize, and execute software development workflows right in your repository.             |
| [Ionic](https://ionicframework.com/)                                   | A complete open-source SDK for hybrid mobile app development. It uses web technologies like HTML, CSS, and JavaScript to build cross-platform mobile applications.        |
| API Gateway                                                            | Handles incoming API requests from the Smart City App and City Management System to the Spring Application.                                                               |
| Data Integration Interface                                             | Facilitates communication between the Spring Application and the [Orion Context Broker](https://fiware-orion.readthedocs.io/en/master/).                                  |
| Data Fetching Interface                                                | Used by [Node-RED](https://nodered.org/) to fetch data from City APIs and send mapped data to the [Orion Context Broker](https://fiware-orion.readthedocs.io/en/master/). |
| ADR                                                                    | Architecture Decision Record, a document that captures an important architectural decision made along with its context and consequences.                                  |
| UX/UI                                                                  | User Experience/User Interface, focuses on the design and usability aspects of the application to ensure it is user-friendly and visually appealing.                      |
| CMS                                                                    | Content Management System, used by city officials to manage and monitor city data.                                                                                        |
| [Kotlin](https://kotlinlang.org/)                                      | A modern programming language used to develop the backend of the Urbo app.                                                                                                |
| [Angular](https://angular.dev/)                                        | A platform and framework for building single-page client applications using HTML and TypeScript, used to develop the CMS frontend.                                        |
