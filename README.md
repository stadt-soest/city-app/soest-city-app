# SoestApp

The SoestApp is an open source city app that retrieves services by and information about the city of Soest from various
sources and makes them available on a single platform for citizens.

The app accesses information from the Soest data platform and bundles the content available there into
thematic-functional modules.

The current modules (still under development - as of Jul. 2024) are

- News
- Events
- Citizen services
- Feedback/Support
- Parking
- POIs
- Waste calendar

The app is being developed by the city of Soest together with the company SWCode. The development is supported by the
federal funding programme "Modellprojekte Smart Cities (MPSC)" of the Kreditanstalt für Wiederaufbau (KfW) and the
Bundesministerium des Innern und für Heimat (BMI).

## Documentation

The documentation for the SoestApp is based on the arc42 architecture template. For more details, refer to
the [documentation](./docs/architecture/arc42.md).

## How to contribute

To be able to contribute to the SoestApp, you first need a free account on OpenCoDE.de.

New contributions to a project must go through a review process (pull request) and can only be accepted by the
identified repository owner / maintainer.

In order for your contributions to be approved, please fill out and sign
the [Contributor Licence Agreement (CLA)](./docs/cla.pdf) and send it to app@soest.de.

## Contact

Technical: m.delgado@soest.de<br>
Organisational: h.ernst@soest.de

## License

Copyright Stadt Soest, 2024, licensed under the EUPL.
